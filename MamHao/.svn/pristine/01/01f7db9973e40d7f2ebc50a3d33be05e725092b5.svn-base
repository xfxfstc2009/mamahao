//
//  FilterTermInfo.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


class FilterTermInfo: NSObject {
    
    let termClasses: [FilterTermClass]
    
    init(dictionary: [String: AnyObject]) {
        var classes: [FilterTermClass] = [FilterTermClass]()
        
        if var brandDictionaries = dictionary["brands"] as? [[String: AnyObject]] {
            let filtered = brandDictionaries.filter({ (aBrandDictionary: [String: AnyObject]) -> Bool in
                let b = aBrandDictionary as NSDictionary
                let flag0 = b.hasKey("bId")
                let flag1 = b.hasKey("brand_id")
                return flag0 || flag1
            })
            let brandTerms = filtered.map({(brandDictionary: [String : AnyObject]) -> FilterTermBrand in
                return FilterTermBrand(dictionary: brandDictionary)
            })
            if brandTerms.count != 0 {
                let brandClass = FilterTermClass(name: "品牌", type: .Brand, terms: brandTerms)
                classes.append(brandClass)
            }
        }
        
        if let ageStrings = dictionary["ages"] as? [String] {
            let ageTerms = ageStrings.map({(ageString: String) -> FilterTermAge in
                return FilterTermAge(string: ageString)
            })
            if ageTerms.count != 0 {
                let ageClass = FilterTermClass(name: "年龄", type: .Normal, terms: ageTerms)
                classes.append(ageClass)
            }
        }
        
        if let categoryDictionaries = dictionary["types"] as? [[String: AnyObject]] {
            let categoryTerms = categoryDictionaries.map({(categoryDictionary: [String : AnyObject]) -> FilterTermCategory in
                return FilterTermCategory(dictionary: categoryDictionary)
            })
            if categoryTerms.count != 0 {
                let categoryClass = FilterTermClass(name: "类目", type: .Category, terms: categoryTerms)
                classes.append(categoryClass)
            }
        }
        
        self.termClasses = classes
        
        super.init()
    }

}


class FilterTermClass: NSObject {
    let name: String
    let type: MMHFilterTermType
    let terms: [FilterTerm]
    
    init(name: String, type: MMHFilterTermType, terms: [FilterTerm]) {
        self.name = name
        self.type = type
        self.terms = terms
    }
}


class FilterTerm: NSObject {
    
}


class FilterTermBrand: FilterTerm {
    let id: String
    let name: String
    let imageURLString: String
    
    init(dictionary: [String: AnyObject]) {
        if let id = dictionary.stringForKey("bId") {
            self.id = id
        }
        else if let id = dictionary.stringForKey("brand_id") {
            self.id = id
        }
        else {
            self.id = ""
        }

        if let imageURLString = dictionary.stringForKey("bLogo") {
            self.imageURLString = imageURLString
        }
        else if let imageURLString = dictionary.stringForKey("brand_logo") {
            self.imageURLString = imageURLString
        }
        else {
            self.imageURLString = ""
        }

        if let name = dictionary.stringForKey("bName") {
            self.name = name
        }
        else if let name = dictionary.stringForKey("brand_name") {
            self.name = name
        }
        else {
            self.name = ""
        }
        
        super.init()
    }
}


class FilterTermCategory: FilterTerm {
    let id: String
    let name: String
    
    init(dictionary: [String: AnyObject]) {
        if let id = dictionary.stringForKey("style_id") {
            self.id = id
        }
        else if let id = dictionary.stringForKey("styleId") {
            self.id = id
        }
        else if let id = dictionary.stringForKey("typeId") {
            self.id = id
        }
        else {
            self.id = ""
        }
        
        if let name = dictionary.stringForKey("style_id") {
            self.name = name
        }
        else if let name = dictionary.stringForKey("styleName") {
            self.name = name
        }
        else if let name = dictionary.stringForKey("typeName") {
            self.name = name
        }
        else {
            self.name = ""
        }
        
        super.init()
    }
}


class FilterTermAge: FilterTerm {
    let monthAge: MonthAge
    
    var name: String {
        get {
            return self.monthAge.displayName()
        }
    }
    
    var parameter: String {
        get {
            return self.monthAge.rawString()
        }
    }
    
    init(string: String) {
        self.monthAge = MonthAge(string: string)
        super.init()
    }
}
