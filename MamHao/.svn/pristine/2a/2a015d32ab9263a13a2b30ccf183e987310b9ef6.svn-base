//
//  MMHRefundDetailViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHRefundHomeViewController.h"
#import "MMHOrderDetailViewController.h"
@class MMHOrderDetailViewController;
@class MMHRefundHomeViewController;

#pragma mark 判断进入页面
typedef NS_ENUM(NSInteger, MMHRefundDetailPageUsingType) {
    MMHRefundDetailPageUsingTypeNormal,                               /**< 正常调用*/
    MMHRefundDetailPageUsingTypeRefundDetail,                         /**< 退款退货详情调用*/
};

#pragma mark 配送方式
typedef NS_ENUM(NSInteger, MMHRefundType) {
    MMHRefundTypeShop = 1,                                                              /**< 门店配送*/
    MMHRefundTypeSelf = 2,                                                              /**< 自提*/
    MMHRefundTypeExpress = 3,                                                           /**< 快递*/
};

#pragma mark 退货状态
typedef NS_ENUM(NSInteger, MMHRefundState) {
    MMHRefundStateNormol,                                                               /**< 申请退货*/
    MMHRefundStateApply,                                                                /**< 审核*/
    MMHRefundStateInspection,                                                           /**< 验货*/
    MMHRefundStateSuccess,                                                              /**< 完成*/
};

#pragma mark 审核状态
typedef NS_ENUM(NSInteger, MMHRefundApplyState) {
    MMHRefundApplyStateNormol = 1,                                                      /**< 审核中*/
    MMHRefundApplyStateSuccess = 2,                                                     /**< 审核成功*/
    MMHRefundApplyStateFail = 5,                                                        /**< 审核失败*/
    MMHRefundApplyStateCloseNormol = 6,                                                 /**< 用户主动关闭*/
    MMHRefundApplyStateCloseNotPass = 8,                                                /**< 未通过关闭*/
};

#pragma mark 支付类型
typedef NS_ENUM(NSInteger, MMHRefundPayType) {
    MMHRefundPayTypeOnLine = 1,                                                         /**< 线上支付*/
    MMHRefundPayTypeCashOnDelivery = 2                                                  /**< 货到付款*/
};

#pragma mark 用户是否收货状态
typedef NS_ENUM(NSInteger, MMHRefundIsGoodsReceipt) {                                   // 基于 MMHRefundApplyState
    MMHRefundIsGoodsReceiptNormol,                                                      /**< 其他*/
    MMHRefundIsGoodsReceiptYes = 3,                                                     /**< 收到货*/
    MMHRefundIsGoodsReceiptNo = 4                                                       /**< 没有收到货*/
};

#pragma mark 门店配送状态
typedef NS_ENUM(NSInteger, MMHRefundShopType) {                                          // 门店配送
    MMHRefundShopTypeNormol,                                                             /**< 未选择*/
    MMHRefundShopTypeSelf = 2,                                                           /**< 自己送货上门店退货*/
    MMHRefundShopTypeExpress = 3,                                                        /**< 快递上门取件退货*/
    MMHRefundShopTypeShopGet = 1                                                         /**< 门店上门取件*/
};

#pragma mark 上门取件类别
typedef NS_ENUM(NSInteger, MMHRefundShopTypeExpressState) {
    MMHRefundShopTypeExpressStateChoose,                                                 /**< 快递上门取件-选择物流*/
    MMHRefundShopTypeExpressStateNormol                                                  /**< 快递上门取件-完成*/
};

@interface MMHRefundDetailViewController : AbstractViewController
@property (nonatomic,assign)MMHRefundDetailPageUsingType refundDetailPageUsingType;      /**< 退款页面类型*/
@property (nonatomic,assign)MMHRefundType refundType;                                    /**< 退款送货类型*/
@property (nonatomic,assign)MMHRefundState refundState;                                  /**< 退款状态*/
@property (nonatomic,assign)MMHRefundApplyState refundApplyState;                        /**< 审核状态*/
@property (nonatomic,assign)MMHRefundTypeWithHome refundTypeWithHome;                    /**< 是否退货*/
@property (nonatomic,assign)MMHRefundPayType refundPayType;                              /**< 支付方式*/
@property (nonatomic,assign)MMHRefundIsGoodsReceipt refundIsGoodsReceipt;                /**< 是否收货*/
@property (nonatomic,assign)MMHRefundShopType refundShopType;                            /**< 退货方式选择*/
@property (nonatomic,assign)MMHRefundShopTypeExpressState refundShopTypeExpressState;    /**< 上门取件*/

// 上个页面传递的订单编号
@property (nonatomic,copy)NSString *transferOrderNo;                                 /**< 上个页面传递的订单编号*/
@property (nonatomic,copy)NSString *transferItemId;                                  /**< 上个页面传递的商品编号*/

@end
