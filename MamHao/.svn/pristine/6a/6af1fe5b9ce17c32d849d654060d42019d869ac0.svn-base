//
//  MMHCurrentLocationModel.m
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCurrentLocationModel.h"
#import "MMHAddressSingleModel.h"


static NSString *defaultAreaId = @"330103";
static CLLocationDegrees defaultLongitude = 120.188484;
static CLLocationDegrees defaultLatitude = 30.2325363;


@implementation MMHCurrentLocationModel


+ (MMHCurrentLocationModel *)sharedLocation {
    static MMHCurrentLocationModel *location = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        location = [[MMHCurrentLocationModel alloc] init];
    });
    return location;
}


- (NSString *)areaId {
    if (self.selectedAddressSingleModel) {
        return self.selectedAddressSingleModel.areaId;
    }

    if (_receiptAreaId.length != 0) {                // 如果有默认收货地址id
        return _receiptAreaId;
    } else {
        if (_currentAreaId.length != 0) {
            return _currentAreaId;
        }
    }
    return defaultAreaId;
}


- (CGFloat)lat {
    if (self.selectedAddressSingleModel) {
        return self.selectedAddressSingleModel.lat;
    }

    if (_receiptLat) {
        return _receiptLat;
    } else {
        if (_currentLat) {
            return _currentLat;
        }
    }
    return defaultLatitude;
}

- (CGFloat)lng {
    if (self.selectedAddressSingleModel) {
        return self.selectedAddressSingleModel.lng;
    }

    if (_receiptLng){
        return _receiptLng;
    } else {
        if (_currentLng){
            return _currentLng;
        }
        return defaultLongitude;
    }
}

-(NSString *)deliveryAddrId{                // -1 代表用户没有默认地址
    if (_deliveryAddrId){
        return _deliveryAddrId;
    }
    return @"-1";
}


- (NSString *)city {
    if (self.selectedAddressSingleModel) {
        return self.selectedAddressSingleModel.city;
    }

    if (self.receiptCity.length != 0) {
        return self.receiptCity;
    }

    if (self.currentCity.length != 0) {
        return self.currentCity;
    }
    return @"杭州";
}


-(NSString *)district{
    if (_receiptArea.length != 0){
        return _receiptArea;
    } else {
        if (_currentDistrict.length != 0){
            return _currentDistrict;
        }
    }
    return @"上城区";
}


- (NSString *)street
{
    if (self.selectedAddressSingleModel) {
        return self.selectedAddressSingleModel.gpsAddress;
    }

    if (_receiptArea.length != 0){
        return _receiptArea;
    }
    if (self.currentStreet.length != 0) {
        return self.currentStreet;
    }

    if (_street.length != 0) {
        return _street;
    }

    return @"西湖";
}


- (CLLocation *)myGeographicalLocation
{
    if (self.lat && self.lng) {
        return [[CLLocation alloc] initWithLatitude:self.lat longitude:self.lng];
    }
    return [[CLLocation alloc] initWithLatitude:defaultLatitude longitude:defaultLongitude];
}


- (CLLocation *)gpsLocation
{
    if (self.currentLat && self.currentLng) {
        return [[CLLocation alloc] initWithLatitude:self.currentLat longitude:self.currentLng];
    }
    return [[CLLocation alloc] initWithLatitude:defaultLatitude longitude:defaultLongitude];
}


- (NSString *)gpsAreaID
{
    NSString *areaId = self.currentAreaId;
    if (areaId.length != 0) {
        return areaId;
    }

    return defaultAreaId;
}


- (void)setSelectedAddressSingleModel:(MMHAddressSingleModel *)selectedAddressSingleModel
{
    _selectedAddressSingleModel = selectedAddressSingleModel;

    [[NSNotificationCenter defaultCenter] postNotificationName:MMHLocationChangedNotification
                                                        object:nil
                                                      userInfo:nil];
}


- (NSString *)streetAddress
{
    if (self.selectedAddressSingleModel != nil) {
//        if (self.selectedAddressSingleModel.addrDetail.length != 0) {
//            return self.selectedAddressSingleModel.addrDetail;
//        }
        if (self.selectedAddressSingleModel.shortGPSAddress.length != 0) {
            return self.selectedAddressSingleModel.shortGPSAddress;
        }
        return self.selectedAddressSingleModel.gpsAddress;
    }
    
//    if (self.receiptAddressDetail.length != 0) {
//        return self.receiptAddressDetail;
//    }

    if (self.receiptGpsAddress.length != 0) {
        return self.receiptGpsAddress;
    }

    if (self.currentStreet.length != 0) {
        return self.currentStreet;
    }

    if (self.currentTownship.length != 0) {
        return self.currentTownship;
    }
    
    return @"西湖";
}


- (BOOL)isLocationUserSelected
{
    return self.selectedAddressSingleModel != nil;
}

- (NSDictionary *)parametersForCurrentLocation{
    NSMutableDictionary *parametersDic = [NSMutableDictionary dictionary];
    if (_selectedAddressSingleModel){
        [parametersDic setObject:_selectedAddressSingleModel.areaId forKey:@"areaId"];
        [parametersDic setObject:@(_selectedAddressSingleModel.lng) forKey:@"lng"];
        [parametersDic setObject:@(_selectedAddressSingleModel.lat) forKey:@"lat"];
    } else {
        [parametersDic setObject:[MMHCurrentLocationModel sharedLocation].areaId forKey:@"areaId"];
        [parametersDic setObject:@([MMHCurrentLocationModel sharedLocation].lng) forKey:@"lng"];
        [parametersDic setObject:@([MMHCurrentLocationModel sharedLocation].lat) forKey:@"lat"];
    }
    
    return parametersDic;
}
@end
