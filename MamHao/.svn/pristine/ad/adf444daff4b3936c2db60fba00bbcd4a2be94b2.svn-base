//
//  AbstractViewController.m
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <objc/runtime.h>
#import "AbstractViewController.h"
#import "MMHAccount.h"
#import "MMHLoginViewController.h"
#import "MMHAccountSession.h"
#import "MamHao-Swift.h"


#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:16.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:14.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   200

static char buttonActionBlockKey;

@interface AbstractViewController ()
@property (nonatomic,strong) UIView  *barTitleView;
@property (nonatomic,strong) UILabel *barMainTitleLabel;
@property (nonatomic,strong) UILabel *barSubTitleLabel;

@end

@implementation AbstractViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [MMHLogbook beginLogPageView:NSStringFromClass([self class])];    // 埋点
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [MMHLogbook endLogPageView:NSStringFromClass([self class])];    // 埋点
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = kStyleBackgroundColor;
    // 初始化自定义titleView
//    [self createBarTitleView];
    
    
    if (IS_IOS7_LATER) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    } else {
        [[UIApplication sharedApplication] setStatusBarStyle:2];
    }
    
//    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (self != [self.navigationController.viewControllers firstObject]) {
        [self setGoBackItemOnLeftBarButtonItem];
    }
}


- (void)setDismissItemOnLeftBarButtonItem
{
    UIBarButtonItem *cancelItem = [UIBarButtonItem itemWithImageName:@"mine_details_close_normal_icon"
                                                highlightedImageName:@"mine_details_close_down_icon"
                                                               title:nil
                                                              target:self
                                                              action:@selector(dismissViewController)];
    self.navigationItem.leftBarButtonItem = cancelItem;
}


- (void)setGoBackItemOnLeftBarButtonItem {
    UIBarButtonItem *goBackItem = [UIBarButtonItem itemWithImageName:@"basc_nav_back"
                                                highlightedImageName:nil
                                                               title:nil
                                                              target:self
                                                              action:@selector(popViewController)];
    self.navigationItem.leftBarButtonItems = @[goBackItem];
    
//    [self leftBarButtonWithTitle:@"" barNorImage:[[UIImage imageNamed:@"basc_nav_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] barHltImage:[[UIImage imageNamed:@"navBar_back_hlt"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] action:^{
//        [self popViewController];
//    }];
}


#pragma mark - View 配置
-(void)createBarTitleView{
    // view
    _barTitleView = [[UIView alloc]initWithFrame:CGRectZero];
    _barTitleView.backgroundColor = [UIColor clearColor];
    _barTitleView.clipsToBounds = YES;
    
    // barMainTitle
    _barMainTitleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _barMainTitleLabel.backgroundColor = [UIColor clearColor];
    _barMainTitleLabel.font = BAR_MAIN_TITLE_FONT;
    _barMainTitleLabel.textColor = [UIColor blackColor];
    [_barTitleView addSubview:_barMainTitleLabel];
    
    // barSubTitle
    _barSubTitleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    _barSubTitleLabel.backgroundColor = [UIColor clearColor];
    _barSubTitleLabel.font = BAR_SUB_TITLE_FONT;
    _barSubTitleLabel.textColor = [UIColor blackColor];
    [_barTitleView addSubview:_barSubTitleLabel];
    
    self.navigationItem.titleView = _barTitleView;
}


#pragma mark 赋值
-(void)setBarMainTitle:(NSString *)barMainTitle
{
    if (_barTitleView == nil) {
        [self createBarTitleView];
    }

    _barMainTitle = [barMainTitle copy];
    _barMainTitleLabel.text = _barMainTitle;
    
    [_barMainTitleLabel sizeToFit];
    CGRect rect = _barMainTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barMainTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

-(void)setBarSubTitle:(NSString *)barSubTitle{
    if (_barTitleView == nil) {
        [self createBarTitleView];
    }

    _barSubTitle = [barSubTitle copy];
    _barSubTitleLabel.text = _barSubTitle;
    
    [_barSubTitleLabel sizeToFit];
    CGRect rect = _barSubTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barSubTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

-(void)resetBarTitleView{
    CGSize mainTitleSize = _barMainTitleLabel.bounds.size;
    CGSize subTitleSize = _barSubTitleLabel.bounds.size;
    
    CGFloat titleViewHeight = mainTitleSize.height + subTitleSize.height;
    if (_barMainTitle.length && _barSubTitle.length){
        titleViewHeight += BAR_TITLE_PADDING_TOP;
    }
    
    _barTitleView.bounds = CGRectMake(0, 0, BAR_TITLE_MAX_WIDTH, titleViewHeight);
    if ([MMHTool isEmpty:_barSubTitle]){
        if (titleViewHeight < 44){
            titleViewHeight = 44;
        }
        _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH *.5, 22);
    } else {
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH *.5, _barMainTitleLabel.bounds.size.height * .5);
        _barSubTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, titleViewHeight - _barSubTitleLabel.bounds.size.height*0.5);
    }
}


#pragma mark - BarButton

-(void)hidesBackButton{
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void (^)(void))actionBlock{
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock
{
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    return button;
}



- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    
    return button;
}

-(void)actionButtonClicked:(UIButton *)sender{
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &buttonActionBlockKey);
    actionBlock();
}


#pragma mark - TabBar


-(void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated{
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    if (!tabBarController || (tabBar.hidden == hidden)){
        return;
    }
    
    CGFloat tabBarHeight = tabBar.bounds.size.height;
    CGFloat adjustY = hidden ? tabBarHeight : -tabBarHeight;
    if (!hidden){
        tabBar.hidden = NO;
    }
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:animated ? 0.3 : 0. animations:^{
        // adjust TabBar
        CGRect rect = tabBar.frame;
        rect.origin.y += adjustY;
        tabBar.frame = rect;
        
        // adjust TransitionView
        for (UIView *view in tabBarController.view.subviews)
        {
            if ([NSStringFromClass([view class]) hasSuffix:@"TransitionView"])
            {
                if (!IS_IOS7_LATER)
                {
                    CGRect rect = view.frame;
                    rect.size.height += adjustY;
                    view.frame = rect;
                }
                view.backgroundColor = weakSelf.view.backgroundColor;
            }
        }
    }
                     completion:^(BOOL finished)
     {
         tabBar.hidden = hidden;
         
         // adjust self.view
         if (IS_IOS7_LATER)
         {
             CGRect rect = weakSelf.view.frame;
             rect.size.height += adjustY;
             weakSelf.view.frame = rect;
         }
     }];
}


- (void)presentLoginViewControllerWithSucceededHandler:(void(^)(void))succeededHandler
{
#if !defined (DEBUG_LOUIS)
    if ([[MMHAccountSession currentSession] hasAnyAccountLoggedInOrLoggingIn]) {
        return;
    }
#endif
    
    LoginViewController *loginViewController = [[LoginViewController alloc] init];
    if (succeededHandler) {
        loginViewController.succeededHandler = ^(MMHAccount *account) {
            succeededHandler();
        };
    }
    MMHNavigationController *navigationController = [[MMHNavigationController alloc] initWithRootViewController:loginViewController];
    [self.navigationController presentViewController:navigationController animated:YES completion:^{
        
    }];
    return;
}


- (void)pushViewController:(UIViewController *)viewController
{
    if (self.navigationController) {
        [self.navigationController pushViewController:viewController animated:YES];
    }
}


- (void)pushViewControllers:(NSArray *)viewControllers
{
    if (self.navigationController) {
        NSArray *existingViewControllers = self.navigationController.viewControllers;
        NSArray *finalViewControllers = [existingViewControllers arrayByAddingObjectsFromArray:viewControllers];
        [self.navigationController setViewControllers:finalViewControllers animated:YES];
    }
}


- (void)popViewController
{
    if (self.navigationController) {
        UIBarButtonItem *item = [[self.navigationItem leftBarButtonItems] firstObject];
        item.enabled = NO;
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)popToViewControllerOfClass:(Class)aClass {
    if (aClass == nil) {
        return;
    }

    if (self.navigationController) {
        UIBarButtonItem *item = self.navigationItem.leftBarButtonItems.firstObject;
        item.enabled = NO;
        [self.navigationController popToViewControllerOfClass:aClass];
    }
}


- (void)dismissViewController
{
    if (self.navigationController) {
        [self.navigationController dismissViewControllerAnimated:YES
                                                      completion:nil];
        return;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)blankViewButtonTapped
{
    
}


@end
