//
//  MMHShopFilter.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFilter.h"

@interface MMHShopFilter : MMHFilter

- (id)initWithShopID:(NSString *)shopID groupID:(NSString *)groupID;
@end
