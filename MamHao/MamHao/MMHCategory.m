//
//  MMHCategory.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCategory.h"


@implementation MMHCategory


- (NSString *)categoryID
{

    if (_categoryID) {
        return _categoryID;
    }

    return @"0";
}


- (id)initWithCategoryID:(NSString *)categoryID
{
    self = [self init];
    if (self) {
        self.categoryID = categoryID;
    }
    return self;
}


- (NSDictionary *)parameters
{
    return @{@"goodsTypeId": self.categoryID};
}


@end
