//
//  MMHFilter.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFilter.h"
#import "MMHCategory.h"
#import "MMHSort.h"
#import "MMHFilterTermData.h"
#import "MamHao-Swift.h"
#import "UIImage+RenderedImage.h"
#import "MMHAssistant.h"
#import "MMHFilterTerm.h"
#import "MMHCurrentLocationModel.h"
#import "MMHShopFilter.h"


@interface MMHFilter () <MMHSortDelegate>

@property (nonatomic, strong) MMHSort *sort;
@property (nonatomic, strong) MMHCategory *category;
@property (nonatomic, strong) NSMutableArray *selectedTerms;
@property (nonatomic) BOOL hasPendingTermChanges;
//@property (nonatomic, strong) NSMutableArray *pendingSelectedTermChanges;
//@property (nonatomic, strong) NSMutableArray *pendingDeselectedTermChanges;
//@property (nonatomic, readwrite) MMHFilterModule module;
@end


@implementation MMHFilter


- (NSMutableArray *)selectedTerms
{
    if (_selectedTerms == nil) {
        _selectedTerms = [[NSMutableArray alloc] init];
    }
    return _selectedTerms;
}


//- (NSMutableDictionary *)pendingTermChanges
//{
//    if (_pendingTermChanges == nil) {
//        _pendingTermChanges = [NSMutableDictionary dictionary];
//    }
//    return _pendingTermChanges;
//}


- (MMHFilter *)initWithCategory:(MMHCategory *)category
{
    self = [self init];
    if (self) {
        self.category = category;
        self.module = MMHFilterModuleCategory;
        self.sort = [[MMHSort alloc] init];
        self.sort.delegate = self;
    }
    return self;
}


- (id)initWithKeyword:(NSString *)keyword
{
    self = [self init];
    if (self) {
        self.keyword = keyword;
        self.module = MMHFilterModuleSearching;
        self.sort = [[MMHSort alloc] init];
        self.sort.delegate = self;
    }
    return self;
}


+ (MMHFilter *)filterWithShopID:(NSString *)shopID
{
    return [[MMHShopFilter alloc] initWithShopID:shopID groupID:nil];
}


+ (MMHFilter *)filterWithShopID:(NSString *)shopID groupID:(NSString *)shopGroupID
{
    return [[MMHShopFilter alloc] initWithShopID:shopID groupID:shopGroupID];
}


- (instancetype)initWithBrandID:(NSString *)brandID
{
    self = [self init];
    if (self) {
        self.brandID = brandID;
        self.module = MMHFilterModuleBrand;
        self.sort = [[MMHSort alloc] init];
        self.sort.delegate = self;
    }
    return self;
}


+ (MMHFilter *)filterWithBrandID:(NSString *)brandID
{
    if ([brandID length] == 0) {
        return nil;
    }
    return [[MMHFilter alloc] initWithBrandID:brandID];
}


- (NSDictionary *)parametersForTermInfo
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    switch (self.module) {
        case MMHFilterModuleCategory:
        [parameters setNullableObject:self.category.categoryID forKey:@"typeId"];
        break;
        case MMHFilterModuleSearching:
        break;
        case MMHFilterModuleShop:
        [parameters setNullableObject:self.shopID forKey:@"shopId"];
        break;
        case MMHFilterModuleBrand:
        [parameters setNullableObject:self.brandID forKey:@"brandId"];
        break;
        default:
        break;
    }
    return parameters;
}


- (NSDictionary *)parameters
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    [parameters addEntriesFromDictionary:[self.category parameters]];
    [parameters addEntriesFromDictionary:[self.sort parameters]];
    parameters[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    
    switch (self.module) {
        case MMHFilterModuleCategory:
            parameters[@"jsonTerm"] = [self termString];
            break;
        case MMHFilterModuleSearching:
            parameters[@"queryStr"] = self.keyword;
            break;
        case MMHFilterModuleShop:
        case MMHFilterModuleBrand:
            parameters[@"jsonTerm"] = [self termString];
            break;
        default:
            break;
    }

    return parameters;
}


- (void)fetchTermsDataWithFinishedHandler:(void (^)(BOOL succeeded))finishedHandler
{
    [[MMHFilterTermData sharedData] fetchTermsForFilter:self
                                         succeededHandler:^(FilterTermInfo *termInfo) {
                                             finishedHandler(YES);
                                         } failedHandler:^(NSError *error) {
                finishedHandler(NO);
            }];
}


- (void)sortDidChange:(MMHSort *)sort
{
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(filterDidChange:)]) {
            [self.delegate filterDidChange:self];
        }
    }
}


- (BOOL)isTermsDataAvailable
{
    return [[MMHFilterTermData sharedData] isTermsDataAvailableForFilter:self];
}


//- (NSInteger)selectedTermIndexAtSection:(NSInteger)section
//{
//    NSNumber *key = @(section);
//    if ([self.pendingTermChanges count] > 0) {
//        if ([[self.pendingTermChanges allKeys] containsObject:key]) {
//            return [self.pendingTermChanges[key] integerValue];
//        }
//    }
//
//    if (![[self.selectedTerms allKeys] containsObject:key]) {
//        return -1;
//    }
//
//    return [self.selectedTerms[key] integerValue];
//}


- (NSArray *)selectedIndexPaths
{
    return self.selectedTerms;
}


- (BOOL)isIndexPathSelected:(NSIndexPath *)indexPath
{
    return [self.selectedTerms containsObject:indexPath];
}


- (void)selectTermAtIndexPath:(NSIndexPath *)indexPath
{
    self.hasPendingTermChanges = YES;
}


- (void)deselectTermAtIndexPath:(NSIndexPath *)indexPath
{
    self.hasPendingTermChanges = YES;
}


- (NSInteger)numberOfTermClasses
{
    return [[MMHFilterTermData sharedData] numberOfTermClassesForFilter:self];
}


- (NSInteger)numberOfTermsAtSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] numberOfTermsAtSection:section forFilter:self];
}


- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] termAtIndex:index atSection:section forFilter:self];
}


- (NSString *)titleForSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] titleForSection:section filter:self];
}


- (MMHFilterTermType)termTypeAtSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] termTypeAtSection:section forFilter:self];
}


- (void)confirmTermChangesWithIndexPaths:(NSArray *)indexPaths
{
    if (!self.hasPendingTermChanges) {
        return;
    }

    self.hasPendingTermChanges = NO;
    self.selectedTerms = [indexPaths mutableCopy];

    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(filterDidChange:)]) {
            [self.delegate filterDidChange:self];
        }
    }
}


- (NSString *)termString
{
    NSMutableDictionary *termDictionary = [[NSMutableDictionary alloc] init];

    NSMutableArray *brands = [[NSMutableArray alloc] init];
    NSMutableArray *ages = [[NSMutableArray alloc] init];
    NSString *categoryID = nil;
    
    for (NSIndexPath *indexPath in self.selectedIndexPaths) {
        FilterTerm *term = [self termAtIndex:indexPath.row atSection:indexPath.section];
        if ([term isKindOfClass:[FilterTermBrand class]]) {
            FilterTermBrand *brand = (FilterTermBrand *)term;
            NSString *brandID = brand.id;
            [brands addObject:brandID];
        }
        else if ([term isKindOfClass:[FilterTermAge class]]) {
            FilterTermAge *age = (FilterTermAge *)term;
            [ages addObject:age.parameter];
        }
        else if ([term isKindOfClass:[FilterTermCategory class]]) {
            FilterTermCategory *category = (FilterTermCategory *)term;
            categoryID = category.id;
        }
    }
    
    termDictionary[@"brand"] = brands;
    termDictionary[@"age"] = ages;
    [termDictionary setNullableObject:categoryID forKey:@"goodsType"];
    
    switch (self.module) {
        case MMHFilterModuleCategory:
            termDictionary[@"goodsType"] = self.category.categoryID;
            break;
        case MMHFilterModuleShop:
            termDictionary[@"shopId"] = self.shopID;
            [termDictionary setNullableObject:self.shopGroupID forKey:@"groupId"];
            break;
        case MMHFilterModuleBrand:
            termDictionary[@"brand"] = @[self.brandID];
            break;
        default:
            break;
    }
    
    NSLog(@"louis: oh my final term: %@", termDictionary);
    return [termDictionary JSONString];
    
}


- (BOOL)multiSelectionEnabledAtSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] multiSelectionEnabledAtSection:section forFilter:self];
}


- (BOOL)hasFilterTerms {
    if (self.module == MMHFilterModuleSearching) {
        return NO;
    }

    if (self.module == MMHFilterModuleShop) {
        if (self.shopGroupID.length != 0) {
            return NO;
        }
    }
    return YES;
}
@end
