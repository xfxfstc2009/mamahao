//
//  MonthAge.swift
//  MamHao
//
//  Created by Louis Zhu on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


@objc
class MonthAge: NSObject {
    
    enum MonthAgeValue {
        
        case All
        case Pregnant
        
        case Range(Int, Int)
        
        
        init(string: String) {
            switch (string) {
            case "-2":
                self = .All
            case "-1-0":
                self = .Pregnant
            case "84-":
                self = .Range(84, 10000)
            default:
                let components = string.componentsSeparatedByString("-")
                if components.count == 2 {
                    let from = (components[0] as NSString).integerValue
                    let to = (components[1] as NSString).integerValue
                    self = .Range(from, to)
                }
                else {
                    self = .Pregnant
                }
            }
        }
        
        
        func rawString() -> String {
            switch self {
            case .Pregnant:
                return "-1-0"
            case .All:
                return "-2"
            case .Range(let from, let to):
                if to == 10000 {
                    return "\(from)-"
                }
                return "\(from)-\(to)"
            }
        }
        
        
        func displayName() -> String {
            switch self {
            case .All:
                return "全部"
            case .Pregnant:
                return "孕妈咪"
            case .Range(let from, let to):
                if from < 12 {
                    return "\(from)-\(to)月"
                }
                
                if (from >= 7 * 12) {
                    return "7岁以上"
                }
                
                let fromYear = from / 12
                let toYear = to / 12
                return "\(fromYear)-\(toYear)岁"
            }
        }
        
    }
    
    var value: MonthAgeValue = .Pregnant
    
    init(string: String) {
        self.value = MonthAgeValue(string: string)
        super.init()
    }
    
    func rawString() -> String {
        return self.value.rawString()
    }
    
    func displayName() -> String {
        return self.value.displayName()
    }
}


extension MonthAge {
    
    
    convenience init(months: NSTimeInterval) {
        if months < 0 {
            self.init(string: "-1-0")
        }
        else if months < 3 {
            self.init(string: "0-3")
        }
        else if months < 6 {
            self.init(string: "3-6")
        }
        else if months < 12 {
            self.init(string: "6-12")
        }
        else if months < 36 {
            self.init(string: "12-36")
        }
        else if months < 84 {
            self.init(string: "36-84")
        }
        else {
            self.init(string: "84-")
        }
    }
    
    
    enum MomStatus: Int {
        case Unknown = 0
        case Mom = 1
        case Pregnant = 2
    }
    
    static func currentSetMonthAge() -> MonthAge {
        var momStatus = MomStatus.Unknown
        if let momStatusString = NSUserDefaults.standardUserDefaults().stringForKey("BabyMaMState") {
            if momStatusString == "孕妈" {
                momStatus = .Pregnant
                return MonthAge(string: "-1-0")
            }
            else if momStatusString == "宝妈" {
                momStatus = .Mom
                if let birthdayString = NSUserDefaults.standardUserDefaults().stringForKey("BabyBirthday") {
                    let dateFormatter = NSDateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    if let date = dateFormatter.dateFromString(birthdayString) {
                        let timeInterval = date.timeIntervalSinceNow * -1.0
                        let months = timeInterval / 3600 / 24 / 30
                        return MonthAge(months: months)
                    }
                }
            }
            if momStatus == .Unknown {
                return MonthAge(string: "-2")
            }
            
            
        }
        
        return MonthAge(string: "-2")
    }
}
