//
//  MMHTimeline.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHLogger.h"
#import "UIView+MMHPrompt.h"


@class MMHTimeline;


@protocol MMHTimelineDelegate <NSObject>

- (void)timelineDataRefetched:(MMHTimeline *)timeline;
- (void)timelineMoreDataFetched:(MMHTimeline *)timeline;
- (void)timeline:(MMHTimeline *)timeline fetchingFailed:(NSError *)error;

@end



@interface MMHTimeline : NSObject

@property (nonatomic, weak) id<MMHTimelineDelegate> delegate;

- (NSInteger)numberOfItems;
- (id)itemAtIndex:(NSInteger)index;
- (BOOL)hasMoreItems;

- (void)refetch;
- (void)fetchMore;

// MUST to override!
@property (nonatomic, readonly) NSInteger pageSize;
- (void)fetchItemsAtPage:(NSInteger)page succeededHandler:(void (^)(NSArray *fetchedItems))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;

@end
