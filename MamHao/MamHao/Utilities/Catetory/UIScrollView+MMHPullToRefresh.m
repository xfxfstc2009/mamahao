//
//  UIScrollView+MMHPullToRefresh.m
//  MamHao
//
//  Created by SmartMin on 15/6/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UIScrollView+MMHPullToRefresh.h"


@implementation UIScrollView (MMHPullToRefresh)
- (void)addMMHPullToRefreshWithActionHandler:(void (^)(void))actionHandler{
    
    [self addPullToRefreshWithActionHandler:actionHandler];
    
    SVPullToRefreshView *refreshView = self.pullToRefreshView;
    CGRect rect = refreshView.frame;
    rect.origin.x = 30.;
    refreshView.frame = rect;
    refreshView.arrowColor = [UIColor colorWithCustomerName:@"红"];
    refreshView.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [refreshView setTitle:@"下拉刷新数据" forState:SVPullToRefreshStateStopped];
    [refreshView setTitle:@"释放进行刷新" forState:SVPullToRefreshStateTriggered];
    [refreshView setTitle:@"正在加载..." forState:SVPullToRefreshStateLoading];
    
//    refreshView.image = [UIImage imageNamed:@"home_icon_refresh"];
}

- (void)addMMHInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler{
    [self addInfiniteScrollingWithActionHandler:actionHandler];
}

@end
