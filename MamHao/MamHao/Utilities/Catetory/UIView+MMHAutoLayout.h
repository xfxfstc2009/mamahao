//
//  UIView+MMHAutoLayout.h
//  MamHao
//
//  Created by SmartMin on 15/4/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MMHAutoLayout)

- (void)autoLayoutWithRect;

@end
