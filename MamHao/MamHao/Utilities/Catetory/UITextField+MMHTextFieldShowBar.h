//
//  UITextField+MMHTextFieldShowBar.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (MMHTextFieldShowBar)

-(void)showBarCallback:(void(^)(UITextField *textField,NSInteger buttonIndex))callBack;
@end
