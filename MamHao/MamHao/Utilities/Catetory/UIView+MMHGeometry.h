//
//  UIView+MMHGeometry.h
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (MMHGeometry)

@property (nonatomic,assign)CGFloat orgin_x;
@property (nonatomic,assign)CGFloat orgin_y;
@property (nonatomic,assign)CGFloat size_width;
@property (nonatomic,assign)CGFloat size_height;
@end
