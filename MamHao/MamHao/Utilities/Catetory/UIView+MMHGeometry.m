//
//  UIView+MMHGeometry.m
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UIView+MMHGeometry.h"

@implementation UIView (MMHGeometry)

-(void)setOrgin_x:(CGFloat)orgin_x{
    self.frame = CGRectMake(orgin_x,self.top , self.width, self.height);
}

-(void)setOrgin_y:(CGFloat)orgin_y{
    self.frame = CGRectMake(self.left, orgin_y, self.width, self.height);
}

-(void)setSize_width:(CGFloat)size_width{
    self.frame = CGRectMake(self.left, self.top, size_width, self.height);
}

-(void)setSize_height:(CGFloat)size_height{
    self.frame = CGRectMake(self.left, self.top, self.width, size_height);
}

-(CGFloat)orgin_x{
    return self.frame.origin.x;
}

-(CGFloat)orgin_y{
    return self.frame.origin.y;
}

-(CGFloat)size_width{
    return self.frame.size.width;
}

-(CGFloat)size_height{
    return self.frame.size.height;
}

@end
