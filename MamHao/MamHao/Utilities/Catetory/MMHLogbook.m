//
//  MMHLogbook.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLogbook.h"
#import "MobClick.h"
#import "MMHLogEntry.h"


#define Umeng           MobClick


@interface MMHLogbook ()

+ (MMHLogbook *)sharedLogbook;

@property (nonatomic, strong) NSMutableSet *activeTimedEvents;

@end


@implementation MMHLogbook


+ (MMHLogbook *)sharedLogbook
{
    static MMHLogbook *sharedLogbook_;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLogbook_ = [[MMHLogbook alloc] init];
    });
    return sharedLogbook_;
}


- (id)init
{
    self = [super init];
    if (self) {
        self.activeTimedEvents = [NSMutableSet set];
    }
    return self;
}


#pragma mark - public apis


+ (void)start
{
#if defined (DEBUG)
    NSString *appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
#else
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
#endif
//    [Flurry setAppVersion:appVersion];
    [Umeng setAppVersion:appVersion];
    
//    [Flurry setCrashReportingEnabled:YES];
    [Umeng setCrashReportEnabled:NO];
//    
//    [Flurry startSession:kYHLogbookAppKeyFlurry withOptions:nil];
    [MobClick startWithAppkey:@"5566ffb267e58e866b0000d1" reportPolicy:BATCH channelId:nil];
}


+ (void)logEvent:(NSString *)eventName
{
    [self logEvent:eventName withParameters:nil timed:NO];
}


+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters
{
    [self logEvent:eventName withParameters:parameters timed:NO];
}

#pragma mark - Ares
+ (void)logEventType:(MMHBuriedPointType)type{
    [self logEvent:[NSString stringWithFormat:@"%li",(long)type]];
}

+ (void)logEventType:(MMHBuriedPointType)type withParameters:(NSDictionary *)parameters{
    [self logEvent:[NSString stringWithFormat:@"%li",(long)type] withParameters:parameters];
}


+ (void)logEvent:(NSString *)eventName timed:(BOOL)timed
{
    [self logEvent:eventName withParameters:nil timed:timed];
}


+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed
{
//    MMHLogEntry *entry = [[MMHLogEntry alloc] initWithName:eventName parameters:parameters];
//    NSString *yohoEventName = [entry nameYOHOLogSystemRepresentation];
//    NSDictionary *yohoParameters = [entry parametersYOHOLogSystemRepresentation];
    
//    [Flurry logEvent:eventName withParameters:parameters timed:timed];
    if (!timed) {
        if (parameters == nil) {
            [Umeng event:eventName];
        }
        else {
            [Umeng event:eventName attributes:parameters];
        }
    }
    else {
        [Umeng beginEvent:eventName primarykey:eventName attributes:parameters];
    }
    
//    if (!timed) {
//        [YHLogSystem logEvent:yohoEventName arguments:yohoParameters];
//    }
//    else {
//        if (![[self sharedLogbook].activeTimedEvents containsObject:entry]) {
//            [[self sharedLogbook].activeTimedEvents addObject:entry];
//        }
//    }
}


+ (void)endTimedEvent:(NSString *)eventName
{
    [self endTimedEvent:eventName withParameters:nil];
}


+ (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters
{
//    MMHLogEntry *entry = [[MMHLogEntry alloc] initWithName:eventName parameters:parameters];
//    NSString *yohoEventName = [entry nameYOHOLogSystemRepresentation];
//    NSDictionary *yohoParameters = [entry parametersYOHOLogSystemRepresentation];
    
//    [Flurry endTimedEvent:eventName withParameters:parameters];
    
    [Umeng endEvent:eventName primarykey:eventName];
    
//    NSSet *beginEntries = [[self sharedLogbook].activeTimedEvents objectsPassingTest:^BOOL(id obj, BOOL *stop) {
//        if ([entry isEqual:obj]) {
//            *stop = YES;
//            return YES;
//        }
//        return NO;
//    }];
//
//    if ([beginEntries count] == 0) {
//        return;
//    }
//
//    MMHLogEntry *beginEntry = [beginEntries anyObject];
//    float timeInterval = (float)[[entry date] timeIntervalSinceDate:[beginEntry date]];
//    NSMutableDictionary *fullParameters = [NSMutableDictionary dictionaryWithDictionary:yohoParameters];
//    [fullParameters setObject:@(timeInterval) forKey:@"DURA"];
//    [YHLogSystem logEvent:yohoEventName arguments:fullParameters];
//    [[self sharedLogbook].activeTimedEvents minusSet:beginEntries];
}


+ (void)beginLogPageView:(NSString *)pageName
{
    [Umeng beginLogPageView:pageName];
}


+ (void)endLogPageView:(NSString *)pageName
{
    [Umeng endLogPageView:pageName];
}


+ (void)setUserID:(NSString *)userID
{
//    [Flurry setUserID:userID];
}


+ (void)setPushNotificationToken:(NSData *)token
{
//    [Flurry setPushToken:[NSString stringWithFormat:@"%@", token]];
}


+ (void)logErrorWithEventName:(NSString *)eventName error:(NSError *)error {
#ifdef DEBUG
    return;
#endif
#ifdef ADHOC
    return;
#endif
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"domain"] = error.domain;
    parameters[@"code"] = @(error.code);
    parameters[@"localizedDescription"] = error.localizedDescription;
    [self logEvent:eventName withParameters:parameters];
}
@end
