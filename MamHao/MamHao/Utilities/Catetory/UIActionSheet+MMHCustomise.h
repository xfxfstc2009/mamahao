//
//  UIActionSheet+MMHCustomise.h
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActionSheet (MMHCustomise)<UIActionSheetDelegate>
+ (instancetype)actionSheetWithTitle:(NSString *)title
                       buttonTitles:(NSArray *)titleArray
                           callback:(void(^)(UIActionSheet *actionSheet,NSInteger buttonIndex))callbackBlock;



@end
