//
//  UIColor+Extended.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extended)
+ (UIColor *)hexChangeFloat:(NSString *)hexColor;
+(UIColor *)colorWithCustomerName:(NSString *)colorName;
@end
