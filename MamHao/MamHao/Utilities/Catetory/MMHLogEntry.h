//
//  MMHLogEntry.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MMHLogEntry : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSDictionary *parameters;
@property (nonatomic, strong) NSDate *date;

- (id)initWithName:(NSString *)name parameters:(NSDictionary *)parameters;

- (NSString *)nameYOHOLogSystemRepresentation;
- (NSDictionary *)parametersYOHOLogSystemRepresentation;

@end
