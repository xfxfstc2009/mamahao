//
//  MMHLogbook.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHBuriedPointManager.h"

@interface MMHLogbook : NSObject


/**
 * Start the logbook session, must be invoked in the #(- application:didFinishLaunchingWithOptions:) method.
 */
+ (void)start;

/**
 * Log an event to the logbook. Or begin a timed event.
 *
 * @param eventName The name of the event, you should use the explicit event name defined in the documentation.
 * @param parameters Ordered parameters/arguments of the event. The order was defined in the documentation.
 * @param keys Ordered key for the parameters/arguments. Flurry uses the keys while YHLogSystem doesn't. Keys use the same order as parameters.
 * @param timed If you want to log a timed event, invoke this method when the event begins and passing YES,
 * then invoke #endTimedEvent:withParameters:keys: when the event ends.
 */
+ (void)logEvent:(NSString *)eventName;
+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;
+ (void)logEvent:(NSString *)eventName timed:(BOOL)timed;
+ (void)logEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters timed:(BOOL)timed;

// ares
+ (void)logEventType:(MMHBuriedPointType)type;
+ (void)logEventType:(MMHBuriedPointType)type withParameters:(NSDictionary *)parameters;

/**
 * End a timed event.
 *
 * @param eventName The name of the event, you should use the explicit event name defined in the documentation.
 * @param parameters Ordered parameters/arguments of the event. The order was defined in the documentation.
 * @param keys Ordered key for the parameters/arguments. Flurry uses the keys while YHLogSystem doesn't. Keys use the same order as parameters.
 */
+ (void)endTimedEvent:(NSString *)eventName;
+ (void)endTimedEvent:(NSString *)eventName withParameters:(NSDictionary *)parameters;	// non-nil parameters will update the parameters

/**
 * Begin/end log a page view.
 * @param pageName The name of the page view.
 */
+ (void)beginLogPageView:(NSString *)pageName;
+ (void)endLogPageView:(NSString *)pageName;

/**
 * Used to identify various users.
 * @param userID A user's id.
 */
+ (void)setUserID:(NSString *)userID;

/**
 * Used to identify various users.
 * @param token Push notification token.
 */
+ (void)setPushNotificationToken:(NSData *)token;


+ (void)logErrorWithEventName:(NSString *)eventName error:(NSError *)error;
@end
