//
//  UIFont+MMHCustomerFont.h
//  MamHao
//
//  Created by SmartMin on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MMHFontType) {
    MMHFontTypeTitle,               /**< 标题*/
    MMHFontTypeSubTitle,            /**< 小标题*/
    MMHFontTypeText,                /**< 正文*/
    MMHFontTypeSubText,             /**< 小正文*/
    MMHFontTypePrompt,              /**< 提示*/
    MMHFontTypeSubPrompt,           /**< 小提示*/
};

@interface UIFont (MMHCustomerFont)

+ (UIFont *)fontWithCustomerSizeName:(NSString *)fontName;

@end
