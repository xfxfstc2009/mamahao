//
//  MMHLogEntry.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLogEntry.h"


@implementation MMHLogEntry


- (id)initWithName:(NSString *)name parameters:(NSDictionary *)parameters
{
    self = [self init];
    if (self) {
        self.name = name;
        self.parameters = [parameters copy];
        self.date = [NSDate date];
    }
    return self;
}


- (NSString *)nameYOHOLogSystemRepresentation
{
#if defined (TARGET_EFASHION)
    NSString *prefix = (kUserInterfaceIdiomIsPad)?@"EPI_":@"EI_";
#elif defined (TARGET_GIRL)
    NSString *prefix = (kUserInterfaceIdiomIsPad)?@"GPI_":@"GI_";
#elif defined (TARGET_E)
    NSString *prefix = @"YI_";
#else
    NSString *prefix = @"";
#endif
    return [prefix stringByAppendingString:[self.name uppercaseString]];
}


- (NSDictionary *)parametersYOHOLogSystemRepresentation
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    for (NSString *key in self.parameters) {
        NSString *uppercaseKey = [key uppercaseString];
        dictionary[uppercaseKey] = self.parameters[key];
    }
    return dictionary;
}


- (BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[self class]]) {
        return NO;
    }
    
    return [self.name isEqualToString:[object name]];
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"YHlogEntry: name = %@, parameters = %@, date = %@", self.name, self.parameters, self.date];
}


@end
