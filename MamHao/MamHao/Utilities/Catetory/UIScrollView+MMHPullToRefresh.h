//
//  UIScrollView+MMHPullToRefresh.h
//  MamHao
//
//  Created by SmartMin on 15/6/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIScrollView+SVPullToRefresh.h"
#import "UIScrollView+SVInfiniteScrolling.h"
@interface UIScrollView (MMHPullToRefresh)
- (void)addMMHPullToRefreshWithActionHandler:(void (^)(void))actionHandler;
- (void)addMMHInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler;

@end
