//
//  UIView+MMHPrompt.h
//  MamHao
//
//  Created by SmartMin on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MMHPromptImageAlignment) {
    MMHPromptImageAlignmentNone,
    MMHPromptImageAlignmentLeft,
    MMHPromptImageAlignmentTop,
};

@interface UIView (MMHPrompt)

- (void)showPrompt:(NSString *)promptString
         withImage:(UIImage *)promptImage
 andImageAlignment:(MMHPromptImageAlignment)imageAlignment;

-(void)dismissPrompt;

@end
