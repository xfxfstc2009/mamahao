//
//  UIFont+MMHCustomerFont.m
//  MamHao
//
//  Created by SmartMin on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UIFont+MMHCustomerFont.h"

@implementation UIFont (MMHCustomerFont)


+ (UIFont *)fontWithCustomerSizeName:(NSString *)fontName{
    CGFloat customerFontSize = 0.0;
    if ([fontName isEqualToString:@"标题"]){
        customerFontSize = 18;
    } else if ([fontName isEqualToString:@"副标题"]){
        customerFontSize = 16.;
    } else if ([fontName isEqualToString:@"正文"]){
        customerFontSize = 15.;
    } else if ([fontName isEqualToString:@"小正文"]){
        customerFontSize = 14.;
    } else if ([fontName isEqualToString:@"提示"]){
        customerFontSize = 13.;
    } else if ([fontName isEqualToString:@"小提示"]){
        customerFontSize = 12.;
    }
    
    if (kScreenWidth == 320) {
        return [UIFont systemFontOfSize:MMHFloat(customerFontSize + 2)];
    }
    return [UIFont systemFontOfSize:MMHFloat(customerFontSize)];
}



@end
