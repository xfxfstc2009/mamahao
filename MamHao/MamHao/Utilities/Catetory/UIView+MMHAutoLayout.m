//
//  UIView+MMHAutoLayout.m
//  MamHao
//
//  Created by SmartMin on 15/4/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UIView+MMHAutoLayout.h"

@implementation UIView (MMHAutoLayout)

- (void)autoLayoutWithRect{
    self.frame = mmh_relative_rect(self.frame);
}

@end
