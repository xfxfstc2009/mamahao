//
//  UITextField+MMHTextFieldShowBar.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UITextField+MMHTextFieldShowBar.h"
#import <objc/runtime.h>
static char cancelCallbackBolckKey;
static char checkCallbackBlockKey;

@implementation UITextField (MMHTextFieldShowBar)

-(void)showBarCallback:(void(^)(UITextField *textField,NSInteger buttonIndex))callBack{
    UIView *toolbarView = [self createInputAccessoryView];

    if (callBack){
        objc_setAssociatedObject(self, &cancelCallbackBolckKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &checkCallbackBlockKey, callBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
    }
    [self setInputAccessoryView:toolbarView];
}

#pragma mark 创建inputAccessoryView
-(UIView *)createInputAccessoryView{
    UIView *toolbarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40)];
    toolbarView.backgroundColor = [UIColor hexChangeFloat:@"F0F1F2"];
    toolbarView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;

    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0, 0, kScreenBounds.size.width , .5f);
    topBorder.backgroundColor = [UIColor colorWithWhite:0.678 alpha:1].CGColor;
    [toolbarView.layer addSublayer:topBorder];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.backgroundColor = [UIColor clearColor];
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelClick) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(0, 0, 80, 40);
    [toolbarView addSubview:cancelButton];
    
    UIButton *checkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    checkButton.backgroundColor = [UIColor clearColor];
    [checkButton setTitle:@"确定" forState:UIControlStateNormal];
    [checkButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
    [checkButton addTarget:self action:@selector(checkClick) forControlEvents:UIControlEventTouchUpInside];
    checkButton.frame = CGRectMake(kScreenBounds.size.width - 80, 0, 80, 40);
    [toolbarView addSubview:checkButton];
    
    return toolbarView;
}


-(void)cancelClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &cancelCallbackBolckKey);
    if (callBack){
        callBack(self,0);
    }
}

-(void)checkClick{
    void (^callBack)(UITextField *textField,NSInteger buttonIndex) = objc_getAssociatedObject(self, &checkCallbackBlockKey);
    if (callBack){
        callBack(self,1);
    }
}

@end
