//
//  MMHTool.m
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHTool.h"

@implementation MMHTool

#pragma mark - check String is Empty
+ (BOOL)isEmpty:(NSString *)string
{
    if (![string isKindOfClass:[NSString class]])
        string = [string description];
    if (string == nil || string == NULL)
        return YES;
    if ([string isKindOfClass:[NSNull class]])
        return YES;
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]==0)
        return YES;
    if ([string isEqualToString:@"(null)"])
        return YES;
    if ([string isEqualToString:@"(null)(null)"])
        return YES;
    if ([string isEqualToString:@"<null>"])
        return YES;
    
    // return Default
    return NO;
}


#pragma mark - 手机号码验证
+ (BOOL)validateMobile:(NSString *)mobile {
    //手机号以13， 15，18开头，八个 \d 数字字符
    NSString *phoneRegex = @"^1((3|5|7|8)\\d)\\d{8}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",phoneRegex];
    return [phoneTest evaluateWithObject:mobile];
}

#pragma mark 密码正则表达式
+(BOOL)validatePassword:(NSString *)password{
    // 密码大小写数字，不能是符号
    NSString *passwordRegex = @"^[A-Za-z0-9]+$";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",passwordRegex];
    return [passwordTest evaluateWithObject:password];
}

#pragma mark - 验证码验证
+ (BOOL)isPureNumandCharacters:(NSString *)string {
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(string.length > 0) {
        return NO;
    }
    return YES;
}

#pragma mark alert
+(void)alertWithMessage:(NSString *)message{
    UIAlertView *failAlertView = [UIAlertView alertViewWithTitle:nil message:message buttonTitles:@[@"确定"] callback:nil];
    [failAlertView show];
}

#pragma mark - 键盘抖动
+(void)lockAnimationForView:(UIView*)view {
    CALayer *lbl = [view layer];
    CGPoint posLbl = [lbl position];
    CGPoint y = CGPointMake(posLbl.x-10, posLbl.y);
    CGPoint x = CGPointMake(posLbl.x+10, posLbl.y);
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setTimingFunction:[CAMediaTimingFunction
                                  functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:x]];
    [animation setToValue:[NSValue valueWithCGPoint:y]];
    [animation setAutoreverses:YES];
    [animation setDuration:0.08];
    [animation setRepeatCount:3];
    [lbl addAnimation:animation forKey:nil];
}

#pragma mark - 拉伸图片
+ (UIImage *)stretchImageWithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];
}

#pragma mark - UITabelViewCell_Clean
+(UIImage *)addBackgroundImageViewWithCellWithDataArray:(NSArray *)dataArray indexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArray = [dataArray objectAtIndex:indexPath.section];
    NSString *backgroundImageName = nil;
    if (sectionOfArray.count == 1){
        backgroundImageName = @"login_frame_single";
    } else if (indexPath.row == 0){
        backgroundImageName = @"login_frame_top";
    } else if (indexPath.row == sectionOfArray.count - 1){
        backgroundImageName = @"login_frame_bottom";
    }
   UIImage *cellBackgroundImage = [MMHTool stretchImageWithName:backgroundImageName];
    return cellBackgroundImage;
}

+(UIImage *)addBackgroundImageViewWithRefundCellWithDataArray:(NSArray *)dataArray indexPath:(NSIndexPath *)indexPath{
    NSArray *sectionOfArray = [dataArray objectAtIndex:indexPath.section];
    NSString *backgroundImageName = nil;
    if (sectionOfArray.count == 1){
        backgroundImageName = @"login_frame_single";
    } else if (indexPath.row == 0){
        backgroundImageName = @"circle_top";
    } else if (indexPath.row == sectionOfArray.count - 1){
        backgroundImageName = @"circle_bottom";
    } else {
        backgroundImageName = @"circle_mid";
    }
    UIImage *cellBackgroundImage = [MMHTool stretchImageWithName:backgroundImageName];
    return cellBackgroundImage;
}


+ (void)animationClickShowWithView:(UIView *)view{
//    view.layer.contents = (id)[UIImage imageNamed:(i%2==0?@"2":@"1")].CGImage;
    CAKeyframeAnimation *k = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    k.values = @[@(0.1),@(1.0),@(1.5)];
    k.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    k.calculationMode = kCAAnimationLinear;
    
    [view.layer addAnimation:k forKey:@"SHOW"];
}

#pragma mark 拨打电话客服
+ (void)callCustomerServer:(UIView *)view phoneNumber:(NSString *)phoneNumber{
    UIActionSheet *sheet = [UIActionSheet actionSheetWithTitle:@"是否拨打电话致客服?" buttonTitles:@[@"取消",@"确定"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0){
            if (phoneNumber.length){
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",phoneNumber]]];
            } else {
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",CustomerServicePhoneNumber]]];
            }
        }
    }];
    [sheet showInView:view];
}

#pragma mark 获取目标时间
+ (NSTimeInterval)getTargetDateTimeWithTimeInterval:(NSTimeInterval)time{
    NSDate *nowDate = [NSDate date];                                // 当前时间
    NSDate *targetDate = [nowDate dateByAddingTimeInterval:time];   // 目标时间
    NSTimeInterval targetDateWithTimeInterval = [targetDate timeIntervalSince1970];
    return targetDateWithTimeInterval;
}


#pragma mark 计算倒计时时间
+ (NSString *)getCountdownWithTargetDate:(NSTimeInterval)targetDateWithTimeInterval{
    NSDate *nowDate = [NSDate date];
    NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970:targetDateWithTimeInterval];
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components: (NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit )
                                                        fromDate:nowDate
                                                          toDate:targetDate
                                                         options:0];
    
    NSString * miniString = [NSString stringWithFormat:@"%ld",(long)[components minute]];
    NSString * secString = [NSString stringWithFormat:@"%ld",(long)[components second]];
    
    return [NSString stringWithFormat:@"%@分%@秒后订单失效",miniString,secString];
}

#pragma mark 统计文字
+ (NSUInteger)calculateCharacterLengthForAres:(NSString *)str {
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < [str length]; i++) {
        asciiLength = str.length;
    }
    return asciiLength;
}

#pragma mark 计算高度
+(CGFloat)contentofHeight:(UIFont *)font{
    CGSize contentOfHeight = [@"计算" sizeWithCalcFont:font constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    return contentOfHeight.height;
}

#pragma mark rangeLabel
+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor normolColor:(UIColor *)normolColor{
    NSMutableArray *hltRangeArr = [NSMutableArray array];
    for (int i = 0;i < hltContentArr.count;i++){
        NSRange range = [content rangeOfString:[hltContentArr objectAtIndex:i]];
        if (range.location != NSNotFound){
            NSValue *rectValue = [NSValue valueWithBytes:&range  objCType:@encode(NSRange)];
            if (rectValue !=nil){
                [hltRangeArr addObject:rectValue];
            }
        }
    }
    NSMutableAttributedString *mutableAttributedString;
    if (content.length){
        mutableAttributedString = [[NSMutableAttributedString alloc]initWithString:content];
    }
    NSMutableArray *rangeTempMutableArr = [NSMutableArray array];
    NSRange zeroRange;
    zeroRange.length = 0;
    zeroRange.location = 0;
    NSValue *zeroRangeValue = [NSValue valueWithBytes:&zeroRange  objCType:@encode(NSRange)];
    [rangeTempMutableArr addObject:zeroRangeValue];
    
    for (int i = 0 ; i < hltRangeArr.count;i++){
        NSRange hltRange = [[hltRangeArr objectAtIndex:i] rangeValue];
        NSRange lastHltRange = [[rangeTempMutableArr lastObject] rangeValue];
        
        // normolRange
        NSRange normolRange;
        normolRange.location = lastHltRange.length + lastHltRange.location;
        normolRange.length = hltRange.location - normolRange.location;
        
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:hltColor range:hltRange];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:normolColor range:normolRange];
        
        NSValue *rectValue = [NSValue valueWithBytes:&hltRange  objCType:@encode(NSRange)];
        [rangeTempMutableArr addObject:rectValue];
    }
    NSRange lastHltRange = [[rangeTempMutableArr lastObject] rangeValue];
    NSRange lastNormolRange;
    lastNormolRange.location = lastHltRange.length + lastHltRange.location;
    lastNormolRange.length = content.length - lastNormolRange.location;
    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:normolColor range:lastNormolRange];
    return  mutableAttributedString;
}




#pragma mark -UserDefault
// 存入
+(void)userDefaulteWithKey:(NSString *)key Obj:(NSString *)objString{
    [USERDEFAULTS synchronize];
    [USERDEFAULTS setObject:objString forKey:key];
}

// 获取
+(NSString *)userDefaultGetWithKey:(NSString *)key{
    NSString *lastDate = [USERDEFAULTS objectForKey:key];
    [USERDEFAULTS synchronize];
    return lastDate;
}
//删除
+(void)userDefaultDelegtaeWithKey:(NSString *)key{
    [USERDEFAULTS removeObjectForKey:key];
    [USERDEFAULTS synchronize];
}
#pragma mark - 号码裁剪
+(NSString *)numberCutting:(NSString *)number {
    NSRange range  = NSMakeRange (0,3);
//    NSRange range1 = NSMakeRange(3,4);
    NSRange range2 = NSMakeRange(7,4);
    
    number=[NSString stringWithFormat:@"%@%@%@",[number substringWithRange:range],@"****",[number substringWithRange:range2]];
    return number;
}

@end
