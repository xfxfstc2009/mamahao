//
//  Assistant.swift
//  MamHao
//
//  Created by Louis Zhu on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


public extension Dictionary {
    internal func stringForKey(key: Key) -> String? {
        if let object = self[key] {
            if object is NSNull {
                return nil
            }
            if object is String {
                return object as? String
            }
            return String(format: "\(object)")
        }
        else {
            return nil
        }
    }
    
    internal func nonnullStringForKey(key: Key) -> String {
        if let string = self.stringForKey(key) {
            return string
        }
        else {
            return ""
        }
    }
    
    internal func intForKey(key: Key) -> Int? {
        if let object = self[key] {
            if object is NSNull {
                return nil
            }
            if object is String {
                return (object as! String).toInt()
            }
            if object is NSNumber {
                return (object as! NSNumber).longValue
            }
        }
        return nil
    }
    
    internal func nonnullIntForKey(key: Key) -> Int {
        if let intValue = self.intForKey(key) {
            return intValue
        }
        else {
            return 0
        }
    }
    
    internal func priceForKey(key: Key) -> MMHPrice? {
        if let object = self[key] {
            if object is NSNull {
                return nil
            }
            if object is String {
                return (object as! String).priceValue()
            }
            if object is NSNumber {
                return (object as! NSNumber).priceValue()
            }
        }
        return nil
    }
    
    internal func nonnullPriceForKey(key: Key) -> MMHPrice {
        let price = self.priceForKey(key)
        if price == nil {
            return 0
        }
        else {
            return price!
        }
    }
}


public extension UIViewController {
    public func showAlertViewWithTitle(title: String!, message: String!, cancelButtonTitle: String! = "确定", cancelButtonHandler: dispatch_block_t? = nil) {
        if NSClassFromString("UIAlertController") == nil {
            LESAlertView.showWithTitle(title, message: message, cancelButtonTitle: cancelButtonTitle, cancelButtonHandler: cancelButtonHandler)
        }
        else {
            var alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
            let cancelAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel, handler: { (action: UIAlertAction!) -> Void in
                cancelButtonHandler?()
            })
            alertController.addAction(cancelAction)
            if self.presentedViewController != nil {
                self.presentedViewController!.showAlertViewWithTitle(title, message: message, cancelButtonTitle: cancelButtonTitle, cancelButtonHandler: cancelButtonHandler)
            }
            else {
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    public func showAlertViewWithError(error: NSError!, cancelButtonHandler: dispatch_block_t! = {}) {
        self.showAlertViewWithTitle("", message: error.displayDescription(), cancelButtonTitle: "确定", cancelButtonHandler: cancelButtonHandler)
    }
}
