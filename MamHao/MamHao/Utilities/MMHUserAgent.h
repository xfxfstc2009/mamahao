//
//  MMHUserAgent.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MMHUserAgent : NSObject

+ (void)configureGlobalUserAgent;

@end
