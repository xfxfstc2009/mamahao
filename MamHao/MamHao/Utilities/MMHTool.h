//
//  MMHTool.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Reachability.h"

@interface MMHTool : NSObject

+ (BOOL)isEmpty:(NSString *)string;


// 验证
+ (BOOL)validateMobile:(NSString *)mobile;
+ (BOOL)isPureNumandCharacters:(NSString *)string;
+ (BOOL)validatePassword:(NSString *)password;

+(void)alertWithMessage:(NSString *)message;

+(void)lockAnimationForView:(UIView*)view;              // 键盘抖动

+ (UIImage *)stretchImageWithName:(NSString *)name;     // 拉伸图片

+(UIImage *)addBackgroundImageViewWithCellWithDataArray:(NSArray *)array indexPath:(NSIndexPath *)indexPath;    // 返回imageViewBackGround
+(UIImage *)addBackgroundImageViewWithRefundCellWithDataArray:(NSArray *)dataArray indexPath:(NSIndexPath *)indexPath;

#pragma mark 抖动
+ (void)animationClickShowWithView:(UIView *)view;

#pragma mark - 拨打客服电话
+ (void)callCustomerServer:(UIView *)view phoneNumber:(NSString *)phoneNumber;

#pragma mark - 获取目标时间
+ (NSTimeInterval)getTargetDateTimeWithTimeInterval:(NSTimeInterval)time;

#pragma mark - 计算倒计时时间
+ (NSString *)getCountdownWithTargetDate:(NSTimeInterval)targetDateWithTimeInterval;

#pragma mark - 统计文字
+ (NSUInteger)calculateCharacterLengthForAres:(NSString *)str;

#pragma mark - 计算高度
+(CGFloat)contentofHeight:(UIFont *)font;

#pragma mark - rangeLabel
+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor normolColor:(UIColor *)normolColor;

#pragma mark - userDefault
+(void)userDefaulteWithKey:(NSString *)key Obj:(NSString *)objString;
+(NSString *)userDefaultGetWithKey:(NSString *)key;
+(void)userDefaultDelegtaeWithKey:(NSString *)key;


#pragma mark - 号码裁剪
+(NSString *)numberCutting:(NSString *)number;

/**
 *  判断当前的网络状态
 */

@end
