//
//  MMHAppearance.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAppearance.h"


UIFont *F0;
UIFont *F1;
UIFont *F2;
UIFont *F3;
UIFont *F4;
UIFont *F5;
UIFont *F6;
UIFont *F7;
UIFont *F8;

UIFont *MMHF0;
UIFont *MMHF1;
UIFont *MMHF2;
UIFont *MMHF3;
UIFont *MMHF4;
UIFont *MMHF5;
UIFont *MMHF6;
UIFont *MMHF7;
UIFont *MMHF8;


@implementation UIFont (MMHBoldFont)

- (UIFont *)boldFont
{
    CGFloat pointSize = [self pointSize];
    return [UIFont boldSystemFontOfSize:pointSize];
}

@end


UIColor *C0;
UIColor *C1;
UIColor *C2;
UIColor *C3;
UIColor *C4;
UIColor *C5;
UIColor *C6;
UIColor *C7;

UIColor *C20;
UIColor *C21;
UIColor *C22;
UIColor *C23;


@implementation UIView (MMHCornerRadius)

- (void)setCornerRadius1
{
    self.layer.cornerRadius = 3.0f;
}

- (void)setCornerRadius2
{
    self.layer.cornerRadius = 5.0f;
}

@end


@implementation MMHAppearance


+ (void)configureGlobalAppearancesWithWindow:(UIWindow *)window
{
    F0 = [UIFont systemFontOfSize:10.0f];
    F1 = [UIFont systemFontOfSize:11.0f];
    F2 = [UIFont systemFontOfSize:12.0f];
    F3 = [UIFont systemFontOfSize:13.0f];
    F4 = [UIFont systemFontOfSize:14.0f];
    F5 = [UIFont systemFontOfSize:15.0f];
    F6 = [UIFont systemFontOfSize:16.0f];
    F7 = [UIFont systemFontOfSize:17.0f];
    F8 = [UIFont systemFontOfSize:18.0f];
    
    MMHF0 = [UIFont systemFontOfCustomeSize:10.0f];
    MMHF1 = [UIFont systemFontOfCustomeSize:11.0f];
    MMHF2 = [UIFont systemFontOfCustomeSize:12.0f];
    MMHF3 = [UIFont systemFontOfCustomeSize:13.0f];
    MMHF4 = [UIFont systemFontOfCustomeSize:14.0f];
    MMHF5 = [UIFont systemFontOfCustomeSize:15.0f];
    MMHF6 = [UIFont systemFontOfCustomeSize:16.0f];
    MMHF7 = [UIFont systemFontOfCustomeSize:17.0f];
    MMHF8 = [UIFont systemFontOfCustomeSize:18.0f];
    
    C0 = [UIColor colorWithHexString:@"fafafa"];
    C1 = [UIColor colorWithHexString:@"f0f0f0"];
    C2 = [UIColor colorWithHexString:@"dcdcdc"];
    C3 = [UIColor colorWithHexString:@"cccccc"];
    C4 = [UIColor colorWithHexString:@"999999"];
    C5 = [UIColor colorWithHexString:@"666666"];
    C6 = [UIColor colorWithHexString:@"333333"];
    C7 = [UIColor colorWithHexString:@"000000"];
    C20 = [UIColor colorWithHexString:@"fc687c"];
    C21 = [UIColor colorWithHexString:@"ff4d61"];
    C22 = [UIColor colorWithHexString:@"477ed8"];
    C23 = [UIColor colorWithHexString:@"5cccccc"];
    
    [[UINavigationBar appearance] setTintColor:[self pinkColor]];
    [[UIBarButtonItem appearance] setTintColor:[self pinkColor]];

    window.tintColor = [MMHAppearance pinkColor];
    
    [[MMHTitleLabel appearance] setFont:[self titleFont]];
    [[MMHTitleLabel appearance] setTextColor:[self blackColor]];
    
    [[MMHSubtitleLabel appearance] setFont:[self subtitleFont]];
    [[MMHSubtitleLabel appearance] setTextColor:[self blackColor]];
    
    [[MMHTextLabel appearance] setFont:[self textFont]];
    [[MMHTextLabel appearance] setTextColor:[self blackColor]];
    
    [[MMHSmallTextLabel appearance] setFont:[self smallTextFont]];
    [[MMHSmallTextLabel appearance] setTextColor:[self grayColor]];
    
    [[MMHTipsLabel appearance] setFont:[self tipsFont]];
    [[MMHTipsLabel appearance] setTextColor:[self lightGrayColor]];
    
    [[MMHSmallTipsLabel appearance] setFont:[self smallTipsFont]];
    [[MMHSmallTipsLabel appearance] setTextColor:[self lightGrayColor]];
}


@end


@implementation MMHAppearance (UIColorExtensions)


+ (UIColor *)blackColor
{
    return [UIColor colorWithHexString:@"333333"];
}


+ (UIColor *)grayColor;
{
    return [UIColor colorWithHexString:@"666666"];
}


+ (UIColor *)lightGrayColor;
{
    return [UIColor colorWithHexString:@"999999"];
}


+ (UIColor *)whiteGrayColor;
{
    return [UIColor colorWithHexString:@"b8b8b8"];
}


+ (UIColor *)redColor;
{
    return [UIColor colorWithHexString:@"ff4d61"];
}


+ (UIColor *)whiteColor;
{
    return [UIColor colorWithHexString:@"ffffff"];
}


+ (UIColor *)blueColor;
{
    return [UIColor colorWithHexString:@"477ed8"];
}


+ (UIColor *)pinkColor;
{
    return [UIColor colorWithHexString:@"fc687c"];
}


+ (UIColor *)disableColor;
{
    return [UIColor colorWithHexString:@"bfbebe"];
}


+ (UIColor *)separatorColor;
{
    return [UIColor colorWithHexString:@"dcdcdc"];
}


+ (UIColor *)backgroundColor;
{
    return [UIColor colorWithHexString:@"f0f0f0"];
}


@end


@implementation MMHAppearance (UIFontExtensions)


+ (UIFont *)titleFont
{
    return [UIFont systemFontOfSize:18.0f];
}


+ (UIFont *)subtitleFont;
{
    return [UIFont systemFontOfSize:16.0f];
}


+ (UIFont *)textFont;
{
    return [UIFont systemFontOfSize:15.0f];
}


+ (UIFont *)smallTextFont;
{
    return [UIFont systemFontOfSize:14.0f];
}


+ (UIFont *)tipsFont;
{
    return [UIFont systemFontOfSize:13.0f];
}


+ (UIFont *)smallTipsFont;
{
    return [UIFont systemFontOfSize:12.0f];
}


@end


@implementation MMHTitleLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance titleFont];
        self.textColor = [MMHAppearance blackColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 18.0f;
}


@end


@implementation MMHSubtitleLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance subtitleFont];
        self.textColor = [MMHAppearance blackColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 16.0f;
}


@end


@implementation MMHTextLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance textFont];
        self.textColor = [MMHAppearance blackColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 15.0f;
}


@end


@implementation MMHSmallTextLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance smallTextFont];
        self.textColor = [MMHAppearance grayColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 14.0f;
}


@end


@implementation MMHTipsLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance tipsFont];
        self.textColor = [MMHAppearance lightGrayColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 13.0f;
}


@end


@implementation MMHSmallTipsLabel


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.font = [MMHAppearance smallTipsFont];
        self.textColor = [MMHAppearance lightGrayColor];
    }
    return self;
}


+ (CGFloat)defaultHeight
{
    return 12.0f;
}


@end
