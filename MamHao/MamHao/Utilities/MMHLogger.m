//
//  MMHLogger.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLogger.h"
#import "LoggerClient.h"


NSString * const MMHLoggerTagGeneral = @"general";
NSString * const MMHLoggerTagTesting = @"testing";
NSString * const MMHLoggerTagNetwork = @"network";
NSString * const MMHLoggerTagFileOperation = @"file operation";
NSString * const MMHLoggerTagTableView = @"table view";
NSString * const MMHLoggerTagViewController = @"view controller";


//static NSString * const MMHLoggerTagNetwork = @"network";


void MMHLog(NSString *tag, MMHLoggerLevel level, NSString *format, va_list args)
{
    LogMessageF_va(__FILE__, __LINE__, __FUNCTION__, tag, level, format, args);
    
#if 0
    va_list args;
    va_start(args, format);
    NSString *ppp = [@"Louis: " stringByAppendingFormat:format, args];
    if (developer == nil) {
        //        message = [@"" stringByAppendingFormat:format, args];
        [ppp writeToFile:[kPathDocument stringByAppendingPathComponent:@"a.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
        //        LogMessageToF_va(NULL, __FILE__, __LINE__, __FUNCTION__, tag, level, format, args);
    }
    else {
        //        message = [[developer stringByAppendingString:@": "] stringByAppendingFormat:format, args];
        //        NSString *message = [@"Lousiiii: " stringByAppendingFormat:format, args];
        //        [message writeToFile:[kPathDocument stringByAppendingPathComponent:@"a.txt"] atomically:YES encoding:NSUTF8StringEncoding error:nil];
        //        LogMessageToF_va(NULL, __FILE__, __LINE__, __FUNCTION__, tag, level, [@"%%@: " stringByAppendingString:format], developer, args);
        //        LogMessageF(__FILE__, __LINE__, __FUNCTION__, tag, level, @"%@", message);
    }
    LogMessageF(__FILE__, __LINE__, __FUNCTION__, tag, level, @"%@", ppp);
    va_end(args);
#endif
}


void MMHLogDoom(NSString *tag, NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    MMHLog(tag, MMHLoggerLevelDoom, format, args);
    va_end(args);
}


void MMHLogError(NSString *tag, NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    MMHLog(tag, MMHLoggerLevelError, format, args);
    va_end(args);
}


void MMHLogWarning(NSString *tag, NSString *format, ...)
{
    va_list args;
    va_start(args, format);
    MMHLog(tag, MMHLoggerLevelWarning, format, args);
    va_end(args);
}


void LLZZLog(NSString *tag, NSString *format, ...)
{
#if defined (DEBUG_LOUIS) || defined (ADHOC)
    va_list args;
    va_start(args, format);
    NSString *message = [[NSString alloc] initWithFormat:format arguments:args];
    LogMessageF(__FILE__, __LINE__, __FUNCTION__, tag, MMHLoggerLevelInformation, @"Louis: %@", message);
    va_end(args);
#endif
}


void LLZZLogTesting(NSString *format, ...)
{
#if defined (DEBUG_LOUIS) || defined (ADHOC)
    va_list args;
    va_start(args, format);
    MMHLog(MMHLoggerTagTesting, MMHLoggerLevelTesting, format, args);
    va_end(args);
#endif
}

