//
//  MMHPullDownToRefresh.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJRefreshHeader.h"


extern NSString *const MMHPullToRefreshHeaderViewStateIdleText;
extern NSString *const MMHPullToRefreshHeaderViewStatePullingText;
extern NSString *const MMHPullToRefreshHeaderViewStateRefreshingText;


@interface MMHPullDownToRefreshHeaderView: MJRefreshHeader

@end


@interface UIScrollView (MMHPullDownToRefresh)

- (void)addPullDownToRefreshHeaderWithRefreshingBlock:(void (^)())block;
- (void)addPullDownToRefreshHeaderWithRefreshingTarget:(id)target refreshingAction:(SEL)action;

@end


@interface MMHPullDownToRefresh : NSObject

@end
