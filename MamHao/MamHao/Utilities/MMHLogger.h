//
//  MMHLogger.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoggerClient.h"


typedef NS_ENUM (NSInteger, MMHLoggerLevel) {
    MMHLoggerLevelDoom,          // very very extremely fatal accident that will cause app crashes immediately
    MMHLoggerLevelError,         // critical errors
    MMHLoggerLevelWarning,       // something goes wrong, may bring on some problems later
    MMHLoggerLevelInformation,   // for developers to print their trival informations
    MMHLoggerLevelTesting,       // just for test, remove these logs immediately after they have became useless
};


extern NSString * const MMHLoggerTagGeneral;
extern NSString * const MMHLoggerTagTesting;
extern NSString * const MMHLoggerTagNetwork;
extern NSString * const MMHLoggerTagFileOperation;
extern NSString * const MMHLoggerTagTableView;
extern NSString * const MMHLoggerTagViewController;

extern void MMHLog(NSString *tag, MMHLoggerLevel level, NSString *format, va_list args);
extern void MMHLogDoom(NSString *tag, NSString *format, ...);
extern void MMHLogError(NSString *tag, NSString *format, ...);
extern void MMHLogWarning(NSString *tag, NSString *format, ...);

extern void LLZZLog(NSString *tag, NSString *format, ...);
extern void LLZZLogTesting(NSString *format, ...);


#if defined (DEBUG_LOUIS) || defined (ADHOC)
  #define LZLog(format, ...) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(format), ##__VA_ARGS__] ); \
    LogMessageF(__FILE__, __LINE__, __FUNCTION__, @"Louis", MMHLoggerLevelInformation, format, ##__VA_ARGS__)
#else
  #define LZLog(format, ...)
#endif


#if defined (DEBUG_ARES) || defined (ADHOC)
  #define ARESLog(format, ...) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(format), ##__VA_ARGS__] ); \
    LogMessageF(__FILE__, __LINE__, __FUNCTION__, @"Ares", MMHLoggerLevelInformation, format, ##__VA_ARGS__)
#else
  #define ARESLog(format, ...)
#endif


#if defined (DEBUG_FISH) || defined (ADHOC)
  #define FISHLog(format, ...) NSLog( @"<%@:(%d)> %@", [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(format), ##__VA_ARGS__] ); \
    LogMessageF(__FILE__, __LINE__, __FUNCTION__, @"Fish", MMHLoggerLevelInformation, format, ##__VA_ARGS__)
#else
  #define FISHLog(format, ...)
#endif


#define LOG_NETWORK(level, ...)    LogMessageF(__FILE__,__LINE__,__FUNCTION__,@"network",level,__VA_ARGS__)