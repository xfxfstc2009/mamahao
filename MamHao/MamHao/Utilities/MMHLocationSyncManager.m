//
//  MMHLocationSyncManager.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLocationSyncManager.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+CommonSync.h"


@import CoreLocation;


@interface MMHLocationSyncManager ()

@property (nonatomic, strong) NSDate *lastSyncedDate;
@end


@implementation MMHLocationSyncManager


+ (MMHLocationSyncManager *)sharedLocationSyncManager
{
    static MMHLocationSyncManager *_manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _manager = [[MMHLocationSyncManager alloc] init];
    });
    return _manager;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(gpsLocationChanged:)
                                                     name:MMHCurrentLocationNotification
                                                   object:nil];
    }
    return self;
}


- (void)gpsLocationChanged:(NSNotification *)notification
{
    if (self.lastSyncedDate) {
        if ([self.lastSyncedDate timeIntervalSinceNow] > -3600.0) {
            return;
        }
    }

    CLLocation *location = [MMHCurrentLocationModel sharedLocation].gpsLocation;
    NSString *areaID = [MMHCurrentLocationModel sharedLocation].gpsAreaID;
    [[MMHNetworkAdapter sharedAdapter] syncGPSLocation:location areaID:areaID succeededHandler:^{
        self.lastSyncedDate = [NSDate date];
    } failedHandler:^(NSError *error) {
    }];
}


@end
