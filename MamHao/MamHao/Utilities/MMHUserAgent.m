//
//  MMHUserAgent.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUserAgent.h"
#include <sys/sysctl.h>


@implementation MMHUserAgent


+ (void)configureGlobalUserAgent
{
    NSString *userAgentString = [self defaultUserAgentString];
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent": userAgentString}];
}


+ (NSString *)defaultUserAgentString
{
    return [NSString stringWithFormat:@"%@,%@,%@,%@", [self deviceModel], [self screenResolution], [self operatingSystemNameAndVersion], [self applicationNameAndVersion]];
}


+ (NSString *)devicePlatform
{
    int mib[2];
    size_t len;
    char *machine;

    mib[0] = CTL_HW;
    mib[1] = HW_MACHINE;
    sysctl(mib, 2, NULL, &len, NULL, 0);
    machine = malloc(len);
    sysctl(mib, 2, machine, &len, NULL, 0);

    NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
    free(machine);
    return platform;
}


+ (NSString *)deviceModel
{
    NSString *platform = [self devicePlatform];

    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone(GSM)";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G(GSM)";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS(GSM)";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4(GSM)";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"iPhone 4(CDMA)";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S(GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5(GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5(GSM+CDMA)";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod touch 1st generation";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod touch 2nd generation";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod touch 3rd generation";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod touch 4th generation";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod touch 5th generation";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2(WIFI)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2(GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2(CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3rd generation(WIFI)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3rd generation(GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3rd generation(GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4th generation(WIFI)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4th generation(GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4th generation(GSM+CDMA)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad mini(WIFI)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad mini(GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad mini(GSM+CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";

    return platform;
}


+ (NSString *)screenResolution
{
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGFloat scale = [[UIScreen mainScreen] scale];
    return [NSString stringWithFormat:@"%ldx%ld",(long)(rect.size.width * scale), (long)(rect.size.height * scale)];
}


+ (NSString *)operatingSystemNameAndVersion
{
    return [NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]];
}


+ (NSString *)applicationNameAndVersion
{
    NSDictionary *appNameDictionary = @{@"com.mamhao.mamhao" : @"Mamhao"};
    NSString *appName = appNameDictionary[[[NSBundle mainBundle] infoDictionary][(NSString *) kCFBundleIdentifierKey]];
    if (appName == nil) {
        NSString *bundleName = [[NSBundle mainBundle] infoDictionary][(NSString *) kCFBundleNameKey];
        appName = bundleName;
    }
    return [NSString stringWithFormat:@"%@ %@", appName, [[NSBundle mainBundle] infoDictionary][(NSString *) kCFBundleVersionKey]];
}


@end
