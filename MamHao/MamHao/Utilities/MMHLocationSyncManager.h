//
//  MMHLocationSyncManager.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMHLocationSyncManager : NSObject

+ (MMHLocationSyncManager *)sharedLocationSyncManager;

@end
