//
//  MMHFilterTermData.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@class MMHFilterTerm;
@class MMHCategory;
@class FilterTermInfo;
@class FilterTerm;
@class MMHFilter;


typedef NS_ENUM(NSInteger, MMHFilterTermType) {
    MMHFilterTermTypeNormal = 0, // with no images
    MMHFilterTermTypeBrand = 1, // with images
    MMHFilterTermTypeCategory = 2, // single selection
//    MMHFilterTermTypeAge = 2, // with images, but age
};



@interface MMHFilterTermData : NSObject

+ (MMHFilterTermData *)sharedData;

- (void)fetchTermsForFilter:(MMHFilter *)filter succeededHandler:(void(^)(FilterTermInfo *termInfo))succeededHandler failedHandler:(void(^)(NSError *error))failedHandler;
- (void)saveTermInfo:(FilterTermInfo *)termInfo forCategory:(MMHCategory *)category;
- (BOOL)isTermsDataAvailableForFilter:(MMHFilter *)filter;

- (NSInteger)numberOfTermClassesForFilter:(MMHFilter *)filter;
- (NSInteger)numberOfTermsAtSection:(NSInteger)section forFilter:(MMHFilter *)filter;
- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section forFilter:(MMHFilter *)filter;
- (NSString *)titleForSection:(NSInteger)section filter:(MMHFilter *)filter;
- (MMHFilterTermType)termTypeAtSection:(NSInteger)section forFilter:(MMHFilter *)filter;

- (BOOL)multiSelectionEnabledAtSection:(NSInteger)section forFilter:(MMHFilter *)filter;

- (void)saveTermInfo:(FilterTermInfo *)termInfo forFilter:(MMHFilter *)filter;
@end


@interface MMHFilterTermData (shop)

//- (void)fetchTermsForShopID:(NSString *)shopID succeededHandler:(void(^)(FilterTermInfo *termInfo))succeededHandler failedHandler:(void(^)(NSError *error))failedHandler;
- (void)saveTermInfo:(FilterTermInfo *)termInfo forShopID:(NSString *)shopID;

- (NSInteger)numberOfTermClassesForShopID:(NSString *)shopID;
- (NSInteger)numberOfTermsAtSection:(NSInteger)section forShopID:(NSString *)shopID;
- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section forShopID:(NSString *)shopID;
- (NSString *)titleForSection:(NSInteger)section shopID:(NSString *)shopID;
- (MMHFilterTermType)termTypeAtSection:(NSInteger)section forShopID:(NSString *)shopID;
@end
