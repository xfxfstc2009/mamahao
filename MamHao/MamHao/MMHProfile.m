//
//  MMHProfile.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProfile.h"
#import "MMHChild.h"
#import "MMHNetworkAdapter+Product.h"


@implementation MMHProfile


- (id)initWithUserIdentity:(MMHUserIdentity)identity childDictionary:(NSDictionary *)childDictionary {
    self = [self init];
    if (self) {
        self.userIdentity = identity;
        if (identity == MMHUserIdentityMom) {
            if (![childDictionary isKindOfClass:[NSNull class]]) {
                if (childDictionary.count != 0) {
                    MMHChild *child = [[MMHChild alloc] initWithDictionary:childDictionary];
                    self.children = @[child];
                }
            }
        }
    }
    return self;
}


- (BOOL)hasAnyChild {
    return (self.children.count != 0);
}
@end
