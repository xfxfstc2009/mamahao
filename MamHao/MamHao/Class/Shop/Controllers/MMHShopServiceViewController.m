//
//  MMHShopServiceViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopServiceViewController.h"
#import "MMHShopGlanceView.h"
#import "MMHShopDetail.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Shop.h"
#import "LESAlertView.h"


@interface MMHShopServiceViewController ()

@property (nonatomic, strong) MMHShopDetail *shopDetail;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) MMHShopGlanceView *glanceView;
@property (nonatomic, strong) MMHShopService *shopService;
@property (nonatomic) CGFloat openHourAndPhoneSeparatorLinePosition;
@end


@implementation MMHShopServiceViewController


- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail
{
    self = [self init];
    if (self) {
        self.shopDetail = shopDetail;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView *contentScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    contentScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    contentScrollView.backgroundColor = [MMHAppearance backgroundColor];
    [self.view addSubview:contentScrollView];
    self.contentScrollView = contentScrollView;

    MMHShopGlanceView *glanceView = [[MMHShopGlanceView alloc] initWithShopDetail:self.shopDetail];
    [self.contentScrollView addSubview:glanceView];
    self.glanceView = glanceView;
    
    [self fetchData];
}


- (void)fetchData
{
    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchShopServiceWithShopDetail:self.shopDetail
                                                                 from:self
                                                     succeededHandler:^(MMHShopService *shopService) {
                                                         [weakSelf.view hideProcessingView];
                                                         self.shopService = shopService;
                                                         [weakSelf configureViews];
                                                     } failedHandler:^(NSError *error) {
                [weakSelf.view hideProcessingView];
                [weakSelf.view showTipsWithError:error];
            }];
}


- (void)configureViews
{
    // 0. introduction view
    UIView *introductionView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.glanceView.bottom, self.view.bounds.size.width, 0.0f)];
    introductionView.backgroundColor = [UIColor whiteColor];
    [self.contentScrollView addSubview:introductionView];

    UIImageView *introductionIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_store_notice"]];
    [introductionIndicatorImageView setOrigin:CGPointMake(17.0f, 17.0f)];
    [introductionView addSubview:introductionIndicatorImageView];

    UILabel *introductionLabel = [[UILabel alloc] initWithFrame:CGRectMake(introductionIndicatorImageView.right + 12.0f, 15.0f, 0.0f, 0.0f)];
    [introductionLabel setMaxX:introductionView.bounds.size.width - 17.0f];
    introductionLabel.font = [MMHAppearance smallTextFont];
    introductionLabel.textColor = [MMHAppearance blackColor];
    [introductionLabel setText:self.shopService.introduction constrainedToLineCount:0];
    [introductionView addSubview:introductionLabel];

    [introductionView setHeight:introductionLabel.bottom + 15.0f];
    [introductionView addBottomSeparatorLine];

    // 1. open hour and phone view
    UIView *openHourAndPhoneView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, CGRectGetMaxY(introductionView.frame) + 8.0f, self.contentScrollView.bounds.size.width, 0.0f)];
    openHourAndPhoneView.backgroundColor = [UIColor whiteColor];
    [self.contentScrollView addSubview:openHourAndPhoneView];

    self.openHourAndPhoneSeparatorLinePosition = MMHFloat(205.0f);

    UILabel *openHourLabel = [[UILabel alloc] initWithFrame:CGRectMake(17.0f, 15.0f, self.openHourAndPhoneSeparatorLinePosition - 17.0f - 17.0f, 0.0f)];
    openHourLabel.font = [MMHAppearance smallTextFont];
    openHourLabel.textColor = [MMHAppearance blackColor];
    NSString *openHourString = [NSString stringWithFormat:@"营业时间：\n周中%ld:%02ld——%ld:%02ld\n周末%ld:%02ld——%ld:%02ld", (long)self.shopService.openFromDateComponents.hour, (long)self.shopService.openFromDateComponents.minute, (long)self.shopService.openUntilDateComponents.hour, (long)self.shopService.openUntilDateComponents.minute, (long)self.shopService.openFromDateComponents.hour, (long)self.shopService.openFromDateComponents.minute, (long)self.shopService.openUntilDateComponents.hour, (long)self.shopService.openUntilDateComponents.minute];
    [openHourLabel setText:openHourString constrainedToLineCount:0];
    [openHourAndPhoneView addSubview:openHourLabel];
    [openHourAndPhoneView setHeight:openHourLabel.bottom + 15.0f];
    [openHourAndPhoneView addTopSeparatorLine];
    [openHourAndPhoneView addBottomSeparatorLine];

    UIView *openHourAndPhoneSeparatorLine = [[UIView alloc] initWithFrame:CGRectMake(self.openHourAndPhoneSeparatorLinePosition, 17.0f, 1.0f / mmh_screen_scale(), openHourAndPhoneView.bounds.size.height - 17.0f - 17.0f)];
    openHourAndPhoneSeparatorLine.backgroundColor = [MMHAppearance separatorColor];
    [openHourAndPhoneView addSubview:openHourAndPhoneSeparatorLine];

    UIImageView *phoneIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_store_call"]];
    phoneIndicatorImageView.left = self.openHourAndPhoneSeparatorLinePosition + 18.0f;
    phoneIndicatorImageView.centerY = openHourAndPhoneSeparatorLine.centerY;
    [openHourAndPhoneView addSubview:phoneIndicatorImageView];

    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(phoneIndicatorImageView.right + 13.0f, 0.0f, openHourAndPhoneView.bounds.size.width - phoneIndicatorImageView.right - 13.0f - 17.0f, openHourAndPhoneView.bounds.size.height)];
    phoneLabel.font = [MMHAppearance smallTextFont];
    phoneLabel.textColor = [MMHAppearance blackColor];
    phoneLabel.lineBreakMode = NSLineBreakByWordWrapping;
    phoneLabel.numberOfLines = 0;
    phoneLabel.text = self.shopService.phone;
    [openHourAndPhoneView addSubview:phoneLabel];

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openHourAndPhoneViewTapped:)];
    openHourAndPhoneView.userInteractionEnabled = YES;
    [openHourAndPhoneView addGestureRecognizer:tapGestureRecognizer];

    // 2. service views
    CGFloat height = openHourAndPhoneView.bottom + 8.0f;
    for (NSNumber *serviceNumber in self.shopService.services) {
        NSInteger serviceTag = [serviceNumber integerValue];
        UIView *serviceView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, height, self.contentScrollView.bounds.size.width - 20.0f, 58.0f)];
        serviceView.backgroundColor = [UIColor whiteColor];
        [self.contentScrollView addSubview:serviceView];

        UIImageView *serviceTagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(17.0f, 17.0f, 24.0f, 24.0f)];
        serviceTagImageView.backgroundColor = [UIColor orangeColor];
        [serviceView addSubview:serviceTagImageView];

        UILabel *serviceTagTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(serviceTagImageView.right + 17.0f, 10.0f, serviceView.bounds.size.width - serviceTagImageView.right - 17.0f - 17.0f, 0.0f)];
        serviceTagTitleLabel.font = [MMHAppearance tipsFont];
        serviceTagTitleLabel.textColor = [MMHAppearance blackColor];
        [serviceTagTitleLabel setText:@"门店配送" constrainedToLineCount:1];
        [serviceView addSubview:serviceTagTitleLabel];

        MMHTipsLabel *serviceTagIntroductionLabel = [[MMHTipsLabel alloc] initWithFrame:CGRectMake(serviceTagTitleLabel.left, serviceTagTitleLabel.bottom + 4.0f, serviceTagTitleLabel.frame.size.width, 0.0f)];
        [serviceTagIntroductionLabel setText:@"7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货7天无理由退货" constrainedToLineCount:0];
        [serviceView addSubview:serviceTagIntroductionLabel];

        [serviceView setHeight:MAX(serviceTagIntroductionLabel.bottom + 10.0f, 58.0f)];
        height = serviceView.bottom + 8.0f;
    }

    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.bounds.size.width, height);
}


- (void)openHourAndPhoneViewTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    CGPoint location = [tapGestureRecognizer locationInView:tapGestureRecognizer.view];
    if (location.x >= self.openHourAndPhoneSeparatorLinePosition) {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", self.shopService.phone]];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            LESAlertView *alertView = [[LESAlertView alloc] initWithTitle:@""
                                message:[NSString stringWithFormat:@"拨打 %@", self.shopService.phone]
                      cancelButtonTitle:@"取消"
                    cancelButtonHandler:^{

                    }];
            [alertView addButtonWithTitle:@"确定" handler:^{
                [[UIApplication sharedApplication] openURL:url];
            }];
            [alertView show];
        }
        else {
            [LESAlertView showWithTitle:nil
                                message:@"您当前的设备不能拨打电话"
                      cancelButtonTitle:@"确定"
                    cancelButtonHandler:nil];
        }
    }
}


@end
