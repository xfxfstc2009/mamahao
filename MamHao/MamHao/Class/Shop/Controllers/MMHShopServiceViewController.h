//
//  MMHShopServiceViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHShopDetail;
@class MMHShopGlanceView;
@class MMHShopService;


@interface MMHShopServiceViewController : AbstractViewController

- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail;

- (void)configureViews;
@end
