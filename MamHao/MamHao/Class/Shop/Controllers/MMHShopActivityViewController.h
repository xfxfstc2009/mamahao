//
//  MMHShopActivityViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHShopDetail;


@interface MMHShopActivityViewController : AbstractViewController

@property (nonatomic, strong) MMHShopDetail *shopDetail;

- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail;
@end
