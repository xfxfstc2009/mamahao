//
//  MMHShopListViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopListViewController.h"
#import "MMHShopDetailViewController.h"
#import "MMHShopInfo.h"


@interface MMHShopListViewController ()

@end


@implementation MMHShopListViewController


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"实体店";
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"mmhtab_icon_stores_hlt"];
        self.tabBarItem.image = [UIImage imageNamed:@"mmhtab_icon_stores_nor"];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        MMHShopInfo *shopInfo = [[MMHShopInfo alloc] init];
        MMHShopDetailViewController *shopDetailViewController = [[MMHShopDetailViewController alloc] initWithShopInfo:shopInfo];
        [self pushViewController:shopDetailViewController];
    });
}


@end
