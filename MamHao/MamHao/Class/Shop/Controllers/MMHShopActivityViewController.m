//
//  MMHShopActivityViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopActivityViewController.h"
#import "MMHShopDetail.h"
#import "MMHShopGlanceView.h"
#import "MMHShopActivity.h"


@interface MMHShopActivityViewController ()

@end


@implementation MMHShopActivityViewController


- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail
{
    self = [self init];
    if (self) {
        self.shopDetail = shopDetail;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configureViews];
}


- (void)configureViews
{
    MMHShopGlanceView *glanceView = [[MMHShopGlanceView alloc] initWithShopDetail:self.shopDetail];
    [self.view addSubview:glanceView];

    CGFloat y = glanceView.bottom + 8.0f;

    for (MMHShopActivity *activity in self.shopDetail.activities) {
        UIView *activityView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, y, self.view.bounds.size.width, 0.0f)];
        activityView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:activityView];

        CGFloat padding = 18.0f;
//        CGFloat activityViewY = padding;

        UIView *headerDecorationView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, padding, 3.0f, 0.0f)];
        headerDecorationView.backgroundColor = [MMHAppearance pinkColor];
        [activityView addSubview:headerDecorationView];

        CGFloat labelLeft = headerDecorationView.right + 7.0f;
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelLeft, headerDecorationView.top, 0.0f, 0.0f)];
        [titleLabel setMaxX:CGRectGetMaxX(self.view.bounds)];
        titleLabel.font = [MMHAppearance textFont];
        titleLabel.textColor = [UIColor blackColor];
        titleLabel.numberOfLines = 0;
        [titleLabel setText:activity.activityName constrainedToLineCount:0];
        [activityView addSubview:titleLabel];

        headerDecorationView.height = titleLabel.height;

        UILabel *periodTipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelLeft, titleLabel.bottom + 22.0f, titleLabel.width, 0.0f)];
        periodTipsLabel.font = [MMHAppearance smallTextFont];
        periodTipsLabel.textColor = [MMHAppearance pinkColor];
        [periodTipsLabel setText:@"活动时间" constrainedToLineCount:0];
        [activityView addSubview:periodTipsLabel];

//        UILabel *periodLabel = [[UILabel alloc] initWithFrame:CGRectMake(labelLeft, <#(CGFloat)y#>, <#(CGFloat)width#>, <#(CGFloat)height#>)]

#if 0
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(self.contentView.bounds), 0.0f, 0.0f, self.contentView.bounds.size.height)];
        [dateLabel setMaxX:CGRectGetMaxX(self.contentView.bounds) - 10.0f];
        dateLabel.font = [MMHAppearance smallTipsFont];
        dateLabel.textColor = [UIColor blackColor];
        dateLabel.textAlignment = NSTextAlignmentRight;
        [dateLabel setBorderColor:[UIColor colorWithHexString:@"dcdcdc"] cornerRadius:2.0f];
        NSString *dateString = [NSString stringWithFormat:@"活动时间 %d.%d-%d.%d日", activity.startDateComponents.month, activity.startDateComponents.day, activity.endDateComponents.month, activity.endDateComponents.day];
        [dateLabel setSingleLineText:dateString constrainedToWidth:CGFLOAT_MAX withEdgeInsets:UIEdgeInsetsMake(2.0f, 6.0f, 2.0f, 6.0f)];
        dateLabel.textAlignment = NSTextAlignmentCenter;
        dateLabel.centerY = CGRectGetMidY(self.contentView.bounds);
        [self.contentView addSubview:dateLabel];
        self.dateLabel = dateLabel;
    }
#endif
    }
}


@end
