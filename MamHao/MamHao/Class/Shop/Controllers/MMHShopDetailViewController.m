//
//  MMHShopDetailViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailViewController.h"
#import "MMHShopInfo.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Shop.h"
#import "MMHShopDetail.h"
#import "MMHShopGlanceView.h"
#import "MMHShopDetailProductCell.h"
#import "MMHShopDetailProductSectionHeaderView.h"
#import "MMHShopDetailShopInfoCell.h"
#import "MMHShopServiceViewController.h"
#import "MMHShopActivity.h"
#import "MMHShopActivityViewController.h"
#import "MMHShopDetailShopRatingCell.h"
#import "MMHShopDetailShopCommentsCell.h"
#import "MMHShopDetailShopShippingInfoCell.h"
#import "MMHShopDetailShopActivitiesCell.h"
#import "MMHShopPraiseViewController.h"
#import "MMHFilter.h"
#import "MMHLocationNavigationManager.h"
#import "MJRefresh.h"
#import "MMHFilterTermSelectionViewController.h"
#import "MMHProductListViewController.h"
#import "MMHProductDetailViewController1.h"
#import "MMHTabBarController.h"
#import "MBProgressHUD.h"

typedef NS_ENUM(NSInteger, MMHShopDetailCollectionViewSection) {
    MMHShopDetailCollectionViewSectionShopInfo = 0,

};


typedef NS_ENUM(NSInteger, MMHShopDetailShopInfoRow) {
    MMHShopDetailShopInfoRowShopInfo = 0,
    MMHShopDetailShopInfoRowShopRating = 1,
    MMHShopDetailShopInfoRowShopComment = 2,
    MMHShopDetailShopInfoRowShopShippingInfo = 3,
    MMHShopDetailShopInfoRowShopActivities = 4,
};


NSString * const MMHShopDetailShopInfoCellIdentifier = @"MMHShopDetailShopInfoCellIdentifier";
NSString * const MMHShopDetailShopRatingCellIdentifier = @"MMHShopDetailShopRatingCellIdentifier";
NSString * const MMHShopDetailShopCommentsCellIdentifier = @"MMHShopDetailShopCommentsCellIdentifier";
NSString * const MMHShopDetailShopShippingInfoCellIdentifier = @"MMHShopDetailShopShippingInfoCellIdentifier";
NSString * const MMHShopDetailShopActivitiesCellIdentifier = @"MMHShopDetailShopActivitiesCellIdentifier";
NSString * const MMHShopDetailProductSectionHeaderIdentifier = @"MMHShopDetailProductSectionHeaderIdentifier";
NSString * const MMHShopDetailProductCellIdentifier = @"MMHShopDetailProductCellIdentifier";

#define PAGESIZE 5
@interface MMHShopDetailViewController () <UICollectionViewDataSource, UICollectionViewDelegate, MMHShopDetailShopInfoCellDelegate, MMHShopDetailShopShippingInfoCellDelegate, MMHShopDetailProductSectionHeaderViewDelegate>

@property (nonatomic, strong) MMHShopInfo *shopInfo;
@property (nonatomic, strong) MMHShopDetail *shopDetail;
@property (nonatomic, strong) MMHShopGlanceView *shopGlanceView;
@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSString *shopId;
@end


@implementation MMHShopDetailViewController


- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        self.dataSource = [NSMutableArray array];
    }
    return _dataSource;
}


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;
    [self collectionView];
}


- (id)initWithShopId:(NSString *)shopId
{
    if (self = [super init]) {
        self.shopId = shopId;
    }
    // TODO: - Louis -
    return self;
}


- (instancetype)initWithShopInfo:(MMHShopInfo *)shopInfo
{
    self = [self init];
    if (self) {
        self.shopInfo = shopInfo;
    }
    return self;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.headerReferenceSize = CGSizeMake(self.view.bounds.size.width, 150.0f + 75.0f + (8.0f + 55.0f) * (CGFloat)(self.shopDetail.activities.count));
        layout.minimumLineSpacing = 0.0f;
        
        UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds
                                                              collectionViewLayout:layout];
        collectionView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 10.0f, 0.0f);
        collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        collectionView.dataSource = self;
        collectionView.delegate = self;
        collectionView.alwaysBounceVertical = YES;
        collectionView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
        [collectionView registerClass:[MMHShopDetailShopInfoCell class] forCellWithReuseIdentifier:MMHShopDetailShopInfoCellIdentifier];
        [collectionView registerClass:[MMHShopDetailShopRatingCell class] forCellWithReuseIdentifier:MMHShopDetailShopRatingCellIdentifier];
        [collectionView registerClass:[MMHShopDetailShopCommentsCell class] forCellWithReuseIdentifier:MMHShopDetailShopCommentsCellIdentifier];
        [collectionView registerClass:[MMHShopDetailShopShippingInfoCell class] forCellWithReuseIdentifier:MMHShopDetailShopShippingInfoCellIdentifier];
        [collectionView registerClass:[MMHShopDetailShopActivitiesCell class] forCellWithReuseIdentifier:MMHShopDetailShopActivitiesCellIdentifier];
        [collectionView registerClass:[MMHShopDetailProductSectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MMHShopDetailProductSectionHeaderIdentifier];
        [collectionView registerClass:[MMHShopDetailProductCell class] forCellWithReuseIdentifier:MMHShopDetailProductCellIdentifier];
        [self.view addSubview:collectionView];
        self.collectionView = collectionView;
       
        
    }
    return _collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    UIBarButtonItem *filterItem = [[UIBarButtonItem alloc] initWithTitle:@"筛选"
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self
                                                                  action:@selector(filterProducts:)];
    filterItem.tintColor = [UIColor colorWithHexString:@"666666"];
    self.navigationItem.rightBarButtonItem = filterItem;
    [self fetchData];
  
}

- (void)viewWillAppear:(BOOL)animated{
     __weak typeof(self)weakSelf = self;
    [self.collectionView addLegendFooterWithRefreshingBlock:^{
        if (!weakSelf) {
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf fetchData];
    }];

}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.collectionView.footer endRefreshing];
    [self.collectionView  removeFooter];
}


- (void)fetchData
{
    [self.collectionView showProcessingView];
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchShopDetailWithShopId:self.shopId memberId:1 start:self.dataSource.count end:self.dataSource.count+PAGESIZE from:nil succeededHandler:^(MMHShopDetail *shopDetail) {
        if (!weakSelf) {
            return ;
        }
         __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.collectionView hideProcessingView];
        strongSelf.shopDetail = shopDetail;
        [strongSelf.dataSource addObjectsFromArray:strongSelf.shopDetail.groupItems];
        if (strongSelf.shopDetail.onlineShopName.length) {
            strongSelf.barMainTitle = shopDetail.onlineShopName;
        }else{
            strongSelf.barMainTitle = shopDetail.shopName;
        }
        if (self.dataSource.count == 0) {
            [self.collectionView.footer endRefreshing];
            [self.collectionView removeFooter];
            [self.collectionView showTips:@"该门店商品正在上架中，敬请期待!"] ;
        }else{
            if (shopDetail.groupItems.count<PAGESIZE) {
                [self.collectionView.footer noticeNoMoreData];
            }
        }
        [strongSelf.collectionView reloadData];
      
    } failedHandler:^(NSError *error) {
        [self.collectionView hideProcessingView];
        [self.collectionView showTipsWithError:error];
    }];
}


#pragma mark - UICollectionView data source and delegate


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.dataSource.count+1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 0) {
        return 5;
    }else{
       
        MMHShopGroupItemModel *itemModel = self.dataSource[section-1];
        return [itemModel.products count];
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)indexPath.section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            switch (indexPath.row) {
                case MMHShopDetailShopInfoRowShopInfo: {
                    MMHShopDetailShopInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailShopInfoCellIdentifier forIndexPath:indexPath];
                    cell.shopDetail = self.shopDetail;
                    cell.delegate = self;
                    return cell;
                }
                case MMHShopDetailShopInfoRowShopRating: {
                    MMHShopDetailShopRatingCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailShopRatingCellIdentifier forIndexPath:indexPath];
                    cell.shopDetail = self.shopDetail;
                    return cell;
                }
                case MMHShopDetailShopInfoRowShopComment: {
                    MMHShopDetailShopCommentsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailShopCommentsCellIdentifier forIndexPath:indexPath];
                    cell.shopDetail = self.shopDetail;
                    return cell;
                }
                case MMHShopDetailShopInfoRowShopShippingInfo: {
                    MMHShopDetailShopShippingInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailShopShippingInfoCellIdentifier forIndexPath:indexPath];
                    cell.shopDetail = self.shopDetail;
                    cell.delegate = self;
                    return cell;
                }
                case MMHShopDetailShopInfoRowShopActivities: {
                    MMHShopDetailShopActivitiesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailShopActivitiesCellIdentifier forIndexPath:indexPath];
                    cell.shopDetail = self.shopDetail;
                    return cell;
                }
                default: {
                    return [[UICollectionViewCell alloc] initWithFrame:CGRectZero];
                }
            }
        }

        default:{
            MMHShopDetailProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHShopDetailProductCellIdentifier forIndexPath:indexPath];
            MMHShopGroupItemModel *itemModel = self.dataSource[indexPath.section-1];
            cell.product = itemModel.products[indexPath.row];
            return cell;
        }
    }
    return [[UICollectionViewCell alloc] initWithFrame:CGRectZero];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            return CGSizeZero;
        }
            default:{
            return CGSizeMake(mmh_screen_width(), 45.0f);
        }

            break;
    }
    return CGSizeZero;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (![kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return nil;
    }

    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)indexPath.section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            return [[UICollectionReusableView alloc] init];
        }

        default:{
            MMHShopDetailProductSectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                                   withReuseIdentifier:MMHShopDetailProductSectionHeaderIdentifier
                                                                                                          forIndexPath:indexPath];
            MMHShopGroupItemModel *model = self.dataSource[indexPath.section - 1];
            [headerView setTitle:model.groupName forSection:s];
            headerView.delegate = self;
            return headerView;
            
        }

    }
    return [[UICollectionReusableView alloc] init];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)indexPath.section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            switch (indexPath.row) {
                case MMHShopDetailShopInfoRowShopInfo: {
                    CGFloat height = [MMHShopDetailShopInfoCell heightForShopDetail:self.shopDetail];
                    return CGSizeMake(mmh_screen_width(), height);
                }
                case MMHShopDetailShopInfoRowShopRating: {
                    CGFloat height = [MMHShopDetailShopRatingCell heightForShopDetail:self.shopDetail];
                    return CGSizeMake(mmh_screen_width(), height);
                }
                case MMHShopDetailShopInfoRowShopComment: {
                    if (self.shopDetail.commentCount == 0) {
                        return CGSizeZero;
                    }
                    CGFloat height = [MMHShopDetailShopCommentsCell heightForShopDetail:self.shopDetail];
                    return CGSizeMake(mmh_screen_width(), height);
                }
                case MMHShopDetailShopInfoRowShopShippingInfo: {
                    CGFloat height = [MMHShopDetailShopShippingInfoCell heightForShopDetail:self.shopDetail];
                    return CGSizeMake(mmh_screen_width(), height);
                }
                case MMHShopDetailShopInfoRowShopActivities: {
                    CGFloat height = [MMHShopDetailShopActivitiesCell heightForShopDetail:self.shopDetail];
                    return CGSizeMake(mmh_screen_width(), height);
                }
                default: {
                    return CGSizeZero;
                }
            }

        }

        default:{
            return [MMHShopDetailProductCell defaultSize];
        }
    }
    return CGSizeZero;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            return UIEdgeInsetsZero;
        }

        default:{

            return UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 10.0f);
        }
    }
    return UIEdgeInsetsZero;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            return 0.0f;
        }

        default:{

            return 8.0f;
        }

    }
    return 0.0f;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            return 0.0f;
        }
        default:{
            return 10.0f;
        }

    }
    return 0.0f;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MMHShopDetailCollectionViewSection s = (MMHShopDetailCollectionViewSection)indexPath.section;
    switch (s) {
        case MMHShopDetailCollectionViewSectionShopInfo: {
            switch (indexPath.row) {
                case MMHShopDetailShopInfoRowShopInfo: {
                    NSLog(@"shopInfo");
                    break;
                }
                case MMHShopDetailShopInfoRowShopRating: {
                    NSLog(@"shopRating");
                    break;
                }
                case MMHShopDetailShopInfoRowShopComment: {
                    NSLog(@"门店口碑");
                    [MMHLogbook logEventType:MMHBuriedPointTypeShopPraise];
                    MMHShopPraiseViewController *shopPraiseViewController = [[MMHShopPraiseViewController alloc] init];
                    shopPraiseViewController.shopId = self.shopDetail.shopId;
                    [self.navigationController pushViewController:shopPraiseViewController animated:YES];
                    break;
                }
                case MMHShopDetailShopInfoRowShopShippingInfo: {
                    NSLog(@"shippinginfo");
                    break;
                }
                case MMHShopDetailShopInfoRowShopActivities: {
                    NSLog(@"活动");
                    break;
                }
                default: {
                    
                }
            }
            
        }
            break;
        default:{
        //TODO: -点击section(setion!=0)的item 调转商品详情
            if (self.dataSource.count==0) {
    
                
            }else{
                MMHShopGroupItemModel *itemModel = self.dataSource[indexPath.section-1];
                MMHShopProduct1 *product = itemModel.products[indexPath.row];
                MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
                [self.navigationController pushViewController:productDetailViewController animated:YES];
            }
        }
    }
    // TODO: - Louis -
    #if 0
//    LZLog(@"-- select: %ld, %d", (long)indexPath.section, indexPath.row);
    NSInteger selectedRow = [self.filter selectedTermIndexAtSection:indexPath.section];
    [collectionView deselectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:indexPath.section] animated:NO];
    for (NSIndexPath *selectedIndexPath in [collectionView indexPathsForSelectedItems]) {
        if (selectedIndexPath.section == indexPath.section) {
            if (selectedIndexPath.row != indexPath.row) {
                [collectionView deselectItemAtIndexPath:selectedIndexPath animated:NO];
            }
        }
    }
    [self.filter setSelectedTermIndex:indexPath.row atSection:indexPath.section];
    #endif
}


#pragma mark - MMHShopDetailShopInfoCellDelegate


- (void)shopDetailAddressViewServiceAreaTapped:(MMHShopDetailAddressView *)addressView
{
    MMHShopServiceViewController *shopServiceViewController = [[MMHShopServiceViewController alloc] initWithShopDetail:self.shopDetail];
    [self pushViewController:shopServiceViewController];
}


- (void)shopDetailShopInfoCell:(MMHShopDetailShopInfoCell *)shopDetailShopInfoCell didSelectActivity:(MMHShopActivity *)activity
{
    __weak MMHShopDetailViewController *weakViewController = self;
    MMHLocationNavigationManager *locationNavigationManager = [[MMHLocationNavigationManager alloc]init];
    [locationNavigationManager showActionSheetWithView:weakViewController.view shopName:self.barMainTitle shopLocationLat:self.shopDetail.latitude shopLocationLon:self.shopDetail.longitude];


}


#pragma mark - MMHShopDetailShopShippingInfoCell delegate


- (void)shopShippingInfoCellHeightDidChange:(MMHShopDetailShopShippingInfoCell *)shopShippingInfoCell
{
    [self.collectionView reloadData];
}


- (void)filterProducts:(UIBarButtonItem *)barButtonItem
{
    [MMHLogbook logEventType:MMHBuriedPointTypeShopScreening];
    MMHFilter *filter = [MMHFilter filterWithShopID:self.shopDetail.shopId];
    MMHProductListViewController *productListViewController = [[MMHProductListViewController alloc] initWithFilter:filter];
    MMHFilterTermSelectionViewController *filterTermSelectionViewController = [[MMHFilterTermSelectionViewController alloc] initWithFilter:filter];
    filterTermSelectionViewController.shouldPopToViewControllerOfClassIfPoppedWithoutAnyFiltering = [self class];
    [self pushViewControllers:@[productListViewController, filterTermSelectionViewController]];
}

#pragma mark - <MMHShopDetailProductSectionHeaderViewDelegate>


- (void)shopDetailProductSectionHeaderView:(MMHShopDetailProductSectionHeaderView *)shopDetailProductSectionHeaderView didSelectedAtSection:(NSInteger)section
{
    MMHShopGroupItemModel *group = [self.shopDetail.groupItems nullableObjectAtIndex:section - 1];
    MMHFilter *filter = [MMHFilter filterWithShopID:self.shopDetail.shopId groupID:[NSString stringWithFormat:@"%ld", (long)group.groupId]];
    MMHProductListViewController *productListViewController = [[MMHProductListViewController alloc] initWithFilter:filter];
    productListViewController.title = group.groupName;
    [self pushViewController:productListViewController];
}

@end
