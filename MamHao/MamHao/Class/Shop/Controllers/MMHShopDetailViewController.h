//
//  MMHShopDetailViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHShopInfo;
@class MMHShopDetail;
@class MMHShopGlanceView;


@interface MMHShopDetailViewController : AbstractViewController

- (instancetype)initWithShopInfo:(MMHShopInfo *)shopInfo;

- (id)initWithShopId:(NSString *)shopId;


@end
