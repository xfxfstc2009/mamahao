//
//  MMHShopGlanceView.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopDetail;


@interface MMHShopGlanceView : UIView

@property (nonatomic, strong) MMHShopDetail *shopDetail;

- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail;

- (void)updateViews;
@end
