//
//  MMHShopDetailAddressView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailAddressView.h"
#import "MMHShopDetail.h"


@interface MMHShopDetailAddressView ()

@property (nonatomic, strong) UIImageView *locationIndicatorImageView;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) MMHTipsLabel *addressLabel;
@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UIImageView *serviceIndicatorImageView;
@property (nonatomic, strong) UILabel *serviceLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicatorImageView;
@end


@implementation MMHShopDetailAddressView


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    CGPoint location = [tapGestureRecognizer locationInView:self];
    if (location.x >= self.separatorLine.centerX) {
        id <MMHShopDetailAddressViewDelegate> delegate = self.delegate;
        if (delegate) {
            if ([delegate respondsToSelector:@selector(shopDetailAddressViewServiceAreaTapped:)]) {
                [delegate shopDetailAddressViewServiceAreaTapped:self];
            }
        }
    }
}


- (void)updateViews
{
    if (self.locationIndicatorImageView == nil) {
        UIImageView *locationIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_store_locate"]];
        [locationIndicatorImageView setOrigin:CGPointMake(16.0f, 18.0f)];
        [self addSubview:locationIndicatorImageView];
        self.locationIndicatorImageView = locationIndicatorImageView;
    }
    
    if (self.distanceLabel == nil) {
        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.locationIndicatorImageView.frame) + 12.0f, 0.0f, 0.0f, 0.0f)];
        distanceLabel.textColor = [UIColor blackColor];
        distanceLabel.font = [MMHAppearance smallTextFont];
        [self addSubview:distanceLabel];
        self.distanceLabel = distanceLabel;
    }
    [self.distanceLabel setSingleLineText:@"距离您100mblabla" constrainedToWidth:CGFLOAT_MAX];
    self.distanceLabel.centerY = self.locationIndicatorImageView.centerY;
    
    if (self.addressLabel == nil) {
        MMHTipsLabel *addressLabel = [[MMHTipsLabel alloc] initWithFrame:CGRectMake(10.0f, 40.0f, 0.0f, 0.0f)];
        [addressLabel setMaxX:mmh_screen_width() - 140.0f];
        [self addSubview:addressLabel];
        self.addressLabel = addressLabel;
    }
    [self.addressLabel setText:@"杭州市 上城区 延安路银泰3楼blabla" constrainedToLineCount:1];
    
    if (self.separatorLine == nil) {
        UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(mmh_screen_width() - 120.0f, 0.0f, 0.5f, 44.0f)];
        separatorLine.centerY = CGRectGetMidY(self.bounds);
        separatorLine.backgroundColor = [UIColor colorWithHexString:@"dcdcdc"];
        [self addSubview:separatorLine];
        self.separatorLine = separatorLine;
    }
    
    if (self.serviceIndicatorImageView == nil) {
        UIImageView *serviceIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_store_store"]];
        serviceIndicatorImageView.top = 17.0f;
        serviceIndicatorImageView.centerX = mmh_screen_width() - 62.0f;
        [self addSubview:serviceIndicatorImageView];
        self.serviceIndicatorImageView = serviceIndicatorImageView;
    }
    
    if (self.serviceLabel == nil) {
        UILabel *serviceLabel = [[MMHTipsLabel alloc] initWithFrame:CGRectMake(0.0f, self.serviceIndicatorImageView.bottom + 5.0f, 0.0f, 0.0f)];
        serviceLabel.centerX = self.serviceIndicatorImageView.centerX;
        serviceLabel.textColor = [UIColor blackColor];
        serviceLabel.textAlignment = NSTextAlignmentCenter;
        [serviceLabel setSingleLineText:@"门店服务" constrainedToWidth:CGFLOAT_MAX];
        [self addSubview:serviceLabel];
        self.serviceLabel = serviceLabel;
    }
    
    if (self.disclosureIndicatorImageView == nil) {
        UIImageView *disclosureIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_go_midgray"]];
        disclosureIndicatorImageView.right = mmh_screen_width() - 10.0f;
        disclosureIndicatorImageView.centerY = CGRectGetMidY(self.bounds);
        [self addSubview:disclosureIndicatorImageView];
        self.disclosureIndicatorImageView = disclosureIndicatorImageView;
    }

    [self addBottomSeparatorLine];
}


@end
