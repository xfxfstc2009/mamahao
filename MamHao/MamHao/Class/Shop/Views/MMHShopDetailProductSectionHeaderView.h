//
//  MMHShopDetailProductSectionHeaderView.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopDetailProductSectionHeaderView;


@protocol MMHShopDetailProductSectionHeaderViewDelegate <NSObject>

//- (void)collectionReusableView:(MMHShopDetailProductSectionHeaderView *)collectionReusableView didSelectedLabel:(UILabel *)label ;
- (void)shopDetailProductSectionHeaderView:(MMHShopDetailProductSectionHeaderView *)shopDetailProductSectionHeaderView didSelectedAtSection:(NSInteger)section;
@end


@interface MMHShopDetailProductSectionHeaderView : UICollectionReusableView
@property (nonatomic, assign) id<MMHShopDetailProductSectionHeaderViewDelegate> delegate;
- (void)setTitle:(NSString *)title forSection:(NSInteger)section;
@end

