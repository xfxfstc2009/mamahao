//
//  MMHShopDetailShopShippingInfoCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailShopShippingInfoCell.h"
#import "MMHShopDetail.h"
#import "MMHExpandableView.h"
#import "UIView+Extension.h"


static CGFloat height_ = 30.0f;


@interface MMHShopDetailShopShippingInfoCell () <MMHExpandableViewDelegate>

@property (nonatomic, strong) MMHExpandableView *expandableView;
@end


@implementation MMHShopDetailShopShippingInfoCell


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addTopSeparatorLine];
        [self.contentView addBottomSeparatorLine];
    }
    return self;
}


- (void)updateViews
{
    self.contentView.backgroundColor = [MMHAppearance backgroundColor];
   
    if (self.shopDetail.shippingRange == nil) {
        self.shopDetail.shippingRange = @"";
    }
    NSString *text = [NSString stringWithFormat:@"配送区域：%@", self.shopDetail.shippingRange];
    if (self.shopDetail.shippingRange) {
        if (self.expandableView == nil) {
            MMHExpandableView *expandableView = [[MMHExpandableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 30.0f)
                                                                             summaryText:text
                                                                                fullText:text];
            expandableView.contentInsets = UIEdgeInsetsMake(6.0f, 10.0f, 6.0f, 10.0f);
            expandableView.delegate = self;
            expandableView.textColor = C5;
            expandableView.font = F4;
            [self.contentView addSubview:expandableView];
            self.expandableView = expandableView;
        }

    }
   }


+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail
{
    return height_;
}


- (void)expandableViewHeightDidChange:(MMHExpandableView *)expandableView
{
    height_ = expandableView.height;

    id<MMHShopDetailShopShippingInfoCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(shopShippingInfoCellHeightDidChange:)]) {
        [delegate shopShippingInfoCellHeightDidChange:self];
    }
}


@end
