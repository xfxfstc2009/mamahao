//
//  MMHShopDetailAddressView.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopDetail;
@class MMHShopDetailAddressView;


@protocol MMHShopDetailAddressViewDelegate <NSObject>

- (void)shopDetailAddressViewServiceAreaTapped:(MMHShopDetailAddressView *)addressView;

@end


@interface MMHShopDetailAddressView : UIView

@property (nonatomic, weak) id<MMHShopDetailAddressViewDelegate> delegate;

@property (nonatomic, strong) MMHShopDetail *shopDetail;
@end
