//
//  MMHShopDetailShopShippingInfoCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopDetail;
@class MMHExpandableView;
@class MMHShopDetailShopShippingInfoCell;


@protocol MMHShopDetailShopShippingInfoCellDelegate <NSObject>

- (void)shopShippingInfoCellHeightDidChange:(MMHShopDetailShopShippingInfoCell *)shopShippingInfoCell;

@end



@interface MMHShopDetailShopShippingInfoCell : UICollectionViewCell

@property (nonatomic, weak) id<MMHShopDetailShopShippingInfoCellDelegate> delegate;

@property (nonatomic, strong) MMHShopDetail *shopDetail;

+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail;
@end
