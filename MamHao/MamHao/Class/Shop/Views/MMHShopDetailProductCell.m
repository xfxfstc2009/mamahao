//
//  MMHShopDetailProductCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MMHShopDetailProductCell.h"
#import "MMHShopProduct.h"
#import "MMHImageView.h"


@interface MMHShopDetailProductCell ()

@property (nonatomic, strong) MMHImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *currentPriceLabel;
@property (nonatomic, strong) UILabel *originalPriceLabel;
@end


@implementation MMHShopDetailProductCell


- (void)setProduct:(MMHShopProduct1 *)product
{
    _product = product;

    CGSize size = [[self class] defaultSize];
    CGFloat borderInset = 10.0f;
    if (self.imageView == nil) {
        MMHImageView *imageView = [[MMHImageView alloc] initWithFrame:CGRectMake(borderInset, borderInset, size.width - borderInset - borderInset, size.width - borderInset - borderInset)]; // Louis: assume that the image view is square of shape
        [self.contentView addSubview:imageView];
        self.imageView = imageView;
    }
//    [self.imageView sd_setImageWithURL:[NSURL URLWithString:product.imageURLString]];
    [self.imageView updateViewWithImageAtURL:product.goodsPic];
    
    if (self.titleLabel == nil) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(borderInset, 176.0f, size.width - borderInset - borderInset, 0.0f)];
        titleLabel.font = [MMHAppearance smallTextFont];
        titleLabel.textColor = [UIColor blackColor];
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;
    }
    [self.titleLabel setText:product.itemName constrainedToLineCount:2];
    
    if (self.currentPriceLabel == nil) {
        UILabel *currentPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(borderInset, 0.0f, 0.0f, 0.0f)];
        currentPriceLabel.font = [MMHAppearance textFont];
        currentPriceLabel.textColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:currentPriceLabel];
        self.currentPriceLabel = currentPriceLabel;
    }
    NSString *currentPriceString = [NSString stringWithPrice:[self getPriceWithProduct:product]];

    [self.currentPriceLabel setSingleLineText:currentPriceString constrainedToWidth:CGFLOAT_MAX];
    [self.currentPriceLabel moveToBottom:size.height - borderInset];
//    if (self.originalPriceLabel == nil) {
//        UILabel *originalPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, 0.0f)];
//        originalPriceLabel.font = [MMHAppearance smallTipsFont];
//        originalPriceLabel.textColor = [MMHAppearance whiteGrayColor];
//        [self.contentView addSubview:originalPriceLabel];
//        self.originalPriceLabel = originalPriceLabel;
//    }
//    NSString *originalPriceString = [NSString stringWithPrice:product.customPrice];
//    [self.originalPriceLabel setSingleLineText:originalPriceString constrainedToWidth:CGFLOAT_MAX];
//    self.originalPriceLabel.left = self.currentPriceLabel.right + 6.0f;
//    [self.originalPriceLabel moveToBottom:size.height - borderInset];
    
}
- (NSInteger)getPriceWithProduct:(MMHShopProduct1 *)shopProduct1{
    if (shopProduct1.proPrice == 0) {
        if (shopProduct1.customPrice == 0) {
            return shopProduct1.retailPrice;
        }else{
            return shopProduct1.customPrice;
        }
    }else{
        return shopProduct1.proPrice;
    }
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
    }
    return self;
}

- (void)setCellstate:(CellState)cellstate {
    _cellstate = cellstate;
    if (self.stateIcon == nil) {
        UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - MMHFloat(7) - MMHFloat(18), MMHFloat(5), MMHFloat(18), MMHFloat(18))];
        [self.contentView addSubview:iconView];
        self.stateIcon = iconView;
    }
  
    if (cellstate == CellStateNormal) {
        self.stateIcon.image = [UIImage imageNamed:@""];
    }else if(cellstate == CellStateDesSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_gray"];
    }else if(cellstate == CellStateDidSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_s"];
        
    }

}

+ (CGSize)defaultSize
{
    CGFloat width = mmh_screen_width() - 8.0f - 20.0f;
    width *= 0.5;
    return CGSizeMake(width, 240.0f);
}


@end
