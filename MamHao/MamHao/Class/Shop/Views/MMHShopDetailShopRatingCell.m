//
//  MMHShopDetailShopRatingCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailShopRatingCell.h"
#import "MMHShopDetail.h"
#import "UIView+Extension.h"


@interface MMHShopRatingView: UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic) double score;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *imageView;
@end


@implementation MMHShopRatingView


- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title score:(CGFloat)score level:(CGFloat)level
{
    CGFloat verticalPadding = 4.0f;
    self = [self initWithFrame:frame];
    if (self) {
        self.title = title;
        self.score = score;

        self.backgroundColor = [UIColor clearColor];
    
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        titleLabel.textColor = C6;
        titleLabel.font = F2;
        [titleLabel setSingleLineText:title];
        titleLabel.centerX = CGRectGetMidX(self.bounds);
        [titleLabel moveToBottom:CGRectGetMidY(self.bounds) - verticalPadding * 0.5f];
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;
    
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        [contentLabel attachToBottomSideOfView:self.titleLabel byDistance:verticalPadding];
//        contentLabel.textColor = C6;
        contentLabel.font = F2;
        [self addSubview:contentLabel];
        self.contentLabel = contentLabel;
        NSString *imageName = @"store_icon_ping";
        UIImageView *imageView = [UIImageView imageViewWithImageName:imageName];
        
        [self changeWithScore:score level:level];
        imageView.centerY = self.contentLabel.centerY;
        [self addSubview:imageView];
        self.imageView = imageView;
        CGFloat width = self.contentLabel.width + 2.0f + imageView.width;
        self.contentLabel.left = floorf((self.bounds.size.width - width) * 0.5f);
        [self.imageView attachToRightSideOfView:self.contentLabel byDistance:2.0f];
    }
    return self;
}

- (void)changeWithScore:(CGFloat)score level:(CGFloat)level{
    
    NSString *contentString = [NSString stringWithFormat:@"%1.1f分", score];
    [self.contentLabel setSingleLineText:contentString];
    MMHShopRatingLevel ratingLevel;
    if (score < level) {
        ratingLevel = MMHShopRatingLevelLow;
    }else if (score == level){
        ratingLevel = MMHShopRatingLevelNormal;
    }else{
        ratingLevel = MMHShopRatingLevelHigh;
    }
    NSString *imageName = @"";
    switch (ratingLevel) {
        case MMHShopRatingLevelLow: {
            self.contentLabel.textColor = [UIColor colorWithHexString:@"20c100"];
            imageName = @"store_icon_di";
            break;
        }
        case MMHShopRatingLevelNormal: {
            self.contentLabel.textColor = [UIColor colorWithHexString:@"ff0000"];
            imageName = @"store_icon_ping";
            break;
        }
        case MMHShopRatingLevelHigh: {
            self.contentLabel.textColor = [UIColor colorWithHexString:@"ff0000"];
            imageName = @"store_icon_gao";
            break;
        }
        default: {
            break;
        }
    }
    self.imageView.image = [UIImage imageNamed:imageName];
  
}

@end

@interface MMHShopDetailShopRatingCell ()

@property (nonatomic, strong)MMHShopRatingView *productrRatingView;
@property (nonatomic, strong)UIView *separatorLine;
@property (nonatomic, strong)UIView *separatorLine1;
@property (nonatomic, strong)MMHShopRatingView *serviceRatingView;
@property (nonatomic, strong)MMHShopRatingView *shippingRatingView;
@property (nonatomic, strong)UIView *bottomLine;
@end

@implementation MMHShopDetailShopRatingCell


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;
    
    [self updateViews];
}


- (void)updateViews
{
    self.contentView.backgroundColor = [UIColor whiteColor];
    float width = floorf(mmh_screen_width() / 3.0f);
    if (self.productrRatingView == nil) {
        self.productrRatingView = [[MMHShopRatingView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, 50.0f)
                                                                     title:@"商品"
                                                                     score:self.shopDetail.productRating
                                                                     level:self.shopDetail.productLevel];
        [self.contentView addSubview:_productrRatingView];
    }
    [self.productrRatingView changeWithScore:self.shopDetail.productRating level:self.shopDetail.productLevel];
    if (self.separatorLine==nil) {
        self.separatorLine = [[UIView alloc] initWithFrame:CGRectMake(_productrRatingView.right, 4.0f, 1.0f / mmh_screen_scale(), 42.0f)];
        _separatorLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_line_dashline"]];
        [self.contentView addSubview:_separatorLine];
    }
    

    if (self.serviceRatingView==nil) {
        self.serviceRatingView = [[MMHShopRatingView alloc] initWithFrame:CGRectMake(width, 0.0f, width, 50.0f)
                                                                                  title:@"服务"
                                                                                  score:self.shopDetail.serviceRating
                                                                                  level:MMHShopRatingLevelHigh];
        [self.contentView addSubview:_serviceRatingView];
    }
    [self.serviceRatingView changeWithScore:self.shopDetail.serviceRating level:self.shopDetail.serviceLevel];
    if (self.separatorLine1 == nil) {
        self.separatorLine1 = [[UIView alloc] initWithFrame:CGRectMake(_serviceRatingView.right, 4.0f, 1.0f / mmh_screen_scale(), 42.0f)];
        _separatorLine1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_line_dashline"]];
        [self.contentView addSubview:_separatorLine1];
    }

    if (self.shippingRatingView==nil) {
        
        self.shippingRatingView = [[MMHShopRatingView alloc] initWithFrame:CGRectMake(width + width, 0.0f, width, 50.0f)
                                                                                   title:@"物流"
                                                                                   score:self.shopDetail.shippingRating
                                                                                   level:MMHShopRatingLevelLow];
        [self.contentView addSubview:_shippingRatingView];
    }
    [self.shippingRatingView changeWithScore:self.shopDetail.shippingRating level:self.shopDetail.shippingLevel];
    if (self.bottomLine == nil) {
        CGFloat pixel = 1.0f / mmh_screen_scale();
        self.bottomLine = [[UIView alloc] initWithFrame:CGRectMake(self.bounds.origin.x, CGRectGetMaxY(self.bounds) - pixel, self.bounds.size.width, pixel)];
        _bottomLine.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        _bottomLine.backgroundColor = [MMHAppearance separatorColor];
        [self.contentView addSubview:_bottomLine];
    }
    
   
}


+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail
{
    CGFloat height = 0.0f;
    height += 50.0f;
    return height;
}


@end
