//
//  MMHShopDetailShopActivitiesCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopDetail;


@interface MMHShopDetailShopActivitiesCell : UICollectionViewCell

@property (nonatomic, strong) MMHShopDetail *shopDetail;

+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail;
@end
