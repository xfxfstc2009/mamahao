//
//  MMHShopDetailShopInfoCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailShopInfoCell.h"
#import "MMHShopDetail.h"
#import "MMHShopGlanceView.h"
#import "MMHShopDetailAddressView.h"
#import "MMHShopDetailActivityView.h"
#import "MMHShopActivity.h"
#import "MMHAssistant.h"
#import "UIView+Extension.h"
#import "MMHNetworkAdapter+Center.h"
#import "MMHAccountSession.h"
@interface MMHShopDetailShopInfoCell ()

@property (nonatomic, strong) MMHShopGlanceView *glanceView;
@property (nonatomic, strong) MMHShopDetailAddressView *addressView;
@property (nonatomic, strong) NSMutableArray *activityViews;
@property (nonatomic, strong) UIButton *favoriteButton;
@property (nonatomic, strong) MMHImageView *backgroundImageView;
@property (nonatomic, strong) UILabel *followerCountLabel;
@property (nonatomic, strong) UIButton *followButton;
@property (nonatomic, strong) UIView *infoContentView;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) UIImageView *distanceIndicator;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UIButton *positionButton;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *phoneTipsLabel;
@property (nonatomic, strong) UIButton *callButton;
@end


@implementation MMHShopDetailShopInfoCell


//- (void)setDelegate:(id<MMHShopDetailShopInfoCellDelegate>)delegate
//{
//    _delegate = delegate;
//
//    self.addressView.delegate = delegate;
//}


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (void)updateViews
{
    if (self.backgroundImageView == nil) {
        MMHImageView *backgroundImageView = [[MMHImageView alloc] initWithFrame:self.bounds];
        [self.contentView addSubview:backgroundImageView];
        self.backgroundImageView = backgroundImageView;
    }
    [self.backgroundImageView updateViewWithImageAtURL:self.shopDetail.imageURLStrings[0]];
    
    if (self.followerCountLabel == nil) {
        UILabel *followerCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        followerCountLabel.y = 10.0f;
        followerCountLabel.textColor = [UIColor whiteColor];
        followerCountLabel.font = F1;
        followerCountLabel.backgroundColor = [UIColor colorWithHexString:@"fc2b43"];
        followerCountLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:followerCountLabel];
        self.followerCountLabel = followerCountLabel;
    }
    long followerCount = self.shopDetail.followerCount;

        NSString *followerCountString = [NSString stringWithFormat:@"%ld", followerCount];
        if (followerCount >= 11000) {
            followerCountString = [NSString stringWithFormat:@"%.1f万", (CGFloat)followerCount / 10000.0f];
        }
        NSString *fullFollowerCountString = [followerCountString stringByAppendingString:@"\n粉丝"];
        [self.followerCountLabel setSingleLineText:fullFollowerCountString
                                constrainedToWidth:CGFLOAT_MAX
                                    withEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 5.0f)];
        self.followerCountLabel.numberOfLines = 0;
        self.followerCountLabel.width = MAX(self.followerCountLabel.width, 45.0f);
        self.followerCountLabel.height = 40.0f;
        [self.followerCountLabel moveToRight:self.bounds.size.width];
    if (self.followButton == nil) {
        UIButton *followButton = [[UIButton alloc] initWithFrame:CGRectMake(self.followerCountLabel.left - 45.0f, self.followerCountLabel.top, 45.0f, 40.0f)];
        followButton.backgroundColor = C21;
        [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        followButton.titleLabel.font = [F1 boldFont];
        followButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        followButton.titleLabel.numberOfLines = 0;
        [followButton addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:followButton];
        self.followButton = followButton;
    }
    //if (followerCount == 0) {
       //[self.followButton moveToRight:mmh_screen_width()];
   // }
    if (self.shopDetail.memberId) {
        self.followButton.hidden = YES;
    }
    else {
        self.followButton.hidden = NO;
        [self.followButton setImage:[UIImage imageNamed:@"store_icon_add"] forState:UIControlStateNormal];
        [self.followButton setTitle:@"关注" forState:UIControlStateNormal];
        [self.followButton makeVerticalWithPadding:3.0f];
    }

    if (self.infoContentView == nil) {
        UIView *infoContentView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 40.0f)];
        [infoContentView moveToBottom:CGRectGetMaxY(self.backgroundImageView.frame)];
        infoContentView.backgroundColor = [C7 colorWithAlphaComponent:0.65];
        [self.contentView addSubview:infoContentView];
        self.infoContentView = infoContentView;
    }
    
    if (self.distanceLabel == nil) {
        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 5.0f, 0.0f, 0.0f)];
        distanceLabel.textColor = [UIColor whiteColor];
        distanceLabel.font = F2;
        [self.infoContentView addSubview:distanceLabel];
        self.distanceLabel = distanceLabel;
    }
    NSString *distanceString = @"门店地址";
    if (self.shopDetail.distance != 0) {
        CGFloat shopDistance = self.shopDetail.distance;
        if (shopDistance>1000) {
            shopDistance = shopDistance/1000.0;
            distanceString = [NSString stringWithFormat:@"距离您%.1fkm", shopDistance];
        }else{
            distanceString = [NSString stringWithFormat:@"距离您%ldm", self.shopDetail.distance];
        }
    }
    [self.distanceLabel setSingleLineText:distanceString];

    if (self.positionButton == nil) {
        UIButton *positionButton = [UIButton buttonWithImageName:@"icon_store_locate"
                                            highlightedImageName:nil
                                                           title:nil
                                                          target:self
                                                          action:@selector(navigate:)];
        [self.infoContentView addSubview:positionButton];
        self.positionButton = positionButton;
    }
    [self.positionButton attachToRightSideOfView:self.distanceLabel byDistance:6.0f];
    self.positionButton.centerY = self.distanceLabel.centerY;
    
    if (self.addressLabel == nil) {
        UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, 0.0f, 0.0f)];
        addressLabel.textColor = [UIColor whiteColor];
        addressLabel.font = F2;
        [self.infoContentView addSubview:addressLabel];
        self.addressLabel = addressLabel;
    }
    [self.addressLabel setSingleLineText:self.shopDetail.address];
    self.addressLabel.lineBreakMode = NSLineBreakByTruncatingHead;
   // self.addressLabel.textAlignment = NSTextAlignmentRight;
    [self.addressLabel moveToBottom:self.infoContentView.bounds.size.height - 3.0f];
    
    if (self.phoneLabel == nil) {
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        phoneLabel.textColor = self.distanceLabel.textColor;
        phoneLabel.font = self.distanceLabel.font;
        [self.infoContentView addSubview:phoneLabel];
        self.phoneLabel = phoneLabel;
    }
    if (!self.shopDetail.phone.length) {
        self.shopDetail.phone = @"联系方式待设置";
    }
    [self.phoneLabel setSingleLineText:self.shopDetail.phone];
    self.phoneLabel.top = self.addressLabel.top;
    self.phoneLabel.height = self.addressLabel.height;
    [self.phoneLabel moveToRight:mmh_screen_width() - 10.0f];
    
    if (self.phoneTipsLabel == nil) {
        UILabel *phoneTipsLabel = [[UILabel alloc] init];
        phoneTipsLabel.textColor = self.phoneLabel.textColor;
        phoneTipsLabel.font = self.phoneLabel.font;
        [self.infoContentView addSubview:phoneTipsLabel];
        self.phoneTipsLabel = phoneTipsLabel;
    }
    [self.phoneTipsLabel setSingleLineText:@"门店咨询"];
    self.phoneTipsLabel.left = self.phoneLabel.left;
    self.phoneTipsLabel.top = self.distanceLabel.top;
    self.phoneTipsLabel.height = self.distanceLabel.height;
    
    if (self.callButton == nil) {
        UIButton *callButton = [UIButton buttonWithImageName:@"store_icon_call"
                                        highlightedImageName:nil
                                                       title:nil
                                                      target:self
                                                      action:@selector(call:)];
        [self.infoContentView addSubview:callButton];
        self.callButton = callButton;
    }
    [self.callButton attachToRightSideOfView:self.phoneTipsLabel byDistance:6.0f];
    self.callButton.centerY = self.phoneTipsLabel.centerY;

    [self.addressLabel setMaxX:self.phoneLabel.left - 5.0f];
}


- (void)follow:(UIButton *)followButton
{

   [[MMHNetworkAdapter sharedAdapter] addCollectWithType:3 collectItemId:self.shopDetail.shopId from:nil succeededHandler:^{
       [self.followButton setHidden:YES];
    long followerCount = [self.followerCountLabel.text  integerValue]+1;
    NSString *followerCountString = [NSString stringWithFormat:@"%ld", followerCount];
       if (followerCount >= 11000) {
           followerCountString = [NSString stringWithFormat:@"%.1f万", (CGFloat)followerCount / 10000.0f];
       }
       NSString *fullFollowerCountString = [followerCountString stringByAppendingString:@"\n粉丝"];
       self.followerCountLabel.text = fullFollowerCountString;
     } failedHandler:^(NSError *error) {
         [followButton setHidden:NO];
         [self showTipsWithError:error];
   }];

}


- (void)navigate:(UIButton *)navigateButton
{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(shopDetailShopInfoCell:didSelectActivity:)]) {
        [self.delegate shopDetailShopInfoCell:self didSelectActivity:nil];
    }
}

- (void)call:(UIButton *)callButton
{
    [UIApplication tryToCallPhoneNumber:self.shopDetail.phone];
}


+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail
{
    CGFloat height = 0.0f;
    height += MMHFloat(180.0f); // shop glance view
    return height;
}


- (void)activityViewTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
   // MMHShopDetailActivityView *activityView = (MMHShopDetailActivityView *) tapGestureRecognizer.view;
    id <MMHShopDetailShopInfoCellDelegate> delegate = self.delegate;
    if (delegate) {
        if ([delegate respondsToSelector:@selector(shopDetailShopInfoCell:didSelectActivity:)]) {
            // TODO: - Louis - remove this
//            [delegate shopDetailShopInfoCell:self didSelectActivity:activityView.activity];
        };
    }
}


@end
