//
//  MMHShopDetailShopActivitiesCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailShopActivitiesCell.h"
#import "MMHShopDetail.h"
#import "MMHShopActivity.h"
#import "MMHShopProduct1.h"

@interface MMHShopDetailShopActivitiesCell ()
@property (nonatomic, strong)UIScrollView *contentScrollView;

@end
@implementation MMHShopDetailShopActivitiesCell


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addTopSeparatorLine];
        [self.contentView addBottomSeparatorLine];
    }
    return self;
}


- (void)updateViews
{
    self.contentView.backgroundColor = [MMHAppearance backgroundColor];
    NSArray *activities = _shopDetail.activities;
    if(self.contentScrollView==nil){
        self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 120.0f)];
        _contentScrollView.backgroundColor = [UIColor whiteColor];
         [self.contentView addSubview:_contentScrollView];
    }
    [self.contentScrollView removeAllSubviews];
    for (int i = 0; i < activities.count; i++) {
        MMHShopActivity *activity = activities[i];
        CGFloat backViewX = 10 + (155+10)*i;
        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(backViewX, 15, 155, 90)];
        backView.backgroundColor = C20;
        [_contentScrollView addSubview:backView];
        UIView *frontView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 141, 80)];
        frontView.center = backView.center;
        frontView.backgroundColor = [UIColor whiteColor];
        [_contentScrollView addSubview:frontView];
        
        UILabel *activityNameLabel = [[UILabel alloc] init];
        activityNameLabel.font = [UIFont boldSystemFontOfSize:20];
        activityNameLabel.textColor = C21;
        activityNameLabel.text = activity.activityName;
        CGSize activityNameSize = [activity.activityName sizeWithCalcFont:activityNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        activityNameLabel.frame = CGRectMake((frontView.width - activityNameSize.width)/2, 10, activityNameSize.width, 20);
        [frontView addSubview:activityNameLabel];
        
        UILabel *activityLabel = [[UILabel alloc] init];
        activityLabel.font = F2;
        activityLabel.textColor = C20;
        activityLabel.text = [NSString stringWithFormat:@"满%@减%@",activity.key, activity.value];
        CGSize size = [activityLabel.text sizeWithCalcFont:activityLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        activityLabel.frame = CGRectMake((frontView.width - size.width)/2, 40, size.width, 12);
        [frontView addSubview:activityLabel];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(4, 55, frontView.width - 8, 0.5)];
        lineView.backgroundColor = C6;
        [frontView addSubview:lineView];
        
        UILabel *validLabel = [[UILabel alloc] init];
        validLabel.font = F1;
        validLabel.textColor = C6;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *beginDate = [formatter dateFromString:activity.beginDay];
        NSDate *endDate = [formatter dateFromString:activity.endDay];
        [formatter setDateFormat:@"M.d"];
        NSString *beginDay = [formatter stringFromDate:beginDate];
        NSString *endDay = [formatter stringFromDate:endDate];
        validLabel.text = [NSString stringWithFormat:@"使用时间: %@~%@", beginDay,endDay];
        validLabel.frame = CGRectMake(0, 59, frontView.width, 11);
        validLabel.textAlignment = NSTextAlignmentCenter;
        [frontView addSubview:validLabel];
        
    }
    _contentScrollView.contentSize = CGSizeMake(10+165*activities.count, 120);
   
    if (!activities.count) {
        [self.contentView setHidden:YES];
    }else{
        [self.contentView setHidden:NO];
    }
}

+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail
{
    if (!shopDetail.activities.count) {
        return 0.0f;
    }
    return 120.0f;
}


@end
