//
//  MMHShopDetailShopInfoCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHShopDetailAddressView.h"


@class MMHShopDetail;
@class MMHShopGlanceView;
@class MMHShopDetailAddressView;
@class MMHShopDetailShopInfoCell;
@class MMHShopActivity;

@protocol MMHShopDetailAddressViewDelegate;


@protocol MMHShopDetailShopInfoCellDelegate <NSObject, MMHShopDetailAddressViewDelegate>

- (void)shopDetailShopInfoCell:(MMHShopDetailShopInfoCell *)shopDetailShopInfoCell didSelectActivity:(MMHShopActivity *)activity;

@end


@interface MMHShopDetailShopInfoCell : UICollectionViewCell

@property (nonatomic, weak) id<MMHShopDetailShopInfoCellDelegate> delegate;

@property (nonatomic, strong) MMHShopDetail *shopDetail;

+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail;
@end
