//
//  MMHShopGlanceView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopGlanceView.h"
#import "MMHShopDetail.h"
#import "UIImageView+EMWebCache.h"
#import "MMHImageView.h"
#import "UIView+Extension.h"
#import "MMHNetworkAdapter+Center.h"

double const MMHShopGlanceRatingExcellentScore = 4.8;
double const MMHShopGlanceRatingTerribleScore = 4.6;



@interface MMHShopGlanceRatingView: UIView

@property (nonatomic, copy) NSString *title;
@property (nonatomic) double score;
@property (nonatomic, strong) UIView *dotView;
@property (nonatomic, strong) UILabel *contentLabel;
@end


@implementation MMHShopGlanceRatingView


- (instancetype)initWithHeight:(CGFloat)height title:(NSString *)title score:(double)score
{
    self = [self initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, height)];
    if (self) {
        self.title = title;
        self.score = score;
        
        self.backgroundColor = [UIColor clearColor];

        UIView *dotView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 4.0f, 4.0f)];
        dotView.centerY = CGRectGetMidY(self.bounds);
        dotView.backgroundColor = [UIColor colorWithHexString:@"6ec70f"];
        dotView.layer.cornerRadius = 2.0f;
        [self addSubview:dotView];
        self.dotView = dotView;

        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, 0.0f, 0.0f)];
        contentLabel.textColor = [UIColor whiteColor];
        contentLabel.font = [MMHAppearance smallTipsFont];
        NSString *contentString = [NSString stringWithFormat:@"%@：%1.1f分", title, score];
        [contentLabel setSingleLineText:contentString constrainedToWidth:CGFLOAT_MAX];
        contentLabel.centerY = CGRectGetMidY(self.bounds);
        [self addSubview:contentLabel];
        self.contentLabel = contentLabel;

        if (score >= MMHShopGlanceRatingExcellentScore) {
            [self.contentLabel setTextColor:[UIColor colorWithHexString:@"6ec70f"] inRange:NSMakeRange(contentString.length - 4, 3)];
        }
        else if (score <= MMHShopGlanceRatingTerribleScore) {
            [self.contentLabel setTextColor:[UIColor colorWithHexString:@"ff4d61"] inRange:NSMakeRange(contentString.length - 4, 3)];
        }

        CGFloat padding = 12.0f;
        self.width = self.contentLabel.right + padding;
    }
    return self;
}


@end


@interface MMHShopGlanceView ()

@property (nonatomic, strong) MMHImageView *backgroundImageView;
@property (nonatomic, strong) UIView *ratingBackgroundView;
@property (nonatomic, strong) UIImageView *tagImageView;
@property (nonatomic, strong) MMHShopGlanceRatingView *shippingRatingView;
@property (nonatomic, strong) MMHShopGlanceRatingView *serviceRatingView;
@property (nonatomic, strong) MMHShopGlanceRatingView *productRatingView;
@property (nonatomic, strong) UILabel *followerCountLabel;
@property (nonatomic, strong) UIButton *followButton;
@end


@implementation MMHShopGlanceView


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (instancetype)initWithShopDetail:(MMHShopDetail *)shopDetail
{
    CGRect frame = CGRectMake(0.0f, 0.0f, mmh_screen_width(), MMHFloat(180.0f));
    self = [self initWithFrame:frame];
    if (self) {
        self.shopDetail = shopDetail;
    }
    return self;
}


- (void)updateViews
{
    if (self.backgroundImageView == nil) {
        MMHImageView *backgroundImageView = [[MMHImageView alloc] initWithFrame:self.bounds];
        [self addSubview:backgroundImageView];
        self.backgroundImageView = backgroundImageView;
    }
//    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:self.shopDetail.imageURLStrings[0]]];
    [self.backgroundImageView updateViewWithImageAtURL:self.shopDetail.imageURLStrings[0]];

    if (self.followerCountLabel == nil) {
        UILabel *followerCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        followerCountLabel.y = 10.0f;
        followerCountLabel.textColor = [UIColor whiteColor];
        followerCountLabel.font = F1;
        followerCountLabel.backgroundColor = [UIColor colorWithHexString:@"f70c34"];
        followerCountLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:followerCountLabel];
        self.followerCountLabel = followerCountLabel;
    }
    long followerCount = self.shopDetail.followerCount;
    NSString *followerCountString = [NSString stringWithFormat:@"%ld", followerCount];
    if (followerCount >= 10000) {
        followerCountString = [NSString stringWithFormat:@"%ld万", followerCount / 10000];
    }
    NSString *fullFollowerCountString = [followerCountString stringByAppendingString:@"\n粉丝"];
    [self.followerCountLabel setSingleLineText:fullFollowerCountString
                            constrainedToWidth:CGFLOAT_MAX
                                withEdgeInsets:UIEdgeInsetsMake(0.0f, 5.0f, 0.0f, 5.0f)];
    self.followerCountLabel.numberOfLines = 0;
    self.followerCountLabel.width = MAX(self.followerCountLabel.width, 45.0f);
    self.followerCountLabel.height = 40.0f;
    [self.followerCountLabel moveToRight:self.bounds.size.width];

    if (self.followButton == nil) {
        UIButton *followButton = [[UIButton alloc] initWithFrame:CGRectMake(self.followerCountLabel.left - 45.0f, self.followerCountLabel.top, 45.0f, 40.0f)];
        followButton.backgroundColor = [UIColor colorWithHexString:@"fa334f"];
        [followButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        followButton.titleLabel.font = F3;
        followButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        followButton.titleLabel.numberOfLines = 0;
        [followButton addTarget:self action:@selector(follow:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:followButton];
        self.followButton = followButton;
    }
    if (self.shopDetail.isFavorite) {
        [self.followButton setTitle:@"已关注" forState:UIControlStateNormal];
    }
    else {
        [self.followButton setTitle:@"+\n关注" forState:UIControlStateNormal];
    }

//    if (self.ratingBackgroundView == nil) {
//        UIView *ratingBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bounds.size.width, 30.0f)];
//        [ratingBackgroundView moveToBottom:CGRectGetMaxY(self.bounds)];
//        ratingBackgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
//        [self addSubview:ratingBackgroundView];
//        self.ratingBackgroundView = ratingBackgroundView;
//    }
//
//    if (self.tagImageView == nil) {
//        UIImageView *tagImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0f, -3.0f, 25.0f, 25.0f)];
//        tagImageView.image = [UIImage imageNamed:@"tag_store_self"];
//        [self.ratingBackgroundView addSubview:tagImageView];
//        self.tagImageView = tagImageView;
//    }
//
//    if (self.shippingRatingView == nil) {
//        MMHShopGlanceRatingView *shippingRatingView = [[MMHShopGlanceRatingView alloc] initWithHeight:self.ratingBackgroundView.height
//                                                                                                title:@"物流"
//                                                                                                score:self.shopDetail.shippingRating];
//        [shippingRatingView moveToRight:CGRectGetMaxX(self.ratingBackgroundView.bounds)];
//        [self.ratingBackgroundView addSubview:shippingRatingView];
//        self.shippingRatingView = shippingRatingView;
//    }
//
//    if (self.serviceRatingView == nil) {
//        MMHShopGlanceRatingView *serviceRatingView = [[MMHShopGlanceRatingView alloc] initWithHeight:self.ratingBackgroundView.height
//                                                                                                title:@"服务"
//                                                                                                score:self.shopDetail.serviceRating];
//        [serviceRatingView moveToRight:self.shippingRatingView.left];
//        [self.ratingBackgroundView addSubview:serviceRatingView];
//        self.serviceRatingView = serviceRatingView;
//    }
//
//    if (self.productRatingView == nil) {
//        MMHShopGlanceRatingView *productRatingView = [[MMHShopGlanceRatingView alloc] initWithHeight:self.ratingBackgroundView.height
//                                                                                                title:@"商品"
//                                                                                                score:self.shopDetail.productRating];
//        [productRatingView moveToRight:self.serviceRatingView.left];
//        [self.ratingBackgroundView addSubview:productRatingView];
//        self.productRatingView = productRatingView;
//    }
}


- (void)follow:(UIButton *)followButton
{
      
}


@end
