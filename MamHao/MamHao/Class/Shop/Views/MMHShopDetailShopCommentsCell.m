//
//  MMHShopDetailShopCommentsCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailShopCommentsCell.h"
#import "MMHShopDetail.h"
#import "MMHRatingView.h"
#import "UIView+Extension.h"


@interface MMHShopDetailShopCommentsCell ()

@property (nonatomic, strong) NSMutableArray *customerAvatarViews;
@property (nonatomic, strong) MMHRatingView *ratingView;
@property (nonatomic, strong) UILabel *commentCountLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicator;
@end


@implementation MMHShopDetailShopCommentsCell


- (void)setShopDetail:(MMHShopDetail *)shopDetail
{
    _shopDetail = shopDetail;

    [self updateViews];
}


- (void)updateViews
{
    self.contentView.backgroundColor = [UIColor whiteColor];

    NSInteger customerCount = MIN(self.shopDetail.customerAvatars.count, 5);
    for (MMHImageView *avatarView in self.customerAvatarViews) {
        [avatarView removeFromSuperview];
    }
    self.customerAvatarViews = [NSMutableArray array];
    CGFloat x = MMHFloat(25.0f);
    for (NSInteger i = 0; i < customerCount; i++) {
        MMHImageView *avatarView = [[MMHImageView alloc] initWithFrame:CGRectMake(x, MMHFloat(15.0f), MMHFloat(30.0f), MMHFloat(30.0f))];
        [avatarView setBorderColor:[UIColor colorWithHexString:@"d7d7d7"] cornerRadius:0.0f];
        [avatarView makeRoundedRectangleShape];
        [avatarView updateViewWithImageAtURL:[self.shopDetail.customerAvatars[i] objectForKey:@"memberHeadPic"]];
        [self.contentView addSubview:avatarView];
        [self.customerAvatarViews addObject:avatarView];
        x = avatarView.right + MMHFloat(10.0f);
    }

    if (self.ratingView == nil) {
        MMHRatingView *ratingView = [[MMHRatingView alloc] initWithEmptyImageName:@"store_rating_empty" halfImageName:@"store_rating_half" fullImageName:@"store_rating_full"];
        ratingView.top = MMHFloat(17.0f);
        [ratingView moveToRight:mmh_screen_width() - MMHFloat(40.0f)];
        [self.contentView addSubview:ratingView];
        self.ratingView = ratingView;
    }
    self.ratingView.rating = self.shopDetail.serviceRating;
    
    if (self.commentCountLabel == nil) {
        UILabel *commentCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        commentCountLabel.textColor = C4;
        commentCountLabel.font = MMHF2;
        [self.contentView addSubview:commentCountLabel];
        self.commentCountLabel = commentCountLabel;
    }
    [self.commentCountLabel setSingleLineText:[NSString stringWithFormat:@"%ld人点评", self.shopDetail.commentCount]];
    self.commentCountLabel.centerY = MMHFloat(40.0f);
    [self.commentCountLabel moveToRight:mmh_screen_width() - MMHFloat(40.0f)];

    if (self.disclosureIndicator == nil) {
        UIImageView *disclosureIndicator = [UIImageView imageViewWithImageName:@"icon_go_midgray"];
        disclosureIndicator.centerY = MMHFloat(60.0f) * 0.5f;
        [disclosureIndicator moveToRight:mmh_screen_width() - MMHFloat(10.0f)];
        [self.contentView addSubview:disclosureIndicator];
        self.disclosureIndicator = disclosureIndicator;
    }
}


+ (CGFloat)heightForShopDetail:(MMHShopDetail *)shopDetail
{
    CGFloat height = 0.0f;
    height += MMHFloat(60.0f);
    return height;
}


@end
