//
//  MMHShopDetailProductSectionHeaderView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailProductSectionHeaderView.h"


@interface MMHShopDetailProductSectionHeaderView ()

@property (nonatomic, strong) UIView *headerDecorationView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicatorImageView;
@property (nonatomic, strong) UILabel *moreLabel;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger section;
@end


@implementation MMHShopDetailProductSectionHeaderView


- (void)setTitle:(NSString *)title forSection:(NSInteger)section {
    _title = title;
    self.titleLabel.text = title;
    self.section = section;
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIView *headerDecorationView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 15.0f, 3.0f, 16.0f)];
        headerDecorationView.backgroundColor = [MMHAppearance pinkColor];
        [self addSubview:headerDecorationView];
        self.headerDecorationView = headerDecorationView;

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headerDecorationView.frame) + 7.0f, 0.0f, 100.0f, self.bounds.size.height)];
        titleLabel.font = [MMHAppearance textFont];
        titleLabel.textColor = [UIColor blackColor];
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;

        UIImageView *disclosureIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_go_small"]];
        [disclosureIndicatorImageView moveToRight:mmh_screen_width() - 13.0f];
        disclosureIndicatorImageView.centerY = CGRectGetMidY(self.bounds);
        [self addSubview:disclosureIndicatorImageView];
        self.disclosureIndicatorImageView = disclosureIndicatorImageView;

        UILabel *moreLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.disclosureIndicatorImageView.left - 103.0f, 0.0f, 100.0f, self.bounds.size.height)];
//        moreLabel.right = ;
        moreLabel.font = [MMHAppearance tipsFont];
        moreLabel.textColor = [UIColor blackColor];
        moreLabel.textAlignment = NSTextAlignmentRight;
        moreLabel.text = @"更多";
        [self addSubview:moreLabel];
        self.moreLabel = moreLabel;

        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        [self addGestureRecognizer:tapGesture];
    }
    return self;
}

- (void)handleTapGesture:(UITapGestureRecognizer *)gestureRecognizer{
    id <MMHShopDetailProductSectionHeaderViewDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(shopDetailProductSectionHeaderView:didSelectedAtSection:)]) {
        [delegate shopDetailProductSectionHeaderView:self didSelectedAtSection:self.section];
    }
}

@end
