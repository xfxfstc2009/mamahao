//
//  MMHShopDetailActivityView.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHShopActivity;


@interface MMHShopDetailActivityView : UIView

@property (nonatomic, strong) MMHShopActivity *activity;

- (id)initWithActivity:(MMHShopActivity *)activity;

@end
