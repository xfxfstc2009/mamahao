//
//  MMHShopDetailActivityView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetailActivityView.h"
#import "MMHShopActivity.h"


@interface MMHShopDetailActivityView ()

@property (nonatomic, strong) UIView *separatorView;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UIView *headerDecorationView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *dateIndicatorImageView;
@property (nonatomic, strong) UIView *dateView;
@end


@implementation MMHShopDetailActivityView


- (id)initWithActivity:(MMHShopActivity *)activity
{
    self = [self initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 8.0f + 55.0f)];
    if (self) {
        self.activity = activity;
        
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bounds.size.width, 8.0f)];
        separatorView.backgroundColor = [MMHAppearance backgroundColor];
        [self addSubview:separatorView];
        self.separatorView = separatorView;

        UIView *contentView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.separatorView.bottom, self.bounds.size.width, 55.0f)];
        contentView.backgroundColor = [UIColor whiteColor];
        [contentView addTopSeparatorLine];
        [contentView addBottomSeparatorLine];
        [self addSubview:contentView];
        self.contentView = contentView;

        UIView *headerDecorationView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, 3.0f, 16.0f)];
        headerDecorationView.centerY = CGRectGetMidY(self.contentView.bounds);
        headerDecorationView.backgroundColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:headerDecorationView];
        self.headerDecorationView = headerDecorationView;

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.headerDecorationView.right + 7.0f, 0.0f, 0.0f, self.contentView.bounds.size.height)];
        [titleLabel setMaxX:CGRectGetMidX(self.contentView.bounds)];
        titleLabel.centerY = CGRectGetMidY(self.contentView.bounds);
        titleLabel.font = [MMHAppearance textFont];
        titleLabel.textColor = [MMHAppearance blackColor];
        titleLabel.numberOfLines = 0;
        titleLabel.text = activity.activityName;
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;

        UIImageView *dateIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_store_time"]];
        dateIndicatorImageView.left = 6.0f;

        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(dateIndicatorImageView.right + 4.0f, 0.0f, 0.0f, 0.0f)];
        dateLabel.font = [MMHAppearance smallTipsFont];
        dateLabel.textColor = [MMHAppearance blackColor];
      //  NSString *dateString = [NSString stringWithFormat:@"活动时间 %ld.%d-%d.%d日", (long)activity.startDateComponents.month, activity.startDateComponents.day, activity.endDateComponents.month, activity.endDateComponents.day];
       // [dateLabel setSingleLineText:dateString constrainedToWidth:CGFLOAT_MAX];// withEdgeInsets:UIEdgeInsetsMake(2.0f, 6.0f, 2.0f, 6.0f)];
        dateLabel.textAlignment = NSTextAlignmentCenter;

        dateIndicatorImageView.centerY = dateLabel.centerY;

        UIView *dateView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, dateLabel.right + 6.0f, dateLabel.height)];
        [dateView setBorderColor:[UIColor colorWithHexString:@"dcdcdc"] cornerRadius:2.0f];
        dateView.width = dateLabel.right + 6.0f;
        [dateView moveToRight:CGRectGetMaxX(self.contentView.bounds) - 10.0f];
        dateView.centerY = CGRectGetMidY(self.contentView.bounds);
        [dateView addSubview:dateIndicatorImageView];
        [dateView addSubview:dateLabel];

        [self.contentView addSubview:dateView];
        self.dateIndicatorImageView = dateIndicatorImageView;
        self.dateLabel = dateLabel;
        self.dateView = dateView;
    }
    return self;
}
@end
