//
//  MMHShopGroupItemModel.h
//  MamHao
//
//  Created by fishycx on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"
#import "MMHShopProduct1.h"


@class MMHShopGroupItemModel;


@protocol MMHShopGroupItemModel <NSObject>

@end


@interface MMHShopGroupItemModel : MMHFetchModel
@property (nonatomic, assign)NSInteger groupId;
@property (nonatomic, strong)NSString *groupName;
@property (nonatomic, strong)NSArray<MMHShopProduct1>* products;
@end
