//
//  MMHShopActivity.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHShopActivity : MMHFetchModel

@property (nonatomic, copy) NSString *endDay;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) NSString *beginDay;
@property (nonatomic, copy) NSString *companyName;
@property (nonatomic, copy) NSString *activityName;
@property (nonatomic, copy) NSString *key;

@end
