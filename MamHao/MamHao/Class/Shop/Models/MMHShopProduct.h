//
//  MMHShopProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProduct.h"

@interface MMHShopProduct : MMHProduct

@property (nonatomic, assign)NSInteger brand_id;  
@property (nonatomic, assign)NSInteger collect_id; //收藏id
@property (nonatomic) NSInteger goodsNo;
@property (nonatomic) BOOL recommended;
@property (nonatomic, strong) NSString *imageURLString;
@property (nonatomic, copy) NSString *title;
@property (nonatomic) MMHPrice originalPrice;
@property (nonatomic) MMHPrice currentPrice;
@property (nonatomic) MMHPrice discount;
@end
