//
//  MMHShopDetail.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHShopGroupItemModel.h"
#import "MMHTimeline.h"
@import CoreLocation;

typedef NS_ENUM(NSInteger, MMHShopRatingLevel) {
    MMHShopRatingLevelLow = -1,
    MMHShopRatingLevelNormal = 0,
    MMHShopRatingLevelHigh = 1
};


@interface MMHShopDetail : MMHFetchModel

@property (nonatomic, copy) NSString *shopId;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *areaId;
@property (nonatomic, copy) NSString *cityNumId;
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSArray *imageURLStrings;
@property (nonatomic, copy) NSString *shopName;
@property (nonatomic) CGFloat productRating;
@property (nonatomic) CGFloat serviceRating;
@property (nonatomic) CGFloat shippingRating;
@property (nonatomic) CGFloat longitude;
@property (nonatomic) CGFloat latitude;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, strong) NSArray *activities;
@property (nonatomic, strong)NSArray<MMHShopGroupItemModel>* groupItems;

@property (nonatomic) BOOL isFavorite;


// TODO: - Louis - for new requirement
@property (nonatomic) long followerCount;
@property (nonatomic) long distance;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic) CGFloat productLevel;
@property (nonatomic) CGFloat serviceLevel;
@property (nonatomic) CGFloat shippingLevel;
@property (nonatomic, strong) NSArray *customerAvatars;
@property (nonatomic) CGFloat shopRating;
@property (nonatomic) long commentCount;
@property (nonatomic, copy) NSString *shippingRange;
@property (nonatomic, copy) NSString *onlineShopName;

@end


@interface MMHShopService : MMHFetchModel

@property (nonatomic, copy) NSString *introduction;
@property (nonatomic, strong) NSDateComponents *openFromDateComponents;
@property (nonatomic, strong) NSDateComponents *openUntilDateComponents;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, strong) NSArray *services;

@end
