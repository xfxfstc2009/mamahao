//
//  MMHShopProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopProduct.h"


@implementation MMHShopProduct


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    NSMutableDictionary *km = [@{@"recommended": @"isRecommended", @"imageURLString": @"image", @"title": @"goodsTitle"} mutableCopy];
    self = [super initWithJSONDict:dict keyMap:km];
    return self;
}


@end
