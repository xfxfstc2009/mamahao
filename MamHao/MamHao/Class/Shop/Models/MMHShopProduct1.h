//
//  MMHShopProduct1.h
//  MamHao
//
//  Created by fishycx on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@class MMHShopProduct1;

@protocol  MMHShopProduct1<NSObject>
@end
@interface MMHShopProduct1 : MMHProduct<MMHProductProtocol>

@property (nonatomic, copy)NSString *styleId; 
@property (nonatomic, copy)NSString *shopId;
@property (nonatomic, copy)NSString *groupName;
@property (nonatomic, assign)NSInteger proPrice;
@property (nonatomic, copy)NSString *itemName;
@property (nonatomic, copy)NSString *goodsPic;
@property (nonatomic, assign)NSInteger retailPrice;
@property (nonatomic, assign)NSInteger customPrice;
@property (nonatomic, copy)NSString *marketEndDay;

@end
