//
//  MMHShopDetail.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopDetail.h"
#import "MMHShopActivity.h"
#import "MMHShopProduct.h"
#import "MMHLocationManager.h"
#import "MMHCurrentLocationModel.h"
@import CoreLocation;

@implementation MMHShopDetail


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.activities = [dict[@"activities"] transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
            return [[MMHShopActivity alloc] initWithJSONDict:originalObject keyMap:nil];
        }];
//        self.arrivals = [dict[@"arrivals"] transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
//            return [[MMHShopProduct alloc] initWithJSONDict:originalObject keyMap:nil];
//        }];
//        self.hotProducts = [dict[@"hotProducts"] transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
//            return [[MMHShopProduct alloc] initWithJSONDict:originalObject keyMap:nil];
//        }];
        self.imageURLStrings = @[[dict stringForKey:@"indexLogoPic"]];
     [MMHLocationManager getArea];
    }
    return self;
}

- (long)distance{
    
    CLLocation *myLocation = [[MMHCurrentLocationModel sharedLocation] myGeographicalLocation];
    CLLocation *shopCLL = [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longitude];
    return [shopCLL distanceFromLocation:myLocation];
}

@end


@implementation MMHShopService


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.introduction = dict[@"description"];
        NSDateComponents *openFromDateComponents = [[NSDateComponents alloc] init];
        openFromDateComponents.hour = [dict integerForKey:@"openFromHour"];
        openFromDateComponents.minute = [dict integerForKey:@"openFromMinute"];
        self.openFromDateComponents = openFromDateComponents;
        NSDateComponents *openUntilDateComponents = [[NSDateComponents alloc] init];
        openUntilDateComponents.hour = [dict integerForKey:@"openUntilHour"];
        openUntilDateComponents.minute = [dict integerForKey:@"openUntilMinute"];
        self.openUntilDateComponents = openUntilDateComponents;
    }
    return self;
}


@end
