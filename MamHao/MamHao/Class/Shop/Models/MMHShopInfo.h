//
//  MMHShopInfo.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHShopInfo : MMHFetchModel
@property (nonatomic, assign)NSInteger shopId;
@property (nonatomic, assign)NSInteger groupId;
@property (nonatomic, assign)NSInteger styleId;
@property (nonatomic, assign)NSInteger brandId;
@property (nonatomic, assign)NSInteger styleApplyAge;
@end
