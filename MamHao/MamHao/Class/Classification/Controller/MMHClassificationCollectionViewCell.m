//
//  MMHClassificationCollectionViewCell.m
//  MamHao
//
//  Created by fishycx on 15/5/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHClassificationCollectionViewCell.h"
#import "MMHImageView.h"
@interface MMHClassificationCollectionViewCell()

@property (nonatomic, strong)MMHImageView *itemImageView;    //类目图片
@property (nonatomic, strong)UILabel *itemDescriptionLabel; //商品描述

@end
@implementation MMHClassificationCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.itemImageView = [[MMHImageView alloc] initWithFrame: CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - MMHFloat(20))];
        [self addSubview:_itemImageView];
        self.itemDescriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_itemImageView.frame), self.bounds.size.width, MMHFloat(20))];
        _itemDescriptionLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        _itemDescriptionLabel.font = MMHFontOfSize(12);
        _itemDescriptionLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_itemDescriptionLabel];
    }
    return self;
}

- (void)setModel:(MMHClassifiCationItemModel *)model {
    _model = model;
    [self.itemImageView updateViewWithImageAtURL:model.pic];
    self.itemDescriptionLabel.text = model.name;
}
@end
