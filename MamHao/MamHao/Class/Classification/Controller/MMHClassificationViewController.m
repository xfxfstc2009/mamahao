//
//  MMHClassificationViewController.m
//  MamHao
//
//  Created by 余传兴 on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHClassificationViewController.h"
#import "NSString+catagory.h"
#import "MMHSearchViewController.h"
#import "MMHSearchBar.h"
#import "MMHNetworkAdapter+Classification.h"
#import "MMHClassifiCationItemModel.h"
#import "MMHMultilevelTableViewCell.h"
#import "MMHClassificationCollectionViewCell.h"
#import "MMHClassificationCollectionReusableView.h"
#import "MMHBannerCell.h"
#import "MMHBannerModel.h"
#import "MMHSearchBar.h"
#import "MMHCategory.h"
#import "MMHFilter.h"
#import "MMHProductListViewController.h"
#import "MMHScanningViewController.h"
#import "MamHao-Swift.h"
#import "Reachability.h"

#define kLeftItemWidht MMHFloat(90)
#define KLeftItemHeight MMHFloat(60)
#define KMMHmutilevelCollectionCell @"MMHmutilevelCollectionCell"
@class MMHClassificationViewController;

@interface MMHClassificationViewController ()<UITextFieldDelegate,UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, UICollectionViewDelegateFlowLayout,UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) MMHSearchBar *searchBar;
@property (nonatomic, strong) UIView *searchBackView;
@property (nonatomic, strong) UITextField *seachTextField; //搜索TextField
@property (nonatomic, strong) UIButton *rightNaviButoon; // 搜索右边的按钮；
@property (nonatomic, strong) MMHSearchViewController *searchVC;//searchVC
@property (nonatomic, strong) NSMutableArray *zeroLevelDataArray;//第0层的数据；
@property (nonatomic, strong) NSMutableArray *rightDataArray; //右边视图的数据；
@property (nonatomic, strong) UITableView *leftTableView; //左边视图
@property (nonatomic, strong) UICollectionView *rightCollectionView; //右边视图
@property (nonatomic, assign) NSInteger selectIndex; //leftView选中的下标
@property (nonatomic, strong) UICollectionView *bannerCollectionView;
@property (strong,nonatomic) UIColor * leftBgColor;
@property (nonatomic, strong) UITextField *search;
@property (nonatomic, assign) BOOL isBrand;
/**
 *  左边点中文字颜色
 */
@property(strong,nonatomic) UIColor * leftSelectColor;
/**
 *  左边点中背景颜色
 */
@property(strong,nonatomic) UIColor * leftSelectBgColor;

/**
 *  左边未点中文字颜色
 */

@property(strong,nonatomic) UIColor * leftUnSelectColor;
/**
 *  左边未点中背景颜色
 */
@property(strong,nonatomic) UIColor * leftUnSelectBgColor;
@property(strong,nonatomic) UIColor * leftSeparatorColor;//分割线颜色
@property(copy,nonatomic,readonly) id block;
@property(assign, nonatomic) BOOL isRecordLastScroll;
@property(assign, nonatomic) BOOL isReturnLastOffset;
@property(assign, nonatomic) BOOL isHaveBanner;//是不是有banner
@property(strong, nonatomic) NSMutableArray *bannerArray;//banner图片

@end

@implementation MMHClassificationViewController

#pragma mark ----getter


- (UICollectionView *)bannerCollectionView {
    if (!_bannerCollectionView) {
        float leftMargin = MMHFloat(10);
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        
        self.bannerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(kLeftItemWidht + leftMargin,10, kScreenWidth - kLeftItemWidht - leftMargin*2, self.view.bounds.size.height) collectionViewLayout:flowLayout];
        self.bannerCollectionView.delegate = self;
        self.bannerCollectionView.dataSource = self;
        _bannerCollectionView.backgroundColor = [UIColor whiteColor];
        [self.bannerCollectionView registerClass:[MMHClassificationCollectionViewCell class] forCellWithReuseIdentifier:KMMHmutilevelCollectionCell];
        [self.bannerCollectionView registerClass:[MMHBannerCell class] forCellWithReuseIdentifier:@"banner"];
        
        [self.bannerCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        [self.bannerCollectionView registerClass:[MMHClassificationCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [self.view addSubview:_bannerCollectionView];

    }
    return _bannerCollectionView;
}
- (MMHSearchViewController *)searchVC {
    if (!_searchVC) {
        self.searchVC = [[MMHSearchViewController alloc] init];
    }
    return _searchVC;
}
- (NSMutableArray *)bannerArray {
    if (!_bannerArray) {
        self.bannerArray = [[NSMutableArray alloc] init];
    }
    return _bannerArray;
}

- (NSMutableArray *)zeroLevelDataArray {
    if (!_zeroLevelDataArray) {
        self.zeroLevelDataArray = [NSMutableArray array];
    }
    return _zeroLevelDataArray;
}
- (UITableView *)leftTableView {
    if (!_leftTableView) {
        self.leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kLeftItemWidht
                                                                      , self.view.bounds.size.height) style:UITableViewStylePlain];
        self.leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        [self.view addSubview:_leftTableView];
    }
    return _leftTableView;
}
- (UICollectionView *)rightCollectionView {
    if (!_rightCollectionView) {
        
        float leftMargin = MMHFloat(10);
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
        self.rightCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(kLeftItemWidht + leftMargin,10, kScreenWidth - kLeftItemWidht - leftMargin*2, self.view.bounds.size.height) collectionViewLayout:flowLayout];
        self.rightCollectionView.delegate = self;
        self.rightCollectionView.dataSource = self;
        _rightCollectionView.backgroundColor = [UIColor whiteColor];
        [self.rightCollectionView registerClass:[MMHClassificationCollectionViewCell class] forCellWithReuseIdentifier:KMMHmutilevelCollectionCell];
        [self.rightCollectionView registerClass:[MMHBannerCell class] forCellWithReuseIdentifier:@"banner"];
    
        [self.rightCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        [self.rightCollectionView registerClass:[MMHClassificationCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [self.view addSubview:_rightCollectionView];
    }
    return _rightCollectionView;
}

- (NSMutableArray *)rightDataArray {
    if (!_rightDataArray) {
        self.rightDataArray = [NSMutableArray array];
    }
    return _rightDataArray;
}

#pragma mark - lifeCircle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSeting];
    [self fetchDataWithTypeId:1 rating:1];
}


- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
      [self rightBarButtonWithTitle:@"" barNorImage:[UIImage imageNamed:@"home_icon_scan"] barHltImage:[UIImage imageNamed:@""] action:^{
          MMHScanningViewController *scanViewController = [[MMHScanningViewController alloc] init];
          [self pushViewController:scanViewController];
      }];
    CGRect frame = CGRectMake(50, 8, self.view.bounds.size.width - 100, 28);
    UITextField *field = [[UITextField alloc] initWithFrame:frame];
    self.search = field;
    field.delegate = self;
    field.backgroundColor = [UIColor whiteColor];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(10, 0, 34, 32);
    [button setImage:[UIImage imageNamed:@"mmh_icon_search"] forState:UIControlStateNormal];
    field.leftView = button;
    field.layer.cornerRadius = MMHFloat(5);
    field.adjustsFontSizeToFitWidth = YES;
    field.leftViewMode = UITextFieldViewModeAlways;
    [self.navigationController.navigationBar addSubview:field];
    field.placeholder = @"搜索商品/品牌";
    field.font = MMHF4;
   
}
- (void)viewWillDisappear:(BOOL)animated {
   // [self.searchBackView removeFromSuperview];
    
   // [self.searchBar removeFromSuperview];
    [self.search removeFromSuperview];
}

#pragma mark - <UITextFieldDelegate>

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    MMHSearchViewController *search = [[MMHSearchViewController alloc] init];
    [self.navigationController pushViewController:search animated:YES];
    return NO;
}
#pragma mark--private method

- (void)fetchDataWithTypeId:(NSInteger)typeId rating:(NSInteger)rating{
   [[MMHNetworkAdapter sharedAdapter] fetchDataWithTypeId:typeId rating:rating succeededHandeler:^(NSArray *array) {
       for (NSDictionary *dic in array) {
           MMHClassifiCationItemModel *model = [[MMHClassifiCationItemModel alloc] initWithJSONDict:dic];
           [self.zeroLevelDataArray addObject:model];
           [self.leftTableView reloadData];
       }
       
       MMHClassifiCationItemModel *model = self.zeroLevelDataArray[0];
       [self fetchRightCollectionViewDataWithModel:model];
        } failedHandler:^(NSError *error) {
            [self.view showTipsWithError:error];
   }];
}
- (void)fetchRightCollectionViewDataWithModel:(MMHClassifiCationItemModel *)model{
    [self.rightDataArray removeAllObjects];
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [model fetchSecondLevelDataSucceed:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        NSArray *banner = dic[@"banner"];
        for (NSDictionary *dic in banner) {
            MMHBannerModel *model = [[MMHBannerModel alloc] initWithJSONDict:dic];
            [strongSelf.bannerArray addObject:model];
        }
        strongSelf.isHaveBanner = NO;
        NSArray *array = dic[@"data"];
        NSMutableArray *rightDataArray = [NSMutableArray array];
        for (NSDictionary *rightDatadic in array) {
            MMHClassifiCationItemModel *model = [[MMHClassifiCationItemModel alloc] initWithJSONDict:rightDatadic];
            [rightDataArray addObject:model];
            if ([rightDatadic hasKey:@"type"]) {
                if ([[rightDatadic stringForKey:@"type"] isEqualToString:@"0"]) {
                    model.isBrand = YES;
                }
            }
        }
        [self.rightDataArray  addObjectsFromArray:rightDataArray];
        if (model.typeId == 0) {
            [strongSelf.view sendSubviewToBack:strongSelf.rightCollectionView];
            [strongSelf.bannerCollectionView reloadData];
        }else{
            [strongSelf.view sendSubviewToBack:strongSelf.bannerCollectionView];
            [strongSelf.rightCollectionView reloadData];
        }
        [strongSelf.rightCollectionView reloadData];
        
    } failed:^(NSError *error) {
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [self.view showTipsWithError:error];
        [strongSelf.rightCollectionView showTipsWithError:error inCenterOfSuperView:strongSelf.view];
        [strongSelf.view hideProcessingView];
        
    }];

}
- (void)pageSeting{
    self.leftBgColor = [UIColor colorWithHexString:@"f6f6f6"];
    self.leftSelectBgColor = [UIColor whiteColor];
    self.leftSelectColor = [UIColor colorWithCustomerName:@"红"];
    self.leftUnSelectColor = [UIColor colorWithCustomerName:@"黑"];
    self.leftSeparatorColor = [UIColor colorWithCustomerName:@"分割线"];
    self.leftUnSelectBgColor = [UIColor hexChangeFloat:@"f6f6f6"];
    self.selectIndex = 0;
    
    self.view.backgroundColor = [UIColor whiteColor];
}

#pragma mark - <UITableViewDataSource>

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.zeroLevelDataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"MutilevelTableViewCell";
    MMHMultilevelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[MMHMultilevelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    MMHClassifiCationItemModel *model = self.zeroLevelDataArray[indexPath.row];
    cell.lable.text = model.name;
    UILabel *line = (UILabel *)[cell viewWithTag:100];
    if (indexPath.row == self.selectIndex) {
        cell.lable.textColor = self.leftSelectColor;
        cell.backgroundColor = self.leftSelectBgColor;
        line.backgroundColor = cell.backgroundColor;
    }else{
        cell.lable.textColor = self.leftUnSelectColor;
        cell.backgroundColor = self.leftUnSelectBgColor;
        line.backgroundColor = self.leftSeparatorColor;
    }
    return cell;
}

#pragma mark -<UITableViewDelegate>

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KLeftItemHeight;
}

- (void)cancelFisrtCell{
    NSIndexPath *IndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    if (self.leftTableView.delegate && [self.leftTableView.delegate respondsToSelector:@selector(tableView:didDeselectRowAtIndexPath:)]) {
        [self.leftTableView.delegate tableView:self.leftTableView didDeselectRowAtIndexPath:IndexPath];
    }
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self cancelFisrtCell];
    MMHMultilevelTableViewCell * cell=(MMHMultilevelTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.lable.textColor=self.leftSelectColor;
    cell.backgroundColor=self.leftSelectBgColor;
    self.selectIndex = indexPath.row;
    MMHClassifiCationItemModel *model = self.zeroLevelDataArray[indexPath.row];
    UILabel * line=(UILabel*)[cell viewWithTag:100];
    line.backgroundColor=[UIColor clearColor];
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    self.isReturnLastOffset = NO;
    [self.rightCollectionView reloadData];
    if (self.isRecordLastScroll) {
        [self.rightCollectionView scrollRectToVisible:CGRectMake(0, model.offsetScorller, self.rightCollectionView.frame.size.width, self.rightCollectionView.frame.size.height) animated:NO];
    }
    else{
        [self.rightCollectionView scrollRectToVisible:CGRectMake(0, 0, self.rightCollectionView.frame.size.width, self.rightCollectionView.frame.size.height) animated:NO];
    }
    
    //[self fetchSecondLevelDataWithTypeId:[model.typeId integerValue]];
    [self fetchRightCollectionViewDataWithModel:model];
    }
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    MMHMultilevelTableViewCell * cell=(MMHMultilevelTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.lable.textColor=self.leftUnSelectColor;
    UILabel * line=(UILabel*)[cell viewWithTag:100];
    line.backgroundColor=tableView.separatorColor;
    cell.backgroundColor=self.leftUnSelectBgColor;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.leftTableView) {
        SeparatorType separatorType = SeparatorTypeBottom;
        [cell addSeparatorLineWithType:separatorType];
        
    }
}

#pragma mark - <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.zeroLevelDataArray.count == 0) {
        return 0;
    }
    if (self.isHaveBanner) {
        return self.rightDataArray.count + 1;
    }else{
        return self.rightDataArray.count;
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.isHaveBanner) {
        if (section == 0) {
            return 1;
        }
         MMHClassifiCationItemModel *model = self.rightDataArray[section - 1];
        return model.data.count;
    }else{
        //从右边数据源中取出数据。
        MMHClassifiCationItemModel *model = self.rightDataArray[section];
        return model.data.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isHaveBanner) {
        if (indexPath.section == 0) {
            MMHBannerCell *banner = [collectionView dequeueReusableCellWithReuseIdentifier:@"banner" forIndexPath:indexPath];
            return banner;
        }
        MMHClassificationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KMMHmutilevelCollectionCell forIndexPath:indexPath];
        MMHClassifiCationItemModel *model = self.rightDataArray[indexPath.section - 1];
        cell.model = model.data[indexPath.row];
        
        return cell;
    }else {
        
        MMHClassificationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KMMHmutilevelCollectionCell forIndexPath:indexPath];
        MMHClassifiCationItemModel *model = self.rightDataArray[indexPath.section];
        cell.model = model.data[indexPath.row];
        return cell;
    }
}

#pragma mark - <UICollectionViewDelegate>
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    MMHClassifiCationItemModel *model = nil;
    //有没有banner
    if (self.isHaveBanner) {
        if (indexPath.section == 0) {
            //
        }else{
        model = self.rightDataArray[indexPath.section - 1];
        }
    
    }else{

        if (!self.rightDataArray.count == 0) {
            
            model = self.rightDataArray[indexPath.section];
        }
        
    }
    
    if (model == nil) {
        return;
    }
    
    MMHClassifiCationItemModel *itemModel = model.data[indexPath.row];
    //下一级界面要用的category
    if (model.isBrand) {
        MMHFilter *filter = [MMHFilter filterWithBrandID:itemModel.typeId];
        if (filter == nil) {
            return;
        }
        MMHProductListViewController *productListViewController = [[MMHProductListViewController alloc] initWithFilter:filter];
        productListViewController.title = itemModel.name;
        [self pushViewController:productListViewController];
    }
    else {
        MMHCategory *category = [[MMHCategory alloc] initWithCategoryID:itemModel.typeId];
        MMHFilter *filter = [[MMHFilter alloc] initWithCategory:category];
        MMHProductListViewController *productListViewController = [[MMHProductListViewController alloc] initWithFilter:filter];
        productListViewController.title = itemModel.name;
        [self pushViewController:productListViewController];
    }
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    MMHClassifiCationItemModel *model;
   
    if (self.isHaveBanner) {
        if (indexPath.section
            == 0) {
        
        }else{
            model = self.rightDataArray[indexPath.section - 1];
        }
    }else{
        model = self.rightDataArray[indexPath.section];
    }
     NSString *reuseIdentifier;
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        reuseIdentifier = @"footer";
        UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        view.backgroundColor = [UIColor whiteColor];
        return view;
    }else{
        reuseIdentifier = @"header";
        MMHClassificationCollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
        view.model = model;
        return view;
    }
   
}

/**
 返回collectionItem的大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isHaveBanner) {
        
        if (indexPath.section == 0) {
            return CGSizeMake(MMHFloat(265), MMHFloat(85));
        }
    }

    if ([collectionView isEqual:self.bannerCollectionView]) {
        return CGSizeMake(MMHFloat(75), MMHFloat(95));
    }
    return CGSizeMake(MMHFloat(75), MMHFloat(105));
}
/**
 设置item之间的间距
 */

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.isHaveBanner) {
        if (section == 0) {
            return UIEdgeInsetsMake(0, MMHFloat(10), 0, MMHFloat(10));
        }
    }
    return UIEdgeInsetsMake(10, MMHFloat(10), 0, MMHFloat(10));
   
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    MMHClassifiCationItemModel *model ;
    if (self.isHaveBanner) {
        if (section == 0) {

            return CGSizeZero;
        }else{
            model = self.rightDataArray[section - 1];
        }
    }else{
    
        model = self.rightDataArray[section];
    }
//    //有三级类目
//    if (model.data.count) {
//        
        NSString *str =@"栋巩巩我地据欧";
        CGSize size = [str sizeWithCalcFont:MMHFontOfSize(12.) constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        return size;
//    }else{
//        return CGSizeZero;
//    }
}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (self.isHaveBanner) {
        if (section == 0) {
            return CGSizeMake(kScreenWidth - MMHFloat(100), 20);
        }
    }
    return CGSizeMake(kScreenWidth - MMHFloat(100), 40);
}


#pragma mark - 记录滑动的坐标

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.rightCollectionView]) {
        
        self.isReturnLastOffset=YES;
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:self.rightCollectionView]) {
        MMHClassifiCationItemModel *model = self.zeroLevelDataArray[self.selectIndex];
        model.offsetScorller=scrollView.contentOffset.y;
        self.isReturnLastOffset=NO;
        
    }
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.rightCollectionView]) {
        
        MMHClassifiCationItemModel *model = self.zeroLevelDataArray[self.selectIndex];
        model.offsetScorller=scrollView.contentOffset.y;
        self.isReturnLastOffset = NO;
    }
    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:self.rightCollectionView] && self.isReturnLastOffset) {
        
        MMHClassifiCationItemModel *model = self.zeroLevelDataArray[self.selectIndex];
        model.offsetScorller=scrollView.contentOffset.y;
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
