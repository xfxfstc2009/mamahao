//
//  MMHCollectionReusableViewBanner.h
//  MamHao
//
//  Created by fishycx on 15/5/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHClassifiCationItemModel.h"
@interface MMHBannerCell : UICollectionViewCell
@property (nonatomic, strong)MMHClassifiCationItemModel *model;
@end
