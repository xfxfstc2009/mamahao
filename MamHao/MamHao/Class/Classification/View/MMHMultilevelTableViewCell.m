//
//  MMHMultilevelTableViewCell.m
//  MamHao
//
//  Created by 余传兴 on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMultilevelTableViewCell.h"



@interface MMHMultilevelTableViewCell ()



@end

@implementation MMHMultilevelTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, MMHFloat(89), MMHFloat(60))];
        self.lable = label;
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = MMHFontOfSize(15);
        [self addSubview:label];
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(kLeftItemWidht - 0.5, 0, 0.5, KLeftItemHeight)];
        line.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
        [self addSubview:line];
        line.tag = 100;
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.lable.text = title;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setZero{
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        self.layoutMargins=UIEdgeInsetsZero;
    }
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        self.separatorInset=UIEdgeInsetsZero;
    }
    
}
@end
