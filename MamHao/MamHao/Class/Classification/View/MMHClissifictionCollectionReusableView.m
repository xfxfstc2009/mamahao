//
//  MMHClissifictionCollectionReusableView.m
//  MamHao
//
//  Created by fishycx on 15/5/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHClissifictionCollectionReusableView.h"

@interface MMHClissifictionCollectionReusableView ()

@property (nonatomic, strong)UILabel *secondLevelItem;//二级类目

@end

@implementation MMHClissifictionCollectionReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        self.secondLevelItem = [[UILabel alloc] initWithFrame:CGRectZero];
        [self addSubview:_secondLevelItem];
    }
    return self;
}

- (void)setModel:(MMHClassifiCationItemModel *)model {
    _model = model;
    NSString *str = model.name;
  CGSize  itemSize = [str sizeWithCalcFont:MMHFontWithSize(12) constrainedToSize:CGSizeMake(MAXFLOAT, MAXFLOAT)];
    self.secondLevelItem.frame = CGRectMake(MMHFloat(10), 0, itemSize.width, itemSize.height);
    _secondLevelItem.font = MMHFontWithSize(12);
    self.secondLevelItem.text = str;
    
}
@end
