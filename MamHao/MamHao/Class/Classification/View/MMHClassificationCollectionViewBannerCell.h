//
//  MMHClassificationCollectionViewBannerCell.h
//  MamHao
//
//  Created by fishycx on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHClassifiCationItemModel.h"
@interface MMHClassificationCollectionViewBannerCell : UICollectionViewCell

@property(nonatomic, strong)MMHClassifiCationItemModel *model; //类目model

@end
