//
//  MMHCollectionReusableViewBanner.m
//  MamHao
//
//  Created by fishycx on 15/5/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHBannerCell.h"
#import "MMHImageView.h"
@interface MMHBannerCell ()
@property (nonatomic, strong)MMHImageView *bannerImageView; //banner
@end
@implementation MMHBannerCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.bannerImageView = [[MMHImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_bannerImageView];
    }
    return self;
}

- (void)setModel:(MMHClassifiCationItemModel *)model {
    _model = model;
    [self.bannerImageView updateViewWithImageAtURL:@""];
  
}
@end
