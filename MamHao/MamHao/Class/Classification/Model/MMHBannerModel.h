//
//  MMHBannerModel.h
//  MamHao
//
//  Created by fishycx on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHBannerModel : MMHFetchModel

@property (nonatomic, copy)NSString *pic;
@property (nonatomic, copy)NSString *url;
@end
