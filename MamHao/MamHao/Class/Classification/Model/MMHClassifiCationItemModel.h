//
//  MMHClassifiCationItem.h
//  MamHao
//
//  Created by 余传兴 on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"

@protocol MMHClassifiCationItemModel <NSObject>


@end

@interface MMHClassifiCationItemModel : MMHFetchModel

@property (nonatomic, copy)NSString *typeId;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *pic;
@property (nonatomic, copy)NSString *itemDescription;
@property (nonatomic, strong)NSArray<MMHClassifiCationItemModel> *data;
@property(assign,nonatomic) float offsetScorller;

@property (nonatomic) BOOL isBrand;

- (void)fetchSecondLevelDataSucceed:(void(^)(NSDictionary *dic))succeededHandler failed:(void(^)(NSError *error)) failedHandler;
@end
