//
//  MMHClassifiCationItem.m
//  MamHao
//
//  Created by 余传兴 on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHClassifiCationItemModel.h"
#import "MMHNetworkAdapter+Classification.h"
#import "MMHBannerModel.h"

@implementation MMHClassifiCationItemModel


- (void)setIsBrand:(BOOL)isBrand
{
    _isBrand = isBrand;
    
    for (MMHClassifiCationItemModel *item in self.data) {
        item.isBrand = isBrand;
    }
}


- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"decription": @"itemDecription"};
}

- (void)fetchSecondLevelDataSucceed:(void (^)(NSDictionary *dic))succeededHandler failed:(void (^)(NSError *))failedHandler {
    [[MMHNetworkAdapter sharedAdapter] fetchDataWithtypeId:[_typeId integerValue] succeededHandler:^(NSDictionary *dic) {
        succeededHandler(dic);
    } failedHandler:^(NSError *error) {
        
        failedHandler(error);
    }];

}
@end
