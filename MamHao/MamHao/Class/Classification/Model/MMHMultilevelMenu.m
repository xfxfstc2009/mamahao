//
//  MMHMultilevelMenu.m
//  MamHao
//
//  Created by 余传兴 on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMultilevelMenu.h"
#import "MMHMultilevelTableViewCell.h"
#import "MMHAssistant.h"
#define kleftWidth MMHFloat(90)
#define KitemHeight MMHFloat(60)
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
#define KMMHmutilevelCollectionCell @"MMHmutilevelCollectionCell"
@interface MMHMultilevelMenu ()

@property (nonatomic, strong) UITableView *leftTableView;
@property (nonatomic, strong) UICollectionView *rightCollectoinView;

@property (nonatomic, assign) BOOL isReturnLastOffset;

@property (nonatomic, assign) BOOL isTwoLevel;
@end
@implementation MMHMultilevelMenu

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame WithData:(NSArray *)data withSelectIndex:(void (^)(NSInteger, NSInteger, id))selectIndex{
    if (self = [super initWithFrame:frame]) {

        if (data.count == 0) {
            return nil;
        }
        _block = selectIndex;
        self.leftSelectColor=[UIColor colorWithCustomerName:@"红"];
        self.leftSelectBgColor=[UIColor whiteColor];
        self.leftBgColor=UIColorFromRGB(0xF3F4F6);
      //  self.leftSeparatorColor=UIColorFromRGB(0xE5E5E5);
        self.leftSeparatorColor = [UIColor colorWithCustomerName:@"分割线"];
        self.leftUnSelectBgColor=UIColorFromRGB(0xF3F4F6);
        self.leftUnSelectColor=[UIColor blackColor];
        _selectIndex = 0;
        _allData = data;
        /**
         *   左边的视图
         */
        self.leftTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kleftWidth, self.bounds.size.height - MMHFloat(64)) style:UITableViewStylePlain];
        self.leftTableView.dataSource = self;
        self.leftTableView.delegate = self;
        self.leftTableView.backgroundColor = self.leftSeparatorColor;
        self.leftTableView.tableFooterView = [[UIView alloc] init];
        self.leftTableView.backgroundColor=self.leftBgColor;
        if ([self.leftTableView respondsToSelector:@selector(setLayoutMargins:)]) {
            self.leftTableView.layoutMargins=UIEdgeInsetsZero;
        }
        if ([self.leftTableView respondsToSelector:@selector(setSeparatorInset:)]) {
            self.leftTableView.separatorInset=UIEdgeInsetsZero;
        }
        [self addSubview:self.leftTableView];
        
        
        /**
         *  右边的视图
         */
        
        float leftMargin = MMHFloat(10);
        
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];

        flowLayout.minimumInteritemSpacing = 10.f;
        flowLayout.minimumLineSpacing =10.f;
        
        self.rightCollectoinView = [[UICollectionView alloc] initWithFrame:CGRectMake(kleftWidth + leftMargin,0, kScreenWidth - kleftWidth - leftMargin*2, frame.size.height) collectionViewLayout:flowLayout];
       
        /**
         *  右边的banner
         */
//        UIImageView *banner = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, MMHFloat(265), MMHFloat(85))];
//        banner.image = [UIImage imageNamed:@"login_thirdLogin_weibo"];
//        banner.backgroundColor =[UIColor redColor];
//        [self.rightCollectoinView addSubview:banner];

        

        self.rightCollectoinView.delegate = self;
        self.rightCollectoinView.dataSource = self;
        [self.rightCollectoinView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:KMMHmutilevelCollectionCell];
        [self.rightCollectoinView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
        [self.rightCollectoinView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];
        [self addSubview:self.rightCollectoinView];
        
        /**
         *  选中某一行，置顶
         */
        if (self.allData.count>0) {
            [self.leftTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionTop];
        }
        self.isReturnLastOffset = YES;
        self.rightCollectoinView.backgroundColor = [UIColor whiteColor];
        self.backgroundColor = self.leftSelectBgColor;
    }
    return self;
}
-(void)setLeftBgColor:(UIColor *)leftBgColor{
    _leftBgColor=leftBgColor;
    self.leftTableView.backgroundColor=leftBgColor;
    
}
-(void)setLeftSelectBgColor:(UIColor *)leftSelectBgColor{
    
    _leftSelectBgColor=leftSelectBgColor;
  //  self.rightCollection.backgroundColor=leftSelectBgColor;
    
    self.backgroundColor=leftSelectBgColor;
    
}
-(void)setLeftSeparatorColor:(UIColor *)leftSeparatorColor{
    _leftSeparatorColor=leftSeparatorColor;
    self.leftTableView.separatorColor=leftSeparatorColor;
}


#pragma mark - UITableViewDataSource method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.allData.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"MutilevelTableViewCell";
    MMHMultilevelTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[MMHMultilevelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(kleftWidth - MMHFloat(1.0), 0, MMHFloat(1.0), KitemHeight)];
        label.backgroundColor = tableView.separatorColor;
        [cell addSubview:label];
        label.tag = 100;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    RightMenu *title = self.allData[indexPath.row];
    cell.title = title.meunName;
    cell.lable.text = title.meunName;
    UILabel *line = (UILabel *)[cell viewWithTag:100];
    if (indexPath.row == self.selectIndex) {
        cell.lable.textColor = self.leftSelectColor;
        cell.backgroundColor = self.leftSelectBgColor;
        line.backgroundColor = cell.backgroundColor;
    }else{
        cell.lable.textColor = self.leftUnSelectColor;
        cell.backgroundColor = self.leftUnSelectBgColor;
        line.backgroundColor = cell.backgroundColor;
    }
    return cell;
}

#pragma mark - UITableViewDelegate method
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return KitemHeight;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MMHMultilevelTableViewCell * cell=(MMHMultilevelTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.lable.textColor=self.leftSelectColor;
    cell.backgroundColor=self.leftSelectBgColor;
    _selectIndex=indexPath.row;
    RightMenu * title=self.allData[indexPath.row];
    UILabel * line=(UILabel*)[cell viewWithTag:100];
    line.backgroundColor=cell.backgroundColor;
    [tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    self.isReturnLastOffset = NO;
    [self.rightCollectoinView reloadData];
    if (self.isRecordLastScroll) {
        [self.rightCollectoinView scrollRectToVisible:CGRectMake(0, title.offsetScorller, self.rightCollectoinView.frame.size.width, self.rightCollectoinView.frame.size.height) animated:NO];
    }
    else{
        
        [self.rightCollectoinView scrollRectToVisible:CGRectMake(0, 0, self.rightCollectoinView.frame.size.width, self.rightCollectoinView.frame.size.height) animated:NO];
    }

    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    MMHMultilevelTableViewCell * cell=(MMHMultilevelTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    cell.lable.textColor=self.leftUnSelectColor;
    UILabel * line=(UILabel*)[cell viewWithTag:100];
    line.backgroundColor=tableView.separatorColor;
    cell.backgroundColor=self.leftUnSelectBgColor;

}

#pragma mark - UICollectionViewDataSource method

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.allData.count == 0) {
        return 0;
    }
    RightMenu *title = self.allData[self.selectIndex];
    return title.nextArray.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    RightMenu *title = self.allData[self.selectIndex];
    if (title.nextArray.count) {
        RightMenu *sub = title.nextArray[section];
        if (sub.nextArray.count == 0) {
            return 1;
        }else{
            return sub.nextArray.count;
        }
    }else{
        return title.nextArray.count;
    }
}
#pragma mark - UICollectionViewDelegate Mothod
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    MMHMultilevelTableViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KMMHmutilevelCollectionCell forIndexPath:indexPath];
    RightMenu *title = self.allData[self.selectIndex];
    NSArray *list;
    RightMenu *menu;
    menu = title.nextArray[indexPath.section];
    if (menu.nextArray.count > 0) {
        menu = title.nextArray[indexPath.section];
        list = menu.nextArray;
        menu = list[indexPath.row];
    }
    void(^select)(NSInteger left, NSInteger right,id info) = self.block;
    select(self.selectIndex, indexPath.row, menu);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:KMMHmutilevelCollectionCell forIndexPath:indexPath];
    RightMenu *title = self.allData[self.selectIndex];
    NSArray *list;
    RightMenu *menu;
    menu = title.nextArray[indexPath.section];
    if (menu.nextArray.count > 0) {
        menu = title.nextArray[indexPath.section];
        list = menu.nextArray;
        menu = list[indexPath.row];
    }
    cell.backgroundColor = [UIColor redColor];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier;
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        reuseIdentifier = @"footer";
    }else{
       reuseIdentifier = @"header";
    }
    RightMenu *title = self.allData[self.selectIndex];
    UICollectionReusableView *view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    UILabel *label = (UILabel *)[view viewWithTag:1];
    view.backgroundColor = [UIColor greenColor];
    label.font=[UIFont systemFontOfSize:15];
    label.textColor=UIColorFromRGB(0x686868);
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]){
        
        if (title.nextArray.count>0) {
            
            
            RightMenu * meun;
            meun=title.nextArray[indexPath.section];
            
            label.text=meun.meunName;
            
        }
        else{
            label.text=@"暂无";
        }
    }
    else if ([kind isEqualToString:UICollectionElementKindSectionFooter]){
        view.backgroundColor = [UIColor lightGrayColor];
        NSLog(@"22222%lf", view.frame.size.height);
        label.text = [NSString stringWithFormat:@"这是footer:%ld",(long)indexPath.section];
    }
    return view;
}

/**
返回collectionItem的大小
 */
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(MMHFloat(75), MMHFloat(105));
}
/**
设置item之间的间距
 */

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
   
    if (section == 0) {
        return UIEdgeInsetsMake(20, 10, 0, 10);
    }
    return UIEdgeInsetsMake(10, 10, 0, 10);
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (section == 0) {
          CGSize size={kScreenWidth - MMHFloat(100),MMHFloat(85)};
        return size;
    }
    NSString *str =@"栋巩巩根据欧";
    CGSize size = [str sizeWithCalcFont:MMHFontWithSize(12.) constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    return size;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    return CGSizeMake(kScreenWidth - MMHFloat(100), MMHFloat(40));
}


#pragma mark - 记录滑动的坐标

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.rightCollectoinView]) {
        
        
        self.isReturnLastOffset=YES;
    }

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:self.rightCollectoinView]) {
        
        RightMenu * title=self.allData[self.selectIndex];
        title.offsetScorller=scrollView.contentOffset.y;
        self.isReturnLastOffset=NO;
        
    }

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([scrollView isEqual:self.rightCollectoinView]) {
        
        RightMenu * title=self.allData[self.selectIndex];
        
        title.offsetScorller=scrollView.contentOffset.y;
        self.isReturnLastOffset=NO;
        
    }

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if ([scrollView isEqual:self.rightCollectoinView] && self.isReturnLastOffset) {
        RightMenu * title=self.allData[self.selectIndex];
        
        title.offsetScorller=scrollView.contentOffset.y;
        
        
    }
}

@end

@implementation RightMenu


@end


