//
//  MMHIntegralTableViewCell.h
//  MamHao
//
//  Created by fishycx on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHIntegralInfoModel.h"
@interface MMHIntegralTableViewCell : UITableViewCell

@property (nonatomic, strong)MMHIntegralInfoModel *model;
@end
