//
//  MMHCouponsCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHCouponsListModel.h"
@interface MMHCouponsCell : UITableViewCell

@property (nonatomic,strong)MMHCouponsSingleModel *couponsSingleModel;

@end
