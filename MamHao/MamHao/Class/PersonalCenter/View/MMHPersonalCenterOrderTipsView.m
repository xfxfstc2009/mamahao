//
//  MMHPersonalCenterOrderTipsView.m
//  MamHao
//
//  Created by fishycx on 15/7/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPersonalCenterOrderTipsView.h"

@interface MMHPersonalCenterOrderTipsView ()
@property (nonatomic, strong)UIImageView *backImageView;
@property (nonatomic, strong)UILabel *label;
@end

@implementation MMHPersonalCenterOrderTipsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        CGRect labelFrame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        self.backImageView = [[UIImageView alloc] initWithFrame:labelFrame];
        self.backImageView.image = [UIImage imageNamed:@"center_bg_point"];
        self.label = [[UILabel alloc] initWithFrame:labelFrame];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.font = [UIFont systemFontOfSize:9];
        self.label.textColor = [UIColor whiteColor];
        [self addSubview:self.backImageView];
        [self addSubview:self.label];
        
    }
    return self;
}
- (void)setTipsValue:(NSString*)tipsValue{
    self.label.text= tipsValue;
    if([tipsValue integerValue]== 0){
        [self setHidden:YES];
    }else{
        [self setHidden:NO];
    }
}
@end
