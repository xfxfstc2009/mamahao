//
//  MMHPersonalCenterOrderButton.m
//  MamHao
//
//  Created by fishycx on 15/7/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPersonalCenterOrderButton.h"

@implementation MMHPersonalCenterOrderButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)layoutSubviews{
    [super layoutSubviews];
    CGPoint center = self.imageView.center;
    center.x = self.frame.size.width/2;
    center.y = MMHFloat(21);
    self.imageView.center = center;
    
    //Center text
    CGRect newFrame = [self titleLabel].frame;
    newFrame.origin.x = 0;
    newFrame.origin.y = CGRectGetMaxY(self.imageView.frame) + MMHFloat(7);
    newFrame.size.width = self.frame.size.width;
    
    
 
    self.titleLabel.frame = newFrame;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
}
@end
