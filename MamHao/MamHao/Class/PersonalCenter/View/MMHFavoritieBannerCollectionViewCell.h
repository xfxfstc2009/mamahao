//
//  MMHFavoritiesCollectionViewCell.h
//  MamHao
//
//  Created by fishycx on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHFavouriteShopBrandModel.h"
@interface MMHFavoritieBannerCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong)MMHFavouriteShopBrandModel *model;
@property (nonatomic, assign)CellState cellstate;
@property (nonatomic, strong)UIImageView *stateIcon;



@end
