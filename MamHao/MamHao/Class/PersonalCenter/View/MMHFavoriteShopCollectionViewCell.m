//
//  MMHFavoriteShopCollectionViewCell.m
//  MamHao
//
//  Created by fishycx on 15/5/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFavoriteShopCollectionViewCell.h"
#import "UIImageView+WebCache.h"
@interface MMHFavoriteShopCollectionViewCell ()

@property (nonatomic, strong)UIImageView *imageView;
@property (nonatomic, strong)UILabel *shopName;
@property (nonatomic, strong)UILabel *addressLabel;
@property (nonatomic, strong)UIButton *distanceButton;

@end
@implementation MMHFavoriteShopCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_imageView];
        self.shopName = [[UILabel alloc] init];
        _shopName.font = [UIFont fontWithCustomerSizeName:@"正文"];
        _shopName.textColor = [UIColor colorWithCustomerName:@"黑"];
        [self.contentView addSubview:_shopName];
        self.addressLabel = [[UILabel alloc] init];
        _addressLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        _addressLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        [self.contentView addSubview:_addressLabel];
        self.distanceButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _distanceButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        _distanceButton.layer.borderWidth = 0.5;
        _distanceButton.layer.cornerRadius = 2;
        _distanceButton.layer.borderColor = [UIColor colorWithHexString:@"dcdcdc"].CGColor;
        [_distanceButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        [self.contentView addSubview:_distanceButton];
        self.stateIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - MMHFloat(7) - MMHFloat(18), MMHFloat(5), MMHFloat(18), MMHFloat(18))];
        
        [self.contentView addSubview:_stateIcon];
    }
    return self;
}

- (void)setShopModel:(MMHFavoritiesShop *)shopModel {
    _shopModel = shopModel;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:shopModel.shopImage]];
    self.imageView.frame = CGRectMake(MMHFloat(16), MMHFloat(14), MMHFloat(77), MMHFloat(77));
    
    self.shopName.text = shopModel.shopName;
    CGSize size = [shopModel.shopName sizeWithCalcFont:self.shopName.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.shopName.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame)+MMHFloat(16), MMHFloat(20), size.width, size.height);
    
    self.addressLabel.text = shopModel.address;
    CGSize addressSize = [shopModel.address sizeWithCalcFont:self.addressLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGFloat labelWidth = kScreenWidth -CGRectGetMinX(self.shopName.frame) - MMHFloat(16);
    self.addressLabel.frame = CGRectMake(CGRectGetMinX(self.shopName.frame), CGRectGetMaxY(self.shopName.frame)+MMHFloat(7), labelWidth, addressSize.height);
    
    [self.distanceButton setImage:[UIImage imageNamed:@"icon_store_locate"] forState:UIControlStateNormal];
    [self.distanceButton setTitle:[NSString stringWithFormat:@"距离您%@m", shopModel.distance] forState:UIControlStateNormal];
    CGSize  distanceButtonTitleSize = [self.distanceButton.titleLabel.text sizeWithCalcFont:self.distanceButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
     [self.distanceButton setImageEdgeInsets:UIEdgeInsetsMake(0, -MMHFloat(7), 0, 0)];
    [self.distanceButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, -MMHFloat(10))];
    self.distanceButton.frame = CGRectMake(CGRectGetMinX(self.addressLabel.frame), CGRectGetMaxY(self.addressLabel.frame)+MMHFloat(7), MMHFloat(7+18+5+10)+distanceButtonTitleSize.width, MMHFloat(24));
    
}

- (void)setCellstate:(CellState)cellstate {
    _cellstate = cellstate;
    if (cellstate == CellStateNormal) {
        self.stateIcon.image = [UIImage imageNamed:@""];
    }else if(cellstate == CellStateDesSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_gray"];
    }else if(cellstate == CellStateDidSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_s"];
        
    }
}


+(CGSize)defaultSize {
    return CGSizeMake((kScreenWidth - MMHFloat(20)), MMHFloat(105));
}
@end
