//
//  MMHFavoritiesCollectionViewCell.m
//  MamHao
//
//  Created by fishycx on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFavoritieBannerCollectionViewCell.h"
#import "MMHImageView.h"

@interface MMHFavoritieBannerCollectionViewCell ()

@property (nonatomic, strong)MMHImageView *bannerImageView;
@property (nonatomic, strong)UILabel *descriptionLabel;

@end
@implementation MMHFavoritieBannerCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        self.bannerImageView = [[MMHImageView alloc] initWithFrame:CGRectMake((self.bounds.size.width - MMHFloat(60)) / 2, MMHFloat(30), MMHFloat(60), MMHFloat(40))];
        [self.contentView addSubview:_bannerImageView];
        self.descriptionLabel = [[UILabel alloc] init];
        _descriptionLabel.textAlignment = NSTextAlignmentCenter;
        _descriptionLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        _descriptionLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        [self.contentView addSubview:_descriptionLabel];
        self.stateIcon = [[UIImageView alloc] initWithFrame:CGRectMake(self.bounds.size.width - MMHFloat(7) - MMHFloat(18), MMHFloat(5), MMHFloat(18), MMHFloat(18))];
      
        [self.contentView addSubview:_stateIcon];
    
    }
    return self;
}

- (void)setModel:(MMHFavouriteShopBrandModel *)model {
    _model = model;
    CGSize descriPtionLabelSize = [model.b_name sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.descriptionLabel.frame = CGRectMake(0, CGRectGetMaxY(self.bannerImageView.frame)+MMHFloat(24), self.bounds.size.width, descriPtionLabelSize.height);
    [self.bannerImageView updateViewWithImageAtURL:model.b_logo];
    
    self.descriptionLabel.text = model.b_name;
}
- (void)setCellstate:(CellState)cellstate {
    _cellstate = cellstate;
    if (cellstate == CellStateNormal) {
        self.stateIcon.image = [UIImage imageNamed:@""];
    }else if(cellstate == CellStateDesSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_gray"];
    }else if(cellstate == CellStateDidSelected){
        self.stateIcon.image = [UIImage imageNamed:@"btn_select_s"];
        
    }
}


@end
