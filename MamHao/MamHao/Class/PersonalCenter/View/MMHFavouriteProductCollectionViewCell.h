//
//  MMHFavouriteCollectionViewCell.h
//  MamHao
//
//  Created by fishycx on 15/7/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFavouriteProductModel.h"
@interface MMHFavouriteProductCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) MMHFavouriteProductModel *product;
@property (nonatomic, assign) CellState cellstate;
@property (nonatomic, strong) UIImageView *stateIcon;
+ (CGSize)defaultSize;

@end
