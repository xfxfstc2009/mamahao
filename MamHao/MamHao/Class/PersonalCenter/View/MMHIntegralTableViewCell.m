//
//  MMHIntegralTableViewCell.m
//  MamHao
//
//  Created by fishycx on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHIntegralTableViewCell.h"
#import "MMHImageView.h"
@interface MMHIntegralTableViewCell()

@property (nonatomic, strong)MMHImageView *icon;
@property (nonatomic, strong)UILabel *title;
@property (nonatomic, strong)UILabel *time;
@property (nonatomic, strong)UILabel *subIngegral;
@end
@implementation MMHIntegralTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.icon = [[MMHImageView alloc] init];
        self.title = [[UILabel alloc] init];
        _title.font = F4;
        _title.textColor = C6;
        self.time = [[UILabel alloc] init];
        _time.font = F2;
        _time.textColor = C4;
        self.subIngegral = [[UILabel alloc] init];
        _subIngegral.font = F4;
        _subIngegral.textColor = C6;
        [self.contentView addSubview:_title];
        [self.contentView addSubview:_icon];
        [self.contentView addSubview:_time];
        [self.contentView addSubview:_subIngegral];
    }
    return self;
}

- (void)setModel:(MMHIntegralInfoModel *)model {
    _model = model;
    [self.icon updateViewWithImageAtURL:model.itemPic];
    switch (model.point_change_type) {
        case 0:
            self.title.text = @"线上购物抵用";
            break;
        case 1:
            self.title.text = @"线上购物抵用";
            break;
        default:
            break;
    }
    self.time.text = model.create_time;
    NSString *operator;
    if (model.math_operator == 0) {
        operator = @"+";
    }else{
      operator = @"-";
    }
    self.subIngegral.text = [NSString stringWithFormat:@"%@%@",operator,model.point_num];

    self.icon.frame = CGRectMake(MMHFloat(10), 15, 55, 55);
    
    CGSize titleSize = [self.title.text sizeWithCalcFont:self.title.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGSize timeSize = [model.create_time sizeWithCalcFont:self.time.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGSize integralSize = [self.subIngegral.text sizeWithCalcFont:self.subIngegral.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGFloat titleY = (55 - titleSize.height - 12 - timeSize.height)/2+CGRectGetMinY(self.icon.frame);
    self.title.frame = CGRectMake(CGRectGetMaxX(self.icon.frame)+MMHFloat(10), titleY, titleSize.width, titleSize.height);
    
    CGFloat timeY = CGRectGetMaxY(self.title.frame)+12;
    self.time.frame = CGRectMake(CGRectGetMinX(self.title.frame), timeY, timeSize.width, timeSize.height);
    
    CGFloat integralY = (85 - integralSize.height)/2;
    self.subIngegral.frame = CGRectMake(kScreenWidth - MMHFloat(10) - integralSize.width, integralY, integralSize.width, integralSize.height);

}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
