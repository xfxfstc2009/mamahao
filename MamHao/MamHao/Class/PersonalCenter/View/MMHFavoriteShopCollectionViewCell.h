//
//  MMHFavoriteShopCollectionViewCell.h
//  MamHao
//
//  Created by fishycx on 15/5/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHFavoritiesShop.h"

@interface MMHFavoriteShopCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong)MMHFavoritiesShop *shopModel;
@property (nonatomic, assign)CellState cellstate;
@property (nonatomic, strong)UIImageView *stateIcon;

+(CGSize)defaultSize;

@end
