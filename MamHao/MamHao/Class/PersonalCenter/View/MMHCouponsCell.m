//
//  MMHCouponsCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCouponsCell.h"

@interface MMHCouponsCell(){
    UIImageView *typeImageView;
    UILabel *moneyLabel;
    UILabel *couponseConditionLabel;
    UILabel *beginTimeLabel;
    UILabel *endTimeLabel;
    UILabel *applicableLabel;
    UIImageView *lineImageView;
    UILabel *fixedMoneyLabel;
    UILabel *fixedCouponseLabel;
    UILabel *fixedValidityPeriodLabel;
}
@end

@implementation MMHCouponsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 1. 创建 $
    fixedMoneyLabel = [[UILabel alloc]init];
    fixedMoneyLabel.backgroundColor = [UIColor clearColor];
    fixedMoneyLabel.textColor = [UIColor whiteColor];
    fixedMoneyLabel.stringTag = @"fixedMoneyLabel";
    fixedMoneyLabel.text = @"￥";
    fixedMoneyLabel.font = [UIFont systemFontOfSize:18];
    CGSize fixedMoneyLabelSize = [@"￥" sizeWithCalcFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    fixedMoneyLabel.frame = CGRectMake(MMHFloat(20), MMHFloat(20), fixedMoneyLabelSize.width, 20);
    [self addSubview:fixedMoneyLabel];
    
    // 2. 创建moneyLabel
    moneyLabel = [[UILabel alloc]init];
    moneyLabel.backgroundColor = [UIColor clearColor];
    moneyLabel.font = [UIFont systemFontOfSize:40];
    moneyLabel.stringTag = @"moneyLabel";
    CGFloat moneyLabelHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:40]];
    CGSize moneyLabelSize = [@"200" sizeWithCalcFont:[UIFont systemFontOfSize:40] constrainedToSize:CGSizeMake(CGFLOAT_MAX, moneyLabelHeight)];
    moneyLabel.frame = CGRectMake(CGRectGetMaxX(fixedMoneyLabel.frame), 20, moneyLabelSize.width, moneyLabelSize.height - 2 * 5);

    moneyLabel.textColor = [UIColor whiteColor];
    // 重置￥
    fixedMoneyLabel.orgin_y = CGRectGetMaxY(moneyLabel.frame) - 20;
    [self addSubview:moneyLabel];
    
    // 3. 创建优惠券fixedLabel
    fixedCouponseLabel = [[UILabel alloc]init];
    fixedCouponseLabel.backgroundColor = [UIColor clearColor];
    fixedCouponseLabel.font = [UIFont systemFontOfSize:12];
    fixedCouponseLabel.text = @"优惠券";
    fixedCouponseLabel.textColor = [UIColor whiteColor];
    CGFloat fixedCouponseHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:12]];
    fixedCouponseLabel.frame = CGRectMake(CGRectGetMaxX(moneyLabel.frame), 20, 50, fixedCouponseHeight);
    fixedCouponseLabel.stringTag = @"fixedCouponseLabel";
    [self addSubview:fixedCouponseLabel];
    
    // 创建优惠条件
    couponseConditionLabel = [[UILabel alloc]init];
    couponseConditionLabel.backgroundColor = [UIColor clearColor];
    couponseConditionLabel.stringTag = @"couponseConditaionLabel";
    couponseConditionLabel.textColor = [UIColor whiteColor];
    couponseConditionLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:couponseConditionLabel];
    
    // 创建line
    lineImageView = [[UIImageView alloc]init];
    lineImageView.backgroundColor = [UIColor clearColor];
    lineImageView.stringTag = @"lineImageView";
    [self addSubview:lineImageView];
    
    // 创建使用label
    applicableLabel = [[UILabel alloc]init];
    applicableLabel.backgroundColor = [UIColor clearColor];
    applicableLabel.textColor = [UIColor whiteColor];
    applicableLabel.font = [UIFont systemFontOfSize:10];
    
    [self addSubview:applicableLabel];
    
    // 创建有效期label
    fixedValidityPeriodLabel = [[UILabel alloc]init];
    fixedValidityPeriodLabel.backgroundColor = [UIColor clearColor];
    fixedValidityPeriodLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    fixedValidityPeriodLabel.textAlignment = NSTextAlignmentCenter;
   
    fixedValidityPeriodLabel.frame = CGRectMake(MMHFloat(227), fixedCouponseLabel.frame.origin.y, MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);
    fixedValidityPeriodLabel.text = @"有效期";

    [self addSubview:fixedValidityPeriodLabel];
    
    // 创建失效时间label
    beginTimeLabel = [[UILabel alloc]init];
    beginTimeLabel.backgroundColor = [UIColor clearColor];
    beginTimeLabel.textAlignment = NSTextAlignmentCenter;
    beginTimeLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    beginTimeLabel.font = [UIFont systemFontOfSize:13];
    beginTimeLabel.frame = CGRectMake(MMHFloat(227) , CGRectGetMaxY(fixedValidityPeriodLabel.frame) + MMHFloat(10), MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);

    [self addSubview:beginTimeLabel];
    
    // 创建endTimeLabel
    endTimeLabel = [[UILabel alloc]init];
    endTimeLabel.backgroundColor = [UIColor clearColor];
    endTimeLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    endTimeLabel.textAlignment = NSTextAlignmentCenter;
    endTimeLabel.font = [UIFont systemFontOfSize:13];
    [self addSubview:endTimeLabel];
    
    // 创建失效imageView
    typeImageView = [[UIImageView alloc]init];
    typeImageView.backgroundColor = [UIColor clearColor];

    typeImageView.frame = CGRectMake(kScreenBounds.size.width - 10 - 84, 18.5, 84, 73);
    [self addSubview:typeImageView];
}

-(void)setCouponsSingleModel:(MMHCouponsSingleModel *)couponsSingleModel{
    _couponsSingleModel = couponsSingleModel;
    
    if (couponsSingleModel.status == 1){           // 没有适用过
        typeImageView.image = [UIImage imageNamed:@""];
    } else if (couponsSingleModel.status == 2){    // 已使用
        typeImageView.image = [UIImage imageNamed:@"coupons_img_used"];
    } else if (couponsSingleModel.status == 3){    // 已失效
        typeImageView.image = [UIImage imageNamed:@"coupons_img_expired"];
    }
    
    moneyLabel.text = [NSString stringWithFormat:@"%li",(long)couponsSingleModel.voucherAmount];
    CGFloat moneyLabelHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:40]];
    CGSize moneyLabelSize = [moneyLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:40] constrainedToSize:CGSizeMake(CGFLOAT_MAX, moneyLabelHeight)];
    moneyLabel.frame = CGRectMake(CGRectGetMaxX(fixedMoneyLabel.frame), 20, moneyLabelSize.width, moneyLabelSize.height - 2 * 5);
    moneyLabel.textColor = [UIColor whiteColor];
    // 重置￥
    fixedMoneyLabel.orgin_y = CGRectGetMaxY(moneyLabel.frame) - 20;
    endTimeLabel.frame =CGRectMake(MMHFloat(227), CGRectGetMaxY(beginTimeLabel.frame)+ MMHFloat(10), MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);
    

    CGFloat fixedCouponseHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:13]];
    fixedCouponseLabel.frame = CGRectMake(CGRectGetMaxX(moneyLabel.frame), 20, 50, fixedCouponseHeight);
    fixedValidityPeriodLabel.frame = CGRectMake(MMHFloat(227), fixedCouponseLabel.frame.origin.y, MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);
    fixedValidityPeriodLabel.text = @"有效期";
    
    beginTimeLabel.frame = CGRectMake(MMHFloat(227) , CGRectGetMaxY(fixedValidityPeriodLabel.frame) + MMHFloat(10), MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);
    endTimeLabel.frame = CGRectMake(MMHFloat(227), CGRectGetMaxY(beginTimeLabel.frame)+MMHFloat(10), MMHFloat(128), [MMHTool contentofHeight:[UIFont systemFontOfSize:13]]);
    
    couponseConditionLabel.text = [NSString stringWithFormat:@"满%li元可用", (long)couponsSingleModel.voucherMoney];
    lineImageView.frame = CGRectMake(MMHFloat(10), CGRectGetMaxY(moneyLabel.frame) + MMHFloat(10), MMHFloat(227) - 2 * MMHFloat(10), .5f);
    CGFloat couponseConditionLabelHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:15]];
    couponseConditionLabel.frame = CGRectMake(CGRectGetMaxX(moneyLabel.frame), CGRectGetMaxY(moneyLabel.frame) - couponseConditionLabelHeight, MMHFloat(227) - CGRectGetMaxX(moneyLabel.frame) - MMHFloat(10), couponseConditionLabelHeight);

    beginTimeLabel.text = couponsSingleModel.beginDate;
    
    endTimeLabel.text = couponsSingleModel.endDate;
    
    // line
    if (couponsSingleModel.status == 1){
        lineImageView.image = [UIImage imageNamed:@"coupons_img_segmentation-line"];
    } else {
        lineImageView.image = [UIImage imageNamed:@"coupons_img_segmentation-whiteline"];
    }
    
    // 适用范围
    applicableLabel.text = [NSString stringWithFormat:@"适用:%@",couponsSingleModel.apply];
    CGFloat applicableLabelSingleHeight = [MMHTool contentofHeight:[UIFont systemFontOfSize:10]];
    CGSize applicableSize = [applicableLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(MMHFloat(187), CGFLOAT_MAX)];
    if (applicableLabelSingleHeight == applicableSize.height){
        applicableLabel.numberOfLines = 1;
        applicableLabel.frame = CGRectMake(MMHFloat(20), CGRectGetMaxY(lineImageView.frame) + MMHFloat(10), MMHFloat(187), [MMHTool contentofHeight:[UIFont systemFontOfSize:10]]);
    } else {
        applicableLabel.numberOfLines = 2;
        applicableLabel.frame = CGRectMake(MMHFloat(20), CGRectGetMaxY(lineImageView.frame) + MMHFloat(5), MMHFloat(187), applicableSize.height);
    }
}

@end
