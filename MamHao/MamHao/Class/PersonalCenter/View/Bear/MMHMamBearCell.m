//
//  MMHMamBearCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamBearCell.h"

@interface MMHMamBearCell(){
    MMHImageView *headerView;
    UILabel *titleLabel;                /**< titleLabel*/
    UILabel *orderLabel;
    UILabel *timeLabel;
    UILabel *detailLabel;               /**< 明细label*/
}

@end

@implementation MMHMamBearCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark CreateView
-(void)createView{
//    headerView = [[MMHImageView alloc]init];
//    headerView.backgroundColor = [UIColor clearColor];
//    headerView.frame = CGRectMake(MMHFloat(10), MMHFloat(15), MMHFloat(70), MMHFloat(70));
//    headerView.layer.cornerRadius = MMHFloat(6);
//    headerView.layer.borderWidth = .5f;
//    headerView.backgroundColor = [UIColor clearColor];
//    headerView.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
//    [self addSubview:headerView];
    
    // 创建商品title
    titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    titleLabel.frame = CGRectMake(MMHFloat(10), MMHFloat(15), MMHFloat(200), [MMHTool contentofHeight:titleLabel.font]);
    [self addSubview:titleLabel];
    
    // 订单号
    orderLabel = [[UILabel alloc]init];
    orderLabel.backgroundColor = [UIColor clearColor];
    orderLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    orderLabel.textAlignment = NSTextAlignmentLeft;
    orderLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    orderLabel.frame = CGRectMake(titleLabel.frame.origin.x, CGRectGetMaxY(titleLabel.frame) + MMHFloat(7), MMHFloat(200), titleLabel.bounds.size.height);
    [self addSubview:orderLabel];
    
    // 时间
    timeLabel = [[UILabel alloc]init];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    timeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    timeLabel.frame = CGRectMake(orderLabel.frame.origin.x, CGRectGetMaxY(orderLabel.frame) + MMHFloat(8), MMHFloat(200), titleLabel.bounds.size.height);
    [self addSubview:timeLabel];
    
    // 创建detailLabel
    detailLabel = [[UILabel alloc]init];
    detailLabel.backgroundColor = [UIColor clearColor];
    detailLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    detailLabel.textAlignment = NSTextAlignmentRight;
    detailLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    [self addSubview:detailLabel];
}

#pragma mark Model
-(void)setBearSingleModel:(MMHBearSingleModel *)bearSingleModel{
    _bearSingleModel = bearSingleModel;
    
    // imageView
    [headerView updateViewWithImageAtURL:bearSingleModel.relativePic];
    
    // title
//    if (bearSingleModel.beanType == MMHBeanTypeShop){
//        titleLabel.text = @"商品购物";
//    } else if (bearSingleModel.beanType == MMHBeanTypeShow){
//        titleLabel.text = @"评价晒单";
//    } else if (bearSingleModel.beanType == MMHBeanTypeGame){
//        titleLabel.text = @"游戏获得";
//    } else if (bearSingleModel.beanType == MMHBeanTypeSign){
//        titleLabel.text = @"签到";
//    } else if (bearSingleModel.beanType == MMHBeanTypeMortgage){
//        titleLabel.text = @"商品抵扣";
//    } else if (bearSingleModel.beanType == MMHBeanTypeRefund){
//        titleLabel.text = @"退货扣除";
//    } else if (bearSingleModel.beanType == MMHBeanTypeOther){
//        titleLabel.text = @"其他";
//    }
    titleLabel.text = bearSingleModel.titleShow;
    orderLabel.text = bearSingleModel.relativeNo.length?[NSString stringWithFormat:@"订单号:%@",bearSingleModel.relativeNo]:@" ";
    
    timeLabel.text = bearSingleModel.createDate;
    
    if (bearSingleModel.mathOperator) {
        detailLabel.textColor = C21;
    }else{
        detailLabel.textColor = [UIColor colorWithHexString:@"477ed8"];
    }
    detailLabel.text = bearSingleModel.mbeanNumShow;
    CGSize detailContentSize = [bearSingleModel.mbeanNumShow sizeWithCalcFont:detailLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(100))];
    detailLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - detailContentSize.width, 0, detailContentSize.width, MMHFloat(100));
}
@end
