//
//  MMHMamBearCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHBearSingleModel.h"
@interface MMHMamBearCell : UITableViewCell

@property (nonatomic,strong)MMHBearSingleModel *bearSingleModel;
@end
