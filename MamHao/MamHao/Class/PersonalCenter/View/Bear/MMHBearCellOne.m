//
//  MMHBearCellOne.m
//  MamHao
//
//  Created by fishycx on 15/7/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHBearCellOne.h"


@interface MMHBearCellOne (){
    UILabel *fixedLabel;
    UILabel *dymicLabel;
    UIImageView *arrawImageView;
}

@end
@implementation MMHBearCellOne

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.stringTag = @"fixedLabel";
        fixedLabel.frame = CGRectMake(MMHFloat(10), 0, 150, self.height);
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        fixedLabel.textAlignment = NSTextAlignmentLeft;
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        fixedLabel.text = @"妈豆交易明细";
        [self.contentView addSubview:fixedLabel];
        
        // 创建
        dymicLabel = [[UILabel alloc]init];
        dymicLabel.backgroundColor = [UIColor clearColor];
        dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        CGSize dymicSize = [@"妈豆说明" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.height)];
        dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(10) - MMHFloat(8) - dymicSize.width, 0, dymicSize.width, self.height);
        dymicLabel.textAlignment = NSTextAlignmentRight;
        dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        [self.contentView addSubview:dymicLabel];
        dymicLabel.text = @"妈豆说明";
        
        // 创建arrow
       arrawImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
        arrawImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(10), (MMHFloat(40) - MMHFloat(18))/2., MMHFloat(10), MMHFloat(18));
        [self.contentView addSubview:arrawImageView];
    }
    return self;
}
@end
