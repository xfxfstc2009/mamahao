//
//  MMHIntegralModel.h
//  MamHao
//
//  Created by fishycx on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//  积分界面数据

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"
#import "MMHIntegralInfoModel.h"
@interface MMHIntegralModel : MMHFetchModel

@property (nonatomic, copy)NSString *totalPoint;
@property (nonatomic, copy)NSDictionary *page;
@property (nonatomic, copy)NSString *pageSize;
@property (nonatomic, copy)NSArray<MMHIntegralInfoModel>*list;

@end
