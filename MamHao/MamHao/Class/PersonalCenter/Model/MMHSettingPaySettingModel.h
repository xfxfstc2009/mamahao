//
//  MMHSettingPaySettingModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHSettingPaySettingModel : MMHFetchModel

@property (nonatomic,assign)NSInteger isSetted;             /**< 用户支付密码是否设置*/
@property (nonatomic,copy)NSString *phone;                  /**< 电话号码*/
@property (nonatomic,assign)BOOL isMaxVisit;                /**< 是否达到最大次数*/
@property (nonatomic,copy)NSString *timeSpan;               /**< 小时数*/
@property (nonatomic,copy)NSString *maxVisit;               /**< 限制最大次数*/

@end
