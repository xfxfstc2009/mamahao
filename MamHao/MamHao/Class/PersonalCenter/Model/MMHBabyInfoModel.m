//
//  MMHBabyInfoModel.m
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHBabyInfoModel.h"

@implementation MMHBabyInfoModel

+ (MMHBabyInfoModel *)sharedBabyInfo {
    static MMHBabyInfoModel *babyInfo = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        babyInfo = [[MMHBabyInfoModel alloc] init];
    });
    return babyInfo;
}

@end
