//
//  MMHFavouriteProductModel.m
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFavouriteProductModel.h"
#import "MMHNetworkAdapter+Center.h"
@implementation MMHFavouriteProductModel


- (instancetype)initWithJSONDict:(NSDictionary *)dict{
    if (self = [super initWithJSONDict:dict]) {
        if ([dict hasKey:@"styleNumId"]) {
            self.templateId = dict[@"styleNumId"];
        }
        else if ([dict hasKey:@"STYLE_NUM_ID"]) {
            self.templateId = dict[@"STYLE_NUM_ID"];
        }
    }
    return self;
}
- (MMHProductModule)module{
    return MMHProductModuleFavourite;
}

- (BOOL)bindsShop{
    return NO;
}
- (MMHProductDetailSaleType)productDetailSaleType{
    return  MMHProductDetailSaleTypeRMB;
}

- (BOOL)isBeanProduct{
    return NO;
}

-(NSDictionary *)parameters{
    
    if (self.module != MMHProductModuleFavourite) {
        return @{};
    }
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"templateId"] = self.templateId;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"deviceId"] = [UIDevice deviceID];
    NSString *jsonTerm = [dictionary JSONString];
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}

@end
