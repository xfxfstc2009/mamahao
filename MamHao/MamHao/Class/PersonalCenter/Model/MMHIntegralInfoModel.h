//
//  MMHIntegralInfoModel.h
//  MamHao
//
//  Created by fishycx on 15/6/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//  积分使用情况具体信息

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"

@class MMHIntegralInfoModel;
@protocol MMHIntegralInfoModel <NSObject>
@end
@interface MMHIntegralInfoModel : MMHFetchModel
@property (nonatomic, copy)NSString *related_no;
@property (nonatomic, copy)NSString *des;
@property (nonatomic, assign)NSInteger math_operator;
@property (nonatomic, copy)NSString *point_num;
@property (nonatomic, assign)NSInteger point_change_type;
@property (nonatomic, copy)NSString *create_time;
@property (nonatomic, copy)NSString *related_item_id;
@property (nonatomic, assign)NSInteger point_shop_type;
@property (nonatomic, copy)NSString *itemPic;
@property (nonatomic, copy)NSString *point_id;
@property (nonatomic, copy)NSString *member_id;

@end
