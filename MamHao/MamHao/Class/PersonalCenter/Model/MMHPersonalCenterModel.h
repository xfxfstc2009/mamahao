//
//  MMHPersonalCenterModel.h
//  MamHao
//
//  Created by fishycx on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"

@interface MMHPersonalCenterModel : MMHFetchModel


@property (nonatomic, assign)NSInteger brandId;

@property (nonatomic, strong)NSString *brandLogo;     //图片名字

@property (nonatomic, strong)NSString *brandName;     //name

@property (nonatomic, assign)NSInteger collectId; //收藏id

@end
