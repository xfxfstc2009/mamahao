//
//  MMHFavoritiesShop.h
//  MamHao
//
//  Created by fishycx on 15/5/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHFavoritiesShop : MMHFetchModel

@property (nonatomic ,strong)NSString *address;//门店地址
@property (nonatomic, strong)NSString *shopImage;
@property (nonatomic, strong)NSString *shopName;//店铺内容
@property (nonatomic, strong)NSString *distance;
@end
