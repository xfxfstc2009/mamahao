//
//  MMHFavouriteProductModel.h
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@interface MMHFavouriteProductModel : MMHProduct<MMHProductProtocol>
@property (nonatomic, copy)NSString *ISHOT;
@property (nonatomic, copy)NSString *STYLE_APPLY_AGE;
@property (nonatomic, copy)NSString *ITEM_NAME;         //规格名
@property (nonatomic, copy)NSString *collect_id;         //收藏的唯一ID
@property (nonatomic, copy)NSString *ONLINE_TIME;
@property (nonatomic, copy)NSString *TIME_NAME;
@property (nonatomic, copy)NSString *BRAND_ID;          //品牌ID
@property (nonatomic, copy)NSString *STYLE_NUM_ID;      //模板id
//@property (nonatomic, copy)NSString *hasOwnPrice;       //有自定义价格
@property (nonatomic, copy)NSString *MIN_PRICE;         //最小价格
@property (nonatomic, copy)NSString *DIV_NUM_ID;        //事业部编号
@property (nonatomic, copy)NSString *areaId;            //区域ID
@property (nonatomic, copy)NSString *styleNumId;        //(同STYLE_NUM_ID)
@property (nonatomic, copy)NSString *cityNumId;         //城市ID
@property (nonatomic, copy)NSString *PIC;               //商品主图
@property (nonatomic, copy)NSString *TOTAL_SALE;        //总销量
@end
