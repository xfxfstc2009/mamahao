//
//  MMHCouponsListModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHCouponsSingleModel.h"
@interface MMHCouponsListModel : MMHFetchModel

@property (nonatomic,strong)NSArray<MMHCouponsSingleModel> *vouchers;
@end
