//
//  MMHPersonalCenterInfoModel.h
//  MamHao
//
//  Created by fishycx on 15/7/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFetchModel.h"
@interface MMHPersonalCenterInfoModel :MMHFetchModel

@property (nonatomic, copy)NSString *mbeanCount;           //**<麻豆*/
@property (nonatomic, copy)NSString *refundOrderCount;     //**<退换货*/
@property (nonatomic, copy)NSString *voucherCount;         //**<优惠券*/
@property (nonatomic, copy)NSString *waitCommentOrderCount;//**<待评论*/
@property (nonatomic, copy)NSString *waitDeliverOrderCount;//**<待发货*/
@property (nonatomic, copy)NSString *waitPayOrderCount;    //**<待支付*/
@property (nonatomic, copy)NSString *waitReceiveOrderCount;//**<待收货*/
@end
