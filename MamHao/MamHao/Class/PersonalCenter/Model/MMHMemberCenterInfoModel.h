//
//  MMHMemberCenterInfoModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHMemberCenterInfoModel : MMHFetchModel

@property (nonatomic,copy)NSString *headPic;            /**< 妈妈头像*/
@property (nonatomic,copy)NSString *nickName;           /**< 妈妈昵称*/
@property (nonatomic,copy)NSString *phone;              /**< 电话*/
@property (nonatomic,assign)NSInteger   status;         /**< 妈妈状态0 未选择，1 孕妈，2 宝妈*/

@property (nonatomic, copy) NSString *memberId;

@end
