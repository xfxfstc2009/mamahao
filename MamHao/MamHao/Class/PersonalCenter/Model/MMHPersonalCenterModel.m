//
//  MMHPersonalCenterModel.m
//  MamHao
//
//  Created by fishycx on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPersonalCenterModel.h"

@implementation MMHPersonalCenterModel

- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"brandId":@"b_id", @"brandLogo":@"b_logo", @"brandName":@"b_name", @"collectId":@"collect_id"};
}
@end
