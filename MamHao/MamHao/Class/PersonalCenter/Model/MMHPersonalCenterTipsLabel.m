//
//  MMHPersonalCenterTipsLabel.m
//  MamHao
//
//  Created by fishycx on 15/7/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPersonalCenterTipsLabel.h"

@implementation MMHPersonalCenterTipsLabel
- (id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super initWithCoder:aDecoder]) {
        
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.layer.cornerRadius = frame.size.height;
        self.backgroundColor =[UIColor redColor];
    }
    return self;
}
- (void)setValue:(NSInteger)value{
    self.text = [NSString stringWithFormat:@"%ld", value];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
