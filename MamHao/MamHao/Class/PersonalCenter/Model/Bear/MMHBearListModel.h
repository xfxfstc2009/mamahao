//
//  MMHBearListModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHBearSingleModel.h"


@interface MMHBearListModel : MMHFetchModel


@property (nonatomic,assign)NSInteger page;
@property (nonatomic,assign)NSInteger pageSize;
@property (nonatomic,assign)NSInteger mbeanCount;
@property (nonatomic,strong)NSArray<MMHBearSingleModel>*mbeans;         /**< 妈豆明细*/
@end
