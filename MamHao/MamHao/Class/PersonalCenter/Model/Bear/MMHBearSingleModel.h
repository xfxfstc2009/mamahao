//
//  MMHBearSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
//0商品购物，1评价晒单，2游戏，3签到，4商品抵扣，5退货扣除，6其他
typedef NS_ENUM(NSInteger, MMHBeanType) {
    MMHBeanTypeShop = 0,            /**< 商品购物*/
    MMHBeanTypeShow = 1,            /**< 评价晒单*/
    MMHBeanTypeGame = 2,            /**< 游戏*/
    MMHBeanTypeSign = 3,            /**< 签到*/
    MMHBeanTypeMortgage = 4,        /**< 商品抵扣*/
    MMHBeanTypeRefund = 5,          /**< 退货扣除*/
    MMHBeanTypeOther = 6,           /**< 其他*/
};

@protocol MMHBearSingleModel <NSObject>


@end

@interface MMHBearSingleModel : MMHFetchModel

@property (nonatomic,copy)NSString *mbeanId;                /**< 会员id*/
@property (nonatomic,copy)NSString *relativeNo;             /**< 关联业务编号，如订单号，退货单号等*/
@property (nonatomic,copy)NSString *memberId;               /**< memberId*/
@property (nonatomic,assign)MMHBeanType beanType;           /**< 0商品购物，1评价晒单，2游戏，3签到，4商品抵扣，5退货扣除，6其他*/
@property (nonatomic,assign)NSInteger mathOperator;         /**< 麻豆增减类型：0减 1加*/
@property (nonatomic,assign)NSInteger mbeanNum;             /**< 麻豆变化数量*/
@property (nonatomic,copy)NSString * des;                   /**< 描述*/
@property (nonatomic,copy)NSString *createDate;             /**< 妈豆变动时间*/
@property (nonatomic,copy)NSString *relativePic;            /**< 图片*/
@property (nonatomic,copy)NSString *mbeanNumShow;           /**< 妈豆数量显示*/
@property (nonatomic,copy)NSString *titleShow;              /**< 标题*/
@end
