//
//  MMHBabyInfoModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHBabyInfoModel <NSObject>

@end

@interface MMHBabyInfoModel : MMHFetchModel

@property (nonatomic,copy)NSString *babyBirthday;   /**< 宝贝生日*/
@property (nonatomic,assign)NSInteger babyGender;   /**< 宝贝性别1男2女*/
@property (nonatomic,copy)NSString *babyId;         /**< 宝贝编号*/
@property (nonatomic,copy)NSString *babyImg;        /**< 宝贝图片*/
@property (nonatomic,copy)NSString *babyNickName;   /**< 宝贝昵称*/
@property (nonatomic,copy)NSString *createDate;     /**< 创建时间*/
@property (nonatomic,copy)NSString *memberId;       /**< 用户id*/


@property (nonatomic,copy)NSString *mamState;       /**< 宝妈状态-单例使用*/
+ (MMHBabyInfoModel *)sharedBabyInfo;
@end
