//
//  MMHFavoritiesViewController.m
//  MamHao
//
//  Created by fishycx on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFavoritiesViewController.h"
#import "UIButton+UIButtonImageWithLable.h"
#import "MMHFavoritieBannerCollectionViewCell.h"
#import "MMHFavouriteProductCollectionViewCell.h"
#import "MMHShopDetailProductCell.h"
#import "MMHFavoriteShopCollectionViewCell.h"
#import "MMHNetworkAdapter+Center.h"
#import "HTHorizontalSelectionList.h"               // segment
#import "MMHProductDetailViewController1.h"
#import "MMHFavouriteProductModel.h"
#import "MMHFavouriteShopBrandModel.h"
#import "MMHProductListViewController.h"
typedef enum{
    RightButtonStateEdit,
    RightButtonStateNormal,
}RightButtonState;

@interface MMHFavoritiesViewController ()<UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>

@property (nonatomic, strong)NSArray *n_imageArray;
@property (nonatomic, strong)NSArray *s_imageArray;
@property (nonatomic, strong)NSMutableArray *btnArray;
@property (nonatomic, assign)NSInteger currentIndex;
@property (nonatomic, strong)UICollectionView *goodsCollectionVew;
@property (nonatomic, strong)NSMutableArray *favoriteDataSource;
@property (nonatomic, strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSArray *segmentItemArray;
@property (nonatomic, strong)NSMutableArray *bannerDataSource;
@property (nonatomic, strong)NSMutableArray *goodsDataSource;
@property (nonatomic, strong)NSMutableArray *shopDataSource;
@property (nonatomic, assign)CellState cellState;
@property (nonatomic, strong)UIButton *rightButton;
@property (nonatomic, assign)RightButtonState rightButtonState;
@property (nonatomic, strong)NSMutableArray *prepareToDeleteCellArray;
@property (nonatomic, strong)NSMutableIndexSet *pendingDeleting;
@property (nonatomic, strong)NSMutableArray *prepareToDeleteData;
@property (nonatomic, strong)NSMutableString *mutableString;
@end

@implementation MMHFavoritiesViewController

#pragma mark - getter Method

- (NSMutableArray *)prepareToDeleteData {
    if (!_prepareToDeleteData) {
        self.prepareToDeleteData = [NSMutableArray array];
    }
    return _prepareToDeleteData;
}
- (NSMutableIndexSet *)pendingDeleting {
    if (!_pendingDeleting) {
        self.pendingDeleting = [[NSMutableIndexSet alloc] init];
    }
    return _pendingDeleting;
}
- (NSMutableArray *)prepareToDeleteCellArray {
    if (!_prepareToDeleteCellArray) {
        self.prepareToDeleteCellArray = [NSMutableArray array];
    }
    return _prepareToDeleteCellArray;
}

- (NSMutableArray *)bannerDataSource {
    if (!_bannerDataSource) {
        self.bannerDataSource = [NSMutableArray array];
    }
    return _bannerDataSource;
}
- (NSMutableArray *)goodsDataSource {
    if (!_goodsDataSource) {
        self.goodsDataSource = [NSMutableArray array];
    }
    return _goodsDataSource;
}
- (NSMutableArray *)shopDataSource {
    if (!_shopDataSource) {
        self.shopDataSource = [NSMutableArray array];
    }
    return _shopDataSource;
}
- (UICollectionView *)goodsCollectionVew {
    if (!_goodsCollectionVew) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.headerReferenceSize = CGSizeMake(kScreenWidth, MMHFloat(10));
        flowLayout.footerReferenceSize = CGSizeMake(kScreenWidth, MMHFloat(10));
        CGRect frame = CGRectMake(0, 40, self.view.bounds.size.width, self.view.bounds.size.height - 40 - 64);
        self.goodsCollectionVew = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:flowLayout];
        [_goodsCollectionVew registerClass:[MMHFavoritieBannerCollectionViewCell class] forCellWithReuseIdentifier:@"banner"];
        [_goodsCollectionVew registerClass:[MMHFavouriteProductCollectionViewCell class] forCellWithReuseIdentifier:@"product"];

        _goodsCollectionVew.delegate = self;
        _goodsCollectionVew.dataSource = self;
        _goodsCollectionVew.backgroundColor = self.view.backgroundColor;
    }
    return _goodsCollectionVew;
}

#pragma mark--lifeCircle Method
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.barMainTitle = @"收藏";
    self.cellState = CellStateNormal;
    self.rightButtonState = RightButtonStateNormal;
    //[self createSegeView];
   [self.view addSubview:self.goodsCollectionVew];
    // 进入收藏的默认页面
    [self fetchDataWithIndex:self.segmentList.selectedButtonIndex];
    [self createSegeList];
    __weak typeof(self)weakself = self;
   self.rightButton = [self rightBarButtonWithTitle:@"编辑" barNorImage:nil barHltImage:nil action:^{
           if (weakself.rightButtonState == RightButtonStateNormal) {
               self.rightButtonState = RightButtonStateEdit;
               self.cellState = CellStateDesSelected;
           }else{
               self.rightButtonState = RightButtonStateNormal;
               self.cellState = CellStateNormal;
           }

       if ([weakself.rightButton.titleLabel.text isEqualToString: @"删除"]) {
            /**
            *  把要删除的数据提交服务器，然后再reloadData
            */
           if (!_mutableString) {
               self.mutableString = [[NSMutableString alloc] init];
           }
           if(self.segmentList.selectedButtonIndex == 0){
               NSRange range = NSMakeRange(0, self.mutableString.length) ;
               [self.mutableString deleteCharactersInRange:range];
               for (MMHFavouriteProductModel *model in self.prepareToDeleteData) {
                   [self.mutableString appendFormat:@"%@,",model.collect_id];
               }
                NSRange range1 = NSMakeRange(self.mutableString.length-1, 1);
                [self.mutableString deleteCharactersInRange:range1];
           }else{
               NSRange range = NSMakeRange(0, self.mutableString.length) ;
               [self.mutableString deleteCharactersInRange:range];
               for (MMHFavouriteShopBrandModel *model in self.prepareToDeleteData) {
                   [self.mutableString appendFormat:@"%ld,",(long)model.collect_id];
                   NSRange range = NSMakeRange(self.mutableString.length-1, 1);
                   [self.mutableString deleteCharactersInRange:range];
               }
            }
           
           
           [[UIAlertView alertViewWithTitle:@"警告" message:@"您确定要删除么" buttonTitles:@[@"取消", @"删除"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
               if (buttonIndex == 1) {
                   //删除
                   [[MMHNetworkAdapter sharedAdapter] deleteCollectWithCollectIds:self.mutableString from:nil succeededHandler:^{
                       //删除成功
                       NSLog(@"删除成功");
                       //reloadData;
                       [self.prepareToDeleteData removeAllObjects];
                       [weakself.favoriteDataSource removeObjectsAtIndexes:self.pendingDeleting];
                       [weakself.goodsCollectionVew deleteItemsAtIndexPaths:self.prepareToDeleteCellArray];
                       [self.pendingDeleting removeAllIndexes];
                       [self.prepareToDeleteCellArray removeAllObjects];
                       [self fetchDataWithIndex:self.segmentList.selectedButtonIndex];
                       [self resetAllState];
                       NSRange range = NSMakeRange(0, self.mutableString.length) ;
                       [self.mutableString deleteCharactersInRange:range];
                   } failedHandler:^(NSError *error) {
                       //删除失败
                       [self.prepareToDeleteData removeAllObjects];
                       [self.pendingDeleting removeAllIndexes];
                       [self.prepareToDeleteCellArray removeAllObjects];
                       [self resetAllState];
                   }];
               }else{
                   [self.prepareToDeleteData removeAllObjects];
                   [self.pendingDeleting removeAllIndexes];
                   [self.prepareToDeleteCellArray removeAllObjects];
                   [self resetAllState];
               }
           }] show];
           
           
       }
         [weakself.goodsCollectionVew reloadData];
   }];
    
}

- (void)resetAllState{
    self.cellState = CellStateNormal;
    self.rightButtonState = RightButtonStateNormal;
    [self.rightButton setTitle:@"编辑" forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = NO;
}
#pragma mark - private Method


- (void)fetchDataWithIndex:(NSInteger)currentIndex{
    
    [[MMHNetworkAdapter sharedAdapter] getMemberCollectWithCollectType:currentIndex+1 from:nil succeededHandler:^(NSMutableDictionary *dic) {
        NSArray *dataArr = dic[@"collectlist"];
        switch (currentIndex) {
            case 0:{
                [self.goodsDataSource removeAllObjects];
                for (int i = 0; i < dataArr.count; i++) {
                    MMHFavouriteProductModel *model = [[MMHFavouriteProductModel alloc] initWithJSONDict:dataArr[i]];
                    [self.goodsDataSource addObject:model];
                }
                self.favoriteDataSource = self.goodsDataSource;
                if (!self.goodsDataSource.count) {
                    [self.rightButton setHidden:YES];
                }else{
                    [self.rightButton setHidden:NO];
                }
                [self.goodsCollectionVew reloadData];
            }
                break;
            case 1:{
                [self.bannerDataSource removeAllObjects];
        
                for (int i = 0; i < dataArr.count; i++) {
                    MMHFavouriteShopBrandModel *model = [[MMHFavouriteShopBrandModel alloc] initWithJSONDict:dataArr[i]];
                    [self.bannerDataSource addObject:model];
                }
                self.favoriteDataSource = self.bannerDataSource;
                [self.goodsCollectionVew reloadData];
            }
                break;
            default:
                break;
        }

    } failedHandler:^(NSError *error) {
        //
    }];
    
}
- (void)createSegeList{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    self.segmentItemArray = @[@"商品", @"品牌"];
    self.segmentList.isNotScroll = YES;
    [self.view addSubview:self.segmentList];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 13, 0.5, 14)];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.centerX = self.segmentList.centerX;
    [self.segmentList addSubview:lineView];
}

//- (void)createSegeView{
//    
//    UIView *segeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 40)];
//    segeView.backgroundColor = [UIColor whiteColor];
//    [self.view addSubview:segeView];
//    
//    NSArray *n_imageArray = @[@"icon_collect_commodity_n", @"icon_collect_brand_n"];
//    self.n_imageArray = n_imageArray;
//    NSArray *s_imageArray =@[@"icon_collect_commodity_s", @"icon_collect_brand_s"];
//    self.s_imageArray = s_imageArray;
//    NSArray *titleArr = @[@"商品", @"品牌"];
//    self.btnArray = [NSMutableArray array];
//    for (int i = 0; i < 2; i++) {
//        
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        CGFloat btnWidth = (self.view.bounds.size.width - 1) / 2;
//        button.frame = CGRectMake(btnWidth*i, 0, btnWidth,segeView.bounds.size.height);
//        button.titleLabel.font = MMHFontOfSize(12);
//        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
//        
//        UIImage *image = [UIImage imageNamed:n_imageArray[i]];
//        [button setTitle:titleArr[i] forState:UIControlStateNormal];
//        button.tag = i;
//        [button setImage:image forState:UIControlStateNormal];
//        
//        [button makeVerticalWithPadding:10.0f];
//       [button addTarget:self action:@selector(handleBtn:) forControlEvents:UIControlEventTouchUpInside];
//        [self.btnArray addObject:button];
//        [segeView addSubview:button];
//        if (i != 0) {
//            UILabel *linelabel = [[UILabel alloc] initWithFrame:CGRectMake(i*btnWidth, MMHFloat(14.5)/2, 0.5, MMHFloat(46))];
//            linelabel.backgroundColor = [UIColor colorWithHexString:@"dcdcdc"];
//            [segeView addSubview:linelabel];
//        }
//    }
//    segeView.layer.borderWidth = 0.5;
//    segeView.layer.borderColor = [UIColor colorWithHexString:@"dcdcdc"].CGColor;
//}


- (void)handleBtn:(UIButton *)sender{
    
    if (self.segmentList.selectedButtonIndex == sender.tag) {
        
    }else{
        
        [self resetAllState];
         self.segmentList.selectedButtonIndex = sender.tag;
        
        [self fetchDataWithIndex:self.segmentList.selectedButtonIndex];
        switch (sender.tag) {
            case 0:
                // 请求商品的接口
                break;
            case 1:
                
                //请求品牌的接口
                break;
            default:
                break;
        }
    }
   
    NSString *title = sender.titleLabel.text;
    [sender setImage:[UIImage imageNamed:self.s_imageArray[sender.tag]] forState:UIControlStateNormal];
    for (int i = 0;i < 2; i++) {
        UIButton *btn = self.btnArray[i];
        if (![btn.titleLabel.text isEqualToString:title]) {
            [btn setImage:[UIImage imageNamed:self.n_imageArray[i]] forState:UIControlStateNormal];
        }
    }

}
#pragma mark - UISegmentOfDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentItemArray.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentItemArray objectAtIndex:index];
}

#pragma mark - UISegmentOfDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    if (index == 0){                    // 当前
        NSLog(@"当前");
    } else {
        NSLog(@"历史");
    }
    [self resetAllState];
    [self.pendingDeleting removeAllIndexes];
    [self.prepareToDeleteData removeAllObjects];
    [self.prepareToDeleteCellArray removeAllObjects];
   [self fetchDataWithIndex:(self.segmentList.selectedButtonIndex)];
}
#pragma mark -<UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.favoriteDataSource.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.segmentList.selectedButtonIndex == 1) {
        MMHFavoritieBannerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"banner" forIndexPath:indexPath];
        MMHFavouriteShopBrandModel *model =self.bannerDataSource[indexPath.item];
        cell.model = model;
        cell.cellstate = self.cellState;
        return cell;
    }else {
        MMHFavouriteProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"product" forIndexPath:indexPath];
        MMHFavouriteProductModel *model = self.favoriteDataSource[indexPath.item];
        cell.product = model;
        cell.cellstate = self.cellState;
        return cell;
    }
}

#pragma mark - <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell1  = [collectionView cellForItemAtIndexPath:indexPath];
    if ((self.rightButtonState == RightButtonStateEdit)) {
        switch (self.segmentList.selectedButtonIndex) {
            case 0:{
                MMHFavouriteProductCollectionViewCell *cell = (MMHFavouriteProductCollectionViewCell *)cell1;
                if (cell.cellstate == CellStateDesSelected) {
                    cell.stateIcon.image = [UIImage imageNamed:@"btn_select_s"];
                    cell.cellstate = CellStateDidSelected;
                    [self.prepareToDeleteData addObject:cell.product];
                    [self.pendingDeleting addIndex:indexPath.row];
                    [self.prepareToDeleteCellArray addObject:indexPath];
                }else{
                    cell.stateIcon.image = [UIImage imageNamed:@"btn_select_gray"];
                    cell.cellstate = CellStateDesSelected;
                    [self.prepareToDeleteData removeObject:cell.product];
                    [self.pendingDeleting removeIndex:indexPath.row];
                    [self.prepareToDeleteCellArray removeObject:indexPath];
                }
                if (self.prepareToDeleteData.count) {
                    [self.rightButton setTitle:@"删除" forState:UIControlStateNormal];
                }else{
                    [self resetAllState];
                    [self.goodsCollectionVew reloadData];
                }
            }
                break;
            case 1:{
        MMHFavoritieBannerCollectionViewCell *cell = (MMHFavoritieBannerCollectionViewCell *)cell1;
                if (cell.cellstate == CellStateDesSelected) {
                    cell.stateIcon.image = [UIImage imageNamed:@"btn_select_s"];
                    cell.cellstate = CellStateDidSelected;
                    [self.prepareToDeleteData addObject:cell.model];
                    [self.pendingDeleting addIndex:indexPath.row];
                    [self.prepareToDeleteCellArray addObject:indexPath];
                }else{
                    cell.stateIcon.image = [UIImage imageNamed:@"btn_select_gray"];
                    cell.cellstate = CellStateDesSelected;
                    [self.prepareToDeleteData removeObject:cell.model];
                    [self.pendingDeleting removeIndex:indexPath.row];
                    [self.prepareToDeleteCellArray removeObject:indexPath];
                }
                [self.rightButton setTitle:@"删除" forState:UIControlStateNormal];
            }
                break;
                
            default:
                break;
        }
    
        
    }else {
        switch (self.segmentList.selectedButtonIndex) {
            case 0:{

                MMHFavouriteProductModel *product = self.favoriteDataSource[indexPath.item];
                MMHProductDetailViewController1 *productDetailViewController1 = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
                [self.navigationController pushViewController:productDetailViewController1 animated:YES];
            }
                break;
            case 1:{
                MMHFavouriteShopBrandModel *brandModel=self.bannerDataSource[indexPath.item];
                if (brandModel.b_id == 0) {
                    return;
                }

                NSString *brandID = [NSString stringWithFormat:@"%ld", (long)brandModel.b_id];
                MMHFilter *filter = [MMHFilter filterWithBrandID:brandID];
                MMHProductListViewController *productListViewController = [[MMHProductListViewController alloc] initWithFilter:filter];
                [self pushViewController:productListViewController];
            }
                break;
            default:
                break;
        }
       
        
    }

}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
}
#pragma mark - <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.segmentList.selectedButtonIndex == 1) {
        CGSize itemSize  = CGSizeMake((kScreenWidth - MMHFloat(36))/3, MMHFloat(140));
        return itemSize;
    }
  
    return [MMHShopDetailProductCell defaultSize];

}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (self.segmentList.selectedButtonIndex == 0) {
        return UIEdgeInsetsMake(0, 8, 0, 8);
    }
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, MMHFloat(10), 0, MMHFloat(10));
    return edgeInsets;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    if (self.segmentList.selectedButtonIndex == 0) {
        return 8;
    }
    return MMHFloat(8);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    if (self.segmentList.selectedButtonIndex == 0) {
        return 8;
    }
    return MMHFloat(8);
}



@end
