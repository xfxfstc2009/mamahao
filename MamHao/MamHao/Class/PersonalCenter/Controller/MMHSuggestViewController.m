//
//  MMHSuggestViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSuggestViewController.h"
#import "MMHNetworkAdapter+Center.h"

@interface MMHSuggestViewController()<UITextViewDelegate,UIAlertViewDelegate>
@property (nonatomic,strong)UITextView *complaintTextView;              /**< 投诉textView*/
@property (nonatomic,strong)UITextView *adviceTextView;                 /**< 建议textView*/
@property (nonatomic,strong)UILabel *placeholder;                       /**< placeholder*/
@property (nonatomic,strong)UISegmentedControl *segment;                /**< segment*/
@property (nonatomic,strong)UIButton *sendButton;
@property (nonatomic,assign)BOOL isComplaint;                           /**< 判断是否是投诉*/
@property (nonatomic,strong)UILabel *textlimitLabel;                    // 控制文字长度的Label
@end

@implementation MMHSuggestViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createSegmentControl];
}

#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"意见反馈";
}

#pragma mark - UISegmentedControl
-(void)createSegmentControl{
//    // segment
//    self.segment = [[UISegmentedControl alloc]initWithItems:@[@"意见建议",@"我要投诉"]];
//    self.segment.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(295))/2., MMHFloat(20), MMHFloat(295), MMHFloat(29));
//    self.segment.backgroundColor = [UIColor clearColor];
//    self.segment.selectedSegmentIndex = 0;
//    self.isComplaint = 0;
//    self.segment.tintColor = [UIColor colorWithCustomerName:@"粉"];
//    [self.segment addTarget:self action:@selector(segmentSelected:) forControlEvents:UIControlEventValueChanged];
//    [self.view addSubview:self.segment];
    
    // 投诉textView
    self.complaintTextView = [[UITextView alloc]init];
    self.complaintTextView.delegate = self;
    self.complaintTextView.textColor = [UIColor blackColor];
    self.complaintTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.complaintTextView.returnKeyType = UIReturnKeyDefault;
    self.complaintTextView.keyboardType = UIKeyboardTypeDefault;
    self.complaintTextView.backgroundColor = [UIColor whiteColor];
    self.complaintTextView.scrollEnabled = YES;
    self.complaintTextView.frame = CGRectMake(MMHFloat(16),  MMHFloat(20), kScreenBounds.size.width - 2 * MMHFloat(16), MMHFloat(128));
    [self.view addSubview:self.complaintTextView];
    
    // 建议textView
    self.adviceTextView = [[UITextView alloc]init];
    self.adviceTextView.delegate = self;
    self.adviceTextView.textColor = [UIColor blackColor];
    self.adviceTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.adviceTextView.returnKeyType = UIReturnKeyDefault;
    self.adviceTextView.keyboardType = UIKeyboardTypeDefault;
    self.adviceTextView.backgroundColor = [UIColor whiteColor];
    self.adviceTextView.scrollEnabled = YES;
    self.adviceTextView.frame = CGRectMake(MMHFloat(16),MMHFloat(20), kScreenBounds.size.width - 2 * MMHFloat(16), MMHFloat(128));
    [self.view addSubview:self.adviceTextView];
    
    // placeholder
    self.placeholder = [[UILabel alloc]init];
    self.placeholder.numberOfLines = 0;
    self.placeholder.frame = CGRectMake(MMHFloat(7 + 16), self.complaintTextView.frame.origin.y + MMHFloat(10), self.complaintTextView.bounds.size.width - 2 * MMHFloat(11), [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]]);
    self.placeholder.backgroundColor = [UIColor clearColor];
    self.placeholder.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.placeholder.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
    [self.view addSubview:self.placeholder];
    
    // 确定发送按钮
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendButton addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.sendButton setTitle:@"确定发送" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    self.sendButton.frame = CGRectMake(MMHFloat(16), CGRectGetMaxY(self.complaintTextView.frame) + MMHFloat(25), kScreenBounds.size.width - 2 * MMHFloat(16), MMHFloat(44));
    [self.view addSubview: self.sendButton];
    
//    // 创建电话button
//    UILabel *contentLabel = [[UILabel alloc]init];
//    NSString *string = [NSString stringWithFormat:@"如遇到问题你可以联系客服%@",CustomerServicePhoneNumber];
//    NSRange range1 = [string rangeOfString:@"如遇到问题你可以"];
//    NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"联系客服%@",CustomerServicePhoneNumber]];
//    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
//    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"白灰"] range:range1];
//    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"蓝"] range:range2];
//    contentLabel.attributedText = str;
//    contentLabel.font=[UIFont fontWithCustomerSizeName:@"小提示"];
//    contentLabel.frame = CGRectMake(MMHFloat(16), CGRectGetMaxY(self.sendButton.frame) + MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(16), 15);
//    contentLabel.textAlignment = NSTextAlignmentLeft;
//    contentLabel.numberOfLines=1;
//    contentLabel.backgroundColor=[UIColor clearColor];
//    [self.view addSubview:contentLabel];

//    // 创建按钮
//    UIButton *customerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [customerButton addTarget:self action:@selector(customerButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    customerButton.frame = contentLabel.frame;
//    customerButton.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:customerButton];
    
    // 创建文字长度
    self.textlimitLabel = [[UILabel alloc]init];
    self.textlimitLabel.frame = CGRectMake(kScreenBounds.size.width - 50 - MMHFloat(16), CGRectGetMaxY(self.adviceTextView.frame) + MMHFloat(3), 50, 20);
    self.textlimitLabel.text = [NSString stringWithFormat:@"%li/200",(long)0];
    self.textlimitLabel.textAlignment = NSTextAlignmentRight;
    self.textlimitLabel.textColor = [UIColor blackColor];
    self.textlimitLabel.font=[UIFont systemFontOfSize:13.];
    self.textlimitLabel.textColor = [UIColor lightGrayColor];
    self.textlimitLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.textlimitLabel];
    
    [self adjustWithButtonAndPlaceholder];

}



#pragma mark - actionClick
-(void)segmentSelected:(UISegmentedControl *)sender{
    NSInteger index = self.segment.selectedSegmentIndex;
    self.isComplaint = index;
    if (index == 0) {           // 意见建议
        [self.view bringSubviewToFront:self.adviceTextView];
    }else{
        [self.view bringSubviewToFront:self.complaintTextView];
    }
    [self.view bringSubviewToFront:self.placeholder];
    [self adjustWithButtonAndPlaceholder];
}

-(void)buttonClick{
    __weak typeof(self)weakSelf = self;
    NSString *content = self.isComplaint == 0?self.adviceTextView.text:self.complaintTextView.text;
    [weakSelf.view showProcessingView];
    [weakSelf sendRequestWithType:(weakSelf.isComplaint + 1) content:content];
}

-(void)customerButtonClick{
    __weak MMHSuggestViewController *weakViewController = self;
    [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
}

#pragma mark - UITextViewDelegate
-(void)textViewDidChange:(UITextView *)textView {
    [self adjustWithButtonAndPlaceholder];
    
}


-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *toBeString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    if(textView == self.complaintTextView) {
        if([toBeString length] > 200) {
            textView.text = [toBeString substringToIndex:200];
            return NO;
        }
    } else if (textView == self.adviceTextView){
        if ([toBeString length] > 200){
            textView.text = [toBeString substringToIndex:200];
            return NO;
        }
    }
    return YES;
}

#pragma mark 判断是否高亮
-(void)adjustWithButtonAndPlaceholder{
    if (self.isComplaint == 0){ // 建议
        if (self.adviceTextView.text.length == 0){
            self.placeholder.text = @"亲爱的麻麻，在此输入您的意见或建议哦。";
            self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
            self.sendButton.enabled = NO;
        } else {
            self.placeholder.text = @"";
            self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
            self.sendButton.enabled = YES;
        }
        self.textlimitLabel.text = [NSString stringWithFormat:@"%li/200",(long)[MMHTool calculateCharacterLengthForAres:self.adviceTextView.text]];
    } else {                    // 投诉
        if (self.complaintTextView.text.length == 0){
            self.placeholder.text = @"请填写投诉";
            self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
            self.sendButton.enabled = NO;
        } else {
            self.placeholder.text = @"";
            self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
            self.sendButton.enabled = YES;
        }
        self.textlimitLabel.text = [NSString stringWithFormat:@"%li/200",(long)[MMHTool calculateCharacterLengthForAres:self.complaintTextView.text]];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (self.isComplaint == 0){
        if ([self.adviceTextView isFirstResponder]){
            [self.adviceTextView resignFirstResponder];
        }
    } else {
        if ([self.complaintTextView isFirstResponder]){
            [self.complaintTextView resignFirstResponder];
        }
    }
}


#pragma mark - sendRequest
-(void)sendRequestWithType:(NSInteger)type content:(NSString *)content{
    [self.adviceTextView resignFirstResponder];
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] sendRequestWithDeedbackType:type content:content from:nil succeededHandler:^(MMHCouponsListModel *couponsListModel) {
        [weakSelf.view hideProcessingView];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"发表成功" message:@"感谢您提出的宝贵建议，我们会认真阅读并依据实际情况来改进。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil , nil];
        [alertView show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark-alertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self popWithAnimation];
}
@end
