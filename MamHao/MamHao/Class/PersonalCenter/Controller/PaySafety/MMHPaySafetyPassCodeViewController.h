//
//  MMHPaySafetyPassCodeViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 设置支付密码 - 验证码
#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger, MMHPaySafetyPageType) {
    MMHPaySafetyPageTypeSetting,                    /**< 从设置页面进入*/
    MMHPaySafetyPageTypeReset,                      /**< 从重置页面进入*/
    MMHPaySafetyPageTypeOrderHome,                  /**< 从订单首页进入*/
    MMHPaySafetyPageTypeOrderDetail,                /**< 从订单详情页进入*/
};

@interface MMHPaySafetyPassCodeViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferPhoneNumber;
@property (nonatomic,assign)BOOL payPasswordIsSetting;                      /**< 支付密码是否设置*/
@property (nonatomic,assign)MMHPaySafetyPageType paySafetPageType;          /**< 跳转进入的页面*/

@end
