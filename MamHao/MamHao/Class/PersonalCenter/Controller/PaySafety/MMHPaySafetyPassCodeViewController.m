//
//  MMHPaySafetyPassCodeViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPaySafetyPassCodeViewController.h"
#import "MMHPaySafetyPasswordSettingViewController.h"
#import "MMHKeyboardView.h"
#import "MMHNetworkAdapter+Center.h"        // 接口

@interface MMHPaySafetyPassCodeViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIKeyboardDelegate>
@property (nonatomic,strong)UIButton *sendExamineButton;
@property (nonatomic,strong)UIButton *smsCodeButton;
@property (nonatomic,strong)NSMutableArray *payPassCodeMutableArr;
@property (nonatomic,strong)UITableView *payPassCodeTableView;
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,assign)NSInteger readTime;

@end

@implementation MMHPaySafetyPassCodeViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    __weak typeof(self)weakSelf = self;
    // [weakSelf sendRequestWithSendSmsCode];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    self.readTime = 60;
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.payPasswordIsSetting){
        self.barMainTitle = @"修改支付密码";
    } else {
        self.barMainTitle = @"设置支付密码";
    }
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - UITableView
-(void)arrayWithInit{
    self.payPassCodeMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.payPassCodeTableView){
        self.payPassCodeTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.payPassCodeTableView.delegate = self;
        self.payPassCodeTableView.dataSource = self;
        self.payPassCodeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.payPassCodeTableView.backgroundColor = [UIColor clearColor];
        self.payPassCodeTableView.scrollEnabled = NO;
        [self.view addSubview:self.payPassCodeTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section != 2){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 创建titleLabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = [UIColor blackColor];
            [cellWithRowOne addSubview:fixedLabel];
            
            // 创建输入框
            UITextField *inputTextField = [[UITextField alloc]init];
            inputTextField.backgroundColor = [UIColor clearColor];
            inputTextField.textAlignment = NSTextAlignmentLeft;
            inputTextField.textColor = [UIColor blackColor];
            inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            inputTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            inputTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            inputTextField.placeholder = @"请输入验证码";
            inputTextField.returnKeyType = UIReturnKeyDone;
            inputTextField.keyboardType = UIKeyboardTypeNumberPad;
            inputTextField.delegate = self;
            inputTextField.stringTag = @"inputTextField";
            [cellWithRowOne addSubview:inputTextField];
            
            // 创建验证码
            self.smsCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.smsCodeButton.backgroundColor = [UIColor whiteColor];
            self.smsCodeButton.stringTag = @"smsCodeButton";
            [self.smsCodeButton addTarget:self action:@selector(smsCodeButtonClick) forControlEvents:UIControlEventTouchUpInside];
            [self.smsCodeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
            [self.smsCodeButton setTitleColor:[UIColor colorWithCustomerName:@"粉"] forState:UIControlStateNormal];
            [cellWithRowOne addSubview:self.smsCodeButton];
            // [self.smsCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            self.smsCodeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            self.smsCodeButton.hidden = YES;
            
            // 创建lineView
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
            lineView.stringTag = @"lineView";
            lineView.hidden = YES;
            [cellWithRowOne addSubview:lineView];
        }
        // 赋值
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
        if (indexPath.section == 0){
            fixedLabel.text = @"手机号";
        } else if (indexPath.section == 1){
            fixedLabel.text = @"验证码";
        }
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        
        // 获取smsButton
        UIButton *smsButton = (UIButton *)[cellWithRowOne viewWithStringTag:@"smsCodeButton"];
        CGFloat smsButtonWidth = [@"55秒后可重新发送" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)].width;
        smsButton.frame = CGRectMake(kScreenBounds.size.width - smsButtonWidth - MMHFloat(10) * 2 , 0, smsButtonWidth + 2 * MMHFloat(10), cellHeight);
        
        // 输入框
        CGFloat fixedLabelWidth = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)].width;
        fixedLabel.frame = CGRectMake(MMHFloat(10), 0, fixedLabelWidth, cellHeight);
        
        // 创建inputTextField
        UITextField *inputTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"inputTextField"];
        inputTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(20), 0, kScreenBounds.size.width - smsButton.size_width - CGRectGetMaxX(fixedLabel.frame) - MMHFloat(20), cellHeight);
        
        // lineView
        UIView *lineView = (UIView *)[cellWithRowOne viewWithStringTag:@"lineView"];
        lineView.frame = CGRectMake(smsButton.frame.origin.x - .5f, 0, .5f, cellHeight);
        if (indexPath.section == 0){
            smsButton.hidden = YES;
            lineView.hidden = YES;
            inputTextField.text = [MMHTool numberCutting:self.transferPhoneNumber];
            inputTextField.enabled = NO;
        } else {
            smsButton.hidden = NO;
            lineView.hidden = NO;
            inputTextField.enabled = YES;
        }
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        UITableViewCell *cellWithOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithOther){
            cellWithOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            cellWithOther.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithOther.backgroundColor = [UIColor clearColor];
            
            self.sendExamineButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.sendExamineButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
            [self.sendExamineButton addTarget:self action:@selector(sendExamineButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.sendExamineButton setTitle:@"下一步" forState:UIControlStateNormal];
            [self.sendExamineButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            self.sendExamineButton.frame = CGRectMake(MMHFloat(16), 0, tableView.bounds.size.width - 2 * MMHFloat(16), MMHFloat(48));
            self.sendExamineButton.enabled = NO;
            self.sendExamineButton.userInteractionEnabled = YES;
            self.sendExamineButton.layer.cornerRadius = MMHFloat(5);
            [cellWithOther addSubview:self.sendExamineButton];
        }
        
        return cellWithOther;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section !=2){
        return 10;
    } else {
        return 20;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - actionClick
-(void)smsCodeButtonClick{
    __weak typeof(self)weakSelf = self;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(readSec) userInfo:nil repeats:YES];
    [weakSelf sendRequestWithSendSmsCode];
}

-(void)sendExamineButtonClick:(UIButton *)sender{
    __weak typeof(self)weakSelf = self;
    UITableViewCell *inputSmsCodeCell = [self.payPassCodeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    UITextField *inputTextField = (UITextField *)[inputSmsCodeCell viewWithStringTag:@"inputTextField"];
    [weakSelf sendRequestWithVericationPhone:weakSelf.transferPhoneNumber vcode:inputTextField.text];
}

-(void)readSec{
    self.readTime --;
    if (self.readTime <= 0){
        [self.smsCodeButton setTitle:@"重新获取" forState:UIControlStateNormal];
        [self.smsCodeButton setTitleColor:[UIColor colorWithCustomerName:@"粉"] forState:UIControlStateNormal];
        self.smsCodeButton.enabled = YES;
    } else {
        [self.smsCodeButton setTitle:[NSString stringWithFormat:@"%li秒后可重发",(long)self.readTime] forState:UIControlStateNormal];
        [self.smsCodeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.smsCodeButton.enabled = NO;
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if ([toBeString length] > 6) {
        textField.text = [toBeString substringToIndex:6];
        return NO;
    }
    if (toBeString.length == 6){
        self.sendExamineButton.enabled = YES;
        self.sendExamineButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
    } else {
        self.sendExamineButton.enabled = NO;
        self.sendExamineButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
    }
    
    return YES;
}


#pragma mark - 接口
#pragma mark 设置妈豆支付密码发送验证码
-(void)sendRequestWithSendSmsCode{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithSettingSmsCodeWithPhone:weakSelf.transferPhoneNumber from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTips:@"验证码已发送，请耐心等待"];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 检验验证码
-(void)sendRequestWithVericationPhone:(NSString *)phoneNumber vcode:(NSString *)vcode{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithverificationSmsCodeWithPhone:phoneNumber vcode:vcode from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        MMHPaySafetyPasswordSettingViewController *paySafetyPasswordSettingViewController =[[MMHPaySafetyPasswordSettingViewController alloc ]init];
        paySafetyPasswordSettingViewController.transferPhoneNumber = strongSelf.transferPhoneNumber;
        paySafetyPasswordSettingViewController.paySafetPageType = strongSelf.paySafetPageType;
        [strongSelf.navigationController pushViewController:paySafetyPasswordSettingViewController animated:YES];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}


@end
