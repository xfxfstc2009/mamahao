//
//  MMHPaySafetyPasswordSettingViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPaySafetyPasswordSettingViewController.h"
#import "MMHNetworkAdapter+Center.h"


@interface MMHPaySafetyPasswordSettingViewController()<UITextFieldDelegate>{
    UITextField     *textFieldPwd;      //  输入密码的TextField控件
    NSString        *pwd_Edit;          //  记录当前界面编辑的密码
    NSString        *pwd_First;         //  第1次输入的密码
    
    UILabel         *labelMessageOne;   //  提示语Label-1 密码黑点上方
    UIImageView     *dot_1;             //  密码圆点1
    UIImageView     *dot_2;             //  密码圆点2
    UIImageView     *dot_3;             //  密码圆点3
    UIImageView     *dot_4;             //  密码圆点4
    UIImageView     *dot_5;             //  密码圆点5
    UIImageView     *dot_6;             //  密码圆点6
    
    NSMutableArray  *dot_Array;         //  存放密码圆点控件的数组
    UILabel         *labelMessageTwo;   //  提示语Label-2 密码黑点下方
}
@property (nonatomic,strong)UIButton *confirmButton;

@end

@implementation MMHPaySafetyPasswordSettingViewController
@synthesize currentEditType;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self baseSetting];
    [self baseControlSetting];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (textFieldPwd){
        [textFieldPwd becomeFirstResponder];
    }
}

- (void)baseSetting {
    self.barMainTitle = @"设置支付密码";
}

// - 控件初始化函数
- (void)baseControlSetting {
    
    // 提示文字
    labelMessageOne = [[UILabel alloc]init];
    labelMessageOne.font = [UIFont fontWithCustomerSizeName:@"副标题"];
    labelMessageOne.frame = CGRectMake(0, MMHFloat(20), kScreenBounds.size.width, [MMHTool contentofHeight:labelMessageOne.font]);
    labelMessageOne.textAlignment = NSTextAlignmentCenter;
    labelMessageOne.backgroundColor = [UIColor clearColor];
    labelMessageOne.text = [self messageSetting];
    [self.view addSubview:labelMessageOne];
    
    // 错误提示文字
    labelMessageTwo = [[UILabel alloc]init];
    labelMessageTwo.font = [UIFont fontWithCustomerSizeName:@"副标题"];
    labelMessageTwo.frame = CGRectMake(0, MMHFloat(20), kScreenBounds.size.width, [MMHTool contentofHeight:labelMessageTwo.font]);
    labelMessageTwo.textAlignment = NSTextAlignmentCenter;
    labelMessageTwo.backgroundColor = [UIColor clearColor];
    labelMessageTwo.text = @"";
    [self.view addSubview:labelMessageTwo];
    
    // 创建下一步按钮
    self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.confirmButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
    [self.confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    [self.confirmButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.confirmButton.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(55) * 6)/2., CGRectGetMaxY(labelMessageOne.frame) + MMHFloat(20) + MMHFloat(45) + MMHFloat(20), self.view.bounds.size.width - 2 * ((kScreenBounds.size.width - MMHFloat(55) * 6)/2.), MMHFloat(48));
    self.confirmButton.enabled = NO;
    self.confirmButton.userInteractionEnabled = YES;
    self.confirmButton.layer.cornerRadius = MMHFloat(5);
    [self.view addSubview:self.confirmButton];
    
    
    [self CheckPasswordControlStatus];
    [self CreateTextFieldPwd];
}

#pragma mark - 密码小黑点控件的方法
// - 密码黑点控件的加载和更新
- (void)CheckPasswordControlStatus {
    if (!dot_Array || dot_Array.count < 6) {
        if (!dot_1) dot_1 = [self CreatePasswordDotWithIndex:0];
        if (!dot_2) dot_2 = [self CreatePasswordDotWithIndex:1];
        if (!dot_3) dot_3 = [self CreatePasswordDotWithIndex:2];
        if (!dot_4) dot_4 = [self CreatePasswordDotWithIndex:3];
        if (!dot_5) dot_5 = [self CreatePasswordDotWithIndex:4];
        if (!dot_6) dot_6 = [self CreatePasswordDotWithIndex:5];
        dot_Array = [[NSMutableArray alloc] initWithArray:@[dot_1, dot_2, dot_3, dot_4, dot_5, dot_6]];
        
        CGFloat centerY;
        centerY = self.view.frame.size.height * 0.2;
        for (int i=0; i<dot_Array.count; i++){
            UIImageView *tempDot = [dot_Array objectAtIndex:i];
            [self.view addSubview:tempDot];
            
            tempDot.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(55) * 6)/2. + i * MMHFloat(55), CGRectGetMaxY(labelMessageOne.frame) + MMHFloat(20), MMHFloat(55), MMHFloat(45));
            
            if (i == 0){
                tempDot.image = [UIImage imageNamed:@"password_bg_uninput01"];
            } else if (i == dot_Array.count - 1){
                tempDot.image = [UIImage imageNamed:@"password_bg_uninput03"];
            } else {
                tempDot.image = [UIImage imageNamed:@"password_bg_uninput02"];
            }
        }
    }
}

// - 创建密码小黑点
- (UIImageView *)CreatePasswordDotWithIndex:(NSInteger)dotIndex {
    UIImageView *dotImageView = [[UIImageView alloc] init];
    return dotImageView;
}

// - 设置密码小黑点状态
// - num 代表黑点的数量 范围是(0~6)
- (void)RefreshDot {
    NSInteger num = pwd_Edit.length;
    if (num<0 || num >6) return;
    if (dot_Array.count < 6) return;
    
    //  设置黑点
    for (int i = 0; i < num; i++) {
        UIImageView *tempDot = [dot_Array objectAtIndex:i];
        if (i == 0){
            tempDot.image = [UIImage imageNamed:@"password_bg_input01"];
        } else if (i == num -1){
            tempDot.image = [UIImage imageNamed:@"password_bg_input03"];
        } else {
            tempDot.image = [UIImage imageNamed:@"password_bg_input02"];
        }
    }
    
    //  设置白点
    for (NSInteger j = num; j < 6; j++){
        UIImageView *tempDot = [dot_Array objectAtIndex:j];
        if (j == 0){
            tempDot.image = [UIImage imageNamed:@"password_bg_uninput01"];
        } else if (j == dot_Array.count - 1){
            tempDot.image = [UIImage imageNamed:@"password_bg_uninput03"];
        } else {
            tempDot.image = [UIImage imageNamed:@"password_bg_uninput02"];
        }
    }
}

#pragma mark - 用于输入密码的TextField控件
- (void)CreateTextFieldPwd
{
    if (!textFieldPwd) {
        textFieldPwd = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [textFieldPwd setBackgroundColor:[UIColor clearColor]];
        [textFieldPwd setTextColor:[UIColor clearColor]];
        [textFieldPwd setDelegate:self];
        [textFieldPwd setKeyboardType:UIKeyboardTypeNumberPad];
        [self.view addSubview:textFieldPwd];
        
        [[NSNotificationCenter defaultCenter]
         addObserver:self
         selector:@selector(textFieldTextDidChangeOneCI:)
         name:UITextFieldTextDidChangeNotification
         object:textFieldPwd];
    }
}

#pragma mark - UITextField Delegate
-(void)textFieldTextDidChangeOneCI:(NSNotification *)notification {
    UITextField *tempTextField=[notification object];
    pwd_Edit = textFieldPwd.text;
    
    //  根据TextField里密码输入长度做相应处理
    if (tempTextField.text.length <6) {
        [self RefreshDot];
    }
    else if (tempTextField.text.length == 6) {
        pwd_Edit = textFieldPwd.text;
        [self RefreshDot];
        
        [UIView animateWithDuration:0.45 animations:^{
            [self RefreshPasswordInputStatus];
        }];
    }
    else if (textFieldPwd.text.length > 6){
        [self.view endEditing:YES];
    }
}

#pragma mark - 根据密码输入状况调整界面状态
//  密码输入达到4位时，更新界面上的密码输入状态
- (void)RefreshPasswordInputStatus {
    if (currentEditType == EditPwdTypeNewPwd) {
        pwd_First = pwd_Edit;
        pwd_Edit = @"";
        [textFieldPwd setText:@""];
        currentEditType = EditPwdTypeConfirmPwd;
        [self hidenMessageTwo];
        
    }  else if (currentEditType == EditPwdTypeConfirmPwd){
        if ([pwd_Edit isEqualToString: pwd_First]){
            self.confirmButton.enabled = YES;
            self.confirmButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
        } else {
            pwd_Edit = @"";
            [textFieldPwd setText:@""];
            currentEditType = EditPwdTypeNewPwd;
            [self ShowMessageTwo:@"密码不匹配，请重新设置"];
            self.confirmButton.enabled = NO;
            self.confirmButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
        }
    }
    
    [self RefreshDot];
    labelMessageOne.text = [self messageSetting];
}

//  根据当前界面类型，判断所需提示语(第一行提示语)
- (NSString*)messageSetting {
    if (currentEditType == EditPwdTypeNewPwd) return @"设置6位数字支付密码";
    else if (currentEditType == EditPwdTypeConfirmPwd) return @"请再次输入以确认";
    else if (currentEditType == EditPwdTypeOldPwd) return  @"请输入旧4位密码";
    else if (currentEditType == EditPwdTypeChangePwd) return  @"请输入新的4位密码";
    else if (currentEditType == EditPwdTypeChangeConfirmPwd) return  @"请重复输入4位密码";
    else if (currentEditType == EditPwdTypeLockOff) return @"请输入4位密码";
    else if (currentEditType == EditPwdTypeCheckPwd) return @"请输入4位密码来解锁";
    else return @"";
}

// - 显示第二行提示语
- (void)ShowMessageTwo:(NSString*)str {
    if (str){
        labelMessageTwo.text = str;
        labelMessageTwo.textColor = [UIColor redColor];
        labelMessageOne.hidden = YES;
    }
}

-(void)hidenMessageTwo{
    labelMessageTwo.backgroundColor = [UIColor clearColor];
    labelMessageTwo.text = @"";
    labelMessageOne.hidden = NO;
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (currentEditType == EditPwdTypeNewPwd) {
        [self hidenMessageTwo];
        
    }  else if (currentEditType == EditPwdTypeConfirmPwd){
        if (textFieldPwd == textField) {
            if ([toBeString length] > 6) {
                textField.text = [toBeString substringToIndex:6];
                if ([pwd_Edit isEqualToString: pwd_First]){                 // 密码匹配
                    self.confirmButton.enabled = YES;
                    self.confirmButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
                }
                return NO;
            } else {                                                        // 密码不匹配
                [self hidenMessageTwo];
                self.confirmButton.enabled = NO;
                self.confirmButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
            }
        }
    }
    return YES;
}

-(void)confirmButtonClick{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithSetPayPasswordPayPassword:pwd_First];
}

-(void)poptoLastViewController{
    if (self.paySafetPageType ==MMHPaySafetyPageTypeSetting){             // 【从设置页面进入】
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[MMHSettingViewController class]]) {
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
    } else if (self.paySafetPageType == MMHPaySafetyPageTypeReset){       // 【从重置页面进入】
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[MMHConfirmationOrderViewController class]]) {
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
    } else if (self.paySafetPageType == MMHPaySafetyPageTypeOrderHome){   // 【从订单首页进入】
        for (UIViewController *viewController in self.navigationController.viewControllers){
            if ([viewController isKindOfClass:[MMHOrderDetailViewController class]]){
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
    } else if (self.paySafetPageType == MMHPaySafetyPageTypeOrderDetail){ // 【从订单详情页面进入】
        for (UIViewController *viewController in self.navigationController.viewControllers){
            if ([viewController isKindOfClass:[MMHOrderDetailViewController class]]){
                [self.navigationController popToViewController:viewController animated:YES];
            }
        }
    }
}

#pragma mark - 接口
-(void)sendRequestWithSetPayPasswordPayPassword:(NSString *)payPassword{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithSetPayPasswordWithPhone:weakSelf.transferPhoneNumber payPassword:payPassword from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([textFieldPwd isFirstResponder]){
            [textFieldPwd resignFirstResponder];
        }
        [strongSelf.view showTips:@"设置成功"];
        [strongSelf performSelector:@selector(poptoLastViewController) withObject:nil afterDelay:.8f];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}
@end
