//
//  MMHPayView.h
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 支付页面
#import <UIKit/UIKit.h>

typedef void(^backPassCodeBlock)(NSString *passCode);
typedef void(^resetRedirectBlock)();
typedef void(^cancelPayBlock)();                                /**< 取消支付*/

@class MMHPayView;
@protocol MMHPayViewDelegate <NSObject>
@optional
-(NSString *)confirmButtonClick:(NSString *)passCode;           // 验证返回按钮是否正确
@end

@interface MMHPayView : UIView

@property (nonatomic,assign)id <MMHPayViewDelegate>payViewDelegate;
-(void)payViewShow;
-(void)payViewShowInView:(UIView *)view;
@property (nonatomic,copy)backPassCodeBlock passCodeBlock;
@property (nonatomic,copy)resetRedirectBlock resetRedirectBlock;
@property (nonatomic,copy)cancelPayBlock cancelPayBlock;                /**< 取消支付*/
@end
