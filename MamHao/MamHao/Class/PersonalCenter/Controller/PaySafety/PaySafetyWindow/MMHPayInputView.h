//
//  MMHPayInputView.h
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kInputNumberCount 6

static NSString *payInputViewCancelButtonClick = @"payInputViewCancelButtonClick";
static NSString *payInputViewConfirmButtonClick = @"payInputViewConfirmButtonClick";
static NSString *payInputViewPassCodeKey = @"payInputViewPassCodeKey";

typedef void(^findMyPwdBlock)();

@class MMHPayInputView;
@protocol MMHPayInputViewDelegate <NSObject>

@optional
-(void)payinputView:(MMHPayInputView *)payInputView confirmButton:(UIButton *)confirmButton;
-(void)payinputView:(MMHPayInputView *)payInputView cancelButton:(UIButton *)cancelButton;
@end

@interface MMHPayInputView : UIView
@property (nonatomic,assign)id<MMHPayInputViewDelegate>payInputDelegate;
@property (nonatomic,copy)findMyPwdBlock findMyPwdBlock;
@end

