//
//  MMHPaySafetyPasswordSettingViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【设置支付密码】
#import "AbstractViewController.h"
#import "MMHPaySafetyPassCodeViewController.h"
#import "MMHSettingViewController.h"
#import "MMHConfirmationOrderViewController.h"
#import "MMHOrderRootViewController.h"
#import "MMHOrderDetailViewController.h"

@class MMHPaySafetyPassCodeViewController;
@class MMHSettingViewController;
@class MMHConfirmationOrderViewController;
@class MMHOrderRootViewController;
@class MMHOrderDetailViewController;

typedef NS_ENUM(NSInteger, MMHEditPwdType) {
    EditPwdTypeNewPwd,                  //  打开密码锁，重新设置密码
    EditPwdTypeConfirmPwd,              //  新密码确认
    EditPwdTypeOldPwd,                  //  更改密码前输入旧密码
    EditPwdTypeChangePwd,               //  更改密码，输入新密码
    EditPwdTypeChangeConfirmPwd,        //  更改密码，确认新密码
    EditPwdTypeLockOff,                 //  关闭密码锁，取消密码
    EditPwdTypeCheckPwd,                //  从后台切换回来，需要输入密码验证
};

@interface MMHPaySafetyPasswordSettingViewController : AbstractViewController

@property (nonatomic, assign)MMHEditPwdType currentEditType;
@property (nonatomic,copy)NSString *transferPhoneNumber;
@property (nonatomic,assign)MMHPaySafetyPageType paySafetPageType;          /**< 跳转进入的页面*/
@end
