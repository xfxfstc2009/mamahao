//
//  MMHCouponsViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCouponsViewController.h"
#import "HTHorizontalSelectionList.h"               // segment
#import "MMHCouponsCell.h"                          // 优惠券
#import "MMHCouponsListModel.h"                     // 优惠券列表
#import "MMHNetworkAdapter+Center.h"

@interface MMHCouponsViewController()<HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    UIView *searchView;                     // 头部的view
    UITextField *inputCouponsTextField;     // 输入inputField
}
@property (nonatomic,strong) UITableView *couponsTableView;
@property (nonatomic,strong) NSMutableArray *dataSourceMutableArr;
@property (nonatomic,strong) HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong) NSArray *segmentItemArray;
@property (nonatomic,strong) NSMutableArray *isSelectedMutableArr;              /**< 选中的优惠券数组*/
@property (nonatomic,strong) UIButton *navBarRightButton;                       /**< 右侧使用按钮*/
@end

@implementation MMHCouponsViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createSearchView];
    [self createSegment];
    [self arrayWithInit];
    [self createTableView];

    __weak typeof(self)weakSelf = self;
    if (self.couponsUsingType == MMHCouponsUsingTypeNormal){
        self.navBarRightButton.hidden = YES;
        [weakSelf sendRequestWithGetCouponsListWithAvilableType:(weakSelf.segmentList.selectedButtonIndex + 1)];
    } else if (self.couponsUsingType == MMHCouponsUsingTypeOrderConfirmation){
        self.segmentList.hidden = YES;
        searchView.hidden = YES;
        [weakSelf sendRequestWithGetCouponsListWithAvilableType:1];
        weakSelf.couponsTableView.frame = weakSelf.view.bounds;
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.couponsUsingType == MMHCouponsUsingTypeNormal){
        self.barMainTitle = @"优惠券";
    } else {
        self.barMainTitle = @"使用优惠券";
    }
    __weak typeof(self)weakSelf = self;
    self.navBarRightButton = [weakSelf rightBarButtonWithTitle:@"使用" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        MMHCouponsSingleModel *singleCouponsModel = [strongSelf.isSelectedMutableArr lastObject];
        strongSelf.couponsUsingBlock(singleCouponsModel);
        [strongSelf popWithAnimation];
    }];
    
}

-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
    self.isSelectedMutableArr = [NSMutableArray array];
}

#pragma mark - HTHorizontalSelectionList
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(searchView.frame), self.view.frame.size.width, 40);
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.font = F4;
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    self.segmentItemArray = @[@"当前",@"历史"];
    self.segmentList.isNotScroll = YES;
    [self.view addSubview:self.segmentList];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 13, 1, 14)];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.centerX = self.segmentList.centerX;
    [self.segmentList addSubview:lineView];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentItemArray.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentItemArray objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self sendRequestWithGetCouponsListWithAvilableType:(self.segmentList.selectedButtonIndex + 1)];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.couponsTableView){
        self.couponsTableView = [[UITableView alloc] initWithFrame:CGRectMake(MMHFloat(10), CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width - 2 * MMHFloat(10), kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame)) style:UITableViewStylePlain];
        self.couponsTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.couponsTableView.delegate = self;
        self.couponsTableView.dataSource = self;
        self.couponsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.couponsTableView.backgroundColor = [UIColor clearColor];
        self.couponsTableView.showsVerticalScrollIndicator = NO;
        [self.view addSubview:self.couponsTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentify = @"cellIdentify";
    MMHCouponsCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell){
        cell = [[MMHCouponsCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        // 创建背景
        UIImageView *cellBackgroundImageView = [[UIImageView alloc]init];
        cellBackgroundImageView.stringTag = @"cellBackgroundImageView";
            cellBackgroundImageView.image = [UIImage imageNamed:@"coupons_img_semi-circle"];
        cell.backgroundView = cellBackgroundImageView;
    }
    MMHCouponsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
    cell.couponsSingleModel = singleModel;
    UIImageView *cellbackgroundImageView = (UIImageView *)[cell viewWithStringTag:@"cellBackgroundImageView"];
    if (singleModel.status == 1){          // 没使用过
        cellbackgroundImageView.image = [UIImage imageNamed:@"coupons_img_semi-circle"];
    } else if (singleModel.status == 2 || singleModel.status == 3){    // 已使用
        cellbackgroundImageView.image = [UIImage imageNamed:@"coupons_img_used-semi-circle"];
    }
    if (self.couponsUsingType == MMHCouponsUsingTypeOrderConfirmation){
        if ([self.isSelectedMutableArr containsObject:singleModel]){
            cellbackgroundImageView.image = [UIImage imageNamed:@"coupons_img_selected"];
        } else {
            cellbackgroundImageView.image = [UIImage imageNamed:@"coupons_img_semi-circle"];
        }
    }
    return cell;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.dataSourceMutableArr.count - 1){
        return 10;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MMHCouponsSingleModel *couponsListModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
    if (![self.isSelectedMutableArr containsObject:couponsListModel]){          // 如果不包含
        [self.isSelectedMutableArr removeLastObject];
        [self.isSelectedMutableArr addObject:couponsListModel];
        [tableView reloadData];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    }
}

#pragma mark - ActionClick
-(void)exchangeButtonClick:(UIButton *)sender{
    __weak typeof(self)weakSelf = self;
    [weakSelf goExchange];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.returnKeyType == UIReturnKeyDone) {
        if (!inputCouponsTextField.text.length){
            [self.view showTips:@"请输入兑换码"];
        } else {
            __weak typeof(self)weakSelf = self;
            [weakSelf goExchange];
        }
    }
    return YES;
}

-(void)goExchange{
    if ([inputCouponsTextField isFirstResponder]){
        [inputCouponsTextField resignFirstResponder];
    }
    if (!inputCouponsTextField.text.length){
        [self.view showTips:@"请输入兑换码"];
    } else {
        __weak typeof(self)weakself = self;
        [[MMHNetworkAdapter sharedAdapter] exchangeVoucherWithCdKey:inputCouponsTextField.text from:nil succeededHandler:^(MMHCouponsSingleModel *couponsListModel) {
            if (!weakself){
                return ;
            }
            __strong typeof(weakself)strongSelf = weakself;
            [strongSelf.dataSourceMutableArr insertObject:couponsListModel atIndex:0];
            NSIndexSet *insertSet = [NSIndexSet indexSetWithIndex:0];
            [strongSelf.couponsTableView insertSections:insertSet withRowAnimation:UITableViewRowAnimationRight];
        } failedHandler:^(NSError *error) {
            if (!weakself){
                return ;
            }
            __strong typeof(weakself)strongSelf = weakself;
            [strongSelf.view showTipsWithError:error];
        }];
    }
}

#pragma mark - 接口
-(void)sendRequestWithGetCouponsListWithAvilableType:(NSInteger)avilableType{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchModelGetCouponsListWithAvailableType:avilableType from:nil succeededHandler:^(MMHCouponsListModel *couponsListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.dataSourceMutableArr.count){
            [strongSelf.dataSourceMutableArr removeAllObjects];
        }
        [strongSelf.dataSourceMutableArr addObjectsFromArray:couponsListModel.vouchers];
        [strongSelf.couponsTableView reloadData];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark - CustomView
-(void)createSearchView{
    searchView = [[UIView alloc]init];
    searchView.backgroundColor = [UIColor whiteColor];
    searchView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(53));
    [self.view addSubview:searchView];
    
    UIView *shadowView = [[UIView alloc]init];
    shadowView.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
    shadowView.frame = CGRectMake(MMHFloat(10), MMHFloat(10), MMHFloat(267), MMHFloat(33));
    shadowView.layer.cornerRadius = 3.;
    shadowView.layer.borderWidth = .5;
    shadowView.layer.borderColor =  [UIColor colorWithCustomerName:@"分割线"].CGColor;
    [searchView addSubview:shadowView];
    
    //inputTextField
    inputCouponsTextField = [[UITextField alloc]init];
    inputCouponsTextField.backgroundColor = [UIColor clearColor];
    inputCouponsTextField.placeholder = @"请输入您的优惠券兑换码";
    inputCouponsTextField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
    inputCouponsTextField.font = [UIFont fontWithCustomerSizeName:@"正文"];
    inputCouponsTextField.frame = CGRectMake(MMHFloat(10) + MMHFloat(16), MMHFloat(11), MMHFloat(267) - MMHFloat(20), MMHFloat(30));
    inputCouponsTextField.delegate = self;
    inputCouponsTextField.returnKeyType = UIReturnKeyDone;
    [searchView addSubview:inputCouponsTextField];
    
    // ExchangeButton
    UIButton *exchangeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    exchangeButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    exchangeButton.frame = CGRectMake(CGRectGetMaxX(inputCouponsTextField.frame) + MMHFontSize(13), inputCouponsTextField.frame.origin.y, kScreenBounds.size.width - 2 * MMHFontSize(10) - MMHFontSize(267) - MMHFloat(13), inputCouponsTextField.bounds.size.height);
    [exchangeButton addTarget:self action:@selector(exchangeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [exchangeButton setTitle:@"立刻兑换" forState:UIControlStateNormal];
    exchangeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [exchangeButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    exchangeButton.layer.cornerRadius = 5.;
    [searchView addSubview:exchangeButton];
    
    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor =[UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, searchView.bounds.size.height - .5f, kScreenBounds.size.width, .5f);
    [searchView addSubview:lineView];
}



@end
