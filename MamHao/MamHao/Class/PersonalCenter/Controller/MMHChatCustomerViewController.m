//
//  MMHChatCustomerViewController.m
//  MamHao
//
//  Created by fishycx on 15/5/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChatCustomerViewController.h"
#import "AbstractViewController+Chatting.h"


@interface MMHChatCustomerViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)UITableView *tableView;

@end

@implementation MMHChatCustomerViewController

#pragma mark - getter method

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(MMHFloat(17), 0, self.view.bounds.size.width - MMHFloat(34), self.view.bounds.size.height) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
    }
    return _tableView;
}

#pragma mark - lifeCircle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
    [self.view addSubview:self.tableView];
    [self pageSetting];
    // Do any additional setup after loading the view.
}
- (void)pageSetting{
    self.navigationController.navigationBarHidden = NO;
   self.barMainTitle = @"联系客服";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - <UITableViewDataSource>
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
     }
    if (indexPath.section == 0) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MMHFloat(117.5), 33, 24, 24)];
        
        imageView.image = [UIImage imageNamed:@"contact_icon_message"];
        [cell.contentView addSubview:imageView];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+MMHFloat(4),CGRectGetMinY(imageView.frame), 100, 24)];
        label.font = [UIFont systemFontOfSize:20];
        label.textColor = [UIColor hexChangeFloat:@"888888"];
        label.text = @"在线客服";
        [cell addSubview:label];
        
    }else{
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MMHFloat(121), 33, 17, 24)];
        imageView.image = [UIImage imageNamed:@"contact_icon_call"];
        [cell.contentView addSubview:imageView];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+MMHFloat(7.5),(90 - 20 - 8.5 - 15) / 2, 100, 20)];
        label.font = [UIFont systemFontOfSize:20];
        label.textColor = [UIColor hexChangeFloat:@"888888"];
        label.text = @"客服热线";
        UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(label.frame), CGRectGetMaxY(label.frame)+ 8.5, 15*9, 15)];
        numberLabel.font = [UIFont systemFontOfSize:15];
        numberLabel.textColor = [UIColor hexChangeFloat:@"20c2c9"];
        numberLabel.text = CustomerServicePhoneNumber;
        [cell addSubview:numberLabel];
        [cell addSubview:label];
    }
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
    
    
    return view;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
    UILabel *colorLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(17), 20, 5, 15)];
    colorLabel.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [view addSubview:colorLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(colorLabel.frame)+MMHFloat(5), 20, 60, 15)];
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.text = @"人工服务";
    titleLabel.textColor = [UIColor colorWithHexString:@"383d40"];
    [view addSubview:titleLabel];
    
    UILabel *contenLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(titleLabel.frame) + MMHFloat(9),22, self.view.bounds.size.width - CGRectGetMaxX(titleLabel.frame)-MMHFloat(9), 13)];
    contenLabel.font = [UIFont systemFontOfSize:13];
    contenLabel.text = @"服务时间为每天8:30-23:30";
    contenLabel.textColor = [UIColor colorWithHexString:@"888888"];
    [view addSubview:contenLabel];
    return view;
}

#pragma mark - <UITableViewDelegate>

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        [self startChattingWithContext:nil];
    }else{
        __weak MMHChatCustomerViewController *weakViewController = self;
        [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 20;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 60;
    }
    return 0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
