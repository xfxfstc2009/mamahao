//
//  MMHPersonalCenterViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPersonalCenterViewController.h"
#import "MMHPersonalCenterScrollView.h"                             // scrollView
#import "MMHOrderRootViewController.h"                              // 订单
#import "MMHSettingViewController.h"                                // 设置
#import "MMHSuggestViewController.h"                                // 投诉建议
#import "MMHChatCustomerViewController.h"                           // 联系客服
#import "MMHFavoritiesViewController.h"                             // 收藏
#import "MMHAddressListViewController.h"                            // 地址选择
#import "MMHCouponsViewController.h"                                // 优惠券
#import "MMHpersonalInfoViewController.h"                           // 个人信息
#import "MMHMamBearViewController.h"                                // 妈豆
#import "MMHNetworkAdapter+Center.h"                                //获取会员信息
#import "MMHAccountSession.h"
#import "MMHIntegralViewController.h"
#import "MamHao-Swift.h"
#import "MMHChattingViewController.h"
#import "MMHChattingViewController+Preparation.h"
#import "AbstractViewController+Chatting.h"
#import "MMHImageView.h"
#import "UIImage+RenderedImage.h"
#import "MMHPersonalCenterOrderButton.h"
#import "MMHPersonalCenterOrderTipsView.h"
#import "MMHPersonalCenterInfoModel.h"

#define Cell_Height 120
#define TABLEVIEWHEADER MMHFloat(190.5)
#define kHeadImg_WH MMHFloat(60.f)                                   // 店铺头像的宽高
#define OtherCell_Height 320
#define HEADER_BGCOLOR [UIColor colorWithRed:166/256. green:166/256. blue:166/256. alpha:1]
#define kSingleWidth (kScreenBounds.size.width - 4 * MMHFloat(10)) / 3.


@interface MMHPersonalCenterViewController()<UITableViewDataSource,UITableViewDelegate>{
    UIImageView *headerbgView;                   // 头部的view
    UIView      *bannerView;                     // bannerView
    MMHImageView *headImg;                        // 头部的image
    UIButton    *shopNameButton;                 // 店铺名称
    MMHPersonalCenterScrollView *parallaxView;   // scrollerView
    UILabel *shopNameLabel;                      // 商店名称label
}
@property (nonatomic,strong)UITableView *personalCenterTableView;
@property (nonatomic,strong)NSArray *iconImageArr;
@property (nonatomic,strong)NSArray *btnTitleArr;
@property (nonatomic,strong)NSArray *stateBtnImageArr;
@property (nonatomic,strong)NSArray *stateTitleArr;
@property (nonatomic,strong)NSMutableArray *subTitleArr;
@property (nonatomic,strong)MMHMemberCenterInfoModel *InfoModel; //会员信息
@property (nonatomic,strong)MMHPersonalCenterInfoModel *personalCenterInfoModel;//个人中心信息。
@property (nonatomic,strong)UILabel *mbean; //妈豆；
@property (nonatomic,strong)UILabel *voucher; //优惠券
@property (nonatomic,strong)MMHPersonalCenterOrderTipsView *tipsView; //订单tips

@end

@implementation MMHPersonalCenterViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    if ([[MMHAccountSession currentSession] alreadyLoggedIn]) {
        //获取个人信息
        [self fetchpersonalInfo];
    }else{
        [self cleanData]; //没有登录的时候，清除信息。
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.title = @"我";
        self.tabBarItem.image = [UIImage imageNamed:@"mmhtab_icon_my_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"mmhtab_icon_my_hlt"];
    }
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createCustomViewWithHeader];
    [self addSelfViewBackgrount:self.view];
    [self createPersonalCenterScrollView];
    [self createOtherHeaderView:parallaxView.scrollView];
}
-(void)cleanData{
    for (int i = 0; i < 5; i++) {
         MMHPersonalCenterOrderTipsView *tipsView0 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:[NSString stringWithFormat:@"%dmsg", i]];
        tipsView0.tipsValue = @"";
        }
        NSInteger mbeanCount = 0;
        NSInteger voucherCount = 0;
        self.mbean.text = [NSString stringWithFormat:@"%ld颗妈豆", (long)mbeanCount];
        self.voucher.text = [NSString stringWithFormat:@"%ld张优惠券", (long)voucherCount];
        UIImage *image = [UIImage imageNamed:@"center_icon_favicon"];
        headImg.image = image;
        shopNameLabel.text = @"未登录";
}
#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的";
}
#pragma mark fetchData

- (void)fetchpersonalInfo{
        [[MMHNetworkAdapter sharedAdapter] getCenterInfoWithMemberId:0 from:self succeededHandler:^(NSDictionary *dic) {
            MMHPersonalCenterInfoModel *infoModel = [[MMHPersonalCenterInfoModel alloc] initWithJSONDict:dic];
            MMHPersonalCenterOrderTipsView *tipsView0 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:@"0Tips"];
            tipsView0.tipsValue = infoModel.waitPayOrderCount;
            MMHPersonalCenterOrderTipsView *tipsView1 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:@"1Tips"];
            tipsView1.tipsValue = infoModel.waitDeliverOrderCount;
            MMHPersonalCenterOrderTipsView *tipsView2 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:@"2Tips"];
            tipsView2.tipsValue = infoModel.waitReceiveOrderCount;
            MMHPersonalCenterOrderTipsView *tipsView3 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:@"3Tips"];
            tipsView3.tipsValue = infoModel.waitCommentOrderCount;
            MMHPersonalCenterOrderTipsView *tipsView4 = (MMHPersonalCenterOrderTipsView *)[self.view viewWithStringTag:@"4Tips"];
            tipsView4.tipsValue = infoModel.refundOrderCount;

            NSInteger mbeanCount = [dic[@"mbeanCount"] integerValue];
            NSInteger voucherCount = [dic[@"voucherCount"] integerValue];
            self.mbean.text = [NSString stringWithFormat:@"%ld颗妈豆", (long)mbeanCount];
            self.voucher.text = [NSString stringWithFormat:@"%ld张优惠券", (long)voucherCount];
            [self.personalCenterTableView reloadData];
            
        } failedHandler:^(NSError *error) {

           // [self.view showTipsWithError:error];
        }];
        
        [[MMHNetworkAdapter sharedAdapter] fetchModelGetMemberCenterInfoFrom:nil succeededHandler:^(MMHMemberCenterInfoModel *memberCenterInfo) {
            self.InfoModel = memberCenterInfo;
            [[MMHAccountSession currentSession] updateMemberInfoWithMemberCenterInfo:memberCenterInfo];
            [headImg updateViewWithImageAtURL:memberCenterInfo.headPic];
            shopNameLabel.text = memberCenterInfo.nickName;
        } failedHandler:^(NSError *error) {
           
        }];
    

}
- (void)fetchData{

    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        [self presentLoginViewControllerWithSucceededHandler:^(){
            [self fetchData];
        }];
    }else {
        [self fetchpersonalInfo];
    
    }
}
#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.iconImageArr = @[@"center_icon_orders", @"center_icon_madou", @"center_icon_integral", @"center_icon_coupons", @"center_icon_coupons_heart", @"center_icon_location", @"center_icon_service", @"center_icon_advice", @"center_icon_cog"];
    self.btnTitleArr = @[@"全部订单", @"妈豆", @"积分", @"优惠券", @"收藏", @"收货地址", @"联系客服", @"意见反馈", @"设置"];
    self.stateBtnImageArr = @[@"center_icon_obligation", @"center_icon_fahuo", @"center_icon_shouhuo", @"center_icon_evaluation", @"center_icon_return"];
    self.stateTitleArr = @[@"待付款", @"待发货", @"待收货", @"待评价", @"退换货"];
    NSArray *array = @[@"查看所有",@"0颗妈豆",@"门店积分",@"0张优惠券",@"我的收藏",@"管理地址",@"",@"",@""];
    self.subTitleArr = [NSMutableArray arrayWithArray:array];
}


#pragma mark - UITableView
-(void)createTableView{
    if (!self.personalCenterTableView){
        self.personalCenterTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.personalCenterTableView.size_height = MMHFloat(10) + (MMHFloat(10) + MMHFloat(122.5)) * 3 + 48;
        self.personalCenterTableView.delegate = self;
        self.personalCenterTableView.dataSource = self;
        self.personalCenterTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.personalCenterTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.personalCenterTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor colorWithRed:244/256. green:244/256. blue:244/256. alpha:1];
        // 创建许多个button
        for (int index = 0 ;index < self.iconImageArr.count; index ++){
            [self createCustomerViewWithButtonIndex:index bgView:cellWithRowOne];
        }
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return MMHFloat(10) + (MMHFloat(10) + MMHFloat(122.5)) * 3;
}
#pragma mark - CustomView
-(void)createCustomViewWithHeader{
    // 1. 添加头部内容
    headerbgView = [[MMHImageView alloc] init];
    headerbgView.frame = CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), TABLEVIEWHEADER + 500);
    headerbgView.clipsToBounds = YES;
    headerbgView.userInteractionEnabled = YES;
    headerbgView.backgroundColor = HEADER_BGCOLOR;
    headerbgView.contentMode = UIViewContentModeScaleToFill;
    headerbgView.image = [UIImage imageNamed:@"center_bg_mh"];
    [parallaxView.foregroundScrollView addSubview:headerbgView];
}
-(void)addSelfViewBackgrount:(UIView *)backgroundView{
    
}

// PersonalCenterScrollView
-(void)createPersonalCenterScrollView{
    parallaxView = [[MMHPersonalCenterScrollView alloc] initWithBackgroundView:headerbgView foregroundView:self.personalCenterTableView];
    parallaxView.frame = kScreenBounds;
    parallaxView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    parallaxView.backgroundHeight = MMHFloat(230.5);
    parallaxView.scrollView.showsVerticalScrollIndicator = NO;
    parallaxView.scrollViewDelegate = self;
    [self.view addSubview:parallaxView];
}

-(void)createOtherHeaderView:(UIView *)View{
    // 1. alphaView
    UIView *alphaView = [[UIView alloc]init];
    alphaView.backgroundColor = [UIColor whiteColor];
    alphaView.alpha = .4f;
    alphaView.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-kHeadImg_WH)/2. - MMHFloat(3),MMHFloat(54.5) - MMHFloat(3), kHeadImg_WH + 2 * MMHFloat(3), kHeadImg_WH + 2 * MMHFloat(3));
    alphaView.layer.cornerRadius = kHeadImg_WH/2. +  MMHFloat(3);
    [View addSubview:alphaView];
    
    // 2. 添加头像
    headImg = [[MMHImageView alloc]initWithFrame: CGRectMake((CGRectGetWidth(self.view.bounds)-kHeadImg_WH)/2.,MMHFloat(54.5), kHeadImg_WH, kHeadImg_WH)];
    headImg.image = [UIImage imageNamed:@"center_icon_favicon"];
    [headImg.layer setCornerRadius:kHeadImg_WH/2.];
    headImg.backgroundColor = [UIColor whiteColor];
    [View addSubview:headImg];
    
    // 2.1 添加头像按钮
    UIButton *headButton = [UIButton buttonWithType:UIButtonTypeCustom];
    headButton.frame = headImg.frame;
    headButton.backgroundColor = [UIColor clearColor];
    [headButton addTarget:self action:@selector(headButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [View addSubview:headButton];
    
    // 3.添加店铺名称
    shopNameLabel = [[UILabel alloc]init];
    shopNameLabel.frame = CGRectMake((CGRectGetWidth(self.view.bounds)-200)/2., CGRectGetMaxY(headImg.frame) + MMHFloat(6), 200, 20);
    shopNameLabel.text = @"未登录";
    shopNameLabel.textAlignment = NSTextAlignmentCenter;
    [shopNameLabel setShadowColor:[UIColor lightGrayColor]];
    [shopNameLabel setShadowOffset:CGSizeMake(0.0f, .5f)];
    shopNameLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    shopNameLabel.textColor = [UIColor whiteColor];
    shopNameLabel.backgroundColor = [UIColor clearColor];
    [View addSubview:shopNameLabel];
    
    // 3.添加右侧的分享按钮
    UIButton *shareButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton addTarget:self action:@selector(shareClick) forControlEvents:UIControlEventTouchUpInside];
    [shareButton setImage:[UIImage imageNamed:@"mmh_icon_message"] forState:UIControlStateNormal];
    shareButton.frame=CGRectMake(kScreenBounds.size.width - (70 - MMHFloat(20)) / 2 - MMHFloat(20) - MMHFloat(10), 10, 70, 70);
    NSLog(@"%.2f",kScreenBounds.size.width);
    [View addSubview:shareButton];
    
    // 4. 创建底部的view
    [self createAlphaViewWithView:View];
}

// createCustomerButton
-(UIView *)createCustomerViewWithButtonIndex:(NSInteger)index bgView:(UITableViewCell *)bgCell{
    
    CGFloat origin_x = MMHFloat(10) + (kSingleWidth + MMHFloat(10)) * (index % 3);
    CGFloat origin_y = MMHFloat(10) + (MMHFloat(122.5)+MMHFloat(10)) * (index / 3);
    
    UIView *singleBgView = [[UIView alloc]init];
    singleBgView.backgroundColor = [UIColor whiteColor];
    singleBgView.stringTag = [NSString stringWithFormat:@"singleBgView%li",(long)index];
    singleBgView.frame = CGRectMake(origin_x, origin_y, kSingleWidth, MMHFloat(122.5));
    singleBgView.clipsToBounds = YES;
    [bgCell addSubview:singleBgView];
    
    // 创建icon
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.image = [UIImage imageNamed:[self.iconImageArr objectAtIndex:index]];
    iconImageView.frame = CGRectMake((singleBgView.bounds.size.width - MMHFloat(32)) / 2, MMHFloat(20), MMHFloat(32), MMHFloat(32));
    iconImageView.backgroundColor = [UIColor clearColor];
    [singleBgView addSubview:iconImageView];
    
    // 创建title
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame)+MMHFloat(15), singleBgView.bounds.size.width, MMHFloat(14));
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [titleLabel setSingleLineText:[self.btnTitleArr objectAtIndex:index]];
    [singleBgView addSubview:titleLabel];
    
    // 创建subTitle
    UILabel *subTitleLabel = [[UILabel alloc]init];
    if (index == 1) {
        self.mbean = subTitleLabel;
    }
    if (index == 3) {
        self.voucher = subTitleLabel;
    }
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    subTitleLabel.frame = CGRectMake(0, CGRectGetMaxY(titleLabel.frame) + MMHFloat(9.5), singleBgView.bounds.size.width, MMHFloat(14));
    subTitleLabel.font = [UIFont systemFontOfSize:12.];
    subTitleLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    subTitleLabel.text = [self.subTitleArr objectAtIndex:index];
    [singleBgView addSubview:subTitleLabel];
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.backgroundColor = [UIColor clearColor];
    actionButton.frame = singleBgView.frame;
    [actionButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    actionButton.tag = (100 + index);
    [bgCell addSubview:actionButton];
    
    return singleBgView;
}

// 创建订单alphaView
-(void)createAlphaViewWithView:(UIView *)view{
    UIView *bgAlphaView = [[UIView alloc]init];
    bgAlphaView.frame =CGRectMake(0,self.personalCenterTableView.frame.origin.y - MMHFloat(60), view.bounds.size.width, MMHFloat(60));
    bgAlphaView.userInteractionEnabled = YES;
    bgAlphaView.backgroundColor = [UIColor hexChangeFloat:@"000000"];
    bgAlphaView.alpha = .2;
    [view addSubview:bgAlphaView];
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0,self.personalCenterTableView.frame.origin.y - MMHFloat(60), view.bounds.size.width, MMHFloat(60))];
    backView.userInteractionEnabled = YES;
    backView.backgroundColor = [UIColor clearColor];
    [view addSubview:backView];
    
    CGFloat btnWidth = view.bounds.size.width / 5;
    for (int btnIndex = 0; btnIndex < 5; btnIndex++) {
        MMHPersonalCenterOrderButton *stateBtn = [MMHPersonalCenterOrderButton buttonWithType:UIButtonTypeCustom];
        stateBtn.frame = CGRectMake(btnWidth * btnIndex, 0, btnWidth, MMHFloat(60));
        stateBtn.tag = 200 + btnIndex;
        [stateBtn setTitle:[self.stateTitleArr objectAtIndex:btnIndex] forState:UIControlStateNormal];
        [stateBtn setImage:[UIImage imageNamed:[self.stateBtnImageArr objectAtIndex:btnIndex]] forState:UIControlStateNormal];
        [stateBtn addTarget:self action:@selector(handleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [stateBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        stateBtn.titleLabel.font = MMHFontOfSize(12.);
        [stateBtn setImageEdgeInsets:UIEdgeInsetsMake(11, 0, 0, 0)];
        [backView addSubview:stateBtn];
        CGRect rect = [stateBtn.imageView convertRect:stateBtn.imageView.bounds toView:view];
        self.tipsView = [[MMHPersonalCenterOrderTipsView alloc] initWithFrame:CGRectMake(rect.origin.x +14 , rect.origin.y - 6, 12, 12)];
        _tipsView.tipsValue = @"0";
        _tipsView.stringTag = [NSString stringWithFormat:@"%dTips", btnIndex];
        [view addSubview:_tipsView];
    }
}

#pragma mark - ActionClick
-(void)shareClick{
    [self startChattingWithContext:nil];
}

-(void)headButtonClick:(UIButton *)sender{
    __weak typeof(self)weakSelf = self;
    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf presentLoginViewControllerWithSucceededHandler:^(){
            [strongSelf fetchData];
        }];
    }else{
        NSLog(@"-------------------->%@", [MMHAccountSession currentSession].token);
        MMHpersonalInfoViewController * personalInfoViewController = [[MMHpersonalInfoViewController alloc]init];
        [self.navigationController pushViewController:personalInfoViewController animated:YES];
    }
}

-(void)buttonClicked:(UIButton *)sender{
    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        [self presentLoginViewControllerWithSucceededHandler:^(){

        }];
    }else{
        UIButton *actionClickButton = (UIButton *)sender;
        if (actionClickButton.tag == 100){                                  //【全部订单】
            MMHOrderRootViewController *orderRootViewController = [[MMHOrderRootViewController alloc]init];
            [self.navigationController pushViewController:orderRootViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderAll];
        } else if (actionClickButton.tag == 101){                           //【妈豆】
            MMHMamBearViewController *mamBearViewController = [[MMHMamBearViewController alloc]init];
            [self.navigationController pushViewController:mamBearViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterMBean];
        } else if (actionClickButton.tag == 103){                           //【优惠券】
            MMHCouponsViewController *couponsViewController = [[MMHCouponsViewController alloc]init];
            couponsViewController.couponsUsingType = MMHCouponsUsingTypeNormal;
            [self.navigationController pushViewController:couponsViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterCoupons];
        } else if (actionClickButton.tag == 104){                           //【收藏】
            MMHFavoritiesViewController *favritiesViewController = [[MMHFavoritiesViewController alloc] init];
            [self.navigationController pushViewController:favritiesViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterCollection];
        } else if (actionClickButton.tag == 102){                           //积分
            MMHIntegralViewController *itegral = [[MMHIntegralViewController alloc] init];
            [self.navigationController pushViewController:itegral animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterIntegral];
        } else if (actionClickButton.tag == 105){                           //【收货地址】
            MMHAddressListViewController *addressListViewController = [[MMHAddressListViewController alloc]init];
            addressListViewController.usingType = MMHAddressListViewControllerUsingPresentCenter;
            [self.navigationController pushViewController:addressListViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterAddress];
        } else if (actionClickButton.tag == 106){                           //【联系客服】
            MMHChatCustomerViewController *chatCustomerViewController = [[MMHChatCustomerViewController alloc] init];
            [self.navigationController pushViewController:chatCustomerViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterLinkCustomer];
        } else if (actionClickButton.tag == 107){                           //【建议投诉】
            MMHSuggestViewController *suggestViewController = [[MMHSuggestViewController alloc] init];
            [self.navigationController pushViewController:suggestViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterFeedback];
        } else if (actionClickButton.tag == 108){                           //【设置】
            MMHSettingViewController *settingViewController = [[MMHSettingViewController alloc] init];
            [self.navigationController pushViewController:settingViewController animated:YES];
        }
    }
}

// 跳转订单
-(void)handleBtnClick:(UIButton *)sender{
    UIButton *handleButton = (UIButton *)sender;
    NSInteger tagInteger = handleButton.tag - 200;
    if (tagInteger == 0){                                                 // 全部
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderAll];
    } else if (tagInteger == 1 ){        //待付款
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderTypePendingPay];
    } else if (tagInteger == 2 ){        //待发货
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderTypeBeShipped];
    } else if (tagInteger == 3 ){        //待收货
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderTypeBeReceipt];
    } else if (tagInteger == 4 ){        //待评价
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderBeEvaluation];
    } else if (tagInteger == 5 ){        //退款退货
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterOrderRefund];
    }

    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        [self presentLoginViewControllerWithSucceededHandler:^(){
            MMHOrderRootViewController *orderRootViewController = [[MMHOrderRootViewController alloc]init];
            orderRootViewController.transferItemSelected = tagInteger + 1;
            [self.navigationController pushViewController:orderRootViewController animated:YES];
        }];
    }else{
        MMHOrderRootViewController *orderRootViewController = [[MMHOrderRootViewController alloc]init];
        orderRootViewController.transferItemSelected = tagInteger + 1;
        [self.navigationController pushViewController:orderRootViewController animated:YES];
    }
}
@end
