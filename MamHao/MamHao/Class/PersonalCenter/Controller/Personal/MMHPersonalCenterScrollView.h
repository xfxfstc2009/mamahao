//
//  MMHPersonalCenterScrollView.h
//  MamHao
//
//  Created by SmartMin on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHPersonalCenterScrollView : UIView
@property (nonatomic, readonly) UIScrollView    *scrollView;
@property (nonatomic, strong)   UIView          *backgroundView;
@property (nonatomic, strong)   UIView          *foregroundView;
@property (nonatomic, strong)   UIScrollView    *backgroundScrollView;
@property (nonatomic, strong)   UIScrollView    *foregroundScrollView;

@property (nonatomic, weak) id<UIScrollViewDelegate> scrollViewDelegate;                            // 代理方法

@property (nonatomic, assign) CGFloat backgroundHeight;                                             // 顶部的高度

- (id)initWithBackgroundView:(UIView *)backgroundView foregroundView:(UIView *)foregroundView;      // 初始化方法

- (void)updateBackgroundFrame;
- (void)updateForegroundFrame;
- (void)updateContentOffset;

@end
