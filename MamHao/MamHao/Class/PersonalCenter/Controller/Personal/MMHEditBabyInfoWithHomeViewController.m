//
//  MMHEditBabyInfoWithHomeViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHEditBabyInfoWithHomeViewController.h"
#import "MMHNotMenuTextField.h"

@interface MMHEditBabyInfoWithHomeViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)NSMutableArray *babyMutableArr;
@property (nonatomic,strong)UITableView *babyTableView;
@property (nonatomic,strong)UITextField *firstTextField;
@property (nonatomic,strong)MMHNotMenuTextField *twoTextField;
@property (nonatomic,copy)NSString *fixedOneString;
@property (nonatomic,copy)NSString *fixedTwoString;
@property (nonatomic,assign)NSInteger isSelected;
@property (nonatomic,copy)NSString *babyBirthday;
@property (nonatomic,strong)UILabel *babyLabel;

@end

@implementation MMHEditBabyInfoWithHomeViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self dataWithInit];
    [self createTableView];
    [self createNotifi];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"编辑宝宝信息";
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerWithAnimation];
    }];
    
    [weakSelf rightBarButtonWithTitle:@"保存" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf saveBabyInfo];
    }];
}

-(void)dataWithInit{
    NSArray *tempArr = @[@[@"选择"],@[@"标语"]];
    self.babyMutableArr = [NSMutableArray arrayWithArray:tempArr];
    
    self.fixedOneString = @"昵称";
    self.fixedTwoString = @"预产期";
}


#pragma mark - UITableView
-(void)createTableView{
    self.babyTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.babyTableView.delegate = self;
    self.babyTableView.dataSource = self;
    self.babyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.babyTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.babyTableView];
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.babyMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionofArr = [self.babyMutableArr objectAtIndex:section];
    return sectionofArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdengityWithRowOne";
            UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowOne.userInteractionEnabled = YES;
                // 1.
                UIView *view1 = [self createCustomViewWithIndex:0];
                view1.stringTag = @"view1";
                UIView *view2 = [self createCustomViewWithIndex:1];
                view2.stringTag = @"view2";
                UIView *view3 = [self createCustomViewWithIndex:2];
                view3.stringTag = @"view3";
                [self createCheckAnimationViewWithCustomerView:view1 imageName:@"animation_Icon_ gravida" checkLabelString:@"未出生" buttonTag:@"view1"];
                [self createCheckAnimationViewWithCustomerView:view2 imageName:@"animation_Icon_girlBaby" checkLabelString:@"公主" buttonTag:@"view2"];
                [self createCheckAnimationViewWithCustomerView:view3 imageName:@"animation_Icon_boyBaby" checkLabelString:@"王子" buttonTag:@"view3"];
                [cellWithRowOne addSubviews:@[view1,view2,view3]];
            }
            UIView *view1 = [cellWithRowOne viewWithStringTag:@"view1"];
            UIView *view2 = [cellWithRowOne viewWithStringTag:@"view2"];
            UIView *view3 = [cellWithRowOne viewWithStringTag:@"view3"];
            UIImageView *checkImageView1 = (UIImageView *)[view1 viewWithStringTag:@"checkImageView"];
            UIImageView *checkImageView2 = (UIImageView *)[view2 viewWithStringTag:@"checkImageView"];
            UIImageView *checkImageView3 = (UIImageView *)[view3 viewWithStringTag:@"checkImageView"];
            if (self.isSelected == 1){
                checkImageView1.image = [UIImage imageNamed:@"address_icon_selected"];
            } else if (self.isSelected == 2){
                checkImageView2.image = [UIImage imageNamed:@"address_icon_selected"];
            } else if (self.isSelected == 3){
                checkImageView3.image = [UIImage imageNamed:@"address_icon_selected"];
            } else {
                checkImageView1.image = [UIImage imageNamed:@"address_icon_untick"];
                checkImageView2.image = [UIImage imageNamed:@"address_icon_untick"];
                checkImageView3.image = [UIImage imageNamed:@"address_icon_untick"];
            }
            
            return cellWithRowOne;
        } else if (indexPath.row == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // 1.
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.stringTag = @"fixedLabel";
                [cellWithRowTwo addSubview:fixedLabel];
                
                // 2. 创建textField
                self.firstTextField = [self createTextField];
                self.firstTextField.placeholder = @"宝宝";
                self.firstTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                [self.firstTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex) {
                    if (buttonIndex == 0){
                        textField.text = @"";
                    }
                    [textField resignFirstResponder];
                }];
                [cellWithRowTwo addSubview: self.firstTextField];
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            // 赋值
            UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = self.fixedOneString;
            CGSize contentSize = [self.fixedOneString sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:fixedLabel.font])];
            fixedLabel.frame = CGRectMake(MMHFloat(11), 0, contentSize.width, cellHeight);
            
            // textField
            CGSize fixedSize = [@"预产期" sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:fixedLabel.font])];
            self.firstTextField.frame = CGRectMake(fixedSize.width + MMHFloat(30), 0,kScreenBounds.size.width - 2 * MMHFloat(11) - fixedSize.width - MMHFloat(30), cellHeight);
            
            
            return cellWithRowTwo;
        } else if (indexPath.row == 2){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleGray;
                // 1.
                self.babyLabel= [[UILabel alloc]init];
                self.babyLabel.backgroundColor = [UIColor clearColor];
                [cellWithRowThr addSubview:self.babyLabel];
                
                // 2. 创建textField
                self.twoTextField = [[MMHNotMenuTextField alloc]init];
                self.twoTextField.backgroundColor = [UIColor clearColor];
                self.twoTextField.clearsOnBeginEditing = YES;
                self.twoTextField.placeholder = @"未选择";
                self.twoTextField.delegate = self;
                self.twoTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
                [self.twoTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex) {
                    [textField resignFirstResponder];
                }];
                self.twoTextField.inputView = [self createDatePicker];
                [cellWithRowThr addSubview: self.twoTextField];
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            // 赋值
            
            CGSize contentSize = [self.fixedTwoString sizeWithCalcFont:self.babyLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.babyLabel.font])];
            self.babyLabel.frame = CGRectMake(MMHFloat(11), 0, contentSize.width, cellHeight);
            
            // textField
            self.twoTextField.frame = self.firstTextField.frame;
            
            
            return cellWithRowThr;
        }
    } else if (indexPath.section == 1){
        static NSString *cellIdengityWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier: cellIdengityWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengityWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowFour.backgroundColor = [UIColor clearColor];
            
            UILabel *footerLabel = [[UILabel alloc]init];
            footerLabel.backgroundColor = [UIColor clearColor];
            footerLabel.text = @"完善宝宝资料将为您推荐适龄品牌和商品";
            footerLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, [MMHTool contentofHeight:footerLabel.font]);
            footerLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            footerLabel.textAlignment = NSTextAlignmentCenter;
            footerLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            
            UILabel *footerSubLabel = [[UILabel alloc]init];
            footerSubLabel.backgroundColor = [UIColor clearColor];
            footerSubLabel.text = @"我们将为您的宝宝推荐优质商品，更好地为您服务";
            footerSubLabel.frame = CGRectMake(0, CGRectGetMaxY(footerLabel.frame), kScreenBounds.size.width, [MMHTool contentofHeight:footerSubLabel.font]);
            footerSubLabel.textAlignment = NSTextAlignmentCenter;
            footerSubLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            footerSubLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            [cellWithRowFour addSubviews:@[footerLabel,footerSubLabel]];
        }
        return cellWithRowFour;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 ){
        return MMHFloat(150);
    } else {
        return 44;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.babyTableView) {
        if (indexPath.section == 0){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] ==2 ) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2){
        if (![self.twoTextField isFirstResponder]){
            [self.twoTextField becomeFirstResponder];
        }
    }
}


-(void)createCheckAnimationViewWithCustomerView:(UIView *)customerView imageName:(NSString *)imageName checkLabelString:(NSString *)checkLabelString buttonTag:(NSString *)buttonStringTag{
    // 2. 创建imageview
    UIImageView *babyImageView = [[UIImageView alloc]init];
    babyImageView.backgroundColor = [UIColor clearColor];
    babyImageView.frame = CGRectMake((customerView.size_width - MMHFloat(75))/2. ,MMHFloat(25), MMHFloat(75), MMHFloat(75));
    babyImageView.image = [UIImage imageNamed:imageName];
    babyImageView.stringTag = @"babyImageView";
    [customerView addSubview:babyImageView];
    customerView.userInteractionEnabled = YES;
    
    CGSize contentOfSize = [checkLabelString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    CGFloat checkImageOrigin_x = (kScreenBounds.size.width / 3. - MMHFloat(20 + 8)- contentOfSize.width) / 2.;
    
    // 4.1 创建checkImage
    UIImageView *checkImageView = [[UIImageView alloc]init];
    checkImageView.backgroundColor = [UIColor clearColor];
    checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    checkImageView.frame = CGRectMake(checkImageOrigin_x, CGRectGetMaxY(babyImageView.frame) + MMHFloat(15), MMHFloat(20), MMHFloat(20));
    checkImageView.stringTag = @"checkImageView";
    [customerView addSubview:checkImageView];
    checkImageView.userInteractionEnabled = NO;
    
    // 4.2 创建label
    UILabel *checkLabel = [[UILabel alloc]init];
    checkLabel.backgroundColor = [UIColor clearColor];
    checkLabel.text = checkLabelString;
    checkLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    CGFloat contentHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"正文"]];
    checkLabel.frame = CGRectMake(CGRectGetMaxX(checkImageView.frame) + MMHFloat(8), CGRectGetMaxY(babyImageView.frame) + MMHFloat(15) + (MMHFloat(20) - contentHeight) /2., contentOfSize.width, contentHeight);
    [customerView addSubview:checkLabel];
    checkLabel.userInteractionEnabled = NO;
    
    // 2.1 创建imageViewButton
    UIButton *babyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    babyButton.backgroundColor = [UIColor clearColor];
    babyButton.stringTag = buttonStringTag;
    [babyButton addTarget:self action:@selector(buttonBabyClick:) forControlEvents:UIControlEventTouchUpInside];
    babyButton.frame = babyImageView.frame;
    [customerView addSubview:babyButton];
    babyButton.size_height = babyImageView.bounds.size.height + MMHFloat(45);
}

#pragma mark UITextField 
-(UITextField *)createTextField{
    UITextField *tempTextField = [[UITextField alloc]init];
    tempTextField.backgroundColor = [UIColor clearColor];
    tempTextField.clearsOnBeginEditing = YES;
    return tempTextField;
}

#pragma mark createCustomView
-(UIView *)createCustomViewWithIndex:(NSInteger)index{
    UIView *customView = [[UIView alloc]init];
    customView.backgroundColor = [UIColor clearColor];
    customView.frame = CGRectMake((kScreenBounds.size.width / 3.) *index, 0, kScreenBounds.size.width / 3., MMHFloat(150));
    return customView;
}

#pragma mark - ActionClick
-(void)buttonBabyClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    if ([button.stringTag isEqualToString:@"view1"]){
        self.isSelected = 1;
    } else if ([button.stringTag isEqualToString:@"view2"]){
        self.isSelected = 2;
    } else if ([button.stringTag isEqualToString:@"view3"]){
        self.isSelected = 3;
    }
    
    UITableViewCell *cell = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UIView *babyBgView = (UIView *)[cell viewWithStringTag:button.stringTag];
    UIImageView *checkImageViewOne = (UIImageView *)[babyBgView viewWithStringTag:@"checkImageView"];
    [self animationWithCollectionWithButton:checkImageViewOne collection:YES];
    
    if ([[self.babyMutableArr objectAtIndex:0] count] == 1){
        if (self.isSelected == 1){
            [self.babyMutableArr replaceObjectAtIndex:0 withObject:@[@"选择",@"昵称",@"预产期"]];
        } else {
            [self.babyMutableArr replaceObjectAtIndex:0 withObject:@[@"选择",@"昵称",@"生日"]];
        }
        NSIndexSet *inset = [NSIndexSet indexSetWithIndex:0];
        [self.babyTableView reloadSections:inset withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.firstTextField becomeFirstResponder];
    }
    if (self.isSelected == 1){
        self.babyLabel.text = @"预产期";
    } else {
        self.babyLabel.text = @"生日";
    }
    UITableViewCell *cellWithRowOne = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    UIView *view1 = [cellWithRowOne viewWithStringTag:@"view1"];
    UIView *view2 = [cellWithRowOne viewWithStringTag:@"view2"];
    UIView *view3 = [cellWithRowOne viewWithStringTag:@"view3"];
    UIImageView *checkImageView1 = (UIImageView *)[view1 viewWithStringTag:@"checkImageView"];
    UIImageView *checkImageView2 = (UIImageView *)[view2 viewWithStringTag:@"checkImageView"];
    UIImageView *checkImageView3 = (UIImageView *)[view3 viewWithStringTag:@"checkImageView"];
    if (self.isSelected == 1){
        checkImageView1.image = [UIImage imageNamed:@"address_icon_selected"];
        checkImageView2.image = [UIImage imageNamed:@"address_icon_untick"];
        checkImageView3.image = [UIImage imageNamed:@"address_icon_untick"];
    } else if (self.isSelected == 2){
        checkImageView2.image = [UIImage imageNamed:@"address_icon_selected"];
        checkImageView1.image = [UIImage imageNamed:@"address_icon_untick"];
        checkImageView3.image = [UIImage imageNamed:@"address_icon_untick"];
    } else if (self.isSelected == 3){
        checkImageView3.image = [UIImage imageNamed:@"address_icon_selected"];
        checkImageView1.image = [UIImage imageNamed:@"address_icon_untick"];
        checkImageView2.image = [UIImage imageNamed:@"address_icon_untick"];
    }

    
}


#pragma mark check动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection{
    if (isCollection){
        collectionButton.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        collectionButton.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    isCollection = !isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}

#pragma mark - createDatePicker
-(UIDatePicker *)createDatePicker{
    // 创建日期选择器
    UIDatePicker *beginDatePicker = [[UIDatePicker alloc]init];
    beginDatePicker.datePickerMode = UIDatePickerModeDate;
    [beginDatePicker addTarget: self action: @selector(onDatePickerChanged:) forControlEvents:UIControlEventValueChanged];
    return beginDatePicker;
}

-(void)saveBabyInfo{
    if (self.isSelected != 0){
        if (!self.firstTextField.text.length){
            [self.view showTips:@"请输入昵称"];
        } else if (!self.twoTextField.text.length){
            [self.view showTips:@"请选择宝贝生日"];
        } else {
            if (self.isSelected == 1){
                [MMHTool userDefaulteWithKey:@"BabyMaMState" Obj:@"孕妈"];                    // 宝妈状态
                [MMHTool userDefaulteWithKey:@"BabyBirthday" Obj:self.twoTextField.text];    // 宝宝生日
                [MMHTool userDefaulteWithKey:@"BabyNickname" Obj:self.firstTextField.text];  // 宝宝昵称
            } else{
                [MMHTool userDefaulteWithKey:@"BabyMaMState" Obj:@"宝妈"];
                [MMHTool userDefaulteWithKey:@"BabyBirthday" Obj:self.twoTextField.text];
                [MMHTool userDefaulteWithKey:@"BabyNickname" Obj:self.firstTextField.text];
                if (self.isSelected == 2){
                    [MMHTool userDefaulteWithKey:@"BabyGender" Obj:@"2"];        // 宝宝性别
                } else {
                    [MMHTool userDefaulteWithKey:@"BabyGender" Obj:@"1"];
                }
            }
//            [[NSNotificationCenter defaultCenter] postNotificationName:MMHChildInfoEditedNotification object:nil];
            [[UIAlertView alertViewWithTitle:nil message:@"宝贝信息保存成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [self dismissViewControllerWithAnimation];
            }]show];
        }
    } else {
        [self.view showTips:@"请选择宝贝状态"];
    }
}

-(void)onDatePickerChanged:(UIDatePicker *)datePicker{
    UIDatePicker *datepicker = (UIDatePicker *)datePicker;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSTimeInterval maxTimeInterval = 280 * 24 * 60 * 60 ;
    
    if (self.isSelected == 1){
        datepicker.minimumDate = [NSDate date];
        datepicker.maximumDate = [[NSDate alloc]initWithTimeIntervalSinceNow:  maxTimeInterval];
    } else {
        datepicker.maximumDate = [NSDate date];
    }
    NSDate *select  = [datepicker date];
    self.twoTextField.text = [dateFormatter stringFromDate:select];
    self.babyBirthday = [dateFormatter stringFromDate:select];
}

#pragma mark - UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField != self.firstTextField){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.twoTextField.text = [dateFormatter stringFromDate:[NSDate date]];
    }
}

#pragma mark - 通知
#pragma mark 手势通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.babyTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.babyTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
@end
