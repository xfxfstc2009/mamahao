//
//  MMHUpdatePersonalInfoViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^nickNameBlock)(NSString *nickName);

@interface MMHUpdatePersonalInfoViewController : AbstractViewController

@property (nonatomic,copy)nickNameBlock nickNameBlock;
@property (nonatomic,copy)NSString *transferNickName;


@end
