//
//  MMHUpdateBabyInfoViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHBabyInfoListModel.h"
#import "MMHMemberCenterInfoModel.h"
typedef void(^updateBabyBlock)(MMHBabyInfoModel *babyInfoModel);
typedef void(^saveBabyBlock)(MMHBabyInfoModel *babyInfoModel);
@interface MMHUpdateBabyInfoViewController : AbstractViewController

@property (nonatomic,strong)MMHMemberCenterInfoModel *memberInfoModel;
@property (nonatomic,strong)MMHBabyInfoModel *transferBabyInfoModel;
// block
@property (nonatomic,copy)updateBabyBlock updateBabyBlock;
@property (nonatomic,copy)saveBabyBlock saveBabyBlock;

@end
