//
//  MMHUpdateBabyInfoViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
#import "MMHUpdateBabyInfoViewController.h"
#import "MMHNetworkAdapter+Center.h"
#import "MMHAssetLibraryViewController.h"
#import "MMHNetworkAdapter+Image.h"
#import "MMHNotMenuTextField.h"

@interface MMHUpdateBabyInfoViewController()<UITableViewDataSource,UITableViewDelegate>{
    UIDatePicker *birthdayPicker;
}

@property (nonatomic, strong) UITableView *babyTableView;
@property (nonatomic, strong) NSArray *babyArray;

//@property (nonatomic,strong)UIDatePicker *birthdayPicker;

@property (nonatomic, strong)MMHImageView *babyPortrait;
@property (nonatomic, strong)UIImage *image;
@property (nonatomic,strong)MMHBabyInfoModel *babyInfoModel;
@property (nonatomic, strong)NSMutableArray *babyPortaitArray; //用户从相册选的图片
@property (nonatomic, assign)BOOL isRightButtonClicked;
@property (nonatomic,strong)MMHBabyInfoModel *tempBabyInfoModel;
@end


@implementation MMHUpdateBabyInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人中心";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"保存" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!self.isRightButtonClicked) {
            
            [strongSelf logicalJudgmentToSendRequest];
        }
    }];
}

#pragma - arrayWithInit
-(void)arrayWithInit{
    self.babyArray = @[@"头像",@"昵称",@"性别",@"生日"];
    self.babyInfoModel = [[MMHBabyInfoModel alloc]init];
    if (self.transferBabyInfoModel){
        self.babyInfoModel = self.transferBabyInfoModel;
    }
    self.babyPortaitArray = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.babyTableView){
        self.babyTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.babyTableView.size_height = MMHFloat(10) + (MMHFloat(10) + MMHFloat(122.5)) * 3 + 48;
        self.babyTableView.delegate = self;
        self.babyTableView.dataSource = self;
        self.babyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.babyTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.babyTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.babyArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            // 创建headerImage
            MMHImageView *babyHeaderImageView = [[MMHImageView alloc]init];
            self.babyPortrait = babyHeaderImageView;
            babyHeaderImageView.backgroundColor = [UIColor clearColor];
            [babyHeaderImageView setPlaceholderImageName:@"center_picture_graygirl"];
            babyHeaderImageView.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(90)) / 2., MMHFloat(15), MMHFloat(90), MMHFloat(90));
            babyHeaderImageView.stringTag = @"babyHeaderImageView";
            babyHeaderImageView.layer.cornerRadius = MMHFloat(43);
            [cellWithRowOne addSubview:babyHeaderImageView];
        }
        //赋值
        MMHImageView *babyHeaderImageView = (MMHImageView *)[cellWithRowOne viewWithStringTag:@"babyHeaderImageView"];
        
        [babyHeaderImageView updateViewWithImageAtURL:self.babyInfoModel.babyImg.length?self.babyInfoModel.babyImg:@""];
        
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleGray;
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            
            // 创建左侧文字
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(11), 0, MMHFloat(35), cellHeight);
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [cellWithRowTwo addSubview:fixedLabel];
            
            // 创建textField
            MMHNotMenuTextField *inputInfoTextField = [[MMHNotMenuTextField alloc]init];
            inputInfoTextField.backgroundColor = [UIColor clearColor];
            inputInfoTextField.textAlignment = NSTextAlignmentLeft;
            inputInfoTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            inputInfoTextField.textColor = [UIColor colorWithCustomerName:@"黑"];
            inputInfoTextField.stringTag = @"inputInfoTextField";
            inputInfoTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(35), 0, kScreenBounds.size.width - CGRectGetMaxX(fixedLabel.frame) - MMHFloat(35) - 2 * MMHFloat(11), cellHeight);
            [cellWithRowTwo addSubview:inputInfoTextField];
            
//            // 创建dataPicker
            if (indexPath.row == 3){
                    birthdayPicker = [[UIDatePicker alloc]init];
                    birthdayPicker.datePickerMode = UIDatePickerModeDate;
                birthdayPicker.backgroundColor = [UIColor whiteColor];
                NSDate *date = [NSDate date];
               [birthdayPicker setMaximumDate:date];
                                    birthdayPicker.bounds = CGRectMake(0, 0, kScreenBounds.size.width, 100);
                [birthdayPicker addTarget: self action: @selector(onDatePickerChanged:) forControlEvents:UIControlEventValueChanged]; // 注册当datepicker值改变时触发事件
                /**
                 *  pan
                 */
                inputInfoTextField.inputView = birthdayPicker;
            }
        }
        // 赋值
        UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [self.babyArray objectAtIndex:indexPath.row];
        
        // textField
        UITextField *inputInfoTextField = (UITextField *)[cellWithRowTwo viewWithStringTag:@"inputInfoTextField"];
        if (indexPath.row == 1){                  // 昵称
            inputInfoTextField.placeholder = @"请给宝贝起个昵称吧";
            inputInfoTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
            inputInfoTextField.text = self.babyInfoModel.babyNickName.length?self.babyInfoModel.babyNickName:@"";
        }else if (indexPath.row == 2){            // 性别
            inputInfoTextField.placeholder = @"未选择";
            inputInfoTextField.clearButtonMode = UITextFieldViewModeNever;
            inputInfoTextField.userInteractionEnabled = NO;
            inputInfoTextField.enabled = YES;
            if (self.babyInfoModel.babyGender){
                if (self.babyInfoModel.babyGender == 1){            // 王子
                    inputInfoTextField.text = @"王子";
                } else {
                    inputInfoTextField.text = @"公主";
                }
            }
        } else if (indexPath.row == 3){           // 生日
            inputInfoTextField.placeholder = @"未选择";
            inputInfoTextField.clearButtonMode = UITextFieldViewModeNever;
            if (self.babyInfoModel.babyBirthday){
                NSArray *babyBirthdayTempArr = [self.babyInfoModel.babyBirthday componentsSeparatedByString:@" "];
                if (babyBirthdayTempArr.count == 2){
                    inputInfoTextField.text = [babyBirthdayTempArr objectAtIndex:0];
                } else {
                    inputInfoTextField.text = self.babyInfoModel.babyBirthday;
                }
            } else {
                inputInfoTextField.text = @"";
            }
        }
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0){
        MMHAssetLibraryViewController *assetLibraryViewController = [[MMHAssetLibraryViewController alloc]init];
        assetLibraryViewController.numberOfCouldSelectingAssets = 1;
        __weak typeof(self)weakSelf = self;
        assetLibraryViewController.selectAssetsBlock = ^(NSArray *imageArr,NSArray *assetArr){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;

            UITableViewCell * headerImageCell = [tableView cellForRowAtIndexPath:indexPath];
            self.babyPortrait= (MMHImageView *)[headerImageCell viewWithStringTag:@"babyHeaderImageView"];
            UIImage *image = [UIImage imageWithCGImage:[[assetArr lastObject]thumbnail]];
            strongSelf.image = image;
            [self.babyPortaitArray addObject:image];
            [self.babyPortrait updateViewWithImage:image];
        };
        UINavigationController *headerImageViewNav = [[UINavigationController alloc]initWithRootViewController:assetLibraryViewController];
        [self.navigationController presentViewController:headerImageViewNav animated:YES completion:nil];
        
    } else if (indexPath.row == 1 || indexPath.row == 3){
        UITableViewCell * nickNameCell = [tableView cellForRowAtIndexPath:indexPath];
        UITextField *nickNameTextField = (UITextField *)[nickNameCell viewWithStringTag:@"inputInfoTextField"];
        [nickNameTextField becomeFirstResponder];
    } else if (indexPath.row == 2){
        UITableViewCell * nickNameCell = [tableView cellForRowAtIndexPath:indexPath];
        UITextField *nickNameTextField = (UITextField *)[nickNameCell viewWithStringTag:@"inputInfoTextField"];
        [[UIActionSheet actionSheetWithTitle:@"性别选择" buttonTitles:@[@"取消",@"王子",@"公主"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                nickNameTextField.text = @"王子";
                if (!self.babyPortaitArray.count) {
                    [self.babyPortrait setPlaceholderImageName:@"animation_Icon_boyBaby"];
                }
            } else if (buttonIndex == 1){
                if (!self.babyPortaitArray.count) {
                    [self.babyPortrait setPlaceholderImageName:@"animation_Icon_girlBaby"];
                }
                nickNameTextField.text = @"公主";
            } else if (buttonIndex == 2){
                // 取消
            }
        }] showInView:self.view];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return MMHFloat(120);
    } else {
        return MMHFloat(45);
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.babyTableView){
        SeparatorType separatorType;
        separatorType  = SeparatorTypeBottom;
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - UIDatePicker
-(void)onDatePickerChanged:(UIDatePicker *)sender{
    UIDatePicker *datepicker = (UIDatePicker *)sender;
        if(datepicker == birthdayPicker){
        NSDate *select  = [birthdayPicker date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *dateAndTime = [dateFormatter stringFromDate:select];
        NSIndexPath *birthdayIndex = [NSIndexPath indexPathForRow:3 inSection:0];
        UITableViewCell *birthdayCell = [self.babyTableView cellForRowAtIndexPath:birthdayIndex];
        UITextField *inputTextField = (UITextField *)[birthdayCell viewWithStringTag:@"inputInfoTextField"];
        inputTextField.text = dateAndTime;
    }
}



#pragma mark  - OtherManager
-(void)logicalJudgmentToSendRequest{
    // 昵称
    UITableViewCell *cellOne = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    UITextField *inputInfoTextFieldOne = (UITextField *)[cellOne viewWithStringTag:@"inputInfoTextField"];
    
    // 性别
    UITableViewCell *cellTwo = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UITextField *inputInfoTextFieldTwo = (UITextField *)[cellTwo viewWithStringTag:@"inputInfoTextField"];
    
    // 生日
    UITableViewCell *cellThr = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
    UITextField *inputInfoTextFieldThr = (UITextField *)[cellThr viewWithStringTag:@"inputInfoTextField"];
    
    //头像
    UITableViewCell *cell0 = [self.babyTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if ([inputInfoTextFieldOne isFirstResponder]){
        [inputInfoTextFieldOne resignFirstResponder];
    } else if ([inputInfoTextFieldTwo isFirstResponder]){
        [inputInfoTextFieldTwo resignFirstResponder];
    } else if ([inputInfoTextFieldThr isFirstResponder]){
        [inputInfoTextFieldThr resignFirstResponder];
    }
    
    if (!inputInfoTextFieldOne.text.length){
        [self.view showTips:@"请填写宝贝昵称"];
        return;
    } else if (!inputInfoTextFieldTwo.text.length){
        [self.view showTips:@"请选择宝贝性别"];
        return;
    } else if (!inputInfoTextFieldThr.text.length){
        [self.view showTips:@"请选择宝贝生日"];
    } else {
        NSLog(@"%@",inputInfoTextFieldOne.text);
        NSLog(@"%@",inputInfoTextFieldTwo.text);
        NSLog(@"%@",inputInfoTextFieldThr.text);
        self.isRightButtonClicked= YES;
        [self.view showProcessingViewWithMessage:@"正在保存宝宝信息"];
        NSInteger babyGender = 0;
        if ([inputInfoTextFieldTwo.text isEqualToString:@"王子"]){
            babyGender = 1;
        } else if ([inputInfoTextFieldTwo.text isEqualToString:@"公主"]){
            babyGender = 2;
        }
        __weak typeof(self) weakSelf = self;
        UIImageView *icon = (UIImageView *)[cell0 viewWithStringTag:@"babyHeaderImageView"];
        [[MMHNetworkAdapter sharedAdapter] uploadPortrait:icon.image from:nil succeededHandler:^(NSString *fileID) {
            
            if (weakSelf.transferBabyInfoModel){
                [weakSelf sendRequestToUpdateBabyInfoWithNickName:inputInfoTextFieldOne.text babyGender:babyGender babyBirthday:inputInfoTextFieldThr.text babyImg:fileID];
                
            } else {
                [weakSelf sendRequestToSaveBabyInfoWithNickName:inputInfoTextFieldOne.text babyGender:babyGender babyBirthday:inputInfoTextFieldThr.text babyImg:fileID];
            }

        } failedHandler:^(NSError *error) {
            if (!weakSelf) {
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.isRightButtonClicked = NO;
            [strongSelf.view hideProcessingView];
        }];
    }
}



#pragma mark - 接口
// 保存宝宝
-(void)sendRequestToSaveBabyInfoWithNickName:(NSString *)nickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg{
    __weak typeof(self)weakSelf =self;
    [[MMHNetworkAdapter sharedAdapter] fetchModelWithBabyNickName:nickName babyGender:babyGender babyBirthday:babyBirthday babyImg:babyImg from:nil succeededHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        NSString *babyHeadPic = dic[@"babyHeadPic"];
        
        strongSelf.tempBabyInfoModel = [[MMHBabyInfoModel alloc]init];
        strongSelf.tempBabyInfoModel.babyBirthday = babyBirthday;
        strongSelf.tempBabyInfoModel.babyGender = babyGender;
        strongSelf.tempBabyInfoModel.babyNickName = nickName;
        strongSelf.tempBabyInfoModel.babyImg = babyHeadPic;
        strongSelf.tempBabyInfoModel.babyId = dic[@"babyId"];
        [strongSelf.view showTips:@"保存宝宝成功"];
        [strongSelf performSelector:@selector(navPop) withObject:strongSelf.tempBabyInfoModel afterDelay:.8f];
    } failedHandler:^(NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.isRightButtonClicked = NO;
        [strongSelf.view showTipsWithError:error];
    }];
}

-(void)navPop{
    self.saveBabyBlock(self.tempBabyInfoModel);
    [self.navigationController popViewControllerAnimated:YES];
}

//  修改宝宝信息
-(void)sendRequestToUpdateBabyInfoWithNickName:(NSString *)nickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg{
    
    
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchModelUpdateBabyWithUpdateBabyInfo:self.transferBabyInfoModel.babyId babyNickName:nickName babyGender:babyGender babyBirthday:babyBirthday babyImg:babyImg from:nil succeededHandler:^(NSDictionary *dic) {
        __weak typeof(self)weakSelf = self;
        
        [[UIAlertView alertViewWithTitle:@"成功" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (buttonIndex == 0){
                MMHBabyInfoModel *babyInfoModel = [[MMHBabyInfoModel alloc] init];
                babyInfoModel.babyId = weakSelf.transferBabyInfoModel.babyId;
                babyInfoModel.babyBirthday = babyBirthday;
                babyInfoModel.babyGender = babyGender;
                babyInfoModel.babyNickName = nickName;
                babyInfoModel.babyImg = dic[@"babyHeadPic"];
                strongSelf.updateBabyBlock(babyInfoModel);
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }]show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark - UIKeyboardNotification
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.babyTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.babyTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    birthdayPicker = nil;
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



@end
