//
//  MMHpersonalInfoViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHpersonalInfoViewController.h"
#import "MMHAssetLibraryViewController.h"                   // 相册
#import "MMHUpdatePersonalInfoViewController.h"             // 修改个人昵称
#import "MMHBabyInfoModel.h"
#import "MMHUpdateBabyInfoViewController.h"                 // 修改宝宝信息
#import "MMHNetworkAdapter+Center.h"        // 接口
#import "MMHNetworkAdapter+Image.h"

@interface MMHpersonalInfoViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *personalInfoTableView;
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;
@property (nonatomic,strong)NSMutableArray *babyDataSourceMutableArr;
@property (nonatomic,strong)MMHMemberCenterInfoModel *memberCenterInfoModel;

@end

@implementation MMHpersonalInfoViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestGetBabyInfo];
    [self sendRequestWithGetMemberCenterInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人信息";
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.babyDataSourceMutableArr = [NSMutableArray array];
    self.dataSourceMutableArr = [NSMutableArray arrayWithObject:@[@"头像",@"昵称",@"身份",@"手机号码"]];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.personalInfoTableView){
        self.personalInfoTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.personalInfoTableView setHeight:self.view.bounds.size.height];
        self.personalInfoTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.personalInfoTableView.delegate = self;
        self.personalInfoTableView.dataSource = self;
        self.personalInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.personalInfoTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.personalInfoTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0 || section == self.dataSourceMutableArr.count - 1){
        NSArray *sectionArr = [self.dataSourceMutableArr objectAtIndex:section];
        return sectionArr.count;
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0){                // 头像
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                CGFloat cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                // 1.  创建label
                UILabel *fixedLabel = [[UILabel alloc]init];
                [self addFixedLabel:fixedLabel andSubCell:cellWithRowOne];
                fixedLabel.frame = CGRectMake(MMHFloat(15), 0, 100, cellheight);
                fixedLabel.stringTag = @"fixedLabel";
                
                // 2. 创建箭头
                UIImageView *accessoryImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
                accessoryImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(20), (cellheight - MMHFloat(18)) / 2., MMHFloat(10), MMHFloat(18));
                [cellWithRowOne addSubview:accessoryImageView];
                
                // 创建头像
                MMHImageView *headerImage = [[MMHImageView alloc]init];
                headerImage.backgroundColor = [UIColor clearColor];
                headerImage.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(20) - MMHFloat(8) - MMHFloat(50), (cellheight - MMHFloat(50)) / 2. , MMHFloat(50), MMHFloat(50));
                headerImage.placeholderImageName = @"members_icon_mamahui";
                headerImage.layer.cornerRadius = MMHFloat(25);
                headerImage.stringTag = @"headerImage";
                [cellWithRowOne addSubview:headerImage];
                
            }
            // 赋值
            UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = @"头像";
            
            MMHImageView *headerImage = (MMHImageView *)[cellWithRowOne viewWithStringTag:@"headerImage"];
            [headerImage updateViewWithImageAtURL:self.memberCenterInfoModel.headPic];
            
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                // 1.  创建label
                UILabel *fixedLabel = [[UILabel alloc]init];
                [self addFixedLabel:fixedLabel andSubCell:cellWithRowTwo];
                fixedLabel.stringTag = @"fixedLabel";
                
                // 2. 创建dymicLabel
                UILabel *dymicLabel = [[UILabel alloc]init];
                dymicLabel.backgroundColor = [UIColor clearColor];
                dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                dymicLabel.textAlignment = NSTextAlignmentRight;
                dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                dymicLabel.stringTag = @"dymicLabel";
                [cellWithRowTwo addSubview:dymicLabel];
                
                // 2. 创建箭头
                UIImageView *accessoryImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
                accessoryImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(20), (cellHeight - MMHFloat(18)) / 2., MMHFloat(10), MMHFloat(18));
                accessoryImageView.stringTag = @"accessoryImageView";
                [cellWithRowTwo addSubview:accessoryImageView];
                
            }
            // 赋值
            CGFloat cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = [[self.dataSourceMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            fixedLabel.frame = CGRectMake(MMHFloat(15),0, 100, cellheight);
            
            UILabel *dymicLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"dymicLabel"];
            dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(20 +8 + 150), 0, MMHFloat(150), cellheight);
            if (indexPath.row == 1){
                dymicLabel.text = self.memberCenterInfoModel.nickName;
            } else if (indexPath.row == 2){
                if (self.memberCenterInfoModel.status == 0){
                    dymicLabel.text = @"未设置";
                } else if (self.memberCenterInfoModel.status == 1){
                    dymicLabel.text = @"孕妈";
                } else if (self.memberCenterInfoModel.status == 2){
                    dymicLabel.text = @"宝妈";
                }
                
            } else if (indexPath.row == 3){
                dymicLabel.text = self.memberCenterInfoModel.phone;
            }
            
            UIImageView *accessoryImageView = (UIImageView *)[cellWithRowTwo viewWithStringTag:@"accessoryImageView"];
            if (indexPath.row == 3){
                accessoryImageView.hidden = YES;
            } else {
                accessoryImageView.hidden = NO;
            }
            
            return cellWithRowTwo;
        }
    } else if (indexPath.section == self.dataSourceMutableArr.count - 1){       // 添加宝宝
        static NSString *cellIdentifyWithCellAdd = @"cellIdentifyWithCellAdd";
        UITableViewCell *cellWithAdd = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithCellAdd];
        if (!cellWithAdd){
            cellWithAdd = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithCellAdd];
            cellWithAdd.selectionStyle = UITableViewCellSelectionStyleNone;
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            cellWithAdd.backgroundColor = [UIColor clearColor];
            
            UIImageView *bgImageView = [UIImageView imageViewWithImageName:@"members_bg_add"];
            cellWithAdd.backgroundView = bgImageView;
            
            // 创建image
            UIImageView *addImageView = [[UIImageView alloc]init];
            addImageView.backgroundColor = [UIColor clearColor];
            addImageView.image = [UIImage imageNamed:@"members_icon_add"];
            [cellWithAdd addSubview:addImageView];
            
            // 创建添加宝宝
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.text = @"添加宝宝";
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
            CGSize contentofSize = [@"添加宝宝" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 30)];
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            CGFloat flagOrigin = (kScreenBounds.size.width - 2 * MMHFloat(10) - contentofSize.width - MMHFloat(10) - 20)/2.;
            
            addImageView.frame = CGRectMake(flagOrigin, (44 - 20)/2, 20, 20);
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(addImageView.frame) + MMHFloat(10), 0, contentofSize.width, cellHeight);
            [cellWithAdd addSubview:fixedLabel];
        }
        return cellWithAdd;
    } else {                    // 宝宝内容
        static NSString *cellIdentifyWithBaby = @"cellIdentifyWithBaby";
        UITableViewCell *cellWithBaby = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithBaby];
        if (!cellWithBaby){
            cellWithBaby = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithBaby];
            cellWithBaby.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithBaby.backgroundColor = [UIColor clearColor];
            
            // bgImageView
            UIImageView *bgImageView = [[UIImageView alloc]init];
            bgImageView.image = [UIImage imageNamed:@"members_bg_baobao-1"];
            cellWithBaby.backgroundView = bgImageView;

            [self createCustomViewWithSubView:cellWithBaby index:0 dymicLabelString:@"卷子馒头油条"];
            [self createCustomViewWithSubView:cellWithBaby index:1 dymicLabelString:@"王子"];
            [self createCustomViewWithSubView:cellWithBaby index:2 dymicLabelString:@"2015-02-23"];
            
            // 创建头像
            MMHImageView *babyImageView = [[MMHImageView alloc]init];
            babyImageView.backgroundColor = [UIColor clearColor];
            babyImageView.stringTag = @"babyImageView";
            CGFloat cellHeitht = [tableView rectForRowAtIndexPath:indexPath].size.height;
            babyImageView.frame = CGRectMake(kScreenBounds.size.width - 10 - MMHFloat(90) - MMHFloat(29), (cellHeitht - MMHFloat(90)) /2., MMHFloat(90), MMHFloat(90));
            babyImageView.layer.cornerRadius = MMHFloat(44);
            babyImageView.image = [UIImage imageNamed:@"animation_Icon_boyBaby"];
            [cellWithBaby addSubview:babyImageView];
            
            UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
            deleteButton.backgroundColor = [UIColor clearColor];
            deleteButton.tag = 301;
            [deleteButton addTarget:self action:@selector(deleteBabyClick:) forControlEvents:UIControlEventTouchUpInside];
            deleteButton.frame = CGRectMake(kScreenBounds.size.width - 10 -MMHFloat(9) - MMHFloat(40), MMHFloat(9), MMHFloat(40), MMHFloat(40));
            [deleteButton setImage:[UIImage imageNamed:@"pic_btn_close"] forState:UIControlStateNormal];
            [cellWithBaby addSubview:deleteButton];
        }
        UIButton *deleteButton = (UIButton *)[cellWithBaby viewWithTag:301];
        deleteButton.stringTag = [NSString stringWithFormat:@"deleteButton-%li",(long)indexPath.section];
        
        MMHBabyInfoModel *babyInfoModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        UILabel *dymicLabel1 = (UILabel *)[cellWithBaby viewWithStringTag:@"dymicLabel0"];
        UILabel *dymicLabel2 = (UILabel *)[cellWithBaby viewWithStringTag:@"dymicLabel1"];
        UILabel *dymicLabel3 = (UILabel *)[cellWithBaby viewWithStringTag:@"dymicLabel2"];
        dymicLabel1.text = babyInfoModel.babyNickName;
        dymicLabel2.text = babyInfoModel.babyGender == 1?@"王子":@"公主";
        
        NSArray *birthdayTempArr = [babyInfoModel.babyBirthday componentsSeparatedByString:@" "];
        if (birthdayTempArr.count == 2){
            dymicLabel3.text = [birthdayTempArr objectAtIndex:0];
        } else {
            dymicLabel3.text = babyInfoModel.babyBirthday;
        }
        
        MMHImageView *babyImageView = (MMHImageView *)[cellWithBaby viewWithStringTag:@"babyImageView"];
        if (babyInfoModel.babyImg.length){
            [babyImageView updateViewWithImageAtURL:babyInfoModel.babyImg];
        } else {
            if (babyInfoModel.babyGender == 1){         // 【王子】
                babyImageView.image = [UIImage imageNamed:@"animation_Icon_boyBaby"];
            } else {                                    // 【公主】
                babyImageView.image = [UIImage imageNamed:@"animation_Icon_girlBaby"];
            }
        }
        return cellWithBaby;
    }
    return nil;
}

-(void)createCustomViewWithSubView:(UITableViewCell *)cell index:(NSInteger)index dymicLabelString:(NSString *)dymicLabelString{
    // 1. 创建昵称
    UILabel *fixedNickLabel = [[UILabel alloc]init];
    fixedNickLabel.backgroundColor = [UIColor clearColor];
    fixedNickLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:MMHFloat(15 + 2)];
    CGFloat contentWidth = [@"昵称" sizeWithCalcFont:fixedNickLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 17)].width;
    fixedNickLabel.frame = CGRectMake(MMHFloat(27) + 10, MMHFloat(27) + index * (MMHFloat(14 + 26)), contentWidth, 17);
    [cell addSubview:fixedNickLabel];
    
    // 创建lineView
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor lightGrayColor];
    CGFloat lineWidth = kScreenBounds.size.width - 2 * 10 - MMHFloat(27) - contentWidth - MMHFloat(10) - MMHFloat(20) - MMHFloat(90)-MMHFloat(20) - MMHFloat(9);
    lineView.frame = CGRectMake(CGRectGetMaxX(fixedNickLabel.frame) + MMHFloat(10), CGRectGetMaxY(fixedNickLabel.frame), lineWidth, .5f);
    [cell addSubview:lineView];
    
    // 创建dymicLabel
    UILabel *dymicLabel = [[UILabel alloc]init];
    dymicLabel.backgroundColor = [UIColor clearColor];
    dymicLabel.text = dymicLabelString;
    dymicLabel.stringTag = [NSString stringWithFormat:@"dymicLabel%li",(long)index];
    dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    dymicLabel.textAlignment = NSTextAlignmentLeft;
    dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    dymicLabel.frame = CGRectMake(lineView.frame.origin.x, fixedNickLabel.frame.origin.y, lineView.bounds.size.width, [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]]);
    [cell addSubview: dymicLabel];
    
    // 创建button
    if (index == 0){
        fixedNickLabel.text = @"昵称";
    } else if (index == 1){
        fixedNickLabel.text = @"性别";
    } else if (index == 2){
        fixedNickLabel.text = @"生日";
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 0){                         // 【相册】
        MMHAssetLibraryViewController *assetLibraryViewController = [[MMHAssetLibraryViewController alloc]init];
        assetLibraryViewController.numberOfCouldSelectingAssets = 1;
        __weak typeof(self)weakSelf = self;
        assetLibraryViewController.selectAssetsBlock = ^(NSArray *imageArr,NSArray *assetArr){
            if (!weakSelf){
                return ;
            }
        
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UITableViewCell * headerImageCell = [tableView cellForRowAtIndexPath:indexPath];
            UIImageView *headerImageView = (UIImageView *)[headerImageCell viewWithStringTag:@"headerImage"];
            UIImage *image = [UIImage imageWithCGImage:[[assetArr lastObject]thumbnail]];
            headerImageView.image = image;
            
            [[MMHNetworkAdapter sharedAdapter] uploadPortrait:image from:nil succeededHandler:^(NSString *fileID) {
                [strongSelf.view showProcessingView];
                [[MMHNetworkAdapter sharedAdapter] fetchModelUpdateMemberInfoWithType:3 andVaule:fileID from:nil succeededHandler:^{
                    [strongSelf.view hideProcessingView];
                    
                } failedHandler:^(NSError *error) {
                 
                }];
            } failedHandler:^(NSError *error) {
               
            }];
        };
        UINavigationController *headerImageViewNav = [[UINavigationController alloc]initWithRootViewController:assetLibraryViewController];
        [self.navigationController presentViewController:headerImageViewNav animated:YES completion:nil];
    } else if (indexPath.section == 0 && indexPath.row == 1){                   // 【昵称】
        MMHUpdatePersonalInfoViewController * updatePersonalInfo = [[MMHUpdatePersonalInfoViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        updatePersonalInfo.nickNameBlock = ^(NSString *nickName){
            weakSelf.memberCenterInfoModel.nickName = nickName;
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            [self.view showTips:@"昵称已修改"];
        };
        updatePersonalInfo.transferNickName = self.memberCenterInfoModel.nickName;
        [self.navigationController pushViewController:updatePersonalInfo animated:YES];
        
    } else if (indexPath.section == 0 && indexPath.row == 2){
        [[UIActionSheet actionSheetWithTitle:@"身份选择" buttonTitles:@[@"取消",@"孕妈",@"宝妈"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            __weak typeof(self)weakSelf = self;
            if (buttonIndex == 0){
                [weakSelf sendRequestWithUpdateUserInfoWithType:1 typeValue:@"孕妈"];
            } else if (buttonIndex == 1){
                [weakSelf sendRequestWithUpdateUserInfoWithType:1 typeValue:@"宝妈"];
            } else if (buttonIndex == 2){
                // 取消
            }
        }] showInView:self.view];
    } else if (indexPath.section == 0 && indexPath.row == 3){
        return;
    } else {
        MMHUpdateBabyInfoViewController *updateBabyInfoViewController = [[MMHUpdateBabyInfoViewController alloc]init];
        updateBabyInfoViewController.memberInfoModel = self.memberCenterInfoModel;
        if (indexPath.section != self.dataSourceMutableArr.count -1){
            updateBabyInfoViewController.transferBabyInfoModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        }
        __weak typeof(self) weakSelf = self;
        updateBabyInfoViewController.updateBabyBlock = ^(MMHBabyInfoModel *babyInfoModel){
            [weakSelf.dataSourceMutableArr replaceObjectAtIndex:indexPath.section withObject:babyInfoModel];
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
            //xxxxx
        };
        updateBabyInfoViewController.saveBabyBlock = ^(MMHBabyInfoModel *babyInfoModel){
            [weakSelf.personalInfoTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            [weakSelf.dataSourceMutableArr insertObject:babyInfoModel atIndex:1];
            [weakSelf.personalInfoTableView insertSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationRight];
            //xxxxx
        };
        
        [self.navigationController pushViewController:updateBabyInfoViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            return 70;
        } else {
            return 44;
        }
    } else if (indexPath.section == self.dataSourceMutableArr.count - 1){
        return 44;
    } else {
        return MMHFloat(162);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == self.dataSourceMutableArr.count - 1){
        return MMHFloat(18);
    } else {
        return MMHFloat(10);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.dataSourceMutableArr.count - 1){
        return MMHFloat(20);
    } else {
        return 0;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.personalInfoTableView){
        if (indexPath.section == 0){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.dataSourceMutableArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

#pragma mark - CustomView
-(void)addFixedLabel:(UILabel *)newLabel andSubCell:(UITableViewCell *)subCell {
    newLabel.frame = CGRectMake(20, 7, 80, 30);
    newLabel.textAlignment = NSTextAlignmentLeft;
    newLabel.backgroundColor = [UIColor clearColor];
    newLabel.font = [UIFont systemFontOfSize:15.];
    newLabel.textColor = [UIColor blackColor];
    [subCell addSubview:newLabel];
}


#pragma mark - ActionClick
-(void)deleteBabyClick:(UIButton *)sender{
    UIButton *deleteBabyButton = (UIButton *)sender;
    NSLog(@"%@",deleteBabyButton.stringTag);
    NSArray *deleteBabyCompArr = [deleteBabyButton.stringTag componentsSeparatedByString:@"-"];
    if (deleteBabyCompArr.count == 2){
        NSLog(@"第%@个宝宝", [deleteBabyCompArr lastObject]);
        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestWithDeleteBabyWithBabyIndex:[[deleteBabyCompArr lastObject] integerValue]];
    }
}

-(void)reloadTableView:(NSString *)indexString{
    for (NSInteger i = [indexString integerValue]; i < self.dataSourceMutableArr.count ; i++){
        NSIndexSet *reloadSet = [NSIndexSet indexSetWithIndex:i - 1];
        [self.personalInfoTableView reloadSections:reloadSet withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(void)scrollToTop:(id)indexPath{
    [self.personalInfoTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}






#pragma mark - 接口
-(void)sendRequestGetBabyInfo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetMemberBabyFrom:nil succeededHandler:^(MMHBabyInfoListModel *babyInfoList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.dataSourceMutableArr addObjectsFromArray:babyInfoList.babylist];
        [strongSelf.dataSourceMutableArr addObject:@[@"增加"]];
        [strongSelf.personalInfoTableView reloadData];
    } failedHandler:^(NSError *error) {
       [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 删除宝宝
-(void)sendRequestWithDeleteBabyWithBabyIndex:(NSInteger)babyIndex{
    
    
    
    __weak typeof(self)weakSelf = self;
    MMHBabyInfoModel *babyInfoModel = [weakSelf.dataSourceMutableArr objectAtIndex:babyIndex];
    [[UIAlertView alertViewWithTitle:nil message:@"妈妈你不要我了吗？" buttonTitles:@[@"取消", @"删除"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf) {
            return;
        }
    __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (buttonIndex == 1) {
            
            [[MMHNetworkAdapter sharedAdapter] fetchmodelWithDeleteBaby:babyInfoModel.babyId from:nil succeededHandler:^(NSDictionary *dic) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                NSIndexSet *deleteIndexSet = [NSIndexSet indexSetWithIndex:babyIndex];
                [strongSelf.dataSourceMutableArr removeObjectAtIndex:babyIndex];
                [strongSelf.personalInfoTableView deleteSections:deleteIndexSet withRowAnimation:UITableViewRowAnimationLeft];
                [strongSelf performSelector:@selector(reloadTableView:) withObject:[NSString stringWithFormat:@"%li",(long)babyIndex] afterDelay:.5f];
                  [strongSelf.view showTips:@"删除baby成功"];
                //xxxxx
            } failedHandler:^(NSError *error) {
                [weakSelf.view showTipsWithError:error];
            }];
            
           
        }
    }] show];

    
   }

#pragma mark 获取会员信息
-(void)sendRequestWithGetMemberCenterInfo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetMemberCenterInfoFrom:nil succeededHandler:^(MMHMemberCenterInfoModel *memberCenterInfo) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.memberCenterInfoModel = memberCenterInfo;
        [strongSelf.personalInfoTableView reloadData];
        
    } failedHandler:^(NSError *error) {
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 修改会员信息
-(void)sendRequestWithUpdateUserInfoWithType:(NSInteger)infoType typeValue:(NSString *)typeValue{
    __weak typeof(self)weakSelf = self;
    
    NSString *typeAnalytical;
    if ([typeValue isEqualToString:@"孕妈"]){
        typeAnalytical = @"1";
    } else if ([typeValue isEqualToString:@"宝妈"]){
        typeAnalytical = @"2";
    }
    
    [[MMHNetworkAdapter sharedAdapter] fetchModelUpdateMemberInfoWithType:infoType andVaule:typeAnalytical from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (infoType == 1){         // 身份
            if ([typeValue isEqualToString:@"孕妈"]){
                strongSelf.memberCenterInfoModel.status = 1;
            } else if ([typeValue isEqualToString:@"宝妈"]){
                strongSelf.memberCenterInfoModel.status = 2;
            }
            // 刷新
            [strongSelf.personalInfoTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [strongSelf.view showTips:[NSString stringWithFormat:@"您已设置为%@",typeValue]];
        }
        
    } failedHandler:^(NSError *error) {
        [weakSelf.view showTipsWithError:error];
    }];
}

@end
