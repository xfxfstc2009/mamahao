//
//  MMHMamBearViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamBearViewController.h"
#import "MMHMamBearCell.h"                  // cell
#import "MMHBearListModel.h"                // bearModel
#import "MMHWebViewController.h"            // webView
#import "MMHNetworkAdapter+Center.h"        // 接口
#import "MJRefresh.h"                       //加载更多

@interface MMHMamBearViewController()<UITableViewDataSource,UITableViewDelegate>{
    UIView *customView;
    UILabel *numberLabel;
}
@property (nonatomic,strong)UITableView *mamBearTableView;
@property (nonatomic,strong)NSMutableArray *mamBearDataSourceMutableArr;
@property (nonatomic,assign)NSInteger currentPage;
@property (nonatomic,strong)MMHBearListModel *beanListModel;

@end

@implementation MMHMamBearViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    self.currentPage = 1;
    [weakSelf sendRequestWithGetMamBearListWithPage:self.currentPage];
    
}
- (void)viewWillDisappear:(BOOL)animated{
    [self.mamBearTableView removeFooter];
}
#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的妈豆";
}

-(void)arrayWithInit{
    self.currentPage = 1;
    self.mamBearDataSourceMutableArr = [NSMutableArray array];
    __weak typeof(self)weakSelf =self;
    [weakSelf rightBarButtonWithTitle:@"妈豆说明" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)stongSelf = weakSelf;
        MMHWebViewController *webViewController = [[MMHWebViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:MMHHTML_Link_Beans];
        [stongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mamBearTableView){
        self.mamBearTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.mamBearTableView.delegate = self;
        self.mamBearTableView.dataSource = self;
        self.mamBearTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mamBearTableView.backgroundColor = [UIColor clearColor];
        __weak typeof(self)weakSelf = self;
        [weakSelf.mamBearTableView addLegendFooterWithRefreshingBlock:^{
            if (!weakSelf) {
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentPage++;
            [strongSelf sendRequestWithGetMamBearListWithPage:strongSelf.currentPage];
            
        }];
        [self.view addSubview:self.mamBearTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.mamBearDataSourceMutableArr.count+1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellRowOne){
            cellRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            // 创建label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, 150, cellHeight);
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            fixedLabel.text = @"妈豆交易明细";
            [cellRowOne addSubview:fixedLabel];
            
//            // 创建
//            UILabel *dymicLabel = [[UILabel alloc]init];
//            dymicLabel.backgroundColor = [UIColor clearColor];
//            dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
//            CGSize dymicSize = [@"妈豆说明" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
//            dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(10) - MMHFloat(8) - dymicSize.width, 0, dymicSize.width, cellHeight);
//            dymicLabel.textAlignment = NSTextAlignmentRight;
//            dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
//            [cellRowOne addSubview:dymicLabel];
//            dymicLabel.text = @"妈豆说明";
//            
//            // 创建arrow
//            UIImageView *arrawImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
//            arrawImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(10), (MMHFloat(40) - MMHFloat(18))/2., MMHFloat(10), MMHFloat(18));
//            [cellRowOne addSubview:arrawImageView];
        }
        return cellRowOne;
    } else {
        static NSString *cellIdentify = @"cellIdentify";
        MMHMamBearCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[MMHMamBearCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.bearSingleModel = [self.mamBearDataSourceMutableArr objectAtIndex:(indexPath.row -1)];
        return cell;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row !=0){
        return MMHFloat(100);
    } else {
        return MMHFloat(40);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return MMHFloat(10);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 0){
        return MMHFloat(10);
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.mamBearTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == self.mamBearDataSourceMutableArr.count - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
      
    }
}

#pragma mark - CreateCustomView
-(void)createCustomView:(NSString *)number{
    customView = [[UIView alloc]init];
    customView.backgroundColor = [UIColor whiteColor];
    customView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(150));
    [self.view addSubview:customView];
    
    // 创建lineView
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame =CGRectMake(0, customView.bounds.size.height - .5f, kScreenBounds.size.width, .5);
    [customView addSubview:lineView];
    
    // 创建
    numberLabel = [[UILabel alloc]init];
    numberLabel.backgroundColor = [UIColor clearColor];
    numberLabel.text = number;
    numberLabel.textAlignment = NSTextAlignmentCenter;
    numberLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    numberLabel.font = [UIFont systemFontOfSize:50];
    numberLabel.frame = CGRectMake(0, MMHFloat(40), kScreenBounds.size.width, MMHFloat(50));
    [customView addSubview:numberLabel];
    
    // 创建个
    UILabel *fixedNumberLabel = [[UILabel alloc]init];
    fixedNumberLabel.backgroundColor = [UIColor clearColor];
    fixedNumberLabel.text = @"个";
    fixedNumberLabel.font = [UIFont systemFontOfSize:30];
    fixedNumberLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    [customView addSubview:fixedNumberLabel];
    
    // 创建文字信息
    UILabel *dymicLabel = [[UILabel alloc]init];
    dymicLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(numberLabel.frame), kScreenBounds.size.width, MMHFloat(60));
    dymicLabel.text = @"100妈豆=1元，购物可抵现金";
    dymicLabel.textAlignment = NSTextAlignmentCenter;
    dymicLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [customView addSubview:dymicLabel];
    
    // 重新计算
    CGSize contentOfSize = [numberLabel.text sizeWithCalcFont:numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    CGSize fixedNumberOfSize = [@"个" sizeWithCalcFont:[UIFont systemFontOfSize:30] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    numberLabel.size_width = contentOfSize.width;
    CGFloat originX = (kScreenBounds.size.width - contentOfSize.width - fixedNumberOfSize.width)/2.;
    numberLabel.orgin_x = originX;
    fixedNumberLabel.frame = CGRectMake(CGRectGetMaxX(numberLabel.frame),MMHFloat(40) + MMHFloat(50) - [MMHTool contentofHeight:[UIFont systemFontOfSize:40]], fixedNumberOfSize.width, [MMHTool contentofHeight:[UIFont systemFontOfSize:40]]);
}

#pragma mark - 接口
-(void)sendRequestWithGetMamBearListWithPage:(NSInteger)page{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetMamBeanInfoWithPage:page pageSize:5 from:nil succeededHandler:^(MMHBearListModel *beanListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (beanListModel.mbeans.count<5) {
            [strongSelf.mamBearTableView.footer noticeNoMoreData];
        }else{
        
            [strongSelf.mamBearTableView.footer endRefreshing];
        }
        strongSelf.beanListModel = beanListModel;
        
        // 创建header
        if (!strongSelf->customView){
            [strongSelf createCustomView:[NSString stringWithFormat:@"%li",(long)beanListModel.mbeanCount]];
            // 重置tableview
            strongSelf.mamBearTableView.orgin_y = CGRectGetMaxY(strongSelf->customView.frame);
            strongSelf.mamBearTableView.size_height = kScreenBounds.size.height - CGRectGetMaxY(strongSelf->customView.frame) - 64;
        }
        [strongSelf.mamBearDataSourceMutableArr addObjectsFromArray:beanListModel.mbeans];
        [strongSelf.mamBearTableView reloadData];
    } failedHandler:^(NSError *error) {
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mamBearTableView.footer endRefreshing];
        [strongSelf.view showTipsWithError:error];
    }];
}
@end
