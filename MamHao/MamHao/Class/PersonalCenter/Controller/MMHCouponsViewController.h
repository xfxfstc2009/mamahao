//
//  MMHCouponsViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【优惠券】
#import "AbstractViewController.h"
#import "MMHCouponsSingleModel.h"
typedef NS_ENUM(NSInteger, MMHCouponsUsingType) {
    MMHCouponsUsingTypeNormal = 1,                              /**< 正常优惠券*/
    MMHCouponsUsingTypeOrderConfirmation = 2,                   /**< 使用优惠券*/
};


typedef void(^CouponsUsingBlock)(MMHCouponsSingleModel *singleCouponsModel);
@interface MMHCouponsViewController : AbstractViewController

@property (nonatomic,copy)CouponsUsingBlock couponsUsingBlock;              /**< block*/
@property (nonatomic,assign)MMHCouponsUsingType couponsUsingType;           /**< 优惠券使用类型*/
@end
