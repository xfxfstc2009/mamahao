//
//  MMHSettingViewController.m
//  MamHao
//
//  Created by fishycx on 15/5/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSettingViewController.h"
#import <StoreKit/StoreKit.h>                           // 应用内跳appstore

#import "MMHPaySafetyPassCodeViewController.h"          // 支付安全
#import "MMHNetworkAdapter+Center.h"                    // 接口

// model
#import "MMHSettingPaySettingModel.h"                   // 获取设置支付状态
#import "MMHWebViewController.h"
#import "LESAlertView.h"
#import "MMHAccountSession.h"
#import "MMHCurrentLocationModel.h"                     // 地址信息

@interface MMHSettingViewController ()<UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate,SKStoreProductViewControllerDelegate>
@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic,strong)MMHSettingPaySettingModel *paySettingModel;
@end


@implementation MMHSettingViewController


#pragma mark - getter method

- (UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
        _tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"设置";
    [self.view addSubview:self.tableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetSettingStatus];
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.paySettingModel = [[MMHSettingPaySettingModel alloc]init];
}

#pragma mark - private method

-(void)handleQuitBtn:(UIButton *)sender{
    LESAlertView *alertView = [[LESAlertView alloc] initWithTitle:nil
                                message:@"确认退出当前帐号？"
                      cancelButtonTitle:@"取消"
                    cancelButtonHandler:^{
                        
                    }];
    __weak __typeof(self) weakSelf = self;
    [alertView addButtonWithTitle:@"退出"
                          handler:^{
                              [weakSelf.view showProcessingView];
                              [[MMHAccountSession currentSession] logoutWithCompletion:^(BOOL succeeded){
                                  [weakSelf.view hideProcessingView];
                                  [weakSelf clearNormalLocationInfo];
                                  [weakSelf popViewController];
                                  [MMHLogbook logEventType:MMHBuriedPointTypeCenterLogOut];
                              }];
                          }];
    [alertView show];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if (indexPath.section == 0){     // 设置
        MMHPaySafetyPassCodeViewController *paySafeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
        paySafeViewController.transferPhoneNumber = self.paySettingModel.phone;
        paySafeViewController.payPasswordIsSetting = self.paySettingModel.isSetted;
        paySafeViewController.paySafetPageType = MMHPaySafetyPageTypeSetting;
        [self.navigationController pushViewController:paySafeViewController animated:YES];
        [MMHLogbook logEventType:MMHBuriedPointTypeCenterSettingPayPassword];
        
    } else if (indexPath.section == 1){
        if (indexPath.row == 0){
            NSString *str = [NSString stringWithFormat:@"http://itunes.apple.com/us/app/id%@", MMHAppIdentify];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterScore];
        } else if (indexPath.row == 1){
            MMHWebViewController *webViewController = [[MMHWebViewController alloc]initWithResourceName:nil title:nil isRemote:YES url:MMHHTML_Link_about];
            [self.navigationController pushViewController:webViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeCenterAbout];
        }

    }
}


#pragma mark - <UITableViewDelegate>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (section == 0){                  // 设置
        return 1;
    } else if (section == 1){           // 评分，关于
        return 2;
    }
    return 0;

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
       cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
      cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    cell.textLabel.textColor = [UIColor hexChangeFloat:@"666666"];
    cell.textLabel.font = MMHFontOfSize(15);
    
    if (indexPath.section == 0){            // 设置密码

        cell.imageView.image = [UIImage imageNamed:@"setup_icon_lock"];
        cell.textLabel.text = self.paySettingModel.isSetted == 1?@"修改支付密码":@"设置支付密码";
    } else if (indexPath.section == 1){
        if (indexPath.row == 0){
            cell.imageView.image = [UIImage imageNamed:@"setup_icon_smile"];
            cell.textLabel.text = @"给妈妈好评分";
        } else if (indexPath.row == 1){
            cell.imageView.image = [UIImage imageNamed:@"setup_icon_star"];
            cell.textLabel.text = @"关于妈妈好";
        }

    }
    

    return cell;
}


#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row == 1) {
        [cell addSeparatorLineWithType:SeparatorTypeBottom];
    }else{
    
        [cell addSeparatorLineWithType:SeparatorTypeSingle];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
    view.backgroundColor = [UIColor clearColor];
    if (section == 1) {
        UIButton *quitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        quitBtn.frame = CGRectMake(MMHFloat(16), 25, (self.view.bounds.size.width - MMHFloat(16)*2) , 44);
        [quitBtn setBackgroundColor:[UIColor hexChangeFloat:@"fc687c"]];
        [quitBtn setTitle:@"退出当前账号" forState:UIControlStateNormal];
        [quitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        quitBtn.titleLabel.font = MMHFontOfSize(15);
        quitBtn.layer.cornerRadius = 6;
        [quitBtn addTarget:self action:@selector(handleQuitBtn:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:quitBtn];
    }
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return 0.0f;
    }
    return 69;
}


#pragma mark - 接口
-(void)sendRequestWithGetSettingStatus{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestGetIsSettingPayPasswordFrom:nil succeededHandler:^(MMHSettingPaySettingModel *paySettedModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.paySettingModel = paySettedModel;
        // 刷新tableView
        [strongSelf.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 清除当前位置信息
-(void)clearNormalLocationInfo{
    [MMHCurrentLocationModel sharedLocation].deliveryAddrId = @"";           // 区域id
    [MMHCurrentLocationModel sharedLocation].receiptProvince = @"";
    [MMHCurrentLocationModel sharedLocation].receiptCity = @"";
    [MMHCurrentLocationModel sharedLocation].receiptArea = @"";
    [MMHCurrentLocationModel sharedLocation].receiptAreaId = @"";
    [MMHCurrentLocationModel sharedLocation].receiptLat =  0;
    [MMHCurrentLocationModel sharedLocation].receiptLng =  0;
}

//905157044
@end
