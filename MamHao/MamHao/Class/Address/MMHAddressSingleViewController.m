//
//  MMHAddressSingleViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressSingleViewController.h"
#import "MMHLocationManager.h"                          // 定位model
#import "MMHSearchAddressViewController.h"              // 用来请求经纬度的controller
#import "MMHNetworkAdapter+Address.h"                   // 数据请求model 

@interface MMHAddressSingleViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *addressTableView;                                  /**< tableView*/
@property (nonatomic,strong)NSArray *dataSourceArray;                                       /**< 数据源*/
@property (nonatomic,strong)UITextField *consigneeTextField;                                /**< 联系人*/
@property (nonatomic,strong)UITextField *phoneNumberTextField;                              /**< 电话号码*/
@property (nonatomic,strong)UITextField *prefixAddressTextField;                            /**< 街道小区*/
@property (nonatomic,strong)UITextField *addressDetailTextFiled;                            /**< 地址详情*/
@property (nonatomic,strong)UISwitch *normalSwitch;                                         /**< 是否默认*/
@end

@implementation MMHAddressSingleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];               // 数组初始化
    [self createTableView];             // 创建tableView
    [self createNotifi];                // 添加键盘通知
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    if (self.addressPageType == MMHAddressPageTypeCreate){
        self.barMainTitle = @"新建地址";
        [weakSelf rightBarButtonWithTitle:@"完成" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 增加地址
            [strongSelf dataOfVerification];
        }];
    } else if (self.addressPageType == MMHAddressPageTypeEdit){
        self.barMainTitle = @"编辑地址";
        [weakSelf rightBarButtonWithTitle:@"完成" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dataOfVerification];
        }];
    }
}

-(void)arrayWithInit{
    self.dataSourceArray = @[@[@"收货人:",@"手机号:",@"地   址:",@"门牌号:",@"设为默认"]];
    if (self.addressPageType == MMHAddressPageTypeEdit){

    } else if (self.addressPageType == MMHAddressPageTypeCreate){
        self.transferAddressSingleModel = [[MMHAddressSingleModel alloc]init];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    self.addressTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.addressTableView setHeight:self.view.bounds.size.height];
    self.addressTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.addressTableView.delegate = self;
    self.addressTableView.dataSource = self;
    self.addressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.addressTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.addressTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建lft_label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F6;
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, MMHFloat(80), cellHeight);
            fixedLabel.textColor = C5;
            [cellWithRowOne addSubview:fixedLabel];
            
            // 创建textField
            self.consigneeTextField = [[UITextField alloc]init];
            self.consigneeTextField.backgroundColor = [UIColor clearColor];
            self.consigneeTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), 0, 200, cellHeight);;
            self.consigneeTextField.textAlignment = NSTextAlignmentLeft;
            self.consigneeTextField.textColor = C5;
            self.consigneeTextField.font = F6;
            self.consigneeTextField.delegate = self;
            self.consigneeTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            self.consigneeTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            self.consigneeTextField.placeholder = @"请输入收货人的姓名";
            self.consigneeTextField.returnKeyType = UIReturnKeyNext;
            self.consigneeTextField.keyboardType = UIKeyboardTypeDefault;
            [cellWithRowOne addSubview:self.consigneeTextField];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.dataSourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        self.consigneeTextField.text = self.transferAddressSingleModel.consignee;
        return cellWithRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建lft_label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F6;
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, MMHFloat(80), cellHeight);
            fixedLabel.textColor = C5;
            [cellWithRowTwo addSubview:fixedLabel];
            
            // 创建textField
            self.phoneNumberTextField = [[UITextField alloc]init];
            self.phoneNumberTextField.backgroundColor = [UIColor clearColor];
            self.phoneNumberTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), 0, 200, cellHeight);;
            self.phoneNumberTextField.textAlignment = NSTextAlignmentLeft;
            self.phoneNumberTextField.textColor = C5;
            self.phoneNumberTextField.font = F6;
            self.phoneNumberTextField.delegate = self;
            self.phoneNumberTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            self.phoneNumberTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            self.phoneNumberTextField.placeholder = @"请输入收货人的手机号码";
            self.phoneNumberTextField.returnKeyType = UIReturnKeyDone;
            self.phoneNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
            self.normalSwitch.hidden = YES;

            [cellWithRowTwo addSubview:self.phoneNumberTextField];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.dataSourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        self.phoneNumberTextField.text = self.transferAddressSingleModel.phone;
        
        return cellWithRowTwo;
    } else if (indexPath.row == 2){
        static NSString *cellIdentifyWithRowThr = @"cellWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建lft_label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F6;
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, MMHFloat(80), cellHeight);
            fixedLabel.textColor = C5;
            [cellWithRowThr addSubview:fixedLabel];
            
            // 创建textField
            self.prefixAddressTextField = [[UITextField alloc]init];
            self.prefixAddressTextField.backgroundColor = [UIColor clearColor];
            self.prefixAddressTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), 0, kScreenBounds.size.width - MMHFloat(10)- MMHFloat(9) - MMHFloat(10) - CGRectGetMaxX(fixedLabel.frame), cellHeight);;
            self.prefixAddressTextField.textAlignment = NSTextAlignmentLeft;
            self.prefixAddressTextField.textColor = C5;
            self.prefixAddressTextField.font = F6;
            self.prefixAddressTextField.delegate = self;
            self.prefixAddressTextField.enabled = NO;
            self.prefixAddressTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            self.prefixAddressTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            self.prefixAddressTextField.placeholder = @"请选择街道小区";
            self.prefixAddressTextField.returnKeyType = UIReturnKeyNext;
            self.prefixAddressTextField.keyboardType = UIKeyboardTypeDefault;
            [cellWithRowThr addSubview:self.prefixAddressTextField];
            
            // 创建arrowImageView
            UIImageView *arrowImageView = [[UIImageView alloc]init];
            arrowImageView.backgroundColor = [UIColor clearColor];
            arrowImageView.image = [UIImage imageNamed:@"tool_arrow"];
            arrowImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(9) - MMHFloat(10), (cellHeight - MMHFloat(15)) / 2., MMHFloat(9), MMHFloat(15));
            [cellWithRowThr addSubview:arrowImageView];

        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowThr viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.dataSourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        NSString *addressString = @"";
        if (self.transferAddressSingleModel.lat){
            if (self.transferAddressSingleModel.gpsAddress.length){
                addressString = self.transferAddressSingleModel.gpsAddress;
            } else {
                if (self.transferAddressSingleModel.province.length){
                    addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.province];
                    if (self.transferAddressSingleModel.city.length){
                        addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.city];
                        if (self.transferAddressSingleModel.area.length){
                            addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.area];
                        }
                    }
                }
            }
        } else {
            addressString = @"";
        }
        self.prefixAddressTextField.text = addressString;
        
        return cellWithRowThr;
    } else if (indexPath.row == 3){
        static NSString *cellIdentifyWithRowFou = @"cellWithRowFou";
        UITableViewCell *cellWithRowFou = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFou];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowFou){
            cellWithRowFou = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFou];
            cellWithRowFou.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建lft_label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F6;
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, MMHFloat(80), cellHeight);
            fixedLabel.textColor = C5;
            [cellWithRowFou addSubview:fixedLabel];
            
            // 创建textField
            self.addressDetailTextFiled = [[UITextField alloc]init];
            self.addressDetailTextFiled.backgroundColor = [UIColor clearColor];
            self.addressDetailTextFiled.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), 0, 200, cellHeight);;
            self.addressDetailTextFiled.textAlignment = NSTextAlignmentLeft;
            self.addressDetailTextFiled.textColor = C5;
            self.addressDetailTextFiled.font = F6;
            self.addressDetailTextFiled.delegate = self;
            self.addressDetailTextFiled.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            self.addressDetailTextFiled.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            self.addressDetailTextFiled.placeholder = @"请输入您的详细地址";
            self.addressDetailTextFiled.returnKeyType = UIReturnKeyDone;
            self.addressDetailTextFiled.keyboardType = UIKeyboardTypeDefault;

            [cellWithRowFou addSubview:self.addressDetailTextFiled];
        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowFou viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.dataSourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        self.addressDetailTextFiled.text = self.transferAddressSingleModel.addrDetail;
        return cellWithRowFou;
    } else if (indexPath.row == 4){
        static NSString *cellIdentifyWithRowFiv = @"cellWithRowFiv";
        UITableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowFiv){
            cellWithRowFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建lft_label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F6;
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.frame = CGRectMake(MMHFloat(10), 0, MMHFloat(80), cellHeight);
            fixedLabel.textColor = C5;
            [cellWithRowFiv addSubview:fixedLabel];
            
            // 是否默认
            self.normalSwitch = [[UISwitch alloc] init];
            self.normalSwitch.frame = CGRectMake(self.view.frame.size.width - 51 - MMHFloat( + 11), (cellHeight - 31) / 2, 51, 31);
            [self.normalSwitch addTarget:self action:@selector(switchAction) forControlEvents:UIControlEventTouchUpInside];
            self.normalSwitch.onTintColor = [UIColor hexChangeFloat:@"FC687C"];
            
            [cellWithRowFiv addSubview:self.normalSwitch];
            
            if (!IS_IOS7_LATER){
                [self.normalSwitch setFrame   :CGRectMake(305-CGRectGetWidth([self.normalSwitch frame]), 7, 31, 27)];
                [self.normalSwitch setOnImage :[[UIImage imageNamed:@"login_thirdLogin_weibo"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 13, 15, 13)]];
                [self.normalSwitch setOffImage:[[UIImage imageNamed:@"switch_off"] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 13, 15, 13)]];
            }

        }
        UILabel *fixedLabel = (UILabel *)[cellWithRowFiv viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.dataSourceArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        if (self.addressPageType == MMHAddressPageTypeCreate){
            self.normalSwitch.on = YES;
        } else if (self.addressPageType == MMHAddressPageTypeEdit){
            if (self.transferAddressSingleModel.isDefault){
                self.normalSwitch.on = YES;
            } else {
                self.normalSwitch.on = NO;
            }
        }

        return cellWithRowFiv;
    }
    return nil;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 2){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [self tapClick];            // 隐藏键盘
        MMHSearchAddressViewController *geoViewController = [[MMHSearchAddressViewController alloc]init];
        geoViewController.transferAddressSingleModel = self.transferAddressSingleModel;
        __weak typeof(self)weakSelf = self;
        geoViewController.locationModelBlock = ^(MMHAddressSingleModel *addressParameterModel){
            NSString *deliveryAddressId = weakSelf.transferAddressSingleModel.deliveryAddrId;
            weakSelf.transferAddressSingleModel = addressParameterModel;
            if (self.addressPageType == MMHAddressPageTypeEdit){
                weakSelf.transferAddressSingleModel.deliveryAddrId = deliveryAddressId;
            }
            
            [weakSelf addressStringReload];
            [weakSelf.addressDetailTextFiled becomeFirstResponder];
        };
        geoViewController.searchAddressUsingType = SearchAddressUsingTypeWithAddressList;
        [self.navigationController pushViewController:geoViewController animated:YES];
    } else {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *indexCell = [tableView cellForRowAtIndexPath:indexPath];
        UITextField *inputTextField = (UITextField *)[indexCell viewWithTag:1];
        [inputTextField becomeFirstResponder];
    }
}

-(void)addressStringReload{
    NSString *addressString = @"";
    if (self.transferAddressSingleModel.lat){
        if (self.transferAddressSingleModel.gpsAddress.length){
            addressString = self.transferAddressSingleModel.gpsAddress;
        } else {
            if (self.transferAddressSingleModel.province.length){
                addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.province];
                if (self.transferAddressSingleModel.city.length){
                    addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.city];
                    if (self.transferAddressSingleModel.area.length){
                        addressString = [addressString stringByAppendingString:self.transferAddressSingleModel.area];
                    }
                }
            }
        }
    } else {
        addressString = @"";
    }
    self.prefixAddressTextField.text = addressString;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [headerView addGestureRecognizer:tapGestureRecognizer];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.addressTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.dataSourceArray objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.dataSourceArray objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - ActionClick
-(void)switchAction{
    if(self.normalSwitch.on){
        self.transferAddressSingleModel.isDefault = NO;
    } else {
        self.transferAddressSingleModel.isDefault = YES;
    }
}

-(void)locationButtonClick{
//    [[MMHLocationManager sharedLocationManager] getLocationCoordinate:^(AMapAddressComponent *addressComponent) {
//        NSString *tempString = [NSString stringWithFormat:@"%@ %@ %@",addressComponent.province,addressComponent.city,addressComponent.district.length?addressComponent.district:@""];
////        NSLog(@"%@",self.addressSingleModelWithSaveData.prefixAddress);
////        self.addressSingleModelWithSaveData.prefixAddress = tempString;
//        [self.addressTableView reloadData];
//    }];
}

-(void)tapClick{
    if ([self.consigneeTextField isFirstResponder]){
        [self.consigneeTextField resignFirstResponder];
    } else if ([self.phoneNumberTextField isFirstResponder]){
        [self.phoneNumberTextField resignFirstResponder];
    } else if ([self.prefixAddressTextField isFirstResponder]){
        [self.prefixAddressTextField resignFirstResponder];
    } else if ([self.addressDetailTextFiled isFirstResponder]){
        [self.addressDetailTextFiled resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.consigneeTextField){
        self.transferAddressSingleModel.consignee = self.consigneeTextField.text;
    } else if (textField == self.phoneNumberTextField){
        self.transferAddressSingleModel.phone = self.phoneNumberTextField.text;
    } else if (textField == self.prefixAddressTextField){
        
    } else if (textField == self.addressDetailTextFiled){
        self.transferAddressSingleModel.addrDetail = self.addressDetailTextFiled.text;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyNext){ // 显示下一个
        if (textField == self.consigneeTextField){
            [self.phoneNumberTextField becomeFirstResponder];
        } else if (textField == self.phoneNumberTextField){
        
        } else if (textField == self.prefixAddressTextField){
            [self.prefixAddressTextField becomeFirstResponder];
        }
    } else {
        [self.addressDetailTextFiled resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (self.phoneNumberTextField == textField) {
        if ([toBeString length] > 11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }
    return YES;
}

#pragma mark - 接口
#pragma mark createAddress
-(void)sendRequestWithCreateAddressWithAreaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat{
    __weak typeof(self)weakSelf = self;
    [self.view showProcessingView];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    [[MMHNetworkAdapter sharedAdapter]sendRequestCreateAddressWithAreaId:areaId gpsAddress:(NSString *)gpsAddress addressDetail:addressDetail cosignee:cosignee phone:phone isDefault:isDefault lng:lng lat:lat from:nil succeededHandler:^(NSString *deliveryAddressId) {
        if (!weakSelf){
            return ;
        }
        [weakSelf.view hideProcessingView];
        weakSelf.navigationItem.rightBarButtonItem.enabled = YES;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.transferAddressSingleModel.deliveryAddrId = deliveryAddressId;
        strongSelf.addAddressBlock(strongSelf.transferAddressSingleModel);
        [strongSelf popWithAnimation];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        [weakSelf.view hideProcessingView];
        weakSelf.navigationItem.rightBarButtonItem.enabled = YES;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
        
    }];
}

#pragma mark changeAddress
-(void)sendRequestWithEditAddressWithDeliveryAddId:(NSString *)deliveryAddId areaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat{
    [self.view showProcessingView];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestUpdateAddressWithDeliveryAddrId:deliveryAddId areaId:areaId  gpsAddress:gpsAddress addressDetail:addressDetail cosignee:cosignee phone:phone isDefault:isDefault lng:lng lat:lat from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        [weakSelf.view hideProcessingView];
        weakSelf.navigationItem.rightBarButtonItem.enabled = YES;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.updateAddressBlock(strongSelf.transferAddressSingleModel);
        [strongSelf popWithAnimation];
    } failedHandler:^(NSError *error) {
        if(!weakSelf){
            return ;
        }
        [weakSelf.view hideProcessingView];
        weakSelf.navigationItem.rightBarButtonItem.enabled = YES;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark - dataManager
-(void)dataOfVerification{
    if (!self.consigneeTextField.text.length){
        [MMHTool lockAnimationForView:self.consigneeTextField];
    } else if (!self.phoneNumberTextField.text.length){
        [MMHTool lockAnimationForView:self.phoneNumberTextField];
    } else if (!self.prefixAddressTextField.text.length){
        [MMHTool lockAnimationForView:self.prefixAddressTextField];
    } else if (!self.addressDetailTextFiled.text.length){
        [MMHTool lockAnimationForView:self.addressDetailTextFiled];
    } else {                // 接口
        if (![MMHTool validateMobile:self.phoneNumberTextField.text]){
            [MMHTool alertWithMessage:@"输入的手机号码不合法"];
        } else {
            
            __weak typeof(self)weakSelf = self;
            weakSelf.transferAddressSingleModel.consignee = self.consigneeTextField.text;
            weakSelf.transferAddressSingleModel.phone = self.phoneNumberTextField.text;
            weakSelf.transferAddressSingleModel.gpsAddress = self.prefixAddressTextField.text;
            weakSelf.transferAddressSingleModel.addrDetail = self.addressDetailTextFiled.text;
            weakSelf.transferAddressSingleModel.isDefault = self.normalSwitch.on;
            if (self.addressPageType == MMHAddressPageTypeCreate){
                [weakSelf sendRequestWithCreateAddressWithAreaId:weakSelf.transferAddressSingleModel.areaId gpsAddress:weakSelf.transferAddressSingleModel.gpsAddress addressDetail:weakSelf.transferAddressSingleModel.addrDetail cosignee:weakSelf.transferAddressSingleModel.consignee phone:weakSelf.transferAddressSingleModel.phone isDefault:weakSelf.transferAddressSingleModel.isDefault lng:weakSelf.transferAddressSingleModel.lng lat:weakSelf.transferAddressSingleModel.lat];
            } else if (self.addressPageType == MMHAddressPageTypeEdit){
                [weakSelf sendRequestWithEditAddressWithDeliveryAddId:weakSelf.transferAddressSingleModel.deliveryAddrId areaId:weakSelf.transferAddressSingleModel.areaId gpsAddress:weakSelf.transferAddressSingleModel.gpsAddress  addressDetail:weakSelf.transferAddressSingleModel.addrDetail cosignee:weakSelf.transferAddressSingleModel.consignee phone:weakSelf.transferAddressSingleModel.phone isDefault:weakSelf.transferAddressSingleModel.isDefault lng:weakSelf.transferAddressSingleModel.lng lat:weakSelf.transferAddressSingleModel.lat];
            }
        }
    }
}

#pragma mark - keyNotifi
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.addressTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.addressTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

@end
