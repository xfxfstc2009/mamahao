//
//  MMHChangeAddressViewController.h
//  MamHao
//
//  Created by SmartMin on 15/7/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHAddressSingleModel.h"
#import "MMHProductDetailViewController1.h"
#import "MMHMamhaoViewController.h"

@class MMHProductDetailViewController1;

typedef NS_ENUM(NSInteger, MMHChangeAddressPageUsingType) {
    MMHChangeAddressPageUsingTypeProduct,                      /**< 商品详情页进入*/
    MMHChangeAddressPageUsingTypeHome,                         /**< 首页进入*/
};

typedef void(^changeAddressBlock)(MMHAddressSingleModel *changedAddressBlock);
@interface MMHChangeAddressViewController : AbstractViewController

@property (nonatomic,assign)MMHChangeAddressPageUsingType changeAddressPageUsingType;
@property (nonatomic,copy)changeAddressBlock changedAddressBlock;
@end
