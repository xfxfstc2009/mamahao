//
//  MMHAddressSingleViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHAddressSingleModel.h"

typedef NS_ENUM(NSInteger, MMHAddressPageType){
    MMHAddressPageTypeEdit,                         /**< 修改*/
    MMHAddressPageTypeCreate,                       /**< 创建*/
};
typedef void(^addAddressBlock)(MMHAddressSingleModel *addressSingleModel);
typedef void(^updateAddressBlock)(MMHAddressSingleModel *addressSingleModel);

@interface MMHAddressSingleViewController : AbstractViewController

// 跳转页面完成类型- 编辑 - 创建
@property (nonatomic,assign)MMHAddressPageType addressPageType;
@property (nonatomic,strong)MMHAddressSingleModel *transferAddressSingleModel;

@property (nonatomic,copy)addAddressBlock addAddressBlock;
@property (nonatomic,copy)updateAddressBlock updateAddressBlock;

@end
