//
//  MMHSearchAddressViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSearchAddressViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>
#import "MMHGeoCodeAnnotation.h"
#import "MMHNetworkAdapter+Order.h"
#import "MMHCurrentLocationModel.h"
#import "MMHLocationManager.h"

@interface MMHSearchAddressViewController()<UITableViewDataSource,UITableViewDelegate,MAMapViewDelegate, AMapSearchDelegate,UISearchBarDelegate, UISearchDisplayDelegate>
@property (nonatomic, strong) UISearchBar *searchBar;                           /**< 搜索条*/
@property (nonatomic, strong) UISearchDisplayController *displayController;     /**< 变幻的viewController */
@property (nonatomic,strong)AMapSearchAPI *search;                              /**< searchAPI*/
@property (nonatomic, strong) NSMutableArray *tips;                             /**< searchBar 搜索到的内容*/

@property (nonatomic,strong)UIView *areaView;                                   /**< 搜索条城市的view */
@property (nonatomic,strong)UILabel *areaLabel;                                 /**< 地区的label*/
@property (nonatomic,strong)UIImageView *arrowImageView;                        /**< 箭头的imageView*/

//【搜索城市界面】
@property (nonatomic,strong)UITableView *searchLocationTableView;               /**< 搜索源tableView*/
@property (nonatomic,strong)NSMutableArray *dataSourceArr;                      /**< 数据源*/
@property (nonatomic,strong)NSMutableArray *dataSourceIndexArray;               /**< 索引数组*/
@property (nonatomic,strong)UILabel *currentLocationLabel;                      /**< 当前地址label*/

@end

@implementation MMHSearchAddressViewController

-(void)dealloc{
    [self clearSearch];
    NSLog(@"释放了");
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createAreaView];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createSearchBar];
    [self createSearchDisplay];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequest];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self currentLocation];
}

#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"请输入地址";
}

-(void)arrayWithInit{
    self.tips = [NSMutableArray array];
    
    // 搜地址
    self.dataSourceArr =[NSMutableArray array];
    self.dataSourceIndexArray = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.searchLocationTableView){
        self.searchLocationTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.searchLocationTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.searchLocationTableView.orgin_y = 44;
        self.searchLocationTableView.delegate = self;
        self.searchLocationTableView.dataSource = self;
        self.searchLocationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.searchLocationTableView.backgroundColor = [UIColor clearColor];
        self.searchLocationTableView.sectionIndexColor = [UIColor blueColor];
        self.searchLocationTableView.sectionIndexTrackingBackgroundColor = [UIColor grayColor];
        [self.view addSubview:self.searchLocationTableView];
        self.searchLocationTableView.hidden = YES;
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.searchLocationTableView){                             // 搜索城市界面
        return self.dataSourceArr.count;
    } else if (tableView == self.displayController.searchResultsTableView){     // searchBar
        return 1;
    } else {
        return 0;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.searchLocationTableView){
        if (section == 1){
            return 1;
        } else {
            NSArray *sectionOfArr = [self.dataSourceArr objectAtIndex:section];
            return sectionOfArr.count;
        }
    } else if (tableView == self.displayController.searchResultsTableView){
        return self.tips.count;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchLocationTableView){
        if (indexPath.section == 0){            //  【您当前定位的城市】
            static NSString *cellIdentifyWithSecOneRowOne = @"cellIdentifyWithSecOneRowOne";
            UITableViewCell *cellWithSecOneRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecOneRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSecOneRowOne){
                cellWithSecOneRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecOneRowOne];
                cellWithSecOneRowOne.selectionStyle = UITableViewCellSelectionStyleGray;
                
                // 创建fixedLabel
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.stringTag = @"fixedLabell";
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.text = @"您当前定位的城市:";
                CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:fixedLabel.font])];
                fixedLabel.frame = CGRectMake(MMHFloat(11), 0, fixedSize.width, cellHeight);
                [cellWithSecOneRowOne addSubview:fixedLabel];
                
                // 创建cityLabel
                self.currentLocationLabel = [[UILabel alloc]init];
                self.currentLocationLabel.backgroundColor = [UIColor clearColor];
                self.currentLocationLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                self.currentLocationLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(5), 0, 100, cellHeight);
                [cellWithSecOneRowOne addSubview:self.currentLocationLabel];
            }
            // 赋值
            UILabel *cityLabel = (UILabel *)[cellWithSecOneRowOne viewWithStringTag:@"cityLabel"];
            cityLabel.text = @"杭州";
            CGSize citySize = [cityLabel.text sizeWithCalcFont:cityLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
            cityLabel.size_width = citySize.width;
            return cellWithSecOneRowOne;
            
        } else if (indexPath.section == 1){
            static NSString *cellIdentifyWithHotLocation = @"cellIdentifyWithHotLocation";
            UITableViewCell *cellWithHotLocation = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithHotLocation];
            if (!cellWithHotLocation){
                cellWithHotLocation = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithHotLocation];
                cellWithHotLocation.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithHotLocation.backgroundColor = [UIColor hexChangeFloat:@"EBEBEC"];
                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                // 创建scrollView
                UIView *hotCityBgView = [[UIView alloc]init];
                hotCityBgView.backgroundColor = [UIColor clearColor];
                hotCityBgView.stringTag = @"hotCityBgView";
                hotCityBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, cellHeight);
                [cellWithHotLocation addSubview:hotCityBgView];
            }
            UIView *hotCityBgView = (UIView *)[cellWithHotLocation viewWithStringTag:@"hotCityBgView"];
            if (!hotCityBgView.subviews.count){
                for (int i = 0 ; i < [[self.dataSourceArr objectAtIndex:indexPath.section] count]; i++){
                    UIButton *cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
                    cityButton.backgroundColor = [UIColor whiteColor];
                    cityButton.clipsToBounds = YES;
                    cityButton.layer.cornerRadius = MMHFloat(3.);
                    cityButton.layer.borderWidth = .5f;
                    [cityButton setTitleColor:C6 forState:UIControlStateNormal];
                    cityButton.titleLabel.font = F5;
                    cityButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
                    [cityButton setTitle:[[self.dataSourceArr objectAtIndex:indexPath.section] objectAtIndex:i] forState:UIControlStateNormal];
                    cityButton.stringTag = [NSString stringWithFormat:@"%li-%li",(long)indexPath.section,(long)i];
                    [cityButton addTarget:self action:@selector(cityButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                    CGFloat buttonWidth = ((kScreenBounds.size.width - MMHFloat(10) * 5)) / 3.;
                    NSInteger numberOfLine = 0;
                    if ([[self.dataSourceArr objectAtIndex:indexPath.section] count] %3 == 0){
                        numberOfLine = [[self.dataSourceArr objectAtIndex:indexPath.section] count] / 3;
                    } else {
                        numberOfLine = [[self.dataSourceArr objectAtIndex:indexPath.section]count] / 3 + 1;
                    }
                    CGFloat buttonOriginX = MMHFloat(10) + (i % 3) * (buttonWidth + MMHFloat(10));
                    CGFloat buttonOriginY = MMHFloat(10) + (i / 3) * (MMHFloat(40) + MMHFloat(10));
                    cityButton.frame = CGRectMake(buttonOriginX, buttonOriginY, buttonWidth, MMHFloat(40));
                    [hotCityBgView addSubview:cityButton];
                }
            }
            
            return cellWithHotLocation;
        } else {
            static NSString *cellIdentifyWithOther = @"cellIdentifyWithOther";
            UITableViewCell *cellWithOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithOther];
            if (!cellWithOther){
                cellWithOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithOther];
                cellWithOther.selectionStyle = UITableViewCellSelectionStyleGray;
                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                UILabel *cityLabel = [[UILabel alloc]init];
                cityLabel.backgroundColor = [UIColor clearColor];
                cityLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                cityLabel.textColor = [UIColor blackColor];
                cityLabel.textAlignment = NSTextAlignmentLeft;
                cityLabel.frame = CGRectMake(MMHFloat(11), 0, 250, cellHeight);
                cityLabel.stringTag = @"cityLabel";
                [cellWithOther addSubview:cityLabel];
            }
            // 赋值
            UILabel *cityLabel = (UILabel *)[cellWithOther viewWithStringTag:@"cityLabel"];
            cityLabel.text = [[self.dataSourceArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithOther;
        }
    } else if (tableView == self.displayController.searchResultsTableView){
        static NSString *tipCellIdentifier = @"tipCellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tipCellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tipCellIdentifier];
            // 1. 创建label
            UILabel *addressNameLabel = [[UILabel alloc]init];
            addressNameLabel.backgroundColor = [UIColor clearColor];
            addressNameLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
            addressNameLabel.textAlignment = NSTextAlignmentLeft;
            addressNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            addressNameLabel.stringTag = @"addressNameLabel";
            addressNameLabel.numberOfLines = 0;
            [cell addSubview:addressNameLabel];
            
            // 2. 创建dymicLabel
            UILabel *addressDymicLabel = [[UILabel alloc]init];
            addressDymicLabel.backgroundColor = [UIColor clearColor];
            addressDymicLabel.font = [UIFont fontWithCustomerSizeName:@"黑"];
            addressDymicLabel.textAlignment = NSTextAlignmentLeft;
            addressDymicLabel.numberOfLines = 0;
            addressDymicLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            addressDymicLabel.stringTag = @"addressDymicLabel";
            [cell addSubview:addressDymicLabel];
        }
        AMapTip *tip = [self.tips objectAtIndex:indexPath.row];
        UILabel *addressNameLabel = (UILabel *)[cell viewWithStringTag:@"addressNameLabel"];
        UILabel *addressDymicLabel = (UILabel *)[cell viewWithStringTag:@"addressDymicLabel"];

        // 1. addressLabel
        addressNameLabel.text = tip.name;
        CGSize addressSize = [tip.name sizeWithCalcFont:addressNameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
        addressNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(11), kScreenBounds.size.width - 2 * MMHFloat(11), addressSize.height);
        
        addressDymicLabel.text = tip.district;
        CGSize addressDymicSize = [tip.district sizeWithCalcFont:addressDymicLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
        addressDymicLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(addressNameLabel.frame), kScreenBounds.size.width - 2 * MMHFloat(11), addressDymicSize.height);
        
        return cell;
    }
    return nil;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView{
    if (tableView == self.searchLocationTableView){
        NSMutableArray *tempIndexArr = [NSMutableArray arrayWithArray:self.dataSourceIndexArray];
        [tempIndexArr replaceObject:@"热门城市" withObject:@"热"];
        return tempIndexArr;
        return self.dataSourceIndexArray;
    } else {
        return nil;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (tableView == self.searchLocationTableView){
        return [self.dataSourceIndexArray objectAtIndex:section];
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.searchLocationTableView){
        if (section == 0){
            return 0;
        } else {
            return MMHFloat(30);
        }
    } else {
        return 0;
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.searchLocationTableView){                                     // 搜地址
        if (indexPath.section == 0){                // 选择当前定位的地址
            if ([MMHCurrentLocationModel sharedLocation].currentCity.length){
                self.areaLabel.text = [MMHCurrentLocationModel sharedLocation].currentCity;
            } else {
                [self.view showTips:@"还未获取当前城市，请稍后再试"];
            }
        } else if (indexPath.section == 1){         // 选择热门城市
            
        } else {                                    // 当前点击的地址
            NSString *checkIndexCity = [[self.dataSourceArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            self.areaLabel.text = checkIndexCity;
        }
        [self updateSearchBarFrame];
    } else if (tableView == self.displayController.searchResultsTableView){             // 【搜索条搜索内容】
        AMapTip *tip = self.tips[indexPath.row];
        
        [self clearAndSearchGeocodeWithKey:tip.name adcode:tip.adcode];
        
        [self.displayController setActive:NO animated:NO];
        
        self.searchBar.placeholder = tip.name;
    }
}

-(void)cityButtonClick:(UIButton *)sender{
    UIButton *cityButton = (UIButton *)sender;
    NSArray *tempArr = [cityButton.stringTag componentsSeparatedByString:@"-"];
    NSString *checkIndexCity = @"";
    if (tempArr.count == 2){
        checkIndexCity = [[self.dataSourceArr objectAtIndex:[[tempArr objectAtIndex:0] integerValue]] objectAtIndex:[[tempArr objectAtIndex:1] integerValue]];
    }
    self.areaLabel.text = checkIndexCity;
    [self updateSearchBarFrame];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.searchLocationTableView){
        if (indexPath.section == 1){
            CGFloat cellHeight = 0;
            NSInteger hotCityCount = [[self.dataSourceArr objectAtIndex:indexPath.section]count];
            NSInteger numberOfLine = 0;
            if (hotCityCount % 3 == 0){
                numberOfLine = hotCityCount / 3;
            } else {
                numberOfLine = hotCityCount / 3 + 1;
            }
            cellHeight = MMHFloat(10) + (MMHFloat(10) + MMHFloat(40)) * numberOfLine + MMHFloat(10);
            return cellHeight;
        } else {
            return MMHFloat(44);
        }
    } else if (tableView == self.displayController.searchResultsTableView){
        AMapTip *tip = [self.tips objectAtIndex:indexPath.row];
        
        CGSize addressSize = [tip.name sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
        
        CGSize addressDymicSize = [tip.district sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];

        return 2 * MMHFloat(11) + addressSize.height + addressDymicSize.height;
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.dataSourceArr.count){
        if(indexPath.section != 1){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.dataSourceArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([[self.dataSourceArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}


#pragma mark - UISearchBar
-(void)createSearch{
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:[MAMapServices sharedServices].apiKey Delegate:nil];
    self.search.delegate = self;
}

- (void)clearSearch {
    self.search.delegate = nil;
}

-(void)createAreaView {
    self.areaView = [[UIView alloc]init];
    self.areaView.backgroundColor = [UIColor hexChangeFloat:@"C9C9CE"];
    self.areaView.frame = CGRectMake(0, 0, MMHFloat(50), 44);
    [self.view addSubview:self.areaView];
    
    // 创建
    self.areaLabel = [[UILabel alloc]init];
    self.areaLabel.backgroundColor = [UIColor clearColor];
    self.areaLabel.font = F4;
    self.areaLabel.frame = CGRectMake(0, 0, self.areaView.bounds.size.width, 44);
    self.areaLabel.textColor = C6;
    NSString *areaContentString = @"获取地址中";
    self.areaLabel.text = areaContentString;
    [self.areaView addSubview:self.areaLabel];
    
    // 创建下拉箭头
    self.arrowImageView = [[UIImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"cart_icon_arrow_down"];
    self.arrowImageView.frame = CGRectMake(0, (44 - MMHFloat(14)) / 2. , MMHFloat(14.), MMHFloat(14.));
    [self.areaView addSubview:self.arrowImageView];
    
    // 调整frame
    CGSize areaSize = [areaContentString sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.areaLabel.font])];
    self.areaLabel.frame = CGRectMake(MMHFloat(10), 0, areaSize.width, 44);
    // 箭头
    self.arrowImageView.orgin_x = CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(3);
    // view
    self.areaView.frame = CGRectMake(0, 0, CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(3) + MMHFloat(14), 44);
    
    // 创建button3
    UIButton *searchAreaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    searchAreaButton.backgroundColor = [UIColor clearColor];
    [searchAreaButton addTarget:self action:@selector(showSearchArea) forControlEvents:UIControlEventTouchUpInside];
    searchAreaButton.frame = self.areaView.bounds;
    self.areaView.userInteractionEnabled = YES;
    [self.areaView addSubview:searchAreaButton];
}

- (void)createSearchBar {
    [self createSearch];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.areaView.frame), 0, kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame), 44)];
    self.searchLocationTableView.size_height -= 44;
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.translucent = YES;
    self.searchBar.delegate = self;
    self.searchBar.placeholder = @"请输入地址";
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:self.searchBar];
}

- (void)createSearchDisplay {
    self.displayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.displayController.delegate = self;
    self.displayController.searchResultsDataSource = self;
    self.displayController.searchResultsDelegate = self;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *key = searchBar.text;
    [self clearAndSearchGeocodeWithKey:key adcode:nil];
    [self.displayController setActive:NO animated:YES];
    self.searchBar.placeholder = key;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
       self.searchBar.text = self.areaLabel.text;
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = 0;
    }];
    return YES;
}


#pragma mark - UISearchDisplayDelegate
- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
        self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
        self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self searchTipsWithKey:searchString];
    return YES;
}

#pragma mark - 地理编码 搜索
- (void)searchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode {
    if (key.length == 0) {
        return;
    }
    
    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
    geo.address = key;
    
    if (adcode.length > 0) {
        geo.city = @[adcode];
    }
    
    [self.search AMapGeocodeSearch:geo];
    
}

/* 输入提示 搜索.*/
- (void)searchTipsWithKey:(NSString *)key {
    if (key.length == 0) {
        return;
    }
    
    AMapInputTipsSearchRequest *tips = [[AMapInputTipsSearchRequest alloc] init];
    tips.keywords = key;
    [self.search AMapInputTipsSearch:tips];
}

- (void)clearAndSearchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode {
    [self searchGeocodeWithKey:key adcode:adcode];
}



#pragma mark - AMapSearchDelegate
/* 输入提示回调. */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response {
    [self.tips setArray:response.tips];
    
    [self.displayController.searchResultsTableView reloadData];
    [self.displayController.searchResultsTableView bringToFront];
}

-(void)searchBarBeginEdit{
    [self.searchBar becomeFirstResponder];
}

/* 地理编码回调.*/
- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response {
    if (response.geocodes.count == 0) {
        [self.view showTips:@"没有找到当前位置，请重新输入"];
        [self performSelector:@selector(searchBarBeginEdit) withObject:nil afterDelay:2];
        return;
    }
    
    NSMutableArray *annotations = [NSMutableArray array];
    __weak typeof(self)weakSelf = self;
    MMHAddressSingleModel *addressSingleModel = [[MMHAddressSingleModel alloc]init];
    [response.geocodes enumerateObjectsUsingBlock:^(AMapGeocode *obj, NSUInteger idx, BOOL *stop) {
        MMHGeoCodeAnnotation *geocodeAnnotation = [[MMHGeoCodeAnnotation alloc] initWithGeocode:obj];
        [annotations addObject:geocodeAnnotation];
        addressSingleModel.areaId = obj.adcode;
        addressSingleModel.lng = obj.location.longitude;
        addressSingleModel.lat = obj.location.latitude;
        addressSingleModel.province = obj.province;
        addressSingleModel.city = obj.city;
        addressSingleModel.gpsAddress = obj.formattedAddress;
        addressSingleModel.shortGPSAddress = request.address;
        addressSingleModel.area = obj.district;
    }];
    
    if (annotations.count == 1) {
        weakSelf.locationModelBlock(addressSingleModel);
        if (weakSelf.searchAddressUsingType == SearchAddressUsingTypeWithAddressList){
            [weakSelf popWithAnimation];
        }
    } else {
        return;
    }
}

#pragma mark - ActionClick
-(void)showSearchArea{      // 刷新数组
    self.searchLocationTableView.hidden = !self.searchLocationTableView.hidden;
}


#pragma mark getCurrentLocation
-(void)currentLocation{
    if ([MMHCurrentLocationModel sharedLocation].currentCity.length){
        self.currentLocationLabel.text = [MMHCurrentLocationModel sharedLocation].currentCity;
        self.areaLabel.text = [MMHCurrentLocationModel sharedLocation].currentCity;
        [self updateSearchBarFrame];
    } else {
        [[MMHStatusBar sharedInstance] showMessage:@"正在获取当前定位地址"];
        [MMHLocationManager getArea];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(currentLocation) name:MMHCurrentLocationNotification object:nil];
    }
}

// 当前地址更新
-(void)currentLocationUpdate{
    self.currentLocationLabel.text = [MMHCurrentLocationModel sharedLocation].currentCity;
    self.areaLabel.text = [MMHCurrentLocationModel sharedLocation].currentCity;
    [self updateSearchBarFrame];
}


#pragma mark - 接口
-(void)sendRequest{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchmodelGetCityListFrom:nil succeededHandler:^(MMHAddressCityListModel *addressCityList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 增加索引数据
        [strongSelf.dataSourceIndexArray addObject:@" "];
        // 2. 增加数据源
        NSArray *currentLocationArr = @[@"当前地址"];
        [strongSelf.dataSourceArr addObject:currentLocationArr];
        
        // 3. 轮询插入
        for (int i = 0 ; i < addressCityList.cityList.count;i++){
            MMHAddressSingleCitysModel *singleCitysModel = [addressCityList.cityList objectAtIndex:i];
            // 插入数据源
            [strongSelf.dataSourceArr addObject:singleCitysModel.citys];
            // 插入索引
            [strongSelf.dataSourceIndexArray addObject:singleCitysModel.tag];
        }
        [strongSelf.dataSourceIndexArray replaceObject:@"HotCity" withObject:@"热门城市"];
        [strongSelf.searchLocationTableView reloadData];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTips:@"系统错误，请稍后再试"];
    }];
}

#pragma mark - OtherManager
// updateSearchBarFrame
-(void)updateSearchBarFrame{
    // 1. 计算文字长度
    CGSize cityNameSize = [self.areaLabel.text sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(44))];
    self.areaLabel.size_width = cityNameSize.width;
    // 2. 箭头移动位置
    self.arrowImageView.orgin_x = CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(3);
    // 3.
    self.arrowImageView.orgin_x = CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(3);
    self.areaView.frame = CGRectMake(0, 0, CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(3) + MMHFloat(14), 44);
    self.searchBar.frame = CGRectMake(CGRectGetMaxX(self.areaView.frame), 0, kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame), self.searchBar.size_height);
    self.searchLocationTableView.hidden = YES;
    [[MMHStatusBar sharedInstance]statusBarWithHide];
}


@end
