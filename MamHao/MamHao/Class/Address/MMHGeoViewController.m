//
//  MMHGeoViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGeoViewController.h"
#import "MMHGeoCodeAnnotation.h"
#import "MMHLocationManager.h"
#import "MMHSearchAddressViewController.h"
#define GeoPlaceHolder @"名称"
@interface MMHGeoViewController ()<UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *displayController;

@property (nonatomic, strong) NSMutableArray *tips;
@property (nonatomic,strong)AMapSearchAPI *search;

@property (nonatomic,strong) UIView *areaView;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)UIImageView *arrowImageView;

@end

@implementation MMHGeoViewController
@synthesize tips = _tips;
@synthesize displayController = _displayController;


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getCurrentLocation];
}

-(void)dealloc{
    [self clearSearch];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"请输入地址";
    [self createAreaView];
    [self arrayWithInit];
    [self createSearchBar];
    [self createSearchDisplay];

}

-(void)createSearch{
    self.search = [[AMapSearchAPI alloc] initWithSearchKey:[MAMapServices sharedServices].apiKey Delegate:nil];
    self.search.delegate = self;
}

- (void)clearSearch {
    self.search.delegate = nil;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.tips = [NSMutableArray array];
}

-(void)createAreaView {
    self.areaView = [[UIView alloc]init];
    self.areaView.backgroundColor = [UIColor colorWithRed:189./256. green:189./256. blue:195./256. alpha:1];
    self.areaView.frame = CGRectMake(0, 0, MMHFloat(50), 44);
    
    [self.view addSubview:self.areaView];
    
    // 创建
    self.areaLabel = [[UILabel alloc]init];
    self.areaLabel.backgroundColor = [UIColor clearColor];
    self.areaLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.areaLabel.frame = CGRectMake(0, 0, self.areaView.bounds.size.width, 44);
    NSString *areaContentString = @"获取地址中";
    self.areaLabel.text = areaContentString;
    [self.areaView addSubview:self.areaLabel];
    
    // 创建下拉箭头
    self.arrowImageView = [[UIImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"cart_icon_arrow_down"];
    self.arrowImageView.frame = CGRectMake(0, (44 - MMHFloat(14)) / 2. , MMHFloat(14.), MMHFloat(14.));
    [self.areaView addSubview:self.arrowImageView];
    
    // 调整frame
    CGSize areaSize = [areaContentString sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.areaLabel.font])];
    self.areaLabel.frame = CGRectMake(MMHFloat(11), 0, areaSize.width, 44);
        // 箭头
    self.arrowImageView.orgin_x = CGRectGetMaxX(self.areaLabel.frame);
        // view
    self.areaView.frame = CGRectMake(0, 0, CGRectGetMaxX(self.areaLabel.frame) + MMHFloat(11) + MMHFloat(14), 44);
    
    // 创建button
    UIButton *searchAreaButton = [UIButton buttonWithType:UIButtonTypeCustom];
    searchAreaButton.backgroundColor = [UIColor clearColor];
    [searchAreaButton addTarget:self action:@selector(redirectToSearchArea) forControlEvents:UIControlEventTouchUpInside];
    searchAreaButton.frame = self.areaView.bounds;
    self.areaView.userInteractionEnabled = YES;
    [self.areaView addSubview:searchAreaButton];
}

- (void)createSearchBar {
    [self createSearch];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.areaView.frame), 0, kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame), 44)];
    self.searchBar.barStyle = UIBarStyleDefault;
    self.searchBar.translucent = YES;
    self.searchBar.delegate = self;
    self.searchBar.placeholder = GeoPlaceHolder;
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:self.searchBar];
}

- (void)createSearchDisplay {
    self.displayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.displayController.delegate = self;
    self.displayController.searchResultsDataSource = self;
    self.displayController.searchResultsDelegate = self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tips.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *tipCellIdentifier = @"tipCellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:tipCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:tipCellIdentifier];
    }
    AMapTip *tip = [self.tips objectAtIndex:indexPath.row];
    cell.textLabel.text = tip.name;
    cell.detailTextLabel.text = tip.district;
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AMapTip *tip = self.tips[indexPath.row];
    
    [self clearAndSearchGeocodeWithKey:tip.name adcode:tip.adcode];
    
    [self.displayController setActive:NO animated:NO];
    
    self.searchBar.placeholder = tip.name;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *key = searchBar.text;
    
    [self clearAndSearchGeocodeWithKey:key adcode:nil];
    
    [self.displayController setActive:NO animated:NO];
    
    self.searchBar.placeholder = key;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = 0;
    }];
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{


}


#pragma mark - UISearchDisplayDelegate
- (void) searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
        self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

- (void) searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller{
    [UIView animateWithDuration:.3f animations:^{
        self.searchBar.orgin_x = CGRectGetMaxX(self.areaView.frame);
        self.searchBar.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.areaView.frame);
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self searchTipsWithKey:searchString];
    
    return YES;
}

#pragma mark - 地理编码 搜索
/* 地理编码 搜索. */
- (void)searchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode {
    if (key.length == 0) {
        return;
    }
    
    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
    geo.address = key;
    
    if (adcode.length > 0) {
        geo.city = @[adcode];
    }
    
    [self.search AMapGeocodeSearch:geo];
    
}

/* 输入提示 搜索.*/
- (void)searchTipsWithKey:(NSString *)key {
    if (key.length == 0) {
        return;
    }
    
    AMapInputTipsSearchRequest *tips = [[AMapInputTipsSearchRequest alloc] init];
    tips.keywords = key;
    [self.search AMapInputTipsSearch:tips];
}

- (void)clearAndSearchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode {
    [self searchGeocodeWithKey:key adcode:adcode];
}



#pragma mark - AMapSearchDelegate
/* 输入提示回调. */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response {
    [self.tips setArray:response.tips];
    
    [self.displayController.searchResultsTableView reloadData];
}

/* 地理编码回调.*/
- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response {
    if (response.geocodes.count == 0) {
        return;
    }
    
    NSMutableArray *annotations = [NSMutableArray array];
    
    [response.geocodes enumerateObjectsUsingBlock:^(AMapGeocode *obj, NSUInteger idx, BOOL *stop) {
        MMHGeoCodeAnnotation *geocodeAnnotation = [[MMHGeoCodeAnnotation alloc] initWithGeocode:obj];
        
        [annotations addObject:geocodeAnnotation];
    }];
    
    if (annotations.count == 1) {
        NSLog(@"%f",[[annotations objectAtIndex:0] coordinate].latitude);
        NSLog(@"%f",[[annotations objectAtIndex:0] coordinate].longitude);
        [[UIAlertView alertViewWithTitle:nil message:[NSString stringWithFormat:@"精度%f，维度%f post",[[annotations objectAtIndex:0] coordinate].latitude,[[annotations objectAtIndex:0] coordinate].longitude] buttonTitles:@[@"确定"] callback:nil]show];
    } else {
        return;
    }
}


#pragma mark - AMapSearchDelegate

- (void)searchRequest:(id)request didFailWithError:(NSError *)error {
    NSLog(@"%s: searchRequest = %@, errInfo= %@", __func__, [request class], error);
}


#pragma mark 获取当前地址
-(void)getCurrentLocation{
    [[MMHLocationManager sharedLocationManager] getLocationCoordinate:^(AMapAddressComponent *addressComponent) {
        self.areaLabel.text = addressComponent.city;
        // 重置frame
        CGSize areaSize = [addressComponent.city sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.areaLabel.font])];
        self.areaLabel.size_width = areaSize.width;
        self.arrowImageView.orgin_x = CGRectGetMaxX(self.areaLabel.frame);
        self.areaView.size_width = 2 * MMHFloat(11) + CGRectGetMaxX(self.arrowImageView.frame) + MMHFloat(14);
        
        // 获取placeholder 区
        
        self.searchBar.placeholder = [NSString stringWithFormat:@"%@ %@",addressComponent.district,addressComponent.township.length?addressComponent.township:addressComponent.streetNumber.street];
    }];
}


#pragma mark - actionClick
-(void)redirectToSearchArea{
    MMHSearchAddressViewController *searchAddressViewController = [[MMHSearchAddressViewController alloc]init];
    [self.navigationController pushViewController:searchAddressViewController animated:YES];
}

@end
