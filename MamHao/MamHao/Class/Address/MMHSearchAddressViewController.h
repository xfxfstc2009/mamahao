//
//  MMHSearchAddressViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHAddressSingleViewController.h"
#import "MMHAddressSingleModel.h"
@class MMHAddressSingleViewController;
typedef NS_ENUM(NSInteger, SearchAddressUsingType) {
    SearchAddressUsingTypeWithChangeAddress,                        /**< 应用于切换地址页面*/
    SearchAddressUsingTypeWithAddressList,                          /**< 应用于地址列表页面*/
};

typedef void(^backLocationModelBlock)(MMHAddressSingleModel *addressParameterModel);

@interface MMHSearchAddressViewController : AbstractViewController

@property (nonatomic,assign)MMHAddressPageType addressPageType;                 /**< 用来判断是创建还是编辑*/
@property (nonatomic,assign)SearchAddressUsingType searchAddressUsingType;      /**< 应用页面*/
@property (nonatomic,strong)MMHAddressSingleModel *transferAddressSingleModel;  /**< 传递过来的地址model*/
// block
@property (nonatomic,copy)backLocationModelBlock locationModelBlock;
@end
