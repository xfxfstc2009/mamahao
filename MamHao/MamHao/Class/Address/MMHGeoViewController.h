//
//  MMHGeoViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

@interface MMHGeoViewController : AbstractViewController<MAMapViewDelegate, AMapSearchDelegate>

#pragma mark 获取当前地址
-(void)getCurrentLocation;
@end
