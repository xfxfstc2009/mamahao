//
//  MMHAddressChooseViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHAddressSingleModel.h"

typedef void(^areaBlock)(MMHAddressSingleModel *addressSingleModel);
@interface MMHAddressChooseViewController : UIViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势


- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

@property (nonatomic,copy)areaBlock areaBlock;



@end
