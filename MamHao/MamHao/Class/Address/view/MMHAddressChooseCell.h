//
//  MMHAddressChooseCell.h
//  MamHao
//
//  Created by SmartMin on 15/7/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHAddressSingleModel.h"
@interface MMHAddressChooseCell : UITableViewCell

@property (nonatomic,strong)MMHAddressSingleModel *addressSingleModel;
@property (nonatomic,assign)CGFloat cellHeight;
@property (nonatomic,assign)    BOOL isChecked;

- (void)setChecked:(BOOL)checked;
+(CGFloat)cellHeightWithSingleModel:(MMHAddressSingleModel *)addressSingleModel;
@end
