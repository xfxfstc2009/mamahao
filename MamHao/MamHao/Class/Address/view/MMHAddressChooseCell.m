//
//  MMHAddressChooseCell.m
//  MamHao
//
//  Created by SmartMin on 15/7/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressChooseCell.h"

@interface MMHAddressChooseCell()
@property (nonatomic,strong)UIImageView *checkImageView;
@property (nonatomic,strong)UILabel *addressLabel;

@end

@implementation MMHAddressChooseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - CreateView
-(void)createView{
    // 1. 创建imageView
    self.checkImageView = [[UIImageView alloc]init];
    self.checkImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.checkImageView];
    
    // 2. 创建label
    self.addressLabel = [[UILabel alloc]init];
    self.addressLabel.backgroundColor = [UIColor clearColor];
    self.addressLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.addressLabel];
}

-(void)setCellHeight:(CGFloat)cellHeight{
    _cellHeight = cellHeight;
}

-(void)setAddressSingleModel:(MMHAddressSingleModel *)addressSingleModel{
    _addressSingleModel = addressSingleModel;
    if (addressSingleModel.isDefault){
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    self.checkImageView.frame = CGRectMake(MMHFloat(11), (self.cellHeight - MMHFloat(20)) / 2., MMHFloat(20), MMHFloat(20));
    
    // label
    NSString *addressString = [NSString stringWithFormat:@"%@%@",addressSingleModel.gpsAddress,addressSingleModel.addrDetail];
    CGSize addressSize = [addressString sizeWithCalcFont:self.addressLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(10) - MMHFloat(20), CGFLOAT_MAX)];
    self.addressLabel.frame = CGRectMake(CGRectGetMaxX(self.checkImageView.frame) + MMHFloat(10), (self.cellHeight - [MMHTool contentofHeight:self.addressLabel.font]) / 2., kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(20) - MMHFloat(10), addressSize.height);
    self.addressLabel.numberOfLines = 0;
    self.addressLabel.text = addressString;
}

+(CGFloat)cellHeightWithSingleModel:(MMHAddressSingleModel *)addressSingleModel{
    NSString *addressString = [NSString stringWithFormat:@"%@%@",addressSingleModel.gpsAddress,addressSingleModel.addrDetail];
    CGSize addressSize = [addressString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(10) - MMHFloat(20), CGFLOAT_MAX)];
    return addressSize.height + 2 * MMHFloat(11);
}

- (void)setChecked:(BOOL)checked{
    if (checked) {
        [self animationWithCollectionWithButton:self.checkImageView collection:YES];
    } else {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    self.isChecked = checked;
}

#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection{
    if (isCollection){
        collectionButton.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        collectionButton.image = nil;
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    isCollection = !isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}
@end
