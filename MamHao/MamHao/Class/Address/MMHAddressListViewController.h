//
//  MMHAddressListViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 地址列表
#import "AbstractViewController.h"
#import "MMHAddressSingleModel.h"                                   // 单个地址modet

typedef NS_ENUM(NSInteger, MMHAddressListViewControllerUsing) {     // 应用场景
    MMHAddressListViewControllerUsingOrder,                         /**< 应用于订单*/
    MMHAddressListViewControllerUsingPresentCenter,                 /**< 应用于个人中心*/
};

typedef void(^addressDidSelected)();
typedef void(^leftButtonClickBlock)(BOOL isNoData);

@interface MMHAddressListViewController : AbstractViewController


@property (nonatomic,strong)MMHAddressSingleModel *transferAddressSingleModel;
@property (nonatomic,assign)MMHAddressListViewControllerUsing usingType;
@property (nonatomic,copy)addressDidSelected addressSelectedBlock;
@property (nonatomic,copy)leftButtonClickBlock leftBtnClickBlock;

@end
