//
//  MMHAddressSingleModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressSingleModel.h"

@implementation MMHAddressSingleModel

+ (MMHAddressSingleModel *)sharedAddress {
    static MMHAddressSingleModel *currentAddress = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentAddress = [[MMHAddressSingleModel alloc] init];
    });
    return currentAddress;
}

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"gpsAddress": @"gpsAddr"};
}
@end
