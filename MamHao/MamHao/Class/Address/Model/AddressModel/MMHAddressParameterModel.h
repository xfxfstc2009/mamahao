//
//  MMHAddressParameterModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 用来传递参数的model
#import "MMHFetchModel.h"

@interface MMHAddressParameterModel : MMHFetchModel
@property (nonatomic,copy)NSString *areaId;                 /**< 区域id*/
@property (nonatomic,copy)NSString *addrDetail;             /**< 详细地址*/
@property (nonatomic,copy)NSString *consignee;              /**< 联系人*/
@property (nonatomic,copy)NSString *phone;                  /**< 联系电话*/
@property (nonatomic,copy)NSString *telephone;              /**< 座机*/
@property (nonatomic,assign)BOOL isDefault;                 /**< 是否默认 0是，1否*/
@property (nonatomic,assign)CGFloat lng;                    /**< 精度*/
@property (nonatomic,assign)CGFloat lat;                    /**< 维度*/

// 多余参数
@property (nonatomic,copy)NSString *province;               /**< 浙江省*/
@property (nonatomic,copy)NSString *city;                   /**< 城市*/
@end
