//
//  MMHAddressCityListModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHAddressSingleCitysModel.h"
@interface MMHAddressCityListModel : MMHFetchModel

@property (nonatomic,strong)NSArray<MMHAddressSingleCitysModel>*cityList;
@end
