//
//  MMHAddressListModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHAddressSingleModel.h"
@interface MMHAddressListModel : MMHFetchModel

@property (nonatomic,strong)NSArray <MMHAddressSingleModel>*data;
@end
