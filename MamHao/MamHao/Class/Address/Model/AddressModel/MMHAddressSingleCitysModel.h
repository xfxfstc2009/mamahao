//
//  MMHAddressSingleCitysModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHAddressSingleCitysModel <NSObject>

@end

@interface MMHAddressSingleCitysModel : MMHFetchModel

@property (nonatomic,strong)NSArray *citys;                     /**< 城市列表*/
@property (nonatomic,copy)NSString  *tag;                       /**< 城市tag*/
@end
