//
//  MMHChangeAddressViewController.m
//  MamHao
//
//  Created by SmartMin on 15/7/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChangeAddressViewController.h"
#import "MMHNetworkAdapter+Address.h"
#import "MMHAddressChooseCell.h"
#import "MMHSearchAddressViewController.h"
#import "MMHAccountSession.h"

@interface MMHChangeAddressViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *changeAddressTableView;
@property (nonatomic,strong)NSMutableArray *changeAddressMutableArr;
@property (nonatomic,strong)MMHAddressListModel *addressCityList;                      /**< 接受地址信息的列表*/
@property (nonatomic,strong)NSMutableArray *isSelectedMutableArr;                      /**< 是否选中的数组*/
@property (nonatomic,strong)MMHAddressSingleModel *currentAddressSingleModel;          /**< 单一一个地址model*/

@end

@implementation MMHChangeAddressViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"切换地址";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.changeAddressMutableArr = [NSMutableArray array];
    self.isSelectedMutableArr = [NSMutableArray array];
    __weak typeof(self)weakSelf =self;
    NSArray *tempArr = @[@[@"定位到当前地址"],@[@"搜索具体地址"]];
    [self.changeAddressMutableArr addObjectsFromArray:tempArr];
    if ([[MMHAccountSession currentSession] alreadyLoggedIn]) {
        [weakSelf sendRequestWithGetAddressList];
    }
    self.currentAddressSingleModel = [[MMHAddressSingleModel alloc]init];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.changeAddressTableView){
        self.changeAddressTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.changeAddressTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.changeAddressTableView.delegate = self;
        self.changeAddressTableView.dataSource = self;
        self.changeAddressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.changeAddressTableView.backgroundColor = [UIColor clearColor];
        self.changeAddressTableView.sectionIndexColor = [UIColor blueColor];
        self.changeAddressTableView.sectionIndexTrackingBackgroundColor = [UIColor grayColor];
        [self.view addSubview:self.changeAddressTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.changeAddressMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.changeAddressMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"定位到当前地址"] || indexPath.section == [self cellIndexPathSectionWithcellData:@"搜索具体地址"]){
        static NSString *cellIdentifyWithRowOne = @"cellWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleGray;
            // 1. 创建定位图标
            UIImageView *fixedLocationImageView = [[UIImageView alloc]init];
            fixedLocationImageView.backgroundColor = [UIColor clearColor];
            fixedLocationImageView.stringTag = @"fixedLocationImageView";
            fixedLocationImageView.image = [UIImage imageNamed:@"center_icon_location"];
            [cellWithRowOne addSubview:fixedLocationImageView];
            
            // 2. 创建文字
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [cellWithRowOne addSubview:fixedLabel];
            
            // 3. 创建currentlocation
            UIImageView *currentImageView = [[UIImageView alloc]init];
            currentImageView.backgroundColor = [UIColor clearColor];
            currentImageView.image = [UIImage imageNamed:@"pro_icon_positioning02"];
            currentImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(20), (cellHeight - MMHFloat(20)) / 2., MMHFloat(20), MMHFloat(20));
            currentImageView.stringTag = @"currentImageView";
            [cellWithRowOne addSubview:currentImageView];
        }
        UIImageView *fixedLocationImageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"fixedLocationImageView"];
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
        UIImageView *currentImageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"currentImageView"];
        
        if(indexPath.section == 0){
            fixedLocationImageView.image = [UIImage imageNamed:@"pro_icon_positioning"];
            fixedLocationImageView.frame = CGRectMake(MMHFloat(10), (cellHeight - MMHFloat(16))/2., MMHFloat(12), MMHFloat(16));
            currentImageView.hidden = NO;
            if (self.currentAddressSingleModel.areaId.length){
                fixedLabel.text = [NSString stringWithFormat:@"%@ %@ %@",self.currentAddressSingleModel.province,self.currentAddressSingleModel.city,self.currentAddressSingleModel.area];
            } else {
                fixedLabel.text = @"定位到当前位置";
            }
        } else {
            fixedLabel.text = @"搜索具体地址";
            fixedLocationImageView.image = [UIImage imageNamed:@"pro_icon_search"];
            fixedLocationImageView.frame = CGRectMake(MMHFloat(10), (cellHeight - MMHFloat(16))/2., MMHFloat(14), MMHFloat(16));
            currentImageView.hidden = YES;
        }
        
        // fixedLabel
        fixedLabel.frame = CGRectMake(CGRectGetMaxX(fixedLocationImageView.frame) + MMHFloat(10), 0, kScreenBounds.size.width - MMHFloat(130), cellHeight);
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"常用收货地址"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.text = @"常用收货地址";
                fixedLabel.frame = CGRectMake(MMHFloat(11), 0, kScreenBounds.size.width, cellHeight);
                [cellWithRowTwo addSubview:fixedLabel];
            }
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            MMHAddressChooseCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[MMHAddressChooseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            cellWithRowThr.cellHeight = cellHeight;
            MMHAddressSingleModel *addressSingleModel = [self.addressCityList.data objectAtIndex:indexPath.row - 1];
            cellWithRowThr.addressSingleModel = addressSingleModel;
            if ([self.isSelectedMutableArr containsObject:addressSingleModel]){
                [cellWithRowThr setChecked:YES];
            } else {
                [cellWithRowThr setChecked:NO];
            }
            return cellWithRowThr;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return MMHFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"定位到当前地址"]){// 获取当前地址
        [self currentLocation];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"常用收货地址"] && indexPath.row > 0){
        MMHAddressSingleModel *addressSingleModel = [self.addressCityList.data objectAtIndex:indexPath.row - 1];
        MMHAddressChooseCell *addressChooseCell = (MMHAddressChooseCell *)[tableView cellForRowAtIndexPath:indexPath];
        NSInteger modelIndex = [self.addressCityList.data indexOfObject:[self.isSelectedMutableArr lastObject]];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:modelIndex + 1 inSection:1];
        MMHAddressChooseCell *lastCell = (MMHAddressChooseCell *)[tableView cellForRowAtIndexPath:indexPath];
        [lastCell setChecked:NO];
        
        [self.isSelectedMutableArr removeLastObject];
        [self.isSelectedMutableArr addObject:addressSingleModel];
        [addressChooseCell setChecked:YES];
        
        [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = addressSingleModel;
        
        [self performSelector:@selector(blockWithAddressSingleModel:) withObject:addressSingleModel afterDelay:.3f];
    } else if (indexPath.section  == [self cellIndexPathSectionWithcellData:@"搜索具体地址"]){
        MMHSearchAddressViewController *searchAddressViewController = [[MMHSearchAddressViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        searchAddressViewController.locationModelBlock = ^(MMHAddressSingleModel *addressParameterModel){
            [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = addressParameterModel;
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.changedAddressBlock(addressParameterModel);
            if (strongSelf.changeAddressPageUsingType == MMHChangeAddressPageUsingTypeProduct){
                for (UIViewController *viewController in strongSelf.navigationController.viewControllers){
                    if ([viewController isKindOfClass:[MMHProductDetailViewController1 class]]){
                        [strongSelf.navigationController popToViewController:viewController animated:YES];
                    }
                }
            } else if (strongSelf.changeAddressPageUsingType == MMHChangeAddressPageUsingTypeHome){
                for (UIViewController *viewController in strongSelf.navigationController.viewControllers){
                    if ([viewController isKindOfClass:[MMHMamhaoViewController class]]){
                        [strongSelf.navigationController popToViewController:viewController animated:YES];
                    }
                }
            }
        };
        [self.navigationController pushViewController:searchAddressViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1){
        if (indexPath.row != 0){
            MMHAddressSingleModel *addressSingleModel = [self.addressCityList.data objectAtIndex:indexPath.row - 1];
            return [MMHAddressChooseCell cellHeightWithSingleModel:addressSingleModel];
        } else {
            return MMHFloat(44);
        }
    } else {
        return MMHFloat(44);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.changeAddressMutableArr.count){
        if ( [indexPath row] == 0) {
            [cell addSeparatorLineWithType:SeparatorTypeSingle];
        } else if ([indexPath row] == [[self.changeAddressMutableArr objectAtIndex:indexPath.section] count] - 1) {
            [cell addSeparatorLineWithType:SeparatorTypeBottom];
        }
        
        if ([[self.changeAddressMutableArr objectAtIndex:indexPath.section] count] == 1) {
            [cell addSeparatorLineWithType:SeparatorTypeSingle];
        }
    }
}

#pragma mark - OtherManager
-(void)currentLocation{// 获取当前定位地址
    __weak typeof(self)weakSelf = self;
    if ([MMHCurrentLocationModel sharedLocation].currentAreaId.length){
        [weakSelf.view hideProcessingView];
        weakSelf.currentAddressSingleModel.areaId = [MMHCurrentLocationModel sharedLocation].currentAreaId;
        weakSelf.currentAddressSingleModel.province = [MMHCurrentLocationModel sharedLocation].currentProvince;
        weakSelf.currentAddressSingleModel.city = [MMHCurrentLocationModel sharedLocation].currentCity;
        weakSelf.currentAddressSingleModel.area = [MMHCurrentLocationModel sharedLocation].currentDistrict;
        weakSelf.currentAddressSingleModel.gpsAddress = [MMHCurrentLocationModel sharedLocation].currentTownship;
        weakSelf.currentAddressSingleModel.addrDetail = [MMHCurrentLocationModel sharedLocation].currentStreet;
        
        [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = weakSelf.currentAddressSingleModel;
        
        [weakSelf.changeAddressTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf performSelector:@selector(blockWithAddressSingleModel:) withObject:weakSelf.currentAddressSingleModel afterDelay:.3f];
        
    } else {
        [weakSelf.view showProcessingView];
        [MMHLocationManager getArea];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(currentLocation) name:MMHCurrentLocationNotification object:nil];
    }
}

-(void)blockWithAddressSingleModel:(MMHAddressSingleModel *)addressSingleModel{
    self.changedAddressBlock(addressSingleModel);
    UIViewController *targetViewController = nil;
    if (self.changeAddressPageUsingType == MMHChangeAddressPageUsingTypeProduct){
        for (UIViewController *viewController in self.navigationController.viewControllers){
            if ([viewController isKindOfClass:[MMHProductDetailViewController1 class]]){
                targetViewController = viewController;
                break;
            }
        }
    }
    else if (self.changeAddressPageUsingType == MMHChangeAddressPageUsingTypeHome){
        for (UIViewController *viewController in self.navigationController.viewControllers){
            if ([viewController isKindOfClass:[MMHMamhaoViewController class]]){
                targetViewController = viewController;
                break;
            }
        }
    }
    if (targetViewController != nil) {
        [self.navigationController popToViewController:targetViewController animated:YES];
    }
}

#pragma mark - 接口
#pragma mark 获取地址
-(void)sendRequestWithGetAddressList{
    __weak typeof(self) weakSelf = self;
    [weakSelf.view showProcessingView];
    [[MMHNetworkAdapter sharedAdapter]sendRequestGetAddressListFrom:nil succeededHandler:^(MMHAddressListModel *addressCityList) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        strongSelf.addressCityList = addressCityList;
        
        NSMutableArray *tempMutableArr = [NSMutableArray array];
        NSInteger cellIndex = [self cellIndexPathSectionWithcellData:@"定位到当前地址"];
        [tempMutableArr addObject:@"常用收货地址"];
        [tempMutableArr addObjectsFromArray:addressCityList.data];
        
        [strongSelf.changeAddressMutableArr insertObject:tempMutableArr atIndex:cellIndex + 1];
        [strongSelf.changeAddressTableView reloadData];
    
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTips:@"获取默认收货地址失败"];
    }];
}

-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.changeAddressMutableArr.count ; i++){
        NSArray *dataTempArr = [self.changeAddressMutableArr objectAtIndex:i];
        NSString *tempString = [dataTempArr objectAtIndex:0];
        if ([tempString isEqualToString:string]){
            cellIndexPathOfSection = i;
        }
    }
    return cellIndexPathOfSection;
}



@end
