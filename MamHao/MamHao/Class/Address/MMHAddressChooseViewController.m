//
//  MMHAddressChooseViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressChooseViewController.h"
#import "UIImage+ImageEffects.h"
#import "MMHAreaAnalysis.h"
#import "MMHAreaTempModel.h"
@interface MMHAddressChooseViewController ()<UITableViewDataSource,UITableViewDelegate>{
    MMHAreaAnalysis *areaAnalysis;
    MMHAreaTempModel *areaTempModel;
    MMHAddressSingleModel *addressSingleModel;
}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)UITableView *citychooseTableView;               // tableView
@property (nonatomic,strong)NSMutableArray *addressMutableArray;            // dataSourceArr;
@property (nonatomic,strong)NSMutableDictionary *addressMutableDic;         // addressMutableDic
@property (nonatomic,strong) NSMutableArray *dataSourceMutableArray;
@property (nonatomic,strong)NSMutableArray *areaTempMutableArray;           // tempArr;
@end

@implementation MMHAddressChooseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
    
}


#pragma mark - 创建view
-(void)createSharetView{
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, screenRect.size.width, screenRect.size.height)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:246/256. green:246/256. blue:246/256. alpha:1];
    [self.view addSubview:_shareView];
}




#pragma mark 创建dismissButton
-(void)createDismissButton{
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}



#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak MMHAddressChooseViewController *weakVC = self;
    
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak MMHAddressChooseViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x - (weakVC.shareView.frame.size.width * 2 / 3.), screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}

- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}



#pragma mark - UITableView

-(void)arrayWithInit{
    areaAnalysis = [[MMHAreaAnalysis alloc]init];
    self.dataSourceMutableArray = [NSMutableArray array];
    [self.dataSourceMutableArray addObject:[areaAnalysis readFileBackProvinceArray]];
    self.areaTempMutableArray = [NSMutableArray array];
    areaTempModel = [[MMHAreaTempModel alloc]init];
    addressSingleModel = [[MMHAddressSingleModel alloc]init];
}


-(void)createTableView{
    self.citychooseTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.citychooseTableView setHeight:self.view.bounds.size.height];
    self.citychooseTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.citychooseTableView.delegate = self;
    self.citychooseTableView.dataSource = self;
    self.citychooseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.citychooseTableView.backgroundColor = [UIColor clearColor];
    [self.shareView addSubview:self.citychooseTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceMutableArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.font = [UIFont systemFontOfSize:MMHFloat(16.)];
        cell.textLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    }
    NSString *areaString = [[self.dataSourceMutableArray objectAtIndex: indexPath.section] objectAtIndex:indexPath.row];
    NSString *areaString1 = [MMHAreaAnalysis cleanAreaString:areaString];
    NSArray *areaArray = [MMHAreaAnalysis getAddress:areaString1];
    cell.textLabel.text = [areaArray objectAtIndex:0];
    
    return cell;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *areaString = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if ([[MMHAreaAnalysis getAddress:areaString] count] == 2){      // 省
        areaTempModel = [areaAnalysis backAreaArrayWithAreaName:areaString andAreaTempModel:areaTempModel];
        if (areaTempModel.areaType == MMHAreaTypeCity){         // 城市结尾-【北京】
        addressSingleModel.province = areaString;
            [self tableviewReloadWithArea];
        } else if (areaTempModel.areaType == MMHAreaTypeArea){      // 区县结尾 -【杭州】
            areaTempModel = [areaAnalysis backAreaArrayWithAreaName:areaString andAreaTempModel:areaTempModel];
        addressSingleModel.province = areaString;
            [self tableviewReloadWithArea];
        }
    } else if ([[MMHAreaAnalysis getAddress:areaString] count] == 3){   // 市
        areaTempModel = [areaAnalysis backCityArrayWithCityName:areaString areaArray:areaTempModel.areaTempArray andAreaTempModel:areaTempModel];
        if (areaTempModel.areaType == MMHAreaTypeEnd){
            addressSingleModel.city = areaString;
            self.areaBlock(addressSingleModel);
            [self sheetViewDismiss];                            // 【dismiss】
        } else if (areaTempModel.areaType == MMHAreaTypeArea){
            addressSingleModel.city = areaString;
            [self tableviewReloadWithArea];
        }
    } else if ([[MMHAreaAnalysis getAddress:areaString] count] == 4){   // 【区】
        addressSingleModel.area = areaString;
        self.areaBlock(addressSingleModel);
        [self sheetViewDismiss];                                //【dismiss】
    }
}

#pragma mark tableViewReload
-(void)tableviewReloadWithArea{
    [self.areaTempMutableArray addObjectsFromArray:[self.dataSourceMutableArray objectAtIndex:0]];
    [self.dataSourceMutableArray removeObjectAtIndex:0];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
    [self.citychooseTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
    
    [self performSelector:@selector(tableReload) withObject:nil afterDelay:.4f];
}

-(void)tableReload{
    if (areaTempModel.areaType == MMHAreaTypeCity){
        [self.dataSourceMutableArray insertObject:[areaTempModel.areaArray objectAtIndex:0] atIndex:0];
        NSMutableIndexSet *shouhuoTimeSet = [[NSMutableIndexSet alloc]initWithIndex:0];
        [self.citychooseTableView insertSections:shouhuoTimeSet withRowAnimation:UITableViewRowAnimationBottom];
    } else {
        [self.dataSourceMutableArray insertObject:areaTempModel.areaArray atIndex:0];
        NSMutableIndexSet *shouhuoTimeSet = [[NSMutableIndexSet alloc]initWithIndex:0];
        [self.citychooseTableView insertSections:shouhuoTimeSet withRowAnimation:UITableViewRowAnimationBottom];
    }
}




-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.citychooseTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [self.dataSourceMutableArray count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([self.dataSourceMutableArray count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return MMHFloat(50);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 64;
    } else {
        return 20;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    if (section == 0){
        headerView.backgroundColor = [UIColor whiteColor];
        headerView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 64);
        UILabel *smartLable = [[UILabel alloc]init];
        smartLable.backgroundColor = [UIColor clearColor];
        smartLable.text = @"配送至";
        smartLable.frame = CGRectMake(0, 0, self.view.frame.size.width * 2 / 3., 64);
        smartLable.textAlignment = NSTextAlignmentCenter;
        [headerView addSubview:smartLable];
    } else {
        headerView.backgroundColor = [UIColor clearColor];
    }
    return headerView;
}




@end