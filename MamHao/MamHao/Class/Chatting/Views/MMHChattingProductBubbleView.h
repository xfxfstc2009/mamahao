//
//  MMHChattingProductBubbleView.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

@import UIKit;
#import "EMChatBaseBubbleView.h"


extern NSString *const MMHRouterEventProductBubbleTapped;


@interface MMHChattingProductBubbleView : EMChatBaseBubbleView

@end
