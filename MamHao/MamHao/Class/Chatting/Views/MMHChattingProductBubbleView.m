//
//  MMHChattingProductBubbleView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChattingProductBubbleView.h"
#import "UIImageView+EMWebCache.h"
#import "MMHAssistant.h"
#import "MMHAppearance.h"


NSString *const MMHRouterEventProductBubbleTapped = @"MMHRouterEventProductBubbleTapped";


static const CGFloat MMHChattingProductBubbleViewImageWidth = 70.0f;
static const CGFloat MMHChattingProductBubbleViewImageHeight = 70.0f;


@interface MMHChattingProductBubbleView ()

@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) MMHImageView *productImageView;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *productPriceLabel;
@end


@implementation MMHChattingProductBubbleView


- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self configureSubviews];
    }
    
    return self;
}


- (void)configureSubviews
{
    CGFloat leftPadding = self.model.isSender ? (BUBBLE_VIEW_PADDING) : (BUBBLE_VIEW_PADDING + BUBBLE_ARROW_WIDTH);
    
    UILabel *tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding, BUBBLE_VIEW_PADDING, 256.0f - BUBBLE_VIEW_PADDING - BUBBLE_VIEW_PADDING - BUBBLE_ARROW_WIDTH, 15.0f)];
    tipsLabel.font = [UIFont systemFontOfSize:15.0f];
    [self addSubview:tipsLabel];
    self.tipsLabel = tipsLabel;
    
    MMHImageView *productImageView = [[MMHImageView alloc] initWithFrame:CGRectMake(leftPadding, 115.0f - BUBBLE_VIEW_PADDING - MMHChattingProductBubbleViewImageHeight, MMHChattingProductBubbleViewImageWidth, MMHChattingProductBubbleViewImageHeight)];
    productImageView.clipsToBounds = YES;
    productImageView.layer.cornerRadius = 3.0f;
    [self addSubview:productImageView];
    self.productImageView = productImageView;
    
    UILabel *productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.productImageView.frame) + 10.0f, CGRectGetMinY(self.productImageView.frame), 256.0f - BUBBLE_VIEW_PADDING - BUBBLE_VIEW_PADDING - BUBBLE_ARROW_WIDTH - MMHChattingProductBubbleViewImageWidth - 10.0f, 0.0f)];
    productNameLabel.numberOfLines = 2;
    productNameLabel.font = [UIFont systemFontOfSize:14.0f];
    [self addSubview:productNameLabel];
    self.productNameLabel = productNameLabel;
    
    UILabel *productPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.productNameLabel.frame), CGRectGetMaxY(self.productNameLabel.frame) + 8.0f, self.productNameLabel.width, 0.0f)];
    productPriceLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    productPriceLabel.textColor = [MMHAppearance pinkColor];
    [self addSubview:productPriceLabel];
    self.productPriceLabel = productPriceLabel;
}


- (CGSize)sizeThatFits:(CGSize)size
{
    return CGSizeMake(256.0f, 115.0f);
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat leftPadding = self.model.isSender ? (BUBBLE_VIEW_PADDING) : (BUBBLE_VIEW_PADDING + BUBBLE_ARROW_WIDTH);
    
    self.tipsLabel.frame = CGRectMake(leftPadding, BUBBLE_VIEW_PADDING, 256.0f - BUBBLE_VIEW_PADDING - BUBBLE_VIEW_PADDING - BUBBLE_ARROW_WIDTH, 15.0f);
    self.productImageView.frame = CGRectMake(leftPadding, 115.0f - BUBBLE_VIEW_PADDING - MMHChattingProductBubbleViewImageHeight, MMHChattingProductBubbleViewImageWidth, MMHChattingProductBubbleViewImageHeight);
    self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + 10.0f, CGRectGetMinY(self.productImageView.frame), 256.0f - BUBBLE_VIEW_PADDING - BUBBLE_VIEW_PADDING - BUBBLE_ARROW_WIDTH - MMHChattingProductBubbleViewImageWidth - 10.0f, 0.0f);
    [self.productNameLabel updateHeight];
    self.productPriceLabel.frame = CGRectMake(CGRectGetMinX(self.productNameLabel.frame), CGRectGetMaxY(self.productNameLabel.frame) + 8.0f, self.productNameLabel.width, 0.0f);
    [self.productPriceLabel updateHeight];
}

#pragma mark - setter

- (void)setModel:(MessageModel *)model
{
    [super setModel:model];
    
    self.tipsLabel.text = @"我正在看：";
    [self.productImageView updateViewWithImageAtURL:model.productImageURLString];
    [self.productNameLabel setText:model.productName constrainedToLineCount:2];
    NSString *priceString = [NSString stringWithPrice:model.productPrice];
    [self.productPriceLabel setText:priceString constrainedToLineCount:1];
}


#pragma mark - public

- (void)bubbleViewPressed:(id)sender
{
    [self routerEventWithName:MMHRouterEventProductBubbleTapped userInfo:@{KMESSAGEKEY: self.model}];
}


+ (CGFloat)heightForBubbleWithObject:(MessageModel *)object
{
    return 115.0f;
}

@end
