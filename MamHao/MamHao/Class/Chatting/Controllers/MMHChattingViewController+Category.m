//
//  MMHChattingViewController+Category.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChattingViewController+Category.h"

@implementation MMHChattingViewController (Category)


- (void)registerBecomeActive{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBecomeActive)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
}


- (void)didBecomeActive{
    [self reloadData];
}


@end
