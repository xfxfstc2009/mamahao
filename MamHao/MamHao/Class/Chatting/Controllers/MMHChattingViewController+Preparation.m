//
//  MMHChattingViewController+Preparation.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <EaseMobSDK/EaseMob.h>
#import "MMHChattingViewController+Preparation.h"
#import "MMHAccountSession.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Chatting.h"


static NSString *MMHChattingViewControllerWaiterID = @"MMHChattingViewControllerWaiterID";
static NSString *MMHChattingViewControllerWaiterName = @"MMHChattingViewControllerWaiterName";


@implementation MMHChattingViewController (Preparation)


+ (BOOL)isReadyToChat
{
    BOOL sessionReady = [[MMHAccountSession currentSession] isReadyToChat];
    if (!sessionReady) {
        return NO;
    }

    NSString *waiterID = [self waiterID];
    if (waiterID.length == 0) {
        return NO;
    }

    return YES;
}


+ (void)prepareForChattingWithContext:(id)context succeededHandler:(void (^)(MMHChattingViewController *chattingViewController))succeededHandler failedHander:(void (^)(NSError *error))failedHandler
{
    [self getWaiterIDWithCompletion:^(NSString *waiterID, NSError *error, BOOL succeeded) {
        if (!succeeded) {
            failedHandler(error);
            return;
        }

        [[MMHAccountSession currentSession] loginChattingAccountWithWaiterID:waiterID succeededHandler:^{
            MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:context];
            succeededHandler(chattingViewController);
        } failedHander:^(NSError *e) {
            failedHandler(e);
        }];
    }];

//    if ([self isReadyToChat]) {
//        MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:context];
//        succeededHandler(chattingViewController);
//        return;
//    }
//
//    [[MMHAccountSession currentSession] loginChattingAccountWithWaiterID:(NSString *)waiterID succeededHandler:^{
//        MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:context];
//        succeededHandler(chattingViewController);
//    } failedHander:^(NSError *error) {
//        failedHandler(error);
//    }];
}


+ (void)getWaiterIDWithCompletion:(void (^)(NSString *waiterID, NSError *error, BOOL succeeded))completion
{
    NSString *waiterID = [self waiterID];
    if (waiterID.length != 0) {
        completion(waiterID, nil, YES);
        return;
    }

    [[MMHNetworkAdapter sharedAdapter] fetchWaiterListFrom:self
                                          succeededHandler:^(NSArray *waiters) {
                                              if (waiters.count == 0) {
                                                  completion(nil, nil, NO);
                                                  return;
                                              }
                                              NSInteger anyIndex = arc4random() % waiters.count;
                                              NSDictionary *waiter = waiters[anyIndex];
                                              NSString *wid = waiter[@"user_name"];
                                              NSString *waiterName = waiter[@"real_name"];
                                              [self setWaiterID:wid];
                                              [self setWaiterName:waiterName];
                                              completion(wid, nil, YES);
                                          } failedHandler:^(NSError *error) {
                completion(nil, error, NO);
            }];
}


+ (NSString *)waiterID
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:MMHChattingViewControllerWaiterID];
}


+ (void)setWaiterID:(NSString *)waiterID
{
    [[NSUserDefaults standardUserDefaults] setObject:waiterID forKey:MMHChattingViewControllerWaiterID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (NSString *)waiterName
{
    return [[NSUserDefaults standardUserDefaults] stringForKey:MMHChattingViewControllerWaiterName];
}


+ (void)setWaiterName:(NSString *)waiterName
{
    [[NSUserDefaults standardUserDefaults] setObject:waiterName forKey:MMHChattingViewControllerWaiterName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
