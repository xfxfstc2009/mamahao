//
//  MMHChattingViewController+Category.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChattingViewController.h"

@interface MMHChattingViewController (Category)

- (void)registerBecomeActive;
- (void)didBecomeActive;

@end
