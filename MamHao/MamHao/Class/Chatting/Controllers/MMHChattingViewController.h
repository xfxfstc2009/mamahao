//
//  MMHChattingViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHProductDetailModel;


@interface MMHChattingViewController : AbstractViewController

//- (instancetype)initWithProductDetail:(MMHProductDetailModel *)productDetail;

- (void)reloadData;

- (instancetype)initWithContext:(id)context4;

@end
