//
//  MMHChattingViewController+Preparation.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChattingViewController.h"


@interface MMHChattingViewController (Preparation)

+ (BOOL)isReadyToChat;

+ (void)prepareForChattingWithContext:(id)context succeededHandler:(void (^)(MMHChattingViewController *chattingViewController))succeededHandler failedHander:(void (^)(NSError *error))failedHandler;

+ (NSString *)waiterID;
+ (void)setWaiterID:(NSString *)waiterID;
+ (NSString *)waiterName;
+ (void)setWaiterName:(NSString *)waiterName;
@end
