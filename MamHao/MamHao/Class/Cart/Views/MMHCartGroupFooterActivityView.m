//
//  MMHCartGroupFooterActivityView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MMHCartGroupFooterActivityView.h"
#import "MamHao-Swift.h"
#import "UIView+Extension.h"


@interface MMHCartGroupFooterActivityView ()

@property (nonatomic, strong) UILabel *typeIndicatorLabel;
@property (nonatomic, strong) UILabel *discountPriceLabel;
@property (nonatomic, strong) UILabel *titleLabel;
@end


@implementation MMHCartGroupFooterActivityView


- (id)initWithFrame:(CGRect)frame activity:(CartItemActivity *)activity
{
    self = [self initWithFrame:frame];
    if (self) {
        self.clipsToBounds = NO;
        self.backgroundColor = [UIColor whiteColor];

        CGFloat centerY = 7.5f;
        
        if (activity.type == MMHActivityTypeReachThenReduce) {
            UILabel *typeIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 15.0f, 15.0f)];
            typeIndicatorLabel.textColor = [UIColor whiteColor];
            typeIndicatorLabel.font = [MMHAppearance smallTipsFont];
            typeIndicatorLabel.backgroundColor = [MMHAppearance redColor];
            typeIndicatorLabel.layer.cornerRadius = 1.0f;
            typeIndicatorLabel.textAlignment = NSTextAlignmentCenter;
            [typeIndicatorLabel setSingleLineText:@"减" constrainedToWidth:CGFLOAT_MAX withEdgeInsets:UIEdgeInsetsMake(0.0f, 2.0f, 0.0f, 2.0f)];
            typeIndicatorLabel.centerY = centerY;
            [self addSubview:typeIndicatorLabel];
            self.typeIndicatorLabel = typeIndicatorLabel;
        }
        else if (activity.type == MMHActivityTypeReachThenRebate) {
            UILabel *typeIndicatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 15.0f, 15.0f)];
            typeIndicatorLabel.textColor = [UIColor whiteColor];
            typeIndicatorLabel.font = [MMHAppearance smallTipsFont];
            typeIndicatorLabel.backgroundColor = [MMHAppearance redColor];
            typeIndicatorLabel.layer.cornerRadius = 1.0f;
            typeIndicatorLabel.textAlignment = NSTextAlignmentCenter;
            [typeIndicatorLabel setSingleLineText:@"折" constrainedToWidth:CGFLOAT_MAX withEdgeInsets:UIEdgeInsetsMake(0.0f, 2.0f, 0.0f, 2.0f)];
            typeIndicatorLabel.centerY = centerY;
            [self addSubview:typeIndicatorLabel];
            self.typeIndicatorLabel = typeIndicatorLabel;
        }
    
        if (activity.enabled) {
            UILabel *discountPriceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            discountPriceLabel.textColor = [MMHAppearance redColor];
            discountPriceLabel.font = [MMHAppearance textFont];
            discountPriceLabel.textAlignment = NSTextAlignmentRight;
            [discountPriceLabel moveToRight:self.bounds.size.width];
            NSString *discountPriceString = [NSString stringWithFormat:@"已减: ￥%ld", (long)activity.discountValue];
            [discountPriceLabel setSingleLineText:discountPriceString];
            discountPriceLabel.centerY = centerY;
            [self addSubview:discountPriceLabel];
            self.discountPriceLabel = discountPriceLabel;
        }
    
        CGFloat x = 0.0f;
        if (self.typeIndicatorLabel) {
            x = self.typeIndicatorLabel.right + 5.0f;
        }
        CGFloat maxX = self.bounds.size.width;
        if (self.discountPriceLabel) {
            maxX = self.discountPriceLabel.left - 5.0f;
        }
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, 0.0f, maxX - x, 0.0f)];
        titleLabel.textColor = [MMHAppearance grayColor];
        titleLabel.font = [MMHAppearance textFont];
        [titleLabel setText:activity.title constrainedToLineCount:1];
        titleLabel.centerY = centerY;
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;
    }
    return self;
}


@end
