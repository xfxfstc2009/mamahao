//
//  MMHCartEnabledActivitiesView.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHCartData;


@interface MMHCartEnabledActivitiesView : UIView

@property (nonatomic, strong) NSArray *activities;

+ (CGFloat)heightForCart:(MMHCartData *)cart;
@end
