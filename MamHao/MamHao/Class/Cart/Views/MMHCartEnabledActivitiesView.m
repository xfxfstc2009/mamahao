//
//  MMHCartEnabledActivitiesView.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartEnabledActivitiesView.h"
#import "MMHCartData.h"
#import "MMHAppearance.h"
#import "MMHCartItemCell.h"
#import "MamHao-Swift.h"


@implementation MMHCartEnabledActivitiesView


- (void)setActivities:(NSArray *)activities
{
    _activities = activities;

    [self removeAllSubviews];
    
    if (activities.count == 0) {
        [self setHeight:0.0f];
        return;
    }

    CGFloat y = 5.0f;
    for (NSInteger i = 0; i < activities.count; i++) {
        CartItemActivity *activity = activities[i];
        MMHSmallTipsLabel *smallTipsLabel = [[MMHSmallTipsLabel alloc] initWithFrame:CGRectMake(15.0f, y, self.bounds.size.width - 15.0f - 10.0f - 90.0f, [MMHAppearance smallTipsFont].pointSize)];
        smallTipsLabel.textColor = [UIColor whiteColor];
        NSString *supplierName = activity.supplierName;
        if (supplierName.length == 0) {
            supplierName = @"妈妈好";
        }
        // TODO: - Louis - cart, to be removed (this entire view)
//        smallTipsLabel.text = [NSString stringWithFormat:@"%@ %@", supplierName, activity.introduction];
        [self addSubview:smallTipsLabel];

        MMHSmallTipsLabel *discountLabel = [[MMHSmallTipsLabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 10.0f - 90.0f, y, 90.0f, [MMHAppearance smallTipsFont].pointSize)];
        discountLabel.textColor = [UIColor whiteColor];
        discountLabel.textAlignment = NSTextAlignmentRight;
        MMHPrice discountValue = activity.discountValue;
        discountLabel.text = [NSString stringWithPrice:(discountValue * -1.0f)];
        [self addSubview:discountLabel];

        y = CGRectGetMaxY(smallTipsLabel.frame) + 5.0f;
    }
    [self setHeight:y];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [[UIColor colorWithHexString:@"f84c60"] colorWithAlphaComponent:0.85f];
    }
    return self;
}


+ (CGFloat)heightForCart:(MMHCartData *)cart
{
    NSInteger count = cart.enabledActivities.count;
    NSLog(@"=== have got %ld enabled activities", (long)count);
    if (count == 0) {
        return 0.0f;
    }
    else {
        CGFloat height = 5.0f;
        height += (5.0f + [MMHAppearance smallTipsFont].pointSize) * (CGFloat)count;
        return height;
    }
}


@end
