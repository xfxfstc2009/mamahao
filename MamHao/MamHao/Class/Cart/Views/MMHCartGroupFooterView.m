//
//  MMHCartGroupFooterView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartGroupFooterView.h"
#import "MamHao-Swift.h"
#import "MMHCartGroupFooterActivityView.h"


@interface MMHCartGroupFooterView ()

@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) NSMutableArray *activityViews;
@end


@implementation MMHCartGroupFooterView


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self addBottomSeparatorLine];
    }
    return self;
}


- (void)setGroup:(CartItemGroup *)group
{
    _group = group;

    if (group.activities.count == 0) {
        if (self.line) {
            [self.line removeFromSuperview];
            self.line = nil;
        }

        for (MMHCartGroupFooterActivityView *activityView in self.activityViews) {
            [activityView removeFromSuperview];
        }
        [self.activityViews removeAllObjects];

        return;
    }

    if (self.line == nil) {
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, mmh_screen_width() - 20.0f, mmh_pixel())];
        line.backgroundColor = [MMHAppearance separatorColor];
        [self.contentView addSubview:line];
        self.line = line;
    }

    for (MMHCartGroupFooterActivityView *activityView in self.activityViews) {
        [activityView removeFromSuperview];
    }
    [self.activityViews removeAllObjects];
    NSMutableArray *activityViews = [[NSMutableArray alloc] init];
    CGFloat y = 10.0f;
    for (CartItemActivity *activity in group.activities) {
        MMHCartGroupFooterActivityView *activityView = [[MMHCartGroupFooterActivityView alloc] initWithFrame:CGRectMake(40.0f, y, mmh_screen_width() - 50.0f, 25.0f) activity:activity];
        [self.contentView addSubview:activityView];
        [activityViews addObject:activityView];
        y = activityView.bottom;
    }
    self.activityViews = activityViews;
}


+ (CGFloat)heightForGroup:(CartItemGroup *)group
{
    NSUInteger count = group.activities.count;
    if (count == 0) {
        return 1.0f;
    }

    CGFloat height = 10.0f;
    height += (CGFloat)count * 25.0f;
    return height;
}


@end
