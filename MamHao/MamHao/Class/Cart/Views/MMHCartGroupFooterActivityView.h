//
//  MMHCartGroupFooterActivityView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CartItemActivity;


@interface MMHCartGroupFooterActivityView : UIView

- (id)initWithFrame:(CGRect)frame activity:(CartItemActivity *)activity;
@end
