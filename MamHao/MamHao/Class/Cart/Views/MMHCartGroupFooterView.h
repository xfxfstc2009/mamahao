//
//  MMHCartGroupFooterView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class CartItemGroup;


@interface MMHCartGroupFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) CartItemGroup *group;

+ (CGFloat)heightForGroup:(CartItemGroup *)group;
@end
