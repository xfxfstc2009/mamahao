//
//  MMHCartItem.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"


typedef NS_ENUM(NSInteger, MMHSupplierType) {
    MMHSupplierTypeShop,
    MMHSupplierTypeCompany,
    MMHSupplierTypeMamhao,
};


@class MMHCartItem;


@protocol MMHCartItemDelegate <NSObject>

- (void)itemQuantityChanged:(MMHCartItem *)item;
- (void)item:(MMHCartItem *)item chosen:(BOOL)chosen;

@end


@interface MMHCartItem : MMHFetchModel

@property (nonatomic, weak) id<MMHCartItemDelegate> delegate;

@property (nonatomic) MMHID itemId;
@property (nonatomic) MMHID templateId;
@property (nonatomic) NSInteger quantity;
@property (nonatomic, strong) NSString *itemName;
@property (nonatomic, strong) NSString *itemPic;
@property (nonatomic) MMHPrice itemPrice;
@property (nonatomic) BOOL isBindShop; // TODO: - Louis - cart
@property (nonatomic) BOOL chosen;
@property (nonatomic) BOOL available;

@property (nonatomic, strong) NSArray *activities;
@property (nonatomic, strong) NSArray *specs;

// these properties are passed from CartItemGroup
@property (nonatomic, copy) NSString *shopId;
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, copy) NSString *companyId;
@property (nonatomic, copy) NSString *warehouseId;
//@property (nonatomic) MMHID areaId;
// end these properties are passed from CartItemGroup


@property (nonatomic) MMHSupplierType supplierType;
@property (nonatomic, readonly, copy) NSString *supplierID;

- (NSString *)specDisplayName;
- (MMHPrice)actualPrice;

- (NSDictionary *)parameters;
@end
