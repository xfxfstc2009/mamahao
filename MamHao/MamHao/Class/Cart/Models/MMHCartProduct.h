//
//  MMHCartProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProduct.h"


@class MMHCartItem;


@interface MMHCartProduct : MMHProduct <MMHProductProtocol>

@property (nonatomic) BOOL bindsShop;
@property (nonatomic, strong) NSString *itemId;

- (id)initWithCartItem:(MMHCartItem *)cartItem;
@end
