//
//  MMHCartItem.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartItem.h"
#import "MamHao-Swift.h"


@implementation MMHCartItem


- (void)setQuantity:(NSInteger)quantity
{
    _quantity = quantity;

    if (self.delegate) {
        [self.delegate itemQuantityChanged:self];
    }
}


- (void)setChosen:(BOOL)chosen
{
    _chosen = chosen;

    if (self.delegate) {
        [self.delegate item:self chosen:chosen];
    }
}


- (NSString *)supplierID
{
    switch (self.supplierType) {
        case MMHSupplierTypeShop:
            return self.shopId;
            break;
        case MMHSupplierTypeCompany:
            return self.companyId;
            break;
        default:
            break;
    }
    return 0;
}


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.activities = [dict[@"activity"] transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
            return [[CartItemActivity alloc] initWithDictionary:originalObject];
        }];
        self.isBindShop = [dict[@"isBindShop"] boolValue];
        
        // simulation
//        self.itemName = @"其实我是拒绝的0其实我是拒绝的1其实我是拒绝的2其实我是拒绝的3其实我是拒绝的4其实我是拒绝的5其实我是拒绝的6其实我是拒绝的7";
//        NSMutableArray *as = [NSMutableArray array];
//        for (NSInteger i = 0; i < 3; i++) {
//            CartItemActivity *activity = [[CartItemActivity alloc] initWithType:@"慢件" introduction:@"因为我没有用过" activityId:@"111"];
//            [as addObject:activity];
//        }
//        self.activities = as;
        // end simulation
        self.specs = [dict[@"spec"] transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
            return [[CartItemSpec alloc] initWithDictionary:originalObject];
        }];

//        [self configureSupplierTypeWithDictionary:dict];
    }
    return self;
}


- (void)configureSupplierTypeWithDictionary:(NSDictionary *)dictionary
{
    if ([[dictionary allKeys] containsObject:@"shopId"]) {
        id shopID = dictionary[@"shopId"];
        if (shopID) {
            if ([shopID isKindOfClass:[NSString class]]) {
                if ([shopID length] != 0) {
                    self.supplierType = MMHSupplierTypeShop;
                    return;
                }
            }
            if ([shopID isKindOfClass:[NSNumber class]]) {
                if ([shopID integerValue] != 0) {
                    self.supplierType = MMHSupplierTypeShop;
                    return;
                }
            }
        }
    }
    if ([[dictionary allKeys] containsObject:@"companyId"]) {
        id companyID = dictionary[@"companyId"];
        if (companyID) {
            if ([companyID isKindOfClass:[NSString class]]) {
                if ([companyID length] != 0) {
                    self.supplierType = MMHSupplierTypeCompany;
                    return;
                }
            }
            if ([companyID isKindOfClass:[NSNumber class]]) {
                if ([companyID integerValue] != 0) {
                    self.supplierType = MMHSupplierTypeCompany;
                    return;
                }
            }
        }
    }

    self.supplierType = MMHSupplierTypeMamhao;
}


- (NSString *)description
{
    NSMutableString *string = [NSMutableString string];
    [string appendFormat:@"itemid = %ld\n", self.itemId];
    [string appendFormat:@"templateId = %ld\n", self.templateId];
    [string appendFormat:@"quantity = %ld\n", (long)self.quantity];
    [string appendFormat:@"itemPrice = %f\n", self.itemPrice];
    [string appendFormat:@"available = %d\n", self.available];
    [string appendFormat:@"shopName = %@\n", self.shopName];
    return string;
};


- (NSString *)specDisplayName
{
    NSArray *names = [self.specs transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
        CartItemSpec *spec = (CartItemSpec *)originalObject;
        return spec.value;
    }];
    return [names componentsJoinedByString:@","];
}


- (MMHPrice)actualPrice
{
    MMHPrice price = self.itemPrice;
    for (CartItemActivity *activity in self.activities) {
        price -= activity.discountValue;
    }
    return price;
}


- (NSDictionary *)parameters
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"itemId"] = @(self.itemId);
    parameters[@"templateId"] = @(self.templateId);
//    parameters[@"areaId"] = @(self.areaId);

    switch (self.supplierType) {
        case MMHSupplierTypeShop: {
            parameters[@"shopId"] = self.shopId;
//            parameters[@"companyId"] = self.companyId;
            [parameters setNullableObject:self.companyId forKey:@"companyId"];
            break;
        }
        case MMHSupplierTypeCompany: {
            parameters[@"companyId"] = self.companyId;
            break;
        }
        default: {
            break;
        }
    }
    
    [parameters setNullableObject:self.warehouseId forKey:@"warehouseId"];

    return parameters;
}
@end
