//
//  CartItemActivity.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class CartItemActivity: NSObject {
    
    var activityId: String
    var type: MMHActivityType = MMHActivityType.ReachThenReduce
    var title: String
//    var introduction: String
    var enabled: Bool
    var discountValue: MMHPrice = 0
    
    var supplierName: String
    
    convenience init(dictionary: [String: AnyObject]) {
        self.init(dictionary: dictionary, supplierName: "")
    }
    
    init(dictionary: [String: AnyObject], supplierName: String) {
        self.activityId = dictionary.nonnullStringForKey("activityId")
        if let typeValue = dictionary.intForKey("type") {
            if let type = MMHActivityType(rawValue: typeValue) {
                self.type = type
            }
        }
        self.title = dictionary["title"] as! String
//        self.introduction = dictionary["description"] as! String
        if let enabled = dictionary["meet"] as! Bool? {
            self.enabled = enabled
        }
        else {
            self.enabled = false
        }
        
        if self.enabled {
            if let discountValue = dictionary["reducePrice"] as! MMHPrice? {
                self.discountValue = discountValue
            }
        }
        
        self.supplierName = supplierName
        super.init()
    }
    
    // simulation
    init(title: String, introduction: String, activityId: String) {
        self.title = title
//        self.introduction = introduction
        self.activityId = activityId
        self.enabled = true
        self.supplierName = ""
        super.init()
    }
    // end simulation

}


//class CartActivityGroup: NSObject {
//    
//    enum CartActivityGroupType {
//        case Shop
//        case Company
//        case Mamhao
//    }
//    
//    var type: CartActivityGroupType
//    var id: String
//    var activities: [CartItemActivity]
//    
//    init(dictionary: [String: AnyObject], type: CartActivityGroupType) {
//        self.type = type
//        if let id: AnyObject = dictionary["id"] {
//            self.id = id as! String
//        }
//        else {
//            self.id = "0"
//        }
//        
//        let a = dictionary["activity"] as! [[String: AnyObject]]
//        self.activities = a.map({
//            (dict: [String : AnyObject]) -> CartItemActivity in
//            let activity = CartItemActivity(dictionary: dict)
//            return activity
//        })
//        
//        super.init()
//    }
//}
//
//
//class CartActivityData: NSObject {
//    
//    var shopGroups: [CartActivityGroup]?
//    var companyGroups: [CartActivityGroup]?
//    var mamhaoGroups: [CartActivityGroup]?
//    
//    init(dictionary: [String: [[String: AnyObject]]]) {
//        if let shops = dictionary["shop"] {
//            self.shopGroups = shops.map({
//                (shopDictionary: [String : AnyObject]) -> CartActivityGroup in
//                let group = CartActivityGroup(dictionary: shopDictionary, type: .Shop)
//                return group
//            })
//        }
//        
//        if let companies = dictionary["company"] {
//            self.companyGroups = companies.map({
//                (shopDictionary: [String : AnyObject]) -> CartActivityGroup in
//                let group = CartActivityGroup(dictionary: shopDictionary, type: .Company)
//                return group
//            })
//        }
//        
//        if let mamhaos = dictionary["mamhao"] {
//            self.mamhaoGroups = mamhaos.map({
//                (shopDictionary: [String : AnyObject]) -> CartActivityGroup in
//                let group = CartActivityGroup(dictionary: shopDictionary, type: .Mamhao)
//                return group
//            })
//        }
//        
//        super.init()
//    }
//    
//}
