//
//  MMHCartData.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"


@class CartItemGroup;
@class MMHCartItem;
@class MMHCartData;
@class CartActivityData;


@protocol MMHCartDataDelegate <NSObject>

- (void)cartDataPriceChanged:(MMHCartData *)cartData;

@end


@interface MMHCartData : MMHFetchModel

@property (nonatomic, weak) id<MMHCartDataDelegate> delegate;

@property (nonatomic) MMHID cartID;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSMutableArray *groups;

@property (nonatomic) MMHPrice originalPrice;
@property (nonatomic) MMHPrice savedPrice;
@property (nonatomic) MMHPrice totalPrice;

@property (nonatomic, strong) CartActivityData *activityData;
@property (nonatomic, strong) NSArray *enabledActivities;

- (BOOL)isEmpty;

- (NSInteger)numberOfGroups;
- (CartItemGroup *)groupAtIndex:(NSInteger)index;

//- (void)changeQuantityOfItem:(MMHCartItem *)cartItem to:(NSInteger)toValue completion:(void (^)(BOOL succeeded, NSError *error))completion;

- (BOOL)allItemChosen;
- (void)chooseAll:(BOOL)flag;

//- (void)deleteItem:(MMHCartItem *)cartItem completion:(void (^)(BOOL succeeded, BOOL groupHasBeenDeletedAsWell, NSError *error))completion;

- (void)configureGroupsWithDictionary:(NSDictionary *)dictionary;
- (void)configurePricesWithDictionary:(NSDictionary *)dictionary;

- (NSInteger)numberOfChosenItems;

//- (void)changeChosenStatusOfItem:(MMHCartItem *)cartItem completion:(void (^)(BOOL succeeded, NSError *error))completion;
//- (void)changeChooseAllStatusWithCompletion:(void (^)(BOOL succeeded, NSError *error))completion;

- (NSArray *)shopIDsAndCompanyIDs;

//- (void)configureActivitiesWithDictionary:(NSDictionary *)dictionary;
@end


@interface MMHCartData (CartID)

+ (void)setCartID:(MMHID)cartID;
+ (MMHID)cartID;

@end
