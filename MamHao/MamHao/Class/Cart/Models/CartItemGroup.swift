//
//  CartItemGroup.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


@objc(MMHCartItemGroup)
class CartItemGroup: NSObject {
    
    var supplierType: MMHSupplierType
    var supplierID: String
    var supplierName: String
//    var areaID: MMHID?
    var shopID: String?
    var shopName: String?
    var companyID: String?
    var warehouseId: String?
    var warehouseName: String?
    var items: [MMHCartItem]
    
    var available: Bool
    
    var activities: [CartItemActivity]?
    
//    func isAvailable() -> Bool {
//        return self.available
//    }
    
//    init(item: MMHCartItem) {
//        self.supplierType = item.supplierType
//        
//        let supplierID = item.supplierID
//        self.supplierID = supplierID
//
//        self.supplierName = item.shopName
//        
//        self.items = [MMHCartItem]()
//        self.items.append(item)
//        
//        self.available = item.available
//        
//        super.init()
//    }
    
    
    init(dictionary: [String: AnyObject], available: Bool) {
//        let shopID = dictionary["shopId"] as! MMHID?
        let shopID = dictionary.stringForKey("shopId")
        let companyID = dictionary.stringForKey("companyId")
        
        var supplierType = MMHSupplierType.Mamhao
        var supplierID: String = ""
        if shopID != nil {
            supplierType = .Shop
            supplierID = shopID!
            
            self.shopID = shopID
            self.companyID = companyID
        }
        else if companyID != nil  {
            supplierType = .Company
            supplierID = companyID!
            
            self.companyID = companyID
        }
        
        self.supplierType = supplierType
        self.supplierID = supplierID
        
        self.shopName = dictionary.stringForKey("shopName")
        self.warehouseId = dictionary.stringForKey("warehouseId")
        self.warehouseName = dictionary.stringForKey("warehouseName")
        
        if self.shopName != nil {
            self.supplierName = self.shopName!
        }
        else if self.warehouseName != nil {
            self.supplierName = self.warehouseName!
        }
        else {
            self.supplierName = "妈妈好"
        }
//        self.supplierName = dictionary.nonnullStringForKey("shopName")
        
        self.available = available
        
//        var areaID: MMHID? = nil
//        let originAreaID: AnyObject? = dictionary["areaId"]
//        if originAreaID != nil {
//            if let o = originAreaID as? MMHID {
//                areaID = o
//                self.areaID = areaID
//            }
//        }
        
        let itemDictionaries = dictionary["cartItems"] as! [[NSObject: AnyObject]]
        self.items = itemDictionaries.map({
            (itemDictionary: [NSObject : AnyObject]) -> MMHCartItem in
            let keymap = ["chosen": "selected"] as [NSObject: AnyObject]
            let item = MMHCartItem(JSONDict: itemDictionary, keyMap: keymap)
            item.available = available
            item.supplierType = supplierType
            if shopID != nil {
                item.shopId = shopID!
            }
            if companyID != nil {
                item.companyId = companyID!
            }
//            if areaID != nil {
//                item.areaId = areaID!
//            }
            item.warehouseId = dictionary.stringForKey("warehouseId")
            return item
        })
        
        self.available = available
        for item in self.items {
            item.available = available
        }
        
        if let activityDictionaries = dictionary["activity"] as? [[String: AnyObject]] {
            self.activities = activityDictionaries.map({
                (activityDictionary: [String : AnyObject]) -> CartItemActivity in
                let item = CartItemActivity(dictionary: activityDictionary)
                return item
            })
        }
        
        super.init()
    }
    
    
    init(group: CartItemGroup, items: [MMHCartItem]) {
        self.shopID = group.shopID
        self.companyID = group.companyID
        self.supplierType = group.supplierType
        self.supplierID = group.supplierID
        self.supplierName = group.supplierName
//        self.areaID = group.areaID
        
        self.items = items
        self.available = false
        
//        self.activities = group.activities
        
        super.init()
    }
    
    
    internal func unavailableGroup() -> CartItemGroup? {
        var unavailableItems = self.items.filter { (item: MMHCartItem) -> Bool in
            return !item.available
        }
        if unavailableItems.count == 0 {
            return nil
        }
        
        self.items = self.items.filter({ (item: MMHCartItem) -> Bool in
            return item.available
        })
        
        var group = CartItemGroup(group: self, items: unavailableItems)
        return group
    }


    internal func addItem(item: MMHCartItem) {
        if self.supplierID == item.supplierID {
            self.items.append(item)
        }
    }


    internal func removeItem(item: MMHCartItem) {
        let objcArray = self.items as NSArray
        let index = objcArray.indexOfObject(item)
        self.items.removeAtIndex(index)
    }
    
    
    internal func shouldContainItem(item: MMHCartItem) -> Bool {
        if self.supplierType != item.supplierType {
            return false
        }
        if self.supplierID != item.supplierID {
            return false
        }
        if self.available != item.available {
            return false
        }
        return true
    }
    
}


// for table view
extension CartItemGroup {
    internal func numberOfItems() -> Int {
        return self.items.count
    }
    
    internal func itemAtIndex(index: Int) -> MMHCartItem {
        let item = self.items[index]
        return item
    }

    internal func totalPrice() -> MMHPrice {
        var price: MMHPrice = 0
        for item in self.items {
            price += item.actualPrice()
        }
        if self.activities != nil {
            for activity in self.activities! {
                price -= activity.discountValue
            }
        }
        return price
    }
}
