//
//  MMHCartData.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartData.h"
#import "MMHCartItem.h"
#import "MamHao-Swift.h"
#import "UIImage+RenderedImage.h"
#import "MMHNetworkAdapter+Cart.h"
#import "MMHCartItemCell.h"


@interface MMHCartData ()

//@property (nonatomic, strong) NSMutableArray *groups;
@end


@implementation MMHCartData


- (void)setItems:(NSArray *)items
{
    _items = items;

//    [self groupOn];
}


//- (void)groupOn
//{
//    NSMutableArray *groups = [[NSMutableArray alloc] init];
//    for (MMHCartItem *item in self.items) {
//        CartItemGroup *group = [groups objectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
//            CartItemGroup *g = (CartItemGroup *)obj;
//            return [g shouldContainItem:item];
//        }];
//        if (group) {
//            [group addItem:item];
//        }
//        else {
//            CartItemGroup *newGroup = [[CartItemGroup alloc] initWithItem:item];
//            if (newGroup.available) {
//                NSInteger lastAvailabelGroupIndex = -1;
//                for (NSInteger i = 0; i < groups.count; i++) {
//                    CartItemGroup *g = groups[i];
//                    if (g.available) {
//                        lastAvailabelGroupIndex = i;
//                    }
//                    else {
//                        break;
//                    }
//                }
//                [groups insertObject:newGroup atIndex:lastAvailabelGroupIndex + 1];
//            }
//            else {
//                [groups addObject:newGroup];
//            }
//        }
//    }
//    self.groups = [groups mutableCopy];
//}


- (void)setActivityData:(CartActivityData *)activityData
{
    _activityData = activityData;
}


- (BOOL)isEmpty
{
    return self.groups.count == 0;
}


- (NSInteger)numberOfGroups
{
    return [self.groups count];
}


- (CartItemGroup *)groupAtIndex:(NSInteger)index
{
    return [self.groups nullableObjectAtIndex:index];
}


- (void)changeQuantityOfItem:(MMHCartItem *)cartItem to:(NSInteger)toValue completion:(void (^)(BOOL succeeded, NSError *error))completion
{
    if (![self hasItem:cartItem]) {
        NSError *error = [NSError errorWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"该商品不存在"}];
        completion(NO, error);
        return;
    }
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] changeQuantityOfItem:cartItem inCart:self to:toValue succeededHandler:^(MMHCartData *cartData){
        __strong __typeof(weakSelf) strongSelf = self;
        if ([strongSelf hasItem:cartItem]) {
            cartItem.quantity = toValue;
        }
        completion(YES, nil);
    } failedHandler:^(NSError *error) {
        completion(NO, error);
    }];
}


- (void)deleteItem:(MMHCartItem *)cartItem completion:(void (^)(BOOL succeeded, BOOL groupHasBeenDeletedAsWell, NSError *error))completion
{
    if (![self hasItem:cartItem]) {
        NSError *error = [NSError errorWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"该商品不存在"}];
        completion(NO, NO, error);
        return;
    }

    __weak __typeof(self) weakSelf = self;

    [[MMHNetworkAdapter sharedAdapter] deleteItems:@[cartItem] inCart:self succeededHandler:^(MMHCartData *cartData){
        __strong __typeof(weakSelf) strongSelf = self;
        CartItemGroup *group = [self.groups objectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
            CartItemGroup *aGroup = (CartItemGroup *)obj;
            return [aGroup.items containsObject:cartItem];
        }];

        if (!group) {
            completion(NO, NO, nil);
            return;
        }

        [group removeItem:cartItem];
        if (group.items.count == 0) {
            [strongSelf.groups removeObject:group];
            completion(YES, YES, nil);
        }
        else {
            completion(YES, NO, nil);
        }
    } failedHandler:^(NSError *error) {
        completion(NO, NO, error);
    }];
}


//- (void)changeChosenStatusOfItem:(MMHCartItem *)cartItem completion:(void (^)(BOOL succeeded, NSError *error))completion
//{
//    if (![self hasItem:cartItem]) {
//        NSError *error = [NSError errorWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"该商品不存在"}];
//        completion(NO, error);
//        return;
//    }
//    
//    [[MMHNetworkAdapter sharedAdapter] changeChosenStatusOfItem:cartItem inCart:self from:self succeededHandler:^{
//        cartItem.chosen = !cartItem.chosen;
//        completion(YES, nil);
//    } failedHandler:^(NSError *error) {
//        completion(NO, error);
//    }];
//}


//- (void)changeChooseAllStatusWithCompletion:(void (^)(BOOL succeeded, NSError *error))completion
//{
//    [[MMHNetworkAdapter sharedAdapter] changeChooseAllStatusInCart:self from:self succeededHandler:^(BOOL nowChosen){
//        [self chooseAll:nowChosen];
//        completion(YES, nil);
//    } failedHandler:^(NSError *error) {
//        completion(NO, error);
//    }];
//}


- (BOOL)hasItem:(MMHCartItem *)cartItem
{
    for (CartItemGroup *group in self.groups) {
        if ([group.items containsObject:cartItem]) {
            return YES;
        }
    }
    return NO;
}


- (BOOL)allItemChosen
{
    for (CartItemGroup *group in self.groups) {
        for (MMHCartItem *item in group.items) {
            if (item.available) {
                if (!item.chosen) {
                    return NO;
                }
            }
        }
    }
    return YES;
}


- (void)chooseAll:(BOOL)flag
{
    for (CartItemGroup *group in self.groups) {
        for (MMHCartItem *item in group.items) {
            if (item.available) {
                item.chosen = flag;
            }
        }
    }
}


- (void)configureGroupsWithDictionary:(NSDictionary *)dictionary
{
    NSArray *availableGroupDictionaries = dictionary[@"data"];
    NSArray *unavailableGroupDictionaries = dictionary[@"invalid"];
    NSMutableArray *availableGroups = [NSMutableArray array];
    NSMutableArray *unavailableGroups = [NSMutableArray array];
    if (availableGroups == unavailableGroups) {
        NSLog(@"FATAL: the two groups are eqal");
    }
    
    for (NSDictionary *groupDictionary in availableGroupDictionaries) {
        CartItemGroup *group = [[CartItemGroup alloc] initWithDictionary:groupDictionary available:YES];
        [availableGroups addObject:group];
    }
    
    for (NSDictionary *groupDictionary in unavailableGroupDictionaries) {
        CartItemGroup *group = [[CartItemGroup alloc] initWithDictionary:groupDictionary available:NO];
        [unavailableGroups addObject:group];
    }
    
    self.groups = availableGroups;
    [self.groups addObjectsFromArray:unavailableGroups];
}


- (void)configurePricesWithDictionary:(NSDictionary *)dictionary
{
    if ([[dictionary allKeys] containsObject:@"price"]) {
        NSDictionary *prices = dictionary[@"price"];
        self.originalPrice = [prices[@"originalPrice"] priceValue];
        self.savedPrice = [prices[@"reducePrice"] priceValue];
        self.totalPrice = [prices[@"totalPrice"] priceValue];
    }
    if (self.delegate) {
        [self.delegate cartDataPriceChanged:self];
    }
}


- (NSInteger)numberOfChosenItems
{
    NSInteger number = 0;
    for (CartItemGroup *group in self.groups) {
        if (group.available) {
            for (MMHCartItem *item in group.items) {
                if (item.chosen) {
                    number++;
                }
            }
        }
    }
    return number;
}


- (NSArray *)shopIDsAndCompanyIDs
{
    NSMutableArray *shopIDs = [NSMutableArray array];
    NSMutableArray *companyIDs = [NSMutableArray array];

    for (CartItemGroup *group in self.groups) {
        switch (group.supplierType) {
            case MMHSupplierTypeShop:
                [shopIDs addObject:group.supplierID];
                break;
            case MMHSupplierTypeCompany:
                [companyIDs addObject:group.supplierID];
                break;
            default:
                break;
        }
    }

    return @[shopIDs, companyIDs];
}


//- (void)configureActivitiesWithDictionary:(NSDictionary *)dictionary
//{
//    NSMutableArray *enabledActivities = [NSMutableArray array];
//
//    NSArray *shopDictionaries = dictionary[@"shop"];
//    for (NSDictionary *shopDictionary in shopDictionaries) {
//        MMHID shopID = [shopDictionary[@"id"] MMHIDValue];
//        for (CartItemGroup *group in self.groups) {
//            if (group.supplierType == MMHSupplierTypeShop) {
//                if (group.supplierID == shopID) {
//                    NSArray *activityDictionaries = shopDictionary[@"activity"];
//                    group.activities = [activityDictionaries transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
//                        CartItemActivity *activity = [[CartItemActivity alloc] initWithDictionary:originalObject supplierName:group.supplierName];
//                        if (activity.enabled) {
//                            NSLog(@"11111");
//                            [enabledActivities addObject:activity];
//                        }
//                        return activity;
//                    }];
//                }
//            }
//        }
//    }
//
//    NSArray *companyDictionaries = dictionary[@"company"];
//    for (NSDictionary *companyDictionary in companyDictionaries) {
//        MMHID companyID = [companyDictionary[@"id"] MMHIDValue];
//        for (CartItemGroup *group in self.groups) {
//            if (group.supplierType == MMHSupplierTypeCompany) {
//                if (group.supplierID == companyID) {
//                    NSArray *activityDictionaries = companyDictionary[@"activity"];
//                    group.activities = [activityDictionaries transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
//                        CartItemActivity *activity = [[CartItemActivity alloc] initWithDictionary:originalObject supplierName:group.supplierName];
//                        if (activity.enabled) {
//                            NSLog(@"22222");
//                            [enabledActivities addObject:activity];
//                        }
//                        return activity;
//                    }];
//                }
//            }
//        }
//    }
//
//    NSArray *mamhaoDictionaries = dictionary[@"mamhao"];
//    [mamhaoDictionaries transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
//        CartItemActivity *activity = [[CartItemActivity alloc] initWithDictionary:originalObject supplierName:@""];
//        if (activity.enabled) {
//            NSLog(@"33333");
//            [enabledActivities addObject:activity];
//        }
//        return activity;
//    }];
//    
//    self.enabledActivities = enabledActivities;
//}
@end


@implementation MMHCartData (CartID)


NSString *const MMHCartDataCartID = @"MMHCartDataCartID";


+ (void)setCartID:(MMHID)cartID
{
    [[NSUserDefaults standardUserDefaults] setObject:@(cartID) forKey:MMHCartDataCartID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (MMHID)cartID
{
    NSNumber *number = [[NSUserDefaults standardUserDefaults] objectForKey:MMHCartDataCartID];
    if (number == nil) {
        return 0;
    }
    
    return [number MMHIDValue];
}


@end
