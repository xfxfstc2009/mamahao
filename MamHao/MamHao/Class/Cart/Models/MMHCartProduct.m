//
//  MMHCartProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartProduct.h"
#import "MMHNetworkAdapter+Product.h"
#import "MMHCartItem.h"


@implementation MMHCartProduct


- (id)initWithCartItem:(MMHCartItem *)cartItem {
    self = [self init];
    if (self) {
        self.templateId = [NSString stringWithMMHID:cartItem.templateId];
        self.itemId = [NSString stringWithMMHID:cartItem.itemId];
        self.bindsShop = cartItem.isBindShop;
    }
    return self;
}


- (MMHProductModule)module
{
    return MMHProductModuleCart;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleCart) {
        return @{};
    }

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    dictionary[@"templateId"] = self.templateId;
    dictionary[@"itemId"] = self.itemId;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);

    dictionary[@"deviceId"] = [UIDevice deviceID];

    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"cart product parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
