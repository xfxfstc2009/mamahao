//
//  CartItemSpec.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class CartItemSpec: NSObject {
    
    let key: String
    let value: String
    
    
    init(dictionary: [String: AnyObject]) {
        if let key = dictionary["key"] as? String {
            self.key = key
        }
        else if let key = dictionary["key"] as? Int {
            self.key = String(key)
        }
        else {
            self.key = ""
        }
        self.value = dictionary["value"] as! String
        super.init()
    }
   
}
