//
//  MMHCartViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHCartData;
@class MMHSmallTipsLabel;
@class MMHTextLabel;
@class MMHSmallTextLabel;
@class MMHExpandableView;
@class MMHCartEnabledActivitiesView;


@interface MMHCartViewController : AbstractViewController

- (void)configureCartViews;
@end
