//
//  MMHCartItemCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHCartItem;
@class MMHSmallTextLabel;
@class MMHCartItemCell;
@class CartItemActivity;


@protocol MMHCartItemCellDelegate <NSObject>

- (BOOL)cartItemCell:(MMHCartItemCell *)cell shouldChangeItemCountFrom:(NSInteger)fromValue to:(NSInteger)toValue;
- (void)cartItemCellWillDeleteItem:(MMHCartItemCell *)cell;
- (void)cartItemCellWillChangeChosenStatusOfItem:(MMHCartItemCell *)cell;

@end


@interface MMHCartItemCellActivityView: UIView

- (instancetype)initWithFrame:(CGRect)frame activity:(CartItemActivity *)activity;
@end


@interface MMHCartItemCell : UITableViewCell

@property (nonatomic, weak) id<MMHCartItemCellDelegate> delegate;

@property (nonatomic, strong) MMHCartItem *item;
@property (nonatomic) BOOL chosen;

+ (CGFloat)heightForItem:(MMHCartItem *)cartItem;
@end
