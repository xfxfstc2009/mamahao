//
//  MMHCartItemCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartItemCell.h"
#import "MMHCartItem.h"
#import "MMHAppearance.h"
#import "MamHao-Swift.h"
#import "UIView+Extension.h"


@interface MMHCartItemCellActivityView ()

@property (nonatomic, strong) CartItemActivity *activity;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) MMHSmallTipsLabel *introductionLabel;
@end


@implementation MMHCartItemCellActivityView


- (instancetype)initWithFrame:(CGRect)frame activity:(CartItemActivity *)activity
{
    self = [super initWithFrame:frame];
    if (self) {
        self.activity = activity;
        [self configureViews];
    }
    return self;
}


- (void)configureViews
{
    UILabel *typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, self.bounds.size.height)];
    typeLabel.backgroundColor = [UIColor colorWithHexString:@"666666"];
    typeLabel.textAlignment = NSTextAlignmentCenter;
    typeLabel.font = [UIFont systemFontOfSize:14.0f];
    typeLabel.textColor = [UIColor whiteColor];
    typeLabel.text = self.activity.title;
    [self addSubview:typeLabel];
    self.typeLabel = typeLabel;

    // TODO: - Louis - cart, to be removed
//    MMHSmallTipsLabel *introductionLabel = [[MMHSmallTipsLabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, self.bounds.size.height)];
//    [introductionLabel attachToRightSideOfView:typeLabel byDistance:8.0f];
//    [introductionLabel setMaxX:CGRectGetMaxX(self.bounds)];
//    introductionLabel.text = self.activity.introduction;
//    [self addSubview:introductionLabel];
//    self.introductionLabel = introductionLabel;
}


@end


@interface MMHCartItemCell () <StepperDelegate, MMHCartItemDelegate>

@property (nonatomic, strong) UIButton *chooseButton;
@property (nonatomic, strong) MMHImageView *portraitImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) MMHSmallTextLabel *priceLabel;
@property (nonatomic, strong) UILabel *specLabel;
@property (nonatomic, strong) Stepper *quantityStepper;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) UIView *topLine;
@property (nonatomic, strong) UIView *unavailableCoverView;
@property (nonatomic, strong) UIImageView *unavailableImageView;
//@property (nonatomic, strong) NSMutableArray *activityViews;
@end


@implementation MMHCartItemCell


- (void)setChosen:(BOOL)chosen
{
    _chosen = chosen;

    if (chosen) {
        [self.chooseButton setImage:[UIImage imageNamed:@"cart_btn_select_s"] forState:UIControlStateNormal];
    }
    else {
        [self.chooseButton setImage:[UIImage imageNamed:@"cart_btn_select_n"] forState:UIControlStateNormal];
    }
}


+ (CGFloat)heightForItem:(MMHCartItem *)cartItem
{
    CGFloat height = 105.0f;
    if (cartItem.activities.count != 0) {
        height += ((CGFloat)(cartItem.activities.count) * (15.0f + 8.0f));
    }

    return height;
}


- (void)setItem:(MMHCartItem *)item
{
    _item = item;

    item.delegate = self;
    [self updateViews];
}


- (void)updateViews
{
    if (self.topLine == nil) {
        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, mmh_screen_width() - 20.0f, mmh_pixel())];
        topLine.backgroundColor = [MMHAppearance separatorColor];
        [self.contentView addSubview:topLine];
        self.topLine = topLine;
    }

    if (self.chooseButton == nil) {
        UIButton *chooseButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 35.0f, 45.0f, 40.0f)];
        [chooseButton addTarget:self action:@selector(choose:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:chooseButton];
        self.chooseButton = chooseButton;
    }
    self.chosen = self.item.chosen;

    if (self.portraitImageView == nil) {
        MMHImageView *portraitImageView = [[MMHImageView alloc] initWithFrame:CGRectMake(45.0f, 15.0f, 75.0f, 75.0f)];
        [portraitImageView setBorderColor:[MMHAppearance separatorColor] cornerRadius:2.0f];
        [self.contentView addSubview:portraitImageView];
        self.portraitImageView = portraitImageView;
    }
    [self.portraitImageView updateViewWithImageAtURL:self.item.itemPic];
    
    if (self.nameLabel == nil) {
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 15.0f, 0.0f, 0.0f)];
        [nameLabel attachToRightSideOfView:self.portraitImageView byDistance:7.0f];
        nameLabel.textColor = [MMHAppearance blackColor];
        nameLabel.font = [MMHAppearance tipsFont];
        [self.contentView addSubview:nameLabel];
        self.nameLabel = nameLabel;
    }
//    update it below

    if (self.priceLabel == nil) {
        MMHSmallTextLabel *priceLabel = [[MMHSmallTextLabel alloc] initWithFrame:CGRectMake(0.0f, 10.0f, 0.0f, 0.0f)];
        priceLabel.textColor = [MMHAppearance blackColor];
        [priceLabel moveToRight:mmh_screen_width() - 10.0f];
        priceLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:priceLabel];
        self.priceLabel = priceLabel;
    }
//    update it below

    if (self.specLabel == nil) {
        UILabel *specLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, 50.0f, 0.0f, 0.0f)];
        specLabel.textColor = [MMHAppearance lightGrayColor];
        specLabel.font = [MMHAppearance tipsFont];
        [self.contentView addSubview:specLabel];
        self.specLabel = specLabel;
    }

    if (self.quantityStepper == nil) {
        Stepper *quantityStepper = [[Stepper alloc] initWithFrame:CGRectMake(self.nameLabel.frame.origin.x, 65.0f, 90.0f, 25.0f)
                                                     minimumValue:1
                                                     maximumValue:NSIntegerMax
                                                            value:self.item.quantity];
        [quantityStepper setDecreaseButtonImage:[UIImage imageNamed:@"cart_icon_shopping_reduce_n"] forState:UIControlStateNormal];
        [quantityStepper setDecreaseButtonImage:[UIImage imageNamed:@"cart_icon_shopping_reduce_d"] forState:UIControlStateDisabled];
        [quantityStepper setIncreaseButtonImage:[UIImage imageNamed:@"cart_icon_shopping_add_n"] forState:UIControlStateNormal];
//        quantityStepper.layer.cornerRadius = 6.0f;
        quantityStepper.delegate = self;
        [self.contentView addSubview:quantityStepper];
        self.quantityStepper = quantityStepper;
    }

    if (self.deleteButton == nil) {
        UIButton *deleteButton = [[UIButton alloc] init];
//        [deleteButton setWidth:60.0f];
//        [deleteButton setHeight:25.0f];
        [deleteButton setImage:[UIImage imageNamed:@"cart_icon_shopping_delete"] forState:UIControlStateNormal];
        [deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        deleteButton.titleLabel.font = [MMHAppearance smallTextFont];
        [deleteButton setTitleColor:[MMHAppearance grayColor] forState:UIControlStateNormal];
        [deleteButton sizeToFit];
        deleteButton.height = 35.0f;
        deleteButton.centerY = self.quantityStepper.centerY;
        [deleteButton moveToRight:mmh_screen_width() - 10.0f];
        [deleteButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
//        deleteButton.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:deleteButton];
        self.deleteButton = deleteButton;
    }

    NSString *price = [NSString stringWithFormat:@"￥%.2f", self.item.itemPrice];
    [self.priceLabel setSingleLineText:price constrainedToWidth:CGFLOAT_MAX];

    [self.nameLabel setMaxX:self.priceLabel.left - 10.0f];
    [self.nameLabel setText:self.item.itemName constrainedToLineCount:2];
    self.quantityStepper.value = self.item.quantity;
//    [self.nameLabel setText:@"打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车打吊车" constrainedToLineCount:2];
    [self.specLabel setSingleLineText:[self.item specDisplayName] constrainedToWidth:self.specLabel.width];
    self.specLabel.centerY = 55.0f;

//    if ([self.item.discounts count] != 0) {
//        
//    }

//    [self updateActivitiesViews];
    
//    CGFloat bottom = self.quantityStepper.bottom;
//    if ([self.activityViews count]) {
//        MMHCartItemCellActivityView *activityView = [self.activityViews lastObject];
//        bottom = activityView.bottom;
//    }
//    self.topLine.top = bottom + 9.5f;

    [self updateAvailableViews];
}


//- (void)updateActivitiesViews
//{
//    for (MMHCartItemCellActivityView *activityView in self.activityViews) {
//        [activityView removeFromSuperview];
//    }
//    self.activityViews = [NSMutableArray array];
//
//    if ([self.item.activities count] == 0) {
//        return;
//    }
//
//    for (NSInteger i = 0; i < self.item.activities.count; i++) {
//        CartItemActivity *activity = self.item.activities[i];
//        MMHCartItemCellActivityView *activityView = [[MMHCartItemCellActivityView alloc] initWithFrame:CGRectMake(self.portraitImageView.frame.origin.x, 0.0f, (CGRectGetMinX(self.deleteButton.frame) - CGRectGetMinX(self.portraitImageView.frame)), 15.0f) activity:activity];
//        [activityView setY:CGRectGetMaxY(self.quantityStepper.frame) + 8.0f + (15.0f + 8.0f) * (CGFloat)(i)];
//        [self.contentView addSubview:activityView];
//        [self.activityViews addObject:activityView];
//    }
//}


- (void)updateAvailableViews
{
    if (self.unavailableImageView) {
        [self.unavailableImageView removeFromSuperview];
        self.unavailableImageView = nil;
    }
    if (self.unavailableCoverView) {
        [self.unavailableCoverView removeFromSuperview];
        self.unavailableCoverView = nil;
    }
    if (!self.item.available) {
        if (self.unavailableCoverView == nil) {
            UIView *unavailableCoverView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), self.portraitImageView.bottom + 15.0f)];
            unavailableCoverView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5f];
//            unavailableCoverView.backgroundColor = [UIColor orangeColor];
            [self.contentView addSubview:unavailableCoverView];
            self.unavailableCoverView = unavailableCoverView;
        }

        if (self.unavailableImageView == nil) {
            UIImageView *unavailableImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 5.0f, 60.0f, 60.0f)];
            [unavailableImageView moveToRight:CGRectGetMaxX(self.unavailableCoverView.bounds) - 12.0f];
            unavailableImageView.image = [UIImage imageNamed:@"cart_img_shopping_fail"];
            [self.unavailableCoverView addSubview:unavailableImageView];
            self.unavailableImageView = unavailableImageView;
        }

        [self.deleteButton bringToFront];
    }
}


- (void)choose:(UIButton *)chooseButton
{
//    BOOL alreadyChosen = self.item.chosen;
//    BOOL willChoose = !alreadyChosen;
//
//    self.item.chosen = !alreadyChosen;
//    self.chosen = !alreadyChosen;

    if (self.delegate) {
        [self.delegate cartItemCellWillChangeChosenStatusOfItem:self];
    }
}


- (void)delete:(UIButton *)deleteButton
{
    if (self.delegate) {
        [self.delegate cartItemCellWillDeleteItem:self];
    }
}


#pragma mark - Stepper delegate


- (BOOL)stepper:(Stepper * __nonnull)stepper shouldChangeValueFrom:(NSInteger)currentValue to:(NSInteger)newValue
{
    if (self.delegate) {
        return [self.delegate cartItemCell:self shouldChangeItemCountFrom:currentValue to:newValue];
    }

    return NO;
}


#pragma mark - Cart item delegate


- (void)itemQuantityChanged:(MMHCartItem *)item
{
    self.quantityStepper.value = item.quantity;
}


- (void)item:(MMHCartItem *)item chosen:(BOOL)chosen
{
    self.chosen = chosen;
}


@end
