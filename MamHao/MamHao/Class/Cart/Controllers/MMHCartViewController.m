//
//  MMHCartViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCartViewController.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Cart.h"
#import "MMHCartData.h"
#import "MMHCartItem.h"
#import "MMHAppearance.h"
#import "MMHCartItemCell.h"
#import "MMHExpandableView.h"
#import "MMHCartEnabledActivitiesView.h"
#import "MMHAssistant.h"
#import "MMHAccountSession.h"
#import "MMHConfirmationOrderViewController.h"
#import "MamHao-Swift.h"
#import "MMHCartGroupHeaderView.h"
#import "MMHCartGroupFooterView.h"
#import "MMHCartProduct.h"
#import "MMHProductDetailViewController1.h"


#define MMHCartViewControllerShowsActivitiesInGroupHeader 0


NSString * const MMHCartItemCellIdentifier = @"MMHCartItemCellIdentifier";
NSString *const MMHCartGroupHeaderViewIdentifier = @"MMHCartGroupHeaderViewIdentifier";
NSString *const MMHCartGroupFooterViewIdentifier = @"MMHCartGroupFooterViewIdentifier";



@interface MMHCartViewController () <UITableViewDataSource, UITableViewDelegate, MMHCartItemCellDelegate, MMHCartDataDelegate, MMHExpandableViewDelegate>

@property (nonatomic, strong) UILabel *failedTipsLabel;
@property (nonatomic, strong) UITableView *cartTableView;
@property (nonatomic, strong) MMHCartData *cartData;
@property (nonatomic, strong) UIView *confirmView;
@property (nonatomic, strong) UIButton *chooseAllButton;
@property (nonatomic, strong) MMHSmallTipsLabel *totalPriceTipsLabel;
@property (nonatomic, strong) MMHTextLabel *totalPriceLabel;
@property (nonatomic, strong) MMHSmallTipsLabel *discountPriceTipsLabel;
@property (nonatomic, strong) MMHSmallTextLabel *discountPriceLabel;
@property (nonatomic, strong) UIButton *confirmButton;
//@property (nonatomic, strong) MMHExpandableView *promotionView;
//@property (nonatomic, strong) MMHCartEnabledActivitiesView *enabledActivitiesView;
@property (nonatomic, strong) UIView *tableHeaderView;
@end


@implementation MMHCartViewController


- (void)setCartData:(MMHCartData *)cartData
{
    _cartData = cartData;

    cartData.delegate = self;
    [self configureCartViews];
}

-(void)dealloc{
    NSLog(@"释放");
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"购物车";
        self.tabBarItem.image = [UIImage imageNamed:@"mmhtab_icon_shop_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"mmhtab_icon_shop_hlt"];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

//    [self configureBlankViews];
}


//- (void)configureBlankViews
//{
//
//}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self fetchData];
}


- (void)fetchData
{
//    self.cartData = nil;
    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchCartDataFrom:self
                                        succeededHandler:^(MMHCartData *cartData) {
                                            [weakSelf.view hideProcessingView];
                                            self.cartData = cartData;
                                        } failedHandler:^(NSError *error) {
                                            [weakSelf.view hideProcessingView];
                                            [weakSelf.view showTipsWithError:error];
                                            [self configureFailedViews];
            }];
}


- (void)configureCartViews
{
    if (self.failedTipsLabel) {
        [self.failedTipsLabel removeFromSuperview];
        self.failedTipsLabel = nil;
    }
    
    if ([self.cartData isEmpty]) {
//        [self.view removeAllSubviews];
//        self.cartTableView = nil;
//        self.promotionView = nil;
//        self.confirmView = nil;
        [self showBlankViewOfType:MMHBlankViewTypeCart belowView:nil];
        return;
    }
    else {
        [self hideBlankView];
    }

    if (self.cartTableView == nil) {
        UITableView *cartTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        cartTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        cartTableView.dataSource = self;
        cartTableView.delegate = self;
        cartTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [cartTableView registerClass:[MMHCartItemCell class] forCellReuseIdentifier:MMHCartItemCellIdentifier];
//        [cartTableView registerClass:[MMHCartGroupHeaderView class] forHeaderFooterViewReuseIdentifier:MMHCartGroupHeaderViewIdentifier];
        [cartTableView registerClass:[MMHCartGroupFooterView class] forHeaderFooterViewReuseIdentifier:MMHCartGroupFooterViewIdentifier];
        self.cartTableView = cartTableView;
        [self.view addSubview:self.cartTableView];
        self.cartTableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 60.0f, 0.0f);
    }

//    if (self.promotionView == nil) {
//        MMHExpandableView *promotionView = [[MMHExpandableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 45.0f)
//        summaryText:@"blablablabla blablablabla"
//        fullText:@" blablablabla0 blablablabla1 blablablabla2 blablablabla3 blablablabla4 blablablabla5 blablablabla6"];
//        promotionView.textColor = [MMHAppearance pinkColor];
//        promotionView.font = [MMHAppearance smallTextFont];
//        promotionView.delegate = self;
//        self.promotionView = promotionView;
//    }

//    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 0.0f)];
//    headerView.height = self.promotionView.frame.size.height;// + [MMHCartEnabledActivitiesView heightForCart:self.cartData];
//    [headerView addSubview:self.promotionView];
//    headerView.backgroundColor = [UIColor whiteColor];
//    self.cartTableView.tableHeaderView = headerView;
//    self.tableHeaderView = headerView;
    
//    if (self.enabledActivitiesView == nil) {
//        MMHCartEnabledActivitiesView *enabledActivitiesView = [[MMHCartEnabledActivitiesView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 0.0f)];
//        enabledActivitiesView.activities = self.cartData.enabledActivities;
//        enabledActivitiesView.top = MAX(self.promotionView.height - self.cartTableView.contentOffset.y, 0.0f);
//        [self.view addSubview:enabledActivitiesView];
//        self.enabledActivitiesView = enabledActivitiesView;
//    }
    
    if (self.confirmView == nil) {
        UIView *confirmView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, 60.0f)];
        [confirmView moveToBottom:CGRectGetMaxY(self.view.bounds)];
        confirmView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        confirmView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:confirmView];
        self.confirmView = confirmView;

        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, confirmView.bounds.size.width, 0.5f)];
        topLine.backgroundColor = [UIColor colorWithHexString:@"dcdcdc"];
        [self.confirmView addSubview:topLine];

        UIButton *chooseAllButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 35.0f, 45.0f, 40.0f)];
        chooseAllButton.centerY = CGRectGetMidY(self.confirmView.bounds);
        [chooseAllButton addTarget:self action:@selector(chooseAll:) forControlEvents:UIControlEventTouchUpInside];
        [self.confirmView addSubview:chooseAllButton];
        self.chooseAllButton = chooseAllButton;

        MMHSmallTextLabel *chooseAllLabel = [[MMHSmallTextLabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)];
        [chooseAllLabel attachToRightSideOfView:chooseAllButton byDistance:0.0f];
        [chooseAllLabel setSingleLineText:@"全选" constrainedToWidth:CGFLOAT_MAX];
        chooseAllLabel.centerY = CGRectGetMidY(self.confirmView.bounds);
        [self.confirmView addSubview:chooseAllLabel];

        MMHSmallTipsLabel *totalPriceTipsLabel = [[MMHSmallTipsLabel alloc] initWithFrame:CGRectMake(0.0f, 14.0f, 100.0f, 0.0f)];
        totalPriceTipsLabel.textAlignment = NSTextAlignmentRight;
        [totalPriceTipsLabel setText:@"合计：" constrainedToLineCount:1];
        [self.confirmView addSubview:totalPriceTipsLabel];
        self.totalPriceTipsLabel = totalPriceTipsLabel;

        MMHTextLabel *totalPriceLabel = [[MMHTextLabel alloc] initWithFrame:CGRectMake(0.0f, 14.0f, 0.0f, 0.0f)];
        if ([UIScreen currentScreenMode] == LESScreenModeIPhone5Series || [UIScreen currentScreenMode] == LESScreenModeIPhone4SOrEarlier) {
            [totalPriceLabel setMaxX:CGRectGetMaxX(self.confirmView.bounds) - 100.0f];
        }
        else {
            [totalPriceLabel setMaxX:CGRectGetMaxX(self.confirmView.bounds) - 150.0f];
        }
        totalPriceLabel.textAlignment = NSTextAlignmentRight;
        totalPriceLabel.textColor = [MMHAppearance redColor];
        [self.confirmView addSubview:totalPriceLabel];
        self.totalPriceLabel = totalPriceLabel;

        MMHSmallTipsLabel *discountPriceTipsLabel = [[MMHSmallTipsLabel alloc] initWithFrame:CGRectMake(0.0f, 34.0f, 100.0f, 0.0f)];
        discountPriceTipsLabel.textAlignment = NSTextAlignmentRight;
        [discountPriceTipsLabel setText:@"为您节省：" constrainedToLineCount:1];
        [self.confirmView addSubview:discountPriceTipsLabel];
        self.discountPriceTipsLabel = discountPriceTipsLabel;

        MMHSmallTextLabel *discountPriceLabel = [[MMHSmallTextLabel alloc] initWithFrame:CGRectMake(0.0f, 34.0f, 0.0f, 0.0f)];
        if ([UIScreen currentScreenMode] == LESScreenModeIPhone5Series || [UIScreen currentScreenMode] == LESScreenModeIPhone4SOrEarlier) {
            [discountPriceLabel setMaxX:CGRectGetMaxX(self.confirmView.bounds) - 100.0f];
        }
        else {
            [discountPriceLabel setMaxX:CGRectGetMaxX(self.confirmView.bounds) - 150.0f];
        }
        discountPriceLabel.textAlignment = NSTextAlignmentRight;
        [self.confirmView addSubview:discountPriceLabel];
        self.discountPriceLabel = discountPriceLabel;

        UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 125.0f, 40.0f)];
        if ([UIScreen currentScreenMode] == LESScreenModeIPhone5Series || [UIScreen currentScreenMode] == LESScreenModeIPhone4SOrEarlier) {
            confirmButton.width = 80.0f;
        }
        confirmButton.right = CGRectGetMaxX(self.confirmView.bounds) - 10.0f;
        confirmButton.centerY = CGRectGetMidY(self.confirmView.bounds);
        [confirmButton setBackgroundImage:[UIImage patternImageWithColor:[MMHAppearance pinkColor]] forState:UIControlStateNormal];
        [confirmButton setBackgroundImage:[UIImage patternImageWithColor:[MMHAppearance disableColor]] forState:UIControlStateDisabled];
        confirmButton.layer.cornerRadius = 2.0f;
        [confirmButton addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
        [self.confirmView addSubview:confirmButton];
        self.confirmButton = confirmButton;
    }
    
    [self updateViews];
}


- (void)updateViews
{
    [self.cartTableView reloadData];
    [self updateChooseAllButtonAndConfirmButtonStatus];
    [self updatePriceLabels];
}


- (void)updateChooseAllButtonAndConfirmButtonStatus
{
    if ([self.cartData allItemChosen]) {
        [self.chooseAllButton setImage:[UIImage imageNamed:@"cart_btn_select_s"] forState:UIControlStateNormal];
    }
    else {
        [self.chooseAllButton setImage:[UIImage imageNamed:@"cart_btn_select_n"] forState:UIControlStateNormal];
    }

    NSInteger numberOfChosenItems = [self.cartData numberOfChosenItems];
    [self.confirmButton setTitle:[NSString stringWithFormat:@"结算(%ld)", (long)numberOfChosenItems] forState:UIControlStateNormal];
    self.confirmButton.enabled = (numberOfChosenItems != 0);
}


- (void)updatePriceLabels
{
    MMHPrice totalPrice = self.cartData.totalPrice;
    MMHPrice discountPrice = self.cartData.savedPrice;
    [self.totalPriceLabel setSingleLineText:[NSString stringWithPrice:totalPrice] constrainedToWidth:CGFLOAT_MAX];
    self.totalPriceTipsLabel.right = self.totalPriceLabel.left;
    [self.discountPriceLabel setSingleLineText:[NSString stringWithPrice:discountPrice] constrainedToWidth:CGFLOAT_MAX];
    self.discountPriceTipsLabel.right = self.discountPriceLabel.left;
}


- (void)configureFailedViews
{
    // remove cart views
    if (self.cartTableView != nil) {
        [self.cartTableView removeFromSuperview];
        self.cartTableView = nil;
    }
    if (self.confirmView != nil) {
        [self.confirmView removeFromSuperview];
        self.confirmView = nil;
    }
//    if (self.failedTipsLabel == nil) {
//        UILabel *failedTipsLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
//        failedTipsLabel.backgroundColor = [UIColor whiteColor];
//        failedTipsLabel.textAlignment = NSTextAlignmentCenter;
//        failedTipsLabel.userInteractionEnabled = YES;
//        failedTipsLabel.text = @"网络在沉睡，先唤醒再试试~";
//        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(failedTipsLabelTapped:)];
//        [failedTipsLabel addGestureRecognizer:tapGestureRecognizer];
//        self.failedTipsLabel = failedTipsLabel;
//    }
//    [self.view addSubview:self.failedTipsLabel];
}


- (void)failedTipsLabelTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self fetchData];
}


#pragma mark - Table view data source and delegate methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.cartData == nil) {
        return 0;
    }
    return [self.cartData numberOfGroups];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CartItemGroup *group = [self.cartData groupAtIndex:section];
    return [group numberOfItems];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    CGFloat height = 40.0f;
#if MMHCartViewControllerShowsActivitiesInGroupHeader
    CartItemGroup *group = [self.cartData groupAtIndex:section];
    if ([group.activities count] != 0) {
        height += [group.activities count] * 20.0f;
    }
#endif
    
    return height;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CartItemGroup *group = [self.cartData groupAtIndex:section];

    
//    MMHCartGroupHeaderView *headerView = (MMHCartGroupHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:MMHCartGroupHeaderViewIdentifier];
//    headerView.group = group;


    CGFloat height = 40.0f;
#if MMHCartViewControllerShowsActivitiesInGroupHeader
    if ([group.activities count] != 0) {
        height += [group.activities count] * 20.0f;
    }
#endif
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), height)];
    headerView.backgroundColor = [UIColor whiteColor];

    UIView *separatorView = [[UIView alloc] initWithFrame:headerView.bounds];
    separatorView.height = 4.0f;
    separatorView.backgroundColor = [MMHAppearance backgroundColor];
    [headerView addSubview:separatorView];

    UILabel *titleLabel = [[MMHSmallTextLabel alloc] initWithFrame:CGRectMake(10.0f, CGRectGetMaxY(separatorView.frame) + 10.0f, headerView.bounds.size.width - 13.0f, 40.0f)];
//    [titleLabel setMaxY:CGRectGetMaxY(headerView.bounds)];
    [headerView addSubview:titleLabel];
    titleLabel.text = group.supplierName;
    [titleLabel sizeToFit];

//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 40.0f, mmh_screen_width() - 20.0f, 1.0f / mmh_screen_scale())];
//    line.backgroundColor = [MMHAppearance separatorColor];
//    [headerView addSubview:line];

//    CGFloat y = line.bottom + 5.0f;
//    for (CartItemActivity *activity in group.activities) {
//        MMHCartItemCellActivityView *activityView = [[MMHCartItemCellActivityView alloc] initWithFrame:CGRectMake(13.0f, y, mmh_screen_width(), 15.0f) activity:activity];
//        [headerView addSubview:activityView];
//        y += 20.0f;
//    }
    
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CartItemGroup *group = [self.cartData groupAtIndex:section];
    return [MMHCartGroupFooterView heightForGroup:group];
    #if 0
    return 32.0f;
    #endif
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    MMHCartGroupFooterView *footerView = (MMHCartGroupFooterView *) [tableView dequeueReusableHeaderFooterViewWithIdentifier:MMHCartGroupFooterViewIdentifier];
    CartItemGroup *group = [self.cartData groupAtIndex:section];
    footerView.group = group;
    return footerView;
        #if 0
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 32.0f)];
    footerView.backgroundColor = [UIColor whiteColor];

    MMHTextLabel *titleLabel = [[MMHTextLabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, mmh_screen_width() - 20.0f, 32.0f)];
    titleLabel.textAlignment = NSTextAlignmentRight;
    [footerView addSubview:titleLabel];

    CartItemGroup *group = [self.cartData groupAtIndex:section];
    titleLabel.text = [NSString stringWithFormat:@"应付：￥%.2f", group.totalPrice];
//    [titleLabel sizeToFit];
    titleLabel.centerY = CGRectGetMidY(footerView.bounds);

    return footerView;
    #endif
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MMHCartItemCell *cell = [tableView dequeueReusableCellWithIdentifier:MMHCartItemCellIdentifier];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;

    CartItemGroup *group = [self.cartData groupAtIndex:indexPath.section];
    MMHCartItem *item = [group itemAtIndex:indexPath.row];
    cell.item = item;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartItemGroup *group = [self.cartData groupAtIndex:indexPath.section];
    MMHCartItem *item = [group itemAtIndex:indexPath.row];
    return [MMHCartItemCell heightForItem:item];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MMHCartItemCell *cell = (MMHCartItemCell *)[tableView cellForRowAtIndexPath:indexPath];
    MMHCartItem *cartItem = cell.item;
    MMHCartProduct *product = [[MMHCartProduct alloc] initWithCartItem:cartItem];
    MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
    [self pushViewController:productDetailViewController];
}


//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    self.enabledActivitiesView.top = MAX(self.promotionView.height - self.cartTableView.contentOffset.y, 0.0f);
//}


#pragma mark - MMHCartItemCell delegate


- (BOOL)cartItemCell:(MMHCartItemCell *)cell shouldChangeItemCountFrom:(NSInteger)fromValue to:(NSInteger)toValue
{
    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] changeQuantityOfItem:cell.item inCart:self.cartData to:toValue succeededHandler:^(MMHCartData *cartData) {
        [weakSelf.view hideProcessingView];
        weakSelf.cartData = cartData;
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        [weakSelf.view showTipsWithError:error];
    }];
//    [self.cartData changeQuantityOfItem:cell.item to:toValue completion:^(BOOL succeeded, NSError *error) {
//        __strong __typeof(weakSelf) strongSelf = self;
//        [strongSelf.view hideProcessingView];
//        if (!succeeded) {
//            if (error) {
//                // TODO: - Louis - alert
//            }
//        }
//    }];
    return NO;
}


- (void)cartItemCellWillDeleteItem:(MMHCartItemCell *)cell
{
    __weak __typeof(self) weakSelf = self;

    LESAlertView *alertView = [[LESAlertView alloc] initWithTitle:nil
                                                          message:@"确认删除这个商品吗？"
                                                cancelButtonTitle:@"取消"
                                              cancelButtonHandler:^{

                                              }];
    [alertView addButtonWithTitle:@"删除"
                          handler:^{
                              [weakSelf.view showProcessingView];
                              [[MMHNetworkAdapter sharedAdapter] deleteItems:@[cell.item]
                                                                      inCart:self.cartData
                                                            succeededHandler:^(MMHCartData *cartData) {
                                                                [weakSelf.view hideProcessingView];
                                                                self.cartData = cartData;
                                                                [MMHLogbook logEventType:MMHBuriedPointTypeShoppingCartDelete];
                                                            } failedHandler:^(NSError *error) {
                                          [weakSelf.view hideProcessingView];
                                          [weakSelf.view showTipsWithError:error];
                                      }];
                          }];
    [alertView show];
}


- (void)cartItemCellWillChangeChosenStatusOfItem:(MMHCartItemCell *)cell
{
    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] changeChosenStatusOfItem:cell.item
                                                         inCart:self.cartData
                                                           from:self
                                               succeededHandler:^(MMHCartData *cartData){
                                                   [weakSelf.view hideProcessingView];
                                                   self.cartData = cartData;
                                               } failedHandler:^(NSError *error) {
                                                   [weakSelf.view hideProcessingView];
                                                   [weakSelf.view showTipsWithError:error];
                                               }];
}


- (void)chooseAll:(UIButton *)chooseAllButton
{
    [MMHLogbook logEventType:MMHBuriedPointTypeShoppingCartAllClick];
    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] changeChooseAllStatusInCart:self.cartData
                                                              from:self
                                               succeededHandler:^(MMHCartData *cartData){
                                                   [weakSelf.view hideProcessingView];
                                                   self.cartData = cartData;
                                               } failedHandler:^(NSError *error) {
                                                   [weakSelf.view hideProcessingView];
                                                   [weakSelf.view showTipsWithError:error];
                                               }];
}


- (void)confirm:(UIButton *)confirmButton
{
    if ([[MMHAccountSession currentSession] alreadyLoggedIn]) {
        [self confirmOrder];
    }
    else {
        __weak typeof(self) weakSelf = self;
        [self presentLoginViewControllerWithSucceededHandler:^{
            [weakSelf confirmOrder];
        }];
    }
}


- (void)confirmOrder
{
    [MMHLogbook logEventType:MMHBuriedPointTypeShoppingCartCalculation];
    MMHConfirmationOrderViewController *confirmationOrderViewController = [[MMHConfirmationOrderViewController alloc] initWithCartData:self.cartData];
    [self pushViewController:confirmationOrderViewController];
}


#pragma mark - MMHCartData delegate


- (void)cartDataPriceChanged:(MMHCartData *)cartData
{
    [self updatePriceLabels];
}


#pragma mark - MMHExpandableView delegate


- (void)expandableViewHeightDidChange:(MMHExpandableView *)expandableView
{
    [self.tableHeaderView setHeight:expandableView.height];// + self.enabledActivitiesView.height];
    self.cartTableView.tableHeaderView = self.tableHeaderView;
}


- (void)blankViewButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:NO];
    self.tabBarController.selectedIndex = 0;
}


@end
