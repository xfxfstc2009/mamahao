//
//  MMHPOIViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPOIViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>

#define GeoPlaceHolder @"名称"

@interface MMHPOIViewController()<UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDelegate,UITableViewDataSource,AMapSearchDelegate>
@property (nonatomic, strong) AMapSearchAPI *search;

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *displayController;

@property (nonatomic, strong) NSMutableArray *tips;
@end
@implementation MMHPOIViewController
@synthesize tips = _tips;
@synthesize searchBar = _searchBar;
@synthesize displayController = _displayController;

#pragma mark - Initialization

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initSearchBar];
    [self initSearchDisplay];
}

- (void)initSearchBar {
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
    self.searchBar.barStyle     = UIBarStyleBlack;
    self.searchBar.translucent  = YES;
    self.searchBar.delegate     = self;
    self.searchBar.placeholder  = GeoPlaceHolder;
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    
    [self.view addSubview:self.searchBar];
}

- (void)initSearchDisplay {
    self.displayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.displayController.delegate                = self;
    self.displayController.searchResultsDataSource = self;
    self.displayController.searchResultsDelegate   = self;
}

#pragma mark - Life Cycle

- (id)init {
    if (self = [super init]){
        self.tips = [NSMutableArray array];
    }
    return self;
}


#pragma mark - createTableView
#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.tips.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: cellIdentifyWithRowOne];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    AMapTip *tip = [self.tips objectAtIndex:indexPath.row];
    cell.textLabel.text = tip.name;
    cell.detailTextLabel.text = tip.district;
    
    return cell;
}


#pragma mark - UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSString *key = searchBar.text;
    [self searchGeocodeWithKey:key adcode:nil];
    [self.displayController setActive:NO animated:YES];
    self.searchBar.placeholder = key;
}

-(void)searchGeocodeWithKey:(NSString *)key adcode:(NSString *)adcode{
    [self searchGeocodeWithKey1:key adcode:adcode];
}

- (void)searchGeocodeWithKey1:(NSString *)key adcode:(NSString *)adcode{
    if (key.length == 0){
        return;
    }
    AMapGeocodeSearchRequest *geo = [[AMapGeocodeSearchRequest alloc] init];
    geo.address = key;
    
    if (adcode.length > 0)
    {
        geo.city = @[adcode];
    }
    
    [self.search AMapGeocodeSearch:geo];

}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self searchTipsWithKey:searchString];
    return YES;
}

-(void)searchTipsWithKey:(NSString *)key{
    if (key.length == 0){
        return;
    }
    AMapInputTipsSearchRequest *tips = [[AMapInputTipsSearchRequest alloc]init];
    tips.keywords = key;
    [self.search AMapInputTipsSearch:tips];
}

/* 输入提示回调. */
- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response {
    [self.tips setArray:response.tips];
    [self.displayController.searchResultsTableView reloadData];
}

/* 地理编码回调.*/
- (void)onGeocodeSearchDone:(AMapGeocodeSearchRequest *)request response:(AMapGeocodeSearchResponse *)response {
    if (response.geocodes.count == 0) {
        return;
    }
    
}
@end
