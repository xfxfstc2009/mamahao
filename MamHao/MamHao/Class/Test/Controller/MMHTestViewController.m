//
//  MMHTestViewController.m
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHTestViewController.h"
#import "MMHActionSheetViewController.h"
#import "MMHFilter.h"
#import "MMHProductDetailModel.h"
#import "MMHRegisterWithPhoneNumberViewController.h"                // 注册-手机号
#import "MMHResetPasswordViewController.h"
#import "MMHLoginViewController.h"
#import "MMHProductListViewController.h"
#import "MMHSortView.h"
#import "MMHFilter.h"
#import "MMHCategory.h"
#import "MMHFilterTermSelectionViewController.h"
#import "MMHNetworkAdapter+Product.h"
#import "MMHAppearance.h"
#import "MMHProductSpecSelectionViewController.h"
#import "MMHTabBarController.h"
//#import "MMHCityChooseViewController.h"
#import "MMHNavigationController.h"
#import "MMHConfirmationOrderViewController.h"
#import "MMHCartViewController.h"
#import "MMHAddressChoose.h"
#import "MMHAddressSingleModel.h"
#import "MMHOrderRootViewController.h"
#import "MMHWordOfMouthViewController.h"
#import "MMHGoodsPraiseViewController.h"
#import "MMHExpressDetailViewController.h"
#import "MMHAssetLibraryViewController.h"
#import "EaseMob.h"
#import "MMHChattingViewController.h"
#import "MMHRefundHomeViewController.h"                             // 退款退货
#import "MMHPraiseShowOrderViewController.h"
#import "MMHClassificationViewController.h"
#import "ZBarSDK.h"
// 支付宝
#import "MMHPayOrderModel.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>
#import "APAuthV2Info.h"
#import "MMHAliPayHandle.h"

#import "MMHExpressListViewController.h"
#import "MMHWriteBabyInfoAnimationViewController.h"
#import "MMHScanningViewController.h"
#import "MMHPaySafetyPassCodeViewController.h"

#import "MMHLocationManager.h"
#import "MMHPayView.h"
#import "MMHProductDetailViewController1.h"
#import "MMHActionSheetWithShareViewController.h"
#import "AMapSearchAPI.h"
#import "MMHAddressSingleViewController.h"
#import "MMHSearchAddressViewController.h"
#import "MMHCurrentLocationModel.h"
#import "MMHLocationNavigationManager.h"
#import "MMHPaySafetyPassCodeViewController.h"
#import "MMHNetworkAdapter+Center.h"
#import <CoreText/CoreText.h>
#import "MMHChattingViewController+Preparation.h"
#import "MMHWebShakeMBeanViewController.h"
#import "MMHNetworkAdapter+Image.h"
#import "MMHEditBabyInfoWithHomeViewController.h"
#import "MMHpaySuccessViewController.h"
#import "MMHUploaderImageViewController.h"
#import "MMHUploadImageManager.h"
#import "MMHChangeAddressViewController.h"
#import "MMHpaySuccessViewController.h"

@interface MMHTestViewController () <MMHFilterDelegate,UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZBarReaderDelegate,MMHPayViewDelegate>

@property (nonatomic, strong) MMHFilter *filter;
@property (nonatomic, strong) MMHSingleProductModel *product;
@property (nonatomic, strong) MMHProductDetailModel *productDetail;
@property (nonatomic, strong) UITableView *testTableView;
@property (nonatomic, strong) NSMutableArray *dataSourceMutableArr;
@end


@implementation MMHTestViewController


#pragma mark 
-(void)选择城市的viewcontroller{
//    MMHAddressChooseViewController *addressChooseViewController = [[MMHAddressChooseViewController alloc]init];
//    addressChooseViewController.isHasGesture = YES;
//    addressChooseViewController.areaBlock = ^(MMHAddressSingleModel *addressSingleModel){
//
//        UIAlertView *showAlert = [UIAlertView alertViewWithTitle:@"选择的城市" message:[NSString stringWithFormat:@"省:%@\n市:%@\n区:%@",addressSingleModel.province,addressSingleModel.city,addressSingleModel.area] buttonTitles:@[@"确定"] callback:nil];
//        [showAlert show];
//    };
//    [addressChooseViewController showInView:self.parentViewController];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [MMHLocationManager getArea];
    [MMHLocationManager getNormalAddress];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(NSString *)testTimeWithTimeInterval:(NSTimeInterval)time{
    NSDate *nowDate = [NSDate date];                                // 当前时间
    NSDate *targetDate = [nowDate dateByAddingTimeInterval:time];   // 目标时间
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components: (NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit )
                                                        fromDate:nowDate
                                                          toDate:targetDate
                                                         options:0];
    NSString *miniString = @"";         // 分钟
    NSString *secString = @"";
    miniString = [NSString stringWithFormat:@"%ld",(long)[components minute]];
    secString = [NSString stringWithFormat:@"%ld",(long)[components second]];
    
    
    [targetDate timeIntervalSinceNow];
    return [NSString stringWithFormat:@"%@分%@秒后订单失效",miniString,secString];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    self.barMainTitle =@"测试";
    [self arrayWithInit];
    [self createTableView];
    
    [self rightBarButtonWithTitle:@"hud Test" barNorImage:nil barHltImage:nil action:^{
        NSTimeInterval now = 10 * 60 + 4;
        NSLog(@"%@",[self testTimeWithTimeInterval:now]);
    }];
    

}

- (void)generateParameters:(id)sender
{
//    RegionSelector *rs = [[RegionSelector alloc] init];
//    rs.delegate = self;
//    MMHCityChooseViewController *cityChooseViewController = [[MMHCityChooseViewController alloc] initWithRegionSelector:rs];
//    [self.navigationController pushViewController:cityChooseViewController animated:YES];
//    return;
    
#if 0
    if (self.productDetail) {
        MMHProductSpecSelectionViewController *productSpecSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.productDetail
                                                                                                                                      specSelectedHander:^{
                                                                                                                                          AbstractViewController *vc = [[AbstractViewController alloc] init];
                                                                                                                                          [self.navigationController pushViewController:vc animated:YES];
                                                                                                                                      }];
        MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
        [tabBarController presentFloatingViewController:productSpecSelectionViewController animated:YES];
        return;
    }
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"goodsNo"] = @"223";
    parameters[@"shopId"] = @"0";
    parameters[@"lng"] = @"120";
    parameters[@"lat"] = @"30";
    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailWithSingleProduct:nil
                                                                      from:nil
                                                          succeededHandler:^(MMHProductDetailModel *productDetail) {
                                                              self.productDetail = productDetail;
                                                              MMHProductSpecSelectionViewController *productSpecSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.productDetail
                                                                                                                                                                                            specSelectedHander:^{
                                                                                                                                                                                                AbstractViewController *vc = [[AbstractViewController alloc] init];
                                                                                                                                                                                                [self.navigationController pushViewController:vc animated:YES];
                                                                                                                                                                                            }];
                                                              MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
                                                              [tabBarController presentFloatingViewController:productSpecSelectionViewController animated:YES];
                                                          } failedHandler:^(NSError *error) {
                
            }];
#endif
}



-(void)fileCreaaaa{
    MMHAddressChoose *addressChoose = [[MMHAddressChoose alloc]init];
    [addressChoose createFile];
}


- (void)filterDidChange:(MMHFilter *)filter
{
    NSLog(@"the test change filtr: %@", filter);
}





-(void)arrayWithInit{
    NSArray *tempArr = @[@"注册",@"登录",@"商品列表",@"订单",@"购物车",@"地址测试",@"订单",@"口碑",@"邮费",@"相册",@"评价晒单",@"pos商店", @"IM",@"退款退货",@"分类",@"扫描",@"支付宝",@"开场动画",@"二维码图片",@"地址",@"支付安全",@"支付",@"商品详情",@"POI",@"分享",@"地址",@"导航",@"宝宝信息",@"其他",@"上传图片",@"MMHChangeAddressViewController"];
    // the indexes are:     0       1        2        3       4         5        6      7      8       9       10         11       12
    self.dataSourceMutableArr = [NSMutableArray arrayWithObject:tempArr];
}


#pragma mark - UITableView
-(void)createTableView{
    self.testTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.testTableView setHeight:self.view.bounds.size.height];
    self.testTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.testTableView.delegate = self;
    self.testTableView.dataSource = self;
    self.testTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.testTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.testTableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = self.dataSourceMutableArr[section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentify = @"cellIdentify";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell){
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
   
        
    }
    cell.textLabel.text = [self.dataSourceMutableArr[indexPath.section] objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){                        // 注册
//        NSString *remoteURL = @"http://h5.weixiao1688.com/beans/";
//        MMHWebShakeMBeanViewController * smartVC = [[MMHWebShakeMBeanViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:remoteURL];
//        [self.navigationController pushViewController:smartVC animated:YES];

        MMHpaySuccessViewController *smart = [[MMHpaySuccessViewController alloc]init];
        [self.navigationController pushViewController:smart animated:YES];
    
    
    } else if (indexPath.row == 1){                 // 登录
//        MMHLoginViewController *restPasswordVC = [[MMHLoginViewController alloc]init];
//        MMHNavigationController *mainNav = [[MMHNavigationController alloc]initWithRootViewController:restPasswordVC];
//        [self.navigationController presentViewController:mainNav animated:YES completion:nil];
        [self presentLoginViewControllerWithSucceededHandler:^{
            
        }];
    } else if (indexPath.row == 2){                 // 订单
        MMHCategory *category = [[MMHCategory alloc] init];
        MMHFilter *filter = [[MMHFilter alloc] initWithCategory:category];
        MMHProductListViewController *mainVC = [[MMHProductListViewController alloc] initWithFilter:filter];
        [self.navigationController pushViewController:mainVC animated:YES];
    } else if (indexPath.row == 3){                 // 商品列表
        MMHConfirmationOrderViewController *orderViewController = [[MMHConfirmationOrderViewController alloc]init];
        [self.navigationController pushViewController:orderViewController animated:YES];
    } else if (indexPath.row == 4){                 // 购物车
        MMHCartViewController *cartViewController = [[MMHCartViewController alloc] init];
        [self.navigationController pushViewController:cartViewController animated:YES];
    } else if (indexPath.row == 5){                 // 地址测试
//        MMHAddressChooseViewController *addressChooseViewController = [[MMHAddressChooseViewController alloc]init];
//        addressChooseViewController.isHasGesture = YES;
//        addressChooseViewController.areaBlock = ^(MMHAddressSingleModel *addressSingleModel){
//            
//            UIAlertView *showAlert = [UIAlertView alertViewWithTitle:@"选择的城市" message:[NSString stringWithFormat:@"省:%@\n市:%@\n区:%@",addressSingleModel.province,addressSingleModel.city,addressSingleModel.area] buttonTitles:@[@"确定"] callback:nil];
//            [showAlert show];
//        };
//        [addressChooseViewController showInView:self.parentViewController];
    } else if (indexPath.row == 6){                 // 订单
        MMHOrderRootViewController *orderRootViewController = [[MMHOrderRootViewController alloc]init];
        [self.navigationController pushViewController:orderRootViewController animated:YES];
    } else if(indexPath.row == 7){
        /**
         *  口碑  word of mouth⁄
         */
        MMHWordOfMouthViewController *wordOfMouthViewController = [[MMHWordOfMouthViewController alloc] init];
        [self.navigationController pushViewController:wordOfMouthViewController animated:YES];
    } else if (indexPath.row == 8){
        MMHExpressListViewController *expressDetailViewController = [[MMHExpressListViewController alloc]init];
        [self.navigationController pushViewController:expressDetailViewController animated:YES];
    } else if (indexPath.row == 9){
       UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"相册" buttonTitles:@[@"取消",@"相册",@"拍照"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                MMHAssetLibraryViewController *assetLibraryViewController = [[MMHAssetLibraryViewController alloc]init];
                assetLibraryViewController.numberOfCouldSelectingAssets = 3;
                __weak typeof(self) weakSelf = self;
                assetLibraryViewController.selectAssetsBlock = ^(NSArray *imageArr,NSArray *assetArr){
                    if (!weakSelf) {
                        return ;
                    }
                   [MMHUploadImageManager uploadImageArrWithImageArr:imageArr callback:^(BOOL isSuccess, NSArray *fileNameArr) {
                       if (isSuccess){
                           [[UIAlertView alertViewWithTitle:@"成功" message:nil buttonTitles:@[@"确定"] callback:nil]show];
                       } else {
                           [[UIAlertView alertViewWithTitle:@"失败" message:nil buttonTitles:@[@"确定"] callback:nil]show];
                       }
                   }];
                };
                
                UINavigationController *smartNav = [[UINavigationController alloc]initWithRootViewController:assetLibraryViewController];
                [self.navigationController presentViewController:smartNav animated:YES completion:nil];
            } else if (buttonIndex == 1){
                if (![UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"当前设备不可用 " buttonTitles:@[@"确认"] callback:nil]show];
                    return ;
                } else {
                    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                    imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                    imagePickerController.delegate = self;
                    [self presentViewController:imagePickerController animated:YES completion:nil];
                }
            } else {
            
            }
        }];
        [actionSheet showInView:self.view];
    }else if (indexPath.row == 10){
        /**
         *  评价晒单
         */
        
        MMHPraiseShowOrderViewController *praiseShowOrderVC = [[MMHPraiseShowOrderViewController alloc] init];
        [self.navigationController pushViewController:praiseShowOrderVC animated:YES];
        
    } else if (indexPath.row == 11){
       
    }
    else if (indexPath.row == 12) {
        [self startChatting];
        return;
        [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:@"louis0" password:@"111" completion:^(NSDictionary *loginInfo, EMError *error) {
            if (!error && loginInfo) {
                NSLog(@"登陆成功");
                [self startChatting];
            }
            else {
                NSLog(@"登陆失败");
            }
        } onQueue:nil];
    } else if (indexPath.row == 13){
        MMHRefundHomeViewController *refundHomeViewController = [[MMHRefundHomeViewController alloc]init];
        [self.navigationController pushViewController:refundHomeViewController animated:YES];
    }else if(indexPath.row == 14){
        MMHClassificationViewController *classficationVC = [[MMHClassificationViewController alloc] init];
        [self.navigationController pushViewController:classficationVC animated:YES];
    } else if (indexPath.row == 15){
        MMHScanningViewController *tiaoxingmaVC = [[MMHScanningViewController alloc]init];
        tiaoxingmaVC.scanFinished = ^(NSString *smartInfo){
            [[UIAlertView alertViewWithTitle:smartInfo message:nil buttonTitles:@[@"确定"] callback:nil]show];
        };
        tiaoxingmaVC.closeCameraBlock = ^(){
            [[UIAlertView alertViewWithTitle:@"亲，您长时间未使用摄像头，妈妈好已为您关闭，避免机器损坏" message:nil buttonTitles:@[@"确定"] callback:nil] show];
        };
        [self.navigationController pushViewController:tiaoxingmaVC animated:YES];
    } else if (indexPath.row == 16){
//        APViewController *smart = [[APViewController alloc]init];
//        [self.navigationController pushViewController:smart animated:YES];
//        return;
        MMHPayOrderModel *payOrderModel = [[MMHPayOrderModel alloc]init];
        payOrderModel.amount = [NSString stringWithFormat:@"%.2f",0.01];
        payOrderModel.tradeNO = [self generateTradeNO];
        payOrderModel.productName = @"我是商品名称";
        payOrderModel.productDescription = @"我是商品描述";
        [MMHAliPayHandle goPayWithOrder:payOrderModel];
    } else if (indexPath.row == 17){
        MMHWriteBabyInfoAnimationViewController *writeBabyInfoAnimationViewController = [[MMHWriteBabyInfoAnimationViewController alloc]init];
        UINavigationController *animationBabyViewController = [[UINavigationController alloc]initWithRootViewController:writeBabyInfoAnimationViewController];
        [self.navigationController presentViewController:animationBabyViewController animated:YES completion:nil];
    } else if (indexPath.row == 18){
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.delegate = self;
        [self presentViewController:imagePickerController animated:YES completion:nil];
    } else if (indexPath.row == 19){
        
        [[MMHLocationManager sharedLocationManager] getLocationCoordinate:^(AMapAddressComponent *addressComponent) {
            MMHCurrentLocationModel *currentLocation = [[MMHCurrentLocationModel alloc]init];
            
                [[UIAlertView alertViewWithTitle:nil message:[NSString stringWithFormat:@"%@",addressComponent]  buttonTitles:@[@"确定"] callback:nil]show];
        }];
    } else if (indexPath.row == 20){
        MMHPaySafetyPassCodeViewController *paySafeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
        paySafeViewController.transferPhoneNumber = @"15669969617";
        [self.navigationController pushViewController:paySafeViewController animated:YES];
    } else if (indexPath.row == 21){
//        MMHPayView *view = [[MMHPayView alloc]init];
//        view.passCodeBlock = ^(NSString *passcode){
//            [[UIAlertView alertViewWithTitle:nil message:passcode buttonTitles:@[@"确定"] callback:nil]show];
//        };
//        view.resetRedirectBlock = ^(){
//            MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
//            
//            [self.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
//        };
//        [view payViewShow];

        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestWithGetSettingStatus];
    } else if (indexPath.row == 22){
        MMHProductDetailViewController1 *productDetailViewCOntroller = [[MMHProductDetailViewController1 alloc]init];
        [self.navigationController pushViewController:productDetailViewCOntroller animated:YES];
    } else if (indexPath.row == 23){        // POI
        // 1. 创建search
//        MMHGeoViewController *geoVC = [[MMHGeoViewController alloc]init];
//        [self.navigationController pushViewController:geoVC animated:YES];
    } else if (indexPath.row == 24){
        MMHActionSheetWithShareViewController *shareViewController = [[MMHActionSheetWithShareViewController alloc]init];
        shareViewController.isHasGesture = YES;
        [shareViewController showInView:self.parentViewController];
    } else if (indexPath.row == 25){
//        [self areaId];
        
        MMHSearchAddressViewController *searchAddressVC = [[MMHSearchAddressViewController alloc]init];
        [self.navigationController pushViewController:searchAddressVC animated:YES];
    } else if (indexPath.row == 26){            // 导航
        __weak MMHTestViewController *weakViewController = self;
        MMHLocationNavigationManager *locationNavigationManager = [[MMHLocationNavigationManager alloc]init];
        [locationNavigationManager showActionSheetWithTabBar:weakViewController.tabBarController.tabBar shopName:@"炒鸡好妈妈" shopLocationLat:30.230555 shopLocationLon:120.192111];
    } else if (indexPath.row == 27){
        MMHEditBabyInfoWithHomeViewController * babyInfoViewController = [[MMHEditBabyInfoWithHomeViewController alloc]init];
        UINavigationController *babyNavigationViewControler = [[UINavigationController alloc]initWithRootViewController:babyInfoViewController];
        [self.navigationController presentViewController:babyNavigationViewControler animated:YES completion:nil];
    } else if (indexPath.row == 28){
        MMHpaySuccessViewController *paySuccessViewController = [[MMHpaySuccessViewController alloc]init];
        UINavigationController *smart = [[UINavigationController alloc]initWithRootViewController:paySuccessViewController];

        [self.navigationController presentViewController:smart animated:YES completion:nil];
    }else if (indexPath.row == 29){
//        MMHUploaderImageViewController *uploaderImageViewController = [[MMHUploaderImageViewController alloc] init];
//        [self.navigationController pushViewController:uploaderImageViewController animated:YES];

    } else if (indexPath.row == 30){
        MMHChangeAddressViewController *changeAddressViewCOntroller = [[MMHChangeAddressViewController alloc]init];
        [self.navigationController pushViewController:changeAddressViewCOntroller animated:YES];
    }
}



#pragma mark 扫描照片
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    ZBarReaderController *reader = [[ZBarReaderController alloc]init];
    CGImageRef smarImage = image.CGImage;
    ZBarSymbol *symbol = nil;
    for (symbol in [reader scanImage:smarImage])
        break;
        NSLog(@"%@",symbol.data);
}






- (void)startChatting {
    return;
    if ([MMHChattingViewController isReadyToChat]) {
        MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:nil];
        [self.navigationController pushViewController:chattingViewController animated:YES];
    }
    else {
        [self.view showProcessingViewWithMessage:@"准备中"];
        __weak __typeof(self) weakSelf = self;
        [MMHChattingViewController prepareForChattingWithContext:nil
                                                succeededHandler:^(MMHChattingViewController *chattingViewController) {
                                                    [weakSelf.view hideProcessingView];
                                                    [weakSelf.navigationController pushViewController:chattingViewController animated:YES];
                                                } failedHander:^(NSError *error) {
                                                    [weakSelf.view hideProcessingView];
                                                    [weakSelf.view showTips:@"无法联系到客服，请重试"];
                                                }];
    }
}


//#pragma mark - image picker delegte
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    [picker dismissViewControllerAnimated:YES completion:^{}];
//    
//    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:9 inSection:0];
//    UITableViewCell *testCell = [self.testTableView cellForRowAtIndexPath:indexPath];
//    testCell.imageView.image = image;
//    [self.testTableView reloadData];
//}

- (NSString *)generateTradeNO {
    static int kNumber = 15;
    
    NSString *sourceStr = @"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    NSMutableString *resultStr = [[NSMutableString alloc] init];
    srand((unsigned)time(0));
    for (int i = 0; i < kNumber; i++)
    {
        unsigned index = rand() % [sourceStr length];
        NSString *oneStr = [sourceStr substringWithRange:NSMakeRange(index, 1)];
        [resultStr appendString:oneStr];
    }
    return resultStr;
}


-(void)getArea{
    [[MMHLocationManager sharedLocationManager] getLocationCoordinate:^(AMapAddressComponent *addressComponent) {
        // 【acode】
        [MMHCurrentLocationModel sharedLocation].areaId = addressComponent.adcode;
        // 【经纬度】
        [MMHCurrentLocationModel sharedLocation].lat = addressComponent.streetNumber.location.latitude?addressComponent.streetNumber.location.latitude:30.2325363;
        [MMHCurrentLocationModel sharedLocation].lng = addressComponent.streetNumber.location.longitude?addressComponent.streetNumber.location.longitude:120.188484;
        [MMHCurrentLocationModel sharedLocation].province = addressComponent.province;
        [MMHCurrentLocationModel sharedLocation].city = addressComponent.city;
        [MMHCurrentLocationModel sharedLocation].district = addressComponent.district;
        [MMHCurrentLocationModel sharedLocation].township = addressComponent.township;
        [MMHCurrentLocationModel sharedLocation].street = addressComponent.streetNumber.street;
        [[NSNotificationCenter defaultCenter] postNotificationName:MMHCurrentLocationNotification object:nil];
    }];
}

#pragma mark - 接口
-(void)sendRequestWithGetSettingStatus{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestGetIsSettingPayPasswordFrom:nil succeededHandler:^(MMHSettingPaySettingModel *paySettedModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (paySettedModel.isSetted){               // 设置了
            MMHPayView *view = [[MMHPayView alloc]init];
            view.passCodeBlock = ^(NSString *passcode){
                [strongSelf sendRequestWithVerificationPayPassCode:passcode];
            };
            view.resetRedirectBlock = ^(){
                MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
                paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
                paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeReset;
                [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
            };
            [view payViewShow];
        } else {                                            // 【提示去设置】
            MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
            paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
            paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeReset;
            [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 验证支付密码
-(void)sendRequestWithVerificationPayPassCode:(NSString *)payPassCode{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithCheckPayPassword:payPassCode from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:nil message:@"成功" buttonTitles:@[@"确定"] callback:nil]show];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}


-(void)createRangeLabel{
    UILabel *smartLabel = [[UILabel alloc]init];
    smartLabel.backgroundColor = [UIColor clearColor];
    smartLabel.text = @"smart";
    smartLabel.frame = CGRectMake(100, 100, 100, 100);
    [self.view addSubview:smartLabel];
    NSMutableAttributedString *mutableAttributedString = [[NSMutableAttributedString alloc]initWithString:smartLabel.text];
    [mutableAttributedString addAttribute:NSFontAttributeName value:F1 range:NSMakeRange(0, 3)];
    smartLabel.attributedText = mutableAttributedString;
}


@end
