//
//  AbstractViewController.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHNavigationController.h"
//#import "MamHao-Swift.h"
#import "MMHLogbook.h"

@class MMHLogbook;
@class MMHAccount;


@protocol AbstractViewControllerBlankViewAction <NSObject>

- (void)blankViewButtonTapped;

@end


@interface AbstractViewController : UIViewController <AbstractViewControllerBlankViewAction>

@property (nonatomic, copy, nullable) NSString *barMainTitle;
@property (nonatomic, copy, nullable) NSString *barSubTitle;


- (void)setDismissItemOnLeftBarButtonItem;
- (void)setGoBackItemOnLeftBarButtonItem;

-(void)hidesBackButton;

NS_ASSUME_NONNULL_BEGIN
// 左侧Nav按钮
- (UIButton *)leftBarButtonWithTitle:(nullable NSString *)title
                         barNorImage:(nullable UIImage *)norImage
                         barHltImage:(nullable UIImage *)hltImage
                              action:(void(^)(void))actionBlock;

// 右侧Nav按钮
- (UIButton *)rightBarButtonWithTitle:(nullable NSString *)title
                          barNorImage:(nullable UIImage *)norImage
                          barHltImage:(nullable UIImage *)hltImage
                               action:(void(^)(void))actionBlock;

//

-(void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated;

- (void)presentLoginViewControllerWithSucceededHandler:(nullable void(^)(void))succeededHandler;

- (void)pushViewController:(UIViewController *)viewController;
- (void)pushViewControllers:(NSArray *)viewControllers;
- (void)popViewController;
- (void)popToViewControllerOfClass:(Class)aClass;
- (void)dismissViewController;

@property (nonatomic, strong, nullable)  UIView *blankView;
NS_ASSUME_NONNULL_END
@end
