//
//  MMHTabBarController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHTabBarController.h"
#import "AbstractViewController.h"
#import "MMHTestViewController.h"
#import "MMHMamhaoViewController.h"
#import "MMHCartViewController.h"
#import "MMHShopListViewController.h"
#import "MMHPersonalCenterViewController.h"
#import "MMHSettingViewController.h"
#import "AppDelegate.h"


typedef NS_ENUM(NSInteger, MMHTabBarControllerViewControllerIndex) {
    MMHTabBarControllerViewControllerIndexMamhao,
    MMHTabBarControllerViewControllerIndexCart,
    MMHTabBarControllerViewControllerIndexPersonCenter,
};


@interface MMHTabBarController ()

@property (nonatomic, strong) UIView *floatingBackgroundView;
@property (nonatomic, strong) MMHFloatingViewController *floatingViewController;

@end


@implementation MMHTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // test
//    MMHTestViewController *testViewController = [[MMHTestViewController alloc] init];
//    MMHNavigationController *mainNavigationVC = [[MMHNavigationController alloc]initWithRootViewController:testViewController];

    // 妈妈好
    MMHMamhaoViewController *mamhaoViewController = [[MMHMamhaoViewController alloc] init];
    MMHNavigationController *mamhaoNavigationController = [[MMHNavigationController alloc] initWithRootViewController:mamhaoViewController];
    mamhaoNavigationController.tabBarItem.tag = 1;
    
    MMHShopListViewController *shopListViewController = [[MMHShopListViewController alloc] init];
    MMHNavigationController *shopListNavigationController = [[MMHNavigationController alloc] initWithRootViewController:shopListViewController];
    shopListNavigationController.tabBarItem.tag = 2;
    
    // 购物车
    MMHCartViewController *cartViewController = [[MMHCartViewController alloc] init];
    MMHNavigationController *cartNavigationController = [[MMHNavigationController alloc] initWithRootViewController:cartViewController];
    cartNavigationController.tabBarItem.tag = 3;
    
    // 个人中心
    MMHPersonalCenterViewController *personalCenterVC = [[MMHPersonalCenterViewController alloc] init];
    MMHNavigationController *personalCenterNavi = [[MMHNavigationController alloc] initWithRootViewController:personalCenterVC];
    personalCenterNavi.tabBarItem.tag = 4;
    
    
//     self.viewControllers = @[mamhaoNavigationController, cartNavigationController, personalCenterNavi, mainNavigationVC];
    
     self.viewControllers = @[mamhaoNavigationController, cartNavigationController, personalCenterNavi];
   
}


- (void)presentFloatingViewController:(MMHFloatingViewController *)viewController animated:(BOOL)animated {
    if (self.floatingViewController != nil) {
        return;
    }

    self.floatingViewController = viewController;

    if (self.floatingBackgroundView == nil) {
        UIView *floatingBackgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        floatingBackgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
        floatingBackgroundView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(floatingBackgroundViewTapped:)];
        [floatingBackgroundView addGestureRecognizer:tapGestureRecognizer];
        [self.view addSubview:floatingBackgroundView];
        self.floatingBackgroundView = floatingBackgroundView;
    }
    self.floatingBackgroundView.alpha = 0.0f;

    CGRect startFrame = self.view.bounds;
    switch (viewController.presentDirection) {
        case MMHFloatingViewControllerPresentDirectionUp: {
            startFrame.origin.y = CGRectGetMaxY(startFrame);
            startFrame.size = [viewController sizeWhileFloating];
            break;
        };
        case MMHFloatingViewControllerPresentDirectionLeft: {
            startFrame.origin.x = CGRectGetMaxX(startFrame);
            startFrame.size = [viewController sizeWhileFloating];
            break;
        };
        default:
            break;
    }
    viewController.view.frame = startFrame;
    
    if (animated) {
        __weak __typeof(self) weakSelf = self;
        [self addChildViewController:viewController];
        viewController.settledViewController = self;
        [self.view addSubview:viewController.view];
        [UIView animateWithDuration:0.25
                         animations:^{
                             weakSelf.floatingBackgroundView.alpha = 1.0f;
                             switch (viewController.presentDirection) {
                                 case MMHFloatingViewControllerPresentDirectionUp: {
                                     [viewController.view moveYOffset:([viewController sizeWhileFloating].height * (-1.0f))];
                                     break;
                                 };
                                 case MMHFloatingViewControllerPresentDirectionLeft: {
                                     [viewController.view moveXOffset:([viewController sizeWhileFloating].width * (-1.0f))];
                                     break;
                                 };
                                 default:
                                     break;
                             }
                         } completion:^(BOOL finished) {
                    [viewController didMoveToParentViewController:weakSelf];
                }];
    }
    else {
        [self addChildViewController:viewController];
        viewController.settledViewController = self;
        [self.view addSubview:viewController.view];
        self.floatingBackgroundView.alpha = 1.0f;
        switch (viewController.presentDirection) {
            case MMHFloatingViewControllerPresentDirectionUp: {
                [viewController.view moveYOffset:([viewController sizeWhileFloating].height * (-1.0f))];
                break;
            };
            case MMHFloatingViewControllerPresentDirectionLeft: {
                [viewController.view moveXOffset:([viewController sizeWhileFloating].width * (-1.0f))];
                break;
            };
            default:
                break;
        }
        [viewController didMoveToParentViewController:self];
    }
}


- (void)floatingBackgroundViewTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self dismissFloatingViewControllerAnimated:YES completion:nil];
}


- (void)dismissFloatingViewControllerAnimated:(BOOL)animated completion:(void(^)())completion
{
    if (animated) {
        self.floatingViewController.settledViewController = nil;
        [self.floatingViewController willMoveToParentViewController:nil];
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.floatingBackgroundView.alpha = 0.0f;
                             switch (self.floatingViewController.presentDirection) {
                                 case MMHFloatingViewControllerPresentDirectionUp: {
                                     [self.floatingViewController.view moveYOffset:[self.floatingViewController sizeWhileFloating].height];
                                     break;
                                 };
                                 case MMHFloatingViewControllerPresentDirectionLeft: {
                                     [self.floatingViewController.view moveXOffset:[self.floatingViewController sizeWhileFloating].width];
                                     break;
                                 };
                                 default:
                                     break;
                             }
                         } completion:^(BOOL finished) {
                    [self.floatingViewController.view removeFromSuperview];
                    [self.floatingViewController removeFromParentViewController];
                             
                             if (completion) {
                                 completion();
                             }

                    self.floatingBackgroundView = nil;
                    self.floatingViewController = nil;
                }];
    }
    else {
        self.floatingViewController.settledViewController = nil;
        [self.floatingViewController willMoveToParentViewController:nil];
        [self.floatingViewController.view removeFromSuperview];
        [self.floatingViewController removeFromParentViewController];
        
        if (completion) {
            completion();
        }

        self.floatingBackgroundView = nil;
        self.floatingViewController = nil;
    }
}

#pragma mark Redirection
+(void)redirectToHome{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    [tabBarController setSelectedIndex:0];
}

+(void)redirectToHomeWithAnimation{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    [tabBarController setSelectedIndex:0];
    MMHNavigationController *mamhaoNavigationController = [tabBarController.viewControllers objectAtIndex:0];
    [mamhaoNavigationController popToRootViewControllerAnimated:NO];
}

+(void)redirectToHomeWithController:(UIViewController *)controller{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    MMHNavigationController *mamhaoNavigationController = [tabBarController.viewControllers objectAtIndex:0];
    NSMutableArray *sourceMutableArr = [NSMutableArray arrayWithArray:mamhaoNavigationController.viewControllers];
    [sourceMutableArr addObject:controller];
    mamhaoNavigationController.viewControllers = sourceMutableArr;
    controller.hidesBottomBarWhenPushed = YES;
    [tabBarController setSelectedIndex:MMHTabBarControllerViewControllerIndexMamhao];
}

+(void)redirectToCart {
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    if (tabBarController.selectedIndex == MMHTabBarControllerViewControllerIndexCart) {
        MMHNavigationController *navigationController = tabBarController.viewControllers[MMHTabBarControllerViewControllerIndexCart];
        if ([navigationController respondsToSelector:@selector(popToRootViewControllerAnimated:)]) {
            [navigationController popToRootViewControllerAnimated:NO];
        }
        return;
    }
    
    NSArray *viewControllers = tabBarController.viewControllers;
    for (MMHNavigationController *navigationController in viewControllers) {
        if ([navigationController respondsToSelector:@selector(popToRootViewControllerAnimated:)]) {
            [navigationController popToRootViewControllerAnimated:NO];
        }
    }
    tabBarController.viewControllers = viewControllers;
    tabBarController.selectedIndex = MMHTabBarControllerViewControllerIndexCart;
}

+(void)redirectToCenter{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    [tabBarController setSelectedIndex:MMHTabBarControllerViewControllerIndexPersonCenter];
}

+(void)redirectToCenterWithController:(UIViewController *)controller{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;
    MMHTabBarController *tabBarController = (MMHTabBarController *)delegate.window.rootViewController;
    MMHNavigationController *personalCenterNavi = [tabBarController.viewControllers objectAtIndex:2];
    NSMutableArray *sourceMutableArr = [NSMutableArray arrayWithArray:personalCenterNavi.viewControllers];
    [sourceMutableArr addObject:controller];
    personalCenterNavi.viewControllers = sourceMutableArr;
    [tabBarController setSelectedIndex:2];
    [personalCenterNavi hidesBottomBarWhenPushed];
    controller.hidesBottomBarWhenPushed = NO;
}

@end
