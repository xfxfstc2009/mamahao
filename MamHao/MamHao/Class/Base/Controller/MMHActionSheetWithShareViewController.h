//
//  MMHActionSheetWithShareViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHHTMLModel.h"
@interface MMHActionSheetWithShareViewController : UIViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)MMHHTMLModel *transferShareModel;               /**< 上个页面传入的model*/

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果
@end
