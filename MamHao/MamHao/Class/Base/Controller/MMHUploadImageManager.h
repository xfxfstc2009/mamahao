//
//  MMHUploadImageManager.h
//  MamHao
//
//  Created by SmartMin on 15/7/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【上传图片】
#import <Foundation/Foundation.h>

@interface MMHUploadImageManager : NSObject

+ (void)uploadImageArrWithImageArr:(NSArray *)imageArray callback:(void(^)(BOOL isSuccess,NSArray *fileNameArr))callbackBlock;
+ (void)uploadCommentImageArrWithImageArr:(NSArray *)imageArray callback:(void(^)(BOOL isSuccess,NSArray *fileNameArr))callbackBlock;
@end
