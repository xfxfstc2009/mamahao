//
//  AbstractViewController+Chatting.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@interface AbstractViewController (Chatting)

- (void)startChattingWithContext:(id)context;

@end
