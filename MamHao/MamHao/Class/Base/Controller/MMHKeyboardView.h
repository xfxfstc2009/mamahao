//
//  MMHKeyboardView.h
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 自定义键盘
#import <UIKit/UIKit.h>
@protocol UIKeyboardDelegate;
@interface MMHKeyboardView : UIView

@property (nonatomic,assign)id<UIKeyboardDelegate>keyboardDelegate;
#define UIKeyboardViewWillRotateNotification @"UIKeyboardViewWillRotateNotification"

#define UIKeyboardViewDidRotateNotification @"UIKeyboardViewDidRotateNotification"
@end


@protocol UIKeyboardDelegate <NSObject>

@optional
-(void)keyboardViewWillAppear:(MMHKeyboardView *)keyboard;
-(void)keyboardViewDidAppear:(MMHKeyboardView *)keyboard;
-(void)keyboardViewWillDisappear:(MMHKeyboardView *)keyboard;
-(void)keyboardViewDidDisappear:(MMHKeyboardView *)keyboard;
@end