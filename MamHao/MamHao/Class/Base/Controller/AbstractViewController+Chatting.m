//
//  AbstractViewController+Chatting.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController+Chatting.h"
#import "MMHChattingViewController+Preparation.h"


@implementation AbstractViewController (Chatting)


- (void)startChattingWithContext:(id)context
{
    if ([MMHChattingViewController isReadyToChat]) {
        MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:context];
        [self.navigationController pushViewController:chattingViewController animated:YES];
    }
    else {
        [self.view showProcessingViewWithMessage:@"准备中"];
        __weak __typeof(self) weakSelf = self;
        [MMHChattingViewController prepareForChattingWithContext:context
                                                succeededHandler:^(MMHChattingViewController *chattingViewController) {
                                                    [weakSelf.view hideProcessingView];
                                                    [weakSelf.navigationController pushViewController:chattingViewController animated:YES];
                                                } failedHander:^(NSError *error) {
                                                    [weakSelf.view hideProcessingView];
                                                    [weakSelf.view showTips:@"无法联系到客服，请重试"];
                                                }];
    }
    self.navigationController.navigationItem.rightBarButtonItem.enabled = YES;
}


@end
