//
//  MMHBuriedPointManager.h
//  MamHao
//
//  Created by SmartMin on 15/7/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MMHBuriedPointType) {
    // 购物主流程
    MMHBuriedPointTypeAddToCart = 11,                    /**< 加入购物车*/
    MMHBuriedPointTypeBuyNow = 12,                       /**< 立即购买*/
    MMHBuriedPointTypeChooseSKU = 13,                    /**< 选择SKU*/
    MMHBuriedPointTypeClickShopCartButton = 14,          /**< 是否点击购物车按钮*/
    MMHBuriedPointTypeShoppingCart = 15,                 /**< 购物车*/
    MMHBuriedPointTypeIsToConfirmation = 16,             /**< 是否去结算*/
    MMHBuriedPointTypeConfirmation = 17,                 /**< 确认订单*/
    MMHBuriedPointTypeIsPayYes = 18,                     /**< 是否支付成功*/
    MMHBuriedPointTypeIsLogin = 19,                      /**< 是否登录*/
    
    // 搜索商品流程
    MMHBuriedPointTypeBrand = 21,                        /**< 品牌*/
    MMHBuriedPointTypeTwoClass = 22,                     /**< 二级类目*/
    MMHBuriedPointTypeThreeClass = 23,                   /**< 三级类目*/
    MMHBuriedPointTypeShopList = 24,                     /**< 商品列表*/
    
    // 首页埋点
    MMHBuriedPointTypeMamhaoBeanSpecial = 31,                        /**< 妈豆尊享*/
    MMHBuriedPointTypeMamhaoShop = 32,                              /**< 附近实体店*/
    MMHBuriedPointTypeMamhaoUserFeatured = 33,                             /**< 热购*/
    MMHBuriedPointTypeMamhaoFeaturedProducts = 34,                         /**< 优选*/
    MMHBuriedPointTypeMamhaoEditProfile = 35,                              /**< 宝妈档案*/
    MMHBuriedPointTypeMamhaoChangeMonthAge = 36,                                   /**< 年龄切换*/
    MMHBuriedPointTypeMamhaoCategory = 37,                                       /**< 类目*/
    MMHBuriedPointTypeMamhaoSearch = 38,                                      /**< 搜索*/
    MMHBuriedPointTypeMamhaoBeanGame = 39,                                  /**< 摇妈豆*/
    MMHBuriedPointTypeMamhaoMessages = 40,                                     /**< 消息*/
    
    // 商品列表
    MMHBuriedPointTypeProductList = 41,                                     /**< 商品列表筛选*/
    MMHBuriedPointTypeProductBack = 42,                                     /**< 商品列表返回*/
    
    // 商品详情
    MMHBuriedPointTypeProductLikeCustomer = 51,                                    /**< 商品详情联系客服*/
    MMHBuriedPointTypeProductShopMainImg = 52,                                     /**< 商品主图*/
    MMHBuriedPointTypeProductPraise = 53,                                          /**< 商品口碑*/
    MMHBuriedPointTypeProductImgTextDetail = 54,                                   /**< 图文详情*/
    MMHBuriedPointTypeProductParameter = 55,                                       /**< 商品参数*/
    MMHBuriedPointTypeProductShop = 56,                                            /**< 门店模块*/
    MMHBuriedPointTypeProductGuessYouLike = 57,                                    /**< 猜你喜欢*/
    MMHBuriedPointTypeProductMotherSay = 58,                                       /**< 妈妈说*/
    
    // 门店首页
    MMHBuriedPointTypeShopNavigation = 61,                                          /**< 门店导航*/
    MMHBuriedPointTypeShopAttention = 62,                                               /**< 门店关注*/
    MMHBuriedPointTypeShopPhone = 63,                                           /**< 门店电话*/
    MMHBuriedPointTypeShopPraise = 64,                                          /**< 门店口碑*/
    MMHBuriedPointTypeShopScreening = 65,                                       /**< 筛选*/
    
    // 购物车
    MMHBuriedPointTypeShoppingCartAllClick = 71,                                /**< 全选按钮*/
    MMHBuriedPointTypeShoppingCartProductClick = 72,                            /**< 购物车按钮*/
    MMHBuriedPointTypeShoppingCartAddAndCutClick = 73,                                      /**< 加减*/
    MMHBuriedPointTypeShoppingCartDelete = 74,                                      /**< 删除*/
    MMHBuriedPointTypeShoppingCartCalculation = 75,                                 /**< 计算*/
    
    // 个人中心
    MMHBuriedPointTypeCenterInfo = 81,          /**< 会员信息*/
    MMHBuriedPointTypeCenterOrderAll = 82,      /**< 全部订单*/
    MMHBuriedPointTypeCenterOrderTypePendingPay = 83, /**< 待付款*/
    MMHBuriedPointTypeCenterOrderTypeBeShipped = 84, /**< 待发货*/
    MMHBuriedPointTypeCenterOrderTypeBeReceipt = 85, /**< 待收货*/
    MMHBuriedPointTypeCenterOrderBeEvaluation = 86, /**< 待评价*/
    MMHBuriedPointTypeCenterOrderRefund = 87,       /**< 退款退货*/
    MMHBuriedPointTypeCenterCoupons = 88,           /**< 优惠券*/
    MMHBuriedPointTypeCenterMBean = 89,             /**< 妈豆*/
    MMHBuriedPointTypeCenterIntegral = 810,                /**< 积分*/
    MMHBuriedPointTypeCenterAddress =811,           /**< 收货地址*/
    MMHBuriedPointTypeCenterLinkCustomer = 812,     /**< 联系客服*/
    MMHBuriedPointTypeCenterFeedback = 813,         /**< 意见反馈*/
    MMHBuriedPointTypeCenterMessage = 814,          /**< 消息*/
    MMHBuriedPointTypeCenterAbout = 815,            /**< 关于妈妈好*/
    MMHBuriedPointTypeCenterScore = 816,            /**< 评分*/
    MMHBuriedPointTypeCenterSettingPayPassword = 817,     /**< 设置密码*/
    MMHBuriedPointTypeCenterLogOut = 818,           /**< 退出*/
    MMHBuriedPointTypeCenterCollection = 819,       /**< 收藏*/
    
    // 运营
    MMHBuriedPointTypeOperateRecommend = 91,        /**< 推荐*/
    MMHBuriedPointTypeOperateEatting = 92,          /**< 吃什么*/
    MMHBuriedPointTypeOperateWear = 93,             /**< 穿什么*/
    MMHBuriedPointTypeOperateUsing = 94,              /**< 用什么*/
    MMHBuriedPointTypeOperatePlay = 95,               /**< 玩什么*/
    MMHBuriedPointTypeOperateStydy = 96,                /**< 学什么*/
    MMHBuriedPointTypeOperateMamCenter = 97,            /**< 妈妈专区*/
    MMHBuriedPointTypeOperateHotWords = 98,             /**< 关键热词*/
    
    // 商业效果
    MMHBuriedPointTypeBusinessBindingShop = 101,        /**< 门店下单绑定店铺*/
    MMHBuriedPointTypeBusinessMBean = 102,              /**< 妈豆商品下单*/
    MMHBuriedPointTypeBusinessClassAndSearch = 103,     /**< 类目搜索下单*/
    MMHBuriedPointTypeBusinessPreferred = 104,      /**< 为您优选*/
    MMHBuriedPointTypeBusinessHotShoppingOrder = 105,/**< 同龄宝妈热购*/
};


@interface MMHBuriedPointManager : NSObject


@end
