//
//  MMHKeyboardView.m
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHKeyboardView.h"

@interface MMHKeyboardView()
@property (nonatomic,assign) BOOL isRotating;
-(void)keyboardViewCommonInit;
-(void)keyboardWillAppear:(NSNotification *)notification;
-(void)keyboardWillDisappear:(NSNotification *)notification;
-(void)viewControllerWillRotate:(NSNotification *)notification;
-(void)viewControllerDidRotate:(NSNotification *)notification;
-(CGRect)fixedKeyboardRect:(CGRect)originalRect;
@end

@implementation MMHKeyboardView

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(id)init{
    self = [super init];
    if (self){
        [self keyboardViewCommonInit];
    }
    return self;
}

- (void) keyboardViewCommonInit{
    self.isRotating = NO;
    
    //Register for notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppear:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillDisappear:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewControllerWillRotate:) name:UIKeyboardViewWillRotateNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(viewControllerDidRotate:) name:UIKeyboardViewDidRotateNotification object:nil];
    
}

#pragma mark - Keyboard notification methods
- (void) keyboardWillAppear:(NSNotification*)notification{
    
    //Get begin, ending rect and animation duration
    CGRect beginRect = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect endRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat animDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    //Transform rects to local coordinates
    beginRect = [self fixedKeyboardRect:beginRect];
    endRect = [self fixedKeyboardRect:endRect];
    
    //Get this view begin and end rect
    CGRect selfBeginRect = CGRectMake(beginRect.origin.x,
                                      beginRect.origin.y - self.frame.size.height,
                                      beginRect.size.width,
                                      self.frame.size.height);
    CGRect selfEndingRect = CGRectMake(endRect.origin.x,
                                       endRect.origin.y - self.frame.size.height,
                                       endRect.size.width,
                                       self.frame.size.height);
    
    //Set view position and hidden
    self.frame = selfBeginRect;
    self.alpha = 0.0f;
    [self setHidden:NO];
    
    //If it's rotating, begin animation from current state
    UIViewAnimationOptions options = UIViewAnimationOptionAllowAnimatedContent;
    if (self.isRotating){
        options |= UIViewAnimationOptionBeginFromCurrentState;
    }
    
    //Start the animation
    if ([self.keyboardDelegate respondsToSelector:@selector(keyboardViewWillAppear:)])
        [self.keyboardDelegate keyboardViewWillAppear:self];
    [UIView animateWithDuration:animDuration delay:0.0f
                        options:options
                     animations:^(void){
                         self.frame = selfEndingRect;
                         self.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         self.frame = selfEndingRect;
                         self.alpha = 1.0f;
                         if ([self.keyboardDelegate respondsToSelector:@selector(keyboardViewDidAppear:)])
                             [self.keyboardDelegate keyboardViewDidAppear:self];
                     }];
    
}


- (void) keyboardWillDisappear:(NSNotification*)notification{
    
    //Start animation ONLY if the view will not rotate
    if (!self.isRotating){
        
        //Get begin, ending rect and animation duration
        CGRect beginRect = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
        CGRect endRect = [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
        CGFloat animDuration = [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
        
        //Transform rects to local coordinates
        beginRect = [self fixedKeyboardRect:beginRect];
        endRect = [self fixedKeyboardRect:endRect];
        
        //Get this view begin and end rect
        CGRect selfBeginRect = CGRectMake(beginRect.origin.x,
                                          beginRect.origin.y - self.frame.size.height,
                                          beginRect.size.width,
                                          self.frame.size.height);
        CGRect selfEndingRect = CGRectMake(endRect.origin.x,
                                           endRect.origin.y - self.frame.size.height,
                                           endRect.size.width,
                                           self.frame.size.height);
        
        //Set view position and hidden
        self.frame = selfBeginRect;
        self.alpha = 1.0f;
        
        
        //Animation options
        UIViewAnimationOptions options = UIViewAnimationOptionAllowAnimatedContent;
        
        //Animate view
        if ([self.keyboardDelegate respondsToSelector:@selector(keyboardViewWillDisappear:)])
            [self.keyboardDelegate keyboardViewWillDisappear:self];
        [UIView animateWithDuration:animDuration delay:0.0f
                            options:options
                         animations:^(void){
                             self.frame = selfEndingRect;
                             self.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             self.frame = selfEndingRect;
                             self.alpha = 0.0f;
                             [self setHidden:YES];
                             if ([self.keyboardDelegate respondsToSelector:@selector(keyboardViewDidDisappear:)])
                                 [self.keyboardDelegate keyboardViewDidDisappear:self];
                         }];
    }
}











#pragma mark - Custom rotation notification methods
- (void) viewControllerWillRotate:(NSNotification*)notification{
    //Is rotating
    self.isRotating = YES;
}


- (void) viewControllerDidRotate:(NSNotification*)notification{
    //Is not rotating
    self.isRotating = NO;
}









#pragma mark - Private methods
- (CGRect) fixedKeyboardRect:(CGRect)originalRect{
    
    //Get the UIWindow by going through the superviews
    UIView * referenceView = self.superview;
    while ((referenceView != nil) && ![referenceView isKindOfClass:[UIWindow class]]){
        referenceView = referenceView.superview;
    }
    
    //If we finally got a UIWindow
    CGRect newRect = originalRect;
    if ([referenceView isKindOfClass:[UIWindow class]]){
        //Convert the received rect using the window
        UIWindow * myWindow = (UIWindow*)referenceView;
        newRect = [myWindow convertRect:originalRect toView:self.superview];
    }
    
    //Return the new rect (or the original if we couldn't find the Window -> this should never happen if the view is present)
    return newRect;
}



@end
