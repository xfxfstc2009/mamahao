//
//  MMHTabBarController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "MMHFloatingViewController.h"

@interface MMHTabBarController : UITabBarController

- (void)presentFloatingViewController:(MMHFloatingViewController *)viewController animated:(BOOL)animated;
- (void)dismissFloatingViewControllerAnimated:(BOOL)animated completion:(void(^)())completion;

#pragma mark redirection
+(void)redirectToHome;
+(void)redirectToHomeWithAnimation;
+(void)redirectToHomeWithController:(UIViewController *)controller;

+(void)redirectToCart;

+(void)redirectToCenter;
+(void)redirectToCenterWithController:(UIViewController *)controller;
@end
