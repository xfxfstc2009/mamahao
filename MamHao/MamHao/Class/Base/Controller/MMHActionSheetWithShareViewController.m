//
//  MMHActionSheetWithShareViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHActionSheetWithShareViewController.h"
#import "UIImage+ImageEffects.h"
#import "MMHShareSDKMethod.h"                                       // 分享


#define SheetViewHeight       mmh_relative_float(128 + 49)

@interface MMHActionSheetWithShareViewController(){

}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)NSArray *shareButtonArray;
@end

@implementation MMHActionSheetWithShareViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];                  // 创建dismissButton
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
    
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
//    self.shareButtonArray = @[@[@"微博",@"share_weibo",@"weibo"],@[@"微信",@"share_wechat",@"wechat"],@[@"朋友圈",@"share_friendsCircle",@"friendsCircle"],@[@"QQ",@"share_qq",@"QQ"],@[@"QQ空间",@"share_qzone",@"QQZone"],@[@"联系人",@"share_message",@"message"],@[@"复制链接",@"share_copy",@"copy"]];

    self.shareButtonArray = @[@[@"微信",@"share_wechat",@"wechat"],@[@"朋友圈",@"share_friendsCircle",@"friendsCircle"],@[@"短信",@"share_message",@"message"],@[@"复制链接",@"share_copy",@"copy"]];
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:246/256. green:246/256. blue:246/256. alpha:.9];
    [self.view addSubview:_shareView];
    
    // 创建图标
    [self createShareButton];
}

#pragma mark 创建分享图标
-(void)createShareButton{
    // 1. 计算左边距
    CGFloat margin = MMHFloat(30);
    // 2. 加载button
    for (int i = 0 ; i < self.shareButtonArray.count;i++){
        UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [shareButton setImage:[UIImage imageNamed:[[self.shareButtonArray objectAtIndex:i] objectAtIndex:1]] forState:UIControlStateNormal];
//        CGFloat origin_X = margin + (i % 4) * (MMHFloat(55) + MMHFloat(40));
        CGFloat origin_Y = MMHFloat(30) + (i / 4) * (MMHFloat(20) + MMHFloat(45) + MMHFloat(10));
        CGFloat origin_X = margin + (i % 4) * (MMHFloat(60) + MMHFloat(25));
        shareButton.frame = CGRectMake(origin_X, origin_Y, MMHFloat(60), MMHFloat(45));

        [shareButton addTarget:self action:@selector(shareClick:) forControlEvents:UIControlEventTouchUpInside];
        shareButton.stringTag = [[self.shareButtonArray objectAtIndex:i]objectAtIndex:2];
        [shareButton setTitle:[[self.shareButtonArray objectAtIndex:i] objectAtIndex:0] forState:UIControlStateNormal];
        [shareButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        shareButton.titleLabel.font = F3;
        shareButton.imageEdgeInsets = UIEdgeInsetsMake(0, MMHFloat(7.5), 0, MMHFloat(7.5));
        [shareButton setTitleColor:C5 forState:UIControlStateNormal];
        shareButton.backgroundColor = [UIColor clearColor];
        if ([shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:0] objectAtIndex:2]] ||[shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:2] objectAtIndex:2]]){
            shareButton.titleEdgeInsets = UIEdgeInsetsMake(60, -45 , 0, 0);
        } else if ([shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:3] objectAtIndex:2]] || [shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:3] objectAtIndex:2]]){
            shareButton.titleEdgeInsets = UIEdgeInsetsMake(60, -47 , 0, 0);
        }
        
        
//        }  else if ([shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:2] objectAtIndex:2]] || [shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:5] objectAtIndex:2]]){
//            shareButton.titleEdgeInsets = UIEdgeInsetsMake(60, -44, 0, 0);
//        } else if ([shareButton.stringTag isEqualToString:[[self.shareButtonArray objectAtIndex:3] objectAtIndex:2]]){
//            shareButton.titleEdgeInsets = UIEdgeInsetsMake(60, -44, 0, 0);
//        }
        [_shareView addSubview:shareButton];
    }
    // 3. 小修改
    // weibo
    UIButton *weiboButton = (UIButton *)[_shareView viewWithStringTag:[[self.shareButtonArray objectAtIndex:0] objectAtIndex:2]];
    weiboButton.titleEdgeInsets = UIEdgeInsetsMake(60, -45, 0, 0);
    
    // wechat
    UIButton *wechatButton = (UIButton *)[_shareView viewWithStringTag:[[self.shareButtonArray objectAtIndex:1] objectAtIndex:2]];
    wechatButton.titleEdgeInsets = UIEdgeInsetsMake(60, -45, 0, 0);
}



#pragma mark 创建dismissButton
-(void)createDismissButton{
    // Dismiss_Button
    UIButton *determineButton = [UIButton buttonWithType:UIButtonTypeCustom];
    determineButton.frame = CGRectMake(0, _shareView.frame.size.height - MMHFloat(49), _shareView.frame.size.width, MMHFloat(49));
    [determineButton setTitle:@"确定" forState:UIControlStateNormal];
    determineButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(18.)];
    [determineButton addTarget:self action:@selector(sheetViewDismiss) forControlEvents:UIControlEventTouchUpInside];
    determineButton.backgroundColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:.8];
    [determineButton setTitleColor:[UIColor hexChangeFloat:@"666666"] forState:UIControlStateNormal];
    [_shareView addSubview:determineButton];
    
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

-(void)shareClick:(UIButton *)sender{
    UIButton *shareButton = (UIButton *)sender;
    MMHShareSDKMethod *shareSDKMethod = [[MMHShareSDKMethod alloc]init];
    if ([shareButton.stringTag isEqualToString:@"weibo"]){                  // 微博
        [shareSDKMethod shareToSinaWeiboWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"wechat"]){          // 微信
        [shareSDKMethod shareNewsWithWechatWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"friendsCircle"]){   // 朋友圈
        [shareSDKMethod shareAppContentWithFriendsCricleWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"QQ"]){              // QQ
        [shareSDKMethod shareNewsMessageWithQQWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"QQZone"]){          // QQ空间
        [shareSDKMethod shareToQQSpaceWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"copy"]){            // 复制
        [shareSDKMethod copyShareContentWithModel:self.transferShareModel];
    } else if ([shareButton.stringTag isEqualToString:@"message"]){         // 消息
        [shareSDKMethod shareBySMSWithModel:self.transferShareModel];
    }
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak MMHActionSheetWithShareViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak MMHActionSheetWithShareViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

@end
