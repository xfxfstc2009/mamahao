//
//  MMHAliPayHandle.m
//  MamHao
//
//  Created by SmartMin on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAliPayHandle.h"
#import <AlipaySDK/AlipaySDK.h>
#import "DataSigner.h"
#import "MMHNetworkEngine.h"


@implementation MMHAliPayHandle

#pragma mark - go pay
+ (void)goPayWithOrder:(MMHPayOrderModel *)payOrder{
    NSString *fileMessage = @"";
    static NSString *privateKey = @"MIICXQIBAAKBgQCnV7/fVzYxNCBDYvFAMVdnmXkru5F0QpZqRVpTvyOG0aa439x5FaVAChBzRxxxSHAiIv17QzUFsDEPPljESqlx2wdSuzhkIVwlLU+0LyBVw0wA4Sovk0omjNyryC1t5RvjrNG61fRPPFjXvTu6iHp3TrHkTRnbBvEdabw1RrJewwIDAQABAoGBAIg4yUtgrg+ttBnG4EZ2JAWEjHvKK2Stk84ceKQ1sxDbd1GFge7bbPLNfoiouYGKCyXuv9NoaTAhNj6HbTEU6gsg1YryZX0m2HWdZCTly1wEIfRg1JzXJeS00KqOgovrAokyqwM8wlTg6nqfn4npNitVjM+8QoJIN9e0qXTRhOfZAkEA0ZFLpugowNf+vSD3TD+iFM6OidtpWPvx/ed4h/ragEvIEK8gaC3PyCRvhDbJMhp2Rmj/hzzkUXRDMFsmwLEudQJBAMxrdRjP+Ptr3zL1YP+Sc8HZAY+RAYyQb6eLQ8V/Qo3X7vBYOR/EStEm1Yi8zY42Bfnr/xGfQsXP2WayPKq0oVcCQQDP0LJhkCUhns+ZA0DYlt2GnpKQuEjOmgEN1OUcizD0OrHpgZC0XoGGqkL/rEh16/HPvr1ugX60G4OpRyq1uL4lAkBJC1TKSvZTENaupNPGPxSx/dL68/uzuKDNI4xR9AlaGgC9TSkMWt3JIrFGrQBegptcFKIOL/RyLrUmZyYacXT9AkA7XKiTEg1iVeQdqt/rr7blx4SLRvVI6Sqqq1nO1QIs8D0CAM/BfDInu1QkLDzdY2hDco6kpXuFMgt+Pgy5RD/4";
    static NSString *partner = @"2088811103151315";
    static NSString *seller = @"laia111222@163.com";
    static NSString *mmhCallbackURL =  @"http://101.68.78.46:8091/gd-app-api/alipay/alipayNotify.do"; //回调URL
    payOrder.notifyURL = mmhCallbackURL;
    if ([[MMHNetworkEngine sharedEngine].baseURL.host isEqualToString:@"api.mamahao.com"]) {
        payOrder.notifyURL = @"http://api.mamahao.com/alipay/alipayNotify.do";
    }
    payOrder.partner = partner;
    payOrder.seller = seller;
    payOrder.service = @"mobile.securitypay.pay";
    payOrder.paymentType = @"1";
    payOrder.inputCharset = @"utf-8";
    payOrder.itBPay = @"30m";
    payOrder.showUrl = @"m.alipay.com";
    
    if ((!partner.length) || (!seller.length) || (!privateKey.length)){
        fileMessage = @"主要参数缺省";
    }
    
    if (!payOrder.tradeNO){
        fileMessage = @"没有单号";
    } else if (!payOrder.productName){
        fileMessage = @"没有商品名称";
    } else if (!payOrder.productDescription){
        fileMessage = @"没有商品描述";
    } else if (!payOrder.amount){
        fileMessage = @"没有商品价格";
    }
    
    if (fileMessage.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:fileMessage buttonTitles:@[@"确定"] callback:nil]show];
    }
    
    NSString *appScheme = @"mamhao-alipay";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [payOrder description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"",
                       orderSpec, signedString, @"RSA"];
        
        /*
         9000 订单支付成功
         8000 正在处理中
         4000 订单支付失败
         6001 用户中途取消
         6002 网络连接出错
         */
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
//            NSString *numberOfNotification = [resultDic objectForKey:@"resultStatus"];
            [[NSNotificationCenter defaultCenter]postNotificationName:MMHAlipayNotification object:nil userInfo:resultDic];
        }];
    }
}


@end
