//
//  AbstractViewController+Extensions.swift
//  MamHao
//
//  Created by Louis Zhu on 15/5/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


extension AbstractViewController {
    
    @objc
    enum MMHBlankViewType: Int {
        case Cart
        case Comment
        case Address
        case ProductList
        case ProductDetail
        case OrderList
        case BeanList
        case Favorite
        case Coupon
        case Integral
        case parameter
        case photo
        
        func image() -> UIImage? {
            switch self {
            case .Cart:
                return UIImage(named: "center_picture_cart")
            case .Comment:
                return UIImage(named: "center_picture_write")
            case .Address:
                return UIImage(named: "center_picture_location")
            case .ProductList:
                return UIImage(named: "center_picture_search")
            case .ProductDetail:
                return UIImage(named: "center_picture_box")
            case .OrderList:
                return UIImage(named: "center_picture_orders")
            case .BeanList:
                return UIImage(named: "center_picture_madou")
            case .Favorite:
                return UIImage(named: "center_picture_heart")
            case .Coupon:
                return UIImage(named: "center_picture_coupons")
            case .Integral:
                return UIImage(named: "center_picture_integral")
            case .parameter:
                return UIImage(named: "center_picture_parameter")
            case .photo:
                return UIImage(named: "center_picture_photo")
            }
        }
        
        func tips() -> String {
            switch self {
            case .Cart:
                return "您还没有为您可爱的宝宝挑选商品哦 随便逛逛吧"
            case .Comment:
                return "商品暂无评论哦~"
            case .Address:
                return "您还没有添加收货地址哦~"
            case .ProductList:
                return "抱歉，没有找到符合条件的商品~"
            case .ProductDetail:
                return "这件商品卖完了哦~"
            case .OrderList:
                return "您还没有任何订单哦~"
            case .BeanList:
                return "您还没有获得妈豆哦~"
            case .Favorite:
                return "您还没有收藏任何商品哦~"
            case .Coupon:
                return "您还没有任何优惠券哦~"
            case .Integral:
                return "暂时没有积分哦~"
            case .parameter:
                return "暂时没有商品参数哦~"
            case .photo:
                return "暂时没有图文详情哦~"
            }
        }
        
        func buttonTitle() -> String? {
            switch self {
            case .Cart:
                return "随便逛逛"
//            case .Address:
//                return "添加地址"
            case .Favorite:
                return "随便逛逛"
            case .Coupon:
                return "领优惠券"
            default:
                return nil
            }
        }
    }

}


extension AbstractViewController {
    
    
    func showBlankViewOfType(type: MMHBlankViewType, belowView viewToBelow: UIView? = nil) {
        self.blankView?.removeFromSuperview()
        self.blankView = nil
        
        let blankView = UIView(frame: self.view.bounds)
        blankView.backgroundColor = UIColor.whiteColor()
        if viewToBelow != nil && viewToBelow?.superview == self.view {
            self.view.insertSubview(blankView, belowSubview: viewToBelow!)
        }
        else {
            self.view.addSubview(blankView)
        }
        self.blankView = blankView
        
        var imageView = UIImageView(frame: MMHRectMake(0, 180, 125, 125))
        imageView.setCenterX(CGRectGetMidX(self.view.bounds))
        imageView.makeRoundedRectangleShape()
        imageView.backgroundColor = UIColor(hexString: "cccccc")
        imageView.contentMode = .Center
        imageView.image = type.image()
        blankView.addSubview(imageView)

        var label = UILabel(frame: CGRectMake(0, CGRectGetMaxY(imageView.frame) + MMHFloat(21), blankView.width(), 0))
        label.textColor = UIColor(hexString: "9d9d9d")
        label.font = MMHFontOfSize(13)
        label.textAlignment = .Center
        label.setText(type.tips(), constrainedToLineCount: 0)
        blankView.addSubview(label)
        
        if let buttonTitle = type.buttonTitle() {
            var button = UIButton(frame: MMHRectMake(0, 0, 100, 35))
            button.setCenterX(CGRectGetMidX(blankView.bounds))
            button.attachToBottomSideOfView(label, byDistance: MMHFloat(24))
            button.setBorderColor(UIColor(hexString: "fc697c"), cornerRadius: MMHFloat(5))
//            button.setBackgroundImage(UIImage.patternImageWithColor(UIColor.whiteColor()), forState: .Normal)
            button.backgroundColor = UIColor.whiteColor();
            button.setTitleColor(UIColor(hexString: "fc697c"), forState: .Normal)
            button.titleLabel?.font = MMHFontOfSize(18)
            button.setTitle(buttonTitle, forState: .Normal)
            button.addTarget(self, action: Selector("blankViewButtonTapped"), forControlEvents: .TouchUpInside)
            blankView.addSubview(button)
        }
        
        if type == .ProductDetail {
            let arrowImageView = UIImageView(image: UIImage(named: "center_picture_arrow"))
            arrowImageView.attachToBottomSideOfView(label, byDistance: MMHFloat(11))
            arrowImageView.setCenterX(CGRectGetMidX(blankView.bounds))
            blankView.addSubview(arrowImageView)
        }
    }
    
    
    func hideBlankView() {
        self.blankView?.removeFromSuperview()
        self.blankView = nil
    }
    
}
