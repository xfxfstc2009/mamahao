//
//  MMHUploadImageManager.m
//  MamHao
//
//  Created by SmartMin on 15/7/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUploadImageManager.h"
#import "MMHNetworkAdapter+Image.h"
@implementation MMHUploadImageManager

+ (void)uploadImageArrWithImageArr:(NSArray *)imageArray callback:(void(^)(BOOL isSuccess,NSArray *fileNameArr))callbackBlock{
    NSMutableArray *fileImageNameArr = [NSMutableArray array];
    for (int i = 0 ; i <imageArray.count;i++){
        UIImage *uploadImage = [imageArray objectAtIndex:i];
        [[MMHNetworkAdapter sharedAdapter] uploadRefundImage:uploadImage from:nil succeededHandler:^(NSString *fileID) {
            [fileImageNameArr addObject:fileID];
            if (imageArray.count == fileImageNameArr.count){
                callbackBlock(YES,fileImageNameArr);
            }
        } failedHandler:^(NSError *error) {
            callbackBlock(NO,fileImageNameArr);
        }];
    }
}
+ (void)uploadCommentImageArrWithImageArr:(NSArray *)imageArray callback:(void(^)(BOOL isSuccess,NSArray *fileNameArr))callbackBlock{
    NSMutableArray *fileImageNameArr = [NSMutableArray array];
    for (int i = 0 ; i <imageArray.count;i++){
        UIImage *uploadImage = [imageArray objectAtIndex:i];
        [[MMHNetworkAdapter sharedAdapter] uploadCommentImage:uploadImage from:nil succeededHandler:^(NSString *fileID) {
            [fileImageNameArr addObject:fileID];
            if (imageArray.count == fileImageNameArr.count){
                callbackBlock(YES,fileImageNameArr);
            }

        } failedHandler:^(NSError *error) {
            callbackBlock(NO,fileImageNameArr);
        }];

    }
}


@end
