//
//  MMHNavigationController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNavigationController.h"
#import "AbstractViewController.h"
#import "MMHFloatingViewController.h"


@implementation MMHNavigationController

-(void)dealloc{
    NSLog(@"shifang l ");
}

- (CGSize)sizeWhileFloating
{
    if ([self.viewControllers count] != 0) {
        MMHFloatingViewController *rootViewController = (MMHFloatingViewController *)([self.viewControllers firstObject]);
        if ([rootViewController respondsToSelector:@selector(sizeWhileFloating)]) {
            CGSize size = [rootViewController sizeWhileFloating];
            return size;
        }
    }

    return [[UIScreen mainScreen] applicationFrame].size;
}


- (MMHFloatingViewControllerPresentDirection)presentDirection
{
    if ([self.viewControllers count] != 0) {
        MMHFloatingViewController *rootViewController = (MMHFloatingViewController *)([self.viewControllers firstObject]);
        if ([rootViewController respondsToSelector:@selector(presentDirection)]) {
            return [rootViewController presentDirection];
        }
    }

    return MMHFloatingViewControllerPresentDirectionUp;
}


- (void)setSettledViewController:(MMHTabBarController *)settledViewController
{
    if ([self.viewControllers count] != 0) {
        MMHFloatingViewController *rootViewController = (MMHFloatingViewController *)([self.viewControllers firstObject]);
        if ([rootViewController respondsToSelector:@selector(setSettledViewController:)]) {
            [rootViewController setSettledViewController:settledViewController];
        }
    }
}


- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController isKindOfClass:[AbstractViewController class]]) {
        AbstractViewController *abstractViewController = (AbstractViewController *)viewController;
        if (self.viewControllers.count == 0) {
            abstractViewController.hidesBottomBarWhenPushed = NO;
        }
        else {
            abstractViewController.hidesBottomBarWhenPushed = YES;
        }
    }
    [super pushViewController:viewController animated:animated];
}


@end
