//
//  MMHDistanceWithLocation.m
//  MamHao
//
//  Created by SmartMin on 15/6/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHDistanceWithLocation.h"

@implementation MMHDistanceWithLocation

#pragma mark 计算当前经纬度距离商店的距离
+(NSInteger)distanceWithLocationWithShopLat:(CGFloat)lat lng:(CGFloat)lng{
    static double earth_randius = 6378137;
    CGFloat r = M_PI / 180.;
    double radLat1 = r * lat;
    double radLat2 = r * [MMHCurrentLocationModel sharedLocation].lat;
    
    double radLon1 = r * lng;
    double radLon2 = r * [MMHCurrentLocationModel sharedLocation].lng;
    
    if (radLat1 < 0)
        radLat1 = M_PI / 2 + ABS(radLat1);// south
    if (radLat1 > 0)
        radLat1 = M_PI / 2 - ABS(radLat1);// north
    if (radLon1 < 0)
        radLon1 = M_PI * 2 - ABS(radLon1);// west
    if (radLat2 < 0)
        radLat2 = M_PI / 2 + ABS(radLat2);// south
    if (radLat2 > 0)
        radLat2 = M_PI / 2 - ABS(radLat2);// north
    if (radLon2 < 0)
        radLon2 = M_PI * 2 - ABS(radLon2);// west
    double x1 = earth_randius * cos(radLon1) * sin(radLat1);
    double y1 = earth_randius * sin(radLon1) * sin(radLat1);
    double z1 = earth_randius * cos(radLat1);
    
    double x2 = earth_randius * cos(radLon2) * sin(radLat2);
    double y2 = earth_randius * sin(radLon2) * sin(radLat2);
    double z2 = earth_randius * cos(radLat2);
    
    double d = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)+ (z1 - z2) * (z1 - z2));
    //余弦定理求夹角
    double theta = acos((earth_randius * earth_randius + earth_randius * earth_randius - d * d) / (2 * earth_randius * earth_randius));
    double dist = theta * earth_randius;
    return dist;
}



@end
