//
//  MMHLocationManager.h
//  MamHao
//
//  Created by SmartMin on 15/6/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <AMapSearchKit/AMapSearchAPI.h>
#import "MMHCurrentLocationModel.h"

typedef void(^currentAddressBlock)(AMapAddressComponent *addressComponent);
typedef void(^currentLocationBlock)(NSString *lat,NSString *lon);

@interface MMHLocationManager : NSObject

+ (MMHLocationManager *)sharedLocationManager;
- (void) getLocationCoordinate:(currentAddressBlock) currentAddressBlock ;
+ (void)getArea;                         // 获取当前地址信息 单例
+ (void)getNormalAddress;                // 获取当前默认地址 单例
@end
