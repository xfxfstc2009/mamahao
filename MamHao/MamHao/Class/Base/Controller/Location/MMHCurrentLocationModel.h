//
//  MMHCurrentLocationModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreLocation;


@class MMHAddressSingleModel;


@interface MMHCurrentLocationModel : NSObject

+ (MMHCurrentLocationModel *)sharedLocation;

- (NSString *)streetAddress;
@property (nonatomic,copy)NSString *areaId;
- (CLLocation *)myGeographicalLocation;
- (CLLocation *)gpsLocation;
- (NSString *)gpsAreaID;

- (BOOL)isLocationUserSelected;

// 全局的地址
@property (nonatomic,assign)CGFloat lng;
@property (nonatomic,assign)CGFloat lat;
@property (nonatomic,copy)NSString *province;                   /**< 省名*/
@property (nonatomic,copy)NSString *city;                       /**< 城市*/
@property (nonatomic,copy)NSString *district;                   /**< 上城区*/
@property (nonatomic,copy)NSString *township;                   /**< 街道名称*/
@property (nonatomic,copy)NSString *street;                     /**< 路的名称*/
                                                                 

// 默认的收货地址
@property (nonatomic,copy)NSString *deliveryAddrId;                /**< 收货地址唯一id*/
@property (nonatomic,copy)NSString *receiptAreaId;                 /**< 区域id*/
@property (nonatomic,copy)NSString *receiptProvince;               /**< 省*/
@property (nonatomic,copy)NSString *receiptCity;                   /**< 城市*/
@property (nonatomic,copy)NSString *receiptArea;                   /**< 区*/
@property (nonatomic,assign)CGFloat receiptLng;                    /**< 精度*/
@property (nonatomic,assign)CGFloat receiptLat;                    /**< 维度*/
@property (nonatomic,copy)NSString *receiptGpsAddress;                    /**< 街道*/
@property (nonatomic,copy)NSString *receiptAddressDetail;                   /**< 路*/


// 当前定位地址
@property (nonatomic,copy)NSString *currentAreaId;                     /**< 区域id*/
@property (nonatomic,assign)CGFloat currentLng;                        /**< 经纬度*/
@property (nonatomic,assign)CGFloat currentLat;
@property (nonatomic,copy)NSString *currentProvince;                   /**< 省名*/
@property (nonatomic,copy)NSString *currentCity;                       /**< 城市*/
@property (nonatomic,copy)NSString *currentDistrict;                   /**< 上城区*/
@property (nonatomic,copy)NSString *currentTownship;                   /**< 街道名称*/
@property (nonatomic,copy)NSString *currentStreet;                     /**< 路的名称*/

@property (nonatomic,strong) MMHAddressSingleModel *currentAddressSingleModel;
// user selected address
@property (nonatomic, strong) MMHAddressSingleModel *selectedAddressSingleModel;


-(NSDictionary *)parametersForCurrentLocation;
@end
