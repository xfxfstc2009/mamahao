//
//  MMHGeoCodeAnnotation.m
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGeoCodeAnnotation.h"

@interface MMHGeoCodeAnnotation ()

@property (nonatomic, readwrite, strong) AMapGeocode *geocode;

@end

@implementation MMHGeoCodeAnnotation
@synthesize geocode = _geocode;

#pragma mark - MAAnnotation Protocol

- (NSString *)title
{
    return self.geocode.formattedAddress;
}

- (NSString *)subtitle
{
    return [self.geocode.location description];
}

- (CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake(self.geocode.location.latitude, self.geocode.location.longitude);
}

#pragma mark - Life Cycle

- (id)initWithGeocode:(AMapGeocode *)geocode
{
    if (self = [super init])
    {
        self.geocode = geocode;
    }
    
    return self;
}

@end