//
//  MMHDistanceWithLocation.h
//  MamHao
//
//  Created by SmartMin on 15/6/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMHDistanceWithLocation : NSObject

#pragma mark 根据传入的经纬度获取距离
+(NSInteger)distanceWithLocationWithShopLat:(CGFloat)lat lng:(CGFloat)lng;

@end
