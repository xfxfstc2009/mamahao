//
//  MMHLocationManager.m
//  MamHao
//
//  Created by SmartMin on 15/6/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLocationManager.h"
#import "MMHNetworkAdapter+Order.h"                     //Connection
@interface MMHLocationManager()<AMapSearchDelegate,MKMapViewDelegate,CLLocationManagerDelegate>{
    
}

@property (nonatomic,strong)AMapSearchAPI *search;
@property (nonatomic,strong)MKMapView *mapView;

@property (nonatomic,copy)currentAddressBlock currentAddressBlock;
@property (nonatomic,copy)currentLocationBlock currentLocationBlock;
@property (nonatomic,strong)CLLocationManager *locationManager;
@end

@implementation MMHLocationManager

+ (MMHLocationManager *)sharedLocationManager {
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(void)getLocationCoordinate:(currentAddressBlock)currentAddressBlock{
    self.currentAddressBlock = currentAddressBlock;
    [self startLocation];
}


#pragma mark 初始化search
-(void)initSearch{
    self.search = [[AMapSearchAPI alloc]initWithSearchKey:MMHAMapKey Delegate:self];
}


#pragma mark 逆地理编码请求
-(void)reGeoctionWithLocation:(CLLocation *)currentLocation{
    AMapReGeocodeSearchRequest *request = [[AMapReGeocodeSearchRequest alloc]init];
    request.location = [AMapGeoPoint locationWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
    [self.search AMapReGoecodeSearch:request];
    [self stopLocation];
}

#pragma mark 搜索回调
-(void)searchRequest:(id)request didFailWithError:(NSError *)error{
    NSLog(@"request:%@ ,error :%@",request,error);
}

-(void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response{
    if (self.currentAddressBlock){
        self.currentAddressBlock(response.regeocode.addressComponent);
        [self stopLocation];
    }
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    CLLocation *newLocation = userLocation.location;
    [self initSearch];
    [self reGeoctionWithLocation:newLocation];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    switch (status) {
        case kCLAuthorizationStatusDenied :{
            if ([[MMHTool userDefaultGetWithKey:MMHIsOpenLocation] isEqualToString:@"No"]){
                UIAlertView *tempA = [[UIAlertView alloc]initWithTitle:@"提醒" message:@"请在设置-隐私-定位服务中开启定位功能！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [tempA show];
                [[MMHTool userDefaultGetWithKey:MMHIsOpenLocation] isEqualToString:@"Yes"];
            }
        } break;
        case kCLAuthorizationStatusNotDetermined :
            if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [_locationManager requestAlwaysAuthorization];
                
            }
            break;
        case kCLAuthorizationStatusRestricted:{
            UIAlertView *tempA = [[UIAlertView alloc]initWithTitle:@"提醒" message:@"定位服务无法使用！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [tempA show];
        } default:
            [self.locationManager startUpdatingLocation];
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *currentLocation = [locations lastObject];
    [self initSearch];
    self.locationManager = manager;
    [self reGeoctionWithLocation:currentLocation];
}




#pragma mark - location
-(void)startLocation {
    if (IS_IOS8_LATER){
        _locationManager = [[CLLocationManager alloc]init];
        _locationManager.delegate = self;
        _locationManager.distanceFilter = 200;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    } else {
        if (_mapView) {
            _mapView = nil;
        }
        
        _mapView = [[MKMapView alloc] init];
        _mapView.delegate = self;
        _mapView.showsUserLocation = YES;
    }
}

-(void)stopLocation {
    if (IS_IOS8_LATER){
        _locationManager = nil;
    } else {
        _mapView.showsUserLocation = NO;
        _mapView = nil;
    }
}

- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error {
    [self stopLocation];
}

-(void)requestAlwaysAuthorization{
    NSLog(@"123");
}

+(void)getArea{
    [[MMHLocationManager sharedLocationManager] getLocationCoordinate:^(AMapAddressComponent *addressComponent) {
        // 【acode】
        [MMHCurrentLocationModel sharedLocation].currentAreaId = addressComponent.adcode;
        // 【经纬度】
        [MMHCurrentLocationModel sharedLocation].currentLat = addressComponent.streetNumber.location.latitude;
        [MMHCurrentLocationModel sharedLocation].currentLng = addressComponent.streetNumber.location.longitude;
        [MMHCurrentLocationModel sharedLocation].currentProvince = addressComponent.province;
        [MMHCurrentLocationModel sharedLocation].currentCity = addressComponent.city;
        [MMHCurrentLocationModel sharedLocation].currentDistrict = addressComponent.district;
        [MMHCurrentLocationModel sharedLocation].currentTownship = addressComponent.township;
        [MMHCurrentLocationModel sharedLocation].currentStreet = addressComponent.streetNumber.street;
        [[NSNotificationCenter defaultCenter] postNotificationName:MMHCurrentLocationNotification object:nil];
    }];
}

#pragma mark 获取当前默认地址
+(void)getNormalAddress{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetNormolAddressFrom:nil succeededHandler:^(MMHAddressSingleModel *addressSingleModel) {
        if (!weakSelf){
            return ;
        }
        //
        [MMHCurrentLocationModel sharedLocation].deliveryAddrId = addressSingleModel.deliveryAddrId;           // 区域id
        [MMHCurrentLocationModel sharedLocation].receiptProvince = addressSingleModel.province;
        [MMHCurrentLocationModel sharedLocation].receiptCity = addressSingleModel.city;
        [MMHCurrentLocationModel sharedLocation].receiptArea = addressSingleModel.area;
        [MMHCurrentLocationModel sharedLocation].receiptAreaId = addressSingleModel.areaId;
        [MMHCurrentLocationModel sharedLocation].receiptLat =  addressSingleModel.lat;
        [MMHCurrentLocationModel sharedLocation].receiptLng =  addressSingleModel.lng;
        [MMHCurrentLocationModel sharedLocation].receiptGpsAddress =  addressSingleModel.gpsAddress;
        [MMHCurrentLocationModel sharedLocation].receiptAddressDetail =  addressSingleModel.addrDetail;
        [[NSNotificationCenter defaultCenter]postNotificationName:MMHGetNormalAddressNotification object:nil];
    } failedHandler:^(NSError *error) {
        [MMHCurrentLocationModel sharedLocation].deliveryAddrId = @"-1";
        [[NSNotificationCenter defaultCenter]postNotificationName:MMHGetNormalAddressNotification object:nil];
    }];
}
@end
