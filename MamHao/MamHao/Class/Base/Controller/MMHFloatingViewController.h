//
//  MMHFloatingViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


typedef NS_ENUM(NSInteger, MMHFloatingViewControllerPresentDirection) {
    MMHFloatingViewControllerPresentDirectionUp,
    MMHFloatingViewControllerPresentDirectionLeft,
};


@class MMHTabBarController;


@interface MMHFloatingViewController : AbstractViewController

@property (nonatomic, weak) MMHTabBarController *settledViewController;

- (CGSize)sizeWhileFloating;
- (MMHFloatingViewControllerPresentDirection)presentDirection;

@end
