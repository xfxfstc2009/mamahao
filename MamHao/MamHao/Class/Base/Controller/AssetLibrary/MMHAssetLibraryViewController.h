//
//  MMHAssetLibraryViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 我是相册
#import "AbstractViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>



// Block
typedef void(^completeSelectAssetsBlock)(NSArray *imageArr,NSArray *assetArr);
typedef void(^singleSelectImageBlock)(UIImage *imageOfCropped);

@interface MMHAssetLibraryViewController : AbstractViewController

@property (nonatomic,assign)NSInteger numberOfSelectingAssets;
@property (nonatomic,assign)NSInteger numberOfCouldSelectingAssets;
@property (nonatomic,assign)BOOL isOnlyOneSelectable;
@property (nonatomic,copy)completeSelectAssetsBlock selectAssetsBlock;
@property (nonatomic,copy)singleSelectImageBlock singleSelectImageBlock;




@end
