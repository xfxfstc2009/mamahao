//
//  MMHAssetsCommonCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

#define ASSET_X_SPACE        10.
#define ASSET_Y_SPACE        15.
#define ASSET_X_OUTER_SPACE  7.5
#define ASSET_WH             (kScreenBounds.size.width - 2 * (10 + 7.5)) / 3.



@class MMHAssetsCommonCell;
typedef void(^takePhotoBlock)();
typedef void(^selectionAssetBlock)(ALAsset *,BOOL,MMHAssetsCommonCell *,UIImageView *);

@interface MMHAssetsCommonCell : UITableViewCell

// block
@property (nonatomic,copy)takePhotoBlock takePhotoBlock;
@property (nonatomic,copy)selectionAssetBlock selectionAssetBlock;

//
@property (nonatomic,strong)NSArray *assetsArr;
@property (nonatomic,assign) BOOL isOnlyOneSelectable;              // 判断是否只选择了1张图片
@property (nonatomic,strong)NSMutableArray *hasSelectionArr;        // 相册选择的assets数组
@property (nonatomic,assign)NSUInteger numberOfCouldSelection;      // 相册库能选择的最大的相片数量

// 方法
-(void)actionReloadTextValueWithIndex:(NSUInteger)index;
-(void)actionReloadSelectionStateWithIndex:(NSUInteger)index isSelection:(BOOL)isSelected;






@end
