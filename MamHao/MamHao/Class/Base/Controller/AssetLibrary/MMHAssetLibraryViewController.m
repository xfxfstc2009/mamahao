//
//  MMHAssetLibraryViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
#import <objc/runtime.h>

#import "MMHAssetLibraryViewController.h"
// cell
#import "MMHAssetsCommonCell.h"
#import "UIImage+ImageEffects.h"

#import <CoreLocation/CoreLocation.h>

// stringTag
#define kOriginalImageView      @"originalImageView"
#define kOriginalScrollView     @"originalScrollView"
#define kOriginalMaskImageView  @"originalMaskImageView"
#define kOriginalNaviBar        @"originalNaviBar"
#define kOruginaDismissBtn      @"oruginaDismissBtn"
#define kSelectionLabKey        @"selectionLabKey"
#define kSelectionImageKey      @"selectionOImageKey"



@interface MMHAssetLibraryViewController ()<CLLocationManagerDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
    ALAssetsLibrary *assetLibrary;              /**< assetLibrary*/
    UIButton *rightBtn;                         /**< nav右侧按钮*/
    
    NSMutableArray *assetsArr;                  /**<照片组 */
    NSMutableArray *ablumsArr;                  /**<相册数组 */
    NSMutableArray *selectedAssetsArr;          /**<选择的照片对象数组*/
    NSMutableArray *selectedImagesArr;          /**< 选择的照片image数组*/
    
    NSString *ablumName;                        /**< 相册名*/
    UIImage *blurImage;
    
    
    dispatch_queue_t videoSessionQueue;
}
@property (nonatomic,strong)UITableView *assetsTableView;                   // 相片列表
@property (nonatomic,strong)UITableView *ablumsTbleView;                    // 相册组列表

@end

static char selectionAssetIndex;                    // 选择的相片的索引
static char originalFrameKey;
static char hasSelectionKey;
static char assetOfIndicatorBtnKey;
static char cellOfIndicatorBtnKey;


@implementation MMHAssetLibraryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self acionPrepareDataComponents];
    [self actionPrepareUIComponents];
}

#pragma mark 准备执行数据
-(void)acionPrepareDataComponents{
    // 1. 数组初始化
    [self arrayWithInit];
    assetLibrary = [[ALAssetsLibrary alloc]init];
    if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied){
        [[UIAlertView alertViewWithTitle:@"提示" message:@"请在iPhone的\"设置－隐私－照片\"选项中，允许块店访问你的手机相册" buttonTitles:@[@"确定"] callback:NULL] show];
        return;
    }
    // 2. 查询
    __weak typeof(self) weakSelf = self;
    void (^assetsEnumerationBlock)(ALAssetsGroup *,BOOL *) = ^(ALAssetsGroup *group,BOOL *stop){
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (group){                         // 判断是否拥有组
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];             // 获取所有照片资源
        }
        if (group.numberOfAssets){          // 组内的照片数量
            [strongSelf ->ablumsArr addObject:group];
        }
        if (strongSelf ->ablumsArr.count == 1){
            if (group.numberOfAssets > 0 ){ // 组内的照片数量
                [strongSelf actionEnumerateAssetsWithGroup:group];
                NSUInteger numberOfRow = [strongSelf ->_assetsTableView numberOfRowsInSection:0];
                NSIndexPath *indexPathWithBottom = [NSIndexPath indexPathForRow:numberOfRow - 1 inSection:0];
                // 滚动到最底部
                [strongSelf -> _assetsTableView scrollToRowAtIndexPath:indexPathWithBottom atScrollPosition:UITableViewScrollPositionBottom animated:NO];
            }
        }
    };
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:assetsEnumerationBlock failureBlock:NULL];
    [assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:assetsEnumerationBlock failureBlock:NULL];
}

#pragma mark actionPrepareUIComponents
-(void)actionPrepareUIComponents{
    __weak typeof(self)weakSelf = self;
    UIButton *leftBtn = [self leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
    [leftBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];

    rightBtn = [self rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf.isOnlyOneSelectable){                                            // 【跳转剪切页面】
//            ALAsset *selectAlasset = [strongSelf -> selectedAssetsArr objectAtIndex:0];
//            [[UIAlertView alertViewWithTitle:@"smar" message:nil buttonTitles:@[@"确定"] callback:nil] show];
            [self.view showTips:@"错误"];
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
        } else {
            strongSelf->selectedImagesArr = [NSMutableArray array];
            for (int i = 0 ; i < strongSelf->selectedAssetsArr.count ;i++){
                ALAsset *singleAlasset = [strongSelf ->selectedAssetsArr objectAtIndex:i];
                ALAssetRepresentation* representation = [singleAlasset defaultRepresentation];
                UIImage *singleImage = [UIImage imageWithCGImage:[representation fullScreenImage]];
                [strongSelf->selectedImagesArr addObject: singleImage];
            }
            
            if (weakSelf.selectAssetsBlock){
                strongSelf.selectAssetsBlock(strongSelf->selectedImagesArr,strongSelf->selectedAssetsArr);
            }
            
            [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
    rightBtn.enabled = NO;
    self.barMainTitle = @"添加图片";
    self.barSubTitle = @"相机胶卷";

    // 添加naviBar btn
    UIButton *showAblumsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    showAblumsBtn.frame = self.navigationItem.titleView.bounds;
    [showAblumsBtn setImage:[UIImage imageNamed:@"home_icon_down"] forState:UIControlStateNormal];
    [showAblumsBtn setImage:[UIImage imageNamed:@"home_icon_down"] forState:UIControlStateHighlighted];
    CGFloat verticalInset = (CGRectGetHeight(showAblumsBtn.frame)-8)/2.;
    CGFloat horizontalInset = CGRectGetWidth(showAblumsBtn.frame)-12;
    showAblumsBtn.imageEdgeInsets = UIEdgeInsetsMake(verticalInset, horizontalInset-48, verticalInset, 48);
    [showAblumsBtn addTarget:self action:@selector(actionShowAblums) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationItem.titleView addSubview:showAblumsBtn];
    
    // 添加tableView
    [self assetsTableWithInit];
    [self ablumsTableWithInit];
}



#pragma mark -
#pragma mark - 列举照片组
-(void)actionEnumerateAssetsWithGroup:(ALAssetsGroup *)group{
    if (!assetsArr){
        assetsArr = [NSMutableArray array];
    } else {
        [assetsArr removeAllObjects];
    }
    
    __weak typeof(self)weakSelf = self ;
    ////按顺便遍历获取相册中所有的资源，index代表资源的索引，stop赋值为false时，会停止遍历
    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (result){                                // 设置关联函数
            objc_setAssociatedObject(result, &selectionAssetIndex, [NSNumber numberWithInt:(int)index], OBJC_ASSOCIATION_RETAIN);
            [strongSelf ->assetsArr addObject:result];
        }
    }];
    self.barSubTitle = [group valueForProperty:ALAssetsGroupPropertyName];
    ablumName =self.barSubTitle;
    [_assetsTableView reloadData];                              // 列表更新
    blurImage = [[UIImage screenShoot:self.navigationController.view]applyDarkEffect];

}

#pragma mark -
#pragma mark -Other
#pragma mark 数组初始化
-(void)arrayWithInit{
    ablumsArr = [NSMutableArray array];
    selectedAssetsArr = [NSMutableArray array];
}

#pragma mark 按钮点击事件
// naviBar上的按钮点击
-(void)actionShowAblums{
    if (ablumsArr.count == 0){                      // 手机相册专辑
        [[UIAlertView alertViewWithTitle:nil message:@"当前手机相册为空" buttonTitles:@[@"确定"] callback:nil] show];
        return;
    }
    BOOL hadShowAblumTableView = (CGRectGetHeight(_ablumsTbleView.frame) == CGRectGetHeight(self.view.bounds) - 64);
    UIImageView *blurImageBiew = (UIImageView *)[self.navigationController.view viewWithStringTag:@"blurImageView"];
    if (!blurImageBiew){
        blurImageBiew = [[UIImageView alloc]initWithFrame:[UIScreen mainScreen].bounds];
        blurImageBiew.userInteractionEnabled = YES;
        blurImageBiew.stringTag = @"blurImageView";
        blurImageBiew.image = blurImage;
        
        // 添加手势
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionDismissAblums)];
        [blurImageBiew addGestureRecognizer:gesture];
    }
    if (!hadShowAblumTableView){
        [self.navigationController.view insertSubview:blurImageBiew belowSubview:_ablumsTbleView];
        _ablumsTbleView.alpha = .0;
        [UIView animateWithDuration:.6 animations:^{
            _ablumsTbleView.alpha = 1;
        } completion:nil];
    }
    if (hadShowAblumTableView){
        _ablumsTbleView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds) - 20, 0);
        [UIView animateWithDuration:.5f animations:^{
            _ablumsTbleView.alpha = 0;
            blurImageBiew.alpha = 0;
        } completion:^(BOOL finished) {
            [blurImageBiew removeFromSuperview];
        }];
    } else {
        if ([_ablumsTbleView numberOfRowsInSection:0] == 0){
            [_ablumsTbleView reloadData];
        }
        _ablumsTbleView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds)-20, CGRectGetHeight(self.view.bounds)-64);
    }
}

// 手势
-(void)actionDismissAblums{
    [self actionShowAblums];
}

#pragma mark - UITableView
-(void)assetsTableWithInit{                         // 创建tableView
    if (!_assetsTableView){
        _assetsTableView = [[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _assetsTableView.delegate = self;
        _assetsTableView.dataSource = self;
        _assetsTableView.backgroundColor = [UIColor whiteColor];
        _assetsTableView.contentInset = UIEdgeInsetsMake(15, 0, 15, 0);
        _assetsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _assetsTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:_assetsTableView];
    }
}

-(void)ablumsTableWithInit{                         // 创建专辑组列表
    if (!_ablumsTbleView){
        _ablumsTbleView = [[UITableView alloc] initWithFrame:CGRectMake(10, 64, CGRectGetWidth(self.view.bounds) - 20, 0) style:UITableViewStylePlain];
        _ablumsTbleView.delegate = self;
        _ablumsTbleView.dataSource = self;
        _ablumsTbleView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _ablumsTbleView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        // 切圆角
        _ablumsTbleView.layer.cornerRadius = 5.;
        _ablumsTbleView.layer.masksToBounds = YES;
        _ablumsTbleView.layer.zPosition = MAXFLOAT;
        [self.navigationController.view addSubview:_ablumsTbleView];
    }
}


#pragma mark -UITableViewDateSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == _assetsTableView){
        return [self getAssetsRowsCount];
    } else if (tableView == _ablumsTbleView){
        return ablumsArr.count;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _assetsTableView){
        static NSString *identifierOfCommonRow = @"identifierOfCommonRow";
        MMHAssetsCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierOfCommonRow];
        
        if (!cell){
            cell = [[MMHAssetsCommonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierOfCommonRow];
            cell.backgroundColor = [UIColor whiteColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        __weak typeof(self) weakSelf = self;
        cell.isOnlyOneSelectable = _isOnlyOneSelectable;
        cell.assetsArr = [self getAssetsWithIndexPath:indexPath];
        cell.hasSelectionArr = selectedAssetsArr;
        cell.numberOfCouldSelection = self.numberOfCouldSelectingAssets;
        
        cell.selectionAssetBlock = ^(ALAsset *asset,BOOL showOriginalPicture,MMHAssetsCommonCell *operationCell,UIImageView *operationImageView){
            if (!showOriginalPicture){
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                for (ALAsset *selectedAsset in operationCell.hasSelectionArr){
                    if ([[[selectedAsset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[asset valueForProperty:ALAssetPropertyAssetURL] description]]) {
                        NSUInteger indexOfDelettingAsset = [operationCell.hasSelectionArr indexOfObject:selectedAsset];
                        [operationCell.hasSelectionArr removeObject:selectedAsset];
                        [rightBtn setTitle:[NSString stringWithFormat:@"%lu/%li确定",(unsigned long)operationCell.hasSelectionArr.count,(long)self.numberOfCouldSelectingAssets] forState:UIControlStateNormal];
                        [rightBtn sizeToFit];
                        if (!operationCell.isOnlyOneSelectable){                // 选择多张图片
                            [strongSelf actionReloadIndexValue:indexOfDelettingAsset];
                        }
                        if (operationCell.hasSelectionArr.count == 0){
                            strongSelf ->rightBtn.enabled = NO;
                        }
                        return;
                        break;
                    }
                }
                
                [operationCell.hasSelectionArr addObject:asset];
                [rightBtn setTitle:[NSString stringWithFormat:@"%lu/%li确定",(unsigned long)operationCell.hasSelectionArr.count,(long)self.numberOfCouldSelectingAssets] forState:UIControlStateNormal];
                [rightBtn sizeToFit];
                
                if ((operationCell.hasSelectionArr.count > 0) && !strongSelf ->rightBtn.enabled){
                    strongSelf->rightBtn.enabled = YES;
                }
            } else {
                [weakSelf actionShowOriginalPhotoWithAsset:asset thumailImageView:operationImageView cell:operationCell];
            }
        };
        
        return cell;
    } else if (tableView == _ablumsTbleView){
        static NSString *identifierOfAblumsRow = @"identifierOfAblumsRow";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierOfAblumsRow];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierOfAblumsRow];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            // 创建 image
            UIImageView *posterImageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 6, 50, 50)];
            posterImageView.stringTag = @"posterImageView";
            [cell addSubview:posterImageView];
            
            // 创建相册名称
            UILabel *ablumLabel = [[UILabel alloc]init];
            ablumLabel.stringTag = @"ablumLabel";
            ablumLabel.font = [UIFont systemFontOfSize:15.];
            ablumLabel.backgroundColor = [UIColor clearColor];
            [cell addSubview:ablumLabel];
            
            // 创建副标题《相册照片数量》
            UILabel *numberOfAblumLabel = [[UILabel alloc]init];
            numberOfAblumLabel.stringTag = @"numberOfAblumLabel";
            numberOfAblumLabel.backgroundColor = [UIColor clearColor];
            numberOfAblumLabel.font = [UIFont systemFontOfSize:12.];
            numberOfAblumLabel.textColor = [UIColor hexChangeFloat:@"9d9d9d"];
            [cell addSubview:numberOfAblumLabel];
        }
        // 赋值
        ALAssetsGroup *ablum = (ALAssetsGroup *)[ablumsArr objectAtIndex:indexPath.row];
        
        // image
        UIImageView *posterImageView = (UIImageView *)[cell viewWithStringTag:@"posterImageView"];
        // label
        UILabel *ablumLabel = (UILabel *)[cell viewWithStringTag:@"ablumLabel"];
        // 副标题。
        UILabel *numberOfAblumLabel = (UILabel *)[cell viewWithStringTag:@"numberOfAblumLabel"];
        
        posterImageView.image = [UIImage imageWithCGImage:ablum.posterImage];
        ablumLabel.text = [ablum valueForProperty:ALAssetsGroupPropertyName];
        numberOfAblumLabel.text = [NSString stringWithFormat:@"%ld",(long)[ablum numberOfAssets]];
        return cell;
    }
    return nil;
}

#pragma mark -UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _ablumsTbleView){
        return 62.;
    } else if (tableView == _assetsTableView){
        return (1 * ASSET_Y_SPACE + ASSET_WH);
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == _ablumsTbleView){
        return 40;
    } else {
        return 0.;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == _ablumsTbleView){
        UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_ablumsTbleView.frame), 40)];
        headerLabel.backgroundColor = [UIColor whiteColor];
        headerLabel.font = [UIFont systemFontOfSize:16.];
        headerLabel.text = @"我的相册";
        headerLabel.textAlignment = NSTextAlignmentCenter;
        
        // image
        UIImageView *separatorView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 39.5, CGRectGetWidth(_ablumsTbleView.frame), .5f)];
        separatorView.backgroundColor = [UIColor hexChangeFloat:@"FFFFFF"];
        [headerLabel addSubview:separatorView];
        return headerLabel;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _ablumsTbleView) {
        UIImageView *posterImageView = (UIImageView *)[cell viewWithStringTag:@"posterImageView"];
        UILabel *ablumLab = (UILabel *)[cell viewWithStringTag:@"ablumLabel"];
        UILabel *numberOfAblumLab = (UILabel *)[cell viewWithStringTag:@"numberOfAblumLabel"];
        [ablumLab sizeToFit];
        [numberOfAblumLab sizeToFit];
        
        posterImageView.frame = CGRectMake(12, 6, 50, 50);
        ablumLab.frame = (CGRect){CGPointMake(72, 12),ablumLab.frame.size};
        numberOfAblumLab.frame = (CGRect){CGPointMake(72, 22+CGRectGetHeight(ablumLab.frame)),numberOfAblumLab.frame.size};
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == _ablumsTbleView){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        ALAssetsGroup *group = (ALAssetsGroup *)[ablumsArr objectAtIndex:indexPath.row];
        [self actionEnumerateAssetsWithGroup:group];
        [self actionShowAblums];
        [_assetsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[_assetsTableView numberOfRowsInSection:0]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        
    }
    
}


#pragma mark -Other
#pragma mark 获取相片数量
-(NSInteger)getAssetsRowsCount{                 // 获取多少个相片
    if (!assetsArr){
        return 0;
    }
    
    NSUInteger assetsCount = assetsArr.count;
    return (assetsCount / 3 +(assetsCount % 3 == 0?0:1));
}

- (NSUInteger)getLastRowCountOfAssets{
    NSUInteger assetsCount = assetsArr.count;
//    assetsCount ++;
    return assetsCount%3==0?3:assetsCount%3;
}

- (NSArray *)getAssetsWithIndexPath:(NSIndexPath *)indexPath{
    NSUInteger row = indexPath.row;
    if (assetsArr.count == 0) {
        return nil;
    }
    if (assetsArr.count >= row * 3) {
        if (row < [self getAssetsRowsCount] -1) {
            return [assetsArr subarrayWithRange:NSMakeRange(row*3, 3)];
        } else {
            NSUInteger assetsCount = assetsArr.count;
            return [assetsArr subarrayWithRange:NSMakeRange(row*3, (assetsCount%3==0)?3:assetsCount%3)];
        }
    }
    return nil;
}

#pragma mark -actionReloadIndexValue
-(void)actionReloadIndexValue:(NSUInteger)minIndexValue{
    NSArray *arrofNeedReload = [selectedAssetsArr subarrayWithRange:NSMakeRange(minIndexValue, selectedAssetsArr.count - minIndexValue)];
    for (ALAsset *assetOfReloadIndex in arrofNeedReload){
        NSUInteger index = [objc_getAssociatedObject(assetOfReloadIndex, &selectionAssetIndex) intValue];
        NSUInteger row = index / 3;
        MMHAssetsCommonCell *cellOfRow = (MMHAssetsCommonCell *)[_assetsTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];
        [cellOfRow actionReloadTextValueWithIndex:(index % 3)];
    }
}

#pragma mark 显示原始照片 scrollView 弹出window
-(void)actionShowOriginalPhotoWithAsset:(ALAsset *)asset thumailImageView:(UIImageView *)thumailImageView cell:(MMHAssetsCommonCell *)currentCell{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    if (originalScrollView) {
        return;
    }
    CGRect convertFrame = [currentCell convertRect:thumailImageView.superview.frame toView:self.view.window];
    originalScrollView = [[UIScrollView alloc] initWithFrame:convertFrame];
    originalScrollView.stringTag = kOriginalScrollView;
    originalScrollView.layer.zPosition = MAXFLOAT;
    originalScrollView.delegate = self;
    originalScrollView.layer.masksToBounds = YES;
    originalScrollView.backgroundColor = [UIColor blackColor];
    originalScrollView.showsHorizontalScrollIndicator = NO;
    originalScrollView.showsVerticalScrollIndicator = NO;
    
    UIImageView *originalImageView = [[UIImageView alloc] initWithFrame:originalScrollView.bounds];
    originalImageView.stringTag = kOriginalImageView;
    UIImage *originalImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(320, 320*originalImage.size.height/originalImage.size.width)];
    originalImageView.image = originalImage;
    originalImageView.contentMode = UIViewContentModeScaleAspectFit;
    [originalScrollView addSubview:originalImageView];
    [self.view.window addSubview:originalScrollView];
    
    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        originalScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
    }
    originalScrollView.minimumZoomScale = 1.;
    
    [UIView animateWithDuration:.5 delay:0. options:UIViewAnimationOptionCurveEaseInOut animations:^{
        originalScrollView.frame = self.navigationController.view.bounds;
        originalImageView.frame = self.navigationController.view.bounds;
    } completion:^(BOOL finished) {
        UIImageView *  maskImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.window.frame), 64)];
        maskImageView.stringTag = kOriginalMaskImageView;
        maskImageView.layer.zPosition = originalScrollView.layer.zPosition+0.01;
        maskImageView.backgroundColor = [UIColor clearColor];
        maskImageView.alpha = 0.1;
        [self.view.window addSubview:maskImageView];
        
//        UIImage *naviBackgroundImage = [UIImage imageWithRenderColor:[UIColor clearColor] renderSize:CGSizeMake(10, 10.)];
//        UINavigationBar *naviBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, 320, 44)];
//        naviBar.stringTag = kOriginalNaviBar;
//        [naviBar setBackgroundImage:naviBackgroundImage forBarMetrics:UIBarMetricsDefault];
//        naviBar.layer.zPosition = maskImageView.layer.zPosition + 0.01;
//
        // 添加返回按钮
        UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [dismissBtn sizeToFit];
        dismissBtn.stringTag = kOruginaDismissBtn;
        [dismissBtn addTarget:self action:@selector(actionDismissOriginalPhoto:) forControlEvents:UIControlEventTouchUpInside];
        objc_setAssociatedObject(dismissBtn, &originalFrameKey, [NSValue valueWithCGRect:convertFrame], OBJC_ASSOCIATION_RETAIN);
//        [naviItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:dismissBtn]];
        dismissBtn.frame = self.view.bounds;
        [maskImageView.window addSubview:dismissBtn];
        
        BOOL hasSelectedAsset = NO;
        NSUInteger indexOfSelectionAsset = 0;
        for (ALAsset *selectedAsset in selectedAssetsArr) {
            if ([[[selectedAsset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[asset valueForProperty:ALAssetPropertyAssetURL] description]]) {
                hasSelectedAsset = YES;
                indexOfSelectionAsset = [selectedAssetsArr indexOfObject:selectedAsset];
                break;
            }
        }
        
        UIButton *indicatorBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        indicatorBtn.frame = CGRectMake(0, 0, 44, 44);
        [indicatorBtn setImage:[UIImage imageNamed:hasSelectedAsset?@"address_icon_selected":@"address_icon_untick"] forState:  UIControlStateNormal];
        objc_setAssociatedObject(indicatorBtn, &hasSelectionKey, [NSNumber numberWithBool:hasSelectedAsset], OBJC_ASSOCIATION_RETAIN);
        objc_setAssociatedObject(indicatorBtn, &assetOfIndicatorBtnKey, asset, OBJC_ASSOCIATION_RETAIN);
        objc_setAssociatedObject(indicatorBtn, &cellOfIndicatorBtnKey, currentCell, OBJC_ASSOCIATION_RETAIN);
        indicatorBtn.imageEdgeInsets = UIEdgeInsetsMake(9, 9, 9, 9);
        if (hasSelectedAsset) {
            if (_isOnlyOneSelectable) {
                UIImageView * assetSingleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(9, 9, 26, 26)];
                assetSingleImageView.stringTag = kSelectionImageKey;
                assetSingleImageView.image = [UIImage imageNamed:@"address_icon_selected"];
                [indicatorBtn setImage:nil forState:UIControlStateNormal];
                [indicatorBtn addSubview:assetSingleImageView];
            } else {
                UILabel *indexNumLab = [[UILabel alloc] init];
                indexNumLab.frame = CGRectMake(10, 10, 24, 24);
                indexNumLab.stringTag = kSelectionLabKey;
                indexNumLab.textAlignment = NSTextAlignmentCenter;
                indexNumLab.text = [NSString stringWithFormat:@"%li",(long)indexOfSelectionAsset+1];
                [indicatorBtn addSubview:indexNumLab];
            }
        }
        [indicatorBtn addTarget:self action:@selector(actionOperationSelection:) forControlEvents:UIControlEventTouchUpInside];
//        naviItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:indicatorBtn];
        
//        [naviBar pushNavigationItem:naviItem animated:NO];
        
//        [self.view.window addSubview:naviBar];
    }];
}


#pragma mark -actionOperationSelection
// 进行选择
-(void)actionOperationSelection:(UIButton *)sender{
    BOOL hasSelectedAsset = [objc_getAssociatedObject(sender, &hasSelectionKey)boolValue];
    UILabel *indexNumLabel = (UILabel *)[sender viewWithStringTag:kSelectionLabKey];
    UIImageView *assetSingleImageView = (UIImageView *)[sender viewWithStringTag:kSelectionImageKey];
    ALAsset *assetOfbtn = (ALAsset *)objc_getAssociatedObject(sender, &assetOfIndicatorBtnKey);
    MMHAssetsCommonCell *currentCell = (MMHAssetsCommonCell *)objc_getAssociatedObject(sender, &cellOfIndicatorBtnKey);
    NSUInteger indexOfReload = [objc_getAssociatedObject(assetOfbtn, &selectionAssetIndex)intValue];
    if(hasSelectedAsset){
        for (ALAsset *selectedAsset in selectedAssetsArr){
            if ([[[selectedAsset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[assetOfbtn valueForProperty:ALAssetPropertyAssetURL] description]]) {
                NSUInteger indexOfDeletetingAsset = [selectedAssetsArr indexOfObject:selectedAsset];
                [selectedAssetsArr removeObject:selectedAsset];
                if (!_isOnlyOneSelectable){
                    [self actionReloadIndexValue:indexOfDeletetingAsset];
                }
                break;
            }
        }
        [indexNumLabel removeFromSuperview];
        [assetSingleImageView removeFromSuperview];
        [sender setImage:[UIImage imageNamed:@"unSelectPhoto"] forState:UIControlStateNormal];
        objc_setAssociatedObject(sender, &hasSelectionKey, [NSNumber numberWithBool:NO], OBJC_ASSOCIATION_RETAIN);
        [currentCell actionReloadSelectionStateWithIndex:(indexOfReload % 3) isSelection:NO];
        if (rightBtn.enabled){
            rightBtn.enabled = NO;
        }
    } else {
        if (_isOnlyOneSelectable){
            if (selectedAssetsArr.count == 1){
                return;
            }
        } else {
            if (selectedAssetsArr.count == _numberOfCouldSelectingAssets){
                [self.view showTips:[NSString stringWithFormat:@"只能选择%ld张图片",(long)_numberOfCouldSelectingAssets]];
                return;
            }
        }
        [selectedAssetsArr addObject:assetOfbtn];
        
        if (_isOnlyOneSelectable){
            UIImageView *assetSingleImageView = [[UIImageView alloc]initWithFrame:CGRectMake(9, 9, 26, 26)];
            assetSingleImageView.stringTag = kSelectionImageKey;
            assetSingleImageView.image = [UIImage imageNamed:@"singleSelect"];
            [sender addSubview:assetSingleImageView];
        } else {
            [sender setImage:[UIImage imageNamed:@"selectPhoto"] forState:UIControlStateNormal];
            UILabel *indexNumLabel = [[UILabel alloc]init];
            indexNumLabel.frame = CGRectMake(10, 10, 24, 24);
            indexNumLabel.stringTag = kSelectionLabKey;
            indexNumLabel.textAlignment = NSTextAlignmentCenter;
            indexNumLabel.text = [NSString stringWithFormat:@"%lu",(unsigned long)selectedAssetsArr.count];
            [sender addSubview:indexNumLabel];
        }
        objc_setAssociatedObject(sender, &hasSelectionKey, [NSNumber numberWithBool:YES], OBJC_ASSOCIATION_RETAIN);
        [currentCell actionReloadSelectionStateWithIndex:(indexOfReload % 3) isSelection:YES];
        if (!rightBtn.enabled){
            rightBtn.enabled = YES;
        }
    }
}

#pragma mark navi Dismiss
-(void)actionDismissOriginalPhoto:(UIButton *)sender {
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageView];
    UIImageView *maskImageView = (UIImageView *)[self.view.window viewWithStringTag:kOriginalMaskImageView];
    UIButton *dismissButton = (UIButton *)[maskImageView.window viewWithStringTag:kOruginaDismissBtn];
    UINavigationBar *originalNaviBar = (UINavigationBar *)[self.view.window viewWithStringTag:kOriginalNaviBar];
    CGRect originalFrame = (CGRect)[objc_getAssociatedObject(sender, &originalFrameKey)CGRectValue];
    
    [maskImageView removeFromSuperview];
    [originalNaviBar removeFromSuperview];
    
    [UIView animateWithDuration:.5 animations:^{
        originalScrollView.frame = originalFrame;
        originalScrollView.layer.cornerRadius = 5;
        originalScrollView.alpha = 0.;
        originalImageView.frame = CGRectMake(0, 0, CGRectGetWidth(originalFrame), CGRectGetHeight(originalFrame));
    } completion:^(BOOL finished) {
        [originalScrollView removeFromSuperview];
        [dismissButton removeFromSuperview];
    }];
}



#pragma mark -
#pragma mark -UIScrollViewDelegate
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    UIImageView *originalImageView = (UIImageView *)[scrollView viewWithStringTag:kOriginalImageView];
    if (originalImageView){
        return originalImageView;
    } else {
        return nil;
    }
}

#pragma mark -UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    __weak typeof(self) weakSelf = self;
    [picker dismissViewControllerAnimated:YES completion:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf  = weakSelf;
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        [strongSelf->assetLibrary writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error) {
            [strongSelf->assetLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
                if (asset) {
                    NSUInteger index = 0;
                    ALAsset *lastAsset = (ALAsset *)[strongSelf->assetsArr lastObject];
                    if (lastAsset) {
                        index = [objc_getAssociatedObject(lastAsset, &selectionAssetIndex) intValue];
                        index ++;
                    }
                    objc_setAssociatedObject(asset, &selectionAssetIndex, [NSNumber numberWithInt:(int)index], OBJC_ASSOCIATION_RETAIN);
                    
                    BOOL necessaryOfInsertting = strongSelf->assetsArr.count%3==2?YES:NO;
                    [strongSelf->selectedAssetsArr addObject:asset];
                    [strongSelf->assetsArr addObject:asset];
                    if (necessaryOfInsertting) {
                        [strongSelf->_assetsTableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[strongSelf getAssetsRowsCount]-1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        if ([strongSelf getAssetsRowsCount]-2 > 0) {
                            [strongSelf->_assetsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[strongSelf getAssetsRowsCount]-2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                            [strongSelf->_assetsTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[strongSelf getAssetsRowsCount]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                        }
                    } else {
                        [strongSelf->_assetsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:[strongSelf getAssetsRowsCount]-1 inSection:0]]  withRowAnimation:UITableViewRowAnimationNone];
                    }
                    if (!strongSelf->rightBtn.enabled) {
                        strongSelf->rightBtn.enabled = YES;
                    }
                    
                    if (strongSelf->ablumsArr.count == 0) {
                        void (^assetsEnumerationBlock)(ALAssetsGroup *,BOOL *) = ^(ALAssetsGroup *group,BOOL *stop) {
                            if (!weakSelf) {
                                return ;
                            }
                            __strong typeof(weakSelf) strongSelf = weakSelf;
                            if (group) {
                                [group setAssetsFilter:[ALAssetsFilter allPhotos]];
                            }
                            if (group.numberOfAssets > 0) {
                                [strongSelf->ablumsArr addObject:group];
                            }
                        };
                        [strongSelf->assetLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:assetsEnumerationBlock failureBlock:NULL];
                        [strongSelf->assetLibrary enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                                                usingBlock:assetsEnumerationBlock
                                                              failureBlock:NULL];
                        strongSelf->blurImage = [[UIImage screenShoot:strongSelf.navigationController.view]
                                                 applyDarkEffect];
                    }
                }
            } failureBlock:^(NSError *error) {
                
            }];
        }];
    }];
}


#pragma mark -计算文字高度
-(CGFloat)contentOfSizeWithString:(NSString *)content andTextFont:(UIFont *)font andTextWidth:(CGFloat)textWidth{
    CGSize contentSize = [content sizeWithCalcFont:font constrainedToSize:CGSizeMake(textWidth, CGFLOAT_MAX)];
    return contentSize.height;
}
@end
