//
//  MMHAssetsCommonCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAssetsCommonCell.h"
#import <objc/runtime.h>

#define kSELECTEDBACKGROUND [UIColor colorWithRed:1 green:1 blue:1 alpha:.4f]

@interface MMHAssetsCommonCell(){
    UIImageView *assetOneImageView;
    UIImageView *assetTwoImageView;
    UIImageView *assetThrImageView;
    
    UIImageView *assetMaskOneImageView;
    UIImageView *assetMaskTwoImageView;
    UIImageView *assetMaskThrImageView;
    
    UIImageView *assetSingleOneImageView;
    UIImageView *assetSingleTwoImageView;
    UIImageView *assetSingleThrImageView;
    
    UILabel *assetMultiOneLab;
    UILabel *assetMultiTwoLab;
    UILabel *assetMultiThrLab;
    
    BOOL assetOneCollection;
    BOOL assetTwoCollection;
    BOOL assetThrCollection;
}

@end
//address_icon_selected
// address_icon_untick

static char *assetHasLoaded;

@implementation MMHAssetsCommonCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 【 one 】
        // 相册图片
        assetOneImageView = [[UIImageView alloc] init];
        assetOneImageView.frame = CGRectMake(ASSET_X_OUTER_SPACE, ASSET_Y_SPACE, ASSET_WH, ASSET_WH);
        [self addSubview:assetOneImageView];
        
        // alpha
        assetMaskOneImageView = [[UIImageView alloc] initWithFrame:assetOneImageView.bounds];
        [assetOneImageView addSubview:assetMaskOneImageView];
        
        // sign
        assetSingleOneImageView = [[UIImageView alloc] initWithFrame:CGRectMake(assetOneImageView.bounds.size.width - MMHFloat(25),MMHFloat(5), MMHFloat(20), MMHFloat(20))];
        [assetMaskOneImageView addSubview:assetSingleOneImageView];
        
        
        // 【two】
        // 相册图片
        assetTwoImageView = [[UIImageView alloc] init];
        assetTwoImageView.frame = CGRectMake(ASSET_X_OUTER_SPACE+ASSET_X_SPACE+ASSET_WH, ASSET_Y_SPACE, ASSET_WH, ASSET_WH);
        [self addSubview:assetTwoImageView];
        
        // alpha
        assetMaskTwoImageView = [[UIImageView alloc] initWithFrame:assetTwoImageView.bounds];
        [assetTwoImageView addSubview:assetMaskTwoImageView];
        
        // sign
        assetSingleTwoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(assetTwoImageView.bounds.size.width - MMHFloat(25), MMHFloat(5), MMHFloat(20), MMHFloat(20))];
        [assetMaskTwoImageView addSubview:assetSingleTwoImageView];
        
        
        // 【three】
        // 相册图片
        assetThrImageView = [[UIImageView alloc] init];
        assetThrImageView.frame = CGRectMake(ASSET_X_OUTER_SPACE+(ASSET_X_SPACE+ASSET_WH)*2, ASSET_Y_SPACE, ASSET_WH, ASSET_WH);
        [self addSubview:assetThrImageView];
        
        // alpha
        assetMaskThrImageView = [[UIImageView alloc] initWithFrame:assetThrImageView.bounds];
        [assetThrImageView addSubview:assetMaskThrImageView];
        
        // sign
        assetSingleThrImageView = [[UIImageView alloc] initWithFrame:CGRectMake(assetThrImageView.bounds.size.width - MMHFloat(25), MMHFloat(5), MMHFloat(20), MMHFloat(20))];
        [assetMaskThrImageView addSubview:assetSingleThrImageView];
        
        
        assetOneImageView.layer.zPosition = MAXFLOAT;
        assetTwoImageView.layer.zPosition = MAXFLOAT;
        assetThrImageView.layer.zPosition = MAXFLOAT;
    }
    return self;
}


#pragma mark -override
// 手势
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    UITableView *superView;
    if (IS_IOS7_LATER) {
        superView = (UITableView *)self.superview.superview;
    } else {
        superView = (UITableView *)self.superview;
    }
    if (superView.decelerating || superView.isDragging) {
        return ;
    }
    
    CGPoint locationPoint = [touches.anyObject locationInView:self];
    CGPoint original;
    CGSize size;
    ALAsset *operationAsset;
    UIImageView *maskImageView;
    UILabel *assetMultiLab;
    UIImageView *assetSingleImageView;
    BOOL showOriginalPicture;
    if (CGRectContainsPoint(assetOneImageView.frame, locationPoint)) {
        if (_assetsArr.count < 1) {
            return ;
        }
        original = assetOneImageView.frame.origin;
        size = assetOneImageView.frame.size;
        operationAsset = [_assetsArr objectAtIndex:0];
        maskImageView = assetMaskOneImageView;
        assetMultiLab = assetMultiOneLab;
        assetSingleImageView = assetSingleOneImageView;
    } else if (CGRectContainsPoint(assetTwoImageView.frame, locationPoint)) {
        if (_assetsArr.count < 2) {
            return ;
        }
        original = assetTwoImageView.frame.origin;
        size = assetTwoImageView.frame.size;
        operationAsset = [_assetsArr objectAtIndex:1];
        maskImageView = assetMaskTwoImageView;
        assetMultiLab = assetMultiTwoLab;
        assetSingleImageView = assetSingleTwoImageView;
    } else if (CGRectContainsPoint(assetThrImageView.frame, locationPoint)) {
        if (_assetsArr.count < 3) {
            return ;
        }
        original = assetThrImageView.frame.origin;
        size = assetThrImageView.frame.size;
        operationAsset = [_assetsArr objectAtIndex:2];
        maskImageView = assetMaskThrImageView;
        assetMultiLab = assetMultiThrLab;
        assetSingleImageView = assetSingleThrImageView;
    }
    
    if (_selectionAssetBlock&&operationAsset) {
        if (CGRectContainsPoint((CGRect){original,size.width,size.height/2},locationPoint)) {
            showOriginalPicture = NO;
            BOOL hasFoundSelectedAsset = NO;
            
            for (ALAsset * asset in _hasSelectionArr) {
                if ([[[asset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[operationAsset valueForProperty:ALAssetPropertyAssetURL] description]]) {
                    hasFoundSelectedAsset = YES;
                    assetSingleImageView.image = nil;
                    maskImageView.backgroundColor = [UIColor clearColor];
                    
                    if (_isOnlyOneSelectable) {
                        assetSingleImageView.image = nil;
                    } else {
                        assetMultiLab.text = @"";
                    }
                    break;
                }
            }
            if (!hasFoundSelectedAsset) {
                if (!_isOnlyOneSelectable) {
                    if (_hasSelectionArr.count >= _numberOfCouldSelection) {
                        [self.window showTips:[NSString stringWithFormat:@"只能选择%li张图片",(long)_numberOfCouldSelection] ];
                        return ;
                    }
                } else {
                    if (_hasSelectionArr.count == 1) {
                        return ;
                    }
                }
            }
            _selectionAssetBlock(operationAsset, showOriginalPicture,self,maskImageView);
            if (!hasFoundSelectedAsset) {
                if (!_isOnlyOneSelectable) {
                    if ([[maskImageView.subviews objectAtIndex:0] isKindOfClass:[UIImageView class]]){
                        if ([maskImageView.subviews objectAtIndex:0] == assetSingleOneImageView){
                            [self animationWithCollectionWithButton:[maskImageView.subviews objectAtIndex:0] collection:!assetOneCollection];
                        } else if ([maskImageView.subviews objectAtIndex:0] == assetSingleTwoImageView){
                            [self animationWithCollectionWithButton:[maskImageView.subviews objectAtIndex:0] collection:!assetTwoCollection];
                        } else if ([maskImageView.subviews objectAtIndex:0] == assetSingleThrImageView){
                            [self animationWithCollectionWithButton:[maskImageView.subviews objectAtIndex:0] collection:!assetThrCollection];
                        }
                        
                        maskImageView.backgroundColor = kSELECTEDBACKGROUND;
                    } else {
                        maskImageView.image = [UIImage imageNamed:@"photoMaskHigh"];
                        
                    }
                    
                    assetMultiLab.text = [NSString stringWithFormat:@"%lu",(unsigned long)_hasSelectionArr.count];
                } else {
                    assetSingleImageView.image = [UIImage imageNamed:@"singleSelect"];
                }
            }
        } else {
            showOriginalPicture = YES;
            _selectionAssetBlock(operationAsset, showOriginalPicture,self,maskImageView);
        }
    }
}

#pragma mark -getter or setter
- (void)setIsOnlyOneSelectable:(BOOL)isOnlyOneSelectable{
    _isOnlyOneSelectable = isOnlyOneSelectable;
}

- (void)setAssetsArr:(NSArray *)assetsArr{
    _assetsArr = assetsArr;
    assetOneImageView.image = nil;
    assetTwoImageView.image = nil;
    assetThrImageView.image = nil;
    
    if (_assetsArr.count >= 1) {
        ALAsset *assetOne = (ALAsset *)[_assetsArr objectAtIndex:0];
        [self actionSetImageWithImageView:assetOneImageView asset:assetOne andIndex:0];
        
    } else {
        assetMaskOneImageView.image = nil;
    }
    if (_assetsArr.count >= 2) {
        ALAsset *assetTwo = (ALAsset *)[_assetsArr objectAtIndex:1];
        assetMaskTwoImageView.image = [UIImage imageNamed:@"photoMaskNormal"];
        [self actionSetImageWithImageView:assetTwoImageView asset:assetTwo andIndex:1];
    } else {
        assetMaskTwoImageView.image = nil;
    }
    
    if (_assetsArr.count == 3) {
        ALAsset *assetThr = (ALAsset *)[_assetsArr objectAtIndex:2];
        assetMaskThrImageView.image = [UIImage imageNamed:@"photoMaskNormal"];
        [self actionSetImageWithImageView:assetThrImageView asset:assetThr andIndex:2];
    } else {
        assetMaskThrImageView.image = nil;
    }
}

- (void)setHasSelectionArr:(NSMutableArray *)hasSelectionArr {
    _hasSelectionArr = hasSelectionArr;
    assetMultiOneLab.text = @"";
    assetMultiTwoLab.text = @"";
    assetMultiThrLab.text = @"";
    assetSingleOneImageView.image = nil;
    assetSingleTwoImageView.image = nil;
    assetSingleThrImageView.image = nil;
    assetMaskOneImageView.backgroundColor = [UIColor clearColor];
    assetMaskTwoImageView.backgroundColor = [UIColor clearColor];
    assetMaskThrImageView.backgroundColor = [UIColor clearColor];
    
    
    for (ALAsset *asset in _assetsArr){
        NSUInteger indexOfAsset = [_assetsArr indexOfObject:asset];
        if (indexOfAsset == 0){
//            assetSingleOneImageView.image = [UIImage imageNamed:@"address_icon_untick"];
        } else if (indexOfAsset == 1){
//            assetSingleTwoImageView.image = [UIImage imageNamed:@"address_icon_untick"];
        } else if (indexOfAsset == 2){
//            assetSingleThrImageView.image = [UIImage imageNamed:@"address_icon_untick"];
        }
    }
    
    for (ALAsset *selectedAsset in _hasSelectionArr) {
        for (ALAsset *asset in _assetsArr) {
            if ([[[selectedAsset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[asset valueForProperty:ALAssetPropertyAssetURL] description]]) {
                NSUInteger indexOfAsset = [_assetsArr indexOfObject:asset];
                if (_isOnlyOneSelectable) {
                    if (indexOfAsset == 0) {
                        assetSingleOneImageView.image = [UIImage imageNamed:@"singleSelect"];
                    } else if (indexOfAsset == 1) {
                        assetSingleTwoImageView.image = [UIImage imageNamed:@"singleSelect"];
                    } else if (indexOfAsset == 2) {
                        assetSingleThrImageView.image = [UIImage imageNamed:@"singleSelect"];
                    }
                    return;
                    break;
                } else {
                    NSUInteger indexOfSelectedAsset = [_hasSelectionArr indexOfObject:selectedAsset]+1;
                    if (indexOfAsset == 0) {
                        assetSingleOneImageView.image = [UIImage imageNamed:@"address_icon_selected"];
                        assetMaskOneImageView.backgroundColor = kSELECTEDBACKGROUND;
                        assetMultiOneLab.text = [NSString stringWithFormat:@"%lu",(unsigned long)indexOfSelectedAsset];
                    } else if (indexOfAsset == 1){
                        assetSingleTwoImageView.image = [UIImage imageNamed:@"address_icon_selected"];
                        assetMaskTwoImageView.backgroundColor = kSELECTEDBACKGROUND;
                        assetMultiTwoLab.text = [NSString stringWithFormat:@"%lu",(unsigned long)indexOfSelectedAsset];
                    } else if (indexOfAsset == 2) {
                        assetSingleThrImageView.image = [UIImage imageNamed:@"address_icon_selected"];
                        assetMaskThrImageView.backgroundColor = kSELECTEDBACKGROUND;
                        assetMultiThrLab.text = [NSString stringWithFormat:@"%lu",(unsigned long)indexOfSelectedAsset];
                    }
                }
            }
        }
    }
}



#pragma mark -actionSetImageWithImageView
- (void)actionSetImageWithImageView:(UIImageView *)imageView asset:(ALAsset *)asset andIndex:(NSInteger)index{
    if (asset != nil) {
        imageView.image = [UIImage imageWithCGImage:asset.thumbnail];
        BOOL assetOfHasLoaded =  [objc_getAssociatedObject(asset, assetHasLoaded) boolValue];
        if (!assetOfHasLoaded) {
            imageView.alpha = 0.;
            [UIView animateWithDuration:.5 animations:^{
                imageView.alpha = 1.;
                imageView.image = [UIImage imageWithCGImage:asset.thumbnail];
            } completion:^(BOOL finished) {
                if (finished) {
                    objc_setAssociatedObject(asset, assetHasLoaded, [NSNumber numberWithBool:YES], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
                    if (index == 0){
                        
                        assetSingleOneImageView.alpha = 0;
                    } else if (index == 1){
                        
                        assetSingleTwoImageView.alpha = 0;
                    } else if (index == 2){
                        
                        assetSingleThrImageView.alpha = 0;
                    }
                    [UIView animateWithDuration:.3f animations:^{
                        if (index == 0){
                            assetSingleOneImageView.alpha = 1;
                            //                            assetSingleOneImageView.image = [UIImage imageNamed:@"address_icon_untick"];
                        } else if (index == 1){
                            assetSingleTwoImageView.alpha = 1;
                            //                            assetSingleTwoImageView.image = [UIImage imageNamed:@"address_icon_untick"];
                        } else if (index == 2){
                            assetSingleThrImageView.alpha = 1;
                            //                            assetSingleThrImageView.image = [UIImage imageNamed:@"address_icon_untick"];
                        }
                    }];
                }
            }];
        } else {
            
        }
    }
}

#pragma mark -actionReloadTextValueWithIndex
- (void)actionReloadTextValueWithIndex:(NSUInteger)index{
    ALAsset *asset = [_assetsArr objectAtIndex:index];
    for (ALAsset *selectionAsset in _hasSelectionArr) {
        if ([[[selectionAsset valueForProperty:ALAssetPropertyAssetURL] description] isEqualToString:[[asset valueForProperty:ALAssetPropertyAssetURL] description]]) {
            NSUInteger indexOfSelectionArr = [_hasSelectionArr indexOfObject:selectionAsset];
            if (index == 0) {
                assetMultiOneLab.text = [NSString stringWithFormat:@"%lu",(long)indexOfSelectionArr+1];
            } else if (index == 1) {
                assetMultiTwoLab.text = [NSString stringWithFormat:@"%lu",(long)indexOfSelectionArr+1];
            } else if (index == 2){
                assetMultiThrLab.text = [NSString stringWithFormat:@"%lu",(long)indexOfSelectionArr+1];
            }
            break;
        }
    }
}

#pragma mark -actionReloadSelectionStateWithIndex
- (void)actionReloadSelectionStateWithIndex:(NSUInteger)index isSelection:(BOOL)isSelected{
    if (isSelected) {
        [self actionReloadTextValueWithIndex:index];
        if (index == 0) {
            if (_isOnlyOneSelectable) {
                assetMultiOneLab.text = @"";
                assetSingleOneImageView.image = [UIImage imageNamed:@"singleSelect"];
            } else {
                assetMaskOneImageView.image = [UIImage imageNamed:@"photoMaskHigh"];
            }
        } else if (index == 1) {
            if (_isOnlyOneSelectable) {
                assetMultiTwoLab.text = @"";
                assetSingleTwoImageView.image = [UIImage imageNamed:@"singleSelect"];
            } else {
                assetMaskTwoImageView.image = [UIImage imageNamed:@"photoMaskHigh"];
            }
        } else if (index == 2){
            if (_isOnlyOneSelectable) {
                assetMultiThrLab.text = @"";
                assetSingleTwoImageView.image = [UIImage imageNamed:@"singleSelect"];
            } else {
                assetMaskThrImageView.image = [UIImage imageNamed:@"photoMaskHigh"];
            }
        }
    } else {
        if (index == 0) {
            assetMultiOneLab.text = @"";
            assetSingleOneImageView.image = nil;
            assetMaskOneImageView.image = [UIImage imageNamed:@"photoMaskNormal"];
        } else if (index == 1){
            assetMultiTwoLab.text = @"";
            assetSingleTwoImageView.image = nil;
            assetMaskTwoImageView.image = [UIImage imageNamed:@"photoMaskNormal"];
        } else if (index == 2){
            assetMultiThrLab.text = @"";
            assetSingleThrImageView.image = nil;
            assetMaskThrImageView.image = [UIImage imageNamed:@"photoMaskNormal"];
        }
    }
}

#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection{
    if (isCollection){
        collectionButton.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        collectionButton.image = nil;
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    isCollection = !isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}


@end
