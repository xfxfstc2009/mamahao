//
//  MMHMessageBarButtonItem.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHAssistant.h"


@interface MMHMessageBarButtonItem : UIBarButtonItem

- (instancetype)initWithBadgeNumber:(NSInteger)number target:(id)target action:(SEL)action;

- (void)setBadgeNumber:(NSInteger)badgeNumber;
@end
