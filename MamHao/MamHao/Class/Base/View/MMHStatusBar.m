//
//  MMHStatusBar.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHStatusBar.h"

static MMHStatusBar *bar = nil;

@implementation MMHStatusBar

//Signleton
+(MMHStatusBar *)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        bar = [[MMHStatusBar alloc] init];
    });
    return bar;
}

-(id)init{
    if (self = [super init]){
        self.frame = [[UIApplication sharedApplication]statusBarFrame];
        self.windowLevel = UIWindowLevelStatusBar + 1.f;
        self.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
        
        if (!self.messageLabel){
            self.messageLabel = [[UILabel alloc]init];
            self.messageLabel.font = [UIFont systemFontOfSize:12.];
            self.messageLabel.textAlignment = NSTextAlignmentRight;
            self.messageLabel.textColor = [UIColor hexChangeFloat:@"FFFFFF"];
            self.messageLabel.backgroundColor = [UIColor clearColor];
        }
        [self addSubview:self.messageLabel];
    }
    return self;
}


#pragma mark - showMessage
-(void)showMessage:(NSString *)message{
    [self setHidden:NO];
    self.alpha = 1.;
    self.messageLabel.text = message;
    [self.messageLabel sizeToFit];
    [self setFixPosition:self.messageLabel position:CGPointMake((320-CGRectGetWidth(self.messageLabel.frame))/2., (CGRectGetHeight([self frame])-CGRectGetHeight(self.messageLabel.frame))/2.)];
    [UIView animateWithDuration:.5f animations:^{
        self.frame = (CGRect){self.frame.origin,CGSizeMake(320, 20)};
    } completion:^(BOOL finished) {
        [self performSelector:@selector(statusBarWithHide) withObject:nil afterDelay:3.];
    }];
}

-(void)statusBarWithHide{
    self.alpha = 1.f;
    [UIView animateWithDuration:.1f animations:^{
        self.alpha = 0.f;
    } completion:^(BOOL finished) {
        self.messageLabel.text = @"";
        self.hidden = YES;
    }];
}


// setFixPosition
- (void)setFixPosition:(UIView *)insertView position:(CGPoint)resetPosition {
    CGRect originalFrame = [insertView frame];
    originalFrame.origin = resetPosition;
    [insertView setFrame:originalFrame];
}

@end
