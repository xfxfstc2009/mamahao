//
//  MMHSegmentView.m
//  MamHao
//
//  Created by SmartMin on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSegmentView.h"

#define kDefaultTintColor  [UIColor colorWithRed:3/256. green:116/256. blue:255/256. alpha:1]
#define kLeftMargin 15
#define kItemHeight 30
#define kBorderLineWidth 0.5

#define kNorFont [UIFont systemFontOfSize:14]
#define kHltFont [UIFont systemFontOfSize:17]

@class MMHSegmentItem;
@protocol MMHSegmentItemDelegate
-(void)itemStateChanged:(MMHSegmentItem *)item index:(NSInteger )index isSelected:(BOOL)isSelected;
@end

#pragma mark - SegmentItem

@interface MMHSegmentItem : UIView
@property (nonatomic,strong) UIColor *norColor;
@property (nonatomic,strong) UIColor *hltColor;
@property (nonatomic,strong) UILabel *titleLabel;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,assign) BOOL isSelected;
@property (nonatomic) id    delegate;

@end

@implementation MMHSegmentItem

- (id)initWithFrame:(CGRect)frame index:(NSInteger)index title:(NSString *)title norColor:(UIColor *)norColor hltColor:(UIColor *)hltColor isSelected:(BOOL)isSelected;
{
    self = [super initWithFrame:frame];
    if (self) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _titleLabel.textAlignment   = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = kNorFont;
        [self addSubview:_titleLabel];
        
        _norColor        = norColor;
        _hltColor        = hltColor;
        _titleLabel.text = title;
        _index           = index;
        _isSelected      = isSelected;
    }
    return self;
}


- (void)setSelColor:(UIColor *)selColor {
    if (_hltColor != selColor) {
        _hltColor = selColor;
        
        if (_isSelected) {
            _titleLabel.textColor = _norColor;
        } else  {
            _titleLabel.textColor = _hltColor;
            self.backgroundColor = _norColor;
        }
    }
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    __weak typeof(self)weakSelf = self;
    if (_isSelected) {
        [UIView animateWithDuration:.5 animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->_titleLabel.textColor = strongSelf->_norColor;
            strongSelf->_titleLabel.font = kHltFont;
            self.backgroundColor = strongSelf->_hltColor;
        }];
    } else {
        [UIView animateWithDuration:.5 animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->_titleLabel.textColor = strongSelf->_hltColor;
            strongSelf->_titleLabel.font = kNorFont;
            self.backgroundColor = strongSelf->_norColor;
        }];

    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    self.isSelected = !_isSelected;
    
    if (_delegate) {
        [_delegate itemStateChanged:self index:_index isSelected:self.isSelected];
    }
}

@end



#pragma mark - SegmentView

@interface MMHSegmentView()
@property (nonatomic,strong) UIView *bgView;
@property (nonatomic,strong) NSMutableArray *titlesArr;
@property (nonatomic,strong) NSMutableArray *itemsArr;
@property (nonatomic,strong) NSMutableArray *linesArr;

@end

@implementation MMHSegmentView
-(id)initWithFrame:(CGRect)frame items:(NSArray *)items{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        CGFloat viewWidth = CGRectGetWidth(frame);
        CGFloat viewHeitht = CGRectGetHeight(frame);
        CGFloat viewOriginX = CGRectGetMinX(frame);
        CGFloat viewOriginY = CGRectGetMinY(frame);
        
        _bgView = [[UIView alloc]initWithFrame:CGRectMake(kLeftMargin, (viewHeitht - kItemHeight) / 2., viewWidth - 2*kLeftMargin, kItemHeight)];
        _bgView.backgroundColor = [UIColor whiteColor];
        _bgView.clipsToBounds = YES;
        _bgView.layer.cornerRadius = 5.;
        _bgView.layer.borderWidth = kBorderLineWidth;
        _bgView.layer.borderColor = kDefaultTintColor.CGColor;
        [self addSubview:_bgView];
        
        viewOriginX = 0;
        viewOriginY = 0;
        
        CGFloat itemWidth = CGRectGetWidth(_bgView.frame)/items.count;
        CGFloat itemHeight = CGRectGetHeight(_bgView.frame);
        if (items.count >= 2){
            for (int i = 0 ; i < items.count ; i++){
                
                MMHSegmentItem *segmentItem = [[MMHSegmentItem alloc]initWithFrame:CGRectMake(viewOriginX, viewOriginY, itemWidth, itemHeight) index:i title:[items objectAtIndex:i] norColor:[UIColor whiteColor] hltColor:kDefaultTintColor isSelected:(i == 0)? YES : NO];
                viewOriginX += itemWidth;
                [_bgView addSubview:segmentItem];
                segmentItem.delegate = self;
                
                if (!_itemsArr){
                    _itemsArr = [NSMutableArray arrayWithCapacity:items.count];
                }
                [_itemsArr addObject:segmentItem];
            }
        }
        
        // 添加竖线
        viewOriginX = 0;
        for (int i = 0 ; i < (items.count - 1 ); i++){
            viewOriginX += itemWidth;
            UIView *lintView = [[UIView alloc]initWithFrame:CGRectMake(viewOriginX, 0, kBorderLineWidth, itemHeight)];
            lintView.backgroundColor = kDefaultTintColor;
            [_bgView addSubview:lintView];
            
            if (!_linesArr){
                _linesArr = [NSMutableArray arrayWithCapacity:items.count];
            }
            [_linesArr addObject:lintView];
        }
        
    } else {
        NSException *exc = [[NSException alloc] initWithName:@"items count error"
                                                      reason:@"items count at least 2"
                                                    userInfo:nil];
        @throw exc;

    }
    return self;
}

- (void)setTintColor:(UIColor *)tintColor {
    if (self.itemsArr.count < 2) {
        return;
    }
    
    if (_tintColor != tintColor) {
        
        self.bgView.layer.borderColor  = tintColor.CGColor;
        
        for (NSInteger i = 0; i<self.itemsArr.count; i++) {
            MMHSegmentItem *item = self.itemsArr[i];
            item.hltColor = tintColor;
            [item setNeedsDisplay];
        }
        
        for (NSInteger i = 0; i<self.linesArr.count; i++) {
            UIView *lineView = self.linesArr[i];
            lineView.backgroundColor = tintColor;
        }
    }
    
}

#pragma mark - MMHSegmentItemDelegate
-(void)itemStateChanged:(MMHSegmentItem *)currentItem index:(NSInteger )index isSelected:(BOOL)isSelected{
    if (_itemsArr.count < 2){
        return;
    }
    
    for (int i = 0 ; i < _itemsArr.count; i++){
        MMHSegmentItem *item = [_itemsArr objectAtIndex:i];
        item.isSelected = NO;
    }
    currentItem.isSelected = YES;
    if (_delegate && [_delegate respondsToSelector:@selector(segmentViewSelectIndex:)])
    {
        [_delegate segmentViewSelectIndex:index];
    }

}


@end


