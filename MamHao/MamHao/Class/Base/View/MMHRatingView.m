//
//  MMHRatingView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRatingView.h"
#import "UIView+Extension.h"


@interface MMHRatingView ()

@property (nonatomic, copy) NSString *emptyImageName;
@property (nonatomic, copy) NSString *halfImageName;
@property (nonatomic, copy) NSString *fullImageName;
@property (nonatomic, strong) NSArray *starViews;
@property (nonatomic, assign) CGFloat padding;
@end


@implementation MMHRatingView


- (void)setPadding:(CGFloat)padding {
    _padding = padding;
    [self updateViews];
}

- (void)setRating:(CGFloat)rating
{
    _rating = rating;

    [self updateViews];
}


- (instancetype)initWithEmptyImageName:(NSString * __nonnull)emptyImageName halfImageName:(NSString * __nonnull)halfImageName fullImageName:(NSString * __nonnull)fullImageName
{
    self = [self init];
    if (self) {
        self.emptyImageName = emptyImageName;
        self.halfImageName = halfImageName;
        self.fullImageName = fullImageName;
        self.rating = 0.0f;
        self.padding = 0.5f;
    }
    return self;
}


- (void)updateViews
{
    if ([self.starViews count] != 0) {
        for (UIImageView *starView in self.starViews) {
            [starView removeFromSuperview];
        }
    }

    CGFloat x = 0.0f;
    NSMutableArray *starViews = [NSMutableArray array];
    for (NSInteger i = 0; i < 5; i++) {
        NSString *imageName = self.emptyImageName;
        CGFloat floatI = (CGFloat)(i + 1);
        if (self.rating < i + 0.25f) {
            imageName = self.emptyImageName;
        }
        else if (self.rating < i + 0.75f) {
            imageName = self.halfImageName;
        }
        else {
            imageName = self.fullImageName;
        }
        UIImageView *starView = [UIImageView imageViewWithImageName:imageName];
        starView.left = x;
        x = starView.right + self.padding;
        [self addSubview:starView];
        [starViews addObject:starView];

        self.width = starView.right;
    }
    self.starViews = starViews;
}


@end
