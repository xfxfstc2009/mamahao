//
//  MMHMessageBarButtonItem.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMessageBarButtonItem.h"
#import "MMHLogger.h"
#import "MMHChattingSession.h"


@interface MMHMessageBarButtonItem () <MMHChattingSessionUnreadCountDelegate>

@property (nonatomic, strong) UILabel *badgeLabel;
@end


@implementation MMHMessageBarButtonItem


- (instancetype)initWithBadgeNumber:(NSInteger)number target:(id)target action:(SEL)action
{
    UIImage *image = [UIImage imageNamed:@"home_icon_news"];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, image.size.width, image.size.height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];

    UILabel *badgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 12.0f, 12.0f)];
    badgeLabel.center = CGPointMake(button.right, 0.0f);
    [badgeLabel makeRoundedRectangleShape];
    badgeLabel.textAlignment = NSTextAlignmentCenter;
    badgeLabel.backgroundColor = [UIColor colorWithHexString:@"dd2727"];
    badgeLabel.textColor = [UIColor whiteColor];
    badgeLabel.font = [UIFont systemFontOfSize:9.0f];
    [button addSubview:badgeLabel];
    self.badgeLabel = badgeLabel;

    [self setBadgeNumber:number];

    self = [self initWithCustomView:button];
    if (self) {
        [MMHChattingSession currentSession].unreadCountDelegate = self;
    }
    return self;
}


- (void)setBadgeNumber:(NSInteger)badgeNumber
{
    NSString *text = @"";
    if (badgeNumber == 0) {
        self.badgeLabel.hidden = YES;
    }
    else {
        self.badgeLabel.hidden = NO;
        text = [@(badgeNumber) description];
    }

    if ([text length] > 1) {
        self.badgeLabel.font = [UIFont systemFontOfSize:7.0f];
    }
    else {
        self.badgeLabel.font = [UIFont systemFontOfSize:9.0f];
    }
    self.badgeLabel.text = text;
}


- (void)chattingSessionUnreadCountChanged:(MMHChattingSession *)chattingSession
{
    [self setBadgeNumber:[chattingSession unreadMessageCount]];
}
@end
