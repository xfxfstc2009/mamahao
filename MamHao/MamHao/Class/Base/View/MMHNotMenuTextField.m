//
//  MMHNotMenuTextField.m
//  MamHao
//
//  Created by SmartMin on 15/7/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNotMenuTextField.h"

@implementation MMHNotMenuTextField


-(BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    if (menuController) {
        [UIMenuController sharedMenuController].menuVisible = NO;
    }
    return NO;
}
@end
