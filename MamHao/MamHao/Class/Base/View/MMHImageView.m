//
//  MMHImageView.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHImageView.h"
#import "SDImageCache.h"
#import "UIImageView+WebCache.h"
#import "UIView+Extension.h"


@interface MMHImageView ()

@property (nonatomic, copy) NSString *updatingImageURLString;
@end


@implementation MMHImageView


- (void)setPlaceholderImageName:(NSString *)placeholderImageName
{
    _placeholderImageName = placeholderImageName;

    [self updateViewWithPlaceholderImage];
}

- (void)setActionBlock:(MMHImageViewActionBlock)actionBlock
{
    _actionBlock = [actionBlock copy];
    
    if (actionBlock) {
        self.userInteractionEnabled = YES;
    }
    else {
        self.userInteractionEnabled = NO;
    }
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.clipsToBounds = YES;
        self.shouldClearPreviousImageWhenUpdating = YES;
        [self updateViewWithPlaceholderImage];
//        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beenTapped:)];
        gr.cancelsTouchesInView = NO;
        [self addGestureRecognizer:gr];
    }
    return self;
}


- (void)updateViewWithImageAtURL:(NSString *)urlString
{
    [self updateViewWithImageAtURL:urlString finishBlock:nil];
}


- (void)updateViewWithImageAtURL:(NSString *)urlString finishBlock:(dispatch_block_t)finishBlock
{
    if(![urlString isKindOfClass:[NSString class]]){
        return;
    }
    if ([urlString length] == 0) {
        [self updateViewWithImage:nil];
        return;
    }

    NSString *actualURLString = urlString;
    
    /*
        temData.getGoodsPic()+"@1e_200w_200h_0c_0i_0o_60q_1x.jpg"
     */
    
    if (![urlString hasPrefix:@"http://"]) {
        actualURLString = [NSString stringWithFormat:@"http://image.weixiao1688.com/%@", urlString];
        #if defined (API_MAMAHAO_COM)
                actualURLString = [NSString stringWithFormat:@"http://img.mamahao.com/%@", urlString];
        #endif
        if (self.urlPrefixString.length != 0) {
            actualURLString = [NSString stringWithFormat:@"%@%@", self.urlPrefixString, urlString];
        }
    }

//    NSLog(@"url string: %@", urlString);
//    NSLog(@"actual string: %@", actualURLString);
//    NSLog(@"url string: %@", urlString);
    
    NSInteger expectedWidth = (NSInteger)(self.frame.size.width * mmh_screen_scale());
    NSInteger expectedHeight = (NSInteger)(self.frame.size.height * mmh_screen_scale());
    NSString *croppedImageURLString = [actualURLString stringByAppendingFormat:@"@%ldw_%ldh_1e", (long)expectedWidth, (long)expectedHeight];
//#ifdef DEBUG
    if ((![actualURLString hasPrefix:@"http://img.mamahao.com/"]) || (self.imageCroppingDisabled)) {
        croppedImageURLString = actualURLString;
    }
//#endif

    if ([self.updatingImageURLString length] != 0) {
        if ([self.updatingImageURLString isEqualToString:croppedImageURLString]) {
            return;
        }
    }

    self.updatingImageURLString = croppedImageURLString;
    
//    if (self.networkOperation != nil) {
//        if ([croppedImageURLString isEqualToString:self.networkOperation.url]) {
//            return;
//        }
//    }
//
//    if (self.networkOperation != nil) {
//        [self.networkOperation cancel];
//        self.networkOperation = nil;
//    }
    
    if (self.shouldClearPreviousImageWhenUpdating) {
        [self updateViewWithImage:nil];
    }
    __weak __typeof(self) weakSelf = self;
    
//    NSLog(@"cropped string: %@", croppedImageURLString);

    NSString *finalURLString = [croppedImageURLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:finalURLString];
    [self sd_setImageWithPreviousCachedImageWithURL:url
                                andPlaceholderImage:nil
                                            options:0
                                           progress:nil
                                          completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                              if (image == nil || error) {
                                                  weakSelf.updatingImageURLString = nil;
//                                                  #ifdef DEBUG
//                                                  [weakSelf updateViewWithImageAtURL:@"http://images.apple.com/v/home/bv/images/home_hero_iphone_medium.png" finishBlock:finishBlock];
//                                                  #endif
                                                  [MMHLogbook logEvent:@"imageCannotBeFetched" withParameters:@{@"url": [imageURL absoluteString]}];
                                                  return;
                                              }

                                              [self updateViewWithImage:image];
                                              if (finishBlock != nil) {
                                                  dispatch_async(dispatch_get_main_queue(), finishBlock);
                                              }
                                          }];
}


- (void)updateViewWithImage:(UIImage *)image
{
    if (image == nil) {
        self.updatingImageURLString = nil;
        [self updateViewWithPlaceholderImage];
        return;
    }
    
    self.image = image;
}


- (void)updateViewWithPlaceholderImage
{
    if ([self.placeholderImageName length] != 0) {
        UIImage *image = [UIImage imageNamed:self.placeholderImageName];
        if (image) {
            self.image = image;
            return;
        }
    }
//    self.backgroundColor = [MMHAppearance separatorColor];
//    
//    NSString *imageName = @"mamahao_placeholder_120";
//    if (self.width * self.height > 150.0f * 150.0f) {
//        imageName = @"mamahao_placeholder_300";
//    }
//    if (self.width * self.height > 350.0f * 350.0f) {
//        imageName = @"mamahao_placeholder_700";
//    }
//
//    self.image = [UIImage imageNamed:imageName];
    
    self.image = [UIImage patternImageWithColor:[MMHAppearance separatorColor]];
}


- (void)beenTapped:(UITapGestureRecognizer *)gr
{
    if (self.actionBlock != nil) {
        self.actionBlock();
    }
}


@end
