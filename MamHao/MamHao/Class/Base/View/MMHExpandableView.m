//
//  MMHExpandableView.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpandableView.h"
#import "UIView+Extension.h"


@interface MMHExpandableView ()

@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *indicatorImageView;
@property (nonatomic, copy) NSString *summaryText;
@property (nonatomic, copy) NSString *fullText;
@property (nonatomic) BOOL expanded;
@property (nonatomic) CGRect closedFrame;
@end


@implementation MMHExpandableView


- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    self.textLabel.textColor = textColor;
    [self updateViewFrames];
}


- (void)setFont:(UIFont *)font
{
    _font = font;
    self.textLabel.font = font;
    [self updateViewFrames];
}


- (void)setContentInsets:(UIEdgeInsets)contentInsets
{
    _contentInsets = contentInsets;
    [self updateViewFrames];
}


- (void)setExpanded:(BOOL)expanded
{
    if (_expanded == expanded) {
        return;
    }
    
    _expanded = expanded;

    if (expanded) {
        self.indicatorImageView.image = [UIImage imageNamed:@"cart_icon_arrow_up"];
        [self.textLabel setText:self.fullText constrainedToLineCount:0];
    }
    else {
        self.indicatorImageView.image = [UIImage imageNamed:@"cart_icon_arrow_down"];
        [self.textLabel setText:self.summaryText constrainedToLineCount:1];
    }
    
    [self updateViewFrames];

    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(expandableViewHeightDidChange:)]) {
            [self.delegate expandableViewHeightDidChange:self];
        }
    }
}


//- (void)setText:(NSString *)text
//{
//    _text = text;
//    [self.textLabel setText:text constrainedToLineCount:1];
//}


- (instancetype)initWithFrame:(CGRect)frame summaryText:(NSString *)summaryText fullText:(NSString *)fullText
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.closedFrame = frame;
        self.contentInsets = UIEdgeInsetsMake(15.0f, 15.0f, 15.0f, 15.0f);
        self.summaryText = summaryText;
        self.fullText = fullText;
        [self configureViews];
    }
    return self;
}


- (void)configureViews
{
    if (self.textLabel == nil) {
        UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        textLabel.font = self.font;
        textLabel.textColor = self.textColor;
        [textLabel setText:self.summaryText constrainedToLineCount:1];
        [self addSubview:textLabel];
        self.textLabel = textLabel;
    }
    
    if (self.indicatorImageView == nil) {
        UIImageView *indicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cart_icon_arrow_down"]];
        [self addSubview:indicatorImageView];
        self.indicatorImageView = indicatorImageView;
    }

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    self.userInteractionEnabled = YES;
    [self addGestureRecognizer:tapGestureRecognizer];

    [self updateViewFrames];
}


- (void)updateViewFrames
{
    [self.indicatorImageView moveToRight:CGRectGetMaxX(self.bounds) - self.contentInsets.right];
    self.indicatorImageView.top = self.contentInsets.top;

    [self.textLabel setLeft:self.contentInsets.left];
    [self.textLabel setMaxX:CGRectGetMinX(self.indicatorImageView.frame) - 15.0f];
    self.textLabel.top = self.contentInsets.top;
    
    if (self.expanded) {
        [self setHeight:self.textLabel.bottom + self.contentInsets.bottom];
    }
    else {
        self.size = self.closedFrame.size;
        self.textLabel.centerY = self.closedFrame.size.height * 0.5f;
    }
}


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    self.expanded = !self.expanded;
}


@end
