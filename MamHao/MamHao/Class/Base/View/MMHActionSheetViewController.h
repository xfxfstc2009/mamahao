//
//  MMHActionSheetViewController.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// actionSheet
#import <UIKit/UIKit.h>

// 分享类型
typedef NS_ENUM(NSUInteger, MMHShareType) {
    MMHShareWeiXin = 0,
    MMHShareFriendsCircle ,
    MMHShareSinaWeibo ,
    MMHShareQQ ,
    MMHShareQQSpace ,
    MMHShareFacebook,
    MMHCopyProduct,
    MMHSwitchToShop
};

// 操作类型
typedef NS_ENUM(NSUInteger, MMHSceneType) {
    MMHProductType = 0,             // 商品分享
    MMHOtherType,                   // 其他分享
    MMHShareAppType                  // 分享app
};


@protocol  socialShareClickDelegate<NSObject>

- (void)didClickShareButtonDelegate:(MMHShareType)shareType;

@end


@interface MMHActionSheetViewController : UIViewController
@property (nonatomic,assign) MMHSceneType sceneType;                        // 操作类型
@property (nonatomic,weak) id<socialShareClickDelegate> delegate;           // 执行类型
@property (nonatomic,strong) UIImage *backGroundImage;                      // 背景图片

@property (nonatomic,assign) BOOL isShareProduct;                           // 判断是否分享
@property (nonatomic,assign) BOOL isHasCancelButton;                        // 判断是否需要有取消按钮
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

-(void)hideParentViewControllerTabbar:(UIViewController *)viewController;
-(void)showInView:(UIViewController *)viewController;                           // 显示show
-(void)dismissFromView:(UIViewController *)viewController;                      // 隐藏dismiss


@end
