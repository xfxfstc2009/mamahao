//
//  MMHRatingView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MMHRatingView : UIView

@property (nonatomic) CGFloat rating;

- (nonnull instancetype)initWithEmptyImageName:(NSString * __nonnull)emptyImageName halfImageName:(NSString * __nonnull)halfImageName fullImageName:(NSString * __nonnull)fullImageName;

- (void)setPadding:(CGFloat)padding;
@end
