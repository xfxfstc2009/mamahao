//
//  MMHSegmentView.h
//  MamHao
//
//  Created by SmartMin on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMHSegmentViewDelegate <NSObject>
- (void)segmentViewSelectIndex:(NSInteger)index;
@end

@interface MMHSegmentView : UIView

@property (nonatomic,strong)UIColor *tintColor;
@property (nonatomic)id <MMHSegmentViewDelegate> delegate;

- (id)initWithFrame:(CGRect)frame items:(NSArray *)items;

@end
