//
//  MMHPopView.h
//  MamHao
//
//  Created by SmartMin on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHsinceTheMentionAddressModel.h"
#import "MMHOrderRootViewController.h"                  // 拨打电话使用
#import "MMHProductDetailModel.h"
#import "MMHOrderListShopInfoCell.h"

@class MMHOrderListShopInfoCell;
typedef void(^callPhoneBlock)(NSString *phoneNumber);

typedef NS_ENUM(NSInteger, MMHPopViewUsingType) {
    MMHUsingTypeProductGuarantees ,              /**< 服务担保 - 应用于商品详情 */
    MMHUsingTypeOrderExpress,                    /**< 订单电话 */
    MMHUsingTypeOrderShopInfo                    /**< 商店信息 */
};

@interface MMHPopView : UIView


-(void)viewShow;
-(void)viewDismiss;

@property (nonatomic,assign)MMHPopViewUsingType usingType;
@property (nonatomic,strong)NSArray<MMHProductDetailGoodsTagModel>*goodsTag;        /**< 商品标签*/

// 商店信息Model
@property (nonatomic,strong)MMHsinceTheMentionAddressModel *shopInfoModel;

// 电话
@property (nonatomic,copy)callPhoneBlock callBlock;
@end
