//
//  MMHActionSheetViewController.m
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHActionSheetViewController.h"
#import "AbstractViewController.h"
#import "UIImageView+WebCache.h"
#import "UIImage+ImageEffects.h"
#import "MMHDeleteLineLabel.h"

#define SheetHeight       400

// SKU_Color
#define SheetColorSKU_H      90                        // 颜色sku高度
#define colorSKUButton_WH    20                        // 颜色sku按钮宽高

// SKU_Size
#define SheetSizeSKU_H       180                       // 大小SKU高度
#define sizeSKUButton_WH     20                        // 大小sku按钮宽高

// SKU_Number
#define SheetNumberSKU_H     270                       // 数量SKU高度

#define kCancelButton_WH     30                        // 取消按钮的长宽

@interface MMHActionSheetViewController () {
    MMHShareType shareType;
    
    UILabel         *titleLabel;
    UILabel         *priceLabel;
    UIView          *backgrondView;
    UIImageView     *imageView;
    UIScrollView    *colorSKUScrollView;
    UIScrollView    *sizeSKUScrollView;
    
    MMHDeleteLineLabel *oldPrice;
    UILabel *newPrice;
    UILabel *numberLabel;
    NSMutableArray *colorArr;
    NSMutableArray *sizeArr;
}

@property (nonatomic,   weak) UIViewController  *showViewController;
@property (nonatomic, strong) UIImageView       *shareView;

@property (nonatomic,assign)NSInteger customerProductNumber;
@end

@implementation MMHActionSheetViewController


-(void)dealloc{
    imageView = nil;
    priceLabel = nil;
    _shareView = nil;
    colorSKUScrollView = nil;
    sizeSKUScrollView = nil;
    
    [imageView removeFromSuperview];
    [priceLabel removeFromSuperview];
    [_shareView removeFromSuperview];
    [_shareView removeFromSuperview];
    [colorSKUScrollView removeFromSuperview];
    [sizeSKUScrollView removeFromSuperview];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

-(void)viewDidLoad{
    [self arrayWithInit];                        // 数组初始化
    [self createSheetView];                      // 加载Sheetview
}

-(void)arrayWithInit{
    // color Array
    UIColor *redColor = [UIColor redColor];
    UIColor *blackColor = [UIColor blackColor];
    UIColor *blueColor = [UIColor blueColor];
    UIColor *greenColor = [UIColor greenColor];
    colorArr = [NSMutableArray arrayWithObjects:redColor,blackColor,blueColor,greenColor,nil];
    
    //
    sizeArr = [NSMutableArray arrayWithObjects:@"0",@"1",@"2",@"3",@"4", nil];
}


#pragma mark - createView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.3f];
    [self.view addSubview:backgrondView];
    
    // 背景色渐入
    [self backGroundColorFadeInOrOutFromValue:0.0f toValue:1.0f];
    
    CGFloat viewHeight = SheetHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    if (_sceneType == MMHOtherType){
        viewHeight = 155;
    } else if (_sceneType == MMHProductType){
        viewHeight = 275 + SheetColorSKU_H;
    }
    
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.image = _backGroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:.8];
    [self.view addSubview:_shareView];
    
    // 创建shareView
    [self createCellWithProduct];
    // 创建颜色SKU
    [self createProductColorSKU];
    // 创建尺寸SKU
    [self createSizeSKU];
    // 创建数量SKU
    [self createNumberCell];
    
    
    // Dismiss_Button
    UIButton *determineButton = [UIButton buttonWithType:UIButtonTypeCustom];
    determineButton.frame = CGRectMake(0, _shareView.frame.size.height - 45, _shareView.frame.size.width, 45);
    [determineButton setTitle:@"确定" forState:UIControlStateNormal];
    determineButton.titleLabel.font = [UIFont systemFontOfSize:20.f];
    [determineButton addTarget:self action:@selector(sheetViewDismiss) forControlEvents:UIControlEventTouchUpInside];
    determineButton.backgroundColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:.2f];
    [determineButton setTitleColor:[UIColor colorWithRed:14/256. green:69/256. blue:250/256. alpha:1] forState:UIControlStateNormal];
    [_shareView addSubview:determineButton];

    // Gesture
    if (_isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gestureRecognizerHandle:)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
}

// 创建商品row
-(void)createCellWithProduct{
    UIView *productView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _shareView.frame.size.width, 90)];
    [_shareView addSubview:productView];

    // 商品image
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 60, 60)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 3.;
    imageView.backgroundColor = [UIColor redColor];
    imageView.image = [UIImage imageNamed:@"test"];
    [productView addSubview:imageView];
    
    // 商品title
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(imageView.frame)+CGRectGetWidth(imageView.frame)+10, CGRectGetMinY(imageView.frame), 215, 34)];
    titleLabel.numberOfLines = 0;
    titleLabel.backgroundColor = [UIColor redColor];
    titleLabel.text = @"日本原装进口 花王 婴儿纸尿裤";
    titleLabel.font = [UIFont systemFontOfSize:14.f];
    [_shareView addSubview:titleLabel];
    
    // 原价
    oldPrice = [[MMHDeleteLineLabel alloc]initWithFrame:CGRectMake(titleLabel.frame.origin.x, CGRectGetMaxY(titleLabel.frame)+5, 100, 30)];
    oldPrice.numberOfLines = 0;
    oldPrice.backgroundColor = [UIColor redColor];
    oldPrice.font = [UIFont systemFontOfSize:14.];
    oldPrice.text = @"$123";
    oldPrice.strikeThroughColor = [UIColor yellowColor];
    oldPrice.strikeThroughEnabled = YES;
    [_shareView addSubview:oldPrice];
    
    // 现价
    newPrice = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMidX(oldPrice.frame), oldPrice.frame.origin.y, 100, 30)];
    newPrice.numberOfLines = 0;
    newPrice.backgroundColor = [UIColor redColor];
    newPrice.font = [UIFont systemFontOfSize:14.];
    newPrice.text = @"$333";
    [_shareView addSubview:newPrice];
    
    // 取消按钮
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.backgroundColor = [UIColor clearColor];
    [cancelButton addTarget:self action:@selector(sheetViewDismiss) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.frame = CGRectMake(_shareView.frame.size.width - kCancelButton_WH, 0, kCancelButton_WH, kCancelButton_WH);
    [cancelButton setImage:[UIImage imageNamed:@"test"] forState:UIControlStateNormal];
    [_shareView addSubview:cancelButton];
    
    // 横线
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, productView.frame.size.height -.5f, _shareView.frame.size.width, .5f)];
    lineView.backgroundColor = [UIColor colorWithRed:172/256. green:172/256. blue:172/256. alpha:1];
    [_shareView addSubview:lineView];
}

// 创建颜色sku
-(void)createProductColorSKU{
    CGFloat originY_ColorSKU = 90;
    if (_sceneType == MMHOtherType){
        originY_ColorSKU = 0;
    } else if (_sceneType == MMHProductType){
        originY_ColorSKU = SheetColorSKU_H;
    }
    
    UILabel *colorLabel = [[UILabel alloc]init];
    colorLabel.backgroundColor = [UIColor clearColor];
    colorLabel.font = [UIFont systemFontOfSize:13.];
    colorLabel.text = @"颜色：";
    colorLabel.frame = CGRectMake(20, originY_ColorSKU + 5, 100, 30);
    [_shareView addSubview:colorLabel];

    // color scrollView
    colorSKUScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, originY_ColorSKU + 35, _shareView.frame.size.width, 60)];
    colorSKUScrollView.exclusiveTouch = YES;
    colorSKUScrollView.showsHorizontalScrollIndicator = NO;
    colorSKUScrollView.backgroundColor = [UIColor yellowColor];
    colorSKUScrollView.contentSize = CGSizeMake(465, colorSKUScrollView.frame.size.height);
    [_shareView addSubview:colorSKUScrollView];
    
    // scrollView  上面添加颜色按钮
    for (int colorIndex = 0 ;colorIndex < colorArr.count ; colorIndex ++){
        UIButton *colorButton = [UIButton buttonWithType: UIButtonTypeCustom];
        colorButton.backgroundColor = [colorArr objectAtIndex:colorIndex];
        colorButton.frame = CGRectMake(15 + (colorSKUButton_WH + 15) * colorIndex, 20, colorSKUButton_WH, colorSKUButton_WH);
        colorButton.stringTag = [NSString stringWithFormat:@"colorButton%i",colorIndex];
        [colorButton addTarget:self action:@selector(colorButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [colorSKUScrollView addSubview:colorButton];
    }
    
    // Line
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(colorSKUScrollView.frame),_shareView.frame.size.width, .5f)];
    lineView.backgroundColor = [UIColor colorWithRed:172/256. green:172/256. blue:172/256. alpha:1];
    [_shareView addSubview:lineView];

}

// create Size SKU
-(void)createSizeSKU{
    CGFloat originY_SizeSKU = 90;
    if (_sceneType == MMHOtherType){
        originY_SizeSKU = 0;
    } else if (_sceneType == MMHProductType){
        originY_SizeSKU = SheetSizeSKU_H;
    }
    
    UILabel *sizeLabel = [[UILabel alloc]init];
    sizeLabel.backgroundColor = [UIColor clearColor];
    sizeLabel.font = [UIFont systemFontOfSize:13.];
    sizeLabel.text = @"尺寸";
    sizeLabel.frame = CGRectMake(20, originY_SizeSKU + 5, 100, 30);
    [_shareView addSubview:sizeLabel];
    
    // size scrollView
    sizeSKUScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, originY_SizeSKU + 35, _shareView.frame.size.width, 60)];
    sizeSKUScrollView.exclusiveTouch = YES;
    sizeSKUScrollView.showsHorizontalScrollIndicator = NO;
    sizeSKUScrollView.backgroundColor = [UIColor greenColor];
    sizeSKUScrollView.contentSize = CGSizeMake(465, sizeSKUScrollView.frame.size.height);
    [_shareView addSubview:sizeSKUScrollView];
    
    // add size Button
    for (int sizeIndex = 0 ; sizeIndex < sizeArr.count ; sizeIndex ++){
        UIButton *sizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [sizeButton setTitle:[sizeArr objectAtIndex:sizeIndex] forState:UIControlStateNormal];
        sizeButton.backgroundColor = [UIColor redColor];
        sizeButton.stringTag = [NSString stringWithFormat:@"sizeButton%i",sizeIndex];
        sizeButton.frame = CGRectMake(15 + (sizeSKUButton_WH + 15) *sizeIndex , 20, sizeSKUButton_WH, sizeSKUButton_WH);
        [sizeButton addTarget:self action:@selector(sizeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [sizeSKUScrollView addSubview:sizeButton];
    }
    // line
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(sizeSKUScrollView.frame),_shareView.frame.size.width, .5f)];
    lineView.backgroundColor = [UIColor colorWithRed:172/256. green:172/256. blue:172/256. alpha:1];
    [_shareView addSubview:lineView];
}

// 增加数量row
-(void)createNumberCell{
    CGFloat originY_NumberSKU = 90;
    if (_sceneType == MMHOtherType){
        originY_NumberSKU = 0;
    } else if (_sceneType == MMHProductType){
        originY_NumberSKU = SheetNumberSKU_H;
    }
    
    _customerProductNumber = 1;
    
    UILabel *numberFixedLabel = [[UILabel alloc]init];
    numberFixedLabel.backgroundColor = [UIColor clearColor];
    numberFixedLabel.text = @"数量";
    numberFixedLabel.font = [UIFont systemFontOfSize:14.];
    numberFixedLabel.frame = CGRectMake(20, originY_NumberSKU + 5, 100, 30);
    [_shareView addSubview:numberFixedLabel];
    
    // 创建控件
    UIView *borderView = [[UIView alloc]init];
    borderView.layer.borderColor = [UIColor blackColor].CGColor;
    borderView.frame = CGRectMake(_shareView.frame.size.width - 120 , originY_NumberSKU + 10, 100, 30);
    borderView.layer.borderWidth = 2;
    borderView.backgroundColor = [UIColor redColor];
    [_shareView addSubview:borderView];
    
    // 创建button
    UIButton *cutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cutButton.backgroundColor = [UIColor clearColor];
    cutButton.stringTag = @"cutButton";
    [cutButton setTitle:@"-" forState:UIControlStateNormal];
    [cutButton addTarget:self action:@selector(numberOfAddAndCut:) forControlEvents:UIControlEventTouchUpInside];
    cutButton.frame = CGRectMake(0, 0, borderView.frame.size.width / 3., borderView.frame.size.height);
    [borderView addSubview:cutButton];
    
    // 创建numberLabel
    numberLabel = [[UILabel alloc]init];
    numberLabel.backgroundColor = [UIColor clearColor];
    numberLabel.font = [UIFont systemFontOfSize:13.];
    numberLabel.text = [NSString stringWithFormat:@"%li",(long)_customerProductNumber];
    numberLabel.frame = CGRectMake(CGRectGetMaxX(cutButton.frame), 0, borderView.frame.size.width / 3., borderView.frame.size.height);
    numberLabel.textAlignment = NSTextAlignmentCenter;
    [borderView addSubview:numberLabel];
    
    // 创建add button
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.backgroundColor = [UIColor clearColor];
    addButton.stringTag = @"addButton";
    [addButton setTitle:@"+" forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(numberOfAddAndCut:) forControlEvents:UIControlEventTouchUpInside];
    addButton.frame = CGRectMake(CGRectGetMaxX(numberLabel.frame), 0, borderView.frame.size.width / 3. , borderView.frame.size.height);
    [borderView addSubview:addButton];
    
    // line
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(borderView.frame) + 10,_shareView.frame.size.width, .5f)];
    lineView.backgroundColor = [UIColor colorWithRed:172/256. green:172/256. blue:172/256. alpha:1];
    [_shareView addSubview:lineView];

}



// 背景颜色渐入渐出
- (void)backGroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = .4f;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}


#pragma mark - ActionClick
// 手势进行隐藏
- (void)gestureRecognizerHandle:(UITapGestureRecognizer *)sender {
    [self dismissFromView:_showViewController];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak MMHActionSheetViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak MMHActionSheetViewController *weakVC = self;
    
    if (_sceneType != MMHProductType){
        [(AbstractViewController *)viewController hidesTabBar:NO animated:NO];
    }
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backGroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 隐藏tabBar
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
   self.backGroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

-(void)buttonClick{
    NSLog(@"123");
}

-(void)colorButtonClick:(UIButton *)sender{
    UIButton *colorButton = (UIButton *)sender;
    NSArray *colorArray=[colorButton.stringTag componentsSeparatedByString:@"colorButton"];
    if (colorArray.count == 2){
        NSLog(@"%@",[colorArray objectAtIndex:1]);
    }

}

-(void)sizeButtonClick:(UIButton *)sender{
    UIButton *sizeButton  = (UIButton *)sender;
    NSArray *sizeArray=[sizeButton.stringTag componentsSeparatedByString:@"sizeButton"];
    if (sizeArray.count == 2){
        NSLog(@"%@",[sizeArray objectAtIndex:1]);
    }
}

-(void)numberOfAddAndCut:(UIButton *)sender{
    UIButton *numberButton = (UIButton *)sender;
    if ([numberButton.stringTag isEqualToString:@"cutButton"]){
        if (_customerProductNumber <=1){
            _customerProductNumber = 1;
        } else {
            _customerProductNumber --;
        }
        numberLabel.text = [NSString stringWithFormat:@"%li",(long)_customerProductNumber];
        
    } else if ([numberButton.stringTag isEqualToString:@"addButton"]){
        _customerProductNumber ++;
        numberLabel.text = [NSString stringWithFormat:@"%li",(long)_customerProductNumber];
    }
}
@end
