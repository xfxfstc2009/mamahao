//
//  MMHPopView.m
//  MamHao
//
//  Created by SmartMin on 15/4/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPopView.h"


@interface MMHPopView()<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong)UITableView *popViewTableView;
@property (nonatomic,strong)NSMutableArray *dataSourceArray;
@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)UIView *tableBgView;
@end

@implementation MMHPopView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
        [self createDismissBackgroundView];
    }
    return self;
}

-(void)setUsingType:(MMHPopViewUsingType)usingType{
    _usingType = usingType;
    if (usingType == MMHUsingTypeProductGuarantees){                    // 商品详情
        [self arrayWithInit];
        [self createTableView];
        [self createHeaderViewForProductDetailGoodsTag];
        self.popViewTableView.orgin_y = [MMHTool contentofHeight:F4] + 2 *MMHFloat(15);
        self.popViewTableView.size_height -=  [MMHTool contentofHeight:F4] + 2 *MMHFloat(15);
        
    } else if (usingType == MMHUsingTypeOrderExpress){                  // 物流
        [self createViewWithOrder];
    } else if (usingType == MMHUsingTypeOrderShopInfo){                 // 订单详情
        [self arrayWithInit];
        [self createTableView];
        self.popViewTableView.scrollEnabled = NO;
    }
}

-(void)createTableView{
    // 创建tableViewBgView
    self.tableBgView = [[UIView alloc]init];
    self.tableBgView.backgroundColor = [UIColor whiteColor];
    self.tableBgView.frame = self.bounds;
    self.tableBgView.userInteractionEnabled = YES;
    [self addSubview:self.tableBgView];
    
    self.popViewTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
    self.popViewTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.popViewTableView.delegate = self;
    self.popViewTableView.dataSource = self;
    self.popViewTableView.showsVerticalScrollIndicator = NO;
    self.popViewTableView.scrollsToTop = YES;
    self.popViewTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.popViewTableView.backgroundColor = [UIColor clearColor];
    [self.tableBgView addSubview:self.popViewTableView];
}



-(void)arrayWithInit{
    self.dataSourceArray = [NSMutableArray array];

    if (self.usingType == MMHUsingTypeOrderShopInfo){
        NSArray *tempArr = @[@"门店信息"];
        [self.dataSourceArray addObjectsFromArray:tempArr];
    } else if (self.usingType == MMHUsingTypeProductGuarantees){
        [self.dataSourceArray addObjectsFromArray:self.goodsTag];
    }
}


#pragma mark - UITableViewDataSource 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArray.count ;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.usingType == MMHUsingTypeOrderShopInfo){
        static NSString *cellIdentifyWithShopInfo = @"cellIdentifyWithShopInfo";
        MMHOrderListShopInfoCell *cellWithShopInfo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithShopInfo];
        if (!cellWithShopInfo){
        
            cellWithShopInfo = [[MMHOrderListShopInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithShopInfo];
            cellWithShopInfo.userInteractionEnabled = NO;
            cellWithShopInfo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 创建一个电话button
            UIButton *phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
            phoneButton.backgroundColor = [UIColor clearColor];
            [phoneButton addTarget:self action:@selector(phoneButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            phoneButton.stringTag = @"phoneButton";
            // phoneButton _frame
            [self addSubview:phoneButton];
        }
        cellWithShopInfo.viewWidth = self.size_width;
        cellWithShopInfo.transferSinceTheMentionAddressModel = self.shopInfoModel;
        UIButton *phoneButton = (UIButton *)[self viewWithStringTag:@"phoneButton"];
        phoneButton.frame = cellWithShopInfo.phoneNumberButton.frame;
        return cellWithShopInfo;
    } else if (self.usingType == MMHUsingTypeProductGuarantees){
        static NSString *cellIdentify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 创建view
            MMHImageView *iconView = [[MMHImageView alloc]init];
            iconView.backgroundColor = [UIColor clearColor];
            iconView.frame = CGRectMake(MMHFloat(20), MMHFloat(15), MMHFloat(17), MMHFloat(17));
            iconView.stringTag = @"iconView";
            [cell addSubview:iconView];
            
            // 创建label
            UILabel *titleLabel = [[UILabel alloc]init];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.stringTag = @"titleLabel";
            titleLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
            titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [cell addSubview:titleLabel];
            
            // 创建subTitle
            UILabel *subTitleLabel = [[UILabel alloc]init];
            subTitleLabel.backgroundColor = [UIColor clearColor];
            subTitleLabel.stringTag = @"subTitle";
            subTitleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            subTitleLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            subTitleLabel.numberOfLines = 0;
            [cell addSubview:subTitleLabel];
            
        }
        // 赋值
        MMHProductDetailGoodsTagModel *singleGoodsTagModel = [self.dataSourceArray objectAtIndex:indexPath.row];
        MMHImageView *iconView = (MMHImageView *)[cell viewWithStringTag:@"iconView"];
        [iconView updateViewWithImageAtURL:singleGoodsTagModel.pic];
        
        // titleLabel
        UILabel *titleLabel = (UILabel *)[cell viewWithStringTag:@"titleLabel"];
        titleLabel.text = singleGoodsTagModel.view;
        CGSize titleOfSize = [singleGoodsTagModel.view sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(self.bounds.size.width - 2 * MMHFloat(20), CGFLOAT_MAX)];
        titleLabel.frame = CGRectMake(CGRectGetMaxX(iconView.frame) + MMHFloat(10), MMHFloat(15), self.bounds.size.width - MMHFloat(67), titleOfSize.height);
        
        //
        UILabel *subTitleLabel = (UILabel *)[cell viewWithStringTag:@"subTitle"];
        subTitleLabel.text = singleGoodsTagModel.data;
        CGSize contentOfSize = [singleGoodsTagModel.data sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(self.bounds.size.width - 2 * MMHFloat(20), CGFLOAT_MAX)];
        subTitleLabel.frame = CGRectMake(MMHFloat(20),CGRectGetMaxY(iconView.frame) + MMHFloat(10) , (self.bounds.size.width - 2 * MMHFloat(20)), contentOfSize.height);
        
        return cell;
    }
    return nil;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.usingType == MMHUsingTypeOrderShopInfo){
        self.size_height = [MMHOrderListShopInfoCell heightForCellWithShopInfo:self.shopInfoModel UsingClass:MMHShopInfoCellUsingClass1Normol] ;
       return [MMHOrderListShopInfoCell heightForCellWithShopInfo:self.shopInfoModel UsingClass:MMHShopInfoCellUsingClass1Normol];
    } else if (self.usingType == MMHUsingTypeProductGuarantees){
        MMHProductDetailGoodsTagModel *singleGoodsTagModel = [self.dataSourceArray objectAtIndex:indexPath.row];
        NSString *content = singleGoodsTagModel.data;
        CGSize contentOfSize = [content sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(self.bounds.size.width - 2 * MMHFloat(20), CGFLOAT_MAX)];
        return MMHFloat(15) + MMHFloat(17) + MMHFloat(10) + MMHFloat(5) + contentOfSize.height;
    } else {
        return MMHFloat(44);
    }
}



#pragma mark - 订单页面使用
-(void)createViewWithOrder{
    UIView *alertView = [[UIView alloc]init];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.frame = self.bounds;
    [self addSubview:alertView];
    
    // 创建view
    for (int i = 0 ; i < 4 ; i++){
        UILabel *smartLabel = [[UILabel alloc]init];
        smartLabel.text = @"我是测试:我是测试:";
        smartLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(20) + i * 15, self.bounds.size.width - 2 *MMHFloat(11), 10);
        smartLabel.font = [UIFont systemFontOfSize:12.];
        [alertView addSubview:smartLabel];
    }
    
    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(MMHFloat(0), 100, self.bounds.size.width, .5f);
    [alertView addSubview:lineView];
    
    // 创建btn
    UIButton *iKnowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [iKnowButton setTitle:@"我知道了 " forState:UIControlStateNormal];
    [iKnowButton setTitleColor: [UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    [iKnowButton addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
    iKnowButton.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(lineView.frame) + MMHFloat(20), self.bounds.size.width - 2 * MMHFloat(11), MMHFloat(50));
    iKnowButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [alertView addSubview:iKnowButton];
}



#pragma mark createHeaderView
-(void)createHeaderViewForProductDetailGoodsTag{
    // 创建View
    UIView *productDetailHeaderView = [[UIView alloc]init];
    productDetailHeaderView.backgroundColor = [UIColor clearColor];
    [self.tableBgView addSubview:productDetailHeaderView];
    
    UILabel *danbaoLabel = [[UILabel alloc]init];
    danbaoLabel.backgroundColor = [UIColor whiteColor];
    danbaoLabel.textColor = [UIColor hexChangeFloat:@"9877b6"];
    danbaoLabel.text = @"服务担保";
    danbaoLabel.font = F4;
    danbaoLabel.frame = CGRectMake(0, MMHFloat(15), self.bounds.size.width,[MMHTool contentofHeight:F4]);
    danbaoLabel.textAlignment = NSTextAlignmentCenter;
    [productDetailHeaderView addSubview:danbaoLabel];
    
    productDetailHeaderView.frame = CGRectMake(0, 0, self.bounds.size.width, [MMHTool contentofHeight:danbaoLabel.font] + 2 *MMHFloat(15));
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, productDetailHeaderView.size_height - .5f, self.bounds.size.width, .5f);
    [productDetailHeaderView addSubview:lineView];
}

#pragma mark - buttonClick
-(void)phoneButtonClick:(UIButton *)sender {                 // 拨打电话
    self.callBlock(self.shopInfoModel.telephone);
}

#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}

- (void)viewShow {
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);

    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}


@end
