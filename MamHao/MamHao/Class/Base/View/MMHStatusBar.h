//
//  MMHStatusBar.h
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHStatusBar : UIWindow
@property (nonatomic,strong)UILabel *messageLabel;
@property (nonatomic,strong)UIImageView *successImageView;

// Signleton
+ (MMHStatusBar *)sharedInstance;

-(void)showMessage:(NSString *)message;
-(void)statusBarWithHide;

@end
