//
//  MMHExpandableView.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHExpandableView;


@protocol MMHExpandableViewDelegate <NSObject>

- (void)expandableViewHeightDidChange:(MMHExpandableView *)expandableView;

@end


@interface MMHExpandableView : UIView

@property (nonatomic, weak) id<MMHExpandableViewDelegate> delegate;

@property (nonatomic) UIEdgeInsets contentInsets;

@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;

- (instancetype)initWithFrame:(CGRect)frame summaryText:(NSString *)summaryText fullText:(NSString *)fullText;

@end
