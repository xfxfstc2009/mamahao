//
//  MMHAccount.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAccount.h"


@implementation MMHAccount


- (NSString *)description
{
    NSMutableString *string = [NSMutableString string];
    [string appendFormat:@"%@ = %@\n", @"memberID", self.memberID];
    [string appendFormat:@"%@ = %@\n", @"name", self.name];
    [string appendFormat:@"%@ = %@\n", @"nickname", self.nickname];
    [string appendFormat:@"%@ = %@\n", @"token", self.token];
    return string;
}


- (BOOL)shouldAutoLogin
{
    return YES;
}


- (BOOL)isActivated
{
    return YES;
}


- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.memberID forKey:@"memberID"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.nickname forKey:@"nickname"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
    [aCoder encodeObject:self.password forKey:@"password"];
}


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [self init];
    if (self) {
        self.memberID = [aDecoder decodeObjectForKey:@"memberID"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.nickname = [aDecoder decodeObjectForKey:@"nickname"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.phoneNumber = [aDecoder decodeObjectForKey:@"phoneNumber"];
        self.password = [aDecoder decodeObjectForKey:@"password"];
    }
    return self;
}


@end
