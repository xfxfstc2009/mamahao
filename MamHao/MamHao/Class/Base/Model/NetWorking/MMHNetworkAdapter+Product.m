//
//  MMHNetworkAdapter+Product.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Product.h"
#import "MMHFilterTerm.h"
#import "MMHCategory.h"
#import "MMHProductDetailModel.h"
#import "MMHShopInfo.h"
#import "MMHFilter.h"
#import "MamHao-Swift.h"
#import "MMHCurrentLocationModel.h"



@implementation MMHNetworkAdapter (Product)


- (void)fetchTermsForFilter:(MMHFilter *)filter succeededHandler:(void(^)(FilterTermInfo *termInfo))succeededHandler failedHandler:(void(^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters addEntriesFromDictionary:[filter parametersForTermInfo]];

    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/basic/getGoodsListFilter.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      FilterTermInfo *info = [[FilterTermInfo alloc] initWithDictionary:responseJSONObject];
                                      succeededHandler(info);
                                  } failedBlock:^(NSError *error) {
                failedHandler(error);
            }];
}


- (void)fetchProductListWithFilter:(MMHFilter *)filter
                              page:(NSInteger)page
                          pageSize:(NSInteger)pageSize
                              from:(id)requester
                  succeededHandler:(void(^)(MMHProductsListModel *productsListModel))succeededHandler
                     failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters addEntriesFromDictionary:[filter parameters]];
    parameters[@"page"] = @(page + 1);
    parameters[@"count"] = @(pageSize);

    NSString *api = @"shop/basic/searchGoodsList.htm";
    if (filter.module == MMHFilterModuleSearching) {
        api = @"client/search/search.htm";
    }
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    [engine postWithAPI:api
             parameters:parameters
                   from:requester
    responseObjectClass:[MMHProductsListModel class]
   responseObjectKeyMap:@{}
         succeededBlock:^(id responseObject, id responseJSONObject) {
             succeededHandler(responseObject);
         } failedBlock:^(NSError *error) {
                failedHandler(error);
            }];
}


- (void)fetchProductDetailWithProduct:(MMHProduct<MMHProductProtocol> *)product
                                 from:(id)requester
                     succeededHandler:(void(^)(MMHProductDetailModel *productDetail))succeededHandler
                        failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters addEntriesFromDictionary:[product parameters]];
    NSDictionary *keymap = @{@"graphicDetailsURLString": @"graphicDetails", @"specialistPortraitURLString": @"specialistPhoto"};
    
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
   [engine postWithAPI:@"shop/basic/searchTemplateDetail.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:[MMHProductDetailModel class]
                            responseObjectKeyMap:keymap
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHProductDetailModel *productDetail = (MMHProductDetailModel *)responseObject;
                                      productDetail.module = product.module;
                                      NSArray *specsArray = responseJSONObject[@"specs"];
                                      [productDetail configureSpecsWithArray:specsArray];
                                      productDetail.bindsShop = product.bindsShop;
                                      productDetail.shopId = [product shopId];
                                      if ([product respondsToSelector:@selector(isBeanProduct)]) {
                                          productDetail.isBeanProduct = [product isBeanProduct];
                                      }
                                      else {
                                          productDetail.isBeanProduct = NO;
                                      }
                                      succeededHandler(productDetail);
                                  } failedBlock:^(NSError *error) {
                failedHandler(error);
            }];
}


#pragma mark  获取商品详情的口碑列表
-(void)fetchWomOfProductDetailWithMemberId:(NSInteger)memberId goodsTemplated:(NSInteger)goodsTemplateId page:(NSInteger)page count:(NSInteger)count from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    NSDictionary *parameterDic = @{@"memberId":@(memberId), @"goodsTemplated":@(goodsTemplateId), @"page":@(page), @"count":@(count)};
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"/comment/basic/queryTemplateCommentListByChart.htm" parameters:parameterDic from:nil responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark - 类目购物须知
-(void)fetchProductListShoppingTipsWithCategory:(MMHCategory *)category
                                           from:(id)requester
                               succeededHandler:(void (^)(MMHShoppingTipsModel *productDetail))succeededHandler
                                  failedHandler:(MMHNetworkFailedHandler)failedHandler{

    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/basic/shoppingTips.htm" parameters:@{@"typeThirdId": category.categoryID} from:nil responseObjectClass:[MMHShoppingTipsModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark - 猜你喜欢
-(void)fetchProductDetailGuessYouLikeWithProduct:(MMHProduct<MMHProductProtocol> *)product from:(id)requester succeededHandler:(void(^)(MMHGuessYouLikeListModel *guessYouLikeModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    
    NSMutableDictionary *guessYouLikeDic = [NSMutableDictionary dictionary];
    MMHCategory *category = [product category];
    if (category.categoryID){
        [guessYouLikeDic setObject:category.categoryID forKey:@"typeThirdId"];
    }
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/basic/guessYouLike.htm" parameters:guessYouLikeDic from:requester responseObjectClass:[MMHGuessYouLikeListModel class] responseObjectKeyMap:nil
    succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


- (void)fetchProductDetailSpecParameterWithProductDetailSpecFilter:(ProductSpecFilter *)specFilter
                                                  succeededHandler:(void(^)(NSDictionary *dictionary))succeededHandler
                                                     failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"templateId"] = specFilter.templateID;
    parameters[@"inlet"] = @(specFilter.module);
    parameters[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    parameters[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    parameters[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    parameters[@"itemId"] = [specFilter selectedItemID];
    [parameters setNullableObject:specFilter.shopId forKey:@"shopId"];
    [parameters setNullableObject:specFilter.companyId forKey:@"companyId"];
    [parameters setNullableObject:specFilter.warehouseId forKey:@"warehouseId"];
    parameters[@"deviceId"] = [UIDevice deviceID];
    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/basic/searchItemDetail.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      succeededHandler(responseJSONObject);
                                  } failedBlock:failedHandler];
}


- (void)fetchActualPriceForProduct:(MMHProduct *)product from:(id)requester succeededHandler:(void(^)(MMHPrice price))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"styleNumIds"] = product.templateId;
    parameters[@"deviceId"] = [UIDevice deviceID];
    parameters[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    parameters[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    parameters[@"areaNumId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    [engine postWithAPI:@"shop/basic/getProPriceByStyle.htm"
             parameters:parameters
                   from:requester
    responseObjectClass:nil
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             NSArray *prices = responseJSONObject[@"data"];
             for (NSDictionary *aPrice in prices) {
                 if ([[aPrice stringForKey:@"templateId"] isEqualToString:product.templateId]) {
                     MMHPrice price = [aPrice[@"minPrice"] priceValue];
                     succeededHandler(price);
                     return;
                 }
             }
             failedHandler(nil);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}



@end
