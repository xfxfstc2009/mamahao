//
//  MMHNetworkAdapter+Chatting.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Chatting.h"
#import "MMHWaiter.h"
#import "MMHAccountSession.h"


@implementation MMHNetworkAdapter (Chatting)


- (void)fetchWaiterListFrom:(id)requester succeededHandler:(void(^)(NSArray *waiters))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"im/service/list.htm"
                                      parameters:nil
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:@{}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      NSArray *waiters = responseJSONObject[@"datas"];
                                      succeededHandler(waiters);
                                  } failedBlock:failedHandler];
}


- (void)addFriendWithMyUserID:(NSString *)myUserID waiterUserID:(NSString *)waiterUserID from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"username"] = myUserID;
    parameters[@"waiter"] = waiterUserID;
    NSString *phone = [MMHAccountSession currentSession].phone;
    if (phone.length != 0) {
        parameters[@"nickname"] = phone;
    }
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"im/service/addFriend.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {

                                  } failedBlock:failedHandler];
}


@end
