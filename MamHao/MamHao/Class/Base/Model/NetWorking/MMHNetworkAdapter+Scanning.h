//
//  MMHNetworkAdapter+Scanning.h
//  MamHao
//
//  Created by SmartMin on 15/6/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【扫码】接口

#import "MMHNetworkAdapter.h"
#import "MMHOrderWithConfirmationModel.h"                   // 去结算
@interface MMHNetworkAdapter (Scanning)

#pragma mark  扫码订单
-(void)fetchModelScanningWithParameter:(NSDictionary *)parameter from:(id)requester succeededHandler:(void(^)(MMHOrderWithConfirmationModel *orderWithConfirmationModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
@end
