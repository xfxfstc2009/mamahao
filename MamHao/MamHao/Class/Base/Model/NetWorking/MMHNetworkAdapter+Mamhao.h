//
//  MMHNetworkAdapter+Mamhao.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"
#import "MMHMamhaoData.h"


@class MonthAge;
@class MMHMamhaoBeanSpecialProduct;
@class MMHMamhaoMamhaoFeaturedProduct;
@class MMHMamhaoUserFeaturedData;
@class MMHMamhaoShop;


@interface MMHNetworkAdapter (Mamhao)

- (void)fetchMamhaoBeanSpecialDataFrom:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoBeanSpecialProduct *beanSpecialProduct, NSError *error))completion;
- (void)fetchMamhaoShopsFrom:(id)requester completion:(void (^)(BOOL succeeded, NSArray *shops, NSError *error))completion;
- (void)fetchMamhaoCustomersNearShop:(MMHMamhaoShop *)shop from:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion;
- (void)fetchMamhaoMamhaoFeaturedProductFrom:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoMamhaoFeaturedProduct *mamhaoFeaturedProduct, NSError *error))completion;
- (void)fetchMamhaoUserFeaturedDataWithMonthAge:(MonthAge *)monthAge from:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoUserFeaturedData *userFeaturedData, NSError *error))completion;

- (void)fetchMamhaoFeaturedProductsForMonthAge:(MonthAge *)monthAge from:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion;
- (void)fetchMamhaoUserInfoFrom:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion;

- (void)fetchMamhaoActivityImagesFrom:(id)requester completion:(void (^)(BOOL succeeded, NSString *beanGameImageURLString, NSString *categoryImageURLString, NSError *error))completion;
@end
