//
//  MMHNetworkAdapter+Login.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"
#import "MMHAccount.h"


@interface MMHNetworkAdapter (Login)

#pragma mark 登录
- (void)loginWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
- (void)loginWithPhoneNumber:(NSString *)phoneNumber verificationCode:(NSString *)verificationCode from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 注册
- (void)registerWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 绑定三方信息
-(void)bindingWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password extId:(NSString *)extId extType:(NSInteger)extType vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 获取验证码接口【短信验证码】 - - 注册 - - 手机没有被注册才发送短信
-(void)sendSmsForNoExistsWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 发送短信验证码 - 用户已经注册
-(void)sendSmsForExistsWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void (^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
-(void)sendSmsForAllWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 验证验证码
-(void)validateSmsWithPhoneNumber:(NSString *)phoneNumber vCode:(NSString *)vCode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 忘记密码
-(void)resetPasswordWithPhoneNumber:(NSString *)phoneNumber newPassword:(NSString *)password vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
