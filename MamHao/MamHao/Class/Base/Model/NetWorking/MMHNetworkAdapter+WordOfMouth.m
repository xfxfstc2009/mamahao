//
//  MMHNetworkAdapter+WordOfMouth.m
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHPublicPraiseModel.h"
#import "MMHProductParameterModel.h"
@implementation MMHNetworkAdapter (WordOfMouth)


- (void)fetchPublicPrasieWithTemplateId:(NSString *) templateId page:(NSInteger)page pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle{
    
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    NSMutableDictionary *parameterDic = [NSMutableDictionary dictionary];
    if (templateId) {
        [parameterDic setObject:templateId forKey:@"templateId"];
    }
    [parameterDic setObject:@(page) forKey:@"page"];
    [parameterDic setObject:@(pageSize) forKey:@"pageSize"];
    [engine postWithAPI:@"comment/basic/queryGoodsTemplateCommentList.htm" parameters:parameterDic from:requester responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        //在这里传入参数，调用succeedHandle
        succeededHandler(responseJSONObject);
        
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];

}

- (void)fetchTotalCountOfPublicPrasieWithGoodsTemplatedId:(NSString*) templateId from :(id)requester suuccedHandler:(void(^)(NSMutableArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle{
    NSMutableDictionary *parameterDic = [NSMutableDictionary dictionary];
    if (templateId) {
        [parameterDic setObject:templateId forKey:@"templateId"];
    }
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"comment/basic/queryGoodsTemplateCommentChart.htm" parameters:parameterDic from:requester responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject[@"ratingCount"]);
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];

}

- (void)praiseWithShopId:(NSString *)shopId orderNo:(NSString *)orderNO  itemId:(NSString *)itemId templateId:(NSString *)templateId star:(NSInteger)star content:(NSString *)content pics:(NSString *)pics from:(id)requester succedHandler:(void (^)(NSDictionary *))succededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle{
    
    NSMutableDictionary *parameterDic = [NSMutableDictionary dictionary];
    if (shopId) {
        [parameterDic setObject:shopId forKey:@"shopId"];
    }
    if (templateId) {
        [parameterDic setObject:templateId forKey:@"templateId"];
    }
    if (orderNO) {
          [parameterDic setObject:orderNO forKey:@"orderNo"];
    }
    if (star) {
            [parameterDic setObject:@(star) forKey:@"star"];
    }
    if (content) {
         [parameterDic setObject:content forKey:@"content"];
    }
   
    if (itemId) {
        [parameterDic setObject:itemId forKey:@"itemId"];
    }
    if (pics.length) {
        [parameterDic setValue:pics forKey:@"pics"];
    }
    
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"comment/basic/commentGoodsTemplate.htm" parameters:parameterDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
       succededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];
}


- (void)additionalPraiseWithShopId:(NSString *)shopId warehouseId:(NSString *)warehouseId orderNo:(NSString *)orderNo itemId:(NSString *)itemId templateId:(NSString *)templateId bufferContentContent:(NSString *)content bufferPics:(NSString *)pics from:(id)requester succedHandler:(void (^)(NSDictionary *dic))succededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle {
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (warehouseId) {
        [dic setObject:warehouseId forKey:@"warehouseId"];
    }
    if (shopId) {
        [dic setObject:shopId forKey:@"shopId"];
    }
    if (orderNo) {
        [dic setObject:orderNo forKey:@"orderNo"];
    }
    if (itemId) {
        [dic setObject:itemId forKey:@"itemId"];
    }
    if (templateId) {
        [dic setObject:templateId forKey:@"templateId"];
    }
    if (content) {
        [dic setObject:content forKey:@"bufferContent"];
    }
    if (pics) {
        [dic setObject:pics forKey:@"bufferPics"];
    }
   #ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"comment/basic/reCommentGoodsTemplate.htm" parameters:dic from:requester responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];

}
-  (void)fetchGoodsOfOrderWithOrderNo:(NSString *)orderNo page:(NSInteger)page count:(NSInteger)count from:(id)request succeedHandler:(void (^)(NSDictionary *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (orderNo) {
        [parameters setObject:orderNo forKey:@"orderNo"];
    }
  //  [parameters setObject:@(page) forKey:@"page"];
 //   [parameters setObject:@(count) forKey:@"count"];
  
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"order/basic/queryOrderGoodsList.htm" parameters:parameters from:request responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        
          succeededHandler(responseJSONObject);
        
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];
}

- (void)praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger)serveStar deliverySpeedStar:(NSInteger)deliverySpeedStar  commentContent:(NSString *)commentContent  from:(id)request succeedHandler:(void (^)(NSDictionary *))succeededHandler faileHandler:(MMHNetworkFailedHandler)failedHandle{
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:orderNo forKey:@"orderNo"];
    [parameters setObject:@(serveStar) forKey:@"serveStar"];
    [parameters setObject:commentContent forKey:@"commentContent"];
#ifdef DEBUG
   
#else
    if (deliverySpeedStar == 10) {
        
    }else{
        [parameters setObject:@(deliverySpeedStar) forKey:@"deliverySpeedStar"];
    }

#endif
    
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"/comment/basic/commentOrder.htm" parameters:parameters from:request responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];
}

- (void)fetchWomOfOrderWithMemberId:(NSInteger)memberId orderNo:(NSInteger)orderNo from:(id)request succeedHandler:(void (^)(NSDictionary *))succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle{
    NSDictionary *dic = @{@"memberId":@(memberId), @"orderNo":@(orderNo)};
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"/comment/basic/queryOrderComment.htm" parameters:dic from:request responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeedHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failerHandle(error);
    }];
}
- (void)fetchImageAndTextDetailInfoWithTemplatedId:(NSString *)templateId succeedHandler:(void (^)(NSString *))succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle {
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.137"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    if (!templateId) {
        templateId = @"";
    }
    [engine postWithAPI:@"shop/basic/getGoodsStyleHtml.htm" parameters:@{@"templateId":templateId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        //
        succeedHandler(responseJSONObject[@"staticUrl"]);
    } failedBlock:^(NSError *error) {
        
        //
        failerHandle(error);
    }];
}
- (void)fetchShopPraiseWithShopId:(NSString *)shopId succeedHandler:(void (^)(NSDictionary  *))succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle{
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
    shopId = @"14004";
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"shop/basic/getShopEvaluationInfo.htm" parameters:@{@"shopId":shopId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeedHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failerHandle(error);
    }];
}

- (void)fetchProductParametersWithTemplateId:(NSString *)templateId succeedHandler:(void(^)(NSDictionary *dic)) succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
#ifdef DEBUG_FISH
#else
#endif
    if (templateId) {
        [parameters setObject:templateId forKey:@"templateId"];
    }
     [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/basic/getGoodsParams.htm" parameters:parameters from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
         succeedHandler(responseJSONObject);
     } failedBlock:^(NSError *error) {
         failerHandle(error);
     }];
}
@end
