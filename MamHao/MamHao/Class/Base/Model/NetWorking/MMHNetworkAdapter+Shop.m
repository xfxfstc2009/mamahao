//
//  MMHNetworkAdapter+Shop.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Shop.h"
#import "MMHShopDetail.h"
#import "MMHShopInfo.h"


@implementation MMHNetworkAdapter (Shop)

- (void)fetchShopDetailWithShopId:(NSString *)shopId memberId:(NSInteger)memberId start:(NSInteger)start end:(NSInteger)end from:(id)requester succeededHandler:(void (^)(MMHShopDetail *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
    [parameters setValue:shopId forKey:@"shopId"];
    [parameters setValue:@(1) forKey:@"start"];
    [parameters setValue:@(5) forKey:@"end"];
#else
    [parameters setValue:shopId forKey:@"shopId"];
    [parameters setValue:@(start) forKey:@"start"];
    [parameters setValue:@(end) forKey:@"end"];
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    
    
    [engine postWithAPI:@"shop/basic/queryShopDetail.htm" parameters:parameters from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        NSDictionary *keyMap = @{@"imageURLStrings": @"images"};
        MMHShopDetail *shopDetail = [[MMHShopDetail alloc] initWithJSONDict:responseJSONObject keyMap:keyMap];
        succeededHandler(shopDetail);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


- (void)fetchShopServiceWithShopDetail:(MMHShopDetail *)shopDetail from:(id)requester succeededHandler:(void (^)(MMHShopService *shopService))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"shop/basic/queryShopService.htm" parameters:@{@"shopId":shopDetail.shopId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        MMHShopService *shopService = [[MMHShopService alloc] initWithJSONDict:responseJSONObject];
        succeededHandler(shopService);
    } failedBlock:^(NSError *error) {
        //
    }];

}

- (void)fetchfilterConditionWithShopInfo:(MMHShopDetail *)shopInfo from:(id)requester succeededHandler:(void (^)(MMHShopDetail *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
  //TODO -获取筛选条件
}
@end
