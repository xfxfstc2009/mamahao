//
//  MMHNetworkAdapter+Express.h
//  MamHao
//
//  Created by SmartMin on 15/6/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"
#import "MMHExpressListModel.h"
@interface MMHNetworkAdapter (Express)

#pragma mark 获取接口数据
-(void)sendRequestToGetExpressOrderWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)(MMHExpressListModel *expressListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
