//
//  MMHNetworkAdapter+Express.m
//  MamHao
//
//  Created by SmartMin on 15/6/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Express.h"

@implementation MMHNetworkAdapter (Express)

#pragma mark 获取物流详情
-(void)sendRequestToGetExpressOrderWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)(MMHExpressListModel *expressListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/getOrderLogisticsList.htm" parameters:@{@"orderNo":orderNo} from:requester responseObjectClass:[MMHExpressListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

@end
