//
//  MMHNetworkAdapter+Cart.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Cart.h"
#import "MMHCartData.h"
#import "MMHCartItem.h"
#import "MMHProductDetailModel.h"


@implementation MMHNetworkAdapter (Cart)

- (void)buyAgainAddToCart:(MMHShopCartModel *)shopCartModel  succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler{
    
}


- (void)addProductToCart:(MMHProductDetailModel *)productDetail succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    ProductSpecParameter *specParameter = productDetail.specFilter.selectedSpecParameter;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setNullableObject:specParameter.itemID forKey:@"itemId"];
    [parameters setNullableObject:productDetail.templateId forKey:@"templateId"];
    [parameters setNullableObject:productDetail.shopId forKey:@"shopId"];
    [parameters setNullableObject:productDetail.companyId forKey:@"companyId"];
    [parameters setNullableObject:productDetail.warehouseId forKey:@"warehouseId"];
    parameters[@"isBindShop"] = @(productDetail.bindsShop);
    [parameters setNullableObject:@(productDetail.specFilter.quantity) forKey:@"count"];
    parameters[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;

    MMHID cartID = [MMHCartData cartID];
    if (cartID != 0) {
        parameters[@"cartId"] = @(cartID);
    }

    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/addCartItem.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      [MMHCartData setCartID:[responseJSONObject[@"cartId"] MMHIDValue]];
                                      succeededHandler();
                                  } failedBlock:failedHandler];
}


- (void)fetchCartDataFrom:(id)requester succeededHandler:(void(^)(MMHCartData *cartData))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"cartData" ofType:@"json"];
//        NSString *jsonString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
//        NSDictionary *jsonDictionary = [jsonString jsonObject];
//        MMHCartData *data = [[MMHCartData alloc] initWithJSONDict:jsonDictionary keyMap:@{@"cartID": @"_id"}];
//        [data configureGroupsWithDictionary:jsonDictionary];
//        [data configurePricesWithDictionary:jsonDictionary];
//        succeededHandler(data);
//    });
//    return;
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    MMHID cartID = [MMHCartData cartID];
    if (cartID != 0) {
        parameters[@"cartId"] = @(cartID);
    }
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/getCart.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:[MMHCartData class]
                            responseObjectKeyMap:@{@"cartID": @"_id"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHCartData *data = (MMHCartData *)responseObject;
                                      [data configureGroupsWithDictionary:responseJSONObject];
                                      [data configurePricesWithDictionary:responseJSONObject];
                                      succeededHandler(responseObject);

                                  } failedBlock:^(NSError *error) {
                failedHandler(error);
            }];
}


- (void)changeQuantityOfItem:(MMHCartItem *)cartItem inCart:(MMHCartData *)cart to:(NSInteger)toValue succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters addEntriesFromDictionary:[cartItem parameters]];

    parameters[@"cartId"] = @(cart.cartID);
    parameters[@"count"] = @(toValue);
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/updateCartItemCount.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:[MMHCartData class]
                            responseObjectKeyMap:@{@"cartID": @"_id"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHCartData *data = (MMHCartData *)responseObject;
                                      [data configureGroupsWithDictionary:responseJSONObject];
                                      [data configurePricesWithDictionary:responseJSONObject];
                                     succeededHandler(data);
                                  } failedBlock:^(NSError *error) {
                failedHandler(error);
            }];
}


- (void)deleteItems:(NSArray *)items inCart:(MMHCartData *)cart succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"cartId"] = @(cart.cartID);

    NSArray *jsonArray = [items transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
        MMHCartItem *cartItem = (MMHCartItem *)originalObject;
        return [cartItem parameters];
    }];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonArray options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    parameters[@"jsonTerm"] = jsonString;

    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/removeCartItem.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:[MMHCartData class]
                            responseObjectKeyMap:@{@"cartID": @"_id"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHCartData *data = (MMHCartData *)responseObject;
                                      [data configureGroupsWithDictionary:responseJSONObject];
                                      [data configurePricesWithDictionary:responseJSONObject];
                                      succeededHandler(data);
                                  } failedBlock:failedHandler];
}


//- (void)fetchActivitiesForCart:(MMHCartData *)cart from:(id)requester succeededHandler:(void (^)(NSDictionary *responseJSONDictionary))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
//{
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"cartId"] = @(cart.cartID);
//
//    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/queryPlatformActivity.htm"
//                                      parameters:parameters
//                                            from:requester
//                             responseObjectClass:nil
//                            responseObjectKeyMap:nil
//                                  succeededBlock:^(id responseObject, id responseJSONObject) {
//                                      succeededHandler(responseJSONObject);
//                                  } failedBlock:^(NSError *error) {
//                failedHandler(error);
//            }];
//}


- (void)changeChosenStatusOfItem:(MMHCartItem *)cartItem inCart:(MMHCartData *)cart from:(id)requester succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters addEntriesFromDictionary:[cartItem parameters]];
    parameters[@"cartId"] = @(cart.cartID);
    parameters[@"isSelected"] = @(cartItem.chosen);

    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/selectedCart.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:[MMHCartData class]
                            responseObjectKeyMap:@{@"cartID": @"_id"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHCartData *data = (MMHCartData *)responseObject;
                                      [data configureGroupsWithDictionary:responseJSONObject];
                                      [data configurePricesWithDictionary:responseJSONObject];
                                      succeededHandler(data);
                                  } failedBlock:failedHandler];
}


- (void)changeChooseAllStatusInCart:(MMHCartData *)cart from:(id)requester succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"cartId"] = @(cart.cartID);
    
    NSInteger selectAll = 2;
    NSInteger deselectAll = 3;
    BOOL alreadyChosen = [cart allItemChosen];
    if (alreadyChosen) {
        parameters[@"isSelected"] = @(deselectAll);
    }
    else {
        parameters[@"isSelected"] = @(selectAll);
    }
    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"shop/cart/selectedCart.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:[MMHCartData class]
                            responseObjectKeyMap:@{@"cartID": @"_id"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHCartData *data = (MMHCartData *)responseObject;
                                      [data configureGroupsWithDictionary:responseJSONObject];
                                      [data configurePricesWithDictionary:responseJSONObject];
                                      succeededHandler(data);
                                  } failedBlock:failedHandler];
}


@end
