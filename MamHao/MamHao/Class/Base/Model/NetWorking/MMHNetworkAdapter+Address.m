//
//  MMHNetworkAdapter+Address.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Address.h"

@implementation MMHNetworkAdapter (Address)

#pragma mark  1 创建新收获地址
-(void)sendRequestCreateAddressWithAreaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat from:(id)requester succeededHandler:(void(^)(NSString *deliveryAddressId))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    NSDictionary *parametersDic = @{@"areaId":areaId,@"gpsAddr":gpsAddress,@"addrDetail":addressDetail,@"consignee":cosignee,@"phone":phone,@"isDefault":isDefault == 1?@"1":@"0",@"lng":[NSString stringWithFormat:@"%f",lng],@"lat":[NSString stringWithFormat:@"%f",lat]};
    
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/delivery/addDeliveryAddr.htm" parameters:parametersDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        NSString *deliveryAddressId =  [responseJSONObject objectForKey:@"deliveryAddrId"];
        succeededHandler(deliveryAddressId);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 2.修改收获地址
-(void)sendRequestUpdateAddressWithDeliveryAddrId:(NSString *)deliveryAddrId areaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    NSDictionary *parametersDic = @{@"deliveryAddrId":deliveryAddrId,@"areaId":areaId,@"addrDetail":addressDetail,@"gpsAddr":gpsAddress,@"consignee":cosignee,@"phone":phone,@"isDefault":isDefault == 1?@"1":@"0",@"lng":[NSString stringWithFormat:@"%f",lng],@"lat":[NSString stringWithFormat:@"%f",lat]};
    
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/delivery/updateDeliveryAddr.htm" parameters:parametersDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 3.删除收获地址
-(void)sendRequestDeleteAddressWithDeliveryAddrId:(NSString *)deliveryAddressId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
        NSDictionary *parametersDic = @{@"deliveryAddrId":deliveryAddressId};
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/delivery/delDeliveryAddr.htm" parameters:parametersDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 4. 获取收货地址
-(void)sendRequestGetAddressListFrom:(id)requester succeededHandler:(void(^)(MMHAddressListModel *addressList))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/delivery/getDeliveryAddr.htm" parameters:nil from:requester responseObjectClass:[MMHAddressListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 5.修改默认地址
-(void)fetchModelUpdateNormolAddressWithAddressId:(NSString *)addressId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    if (!addressId.length){
        [[UIAlertView alertViewWithTitle:nil message:@"亲爱的妈妈，获取不到该地址，请稍后再试" buttonTitles:@[@"确定"] callback:nil]show];
        return;
    }
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/updateDeliveryAddr.htm" parameters:@{@"deliveryAddrId":addressId,@"isDefault":@"1"} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    }
    failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}




-(void)sendRequestGetNormolAddressFrom:(id)requester succeededHandler:(void(^)(MMHAddressSingleModel *addressSingleCityModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/delivery/getDefaultDeliveryAddr.htm" parameters:nil from:requester responseObjectClass:[MMHAddressSingleModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

@end
