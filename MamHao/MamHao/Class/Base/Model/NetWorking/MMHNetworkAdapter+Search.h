//
//  MMHNetworkAdapter+Search.h
//  MamHao
//
//  Created by fishycx on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"

@interface MMHNetworkAdapter (Search)
/**
 * 搜索热词
 */
- (void)getHotSearchWordFrom :(id)requester succedHandler:(void(^)(NSMutableArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;

/**
 *  搜索商品列表信息
 */

 @end
