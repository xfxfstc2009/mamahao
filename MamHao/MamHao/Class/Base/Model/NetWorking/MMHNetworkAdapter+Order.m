//
//  MMHNetworkAdapter+Order.m
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Order.h"
#import "MMHCartData.h"


@implementation MMHNetworkAdapter (Order)

#pragma mark 再次购买
-(void)sendRequestBuyAgain:(NSString *)orderNo items:(NSString *)items from:(id)requester succeededHandler:(void(^)(MMHExpressModel *expressModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/buyAgain.htm" parameters:@{@"orderNo":orderNo,@"items":items} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        [MMHCartData setCartID:[responseJSONObject[@"cartId"] MMHIDValue]];
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark 修改物流信息
-(void)sendRequestToUpdateExpressInfoWithRefundLineId:(NSString *)refundLineId wayBillNumber:(NSString *)waybillNumber platformCode:(NSString *)platformCode from:(id)requester succeededHandler:(void(^)(MMHExpressModel *expressModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/updateLogistics.htm" parameters:@{@"refundLineId":refundLineId,@"waybillNumber":waybillNumber,@"platformCode":platformCode} from:requester responseObjectClass:[MMHExpressModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark 支付成功发票
-(void)sendRequestToGetInvoiceWithParameterDic:(NSDictionary *)parameterDic from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/addOrderInvoice.htm" parameters:parameterDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}
#pragma mark 妈豆付款回调
-(void)sendRequestWithCallBackWithMBeanOrder:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/modouNotifyUrl.htm" parameters:@{@"orderBatchNo":orderNo} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 订单列表直接去支付
-(void)sendRequestToPayWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)(MMHOrderListPay2Model *payInfoModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/pay2.htm" parameters:@{@"orderBatchNo":orderNo} from:nil responseObjectClass:[MMHOrderListPay2Model class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark 支付
-(void)fetchModelPayWithPayParameterDic:(NSDictionary *)parameterDic from:(id)requester succeededHandler:(void(^)(MMHPayInfoModel *payInfoModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/pay.htm" parameters:parameterDic from:requester responseObjectClass:[MMHPayInfoModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}




#pragma mark 获取收货地址列表
-(void)fetchmodelGetCityListFrom:(id)requester succeededHandler:(void(^)(MMHAddressCityListModel *addressCityList))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/getDeliveryCitys.htm" parameters:nil from:requester responseObjectClass:[MMHAddressCityListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}





#pragma mark 获取收货地址
// 1. 获取收货地址
-(void)fetchModelGetDeliveryAddressWithMemberId:(NSString *)memberId from:(id)requester succeededHandler:(void(^)(MMHAddressListModel *addressListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/getDeliveryAddr.htm" parameters:nil from:requester responseObjectClass:[MMHAddressListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 删除收货地址
-(void)fetchModelDeleteAddressWithMemberId:(NSString *)memberId deliveryAddressId:(NSString *)deliveryAddrId from:(id)requester succeededHandler:(void(^)(MMHAddressListModel *addressListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/delDeliveryAddr.htm" parameters:@{@"deliveryAddrId":deliveryAddrId} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 增加收货地址
-(void)fetchModelAddAddressWithProvince:(NSString *)province city:(NSString *)city area:(NSString *)area addressDetail:(NSString *)addressDetail consignee:(NSString *)consignee phone:(NSString *)phone telephone:(NSString *)telePhone isDefault:(BOOL)isDefault memberId:(NSString *)memberid areaId:(NSString *)areaId from:(id)requester succeededHandler:(void(^)(NSString *deliveryAddressId))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/addDeliveryAddr.htm"
                                      parameters:@{@"province":province,@"city":city,@"area":area,@"addrDetail":addressDetail
                                                   ,@"consignee":consignee,@"phone":phone,@"telephone":telePhone,
                                                   @"isDefault":(isDefault?@"1":@"0"),@"areaId":areaId}
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      NSString *deliveryAddressId =  [responseJSONObject objectForKey:@"deliveryAddrId"];
                                      succeededHandler(deliveryAddressId);
                                  }
                                     failedBlock:^(NSError *error) {
                                         
                                     }];
}

#pragma mark 修改收货地址
-(void)fetchModelUpdateAddressWithDeliveryAddrId:(NSString *)delivryAddrId
                                        province:(NSString *)province
                                            city:(NSString *)city
                                            ares:(NSString *)area
                                      addrDetail:(NSString *)addrDetail
                                       consignee:(NSString *)consignee
                                           phone:(NSString *)phone
                                       telephone:(NSString *)telephone
                                       isDefault:(BOOL)isDefault
                                          areaId:(NSString *)areaId
                                            from:(id)requester
                                succeededHandler:(void(^)())succeededHandler
                                   failedHandler:(MMHNetworkFailedHandler)failedHandler {
    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/updateDeliveryAddr.htm"
                                      parameters:@{@"deliveryAddrId":delivryAddrId,@"province":province,@"city":city,@"area":area,@"addrDetail":addrDetail,@"consignee":consignee,@"phone":phone,@"telephone":telephone,
                                                   @"isDefault":(isDefault?@"1":@"0"),@"areaId":areaId}
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      succeededHandler(responseObject);
                                  }
                                     failedBlock:^(NSError *error) {
                                         
                                     }];
}

#pragma mark 修改默认地址
-(void)fetchModelUpdateNormolAddressWithAddressId:(NSString *)addressId from:(id)requester succeededHandler:(void(^)())succeededHandler
failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/updateDeliveryAddr.htm" parameters:@{@"deliveryAddrId":addressId,@"isDefault":@"1"} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    }
  failedBlock:^(NSError *error) {
      failedHandler(error);
  }];
}

#pragma mark 获取默认地址
-(void)fetchModelGetNormolAddressFrom:(id)requester succeededHandler:(void(^)(MMHAddressSingleModel *addressSingleModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/delivery/getDefaultDeliveryAddr.htm" parameters:nil from:requester responseObjectClass:[MMHAddressSingleModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}



#pragma mark  - 订单-去结算
//-(void)fetchModelGetOrderInfoWithMemberId:(NSString *)memberId andCartId:(NSString *)cartid from:(id)requester succeededHandler:(void(^)(MMHOrderWithConfirmationModel *orderWithConfirmationModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
//    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/toSettlement.htm"
//                                      parameters:@{@"cartId":cartid}
//                                            from:requester
//                             responseObjectClass:[MMHOrderWithConfirmationModel class]
//                            responseObjectKeyMap:nil
//                                  succeededBlock:^(id responseObject, id responseJSONObject) {
//                                      succeededHandler(responseObject);
//                                  }
//                                     failedBlock:^(NSError *error) {
//                                         
//                                     }];
//}

#pragma mark 订单- 去结算- 直接从商品进入
-(void)fetchModelGetOrderInfoWithJsonTerm:(NSString *)jsonTerm deliveryAddrId:(NSString *)deliveryAddrId lng:(NSString *)lng lat:(NSString *)lat inlet:(NSInteger)inlet orderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)(MMHOrderWithConfirmationModel *orderWithConfirmationModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSDictionary *parameters = [NSDictionary dictionary];
    if (inlet == 1){                                                    // 从购物车直接进入
        if ([deliveryAddrId isEqualToString:@"-1"]) {
            parameters = @{@"inlet": @"1"};
        }
        else {
            parameters = @{@"inlet":@"1",@"deliveryAddrId":deliveryAddrId};
        }
    } else if (inlet == 2){                                                 // 从商品直接结算
        if (![deliveryAddrId isEqualToString:@"-1"]){
            parameters = @{@"jsonTerm":jsonTerm,@"inlet":@"2",@"deliveryAddrId":deliveryAddrId};
        } else {
            parameters = @{@"jsonTerm":jsonTerm,@"inlet":@"2"};
        }
    } else if (inlet == 3){                                                 // 切换地址
        if (orderNo){
            if (jsonTerm.length){
                parameters = @{@"jsonTerm":jsonTerm,@"inlet":@"3",@"deliveryAddrId":deliveryAddrId,@"orderNo":orderNo};
            } else {
                parameters = @{@"inlet":@"3",@"deliveryAddrId":deliveryAddrId,@"orderNo":orderNo};
            }
        } else {
            if (jsonTerm.length){
                parameters = @{@"jsonTerm":jsonTerm,@"inlet":@"3",@"deliveryAddrId":deliveryAddrId};
            } else {
                parameters = @{@"inlet":@"3",@"deliveryAddrId":deliveryAddrId};
            }
        }
    } else if (inlet == 4){                                                 // 妈豆商品
        parameters = @{@"jsonTerm":jsonTerm,@"inlet":@"4",@"deliveryAddrId":deliveryAddrId};
    }
    
    
    
    
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/toSettlement.htm" parameters:parameters from:(id)requester responseObjectClass:[MMHOrderWithConfirmationModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


//- (void)confirmOrderWithCartData:(MMHCartData *)cart from:(id)requester succeededHandler:(void(^)(MMHOrderWithConfirmationModel *orderWithConfirmationModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
//{
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//    parameters[@"cartId"] = @(cart.cartID);
//    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/toSettlement.htm"
//                                      parameters:parameters
//                                            from:requester
//                             responseObjectClass:[MMHOrderWithConfirmationModel class]
//                            responseObjectKeyMap:nil
//                                  succeededBlock:^(id responseObject, id responseJSONObject) {
//                                      succeededHandler(responseObject);
//                                  } failedBlock:failedHandler];
//}




#pragma mark  - 订单列表
-(void)fetchModelGetOrderListWithQueryType:(NSString *)queryType page:(NSInteger)page from:(id)requester succeededHandler:(void(^)(MMHOrderListModel *orderListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryOrderList.htm" parameters:@{@"queryType":queryType,@"page":[NSString stringWithFormat:@"%li",(long)page],@"count":@"15"} from:requester responseObjectClass:[MMHOrderListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 确认收货
-(void)fetchModelConfirmReceiptWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/confirmReceipt.htm" parameters:@{@"orderNo":orderNo} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 查看门店信息
-(void)fetchModelQueryShopInfoWithOrderNo:(NSString *)orderNo shopId:(NSString *)shopId from:(id)requester succeededHandler:(void(^)(MMHsinceTheMentionAddressModel *shopInfoModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryShopInfo.htm" parameters:@{@"orderNo":orderNo,@"shopId":shopId} from:nil responseObjectClass:[MMHsinceTheMentionAddressModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 申请退款
-(void)fetchModelWithApplyRefundWithOrderNo:(NSString *)orderNo itemId:(NSString *)itemId  from:(id)requester succeededHandler:(void(^)(MMHRefundRequestModel *refundRequestModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/applyRefund.htm" parameters:@{@"orderNo":orderNo,@"itemId":itemId} from:nil responseObjectClass:[MMHRefundRequestModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 取消订单
-(void)fetchModelCancelOrderWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/cancelOrder.htm" parameters:@{@"orderNo":orderNo} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 删除订单
-(void)fetchmodelDeleteOrderWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/delOrder.htm" parameters:@{@"orderNo":orderNo} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark - 订单详情
-(void)fetchmodelGetOrderDetailWithOrderNo:(NSString *)orderNo queryType:(MMHOrderType)queryType from:(id)requester succeededHandler:(void(^)(MMHOrderDetailModel *orderDetailModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSString *queryTypeWithCustom = @"";
    if (queryType == MMHOrderTypeAll){
        queryTypeWithCustom = @"";
    } else {
        queryTypeWithCustom = [NSString stringWithFormat:@"%li",(long)queryType];
    }
    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryOrderDetail.htm" parameters:@{@"orderNo":orderNo,@"queryType":queryTypeWithCustom} from:nil responseObjectClass:[MMHOrderDetailModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark - 提醒发货
-(void)fetchModelToRemindDeliveryWithOrderNo:(NSString *)orderNo from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/remindDelivery.htm" parameters:@{@"orderNo":orderNo } from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark - 退款详情提交
-(void)fetchModelToRefundAndSubmitAuditWithParameters:(NSDictionary *)parametersDic from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    

    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/commitApplyRefund.htm" parameters:parametersDic from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark - 退款原因
-(void)fetchModelGetRefundCauseFrom:(id)requester succeededHandler:(void(^)(MMHCauseArrModel *causeModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
[[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryRefundCause.htm" parameters:nil from:nil responseObjectClass:[MMHCauseArrModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];

}

#pragma mark - 退款详情内容
-(void)fetchModelGetRefundInfoWithOrderNo:(NSString *)orderNo andItemId:(NSString *)itemId from:(id)requester succeededHandler:(void(^)(MMHRefundDetailModel *refundDetailModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {     [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryRefundInfo.htm" parameters:@{@"orderNo":orderNo,@"itemId":itemId} from:nil responseObjectClass:[MMHRefundDetailModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
    succeededHandler(responseObject);
} failedBlock:^(NSError *error) {
    failedHandler(error);
}];
}

#pragma mark 退款确认收款
-(void)fetchModelConfirmReceivablesWithRefundLineId:(NSString *)refundLineId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/confirmReceivables.htm" parameters:@{@"refundLineId":refundLineId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 退款取消申请
-(void)fetchMOdelCancelApplyWithRefundLineId:(NSString *)refundLineId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/cancelApply.htm" parameters:@{@"refundLineId":refundLineId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma 获取物流信息列表
-(void)fetchModelWithGetTrackingPlatformFrom:(id)requester succeededHandler:(void(^)(MMHQueryLogisticsListModel *queryLogisticsListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/queryLogisticsList.htm" parameters:nil from:nil responseObjectClass:[MMHQueryLogisticsListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 提交物流信息
-(void)fetchModelWithCommitLogisticeWithRefundLineId:(NSString *)refundLineId waybillNumber:(NSString *)waybillNumber platformCode:(NSString *)platformCode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSMutableDictionary *parameterDic = [NSMutableDictionary dictionary];
    if (refundLineId){
        [parameterDic setObject:refundLineId forKey:@"refundLineId"];
    }
    if (waybillNumber){
        [parameterDic setObject:waybillNumber forKey:@"waybillNumber"];
    }
    if (platformCode){
        [parameterDic setObject:platformCode forKey:@"platformCode"];
    }
    
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/commitLogistics.htm" parameters:parameterDic from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 获取退货物流跟踪信息
-(void)fetchModelWithLogisticeTrackingWithWaybillNumber:(NSString *)waybillNumber platformCode:(NSString *)platformCode from:(id)requester succeededHandler:(void(^)(MMHExpressModel *expressModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSMutableDictionary *paramateDic = [NSMutableDictionary dictionary];
    if (waybillNumber){
        [paramateDic setObject:waybillNumber forKey:@"waybillNumber"];
    }
    if (platformCode){
        [paramateDic setObject:platformCode forKey:@"platformCode"];
    }
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"order/basic/logisticsTracking.htm" parameters:paramateDic from:nil responseObjectClass:[MMHExpressModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

@end
