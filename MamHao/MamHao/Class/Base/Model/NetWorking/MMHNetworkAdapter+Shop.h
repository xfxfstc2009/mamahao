//
//  MMHNetworkAdapter+Shop.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"


@class MMHShopInfo;
@class MMHShopDetail;
@class MMHShopService;


@interface MMHNetworkAdapter (Shop)

- (void)fetchShopDetailWithShopId:(NSString *)shopId memberId:(NSInteger)memberId start:(NSInteger)start end:(NSInteger)end  from:(id)requester succeededHandler:(void (^)(MMHShopDetail *shopDetail))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
- (void)fetchShopServiceWithShopDetail:(MMHShopDetail *)shopDetail from:(id)requester succeededHandler:(void (^)(MMHShopService *shopService))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
/**
 *  获取门店筛选条件
 */
- (void)fetchfilterConditionWithShopInfo:(MMHShopDetail *)shopInfo from:(id)requester succeededHandler:(void (^)(MMHShopDetail *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
@end
