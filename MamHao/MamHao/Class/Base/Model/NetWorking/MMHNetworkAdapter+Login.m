//
//  MMHNetworkAdapter+Login.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Login.h"
#import "MMHNetworkEngine.h"
#import "MMHActionSheetViewController.h"
#import "MMHAccountSession.h"
#import "MMHNetworkAdapter+CommonSync.h"


@implementation MMHNetworkAdapter (Login)

#pragma mark - 登录
- (void)loginWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    if ([phoneNumber length] == 0) {
        failedHandler(nil);
        return;
    }
    if ([password length] == 0) {
        failedHandler(nil);
        return;
    }
    NSString *deviceIdentifierForVendor = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/login.htm"
                                      parameters:@{@"phone": phoneNumber, @"password": password, @"deviceId": deviceIdentifierForVendor, @"deviceType": @"ios", @"clientVersion": appVersion}
                                            from:requester
                             responseObjectClass:[MMHAccount class]
                            responseObjectKeyMap:@{@"memberID": @"memberId", @"name": @"memberName", @"nickname": @"screenName"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHAccount *account = (MMHAccount *)responseObject;
                                      account.phoneNumber = phoneNumber;
                                      account.password = password;
                                      [[MMHAccountSession currentSession] accountDidLogin:account];
                                      succeededHandler(account);
                                  }
                                     failedBlock:^(NSError *error) {
                                         failedHandler(error);
        [MMHAccountSession currentSession].isLoggingIn = NO;
    }];
}


- (void)loginWithPhoneNumber:(NSString *)phoneNumber verificationCode:(NSString *)verificationCode from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    if ([phoneNumber length] == 0) {
        failedHandler(nil);
        return;
    }
    if ([verificationCode length] == 0) {
        failedHandler(nil);
        return;
    }
    NSString *deviceIdentifierForVendor = [UIDevice deviceID];
    NSString *appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    
//#ifdef DEBUG
//    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99"];
//#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    //#endif
    [engine postWithAPI:@"member/login.htm"
             parameters:@{@"phone": phoneNumber, @"vcode": verificationCode, @"deviceId": deviceIdentifierForVendor, @"deviceType": @"ios", @"clientVersion": appVersion}
                   from:requester
    responseObjectClass:[MMHAccount class]
   responseObjectKeyMap:@{@"memberID": @"memberId", @"name": @"memberName", @"nickname": @"screenName", @"avatar": @"headPic"}
         succeededBlock:^(id responseObject, id responseJSONObject) {
             MMHAccount *account = (MMHAccount *)responseObject;
             account.phoneNumber = phoneNumber;
             [[MMHNetworkAdapter sharedAdapter] syncOfflineDataWithAccount:account
                                                          succeededHandler:^{
                                                              [[MMHAccountSession currentSession] accountDidLogin:account];
                                                              succeededHandler(account);
                                                          } failedHandler:^(NSError *error) {
                                                              failedHandler(error);
                                                              [MMHAccountSession currentSession].isLoggingIn = NO;
                                                          }];
         }
            failedBlock:^(NSError *error) {
                failedHandler(error);
                [MMHAccountSession currentSession].isLoggingIn = NO;
            }];
}


#pragma mark - 注册
- (void)registerWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
//    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/validateSms.htm"
//                                      parameters:@{@"phone": @"13813813813", @"vcode": @"123456"}
//                                            from:nil
//                             responseObjectClass:nil
//                            responseObjectKeyMap:nil
//                                  succeededBlock:^(id responseObject, id responseJSONObject) {
//                                      NSLog(@"sisisis: %@, %@", responseObject, responseJSONObject);
//                                  } failedBlock:^(NSError *error) {
//                NSLog(@"sisisisi: error: %@", error);
//            }];
//    return;
    NSString *deviceIdentifierForVendor = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    NSString *appVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"];
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/reg.htm"
                                      parameters:@{@"phone": phoneNumber, @"password": password, @"deviceId": deviceIdentifierForVendor, @"deviceType": @"ios", @"clientVersion": appVersion}
                                            from:requester
                             responseObjectClass:[MMHAccount class]
                            responseObjectKeyMap:@{@"memberID": @"memberId", @"name": @"memberName", @"nickname": @"screenName"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      MMHAccount *account = (MMHAccount *)responseObject;
                                      account.phoneNumber = phoneNumber;
                                      account.password = password;
                                      [[MMHAccountSession currentSession] accountDidLogin:account];
                                      succeededHandler(account);
            } failedBlock:^(NSError *error) {
                [MMHAccountSession currentSession].isLoggingIn = NO;
                failedHandler(error);
            }];
}

#pragma mark 绑定
-(void)bindingWithPhoneNumber:(NSString *)phoneNumber password:(NSString *)password extId:(NSString *)extId extType:(NSInteger)extType vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)(MMHAccount *account))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/thirdLogging.htm"
                                      parameters:@{@"phone":phoneNumber,@"password":password,@"deviceId":[[[UIDevice currentDevice] identifierForVendor] UUIDString],@"deviceType":@"ios",@"clientVersion":@"1.0",@"extId":extId,@"extType":[NSString stringWithFormat:@"%li",(long)extType],@"vcode":vcode}
                                            from:requester
                             responseObjectClass:[MMHAccount class]
                            responseObjectKeyMap:@{@"memberID": @"memberId", @"name": @"memberName", @"nickname": @"screenName"}
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                MMHAccount *account = (MMHAccount *)responseObject;
                account.phoneNumber = phoneNumber;
                account.externalID = extId;
                account.externalType = (MMHShareType)extType;
                [[MMHAccountSession currentSession] accountDidLogin:account];
                succeededHandler(account);
            } failedBlock:^(NSError *error) {
                [MMHAccountSession currentSession].isLoggingIn = NO;

            }];
}

#pragma mark 发送短信验证码

-(void)sendSmsForNoExistsWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/sendSmsForNoExists.htm" parameters:@{@"phone":phoneNumber} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 发送短信验证码 - 用户已经注册
-(void)sendSmsForExistsWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void (^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/sendSmsForExists.htm" parameters:@{@"phone":phoneNumber} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


-(void)sendSmsForAllWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.99" port:@"8080"];
#else
    MMHNetworkEngine *engine =  [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"sms/vcode/sendSmsVcodeForLoginOrReg.htm"
                                      parameters:@{@"phone": phoneNumber}
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      succeededHandler();
                                  } failedBlock:failedHandler];
}

#pragma mark 验证验证码
-(void)validateSmsWithPhoneNumber:(NSString *)phoneNumber vCode:(NSString *)vCode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/validateSms.htm" parameters:@{@"phone":phoneNumber,@"vcode":vCode} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 忘记密码
-(void)resetPasswordWithPhoneNumber:(NSString *)phoneNumber newPassword:(NSString *)password vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/basic/rPwd.htm" parameters:@{@"phone":phoneNumber,@"newPassword":password,@"vcode":vcode} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler();
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}
@end
