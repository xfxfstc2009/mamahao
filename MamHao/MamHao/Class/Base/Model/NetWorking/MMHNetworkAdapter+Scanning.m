//
//  MMHNetworkAdapter+Scanning.m
//  MamHao
//
//  Created by SmartMin on 15/6/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Scanning.h"

@implementation MMHNetworkAdapter (Scanning)

#pragma mark  扫码订单
-(void)fetchModelScanningWithParameter:(NSDictionary *)parameter from:(id)requester succeededHandler:(void(^)(MMHOrderWithConfirmationModel *orderWithConfirmationModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
   [[MMHNetworkEngine sharedEngine]postWithAPI:@"order/basic/queryPosOrder.htm" parameters:parameter from:requester responseObjectClass:[MMHOrderWithConfirmationModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


@end
