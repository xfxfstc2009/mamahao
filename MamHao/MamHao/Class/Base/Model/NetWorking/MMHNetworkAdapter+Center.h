//
//  MMHNetworkAdapter+Center.h
//  MamHao
//
//  Created by SmartMin on 15/6/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 个人中心
#import "MMHNetworkAdapter.h"
#import "MMHCouponsListModel.h"             // 优惠券列表
#import "MMHBearListModel.h"                // 妈豆
#import "MMHBabyInfoListModel.h"            // 宝宝列表
#import "MMHMemberCenterInfoModel.h"        //
#import "MMHSettingPaySettingModel.h"       // 设置支付密码返回的内容

@interface MMHNetworkAdapter (Center)

#pragma mark 判断当前设置的支付密码是否正确
-(void)sendRequestWithCheckPayPassword:(NSString *)payPassword from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler ;

#pragma mark 判断当前是否设置过支付密码
-(void)sendRequestGetIsSettingPayPasswordFrom:(id)requester succeededHandler:(void(^)(MMHSettingPaySettingModel *paySettedModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 设置妈豆支付密码发送验证码
-(void)sendRequestWithSettingSmsCodeWithPhone:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler ;

#pragma mark 校验验证码
-(void)sendRequestWithverificationSmsCodeWithPhone:(NSString *)phoneNumber vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 设置密码
-(void)sendRequestWithSetPayPasswordWithPhone:(NSString *)phoneNumber payPassword:(NSString *)payPassword from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 意见与投诉
-(void)sendRequestWithDeedbackType:(NSInteger)feedBackType content:(NSString *)content from:(id)requester succeededHandler:(void(^)(MMHCouponsListModel *couponsListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 保存宝贝信息
/** 新增宝宝信息*/
-(void)fetchModelWithBabyInfoWithParameter:(NSDictionary *)parameter From:(id)requester succeededHandler:(void(^)(NSString *babyId))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 优惠券
/**
 *  获取优惠券
 *
 *  @param availableType    查询类型
 *  @param requester        请求体
 *  @param succeededHandler 成功回调
 *  @param failedHandler    失败回调
 */
-(void)fetchModelGetCouponsListWithAvailableType:(NSInteger)availableType from:(id)requester succeededHandler:(void(^)(MMHCouponsListModel *couponsListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
/**
 *  兑换优惠券
 *
 *  @param cdKey            优惠券兑换码
 */
-(void)exchangeVoucherWithCdKey:(NSString *)cdKey  from:(id)requester succeededHandler:(void(^)(MMHCouponsSingleModel *couponsSingleModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 妈豆
-(void)fetchModelGetMamBeanInfoWithPage:(NSInteger)page pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void(^)(MMHBearListModel *beanListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 宝宝信息
-(void)fetchModelGetMemberBabyFrom:(id)requester succeededHandler:(void(^)(MMHBabyInfoListModel *babyInfoList))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 删除宝宝
-(void)fetchmodelWithDeleteBaby:(NSString *)babyId from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 新增宝宝
-(void)fetchModelWithBabyNickName:(NSString *)babyNickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark  修改宝宝信息
-(void)fetchModelUpdateBabyWithUpdateBabyInfo:(NSString *)babyId babyNickName:(NSString *)babyNickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 获取会员信息
-(void)fetchModelGetMemberCenterInfoFrom:(id)requester succeededHandler:(void(^)(MMHMemberCenterInfoModel *memberCenterInfo))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 修改会员信息
-(void)fetchModelUpdateMemberInfoWithType:(NSInteger)type andVaule:(NSString *)typeValue from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 获取用户积分
-(void)sendRequestGetgetUserIntegralWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)(NSInteger gbCount,NSInteger mcCount))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;



#pragma mark 收藏
/**
 *  获取收藏列表
 */
-(void)getMemberCollectWithCollectType:(NSInteger)collectType from:(id)requester succeededHandler:(void(^)(NSMutableDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
/**
 *  删除收藏列表
 */
-(void)deleteCollectWithCollectIds:(NSString *)collectIds from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;


-(void)addCollectWithType:(NSInteger)collectType collectItemId:(NSString *)collectItemId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 个人中心界面数据

/**
 *  个人中心界面数据
 *
 *  @param memberId 用户id
 */
- (void)getCenterInfoWithMemberId:(NSInteger)memberId from:(id)requester succeededHandler:(void (^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
#pragma mark 积分
/**
 *  查询积分
 *
 *  @param type     积分类型 0:GB,1:MC
 *  @param pageNo   分页用的页码 默认：1
 *  @param pageSize 每页的条数 默认：10
 */
- (void)fetchModelgetIntegralWithType:(NSInteger)type pageNo:(NSInteger)pageNo pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
