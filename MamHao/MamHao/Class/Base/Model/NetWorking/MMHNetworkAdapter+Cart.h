//
//  MMHNetworkAdapter+Cart.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"
#import "MamHao-Swift.h"
#import "MMHShopCartModel.h"

@class MMHCartData;
@class MMHCartItem;
@class MMHProductDetailModel;


@interface MMHNetworkAdapter (Cart)

- (void)addProductToCart:(MMHProductDetailModel *)productDetail succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;
- (void)buyAgainAddToCart:(MMHShopCartModel *)shopCartModel succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;


- (void)fetchCartDataFrom:(id)requester succeededHandler:(void(^)(MMHCartData *cartData))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
- (void)changeQuantityOfItem:(MMHCartItem *)cartItem inCart:(MMHCartData *)cart to:(NSInteger)toValue succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;
- (void)deleteItems:(NSArray *)items inCart:(MMHCartData *)cart succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;
//- (void)fetchActivitiesForShopIDs:(NSArray *)shopIDs companyIDs:(NSArray *)companyIDs alongWithMamhao:(BOOL)alongWithMamhao from:(id)requester succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;
- (void)changeChosenStatusOfItem:(MMHCartItem *)cartItem inCart:(MMHCartData *)cart from:(id)requester succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;
- (void)changeChooseAllStatusInCart:(MMHCartData *)cart from:(id)requester succeededHandler:(void (^)(MMHCartData *cartData))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;




@end
