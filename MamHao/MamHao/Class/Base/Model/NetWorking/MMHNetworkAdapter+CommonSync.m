//
//  MMHNetworkAdapter+CommonSync.m
//  MamHao
//
//  Created by fishycx on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+CommonSync.h"
#import "MMHRequestSerializer.h"
#import "MMHAccountSession.h"
#import "MMHCartData.h"

@implementation MMHNetworkAdapter (CommonSync)
//-(void)commonSync{
//    NSString *filePath = [self favoritesFilePath];
//    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
//        NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:filePath];
//        if (array.count) {
//            NSDictionary *dic = @{@"type":@(2),@"data":array};
//            NSString *jsonStr = [dic JSONString];
//            [[MMHNetworkEngine sharedEngine] postWithAPI:@"client/sync/commonSync.htm" parameters:@{@"jsonData":jsonStr} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
//                //
//                [array removeAllObjects];
//                [array writeToFile:filePath atomically:YES];
//            } failedBlock:^(NSError *error) {
//                //
//            }];
//        }
//        
//    }
//}
- (NSString *)favoritesFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *doucumentsDirectory = paths.lastObject;
    NSString *filePath = [doucumentsDirectory stringByAppendingPathComponent:@"collect.plist"];

    return filePath;
}

- (NSString *)memberSettingFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *doucumentsDirectory = paths.lastObject;
    NSString *filePath = [doucumentsDirectory stringByAppendingPathComponent:@"memberSetting.plist"];
    return filePath;
}
- (NSString *)imAcctFilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *doucumentsDirectory = paths.lastObject;
    NSString *filePath = [doucumentsDirectory stringByAppendingPathComponent:@"imAcc.plist"];
    return filePath;
}


- (void)syncOfflineDataWithAccount:(MMHAccount *)account succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{

    NSString *favoritesFilePath = [self favoritesFilePath];
    //同步IM账号信息
   NSString *imName = [MMHAccountSession chattingUserID];
  
    NSMutableDictionary *imAccountInfo = [NSMutableDictionary dictionary];
    if(imName){
        [imAccountInfo setObject:imName forKey:@"acct"];
        [imAccountInfo setObject:imName forKey:@"pwd"];
    }else{
       
    }
    
    //同步购物车信息
    NSString *mmid =[NSString stringWithMMHID:[MMHCartData cartID]];
    NSMutableDictionary *mmidDic = [NSMutableDictionary dictionary];
    if (mmid.length) {
        [mmidDic setObject:mmid forKey:@"cartId"];
    }
    // 读取妈妈信息
    NSMutableDictionary *memberSettingDic = [NSMutableDictionary dictionary];
   NSString *motherStatus = [MMHTool userDefaultGetWithKey:@"BabyMaMState"];
    NSInteger memberStatus = 0;
    if ([motherStatus isEqualToString:@"孕妈"]) {
        memberStatus = 1;
    }else if([motherStatus isEqualToString:@"宝妈"]){
        memberStatus = 2;
    }else{
      //用户跳过，没有设置
    }
    NSString *babyGender = [MMHTool userDefaultGetWithKey:@"BabyGender"];
    NSInteger gender = 0;
    
    if ([babyGender isEqualToString:@"男"]) {
        gender=1;
    }else if([babyGender isEqualToString:@"女"]){
        gender=2;
    }else{
     //用户没有设置宝贝信息
        
    }
      [memberSettingDic setObject:@(memberStatus) forKey:@"memberStatus"];
    if (memberStatus == 1) {
        //孕妈
        if ([MMHTool userDefaultGetWithKey:@"BabyBirthday"]) {
            [memberSettingDic setObject:[MMHTool userDefaultGetWithKey:@"BabyBirthday"] forKey:@"member_due_date"];
        }
    }else if(memberStatus == 2){
       //宝妈
        if ([MMHTool userDefaultGetWithKey:@"BabyNickname"]){
            [memberSettingDic setObject:[MMHTool userDefaultGetWithKey:@"BabyNickname"] forKey:@"babyName"];
        }
        if ([MMHTool userDefaultGetWithKey:@"BabyBirthday"]) {
            [memberSettingDic setObject:[MMHTool userDefaultGetWithKey:@"BabyBirthday"] forKey:@"babyBirthday"];
        }
        if ([MMHTool userDefaultGetWithKey:@"BabyGender"]) {
            [memberSettingDic setObject:@(gender)forKey:@"babyGender"];
        }

    }else{
      //首页跳过，没有选择妈妈status
    }

    NSMutableDictionary *dic = [NSMutableDictionary  dictionary];
    [dic setObject:memberSettingDic forKey:@"memberSetting"];
    [dic setObject:imAccountInfo forKey:@"imAcct"];
    [dic setObject:mmidDic forKey:@"cart"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:favoritesFilePath]) {
        NSMutableArray *favoritesArray = [NSMutableArray arrayWithContentsOfFile:favoritesFilePath];
        if (favoritesArray.count) {
            [dic setObject:favoritesArray forKey:@"favorite"];
        }
    }
    
    NSString *jsonData = [dic JSONString];
    MMHRequestSerializer *requestSerializer = (MMHRequestSerializer *)[MMHNetworkEngine sharedEngine].requestSerializer;
    requestSerializer.temporaryAccount = account;
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"client/sync/commonSync.htm" parameters:@{@"jsonData": jsonData} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        [self deleteFileAtPath:favoritesFilePath];
        [MMHTool userDefaultDelegtaeWithKey:@"BabyNickname"];
        [MMHTool userDefaultDelegtaeWithKey:@"BabyBirthday"];
        [MMHTool userDefaultDelegtaeWithKey:@"BabyGender"];
        [MMHTool userDefaultDelegtaeWithKey:@"member_status"];
        [MMHTool userDefaultDelegtaeWithKey:@"member_due_date"];
        if (succeededHandler) {
            succeededHandler(responseJSONObject);
        }
    } failedBlock:^(NSError *error) {
        if (failedHandler) {
            failedHandler(error);
        }
    }];

}
- (void)deleteFileAtPath:(NSString *)path{
     NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:path]) {
        NSMutableArray *offLineData = [NSMutableArray arrayWithContentsOfFile:path];
        [offLineData removeAllObjects];
        [offLineData writeToFile:path atomically:YES];
    }else{
      //
    }
}


- (void)syncGPSLocation:(CLLocation *)location areaID:(NSString *)areaID succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"deviceId"] = [UIDevice deviceID];
    parameters[@"areaId"] = areaID;
    parameters[@"lng"] = @(location.coordinate.longitude);
    parameters[@"lat"] = @(location.coordinate.latitude);
//    parameters[@"memberPic"] = @"aaa";
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"home/base/uploadMemberLocation.htm"
                                      parameters:parameters
                                            from:nil
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      succeededHandler();
                                  } failedBlock:failedHandler];
}


@end
