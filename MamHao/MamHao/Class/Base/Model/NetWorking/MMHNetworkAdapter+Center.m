//
//  MMHNetworkAdapter+Center.m
//  MamHao
//
//  Created by SmartMin on 15/6/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Center.h"
#import "MMHAccountSession.h"
@implementation MMHNetworkAdapter (Center)

#pragma mark 判断当前设置的支付密码是否正确
-(void)sendRequestWithCheckPayPassword:(NSString *)payPassword from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/center/checkPayPassword.htm" parameters:@{@"payPassword":payPassword} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}
#pragma mark 判断当前是否设置过支付密码
-(void)sendRequestGetIsSettingPayPasswordFrom:(id)requester succeededHandler:(void(^)(MMHSettingPaySettingModel *paySettedModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/getIsSetted.htm" parameters:nil from:requester responseObjectClass:[MMHSettingPaySettingModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 设置妈豆支付密码发送验证码
-(void)sendRequestWithSettingSmsCodeWithPhone:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSMutableDictionary *parametersDic = [NSMutableDictionary dictionary];
    
    if (phoneNumber){
        [parametersDic setObject:phoneNumber forKey:@"phone"];
    }
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"sms/vcode/sendSmsVcodeForSetPayPassword.htm" parameters:parametersDic from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 校验验证码
-(void)sendRequestWithverificationSmsCodeWithPhone:(NSString *)phoneNumber vcode:(NSString *)vcode from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"sms/vcode/checkSetPayPasswordVcode.htm" parameters:@{@"phone":phoneNumber,@"vcode":vcode} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 设置密码
-(void)sendRequestWithSetPayPasswordWithPhone:(NSString *)phoneNumber payPassword:(NSString *)payPassword from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/setPayPassword.htm" parameters:@{@"phone":phoneNumber,@"payPassword":payPassword} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark 意见与投诉
-(void)sendRequestWithDeedbackType:(NSInteger)feedBackType content:(NSString *)content from:(id)requester succeededHandler:(void(^)(MMHCouponsListModel *couponsListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"feedback/addFeedback.htm" parameters:@{@"type":[NSString stringWithFormat:@"%li",(long)feedBackType],@"content":content} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


#pragma mark 优惠券

-(void)fetchModelGetCouponsListWithAvailableType:(NSInteger)availableType from:(id)requester succeededHandler:(void(^)(MMHCouponsListModel *couponsListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"voucher/queryVoucherList.htm" parameters:@{@"queryType":[NSString stringWithFormat:@"%li",(long)availableType]} from:requester responseObjectClass:[MMHCouponsListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

- (void)exchangeVoucherWithCdKey:(NSString *)cdKey from:(id)requester succeededHandler:(void (^)(MMHCouponsSingleModel *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"voucher/exchangeVoucher.htm" parameters:@{@"cdKey":cdKey} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        MMHCouponsSingleModel *model = [[MMHCouponsSingleModel alloc] initWithJSONDict:responseJSONObject];
        succeededHandler(model);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 妈豆
-(void)fetchModelGetMamBeanInfoWithPage:(NSInteger)page pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void(^)(MMHBearListModel *beanListModel))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"mbean/queryMBeanList.htm" parameters:@{@"page":[NSString stringWithFormat:@"%li",(long)page],@"pageSize":[NSString stringWithFormat:@"%li",(long)pageSize]} from:nil responseObjectClass:[MMHBearListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 宝宝信息 - 列表
-(void)fetchModelGetMemberBabyFrom:(id)requester succeededHandler:(void(^)(MMHBabyInfoListModel *babyInfoList))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/getMemberBaby.htm" parameters:nil from:nil responseObjectClass:[MMHBabyInfoListModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 删除宝宝
-(void)fetchmodelWithDeleteBaby:(NSString *)babyId from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/center/deleteMemberBaby.htm" parameters:@{@"babyId":babyId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 保存宝宝信息
-(void)fetchModelWithBabyNickName:(NSString *)babyNickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/addMemberBaby.htm" parameters:@{@"babyNickName":babyNickName,@"babyGender":[NSString stringWithFormat:@"%li",(long)babyGender],@"babyBirthday":babyBirthday,@"babyImg":babyImg} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 保存宝贝信息
-(void)fetchModelWithBabyInfoWithParameter:(NSDictionary *)parameter From:(id)requester succeededHandler:(void(^)(NSString *babyId))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/addMemberBaby.htm" parameters:parameter from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler([responseObject valueForKey:@"msg"]);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark  修改宝宝信息
-(void)fetchModelUpdateBabyWithUpdateBabyInfo:(NSString *)babyId babyNickName:(NSString *)babyNickName babyGender:(NSInteger)babyGender babyBirthday:(NSString *)babyBirthday babyImg:(NSString *)babyImg from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/center/changeMemberBaby.htm" parameters:@{@"babyId":babyId,@"babyNickName":babyNickName,@"babyGender":[NSString stringWithFormat:@"%li",(long)babyGender],@"babyBirthday":babyBirthday,@"babyImg":babyImg} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 获取会员信息 
-(void)fetchModelGetMemberCenterInfoFrom:(id)requester succeededHandler:(void(^)(MMHMemberCenterInfoModel *memberCenterInfo))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    NSString *memberId = [MMHAccountSession currentSession].memberID;
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/center/getMemberCenterInfo.htm" parameters:nil from:nil responseObjectClass:[MMHMemberCenterInfoModel class] responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        MMHMemberCenterInfoModel *info = (MMHMemberCenterInfoModel *)responseObject;
        info.memberId = memberId;
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 修改会员信息
-(void)fetchModelUpdateMemberInfoWithType:(NSInteger)type andVaule:(NSString *)typeValue from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/center/updateMemberParam.htm" parameters:@{@"type":[NSString stringWithFormat:@"%li",(long)type],@"value":typeValue} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

#pragma mark 获取用户积分
-(void)sendRequestGetgetUserIntegralWithPhoneNumber:(NSString *)phoneNumber from:(id)requester succeededHandler:(void(^)(NSInteger gbCount,NSInteger mcCount))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkEngine sharedEngine]postWithAPI:@"member/basic/getUserIntegral.htm" parameters:@{@"phone":phoneNumber} from:requester responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        NSInteger gbCount = [[responseJSONObject objectForKey:@"gbCount"]integerValue];
        NSInteger mcCount = [[responseJSONObject objectForKey:@"mcCount"]integerValue];
        succeededHandler(gbCount,mcCount);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

-(void)getMemberCollectWithCollectType:(NSInteger)collectType from:(id)requester succeededHandler:(void (^)(NSMutableDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"member/collect/getMemberCollect.htm" parameters:@{@"collectType":@(collectType)} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

-(void)deleteCollectWithCollectIds:(NSString *)collectIds from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    
#ifdef DEBUG_FISH

    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif

    [engine postWithAPI:@"/member/collect/deleteMemberCollect.htm" parameters:@{@"collectIds":collectIds} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];

}
-(void)addCollectWithType:(NSInteger)collectType collectItemId:(NSString *)collectItemId from:(id)requester succeededHandler:(void (^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    
    NSMutableDictionary *favouritedic =[[NSMutableDictionary alloc] init];
    [favouritedic setValue:@(collectType) forKey:@"type"];
    if (collectItemId.length) {
        [favouritedic setValue:collectItemId forKey:@"favId"];
    }
    [favouritedic setValue:@(1) forKey:@"status"];
    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        //收藏的item
      
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *doucumentsDirectory = paths.lastObject;
        NSString *filePath = [doucumentsDirectory stringByAppendingPathComponent: @"collect.plist"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:filePath];
            [array addObject:favouritedic];
            [array writeToFile:filePath atomically:YES];
        }else{
            NSMutableArray *array = [NSMutableArray arrayWithObjects:favouritedic, nil];
            [array writeToFile:filePath atomically:YES];
        }
        succeededHandler();
    }else{

#ifdef DEBUG_FISH
        MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
#else
        MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
        
        [engine postWithAPI:@"/member/collect/addMemberCollect.htm" parameters:@{@"collectType":@(collectType), @"collectItemId":collectItemId} from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
            succeededHandler(responseJSONObject);
        } failedBlock:^(NSError *error) {
            failedHandler(error);
        }];
    }

}

- (void)getCenterInfoWithMemberId:(NSInteger)memberId from:(id)requester succeededHandler:(void (^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
   #ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.131"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"member/center/getMemberCenterCountInfo.htm" parameters:nil from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}
- (void)fetchModelgetIntegralWithType:(NSInteger)type pageNo:(NSInteger)pageNo pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void (^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.131" port:@"8080"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [parameters setObject:@(type) forKey:@"type"];
    [parameters setObject:@(pageNo) forKey:@"pageNo"];
    [parameters setObject:@(pageSize) forKey:@"pageSize"];
    [engine postWithAPI:@"member/center/queryMemberPoint.htm" parameters:parameters from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}


@end
