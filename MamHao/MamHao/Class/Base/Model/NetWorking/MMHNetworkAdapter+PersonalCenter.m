//
//  MMHNetworkAdapter+PersonalCenter.m
//  MamHao
//
//  Created by fishycx on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+PersonalCenter.h"

@implementation MMHNetworkAdapter (PersonalCenter)

- (void)fetchMemberInfoWithMemberId:(NSString *)memberId from:(id)requester succeededHandler:(void (^)(NSDictionary *))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle {
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"" parameters:@{@"memberId":memberId} from:requester responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandle(error);
    }];
}
@end
