//
//  MMHNetworkAdapter+WordOfMouth.h
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"
#import "MMHSingleProductWithShopCartModel.h"

@interface MMHNetworkAdapter (WordOfMouth)
/**
 *  获取商品的口碑列表
 *
 *  @param memberId         用户id
 *  @param goodsTemplated   商品id
 *  @param requester        请求体
 *  @param succeededHandler 请求成功回调
 *  @param failedHandle     失败回调
 */
- (void)fetchPublicPrasieWithTemplateId:(NSString *) templateId page:(NSInteger)page pageSize:(NSInteger)pageSize from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;

/**
 *   获取商品的口碑统计
 *  @param templateId       款式Id
 *  @param requester        请求体
 *  @param succeededHandler 成功回调
 *  @param failedHandle     失败回调
 */
- (void)fetchTotalCountOfPublicPrasieWithGoodsTemplatedId:(NSString*) templateId from :(id)requester suuccedHandler:(void(^)(NSMutableArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;

/**
 *  评价商品
 *  @Param orderNo         订单
 *  @param goodsTemplateId 商品模板id
 *  @param star            星级
 *  @param content         评价内容
 *  @param pic             图片链接
 *  @param requester       请求体
 *  @param succededHandler 成功回调
 *  @param failedHandle    失败回调 
 */
- (void)praiseWithShopId:(NSString *)shopId orderNo:(NSString *)orderNo itemId:(NSString *)itemId  templateId:(NSString *)templateId star:(NSInteger)star content:(NSString *)content  pics:(NSString *)pics from:(id)requester succedHandler:(void(^)(NSDictionary *dic))succededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;
/**
 *  追加评价
 *
 *  @param shopId          店铺id
 *  @param orderNo         商品所属的订单号
 *  @param goodsTemplateId 商品模板id
 *  @param content         评价内容
 *  @param pics            图片
 *  @param requester       请求体
 *  @param succededHandler 成功回调
 *  @param failedHandle    失败回调
 */
- (void)additionalPraiseWithShopId:(NSString *)shopId warehouseId:(NSString *)warehouseId orderNo:(NSString *)orderNo itemId:(NSString *)itemId templateId:(NSString *)templateId bufferContentContent:(NSString *)content bufferPics:(NSString *)pics from:(id)requester succedHandler:(void (^)(NSDictionary *dic))succededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;
/**
 *  获取待评价订单下的商品列表
 *
 *  @param memberId         用户id
 *  @param orderNo          商品模板id
 *  @param request          请求体
 *  @param succeededHandler 成功回调
 *  @param failedHandle     失败回调
 */
- (void)fetchGoodsOfOrderWithOrderNo:(NSString *)orderNo  page:(NSInteger)page count:(NSInteger)count from:(id)request succeedHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;
/**
 *  评价订单
 *
 *  @param memberId         用户id
 *  @param orderNo          订单id
 *  @param request          请求体
 *  @param succeededHandler 成功回到方法
 *  @param failedHandle     失败回调方法
 */
- (void)praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger) serveStar deliverySpeedStar:(NSInteger)deliverySpeedStar commentContent:(NSString *)commentContent from :(id)request succeedHandler:(void(^)(NSDictionary *dic))succeededHandler faileHandler:(MMHNetworkFailedHandler)failedHandle;
/**
 *  获取订单的口碑
 *
 *  @param memberId     用户id
 *  @param orderNo      订单id
 *  @param request      请求体
 *  @param failerHandle 失败回调方法
 */
- (void)fetchWomOfOrderWithMemberId:(NSInteger)memberId orderNo:(NSInteger)orderNo from
                                   :(id)request succeedHandler:(void(^)(NSDictionary *dic)) succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle;
/**
 *  获取图文详情URL
 */
- (void)fetchImageAndTextDetailInfoWithTemplatedId:(NSString *)templateId succeedHandler:(void(^)(NSString *urlStr)) succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle;
/**
 *  获取门店的评价
 */
- (void)fetchShopPraiseWithShopId:(NSString *)shopId  succeedHandler:(void(^)(NSDictionary *dic)) succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle;

/**
 *  获取商品规格
 */
- (void)fetchProductParametersWithTemplateId:(NSString *)templateId succeedHandler:(void(^)(NSDictionary *dic)) succeedHandler failedHandler:(MMHNetworkFailedHandler)failerHandle;
@end
