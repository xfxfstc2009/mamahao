//
//  MMHNetworkAdapter+Image.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Image.h"
#import "OSSClient.h"
#import "MMHTimestamp.h"
#import "MMHAccountSession.h"
#import "UIImage+RenderedImage.h"


@implementation MMHNetworkAdapter (Image)


- (void)fetchOSSTokenWithContentString:(NSString *)contentString from:(id)requester succeededHandler:(void (^)(NSString *token))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{

//#ifdef DEBUG
//    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.100"];
//#else

    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
//#endif
    [engine postWithAPI:@"oss/getToken.htm"
                                      parameters:@{@"content": contentString}
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      NSString *token = responseJSONObject[@"token"];
                                      succeededHandler(token);
                                  } failedBlock:failedHandler];
}


- (void)uploadImage:(UIImage *)image folderName:(NSString *)folderName from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        OSSClient *ossClient = [OSSClient sharedInstanceManage];
        [ossClient setGlobalDefaultBucketHostId:@"oss-cn-hangzhou.aliyuncs.com"];
        [ossClient setGlobalDefaultBucketAcl:PRIVATE];
        [ossClient setGenerateToken:^NSString *(NSString *method, NSString *md5, NSString *type, NSString *date, NSString *xoss, NSString *resource){
            __block NSString *token = nil;
            dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
            NSString *content = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@%@", method, md5, type, date, xoss, resource];
            [[MMHNetworkAdapter sharedAdapter] fetchOSSTokenWithContentString:content
                                                                         from:nil
                                                             succeededHandler:^(NSString *theToken) {
                                                                 NSLog(@"oh finally got token: %@", theToken);
                                                                 token = theToken;
                                                                 dispatch_semaphore_signal(semaphore);
                                                             } failedHandler:^(NSError *error) {
                        NSLog(@"xixi failed :%@", error);
                        dispatch_semaphore_signal(semaphore);
                    }];
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER); // TODO: add timeout
//            dispatch_release(semaphore);
            return token;
        }];
    });

    NSString *fileID = [self generateFileID];
    NSString *userID = @"000000";
    NSString *memberID = [[MMHAccountSession currentSession] memberID];
    if (memberID.length != 0) {
        userID = memberID;
    }
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@", folderName, userID, fileID];

    OSSBucket *bucket = [[OSSBucket alloc] initWithBucket:@"mmh-images"];
    OSSData *ossData = [[OSSData alloc] initWithBucket:bucket withKey:path];
    
    NSData *data = UIImageJPEGRepresentation([self resizeImage:image], 1.0f);
    [ossData setData:data withType:@"image/jpeg"];
    [ossData uploadWithUploadCallback:^(BOOL isSuccess, NSError *error) {
        if (isSuccess) {
            succeededHandler(fileID);
        }
        else {
            NSLog(@"errorInfo_testDataUploadWithProgress:%@", error);
            failedHandler(error);
        }
    } withProgressCallback:^(float progress) {
        NSLog(@"current get %f", progress);
    }];
}

- (UIImage *)resizeImage:(UIImage *)image {
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    
    if (width < height) {
        [self resizeSmallSide:&width bigSide:&height];
    } else {
        [self resizeSmallSide:&height bigSide:&width];
    }
    
    CGSize newSize = CGSizeMake(width, height);
    
    UIGraphicsBeginImageContext(CGSizeMake(width, height));
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage* resizedImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImage *img = nil;
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
    NSData *resizedImgData = UIImageJPEGRepresentation(resizedImg, 1.0f);
    if (resizedImgData.length < imageData.length) {
        img = resizedImg;
    } else {
        img = image;
    }
    
    return img;
}

- (void)resizeSmallSide:(CGFloat *)smallSide bigSide:(CGFloat *)bigSide {
    CGFloat small = *smallSide;
    CGFloat big = *bigSide;
    
    CGFloat scale = small / big;
    if (scale <= 1 && scale > 2 / 3) {
        *smallSide = 640;
        *bigSide = 640 / scale;
    } else if (scale <= 2 / 3 && scale >= 9 / 16) {
        *bigSide = 960;
        *smallSide = 960 * scale;
    } else if (scale < 9 / 16 && scale > 1 / 5) {
        *smallSide = 1080;
        *bigSide = 1080 / scale;
    } else {
        *smallSide = 640;
        *bigSide = 640 / scale;
    }
}

- (NSString *)generateFileID
{
    NSString *userID = @"000000";
    NSString *memberID = [[MMHAccountSession currentSession] memberID];
    if (memberID.length != 0) {
        userID = memberID;
    }
    NSString *timestampString = [MMHTimestamp timestampString];
    NSString *fullString = [NSString stringWithFormat:@"%@%@", userID, timestampString];
    NSString *md5String = [fullString md5String];
    return md5String;
}


- (void)uploadCommentImage:(UIImage *)image from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    [self uploadImage:image folderName:@"comment-images" from:requester succeededHandler:succeededHandler failedHandler:failedHandler];
}


- (void)uploadPortrait:(UIImage *)portrait from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    [self uploadImage:portrait folderName:@"member-head-images" from:requester succeededHandler:succeededHandler failedHandler:failedHandler];
}


- (void)uploadRefundImage:(UIImage *)refundImage from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler
{
    [self uploadImage:refundImage folderName:@"refund-images" from:requester succeededHandler:succeededHandler failedHandler:failedHandler];
}



@end
