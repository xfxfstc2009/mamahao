//
//  MMHNetworkAdapter+Search.m
//  MamHao
//
//  Created by fishycx on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Search.h"

@implementation MMHNetworkAdapter (Search)
- (void)getHotSearchWordFrom :(id)requester succedHandler:(void(^)(NSMutableArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle {
    
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.116"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"client/search/getHotSearchWord.htm" parameters:nil from:nil responseObjectClass:nil responseObjectKeyMap:nil succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        //
    }];
}
@end
