//
//  MMHNetworkAdapter+Classification.m
//  MamHao
//
//  Created by 余传兴 on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Classification.h"

@implementation MMHNetworkAdapter (Classification)


- (void)fetchDataWithTypeId:(NSInteger)typeId rating:(NSInteger)rating succeededHandeler:(void (^)(NSArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSString *typeIdStr = [NSString stringWithFormat:@"%ld", (long)typeId];
    if (rating == 1) {
      typeIdStr = @"";
    }
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.116"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
   
    NSDictionary *parameterDic = @{@"typeId":typeIdStr, @"rating":@(rating)};
    [engine postWithAPI:@"shop/basic/queryGoodsType.htm" parameters:parameterDic from:nil responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject[@"data"]);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}

-(void)fetchDataWithtypeId:(NSInteger)typeId succeededHandler:(void (^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler {
    NSDictionary *dic = @{@"typeId":@(typeId)};
#ifdef DEBUG_FISH
    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.116"];
#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
#endif
    [engine postWithAPI:@"shop/basic/queryGoodsTypeTree.htm" parameters:dic from:nil responseObjectClass:nil responseObjectKeyMap:@{} succeededBlock:^(id responseObject, id responseJSONObject) {
        succeededHandler(responseJSONObject);
    } failedBlock:^(NSError *error) {
        failedHandler(error);
    }];
}
@end
