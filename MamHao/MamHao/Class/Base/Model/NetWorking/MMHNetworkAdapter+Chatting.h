//
//  MMHNetworkAdapter+Chatting.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"

@interface MMHNetworkAdapter (Chatting)

- (void)fetchWaiterListFrom:(id)requester succeededHandler:(void(^)(NSArray *waiters))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
- (void)addFriendWithMyUserID:(NSString *)myUserID waiterUserID:(NSString *)waiterUserID from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
