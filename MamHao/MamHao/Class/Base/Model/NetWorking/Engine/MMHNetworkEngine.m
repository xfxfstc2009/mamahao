//
//  MMHNetworkEngine.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkEngine.h"
#import "MMHRequestSerializer.h"
#import "MMHResponseSerializer.h"
#import "MMHNetworkAdapter.h"
#import "MMHFetchModel.h"
#import "MMHTimestamp.h"
#import "MMHAccountSession.h"


@implementation MMHNetworkEngine

+ (MMHNetworkEngine *)sharedEngine
{
    
    static MMHNetworkEngine *_sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{

//#ifdef API_MAMAHAO_COM
         _sharedEngine = [[MMHNetworkEngine alloc] initWithBaseURL:[NSURL URLWithString:@"http://api.mamahao.com:80"]];
//#else
//       _sharedEngine = [[MMHNetworkEngine alloc] initWithBaseURL:[NSURL URLWithString:@"http://192.168.1.236:8080/gd-app-api"]];
//#endif
//        NSDictionary *parameters = [[NSUserDefaults standardUserDefaults] objectForKey:MMHUserDefaultsKeyServerAddress];
//        if ([parameters hasKey:@"server"]) {
//            NSString *server = [parameters stringForKey:@"server"];
//            if (![server isEqualToString:@"standard"]) {
//                NSString *port = [parameters stringForKey:@"port"];
//                if (port.length == 0) {
//                    port = @"8080";
//                }
//                _sharedEngine = [[MMHNetworkEngine alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://192.168.1.%@:%@/gd-app-api", server, port]]];
//            }
//            else {
//                _sharedEngine = [[MMHNetworkEngine alloc] initWithBaseURL:[NSURL URLWithString:@"http://api.mamahao.com:80"]];
//            }
//        }
        _sharedEngine.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        _sharedEngine.requestSerializer = [[MMHRequestSerializer alloc] init];
        _sharedEngine.requestSerializer.timeoutInterval = 10.0;
        _sharedEngine.responseSerializer = [[MMHResponseSerializer alloc] init];
    });
    
    return _sharedEngine;
}


- (instancetype)initWithHost:(NSString *)host
{
    return [self initWithHost:host port:@"8080"];
}


- (instancetype)initWithHost:(NSString *)host port:(NSString *)port
{
    self = [self initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/gd-app-api", host, port]]];
    if (self) {
        self.requestSerializer = [[MMHRequestSerializer alloc] init];
        self.requestSerializer.timeoutInterval = 10.0;
        self.responseSerializer = [[MMHResponseSerializer alloc] init];
    }
    return self;
}


- (void)postWithAPI:(NSString *)api parameters:(NSDictionary *)parameters from:(id)requester responseObjectClass:(Class)responseObjectClass responseObjectKeyMap:(NSDictionary *)responseObjectKeyMap succeededBlock:(void (^)(id responseObject, id responseJSONObject))succeededBlock failedBlock:(MMHNetworkFailedHandler)failedBlock
{
    NSString *actualAPI = [NSString stringWithFormat:@"V1/%@", api];
    NSString *timeIntervalString = [MMHTimestamp timestampString];
    NSMutableDictionary *actualParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    actualParameters[@"ts"] = timeIntervalString;

    if (![[parameters allKeys] containsObject:@"memberId"]) {
        if (![api hasSuffix:@"login.htm"]) {
            NSString *memberID = [[MMHAccountSession currentSession] memberID];
            if (memberID.length != 0) {
                actualParameters[@"memberId"] = memberID;
            }
        }
    }
    LZLog(@"post with api: %@", actualAPI);
    LZLog(@"post with parameters: %@", actualParameters);
    [self POST:actualAPI
    parameters:actualParameters
       success:^(NSURLSessionDataTask *task, id responseObject) {
           NSLog(@"===+++post with api: %@", actualAPI);
           NSLog(@"===+++post with parameters: %@", actualParameters);
           NSLog(@"got response object: %@", responseObject);
           if (responseObject == nil) {
               NSError *error = [NSError errorWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"未知错误"}];
               failedBlock(error);
           }
           if ([responseObject isKindOfClass:[NSDictionary class]]) {
               NSArray *allKeys = [responseObject allKeys];
               if ([allKeys containsObject:@"error"] && [allKeys containsObject:@"error_code"]) {
                   NSError *error = [NSError errorWithDomain:@"MMHErrorDomain"
                                                        code:[responseObject[@"error_code"] integerValue]
                                                    userInfo:@{NSLocalizedDescriptionKey: responseObject[@"error"]}];
                   NSLog(@"got error: %@, request is: %@, parameters: %@", error, task.currentRequest, actualParameters);
                   [MMHLogbook logErrorWithEventName:api error:error];
                   failedBlock(error);
                   return ;
               }
           }

           if (responseObjectClass == nil) {
               succeededBlock(nil, responseObject);
               return;
           }
           if (![responseObjectClass isSubclassOfClass:[MMHFetchModel class]]) {
               succeededBlock(nil, responseObject);
               return;
           }
           if ([responseObjectClass instancesRespondToSelector:@selector(initWithJSONDict:keyMap:)]) {
               MMHFetchModel *responseModelObject = (MMHFetchModel *)[[responseObjectClass alloc] initWithJSONDict:responseObject keyMap:responseObjectKeyMap];
               if (responseModelObject) {
                   succeededBlock(responseModelObject, responseObject);
                   return;
               }
//               [responseModelObject injectJSONData:<#(id)#>]
           }
           NSError *error = [[NSError alloc] initWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey : @"未知错误"}];
           failedBlock(error);
       } failure:^(NSURLSessionDataTask *task, NSError *error) {
           NSLog(@"got error: %@, request is: %@, parameters: %@", error, task.currentRequest, actualParameters);
                [MMHLogbook logErrorWithEventName:api error:error];
                if (failedBlock){
               failedBlock(error);
           }
           
       }];
}
@end
