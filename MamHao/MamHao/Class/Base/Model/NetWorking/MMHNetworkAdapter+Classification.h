//
//  MMHNetworkAdapter+Classification.h
//  MamHao
//
//  Created by 余传兴 on 15/5/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"

@interface MMHNetworkAdapter (Classification)

/**
 *  获取类目
 *
 *  @param typeId           类目id
 *  @param rating           级别
 *  @param succeededHandler 成功回调
 *  @param failedHandler    失败回调
 */
-(void)fetchDataWithTypeId:(NSInteger)typeId rating:(NSInteger)rating succeededHandeler:(void(^)(NSArray *array))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;
/**
 *   获取类目--级联结构
 *
 *  @param typeId           一级类目id
 *  @param succeededHandler 成功回调
 *  @param failedHandler    失败回调
 */
-(void)fetchDataWithtypeId:(NSInteger)typeId succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
