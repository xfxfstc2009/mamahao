//
//  MMHNetworkAdapter+PersonalCenter.h
//  MamHao
//
//  Created by fishycx on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"

@interface MMHNetworkAdapter (PersonalCenter)
/**
 *  fetch member infomation
 *
 *  @param memberId         <#memberId description#>
 */
- (void)fetchMemberInfoWithMemberId:(NSString *)memberId from:(id)requester succeededHandler:(void(^)(NSDictionary *dic))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandle;
@end
