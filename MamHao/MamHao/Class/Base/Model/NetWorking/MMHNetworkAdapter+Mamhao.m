//
//  MMHNetworkAdapter+Mamhao.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter+Mamhao.h"
#import "MMHMonthAgeSelectionView.h"
#import "MamHao-Swift.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHAccountSession.h"
#import "MMHMamhaoUserFeaturedProduct.h"
#import "MMHMamhaoUserFeaturedData.h"
#import "MMHCurrentLocationModel.h"
#import "MMHMamhaoShop.h"
#import "MMHAssistant.h"


@implementation MMHNetworkAdapter (Mamhao)


- (void)fetchMamhaoBeanSpecialDataFrom:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoBeanSpecialProduct *beanSpecialProduct, NSError *error))completion
{
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    [engine postWithAPI:@"home/base/queryActiveGoodMBean.htm"
             parameters:nil
                   from:requester
    responseObjectClass:[MMHMamhaoBeanSpecialProduct class]
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             completion(YES, responseObject, nil);
         } failedBlock:^(NSError *error) {
             completion(NO, nil, error);
         }];
}


- (void)fetchMamhaoShopsFrom:(id)requester completion:(void (^)(BOOL succeeded, NSArray *shops, NSError *error))completion
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"deviceId"] = [UIDevice deviceID];
//#if defined (DEBUG)
//    parameters[@"areaNumId"] = [MMHCurrentLocationModel sharedLocation].areaId;
//#endif
    parameters[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    parameters[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    parameters[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    parameters[@"isSelected"] = @([[MMHCurrentLocationModel sharedLocation] isLocationUserSelected]);

//    #ifdef DEBUG // dazhuang
//    parameters[@"cityNumId"] = @"140100";
//    parameters[@"areaNumId"] = @"140105";
//    parameters[@"lng"] = @(112.554943);
//    parameters[@"lat"] = @(37.817499);
//    #endif

//#ifndef DEBUG
//    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.131"];
//#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
//#endif
    [engine postWithAPI:@"home/base/queryNearbyMemberShops.htm"
             parameters:parameters
                   from:requester
    responseObjectClass:nil
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             NSArray *shops = [responseJSONObject[@"datas"] modelArrayOfClass:[MMHMamhaoShop class]];
             completion(YES, shops, nil);
         } failedBlock:^(NSError *error) {
                completion(NO, nil, error);
            }];
}


- (void)fetchMamhaoCustomersNearShop:(MMHMamhaoShop *)shop from:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    parameters[@"shopId"] = shop.shopId;
    parameters[@"areaNumId"] = shop.areaNumId;
    parameters[@"areaId"] = shop.areaNumId;
//#ifndef DEBUG
//    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.131"];
//#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
//#endif
    [engine postWithAPI:@"home/base/queryNearbyShopMembers.htm"
             parameters:parameters
                   from:requester
    responseObjectClass:nil
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             completion(YES, responseJSONObject, nil);
         } failedBlock:^(NSError *error) {
                completion(NO, nil, error);
            }];
}


- (void)fetchMamhaoMamhaoFeaturedProductFrom:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoMamhaoFeaturedProduct *mamhaoFeaturedProduct, NSError *error))completion {
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    [engine postWithAPI:@"home/base/queryActiveGoodAttention.htm"
             parameters:nil
                   from:requester
    responseObjectClass:[MMHMamhaoMamhaoFeaturedProduct class]
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             completion(YES, responseObject, nil);
         } failedBlock:^(NSError *error) {
                completion(NO, nil, error);
            }];
}


- (void)fetchMamhaoUserFeaturedDataWithMonthAge:(MonthAge *)monthAge from:(id)requester completion:(void (^)(BOOL succeeded, MMHMamhaoUserFeaturedData *userFeaturedData, NSError *error))completion
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    if (monthAge) {
        parameters[@"applyAge"] = [monthAge rawString];
    }
    else if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        parameters[@"applyAge"] = [[MonthAge currentSetMonthAge] rawString];
        NSString *memberID = [[MMHAccountSession currentSession] memberID];
        if (memberID.length != 0) {
            parameters[@"memberId"] = memberID;
        }
    }

    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];

    [engine postWithAPI:@"home/base/queryBabyAgeRelatedGoodsList.htm"
             parameters:parameters
                   from:requester
    responseObjectClass:[MMHMamhaoUserFeaturedData class]
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             completion(YES, responseObject, nil);
         } failedBlock:^(NSError *error) {
                completion(NO, nil, error);
            }];
}


- (void)fetchMamhaoFeaturedProductsForMonthAge:(MonthAge *)monthAge from:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion
{
    // http://localhost:8080/gd-app-api/V1/home/base/queryHomePreferenceList.htm?applyAge=3-6&page=1&count=20&memberId=1
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (monthAge) {
        parameters[@"applyAge"] = [monthAge rawString];
    }
    else if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        parameters[@"applyAge"] = [[MonthAge currentSetMonthAge] rawString];
        NSString *memberID = [[MMHAccountSession currentSession] memberID];
        if (memberID.length != 0) {
            parameters[@"memberId"] = memberID;
        }
    }
    parameters[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    parameters[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    parameters[@"deviceId"] = [UIDevice deviceID];
    parameters[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
//#ifdef DEBUG_LOUIS
//    MMHNetworkEngine *engine = [[MMHNetworkEngine alloc] initWithHost:@"192.168.1.119"];
//#else
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
//#endif
    [engine postWithAPI:@"home/base/queryHomePreferenceList.htm"
                                      parameters:parameters
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      completion(YES, responseJSONObject, nil);
                                  } failedBlock:^(NSError *error) {
                                      completion(NO, nil, error);
                                  }];
}


- (void)fetchMamhaoUserInfoFrom:(id)requester completion:(void (^)(BOOL succeeded, NSDictionary *dictionary, NSError *error))completion
{
    MMHNetworkEngine *engine = [MMHNetworkEngine sharedEngine];
    [engine postWithAPI:@"client/common/query/queryCommonData.htm"
             parameters:nil
                   from:requester
    responseObjectClass:nil
   responseObjectKeyMap:nil
         succeededBlock:^(id responseObject, id responseJSONObject) {
             completion(YES, responseJSONObject, nil);
         } failedBlock:^(NSError *error) {
                completion(NO, nil, error);
            }];
}


- (void)fetchMamhaoActivityImagesFrom:(id)requester completion:(void (^)(BOOL succeeded, NSString *beanGameImageURLString, NSString *categoryImageURLString, NSError *error))completion
{
    [[MMHNetworkEngine sharedEngine] postWithAPI:@"home/base/getHomeActAndCataPic.htm"
                                      parameters:nil
                                            from:requester
                             responseObjectClass:nil
                            responseObjectKeyMap:nil
                                  succeededBlock:^(id responseObject, id responseJSONObject) {
                                      NSString *beanGame = nil;
                                      NSString *category = nil;
                                      NSArray *data = responseJSONObject[@"data"];
                                      for (NSDictionary *aData in data) {
                                          if ([aData[@"key"] isEqualToString:@"ROCK_MBEAN"]) {
                                              beanGame = aData[@"pic"];
                                          }
                                          if ([aData[@"key"] isEqualToString:@"CATEGORY"]) {
                                              category = aData[@"pic"];
                                          }
                                      }
                                      completion(YES, beanGame, category, nil);
                                  } failedBlock:^(NSError *error) {
                completion(NO, nil, nil, error);
            }];
}


@end
