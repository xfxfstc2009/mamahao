//
//  MMHNetworkAdapter+Address.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 地址请求
#import "MMHNetworkAdapter.h"
#import "MMHAddressCityListModel.h"         // 收货地址列表
#import "MMHAddressListModel.h"
@interface MMHNetworkAdapter (Address)

#pragma mark  1 创建新收获地址
-(void)sendRequestCreateAddressWithAreaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat from:(id)requester succeededHandler:(void(^)(NSString *deliveryAddressId))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 2.修改收获地址
-(void)sendRequestUpdateAddressWithDeliveryAddrId:(NSString *)deliveryAddrId areaId:(NSString *)areaId gpsAddress:(NSString *)gpsAddress addressDetail:(NSString *)addressDetail cosignee:(NSString *)cosignee phone:(NSString *)phone isDefault:(BOOL)isDefault lng:(CGFloat)lng lat:(CGFloat)lat from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 3.删除收获地址
-(void)sendRequestDeleteAddressWithDeliveryAddrId:(NSString *)deliveryAddressId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 4. 获取收货地址
-(void)sendRequestGetAddressListFrom:(id)requester succeededHandler:(void(^)(MMHAddressListModel *addressList))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

#pragma mark 5. 获取默认收货地址
-(void)fetchModelUpdateNormolAddressWithAddressId:(NSString *)addressId from:(id)requester succeededHandler:(void(^)())succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
