//
//  MMHAddressChoose.m
//  MamHao
//
//  Created by SmartMin on 15/4/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressChoose.h"

@implementation MMHAddressChoose

// 1. 复制文件内容
-(void)createFile{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:ADDRESS_PLIST_NAME];
    
    // 判断文件是否存在 (使用文件管理器)
    NSFileManager *filemanager=[NSFileManager defaultManager];
    NSString * srcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_PLIST_NAME ofType:nil];
    bool ifFind=[filemanager fileExistsAtPath:path];
    if(!ifFind) {
        NSLog(@"沙盒数据库文件不存在，需要复制!");
        [filemanager copyItemAtPath:srcPath toPath:path error:nil];
    }

    NSError *error1;
    NSString *txtFilePath = [[NSBundle mainBundle] pathForResource:ADDRESS_TXT_NAME ofType:@"txt"];
    NSString *str3=[NSString stringWithContentsOfFile:txtFilePath encoding:NSUTF8StringEncoding error:&error1];
     [self readAddressTxtFileWithString:str3];
}

// 2. 读取地区txt文件
-(void)readAddressTxtFileWithString:(NSString *)string{
    NSMutableArray *addressMutableArr = [NSMutableArray array];
    // 1. 【换成数组】
    NSArray *addressArray=[string componentsSeparatedByString:@"\n"];
    // 2. 【讲item 加入编号】
    for (int i = 0 ; i < addressArray.count;i++){
        NSString * addressName = [addressArray objectAtIndex:i];
        NSString *newAddressName = [addressName stringByAppendingString:[NSString stringWithFormat:@"%i",i+1]];
        [addressMutableArr addObject:newAddressName];
    }
    [self AnalysisWithArr:addressMutableArr];
}

// 3. 转换为数组结构化
-(void)AnalysisWithArr:(NSArray *)addressArr{
    // 1. 加入所有的区为Arr
    // 2. 区arr 存入市数组 - 字典
    // 3. 市arr 存入省数组
    // 4.省数组存入字典
    
    NSMutableArray *shengMutableArr = [NSMutableArray array];
    NSMutableArray *OneShiArr = [NSMutableArray array];
    NSMutableArray *OneQuArr = [NSMutableArray array];
    NSMutableDictionary *TwoShiDic;
    NSMutableArray *shiMutableArr = [NSMutableArray array];
    NSMutableDictionary *countryMutableDic = [NSMutableDictionary dictionary];
    
    //    NSMutableArray
    for (int i = 0 ; i <addressArr.count ; i++){            // [浙江省]  [[杭州市]] [[[萧山区]]]
        NSString *addressName = [addressArr objectAtIndex:i];
        if (!([addressName hasPrefix:@"[["] || [addressName hasPrefix:@"[[["])){            //【省】
            addressName = [addressName stringByReplacingOccurrencesOfString:@"[" withString:@""];
            addressName = [addressName stringByReplacingOccurrencesOfString:@" " withString:@""];
            [shengMutableArr addObject:addressName];
            // 保存最后一个市数组
            if (OneQuArr.count){
                TwoShiDic = [[NSMutableDictionary alloc]init];
                NSString *quName = [OneShiArr lastObject];
                [TwoShiDic setObject:OneQuArr forKey:quName];
                [shiMutableArr addObject:TwoShiDic];
            } else {
                if (OneShiArr.count){
                    NSString *shengName  =[shengMutableArr objectAtIndex:shengMutableArr.count - 2];
                    [countryMutableDic setObject:OneShiArr forKey:shengName];
                    shiMutableArr = [NSMutableArray array];
                    TwoShiDic = [NSMutableDictionary dictionary];
                    OneQuArr = [NSMutableArray array];
                    OneShiArr =[NSMutableArray array];
                }
            }
            
            // 保存数组
            if(shiMutableArr.count){
                //                [shengMutableArr addObject:shiMutableArr];
                [countryMutableDic setObject:shiMutableArr forKey:[shengMutableArr objectAtIndex:shengMutableArr.count - 2]];
                shiMutableArr = [NSMutableArray array];
                TwoShiDic = [NSMutableDictionary dictionary];
                OneQuArr = [NSMutableArray array];
            }
        } else {            // 市   区
            if (![addressName hasPrefix:@"[[["]){                       // 【市】
                addressName = [addressName stringByReplacingOccurrencesOfString:@"[[" withString:@""];
                addressName = [addressName stringByReplacingOccurrencesOfString:@" " withString:@""];
                if (OneQuArr.count){
                    TwoShiDic = [[NSMutableDictionary alloc]init];
                    NSString *quName = [OneShiArr lastObject];
                    [TwoShiDic setObject:OneQuArr forKey:quName];
                    [shiMutableArr addObject:TwoShiDic];
                }
                [OneShiArr addObject:addressName];
                
                OneQuArr = [NSMutableArray array];
            } else {    // 【区】
                addressName = [addressName stringByReplacingOccurrencesOfString:@"[[[" withString:@""];
                addressName = [addressName stringByReplacingOccurrencesOfString:@" " withString:@""];
                [OneQuArr addObject:addressName];
            }
        }
    }
    NSLog(@"%@",countryMutableDic);
    [self writeFileWith:countryMutableDic andKey:@"country"];
}

// 4. 写入plist
-(void)writeFileWith:(NSMutableDictionary *)array andKey:(NSString *)addressKey{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:ADDRESS_PLIST_NAME];
    NSFileManager *fileManager=[NSFileManager defaultManager];
    bool ifFind=[fileManager fileExistsAtPath:path];
    NSString * srcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_PLIST_NAME ofType:nil];
    if(!ifFind) {               // 如果不存在
        [fileManager copyItemAtPath:srcPath toPath:path error:nil];
    }
    
    NSMutableDictionary *data1 = [NSMutableDictionary dictionaryWithContentsOfFile:path];
    
    // 3. 写入数据
    [data1 setObject:array forKey:addressKey];
    [data1 writeToFile:srcPath atomically:YES];
}


-(void)test{
 
}
@end
