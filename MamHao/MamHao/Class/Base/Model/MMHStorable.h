//
//  MMHStorable.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MMHStorable <NSObject, NSCoding>

@end
