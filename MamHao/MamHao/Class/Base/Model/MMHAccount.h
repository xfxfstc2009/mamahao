//
//  MMHAccount.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHActionSheetViewController.h"


@interface MMHAccount : MMHFetchModel <NSCoding>

@property (nonatomic, strong) NSString *memberID;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *nickname;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, strong) NSString *token;

@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *password;

@property (nonatomic, copy) NSString *externalID;
@property (nonatomic) MMHShareType externalType;

@property (nonatomic) MMHID cartID;

- (BOOL)shouldAutoLogin;

- (BOOL)isActivated;
@end
