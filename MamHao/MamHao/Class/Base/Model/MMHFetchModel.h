//
//  MMHFetchModel.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#define MMHBizErrorDomain @"MMHBizErrorDomain"

typedef void (^FetchCompletionHandler) (BOOL isSucceeded, NSError *error);

@interface NSArray (MMHFetchModel)

- (NSArray *)modelArrayOfClass:(Class)modelClass;

@end

@interface MMHFetchModel : NSObject

@property (nonatomic, assign) BOOL bizDataIsNull;
@property (nonatomic, assign) BOOL bizResult;

// 请求参数
@property (nonatomic, strong) NSDictionary *requestParams;

// 自定义的UserAgent信息
+ (NSString *)customUserAgent;

// 请求接口
- (void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;
-(void)fetchWithGetExpressPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;

- (BOOL)isSessionValid;
- (void)clearCookiesForBaseURL;

// 仅供运行时解析JSON使用，子类不要调用此方法初始化对象
- (instancetype)initWithJSONDict:(NSDictionary *)dict;

// 子类需要覆盖此方法，提供model和JSON无法对应到的成员
- (NSDictionary *)modelKeyJSONKeyMapper;

- (void)injectJSONData:(id)dataObject;
- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap;

- (NSArray *)propertiesForCoding;
// equals to -propertiesForCoding by default
- (NSArray *)propertiesForDescription;


@end
