//
//  MMHWordOfMouthViewController.h
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"

@interface MMHWordOfMouthViewController : AbstractViewController

@property (nonatomic, strong) NSString *segeTitle;

@property (nonatomic, assign) NSInteger memberId;

@property (nonatomic, assign) NSInteger goodsTemplateId;

@end
