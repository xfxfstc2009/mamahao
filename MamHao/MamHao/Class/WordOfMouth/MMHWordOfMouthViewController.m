//
//  MMHWordOfMouthViewController.m
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHWordOfMouthViewController.h"
#import "MMHNetworkAdapter.h"
#import "AFNetworking.h"
#import "MMHPublicPraiseModel.h"
#import "MMHPublicPraiseCell.h"
#import "MMHWordOfMouthTableViewCell.h"
#import "MMHPublicPraiseFrameModel.h"
#import "CXStarRatingView.h"
#import "MMHStarRatingView.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHPrasiePeopleCoutCollectionViewCell.h"
@interface MMHWordOfMouthViewController ()<UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, strong) UITableView *tableView;//呈现内容的tableView
@property (nonatomic, strong) NSMutableArray *datasource;//数据源Framemodel的集合

/**
 *  星星
 */
@property (nonatomic, strong) CXStarRatingView *starRatingView;

/**
 *  导航条下放得view，我是放了三个button来实现的
   womBtn -- 商品口碑
 */
@property (nonatomic, strong) UIButton *womBtn;

/**
 *  商品参数
 */
@property (nonatomic, strong) UIButton *goodSparametersBtn;

/**
 *  图文详情
 */
@property (nonatomic, strong) UIButton *imageAndTextBtn;

/**
 * 记录评价总人数
 */

@property (nonatomic, assign) NSInteger count;

@property (nonatomic, strong)NSMutableArray *dataArray;

@end

@implementation MMHWordOfMouthViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
   
    [self pageSetting];
    [self chargeFirstCurrentPage];
}

/**
 *  判断是第一次进入当前页面是 商品口碑，还是商品参数，还是图文详情
 */
- (void)chargeFirstCurrentPage{


    self.segeTitle = @"商品口碑";
    if ([self.segeTitle isEqualToString:@"商品口碑"]) {
        
        self.womBtn.selected = YES;
        [self.tableView registerClass:[MMHWordOfMouthTableViewCell class] forCellReuseIdentifier:@"MMHPubliicPraise"];
        [self.tableView registerClass:[MMHPrasiePeopleCoutCollectionViewCell class] forCellReuseIdentifier:@"Section0Cell"];
        [self.view addSubview:_tableView];
        [self fetchData];
        
    }else if([self.segeTitle isEqualToString:@"商品参数"]){
        //
        
    }else {
        //
        self.segeTitle = @"图文详情";
    }
}

-(UITableView *)tableView{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, MMHFloat(40), self.view.bounds.size.width, self.view.bounds.size.height - MMHFloat(40)) style:UITableViewStylePlain];
        _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}
- (NSMutableArray *)datasource {
    if (!_datasource) {
        self.datasource = [NSMutableArray array];

    }
    return _datasource;
}



/**
 *  获取model
 */
- (void)fetchData {
   
    
    [self.datasource removeAllObjects];
    [[MMHNetworkAdapter sharedAdapter] fetchPublicPrasieWithMemberId:0 goodsTemplateId:0 from:nil succeededHandler:^(NSMutableArray *array) {
        //
        for (NSDictionary *dic in array) {
            MMHPublicPraiseFrameModel *frameModel = [[MMHPublicPraiseFrameModel alloc] init];
            MMHPublicPraiseModel *praiseModel = [[MMHPublicPraiseModel alloc] initWithJSONDict:dic];
            frameModel.praiseModel = praiseModel;
            [self.datasource addObject:frameModel];
        }
        
        [self.tableView reloadData];
    } failedHandler:^(NSError *error) {
        //
       
    }];
    
    
    
   
    
    [[MMHNetworkAdapter sharedAdapter] fetchTotalCountOfPublicPrasieWithMemberId:0 orderNo:0 from:nil suuccedHandler:^(NSMutableArray *array) {
        self.dataArray = array;
        //评论总人数
        NSInteger totalCount = 0;
        
        for (int i = 0; i < 5; i++) {
            NSString *score = [NSString stringWithFormat:@"%d",i+1];
            NSDictionary *dic = _dataArray[i];
            NSInteger peopleCountOfScore = [dic[score] integerValue];
            totalCount += peopleCountOfScore;
        }
        _count = totalCount;
        [self.tableView reloadData];
    } failedHandler:^(NSError *error) {
        //
    }];

   }




/**
 *  页面设置
 */
- (void)pageSetting{

   self.barMainTitle = @"口碑";
   
#warning 导航条还没有设置
    //设置导航条
    
    UIView *segeView = [[UIView alloc] initWithFrame:MMHRectMake(5, 0, 375 - 10, 40)];
    segeView.backgroundColor = [UIColor whiteColor];
    
    UIColor *btnNomalColor = [UIColor colorWithHexString:@"999999"];
    //btn选中时的颜色
    UIColor *btnHightColor = [UIColor colorWithHexString:@"ff4d61"];
    //商品口碑
    
    UIButton *womBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.womBtn = womBtn;
    
    [womBtn setTitleColor: btnNomalColor forState:UIControlStateNormal];
    
    [womBtn setTitleColor: btnHightColor forState:UIControlStateSelected];
    womBtn.titleLabel.font = MMHFontWithSize(15);
    [womBtn addTarget:self action:@selector(handleBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [womBtn setTitle:@"商品口碑" forState:UIControlStateNormal];
    womBtn.frame = CGRectMake(MMHFloat(30), 0, MMHFloat(60), MMHFloat(40));
    
    [segeView addSubview:womBtn];
    
    //商品参数
    
    
    UIButton *goodsParametersBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.goodSparametersBtn = goodsParametersBtn;
    [goodsParametersBtn setTitleColor:btnNomalColor forState:UIControlStateNormal];
    [goodsParametersBtn setTitleColor:btnHightColor forState:UIControlStateSelected];
    goodsParametersBtn.titleLabel.font = MMHFontWithSize(15);
    [goodsParametersBtn addTarget:self action:@selector(handleBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [goodsParametersBtn setTitle:@"商品参数" forState:UIControlStateNormal];
    goodsParametersBtn.frame = CGRectMake(MMHFloat(30) + MMHFloat(60 + 62.5), 0, MMHFloat(60), MMHFloat(40));
    
    [segeView addSubview:goodsParametersBtn];
    
    
    //图文详情
    UIButton *imageAndTextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.imageAndTextBtn = imageAndTextBtn;
    [imageAndTextBtn setTitleColor:btnNomalColor forState:UIControlStateNormal];
    [imageAndTextBtn setTitleColor:btnHightColor forState:UIControlStateSelected];
    imageAndTextBtn.titleLabel.font = MMHFontWithSize(15);
    
    [imageAndTextBtn addTarget:self action:@selector(handleBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [imageAndTextBtn setTitle:@"图文详情" forState:UIControlStateNormal];
    imageAndTextBtn.frame = CGRectMake(MMHFloat(30) + MMHFloat(60 + 62.5)*2, 0, MMHFloat(60), MMHFloat(40));
    
    [segeView addSubview:imageAndTextBtn];
    
    
    [self.view  addSubview:segeView];

}





/**
 *  上面类似segement的东东   -Button点击事件
 *
 *  @param sender 点击的button
 */
- (void)handleBtnAction:(UIButton *)sender{
   
    NSString *str = sender.titleLabel.text;
    if ([str isEqualToString:@"商品口碑"]) {
        
        
        if (!sender.selected) {
            // 切换界面
            [self.tableView registerClass:[MMHWordOfMouthTableViewCell class] forCellReuseIdentifier:@"MMHPubliicPraise"];
            [self.view addSubview:_tableView];
        }
        sender.selected = YES;
        self.goodSparametersBtn.selected = NO;
        self.imageAndTextBtn.selected = NO;
        
    }
    if ([str isEqualToString:@"商品参数"]) {
        
        [self.tableView removeFromSuperview];
        sender.selected = YES;
        self.womBtn.selected = NO;
        self.imageAndTextBtn.selected = NO;
   
    }
    if ([str isEqualToString:@"图文详情"]) {
        [self.tableView removeFromSuperview];
        sender.selected = YES;
        self.goodSparametersBtn.selected = NO;
        self.womBtn.selected = NO;
   
    }

}




#pragma mark --Datasource协议方法


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    /**
     *  根据model的评价人数是否大于20来判断
     */
  
    if (_count >= 20) {
        return 2;
    }else{
    return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_count >= 20 && section == 0) {
        return 1;
    }
    return self.datasource.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if (_count >= 20 && indexPath.section == 0) {
        
    
        MMHPrasiePeopleCoutCollectionViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Section0Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
      
        [cell configCellWithCountOfPraise:self.dataArray];
        return cell;
    }
    
    MMHWordOfMouthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MMHPubliicPraise" forIndexPath:indexPath];
    
    
    //配置cell
    
    MMHPublicPraiseFrameModel *model = self.datasource[indexPath.row];
    cell.separatorInset = UIEdgeInsetsMake(0, 7, 0, 10);
    cell.frameModel = model;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark -UITableViewDelegate协议方法

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return MMHFloat(10);
    
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor clearColor];
        return view;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (_count >= 20 && indexPath.section == 0) {
        return MMHFloat(171);
    }
    MMHPublicPraiseFrameModel *frameModel = self.datasource[indexPath.row];
    
    return frameModel.cellHeightF;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_count >= 20 && section == 0){
        return MMHFloat(0);
    } else {
        return MMHFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
