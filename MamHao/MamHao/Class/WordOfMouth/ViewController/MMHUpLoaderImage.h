//
//  MMHUpLoaderImage.h
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHNetworkAdapter+Image.h"
//typedef void(^MMHNetworkFailedHandler)(NSError *error);
@interface MMHUpLoaderImage : NSObject

@property (nonatomic,readonly,strong)NSString *fileID;
@property (nonatomic,readonly,assign)BOOL isUpLoaded;
@property (nonatomic,readonly,strong)NSError *error;
@property (nonatomic,assign)NSInteger tag;
-(instancetype)initWithImage:(UIImage *)image;
- (void)startUpLoader;
-(void)startLoaderWithImage:(UIImage*)image from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
