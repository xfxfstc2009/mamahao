//
//  MMHUploaderImageViewController.m
//  MamHao
//
//  Created by fishycx on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUploaderImageViewController.h"
#import "MMHUploaderImageManager.h"
#import "MMHNetworkAdapter+Image.h"
@interface MMHUploaderImageViewController ()

@end

@implementation MMHUploaderImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeContactAdd];
    [btn addTarget:self action:@selector(handleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(200, 200, 200, 200);
    [self.view addSubview:btn];
    
}
-(void)handleButtonClick:(UIButton *)sender{
    UIImage *image1 = [UIImage imageNamed:@"address_btn_add"];
    UIImage *image2 = [UIImage imageNamed:@"address_icon_untick"];
    NSArray *array = [NSArray arrayWithObjects:image1,image2, nil];
    [[MMHUploaderImageManager shareManager] startUpLoaderWithImages:array succeededHandler:^(BOOL isAllImageLoaded, NSArray *fileName) {
        NSLog(@"----------->所有图片上传成功");
        NSLog(@"%@", fileName);
    } failedHandler:^(NSError *error) {
        NSLog(@"----------->%@", error);
    }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
