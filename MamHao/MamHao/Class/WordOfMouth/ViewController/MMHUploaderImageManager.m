//
//  MMHUploaderImageManager.m
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUploaderImageManager.h"
@interface MMHUploaderImageManager ()<MMHUploaderImageToolDelegate>
@property (nonatomic, assign)NSInteger uploadedCount;
@property (nonatomic, strong)NSMutableArray *images;
@property (nonatomic, strong)NSMutableString *fileName;
@property (nonatomic, strong)NSMutableArray *fileIds;
@property (nonatomic, strong)NSMutableArray *errors;
@property (nonatomic, strong)NSMutableArray *uploaders;
@property (nonatomic, assign)NSInteger uploadCount; //上传个数；有错有对的
@property (nonatomic, assign)BOOL isAllImageLoadSucceeded; //所有都上传成功
@property (nonatomic, assign)BOOL isAllImageLoad;//所有图片完场上传，有错误，有成功返回
@property (nonatomic, assign)isAllImageLoaded succeedHandler;
@property (nonatomic, assign)MMHUploaderFailedHandler failedHandler;
@end
@implementation MMHUploaderImageManager
+(instancetype)shareManager{
    return [[MMHUploaderImageManager alloc] init];
}

-(void)startUpLoaderWithImages:(NSArray *)images succeededHandler:(isAllImageLoaded)succeedHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    self.succeedHandler = succeedHandler;
    self.failedHandler = failedHandler;
    [self.images addObjectsFromArray:images];
    for (int i = 0; i<images.count; i++) {
        UIImage *image = images[i];
        
        
        MMHUploaderImageTool *uploader= [[MMHUploaderImageTool alloc] initWithImage:image WithUploader:^(NSString *fileId, NSError *error) {
            if (fileId) {
                [self.fileIds addObject:fileId];
            }else{
              [self.errors addObject:error];
            }
            if ([self isAllImageLoad]) {
                if ([self isAllImageLoadSucceeded]) {
                    succeedHandler(YES, self.fileIds);
                }else{
                    failedHandler(error);
                }
            }else{
              //继续上传
            }
        }];
//        MMHUploaderImageTool *uploader = [[MMHUploaderImageTool alloc] initWithImage:image delegate:self];
//        uploader.delegate = self;
        [uploader startUpLoader];
        [self.uploaders addObject:uploader];
    }
}


- (void)upLoadImage:(NSString *)fileId error:(NSError *)error{
    [self.fileIds addObject:fileId];
    NSLog(@"%@", self.fileIds);
}
//所有图片都开始上传
- (BOOL)isAllImageLoad {
    for (MMHUploaderImageTool *uploaderImageTool in self.uploaders) {
        if (uploaderImageTool.iscompleted) {
            self.uploadCount++;
        }
    }
    if (self.uploadCount == self.images.count) {
        return YES;
    }else{
        return NO;
   }
}
//所有图片上传完成
- (BOOL)isAllImageLoadSucceeded{
    for (MMHUploaderImageTool *uploaderImageTool in self.uploaders) {
        if (uploaderImageTool.error) {
            return NO;
        }
    }
    return YES;
}

- (NSMutableArray *)fileIds{
    if (!_fileIds) {
        self.fileIds = [NSMutableArray array];
    }
    return _fileIds;
}
- (NSMutableArray *)uploaders {
    if (!_uploaders) {
        self.uploaders = [NSMutableArray array];
    }
    return _uploaders;
}
- (NSMutableArray *)errors {
    if (!_errors) {
        self.errors = [NSMutableArray array];
    }
    return _errors;
}
@end
