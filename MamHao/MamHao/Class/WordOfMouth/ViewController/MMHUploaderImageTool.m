//
//  MMHUploaderImageTool.m
//  MamHao
//
//  Created by fishycx on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUploaderImageTool.h"

@interface MMHUploaderImageTool ()
@property (nonatomic, strong)UIImage *praiseImage;
@property (nonatomic, strong)id<MMHUploaderImageToolDelegate>uploaderDelegate;
@end
@implementation MMHUploaderImageTool


-(instancetype)initWithImage:(UIImage *)praiseImage WithUploader:(singleImageUploader)uploader{
    if (self = [super init]) {
        self.praiseImage = praiseImage;
        self.uploader = uploader;
    }
    return self;
}

- (instancetype)initWithImage:(UIImage *)praiseImage delegate:(id)delegate{
    if (self = [super init]) {
        self.praiseImage = praiseImage;
        self.delegate = delegate;
    }
    return self;
}


- (void)startUpLoader{
    [[MMHNetworkAdapter sharedAdapter] uploadCommentImage:self.praiseImage from:nil succeededHandler:^(NSString *fileID) {
        self.fileID = fileID;
        self.uploader(fileID, nil);
        self.iscompleted = YES;
//        if (self.delegate&&[self.delegate respondsToSelector:@selector(upLoadImage:error:)]) {
//            [self.delegate upLoadImage:fileID error:nil];
//        }
    } failedHandler:^(NSError *error) {
//        if (self.delegate&&[self.delegate respondsToSelector:@selector(upLoadImage:error:)]) {
//            [self.delegate upLoadImage:nil error:error];
//        }
        self.iscompleted = YES;
        self.error = error;
        self.uploader(nil, error);
    }];
}



@end
