//
//  MMHPraiseShowOrderViewController.h
//  MamHao
//
//  Created by 余传兴 on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHOrderSingleModel.h"
#import "MMHSingleProductWithShopCartModel.h"




typedef void(^pingjiaCallBackBlock)(BOOL isAllpraise);

@interface MMHPraiseShowOrderViewController : AbstractViewController

@property (nonatomic, strong)MMHOrderSingleModel *orderSingleModel; //单个订单的model

@property (nonatomic, strong)MMHSingleProductWithShopCartModel *singleProductModel; //

@property (nonatomic,copy)pingjiaCallBackBlock pingjiaCallBackBlock;

@end
