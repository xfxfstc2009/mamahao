//
//  MMHWordOfMouthViewController.h
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


typedef NS_ENUM(NSInteger, MMHWordOfMouthViewControllerContentType) {
    MMHWordOfMouthViewControllerContentTypeWordOfMouth = 0,
    MMHWordOfMouthViewControllerContentTypeProductSpecs,
    MMHWordOfMouthViewControllerContentTypeProductDetailHTML,
};


@interface MMHWordOfMouthViewController : AbstractViewController

- (instancetype)initWithContentType:(MMHWordOfMouthViewControllerContentType)contentType;

@property (nonatomic, copy) NSString *goodsTemplateId; //传递的参数
@property (nonatomic, copy) NSString *itemNumId; //商品id；

@end
