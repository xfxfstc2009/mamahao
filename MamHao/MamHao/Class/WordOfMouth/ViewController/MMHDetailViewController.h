//
//  MMHDetailViewController.h
//  MamHao
//
//  Created by fishycx on 15/7/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHDetailViewController : UIViewController
@property (nonatomic, strong) UIImage *image;
@end
