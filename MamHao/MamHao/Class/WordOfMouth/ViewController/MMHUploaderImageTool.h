//
//  MMHUploaderImageTool.h
//  MamHao
//
//  Created by fishycx on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHNetworkAdapter+Image.h"
@class MMHUploaderImageTool;

typedef void(^singleImageUploader)(NSString *fileId, NSError *error);

@protocol MMHUploaderImageToolDelegate <NSObject>

- (void)upLoadImage:(NSString *)fileId error:(NSError *)error;

@end

@interface MMHUploaderImageTool : NSObject

@property (nonatomic, weak)id<MMHUploaderImageToolDelegate>delegate;
@property (nonatomic,strong)NSString *fileID;
@property (nonatomic,strong)NSError *error;
@property (nonatomic,assign)BOOL iscompleted;
@property (nonatomic,strong)singleImageUploader uploader;
-(instancetype)initWithImage:(UIImage *)praiseImage WithUploader:(singleImageUploader)uploader;
-(instancetype)initWithImage:(UIImage *)praiseImage delegate:(id)delegate;
- (void)startUpLoader;

@end
