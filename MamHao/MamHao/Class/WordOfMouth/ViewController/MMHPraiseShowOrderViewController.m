//
//  MMHPraiseShowOrderViewController.m
//  MamHao
//
//  Created by 余传兴 on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPraiseShowOrderViewController.h"
#import "MMHOrderProductTableViewCell.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHPraiseOrderTableViewCell.h"
#import "MMHGoodsPraiseViewController.h"
#import "MMHPraiseShowOrderTableViewCell.h"
#import "MMHPraiseOrderTableViewCell2.h"
#import "MMHOrderRootViewController.h"
#import "MJRefresh.h"

@interface MMHPraiseShowOrderViewController ()<UITableViewDelegate, UITableViewDataSource, MMhPraiseShowOrderCellDelegate, MMHPraiseOrderTableViewCell2Delegate,MMHPraiseOrderTableViewCell1Delegate>

@property (nonatomic, strong)UITableView *tableView;
@property (nonatomic, strong)NSMutableArray *dataSource;//数据源
@property (nonatomic, assign)BOOL isExpressDelivery;    //是不是快递送货
@property (nonatomic, assign)NSInteger praiseCount;     //评价次数；
@property (nonatomic, assign)NSString *stringTag;
@property (nonatomic, strong)NSDictionary *commentDic;  //订单评论
@property (nonatomic, assign)BOOL isOrderCommented;     //是否评价过；
@property (nonatomic ,assign)BOOL isAllGoodsPraised;    //全部商品是否评价完毕
@property (nonatomic, strong)UITextView *cellTextView;  //评价文字输入
@property (nonatomic, assign)BOOL isAllpraise;          //所有评价次数
@property (nonatomic, assign)NSInteger currentPage;
@property (nonatomic,strong)UIButton *leftButton;       /**< 左侧按钮*/
@property (nonatomic, strong)NSString *warehourseId;    /**< 仓储id*/
@end

@implementation MMHPraiseShowOrderViewController


#pragma mark - getter method
- (NSMutableArray *)dataSource {
    if (!_dataSource) {
        self.dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

-(UITableView *)tableView {
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
        
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

#pragma mark --- lift circle
- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self pageSetting];
    //[self fetchData];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"PUBLISHED" object:self.stringTag];
    [self.tableView registerClass:[MMHPraiseShowOrderTableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.tableView registerClass:[MMHPraiseOrderTableViewCell class] forCellReuseIdentifier:@"orderWomCell"];
    [self.tableView registerClass:[MMHPraiseOrderTableViewCell2 class] forCellReuseIdentifier:@"orderWomCellNotExpressDelivery"];
    self.currentPage = 1;
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
   [[NSNotificationCenter defaultCenter] postNotificationName:KOrderPraiseContentEditDone object:nil];
}
#pragma mark - private Method
- (void)handleNotification:(NSNotification *)notification{
    self.stringTag = notification.object;
}
- (void)pageSetting{
    self.barMainTitle = @"评价晒单";
    __weak typeof(self)weakSelf = self;
    self.leftButton = [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.isAllpraise){      // 评价完成
            strongSelf.pingjiaCallBackBlock(YES);
        } else {
          strongSelf.pingjiaCallBackBlock(NO);
        }
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [self fetchData];
}

- (void)fetchData{
    __weak typeof(self)weakSelf = self;
   [[MMHNetworkAdapter sharedAdapter] fetchGoodsOfOrderWithOrderNo:weakSelf.orderSingleModel.orderNo page:weakSelf.currentPage count:20  from:nil succeedHandler:^(NSDictionary *dic) {
       if (!weakSelf) {
           return ;
       }
       __strong typeof(weakSelf)strongSelf=weakSelf;
       NSArray *array = dic[@"rows"];
       strongSelf.warehourseId = dic[@"warehouseId"];
       strongSelf.commentDic = dic[@"comment"];//店铺发货速度，服务评论
        if ([strongSelf.commentDic[@"serveStar"] integerValue] == 0) {
               strongSelf.isOrderCommented = NO;
           }else{
               strongSelf.isOrderCommented = YES;
           }
       if ([strongSelf.commentDic hasKey:@"deliverySpeedStar"]) {
           strongSelf.isExpressDelivery = YES;
       }else{
          strongSelf.isExpressDelivery = NO;
       }
       for (NSDictionary *dic1 in array) {
           MMHSingleProductWithShopCartModel *model = [[MMHSingleProductWithShopCartModel alloc] initWithJSONDict:dic1];
           if (strongSelf.dataSource.count){
               [strongSelf.dataSource removeAllObjects];
           }
           [strongSelf.dataSource addObject:model];
       }
       if ([strongSelf isAllGoodsPraised] && strongSelf.isOrderCommented) {
           //订单评价完成，商品评价完成
           strongSelf.isAllpraise = YES;
       }
       [strongSelf.tableView reloadData];
   } failedHandler:^(NSError *error) {
       NSLog(@"%@", error);
   }];

}
-(BOOL)isAllGoodsPraised{
    NSInteger praiseDoneCount = 0;
    for (MMHSingleProductWithShopCartModel *model in self.dataSource) {
        if (model.commentCount == 2||model.commentCount==1) {
            praiseDoneCount++;
        }
    }
    if (praiseDoneCount == self.dataSource.count) {
        return YES;
    }else{
        return NO;
    }
}
#pragma mark -- cell1delegate method
- (void)cell1praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger)star deliverySpeedStar:(NSInteger)deliverSpeedStar commentContent:(NSString *)comment {
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    //发表评价
    [[MMHNetworkAdapter sharedAdapter] praiseOrderWithOrderNo:orderNo serveStar:star deliverySpeedStar: deliverSpeedStar  commentContent:comment from:nil succeedHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        [strongSelf.view hideProcessingView];
        [strongSelf.view showTips:@"该订单评价完成"];
        [strongSelf fetchData];
    } faileHandler:^(NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        [strongSelf.view hideProcessingView];
    }];

}
#pragma mark -- cell2delegate method
- (void)praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger)star deliverySpeedStar:(NSInteger)deliverSpeedStar commentContent:(NSString *)comment {
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] praiseOrderWithOrderNo:orderNo serveStar:star deliverySpeedStar: 10 commentContent:comment from:nil succeedHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        [strongSelf.view hideProcessingView];
        [strongSelf.view showTips:@"该订单评价完成"];
        //判断是否执行成功
        [strongSelf fetchData];
    } faileHandler:^(NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf =weakSelf;
        [strongSelf.view hideProcessingView];
    }];

 
}
- (void)textViewDidEndEidt:(UITextView *)textView{
    self.navigationItem.rightBarButtonItem = nil;
}
-(void)textViewBeginEdit:(UITextView *)textView{
    self.cellTextView = textView;
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(leaveEditMode)];
//    self.navigationItem.rightBarButtonItem = item;
}

#pragma mark - datasource


-  (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.dataSource.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == self.dataSource.count) {
        if (self.isExpressDelivery) {
            static NSString *identifer = @"orderWomCell";
            MMHPraiseOrderTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:identifer forIndexPath:indexPath];
            cell.backgroundColor = [UIColor whiteColor];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.orderId = self.orderSingleModel.orderNo;
            [cell setStarControlRatingWithStarCount:[self.commentDic[@"serveStar"] integerValue] StarControl:0];
            if (![self.commentDic[@"deliverySpeedStar"] isKindOfClass:[NSNull class]]) {
    
                [cell setStarControlRatingWithStarCount:[self.commentDic[@"deliverySpeedStar"] integerValue] StarControl:1];
            }
            cell.cellDelegate = self;
            cell.rightBarItem = self.navigationItem.rightBarButtonItem;
            [cell setButtonStateWithBOOl:self.isOrderCommented];
            [cell setUserInteractionEnabled:!self.isOrderCommented];
            return cell;
        }else{
           static NSString *identifier = @"orderWomCellNotExpressDelivery";
            MMHPraiseOrderTableViewCell2 *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
            [cell setStarControlRatingWithStarCount:[self.commentDic[@"serveStar"] integerValue] StarControl:0];
            [cell setButtonStateWithBOOl:self.isOrderCommented];
           // [cell setUserInteractionEnabled:self.isOrderCommented];
            cell.backgroundColor = [UIColor whiteColor];
            cell.orderId = self.orderSingleModel.orderNo;
            cell.cellDelegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            return cell;
        }
    }         
    MMHPraiseShowOrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.backgroundColor = [UIColor whiteColor];
    cell.stringTag = [NSString stringWithFormat:@"%li", (long)indexPath.section];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.singleProduct = self.dataSource[indexPath.section];
    return cell;
}

- (void)leaveEditMode{
    [self.cellTextView resignFirstResponder];
    self.navigationItem.rightBarButtonItem = nil;
}

#pragma mark - MMHPraiseShowOrderCellDelegate

-(void)praiseShowOrederCell:(MMHPraiseShowOrderTableViewCell *)cell {
    MMHGoodsPraiseViewController *goodsPraiseVC = [[MMHGoodsPraiseViewController alloc] init];
    goodsPraiseVC.singleModel = self.orderSingleModel;
    goodsPraiseVC.goodsModel = cell.singleProduct;
    goodsPraiseVC.praiseBtn = cell.orderTypeButton;
    goodsPraiseVC.warehourseId = self.warehourseId;
    [self.navigationController pushViewController:goodsPraiseVC animated:YES];
}

#pragma mark-UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
    return view;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == self.dataSource.count) {
        if (self.isExpressDelivery) {
            if (!self.isOrderCommented) {
                return MMHFloat(90)+MMHFloat(75)+MMHFloat(20)+MMHFloat(30)+MMHFloat(10);
            }
            return MMHFloat(90);
        }else{
            if (!self.isOrderCommented) {
                return MMHFloat(55+75+20+30);
            }
            return MMHFloat(55);
        }
    }
    return MMHFloat(100);
}

- (void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"PUBLISHED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"praiseAllDone" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
