//
//  MMHUpLoaderImage.m
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHUpLoaderImage.h"
#import "MMHNetworkAdapter+Image.h"

@interface MMHUpLoaderImage ()
@property (nonatomic, strong)UIImage *image;
@end
@implementation MMHUpLoaderImage

-(instancetype)initWithImage:(UIImage *)image{
    if (self = [super init]) {
        self.image = image;
    }
    return self;
}

- (void)startUpLoader{
    [[MMHNetworkAdapter sharedAdapter] uploadCommentImage:self.image from:nil succeededHandler:^(NSString *fileID) {
        _fileID = fileID;
        _isUpLoaded = YES;
    } failedHandler:^(NSError *error) {
        _error = error;
    }];

}

-(void)startLoaderWithImage:(UIImage*)image from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler{
    [[MMHNetworkAdapter sharedAdapter] uploadCommentImage:image from:nil succeededHandler:^(NSString *fileID) {
        succeededHandler(fileID);
    } failedHandler:^(NSError *error) {
        failedHandler(error);
    }];

}


@end
