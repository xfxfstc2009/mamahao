//
//  MMHGoodsPraiseViewController.m
//  MamHao
//
//  Created by 余传兴 on 15/5/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGoodsPraiseViewController.h"
#import "MMHAssetLibraryViewController.h"
#import "AMRatingControl.h"
#import "MMHGoodsPraiseCollectionViewCell.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "NSString+catagory.h"
#import "MMHImageView.h"
#import "MMHPraiseShowOrderViewController.h"
#import "MMHNetworkAdapter+Image.h"
#import "MMHUploadStateModel.h"
#import "MMHUploaderImageManager.h"
#import "MMHUploadImageManager.h"

@interface MMHGoodsPraiseViewController ()<UITextViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, MMHGoodsPrasiseCollectionViewCellDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (nonatomic, strong)UITextView *textView;
@property (nonatomic, strong)NSMutableArray *imageDataSource;
@property (nonatomic, strong)UICollectionView *collectionView;
@property (nonatomic, strong)UIView *headerView;/**页眉，上面放一条商品信息，商品评价文字输入框*/
@property (nonatomic, strong)UILabel *placeholder;
@property (nonatomic, strong)UIImage *iconAdd; //添加按钮对应的图片
@property (nonatomic, assign)NSInteger startcount; //保存星星的个数
@property (nonatomic, strong)NSString *praisecontent;//保存评价内容
@property (nonatomic, strong)UIButton *rightButton;
@end

@implementation MMHGoodsPraiseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createrCollectionView];
    [self pageSetting];
    [self createHeaderView];
    UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeGesture:)];
    swipeGestureRecogniser.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeGestureRecogniser];
    
//    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
//    [self.view addGestureRecognizer:tapGestureRecognizer];
 self.rightButton = [self rightBarButtonWithTitle:@"编辑" barNorImage:nil barHltImage:nil action:^{
     if ([self.rightButton.titleLabel.text isEqualToString:@"编辑"]) {
         [self.rightButton setTitle:@"完成" forState:UIControlStateNormal];
         [self.textView becomeFirstResponder];
     }else{
     
         [self.rightButton setTitle:@"编辑" forState:UIControlStateNormal];
         [self.textView resignFirstResponder];
     }
   }];
    
}

- (void)handleTapGesture:(UITapGestureRecognizer *)gesture{
    [self.textView resignFirstResponder];
}
-(void)handleSwipeGesture:(UISwipeGestureRecognizer *)gesture{
    [self.textView resignFirstResponder];
}
- (void)pageSetting{
    self.barMainTitle = @"商品评价";
    [self setToolView];
}

- (NSMutableArray *)imageDataSource {
    if (!_imageDataSource) {
        self.iconAdd = [UIImage imageNamed:@"score_icon_addpicture"];
        self.imageDataSource = [[NSMutableArray alloc] initWithObjects:_iconAdd, nil];
    }
    return _imageDataSource;
}


/**
 *  创建UICollectionView的页眉,商品信息，星级，评价内容是放在页眉上的
 */
- (void)createHeaderView{

    UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 230)];
   _headerView = headerview;
    MMHImageView *goodsIcon = [[MMHImageView alloc] initWithFrame:CGRectMake(MMHFloat(20), 20, MMHFloat(70), MMHFloat(70))];
    goodsIcon.layer.cornerRadius = MMHFloat(6.);
    goodsIcon.layer.masksToBounds = YES;
    goodsIcon.layer.borderWidth = .5;
    goodsIcon.layer.borderColor = [UIColor grayColor].CGColor;
    [goodsIcon updateViewWithImageAtURL:self.goodsModel.itemPic];
    [headerview addSubview:goodsIcon];
    
    UILabel *textL = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(goodsIcon.frame) + MMHFloat(13), 20, MMHFloat(165+13*3), 35)];
    textL.numberOfLines = 2;
    textL.font = MMHFontOfSize(14.);
    textL.text = self.goodsModel.itemName;
    textL.textColor = [UIColor colorWithCustomerName:@"黑"];
    [headerview addSubview:textL];
    
    CGFloat skuViewWidth = 0;
    for (int i = 0 ; i < self.goodsModel.specSKU.count; i++) {
        UILabel *skuLabel = [[UILabel alloc]init];
        skuLabel.backgroundColor = [UIColor clearColor];
        skuLabel.font = [UIFont systemFontOfSize:MMHFloat(14.)];
        skuLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        skuLabel.stringTag = [NSString stringWithFormat:@"skuLabel%i",i];
        skuLabel.textAlignment = NSTextAlignmentLeft;
        // 计算文字长度
        CGSize skuContentSize = [[[self.goodsModel.specSKU objectAtIndex:i] skuValue] sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(14.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(14.))];
        skuLabel.frame = CGRectMake(CGRectGetMaxX(goodsIcon.frame) + MMHFloat(13.)+MMHFloat(5.) * i + skuViewWidth, 50, skuContentSize.width, 14);
        skuViewWidth = skuContentSize.width;
        skuLabel.text = [[self.goodsModel.specSKU objectAtIndex:i] skuValue];
        [headerview addSubview:skuLabel];
    }
    
    UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(goodsIcon.frame) + 20, self.view.bounds.size.width, 1)];
    lineLabel.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [headerview addSubview:lineLabel];
    
    UILabel *starL = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(20),CGRectGetMaxY(goodsIcon.frame)+ 36, MMHFloat(80), 15)];
    starL.text = @"商品评价:";
    starL.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    starL.font = MMHFontOfSize(15.);
    [headerview addSubview:starL];
    UIImage *star, *dot;
    dot = [UIImage imageNamed:@"score_icon_bigstar_us"];
    star = [UIImage imageNamed:@"score_icon_bigstar_s"];
    
    AMRatingControl *simpleRatingControl =[[AMRatingControl alloc] initWithLocation:CGPointMake(CGRectGetMaxX(starL.frame), CGRectGetMaxY(lineLabel.frame) + 15)  emptyImage:dot solidImage:star andMaxRating:5];
    self.startcount = 5;
    [simpleRatingControl setRating:5];
    
    // Listen to control events
    [simpleRatingControl addTarget:self action:@selector(updateRating:) forControlEvents:UIControlEventEditingChanged];
    [simpleRatingControl addTarget:self action:@selector(updateEndRating:) forControlEvents:UIControlEventEditingDidEnd];
    [headerview addSubview:simpleRatingControl];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(MMHFloat(20), CGRectGetMaxY(lineLabel.frame) + 45,self.view.bounds.size.width - MMHFloat(40), 75)];
    self.textView = textView;
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    textView.delegate = self;
    textView.layer.borderWidth = 1;
    textView.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor
    ;
    textView.keyboardType =  UIKeyboardTypeDefault;
    textView.textColor = [UIColor colorWithCustomerName:@"灰"];
    textView.textAlignment = NSTextAlignmentLeft;
    textView.font = MMHFontOfSize(14);
    
    
    self.placeholder = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(30),CGRectGetMaxY(lineLabel.frame) + 55 , self.view.bounds.size.width - MMHFloat(60), 12)];
    _placeholder.backgroundColor = [UIColor clearColor];
    _placeholder.enabled = NO;
    _placeholder.font = MMHFontOfSize(12);
    _placeholder.text = @"评价一下这件宝贝，您的评价对其他妈妈很重要哦";
    _placeholder.textColor = [UIColor colorWithCustomerName:@"白灰"];
    [headerview addSubview:textView];
    [headerview addSubview:_placeholder];
   
}


- (void)passOrderSingleModel:(MMHOrderSingleModel *)model Block:(PraiseProductDone)praiseProductFinished {
}

#pragma mark - Private Methods

- (void)updateRating:(id)sender
{
    NSLog(@"Rating: %ld", (long)[(AMRatingControl *)sender rating]);
   
}

- (void)updateEndRating:(id)sender
{
    NSLog(@"End Rating: %ld", (long)[(AMRatingControl *)sender rating]);
    NSInteger starCount = (long)[(AMRatingControl *)sender rating];
    self.startcount = starCount;
    
}

/**
 *  创建UICollectionView
 */
- (void)createrCollectionView{

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = MMHSizeMake(60, 60);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = MMHFloat(10);
    layout.headerReferenceSize = CGSizeMake(self.view.bounds.size.width, 230);
    
    CGRect frame = CGRectMake(0,0, self.view.bounds.size.width,self.view.bounds.size.height - 64 -50);
    UICollectionView *collectionview = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    self.collectionView = collectionview;
    layout.sectionInset = UIEdgeInsetsMake(20, MMHFloat(20), 20, MMHFloat(65) );
    [collectionview registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    collectionview.backgroundColor = [UIColor whiteColor];
    
    [collectionview registerClass:[MMHGoodsPraiseCollectionViewCell class] forCellWithReuseIdentifier:@"collectionViewCell"];
    collectionview.delegate = self;
    collectionview.dataSource = self;
    
    [self.view addSubview:collectionview];
}

/**
 *  设置底部的按钮
 */
- (void)setToolView{

    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 64 -50, self.view.bounds.size.width, 50)];
    toolView.userInteractionEnabled = YES;
    UIButton *publishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    publishBtn.frame = CGRectMake(self.view.bounds.size.width - MMHFloat(110), 5, MMHFloat(100), 40);
    publishBtn.layer.cornerRadius = MMHFloat(10);
    [publishBtn setTitle:@"发表评价" forState:UIControlStateNormal];
   // publishBtn.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
    [publishBtn setBackgroundImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"粉"] renderSize:CGSizeMake(MMHFloat(100), MMHFloat(40))] forState:UIControlStateNormal];
    [publishBtn setBackgroundImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"红"] renderSize:CGSizeMake(MMHFloat(100), MMHFloat(40))] forState:UIControlStateHighlighted];
    [publishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    publishBtn.titleLabel.font = MMHFontOfSize(16);
    [publishBtn addTarget:self action:@selector(handleBtnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:publishBtn];
    [self.view addSubview:toolView];
    
}

- (void)handleBtnClickAction:(UIButton *)sender {
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.imageDataSource];
    if (array.count == 9) {
    }else{
        [array removeLastObject];
    }
    if (array.count) {
        [MMHUploadImageManager uploadCommentImageArrWithImageArr:array callback:^(BOOL isSuccess, NSArray *fileNameArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSString *fileTempString = @"";
            if (isSuccess) {
                for (int i = 0; i<fileNameArr.count; i++) {
                    if (i == fileNameArr.count - 1){
                         fileTempString = [fileTempString stringByAppendingString:[NSString stringWithFormat:@"%@",[fileNameArr objectAtIndex:i]]];
                    } else {
                     fileTempString = [fileTempString stringByAppendingString:[NSString stringWithFormat:@"%@,",[fileNameArr objectAtIndex:i]]];
                    }
                }
                [strongSelf publishPraiseWithImageIds:fileTempString];
            }else{
                [strongSelf.view hideProcessingView];
                [strongSelf.view showTips:@"图片上传失败，评价未成功!"];
            }
        }];
    }else{
       //没有图片
        __weak typeof(self)weakSelf = self;
        [weakSelf publishPraiseWithImageIds:@""];
    
    }
   
}

- (void)publishPraiseWithImageIds:(NSString *)fileIds{
        __weak typeof(self)weakSelf = self;
    if ([self.praiseBtn.titleLabel.text isEqualToString:@"追加评价"]) {
        //追加评价
        [[MMHNetworkAdapter sharedAdapter] additionalPraiseWithShopId:weakSelf.singleModel.shopId warehouseId:weakSelf.warehourseId orderNo:weakSelf.singleModel.orderNo itemId:weakSelf.goodsModel.itemId templateId:weakSelf.goodsModel.templateId bufferContentContent:weakSelf.textView.text bufferPics:fileIds from:nil succedHandler:^(NSDictionary *dic) {  
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.view hideProcessingView];
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } failedHandler:^(NSError *error) {
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.view showProcessingView];
            [strongSelf.view showTipsWithError:error];
        }];
    }else{
        //评价商品
        [[MMHNetworkAdapter sharedAdapter] praiseWithShopId:weakSelf.singleModel.shopId orderNo:weakSelf.singleModel.orderNo itemId:weakSelf.goodsModel.itemId templateId:weakSelf.goodsModel.templateId star:weakSelf.startcount content:weakSelf.textView.text pics:fileIds from:nil succedHandler:^(NSDictionary *dic) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.view hideProcessingView];
            if ([strongSelf.praiseBtn.titleLabel.text isEqualToString:@"评价商品"]) {
                [strongSelf.praiseBtn setTitle:@"追加评价" forState:UIControlStateNormal];
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } failedHandler:^(NSError *error) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.view hideProcessingView];
            [strongSelf.view showTipsWithError:error];
        }];
        
    }
}

#pragma mark - UICollectionViewDataSource


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    return self.imageDataSource.count;

}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *reuseView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
    [reuseView addSubview:self.headerView];
    return reuseView;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"collectionViewCell";
    
    MMHGoodsPraiseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    [cell configCellWithImage:self.imageDataSource[indexPath.row]];
    
    if (self.imageDataSource.firstObject != self.iconAdd) {
        
       //最后一行
        if (indexPath.row == self.imageDataSource.count - 1) {
            //最后一行是添加图片
            if (self.imageDataSource.lastObject == self.iconAdd) {
                [cell configIconViewOfBool:YES];  //隐藏
            }else{
              //显示
                [cell configIconViewOfBool:NO];//显示删除按钮
            }
        }else{
            [cell configIconViewOfBool:NO]; //显示
        }

    }else{
        [cell configIconViewOfBool:YES];//隐藏
    }
    cell.delegate = self;
    cell.stringTag = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    return cell;
}
/**
 *  删除图片
 */
- (void)CancelCell:(MMHGoodsPraiseCollectionViewCell *)cell {
    //删除数据
    
    UIImage *image = self.imageDataSource.lastObject;
    if (image != self.iconAdd) {
        [self.imageDataSource addObject:self.iconAdd];
    }
    NSInteger index = [cell.stringTag integerValue];
    [self.imageDataSource removeObjectAtIndex:(index)];
    [self.collectionView reloadData];
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.imageDataSource.count == 9 && (self.imageDataSource.lastObject != self.iconAdd)) {
        
        UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:nil buttonTitles:@[@"取消",@"删除图片"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            //删除数据
            
            if (buttonIndex == 0) {
                UIImage *image = self.imageDataSource.lastObject;
                if (image != self.iconAdd) {
                    [self.imageDataSource addObject:self.iconAdd];
                }
                
                [self.imageDataSource removeObjectAtIndex:indexPath.item];
                [self.collectionView reloadData];
            }
            
            
        }];
        [actionSheet showInView:self.view];
        return;
    }
   if (indexPath.item == (self.imageDataSource.count - 1)) {
    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"相册" buttonTitles:@[@"取消",@"相册",@"拍照"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 0){
            MMHAssetLibraryViewController *assetLibraryViewController = [[MMHAssetLibraryViewController alloc]init];
            assetLibraryViewController.numberOfCouldSelectingAssets = 10 - self.imageDataSource.count;
            __weak typeof(self) weakSelf = self;
            assetLibraryViewController.selectAssetsBlock = ^(NSArray *imageArr,NSArray *assetArr){
                if (!weakSelf) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
              
                if ((10 - strongSelf.imageDataSource.count) >= assetArr.count) {
                    
                    [strongSelf.imageDataSource addObjectsFromArray:imageArr];
                }
                [strongSelf.imageDataSource moveObject:self.iconAdd toIndex:strongSelf.imageDataSource.count - 1];
                NSLog(@"%@", self.imageDataSource);
                
                if (strongSelf.imageDataSource.count == 10) {
                    NSLog(@"最多支持9张图片");
                    [strongSelf.imageDataSource removeLastObject];
                }
                [self.collectionView reloadData];
            };
            UINavigationController *smartNav = [[UINavigationController alloc]initWithRootViewController:assetLibraryViewController];
            [self.navigationController presentViewController:smartNav animated:YES completion:nil];
        } else if (buttonIndex == 1){
            if (![UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
                [[UIAlertView alertViewWithTitle:@"提示" message:@"当前设备不可用 " buttonTitles:@[@"确认"] callback:nil]show];
                return ;
            } else {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                imagePickerController.delegate = self;
                [self presentViewController:imagePickerController animated:YES completion:nil];
            }
        } else {
            
        }
    }];
    [actionSheet showInView:self.view];
   }else{
       UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:nil buttonTitles:@[@"取消",@"删除图片"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
           //删除数据
           
           if (buttonIndex == 0) {
               UIImage *image = self.imageDataSource.lastObject;
               if (image != self.iconAdd) {
                   [self.imageDataSource addObject:self.iconAdd];
               }
               
               [self.imageDataSource removeObjectAtIndex:indexPath.item];
               [self.collectionView reloadData];
           }
          
          
       }];
       [actionSheet showInView:self.view];
       

   }
}


#pragma mark-UIImagePickViewController


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
   
    [self.imageDataSource addObject:image];
    
     [self.imageDataSource moveObject:self.iconAdd toIndex:self.imageDataSource.count - 1];
    if (self.imageDataSource.count == 10) {
        [self.imageDataSource removeLastObject];
    }
    [self.collectionView reloadData];
}


#pragma mark - UITextViewDelegate
/**
 *  控制文字输入的内容和长度
 *
 *  @param textView 参数textView
 *  @param range    文字的range
 *  @param text     输入的文字
 *
 *  @return 是否允许textView的文字改变的BOOL值
 */
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    NSInteger textLength = [textView text].length;
    if (textLength > 140) {
        return NO;
    }
    
   else{
        return YES;
    }
    
   }


- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self.rightButton setTitle:@"完成" forState:UIControlStateNormal];
}
- (void)textViewDidChange:(UITextView *)textView {
 
    if (textView.text.length == 0) {
        
        _placeholder.text = @"评价一下这件宝贝，您的评价对其他妈妈很重要哦";
        
    }else{
    
     _placeholder.text = @"";
        
    }
    if(textView.text.length > 140){
    
        //设置行间距
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = MMHFloat(100);
        NSDictionary *attributes = @{NSFontAttributeName: MMHFontOfSize(14), NSParagraphStyleAttributeName:paragraphStyle};
        //textView.text = [textView.text substringToIndex:140];
        textView.attributedText = [[NSAttributedString alloc] initWithString:textView.text attributes:attributes];
       
    }
}


- (void)textViewDidEndEditing:(UITextView *)textView {
    self.praisecontent = self.textView.text;
    [self.rightButton setTitle:@"编辑" forState:UIControlStateNormal];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
