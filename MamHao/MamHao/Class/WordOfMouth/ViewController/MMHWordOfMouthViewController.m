//
//  MMHWordOfMouthViewController.m
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHWordOfMouthViewController.h"
#import "MMHNetworkAdapter.h"
#import "MMHPublicPraiseModel.h"
#import "MMHPublicPraiseCell.h"
#import "MMHWordOfMouthTableViewCell.h"
#import "MMHPublicPraiseFrameModel.h"
#import "CXStarRatingView.h"
#import "MMHStarRatingView.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHPrasiePeopleCountTableViewCell.h"
#import "MJRefresh.h"
#import "HTHorizontalSelectionList.h"
#import "MamHao-Swift.h"
#import "MMHProductParameterModel.h"
#import "MMHProductDetailViewController1.h"
#import "RefreshControl.h"
#import "MMHGoodsPraiseCollectionViewCell.h"
#import "MMHGoodsParameterstTableViewCell.h"



NSInteger const pageSize = 20;
@interface MMHWordOfMouthViewController ()<UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,RefreshControlDelegate>

@property (nonatomic, strong) UIWebView *imageAndTextDetailView;    //图文详情
@property (nonatomic, strong) UITableView *goodsParameterView;      //商品参数
@property (nonatomic, strong) UITableView *tableView;               //呈现内容的tableView
@property (nonatomic, strong) NSMutableArray *datasource;           //数据源Framemodel的集合
@property (nonatomic, strong) UILabel *lineLable;
@property (nonatomic, strong) HTHorizontalSelectionList *segmentList;
@property (nonatomic, strong) NSArray *segmentItemArray;
@property (nonatomic, strong) NSMutableArray *goodsParameterArray;         //参数名称数组
@property (nonatomic, strong) NSMutableArray *goodsParameterValueArray;    //参数值
@property (nonatomic, strong) CXStarRatingView *starRatingView;     //  星星/
@property (nonatomic, assign) NSInteger count;                      //记录评价的人数
@property (nonatomic, assign) NSInteger page;                       //页码
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) MMHProductParameterModel *productPrarameterModel;
@property (nonatomic) MMHWordOfMouthViewControllerContentType contentType;//口碑类型


@end

@implementation MMHWordOfMouthViewController


#pragma mark - custom getter and setter

- (NSMutableArray*) goodsParameterValueArray {
    if (!_goodsParameterValueArray) {
        self.goodsParameterValueArray = [NSMutableArray array];
    }
    return _goodsParameterValueArray;
}
- (NSMutableArray *) goodsParameterArray {
    if (!_goodsParameterArray) {
      self.goodsParameterArray = [NSMutableArray array];
    }
    return _goodsParameterArray;
}

- (UITableView *)goodsParameterView {
    if (!_goodsParameterView) {
        
        CGRect frame = CGRectMake(0, 40, kScreenWidth, kScreenBounds.size.height - 40-64);
        self.goodsParameterView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
        _goodsParameterView.dataSource = self;
        _goodsParameterView.delegate = self;
        _goodsParameterView.backgroundColor = [UIColor colorWithHexString:@"f6f6f6"];
        // _goodsParameterView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_goodsParameterView];
        [self fetchProductParameters];
    }
    return _goodsParameterView;
}
- (UIWebView *)imageAndTextDetailView {
    if (!_imageAndTextDetailView) {
        
        CGRect frame = CGRectMake(0, 40, kScreenWidth, kScreenBounds.size.height - 40 -64);
        _imageAndTextDetailView = [[UIWebView alloc] initWithFrame:frame];
        _imageAndTextDetailView.backgroundColor = [UIColor colorWithHexString:@"f6f6f6"];
        [self.view addSubview:_imageAndTextDetailView];
        [self fetchImageAndTextDetailInfo];
    }
    return _imageAndTextDetailView;
}
-(UITableView *)tableView{
    if (!_tableView) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, kScreenWidth, kScreenBounds.size.height - 40-64) style:UITableViewStylePlain];
        _tableView.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        __weak typeof(self) weakSelf = self;
        self.page = 1;
        [_tableView addLegendFooterWithRefreshingBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.page++;
            [strongSelf fetchWordOfMouthData];
        }];
        [self fetchWordOfMouthData];
        [_tableView registerClass:[MMHWordOfMouthTableViewCell class] forCellReuseIdentifier:@"MMHPubliicPraise"];
        [_tableView registerClass:[MMHPrasiePeopleCountTableViewCell class] forCellReuseIdentifier:@"Section0Cell"];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}
- (NSMutableArray *)datasource {
    if (!_datasource) {
        self.datasource = [NSMutableArray array];
        
    }
    return _datasource;
}
-(void)removeBlankViewFromView:(UIView *)view{
    UIImageView *blankView = (UIImageView *)[view viewWithStringTag:@"blankView"];
    [blankView removeFromSuperview];
}
#pragma mark - lifeCircle
- (instancetype)initWithContentType:(MMHWordOfMouthViewControllerContentType)contentType
{
    self = [self init];
    if (self) {
        self.contentType = contentType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self pageSetting];
   // [self fetchData];
    [self createSegeList];
    self.segmentList.selectedButtonIndex = self.contentType;
    switch (self.contentType) {
        case MMHWordOfMouthViewControllerContentTypeWordOfMouth:
            [self tableView];
            break;
        case MMHWordOfMouthViewControllerContentTypeProductSpecs:
            [self goodsParameterView];
            break;
        case MMHWordOfMouthViewControllerContentTypeProductDetailHTML:
            [self imageAndTextDetailView];
            break;
        default:
            break;
    }
    if (self.segmentList.delegate && [self.segmentList.delegate respondsToSelector:@selector(selectionList:didSelectButtonWithIndex:)]) {
        [self.segmentList.delegate selectionList:self.segmentList didSelectButtonWithIndex:self.contentType];
    }
}
- (void)createSegeList{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, 0, kScreenWidth, 40);
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    self.segmentList.isWordOfMouth = YES;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    self.segmentItemArray = @[@"商品口碑", @"商品参数", @"图文详情"];
    self.segmentList.isNotScroll = NO;
    [self.view addSubview:self.segmentList];
    
}

- (void)addBlankViewToView:(UIView *)view type:(MMHWordOfMouthViewControllerContentType)type{
    CGRect frame = CGRectMake((kScreenWidth-MMHFloat(105))/2., MMHFloat(148), MMHFloat(105), MMHFloat(105));
    UIImageView *blankView =[[UIImageView alloc]initWithFrame:frame];
    UILabel *tips = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(blankView.frame)+MMHFloat(22), kScreenWidth, MMHFontSize(13.))];
    tips.textAlignment = NSTextAlignmentCenter;
    tips.textColor = [UIColor colorWithHexString:@"9d9d9d"];
    tips.font = MMHFontOfSize(13);
    tips.stringTag = @"tips";
    blankView.stringTag = @"blankView";
    switch (type) {
        case MMHWordOfMouthViewControllerContentTypeWordOfMouth:
            blankView.image = [UIImage imageNamed:@"center_picture_write"];
            tips.text=@"您还没有任何评论哦~";
            break;
        case MMHWordOfMouthViewControllerContentTypeProductSpecs:
            blankView.image = [UIImage imageNamed:@"center_picture_parameter"];
            tips.text=@"暂时没有商品参数哦~";
            break;
        case MMHWordOfMouthViewControllerContentTypeProductDetailHTML:
            blankView.image = [UIImage imageNamed:@"center_picture_photo"];
            tips.text = @"暂时没有商品详情哦~";
            break;
        default:
            break;
    }
    [view addSubview:tips];
    [view addSubview:blankView];

}


- (void)viewWillDisappear:(BOOL)animated {
    [self.tableView removeFooter];
}


- (void)fetchProductParameters{
    __weak typeof(self)weakSelf = self;
    [self.view showProcessingView];
    [[MMHNetworkAdapter sharedAdapter] fetchProductParametersWithTemplateId:weakSelf.goodsTemplateId succeedHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        if (dic.allKeys.count == 0) {
          //  [strongSelf showBlankViewOfType:MMHBlankViewTypeparameter belowView:strongSelf.segmentList];
            [strongSelf addBlankViewToView:strongSelf.goodsParameterView type:MMHWordOfMouthViewControllerContentTypeProductSpecs];
        }
        strongSelf.productPrarameterModel = [[MMHProductParameterModel alloc] initWithJSONDict:dic];
        NSArray *array = @[@"规格参数", @"商品名称", @"品牌名称",@"适用性别", @"型号", @"颜色", @"尺寸"];
        [strongSelf.goodsParameterArray addObjectsFromArray:array];
        [strongSelf.goodsParameterValueArray addObject:@""];
        [strongSelf.goodsParameterValueArray addObject:strongSelf.productPrarameterModel.itemName];
        [strongSelf.goodsParameterValueArray addObject:strongSelf.productPrarameterModel.brandName];
        NSInteger styleApplySex = strongSelf.productPrarameterModel.styleApplySex;
        NSString *str;
        switch (styleApplySex) {
            case 0:
                str = @"通用";
                break;
            case 1:
                str = @"男宝宝";
                break;
            case 2:
                str = @"女宝宝";
                break;
            case 3:
                str = @"宝妈";
                break;
            default:
                str = @"";
                break;
        }
        [strongSelf.goodsParameterValueArray addObject:str];
        if (self.productPrarameterModel.styleDesc) {
            [strongSelf.goodsParameterValueArray addObject:strongSelf.productPrarameterModel.styleDesc];
        }
        [strongSelf.goodsParameterValueArray addObject:strongSelf.productPrarameterModel.colorName];
        [strongSelf.goodsParameterValueArray addObject:strongSelf.productPrarameterModel.sizeName];
        for (NSDictionary *dic in strongSelf.productPrarameterModel.spec) {
            [strongSelf.goodsParameterArray addObject:dic[@"key"]];
            [strongSelf.goodsParameterValueArray addObject:dic[@"value"]];
        }
        [strongSelf.goodsParameterView reloadData];
    } failedHandler:^(NSError *error) {
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
    }];
}

/**
 *  图文详情信息
 */
- (void)fetchImageAndTextDetailInfo{
    
    [self.imageAndTextDetailView showProcessingView];
    __weak typeof(self) weakself = self;
    [[MMHNetworkAdapter sharedAdapter] fetchImageAndTextDetailInfoWithTemplatedId:self.goodsTemplateId succeedHandler:^(NSString *urlStr) {
        if (!weakself) {
            return;
        }
        __strong typeof(weakself)strongSelf = weakself;
        [strongSelf.imageAndTextDetailView hideProcessingView];
        if ([urlStr isEqualToString:@""]) {
           // [strongSelf showBlankViewOfType:MMHBlankViewTypephoto belowView:self.segmentList];
            [strongSelf addBlankViewToView:self.imageAndTextDetailView type:MMHWordOfMouthViewControllerContentTypeProductDetailHTML];
        }else {
            
            NSURL *url = [[NSURL alloc] initWithString:urlStr];
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
            [strongSelf.imageAndTextDetailView loadRequest:request];
        }
    } failedHandler:^(NSError *error) {
        [self.imageAndTextDetailView hideProcessingView];
        [weakself.view showTipsWithError:error];
    }];
}
- (void)fetchWordOfMouthData{
    [self.datasource removeAllObjects];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchTotalCountOfPublicPrasieWithGoodsTemplatedId:weakSelf.goodsTemplateId from:nil suuccedHandler:^(NSMutableArray *array) {
        if (weakSelf) {
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.dataArray = array;
        //评论总人数   
        NSInteger totalCount = 0;
        for (int i = 0; i < 5; i++) {
            NSInteger peopleCountOfScore = [array[i] integerValue];
            totalCount += peopleCountOfScore;
        }
        [strongSelf.tableView reloadData];
    } failedHandler:^(NSError *error) {
    }];
    
    if(self.page == 1) {
        [self.view showProcessingView];
    }
    [[MMHNetworkAdapter sharedAdapter] fetchPublicPrasieWithTemplateId:weakSelf.goodsTemplateId page:weakSelf.page pageSize:pageSize from:nil succeededHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger totalCount = [dic[@"resultCount"] integerValue];
        strongSelf.count = totalCount;
        NSArray *array = dic[@"rows"];
            for (NSDictionary *dic in array) {
            MMHPublicPraiseFrameModel *frameModel = [[MMHPublicPraiseFrameModel alloc] init];
            MMHPublicPraiseModel *praiseModel = [[MMHPublicPraiseModel alloc] initWithJSONDict:dic];
            frameModel.praiseModel = praiseModel;
            [self.datasource addObject:frameModel];
        }
        if (strongSelf.page ==1) {
            //第一次请求数据
            [self.view hideProcessingView];
            if(totalCount == 0){
                [strongSelf addBlankViewToView:strongSelf.tableView type:MMHWordOfMouthViewControllerContentTypeWordOfMouth];
            }else{
              //有数据
                [strongSelf removeBlankViewFromView:strongSelf.tableView];
            }
        }else{
            //加载更多
            if (strongSelf.datasource.count == totalCount) {
                //数据加载完成
            }else{
             //继续
            }
        }

        if (strongSelf.datasource.count == totalCount) {
            [strongSelf.tableView removeFooter];
        }
        [strongSelf.tableView reloadData];
    } failedHandler:^(NSError *error) {
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        strongSelf.count = 0;
        [strongSelf.tableView showTipsWithError:error];
    }];
}

- (void)handleRightBtn:(UIButton *)sender {
    NSLog(@"...");
}

#pragma mark - UISegmentOfDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentItemArray.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentItemArray objectAtIndex:index];
}

#pragma mark - UISegementOfDataDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
   
    switch (index) {
        case 0:{
            [self.view addSubview:self.tableView];
            [self.goodsParameterView removeFromSuperview];
            [self.imageAndTextDetailView removeFromSuperview];
        }
            break;
        case 1:{
            [self.view addSubview:self.goodsParameterView];
            [self.tableView removeFromSuperview];
            [self.imageAndTextDetailView removeFromSuperview];
        }
            break;
        case 2:{
            [self.view addSubview:self.imageAndTextDetailView];
            [self.tableView removeFromSuperview];
            [self.goodsParameterView removeFromSuperview];
        }
            break;
        default:
            break;
    }
}

#pragma mark --Datasource协议方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:self.goodsParameterView]) {
        return 1;
    }
    if (self.count >= 20) {
        return 2;
    }else{
    return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if ([tableView isEqual:self.goodsParameterView]) {
        return self.goodsParameterArray.count;
    }
    if (self.count >= 20 && section == 0) {
        return 1;
    }
    return self.datasource.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([tableView isEqual:self.goodsParameterView]) {
        static NSString *identifier = @"goodsParameter";
        MMHGoodsParameterstTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (!cell) {
            cell = [[MMHGoodsParameterstTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.parametersKey = self.goodsParameterArray[indexPath.row];
        cell.parametersValue = self.goodsParameterValueArray[indexPath.row];

        return cell;
    }else{
        if (self.count >= 20 && indexPath.section == 0) {
            MMHPrasiePeopleCountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Section0Cell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell configCellWithCountOfPraise:self.dataArray];
            return cell;
        }
        MMHWordOfMouthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MMHPubliicPraise" forIndexPath:indexPath];
        //配置cell
        MMHPublicPraiseFrameModel *model = self.datasource[indexPath.row];
        cell.separatorInset = UIEdgeInsetsMake(0, 7, 0, 10);
        cell.frameModel = model;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

#pragma mark -UITableViewDelegate协议方法


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([tableView isEqual:self.goodsParameterView]) {
        return;
    }
//    MMHPublicPraiseFrameModel *model = self.datasource[indexPath.row];
//    MMHProductDetailViewController1 *productDetail = [[MMHProductDetailViewController1 alloc] initWithProduct:model.praiseModel];
//    [self.navigationController pushViewController:productDetail animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
        UIView *view = [[UIView alloc] init];
        view.backgroundColor = [UIColor clearColor];
        return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([tableView isEqual:self.goodsParameterView]) {
        if (indexPath.row == 0) {
            return 40;
        }
        return 50;
    }else{
    if (self.count >= 20 && indexPath.section == 0) {
            return MMHFloat(171);
        }
        MMHPublicPraiseFrameModel *frameModel = self.datasource[indexPath.row];
        return frameModel.cellHeightF;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (_count >= 20 && section == 0){
        return MMHFloat(0);
    } else {
        return 10;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
