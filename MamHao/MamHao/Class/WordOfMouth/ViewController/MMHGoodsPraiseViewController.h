//
//  MMHGoodsPraiseViewController.h
//  MamHao
//
//  Created by 余传兴 on 15/5/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractViewController.h"
#import "MMHSingleProductWithShopCartModel.h"
#import "MMHOrderSingleModel.h"

typedef void(^PraiseProductDone)(MMHOrderSingleModel *orderModel);

@interface MMHGoodsPraiseViewController :AbstractViewController

@property (nonatomic, strong)MMHOrderSingleModel *singleModel; //必须传
@property (nonatomic, strong)MMHSingleProductWithShopCartModel *goodsModel; //必须传
@property (nonatomic, strong)UIButton *praiseBtn; //点击这个按钮push进来的  //必须传
@property (nonatomic, strong)NSString *warehourseId;//**< 仓储id/
@end
