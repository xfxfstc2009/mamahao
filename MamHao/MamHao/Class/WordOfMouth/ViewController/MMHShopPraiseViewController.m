//
//  MMHShopPraiseViewController.m
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopPraiseViewController.h"
#import "MJRefresh.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHWordOfMouthTableViewCell.h"
#import "MMHPrasiePeopleCountTableViewCell.h"
#import "MMHShopPraiseModel.h"
#import "MMHShopPraiseFrameModel.h"
#import "MMHShopPrasieTableViewCell.h"
#import "MamHao-Swift.h"

@interface MMHShopPraiseViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UITableView *praiseTableView;//呈现内容的tableView
@property (nonatomic, strong) NSMutableArray *datasource;
@property (nonatomic, assign) NSInteger count;//评价的总人数
@property (nonatomic, assign) NSInteger page;//页
@property (nonatomic, assign) NSInteger requestCount;//请求的条数
@property (nonatomic, strong)NSMutableArray *dataArray;
@end

@implementation MMHShopPraiseViewController
#pragma mark -- custom getter
-(NSMutableArray *)dataArray {
    if (!_dataArray) {
        self.dataArray = [NSMutableArray array];
    }
    return _dataArray;
}
- (NSMutableArray *)datasource {
    if (!_datasource) {
        self.datasource = [NSMutableArray array];
        
    }
    return _datasource;
}
-(UITableView *)praiseTableView{
    if (!_praiseTableView) {
        self.praiseTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) style:UITableViewStylePlain];
        _praiseTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        _praiseTableView.backgroundColor = [MMHAppearance backgroundColor];
        _praiseTableView.delegate = self;
        _praiseTableView.dataSource = self;
        __weak typeof(self)weakSelf = self;
        _praiseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_praiseTableView registerClass:[MMHShopPrasieTableViewCell class] forCellReuseIdentifier:@"MMHPubliicPraise"];
        [_praiseTableView registerClass:[MMHPrasiePeopleCountTableViewCell class] forCellReuseIdentifier:@"Section0Cell"];
        [self.view addSubview:_praiseTableView];
        __strong typeof (weakSelf) strongself = weakSelf;
        [strongself fetchData];
        [self.view addSubview:_praiseTableView];
    }
    return _praiseTableView;
}
- (void)fetchData{
    [self.praiseTableView showProcessingView];
    [self.datasource removeAllObjects];
    [self.dataArray removeAllObjects];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchShopPraiseWithShopId:self.shopId succeedHandler:^(NSDictionary *dic) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.praiseTableView hideProcessingView];
        MMHShopPraiseModel *model = [[MMHShopPraiseModel alloc] initWithJSONDict:dic];
        strongSelf.barMainTitle = model.shopName;
        strongSelf.count = model.score_1+model.score_2+model.score_3+model.score_4+model.score_5;
        if (strongSelf.count>21) {
            [strongSelf.dataArray addObjectsFromArray:@[@(model.score_5), @(model.score_4), @(model.score_3), @(model.score_2), @(model.score_1)]];
            [strongSelf.datasource addObject:weakSelf.dataArray];
        }else{
        
            NSLog(@"口碑统计少于20人， 不显示口碑统计");
        }
        if (model.evaluationDetail.count==0) {
            [strongSelf showBlankViewOfType: MMHBlankViewTypeComment belowView:nil];
        }
        for (MMHShopEvaluationDetailModel *detailModel in model.evaluationDetail) {
            MMHShopPraiseFrameModel *frameModel = [[MMHShopPraiseFrameModel alloc] init];
            frameModel.model = detailModel;
            [weakSelf.datasource addObject:frameModel];
        }
        [strongSelf.praiseTableView reloadData];
    } failedHandler:^(NSError *error) {
        [self.praiseTableView hideProcessingView];
        [self.praiseTableView showTipsWithError:error];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self praiseTableView];
    // Do any additional setup after loading the view.
}

#pragma mark --Datasource协议方法

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.count<=20) {
    
    }
    return self.datasource.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.count>20) {
        if (indexPath.row == 0) {
            MMHPrasiePeopleCountTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Section0Cell" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.praiseType = ShopPraiseType;
            [cell configCellWithCountOfPraise:self.datasource[0]];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell addSeparatorLineWithType:SeparatorTypeBottom];
            return cell;
        }

    }
       MMHShopPrasieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MMHPubliicPraise" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.datasource[indexPath.row];
    [cell addSeparatorLineWithType:SeparatorTypeBottom];
    return cell;
}

#pragma mark -UITableViewDelegate协议方法

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
//    
//    
//    return 15;
//    
//}
//
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView *view = [[UIView alloc] init];
//    view.backgroundColor = [UIColor clearColor];
//    return view;
//    
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (self.datasource.count == 0) {
        return 0;
    }
    if (self.count>21) {
        if (indexPath.row == 0) {
            return MMHFloat(171);
        }
    }
    
    MMHPublicPraiseFrameModel *frameModel = self.datasource[indexPath.row];
    
    return frameModel.cellHeightF;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (_count >= 20 && section == 0){
//        return MMHFloat(0);
//    } else {
//        return 10;
//    }
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
//    UIView *footerView = [[UIView alloc]init];
//    footerView.backgroundColor = [UIColor clearColor];
//    return footerView;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
