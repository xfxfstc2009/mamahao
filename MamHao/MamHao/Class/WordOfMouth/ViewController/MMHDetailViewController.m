//
//  MMHDetailViewController.m
//  MamHao
//
//  Created by fishycx on 15/7/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHDetailViewController.h"

@interface MMHDetailViewController ()
@property (nonatomic, strong)UIImageView *imageView;
@end

@implementation MMHDetailViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.imageView.image = self.image;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
