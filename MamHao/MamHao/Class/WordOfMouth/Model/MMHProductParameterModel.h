//
//  MMHProductParameterModel.h
//  MamHao
//
//  Created by fishycx on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHProductParameterModel : MMHFetchModel
@property (nonatomic,copy)NSString *itemName;
@property (nonatomic,copy)NSString *brandName;
@property (nonatomic,copy)NSString *styleApplyAge;
@property (nonatomic,copy)NSString *styleDesc;
@property (nonatomic,copy)NSString *colorName;
@property (nonatomic,copy)NSString *sizeName;
@property (nonatomic,strong)NSArray *spec;
@property (nonatomic,assign)NSInteger styleApplySex;
@end
