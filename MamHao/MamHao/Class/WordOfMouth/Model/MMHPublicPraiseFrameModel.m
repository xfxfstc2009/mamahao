//
//  MMHPublicPraiseFrameModel.m
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPublicPraiseFrameModel.h"
#import "MMHPublicPraiseModel.h"
#import "MMHAssistant.h"

@implementation MMHPublicPraiseFrameModel

/**
 *  计算文字的尺寸
 *
 *  @param text    需要计算的文字
 *  @param font    文字的字体
 *  @param maxSize 文字的最大尺寸
 *
 *  @return 文字的最大尺寸
 */
- (CGSize)sizeWithText:(NSString *)text font:(UIFont *)font maxSize:(CGSize)maxSize{
    NSDictionary *attrs = @{NSFontAttributeName :font};
    return [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attrs context:nil].size;
}

- (void)setPraiseModel:(MMHPublicPraiseModel *)praiseModel{

    _praiseModel = praiseModel;
    
    //子控件间的距离
    CGFloat padding = MMHFloat(10);
    
    //头像
    CGFloat iconX = padding;
    CGFloat iconY = MMHFloat(20);
    CGFloat iconW = MMHFloat(40);
    CGFloat iconH = MMHFloat(40);
    _iconImageViewF = CGRectMake(iconX, iconY, iconW, iconH);
    
    //用户昵称
    CGSize nameSize = [self sizeWithText:self.praiseModel.name font:MMHFontOfSize(15) maxSize:MMHSizeMake(MAXFLOAT, MAXFLOAT)];
    CGFloat userNickNameX = CGRectGetMaxX(_iconImageViewF) + padding;

    CGFloat userNickNameY = CGRectGetMinY(_iconImageViewF) + MMHFloat(3.5);
    CGFloat userNickNameW = nameSize.width;
    CGFloat userNickNameH = nameSize.height;
    
    _userNickLabelF = CGRectMake(userNickNameX, userNickNameY, userNickNameW, userNickNameH);

    //宝宝年龄
    CGSize  ageSize = [self sizeWithText:self.praiseModel.babyAge font:MMHFontOfSize(12.) maxSize:MMHSizeMake(MAXFLOAT, MAXFLOAT)];
    CGFloat ageStringX = CGRectGetMaxX(_iconImageViewF) + padding;
    CGFloat ageStringY = CGRectGetMaxY(_userNickLabelF) + MMHFloat(6);
    CGFloat ageStringW = ageSize.width;
    CGFloat ageStringH = ageSize.height;
    
    _ageLableF =  CGRectMake(ageStringX, ageStringY, ageStringW, ageStringH);
    
    //星星，imageView
    _starBackGroundViewF = MMHRectMake(375 -85, 25, 75, 15);
    //发布时间
    CGSize dateTimeSize = [self sizeWithText:self.praiseModel.commentTime font:MMHFontOfSize(12) maxSize:MMHSizeMake(200, 20)];
    CGFloat dateTimeX = kScreenWidth - dateTimeSize.width - MMHFloat(10);
    CGFloat dateTimeY = CGRectGetMaxY(_starBackGroundViewF) + MMHFloat(6);
    CGFloat dateTimeW = dateTimeSize.width;
    CGFloat dateTimeH = dateTimeSize.height;
   
    _dataTimeLabelF = CGRectMake(dateTimeX, dateTimeY, dateTimeW, dateTimeH);
    
    
    //显示已经购买商品

    if (self.praiseModel.isBuy) {
        CGSize bougthStateSize = [self sizeWithText:@"已经购买该商品" font:MMHFontOfSize(12) maxSize: MMHSizeMake(200, 24)];
        CGFloat boughtStateX = padding;
        CGFloat boughtStateY = CGRectGetMaxY(_iconImageViewF) + MMHFloat(15);
        CGFloat boughtStateW = bougthStateSize.width;
        CGFloat boughtStateH = bougthStateSize.height;
        
        _boughtGoodsStateF = CGRectMake(boughtStateX, boughtStateY, boughtStateW, boughtStateH);
    }else{
        CGSize bougthStateSize = [self sizeWithText:@"已经购买该商品" font:MMHFontOfSize(12) maxSize: MMHSizeMake(200, 24)];
        CGFloat boughtStateX = padding;
        CGFloat boughtStateY = CGRectGetMaxY(_iconImageViewF);
        CGFloat boughtStateW = bougthStateSize.width;
        CGFloat boughtStateH = 0;
       _boughtGoodsStateF = CGRectMake(boughtStateX, boughtStateY, boughtStateW, boughtStateH);
    }
    
    //评价内容--文字
    CGSize evvSize = [self sizeWithText:self.praiseModel.commentContent font:MMHFontOfSize(14.) maxSize:CGSizeMake(kScreenWidth -MMHFloat(20), MAXFLOAT)];
    CGFloat evvSizeX = padding;
    CGFloat evvSizeY = CGRectGetMaxY(_boughtGoodsStateF)+MMHFloat(10);
    CGFloat evvSizeW = evvSize.width;
    CGFloat evvSizeH = evvSize.height;
    if (self.praiseModel.commentContent) {
        _evvluationLableF = CGRectMake(evvSizeX, evvSizeY, evvSizeW, evvSizeH);
    }else{
        _evvluationLableF = CGRectMake(evvSizeX, evvSizeY-MMHFloat(10), evvSizeW, 0);
    }
    
    
    //判断是否存在图片
    
    CGFloat evvImageX = padding;
    CGFloat evvImageY = CGRectGetMaxY(_evvluationLableF);
    CGFloat evvImageW = kScreenWidth - MMHFloat(20);
    CGFloat evvImageH = MMHFloat(90);
    
    if (self.praiseModel.pics.count) {
        _evvImageViewF = CGRectMake(evvImageX, evvImageY+padding, evvImageW, evvImageH);
    }else{
        _evvImageViewF = CGRectMake(evvImageX, evvImageY, evvImageW, 0);
    }
    
    //门店回复
    CGFloat shopReplyX = padding;
    CGFloat shopReplyY = CGRectGetMaxY(_evvImageViewF);
    
    CGSize  shopReplySize = [self sizeWithText:[NSString stringWithFormat:@"[门店回复]:%@", self.praiseModel.shopReplyContent] font:MMHF2 maxSize:CGSizeMake(kScreenWidth - MMHFloat(20), CGFLOAT_MAX)];
    if (self.praiseModel.shopReplyContent.length) {
        _shopReplyContentF =  CGRectMake(shopReplyX, shopReplyY+MMHFloat(10), shopReplySize.width, shopReplySize.height);
    }else{
        _shopReplyContentF = CGRectMake(shopReplyX, shopReplyY, 0, 0);
    }
    
    //追加评价
    CGFloat bufferCommentX = padding;
    CGFloat bufferCommentY = CGRectGetMaxY(_shopReplyContentF);
    CGSize bufferCommentSize = [self sizeWithText:[NSString stringWithFormat:@"[%@追评]:%@",self.praiseModel.bufferCommentTime, self.praiseModel.bufferCommentContent] font:MMHF2 maxSize:CGSizeMake(kScreenWidth-MMHFloat(20), CGFLOAT_MAX)];
    if (self.praiseModel.bufferCommentContent.length) {
        _bufferCommentContentF = CGRectMake(bufferCommentX, bufferCommentY+MMHFloat(20), bufferCommentSize.width, bufferCommentSize.height);
    }else{
        _bufferCommentContentF = CGRectMake(bufferCommentX, bufferCommentY, 0, 0);
    }
    //追加评价图片
    CGFloat bufferPicsX = padding;
    CGFloat bufferPicsY = CGRectGetMaxY(_bufferCommentContentF);
    CGFloat bufferPicsW = kScreenWidth - MMHFloat(20);
    CGFloat bufferPicsH = MMHFloat(90);
    if (self.praiseModel.bufferPics.count) {
        _bufferPicsF = CGRectMake(bufferPicsX, bufferPicsY+MMHFloat(10), bufferPicsW, bufferPicsH);
        
    }else{
        _bufferPicsF = CGRectMake(bufferPicsX, bufferPicsY, 0, 0);
    }
    
    _cellHeightF = CGRectGetMaxY(_bufferPicsF)+MMHFloat(20);
}
@end
