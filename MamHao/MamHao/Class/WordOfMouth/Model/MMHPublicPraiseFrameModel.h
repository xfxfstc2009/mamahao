//
//  MMHPublicPraiseFrameModel.h
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MMHPublicPraiseModel;
@interface MMHPublicPraiseFrameModel : NSObject

@property (nonatomic, assign, readonly)CGRect iconImageViewF;

@property (nonatomic, assign)CGRect userNickLabelF;

@property (nonatomic, assign)CGRect ageLableF;

@property (nonatomic, assign)CGRect dataTimeLabelF;

@property (nonatomic, assign)CGRect starBackGroundViewF;

@property (nonatomic, assign)CGRect boughtGoodsStateF;

@property (nonatomic, assign)CGRect evvluationLableF;

@property (nonatomic, assign)CGRect evvImageViewF;

@property (nonatomic, assign)CGRect shopReplyContentF;

@property (nonatomic, assign)CGRect bufferCommentContentF;

@property (nonatomic, assign)CGRect bufferPicsF;

@property (nonatomic, strong)MMHPublicPraiseModel *praiseModel;

/**
 *  cell的高度
 */
@property (nonatomic, assign, readonly)CGFloat cellHeightF;
  

@end
