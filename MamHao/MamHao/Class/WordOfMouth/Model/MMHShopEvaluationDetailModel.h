//
//  MMHShopEvaluationDetailModel.h
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
@class MMHShopEvaluationDetailModel;

@protocol MMHShopEvaluationDetailModel <NSObject>
@end

@interface MMHShopEvaluationDetailModel : MMHFetchModel
@property (nonatomic, strong)NSString *bufferTime;
@property (nonatomic, strong)NSString *content;
@property (nonatomic, strong)NSString *babyBirthday;
@property (nonatomic, strong)NSString *memberNickname;
@property (nonatomic, strong)NSString *createTime;
@property (nonatomic, assign)NSInteger shopId;
@property (nonatomic, strong)NSString *bufferPic;
@property (nonatomic, assign)NSInteger memberId;
@property (nonatomic, strong)NSString *bufferContent;
@property (nonatomic, assign)NSInteger serviceRating;
@property (nonatomic, strong)NSString *memberHeadPic;
@property (nonatomic, strong)NSString *pictures;
@property (nonatomic, strong)NSString *city;
@end
