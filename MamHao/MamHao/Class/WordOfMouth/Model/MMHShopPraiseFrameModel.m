//
//  MMHShopPraiseFrameModel.m
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopPraiseFrameModel.h"

@implementation MMHShopPraiseFrameModel
- (void)setModel:(MMHShopEvaluationDetailModel *)model {
    _model = model;
    //头像
    CGFloat memberHeadPicX = MMHFloat(13);
    CGFloat memberHeadPicY = 20;
    CGFloat memberHeadPicW = MMHFloat(40);
    CGFloat memberHeadPicH = MMHFloat(40);
    self.memberHeadPicF = CGRectMake(memberHeadPicX, memberHeadPicY, memberHeadPicW, memberHeadPicH);
    //昵称
    CGFloat memberNicknameX = CGRectGetMaxX(self.memberHeadPicF)+MMHFloat(10);
    CGFloat memberNicknameY = 27;
    CGSize memberNicknameSize = [model.memberNickname sizeWithCalcFont:MMHF2 constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.memberNicknameF = CGRectMake(memberNicknameX, memberNicknameY, memberNicknameSize.width, memberNicknameSize.height);
    //宝贝年龄
    CGFloat babyBirthDayX = memberNicknameX;
    CGFloat babyBirthDayY = CGRectGetMaxY(self.memberNicknameF)+5;
    CGSize babyNicknameSize = [model.babyBirthday sizeWithCalcFont:MMHF0 constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.babyBirthDayF = CGRectMake(babyBirthDayX, babyBirthDayY, babyNicknameSize.width, babyNicknameSize.height);
    //星级
    CGFloat serviceRatingX = MMHFloat(10)+11*5;
    CGFloat serviceRatingY = 27;
    self.serviceRatingBackViewF = CGRectMake(kScreenWidth-serviceRatingX, serviceRatingY, 55, MMHFloat(11.5f));
    //发布时间
   
    CGFloat commentTimeY = CGRectGetMaxY(self.serviceRatingBackViewF)+5;
    CGSize commentTimeSize = [model.createTime sizeWithCalcFont:MMHF0 constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.commentTimeF = CGRectMake(kScreenWidth - MMHFloat(10) - commentTimeSize.width, commentTimeY, commentTimeSize.width, commentTimeSize.height);
    //地址
    CGFloat addressY = CGRectGetMaxY(self.serviceRatingBackViewF)+5;
    CGSize addressSize = [model.city sizeWithCalcFont:MMHF0 constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGFloat addressX = CGRectGetMinX(self.commentTimeF)-MMHFloat(9)-addressSize.width;
    self.addressF = CGRectMake(addressX, addressY, addressSize.width, addressSize.height);
    //内容
    CGFloat contentX = memberNicknameX;
    CGFloat contentY = CGRectGetMaxY(self.babyBirthDayF)+18;
    CGSize contentSize = [model.content sizeWithCalcFont:MMHF4 constrainedToSize:CGSizeMake(kScreenWidth - memberNicknameX - MMHFloat(10), CGFLOAT_MAX)];
    self.contentF = CGRectMake(contentX, contentY, contentSize.width, contentSize.height);
    
    CGFloat goodsBackViewY = CGRectGetMaxY(self.contentF)+15;
    NSArray *pictures = [model.pictures componentsSeparatedByString:@","];
    CGFloat goodsBackViewH;
    NSInteger picturesCount = pictures.count;
    NSInteger remainder = !picturesCount%4?0:1;
    NSInteger colloum = picturesCount/4+remainder;
    CGFloat  startY = (60 - MMHFloat(30));
    if (pictures.count) {
        
        goodsBackViewH =  startY + (5+MMHFloat(30))*colloum;
    }else{
        goodsBackViewH = 60;
    }
    self.goodsBackViewF = CGRectMake(0, goodsBackViewY, kScreenWidth, goodsBackViewH);
    //购买提示label
    CGSize buyTipsLabelSize = [@"在本店购买的商品" sizeWithCalcFont:MMHF2 constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.buyTipsLabelF = CGRectMake(MMHFloat(63), (60-buyTipsLabelSize.height)/2, buyTipsLabelSize.width, buyTipsLabelSize.height);
    //已经够买的商品图片
    //CGFloat pictureBackViewX = CGRectGetMaxX(self.buyTipsLabelF)+MMHFloat(15);
    //cell的高度
    self.cellHeightF = CGRectGetMaxY(self.goodsBackViewF);
    
}
@end
