//
//  MMHShopPraiseModel.h
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHShopEvaluationDetailModel.h"
@interface MMHShopPraiseModel : MMHFetchModel
@property(nonatomic, copy)NSString  *shopId;
@property(nonatomic, copy)NSString *shopName;
@property(nonatomic, copy)NSString *serviceRating;
@property(nonatomic, assign)NSInteger score_1;
@property(nonatomic, assign)NSInteger score_2;
@property(nonatomic, assign)NSInteger score_3;
@property(nonatomic, assign)NSInteger score_4;
@property(nonatomic, assign)NSInteger score_5;
@property(nonatomic, strong)NSArray<MMHShopEvaluationDetailModel>* evaluationDetail;
@end
