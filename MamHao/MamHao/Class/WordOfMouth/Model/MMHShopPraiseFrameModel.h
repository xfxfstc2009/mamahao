//
//  MMHShopPraiseFrameModel.h
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHShopEvaluationDetailModel.h"
@interface MMHShopPraiseFrameModel : NSObject
@property (nonatomic, strong)MMHShopEvaluationDetailModel *model;
@property (nonatomic, assign)CGRect memberHeadPicF;//头像
@property (nonatomic, assign)CGRect memberNicknameF;//昵称
@property (nonatomic, assign)CGRect babyBirthDayF;//宝贝生日
@property (nonatomic, assign)CGRect serviceRatingBackViewF;//星星
@property (nonatomic, assign)CGRect addressF; //地点
@property (nonatomic, assign)CGRect commentTimeF;//评价时间
@property (nonatomic, assign)CGRect contentF; //内容
/**
 *  已经够买的商品的backView
 */
@property (nonatomic, assign)CGRect goodsBackViewF;
//@property (nonatomic, assign)CGRect pictureBackViewF; //商品的图片
@property (nonatomic, assign)CGRect buyTipsLabelF;//已经够买商品的提示按钮
@property (nonatomic, assign)CGFloat cellHeightF;
@end
