//
//  MMHStarRatingView.m
//  MamHao
//
//  Created by 余传兴 on 15/5/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHStarRatingView.h"

@interface MMHStarRatingView ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIView *foreView;

@end

@implementation MMHStarRatingView


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backView = [self buildStarViewWithColor:[UIColor colorWithHexString:@"eeeeee"]];
        self.foreView = [self buildForeViewWithColor:[UIColor colorWithCustomerName:@"蓝"]];
    }
    return self;
}

- (UIView *)buildStarViewWithColor:(UIColor *)color{

    CGRect frame = self.bounds;
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.layer.cornerRadius = MMHFloat(3);
    view.backgroundColor = color;
    view.clipsToBounds = YES;
    [self addSubview:view];
    return view;
}

-(UIView *)buildForeViewWithColor:(UIColor *)color{
    //CGRect frame = self.bounds;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.backView.width, self.backView.height)];
    //view.layer.cornerRadius = MMHFloat(6);
    view.backgroundColor = color;
   // view.clipsToBounds = YES;
    [self.backView addSubview:view];
    return view;

}
- (void)changeForeViewWithRate:(CGFloat)rate {
    CGFloat width = rate * self.bounds.size.width;
    CGRect frame = self.foreView.frame;
    frame.size.width = width;
    self.foreView.frame = frame;
}
@end
