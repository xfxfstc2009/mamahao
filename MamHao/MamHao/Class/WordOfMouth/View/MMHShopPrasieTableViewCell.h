//
//  MMHShopPrasieTableViewCell.h
//  MamHao
//
//  Created by fishycx on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//  店铺评价cell

#import <UIKit/UIKit.h>
#import "MMHShopPraiseFrameModel.h"
@interface MMHShopPrasieTableViewCell : UITableViewCell
@property (nonatomic, strong)MMHShopPraiseFrameModel *model;
@end
