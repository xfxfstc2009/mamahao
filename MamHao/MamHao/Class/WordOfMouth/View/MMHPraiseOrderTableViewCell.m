//
//  MMHPraiseOrderTableViewCell.m
//  MamHao
//
//  Created by 余传兴 on 15/5/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPraiseOrderTableViewCell.h"
#import "AMRatingControl.h"
#import "MMHNetworkAdapter+WordOfMouth.h"

@interface MMHPraiseOrderTableViewCell ()<UITextViewDelegate>

@property (nonatomic, strong)AMRatingControl *control1; //服务评价

@property (nonatomic, strong)AMRatingControl *control2; //发货评价

@property (nonatomic, strong)UIButton *button;//按钮

@property (nonatomic, strong)UILabel *serviceLabel;//服务评价label
@property (nonatomic, strong)UIImageView *backImageView;//背景图片

// inputView
@property (nonatomic,strong)UITextView *inputExplainTextView;           /**< 输入框*/
@property (nonatomic,strong)UILabel *labelplaceHorder;                  /**< 输入框placeHolder*/
@property (nonatomic,strong)UILabel *textLimitLabel;                    /**< 文字长度*/

// imageBackgroundView
@property (nonatomic,strong)UIView *imageBackgroundView;                /**< 背景图片*/
@property (nonatomic,strong)NSMutableArray *photoImageMutableArr;       /**< 照片数组*/


@end
@implementation MMHPraiseOrderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *label1 = [[UILabel alloc] init];
        label1.text = @"服务评价:";
        label1.textColor = C5;
        label1.font = MMHF5;
        CGSize size = [label1.text sizeWithCalcFont:label1.font constrainedToSize:MMHSizeMake(MAXFLOAT, MAXFLOAT)];
        label1.frame = CGRectMake(MMHFloat(10), MMHFloat(20), size.width, size.height);
        AMRatingControl *control = [[AMRatingControl alloc] initWithLocation:CGPointMake(CGRectGetMaxX(label1.frame)+MMHFloat(15), MMHFloat(20)) emptyImage:[UIImage imageNamed:@"score_icon_bigstar_us"] solidImage:[UIImage imageNamed:@"score_icon_bigstar_s"] andMaxRating:5];
        self.control1 = control;
        [control setRating:5];
        [self.contentView  addSubview:label1];
        [self.contentView  addSubview:control];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(10),MMHFloat(20) + CGRectGetMaxY(label1.frame), size.width, size.height)];
        self.serviceLabel = label2;
        label2.text = @"发货速度:";
        label2.font = MMHF5;
        label2.textColor = C5;
        
        AMRatingControl *control2 = [[AMRatingControl alloc] initWithLocation:CGPointMake(CGRectGetMaxX(label1.frame)+MMHFloat(15),CGRectGetMinY(label2.frame)) emptyImage:[UIImage imageNamed:@"score_icon_bigstar_us"] solidImage:[UIImage imageNamed:@"score_icon_bigstar_s"] andMaxRating:5];
        self.control2 = control2;
        [control2 setRating:5];
        [self.contentView  addSubview:label2];
        [self.contentView addSubview:control2];
        
        
        UIImageView *backgroundImageView = [[UIImageView alloc]init];
        self.backImageView = backgroundImageView;
        backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
        backgroundImageView.frame =CGRectMake(MMHFloat(10), CGRectGetMaxY(label2.frame)+MMHFloat(20), kScreenWidth- 2 * MMHFloat(10) , MMHFloat(75));
        backgroundImageView.userInteractionEnabled = NO;
        [self.contentView addSubview:backgroundImageView];
        
        // 创建门店评价输入框
        self.inputExplainTextView = [[UITextView alloc]initWithFrame:CGRectMake(MMHFloat(10), CGRectGetMaxY(label2.frame)+MMHFloat(20), kScreenWidth- 2 * MMHFloat(10) , MMHFloat(75))];
                                                                                
        self.inputExplainTextView.delegate = self;
        self.inputExplainTextView.textColor = [UIColor blackColor];
        self.inputExplainTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        self.inputExplainTextView.returnKeyType = UIReturnKeyDefault;
        [self.inputExplainTextView setKeyboardType:UIKeyboardTypeDefault];
        self.inputExplainTextView.backgroundColor = [UIColor clearColor];
        self.inputExplainTextView.scrollEnabled = YES;
        
        // 创建placeHolder
        self.labelplaceHorder = [[UILabel alloc]init];
        self.labelplaceHorder.backgroundColor = [UIColor clearColor];
        self.labelplaceHorder.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        self.labelplaceHorder.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
        self.labelplaceHorder.numberOfLines = 0;
        self.labelplaceHorder.text = @"对门铺进行评价，帮助我们为您提供更好的服务！";
        CGSize labelPlaceHolderSize = [self.labelplaceHorder.text sizeWithCalcFont:self.labelplaceHorder.font constrainedToSize:CGSizeMake(kScreenWidth - 4 * MMHFloat(10), CGFLOAT_MAX)];
        self.labelplaceHorder.frame = CGRectMake(MMHFloat(10)*2,CGRectGetMinY(self.inputExplainTextView.frame)+ MMHFloat(10),kScreenBounds.size.width - 4 * MMHFloat(10),labelPlaceHolderSize.height);
        [self.contentView addSubview:self.inputExplainTextView];
        [self.contentView addSubview:self.labelplaceHorder];
        
//        // 增加字数限制
//        self.textLimitLabel = [[UILabel alloc]init];
//        self.textLimitLabel.frame = CGRectMake(CGRectGetMaxX(self.inputExplainTextView.frame) - 50,CGRectGetMaxY(self.inputExplainTextView.frame) + MMHFloat(3), 50, 20);
//        self.textLimitLabel.textColor = [UIColor blackColor];
//        self.textLimitLabel.font=[UIFont systemFontOfSize:13.];
//        self.textLimitLabel.textColor = [UIColor lightGrayColor];
//        self.textLimitLabel.textAlignment = NSTextAlignmentRight;
//        [self.textLimitLabel setText:[NSString stringWithFormat:@"%lu/140", (unsigned long)];
//        self.textLimitLabel.backgroundColor = [UIColor clearColor];
//        [self.contentView addSubview:self.textLimitLabel];
        [MMHTool calculateCharacterLengthForAres:self.inputExplainTextView.text];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button = button;
        button.titleLabel.font = MMHF4;
        button.layer.cornerRadius = MMHFloat(6.);
        button.layer.borderWidth = 0.5;
        button.layer.masksToBounds = YES;
        button.layer.borderColor =  [UIColor colorWithHexString:@"dddddd"].CGColor;
        [button setTitleColor:[UIColor  blackColor] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(handleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.button.frame = CGRectMake(kScreenBounds.size.width - (MMHFloat(85) + MMHFloat(11)),CGRectGetMaxY(self.inputExplainTextView.frame)+MMHFloat(10), MMHFloat(85), MMHFloat(30));
        [self.contentView addSubview:button];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderPraiseContentEditDone:) name:KOrderPraiseContentEditDone object:nil];
    }
    return self;
}


- (void)orderPraiseContentEditDone:(NSNotification *)notification{

    [self.inputExplainTextView resignFirstResponder];
}
- (void)setStarControlRatingWithStarCount:(NSInteger)count StarControl:(NSInteger)StarControlIndex{
    if (count == 0) {
        count = 5;
    }
    if (StarControlIndex == 0) {
        self.control1.rating = count;
    }else{
        self.control2.rating = count;
    }
}

- (void)setButtonStateWithBOOl:(BOOL)isComment {
    NSString *buttonText;
    if (isComment) {
        buttonText = @"已完成";
        [self.button setBackgroundColor:[UIColor hexChangeFloat:@"dcdcdc"]];
        [self setEditing:NO animated:YES];
        
        self.backImageView.frame = CGRectZero;
        
        self.inputExplainTextView.frame =  CGRectZero;
        
        self.labelplaceHorder.frame = CGRectZero;

        CGSize buttonContentSize = [@"发表评价" sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        self.button.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11)- (buttonContentSize.width + 2 * MMHFloat(10)),CGRectGetMaxY(self.serviceLabel.frame)-MMHFloat(30), buttonContentSize.width + 2 *MMHFloat(10), MMHFloat(30));
        
    }else{
       buttonText = @"发表评价";
    }
    [self.button setTitle:buttonText forState:UIControlStateNormal];
      }
- (void)handleBtnClick:(UIButton *)sender{
    
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(cell1praiseOrderWithOrderNo:serveStar:deliverySpeedStar:commentContent:)]) {
        [self.cellDelegate cell1praiseOrderWithOrderNo:self.orderId serveStar:self.control1.rating deliverySpeedStar:self.control2.rating commentContent:self.inputExplainTextView.text];
    }
}

-(void)textViewDidChange:(UITextView *)textView {
    if (self.inputExplainTextView.text.length == 0) {
        self.labelplaceHorder.text = @"对门铺进行评价，帮助我们为您提供更好的服务！";
    }else{
        self.labelplaceHorder.text = @"";
    }
   // [self.textLimitLabel setText:[NSString stringWithFormat:@"%lu/140",(unsigned long)[MMHTool calculateCharacterLengthForAres:self.inputExplainTextView.text]]];
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
