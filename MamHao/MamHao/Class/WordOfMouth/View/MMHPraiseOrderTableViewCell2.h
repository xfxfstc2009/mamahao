//
//  MMHPraiseOrderTableViewCell2.h
//  MamHao
//
//  Created by 余传兴 on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  不是快递送货
 */



@protocol MMHPraiseOrderTableViewCell2Delegate<NSObject>

- (void)textViewDidEndEidt:(UITextView *)textView;
- (void)textViewBeginEdit:(UITextView *)textView;
- (void)praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger)star deliverySpeedStar: (NSInteger)deliverSpeedStar commentContent:(NSString *)comment;
@end
#define KOrderPraiseContentEditDone  @"orderPraiseContentEditDone"

@interface MMHPraiseOrderTableViewCell2 : UITableViewCell
@property (nonatomic, strong)UIBarButtonItem *rightBarItem;
@property (nonatomic, strong)NSString *orderId; //评价订单接口参数2 订单id
@property (nonatomic, weak)id<MMHPraiseOrderTableViewCell2Delegate>cellDelegate;
- (void)setStarControlRatingWithStarCount:(NSInteger)count StarControl:(NSInteger)StarControlIndex;
- (void)setButtonStateWithBOOl:(BOOL)isCommented;

@end
