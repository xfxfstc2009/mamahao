//
//  MMHStarRatingView.h
//  MamHao
//
//  Created by 余传兴 on 15/5/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHStarRatingView : UIView


- (void)changeForeViewWithRate:(CGFloat)rate;

@end
