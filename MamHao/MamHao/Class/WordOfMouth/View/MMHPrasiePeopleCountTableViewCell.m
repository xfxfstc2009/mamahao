//
//  MMHPrasiePeopleCoutCollectionViewCell.m
//  MamHao
//
//  Created by 余传兴 on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPrasiePeopleCountTableViewCell.h"
#import "MMHStarRatingView.h"
#import "MMHRatingView.h"

@interface MMHPrasiePeopleCountTableViewCell ()

/**
 *  平均分
 */
@property (nonatomic, strong)UILabel *averageLabel;

@property (nonatomic, strong)MMHRatingView *ratingView;
/**
 *  进度条
 */
@property (nonatomic, strong)NSMutableArray *progressViewArray;

/**
 *  people count
 */

@property (nonatomic, strong)NSMutableArray *peopleCountLabelArray;


@end

@implementation MMHPrasiePeopleCountTableViewCell



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setAveragerScoreView];
    }
    return self;
}


- (NSMutableArray *)progressViewArray {
    if (!_progressViewArray) {
        self.progressViewArray = [NSMutableArray array];
    }
    return _progressViewArray;
}

- (NSMutableArray *)peopleCountLabelArray {
    if (!_peopleCountLabelArray) {
        self.peopleCountLabelArray = [NSMutableArray array];
    }
    return _peopleCountLabelArray;
}
-(void)setAveragerScoreView{
    
    NSString *str;
    switch (self.praiseType) {
        case GoodsPraseType:
            str = @"平均4.3分";
            break;
        case ShopPraiseType:
            str = @"门店口碑4.3分";
            break;
        default:
            break;
    }
    CGSize size = [str boundingRectWithSize:CGSizeMake(MMHFloat(200), MMHFloat(50)) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: MMHFontOfSize(15)} context:nil].size;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(20), MMHFloat(20), size.width, size.height)];
    self.averageLabel = label;
    label.text = str;
    label.font = MMHFontOfSize(15);
    label.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.contentView addSubview:label];
  
    self.ratingView = [[MMHRatingView alloc] initWithEmptyImageName:@"score_icon_bigstar_us" halfImageName:@"score_icon_bigstarhalf" fullImageName:@"score_icon_bigstar_s"];
    self.ratingView.frame = CGRectMake(CGRectGetMaxX(label.frame)+10, MMHFloat(20), MMHFloat(130), MMHFloat(15)) ;
    label.centerY = _ratingView.centerY;
    [self.ratingView setPadding:10.0f];
    [self.contentView addSubview:_ratingView];
    
    CGSize fiveLSize = [@"5分" boundingRectWithSize:CGSizeMake(50, 50) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: MMHFontOfSize(12)} context:nil].size;
    
    for (int i = 0; i < 5; i++) {
        UILabel *fiveL = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(20), CGRectGetMaxY(label.frame) + MMHFloat(20) + MMHFloat(21) * i, fiveLSize.width, fiveLSize.height)];
        fiveL.textColor = [UIColor colorWithCustomerName:@"蓝"];
        fiveL.font = MMHFontOfSize(12);
        NSString *score = [NSString stringWithFormat:@"%d分",5 - i];
        fiveL.text = score;
        [self.contentView addSubview:fiveL];
        
        MMHStarRatingView *ratingView = [[MMHStarRatingView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(fiveL.frame) + MMHFloat(20), CGRectGetMaxY(label.frame) + MMHFloat(20) + MMHFloat(21) * i, MMHFloat(220), MMHFloat(12))];
        ratingView.tag = i+1000;
        [self.progressViewArray addObject:ratingView];
        [self.contentView addSubview:ratingView];
        /**
         评分对应的人数
         */
        UILabel *praisePeopleCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(ratingView.frame) + MMHFloat(20),  CGRectGetMaxY(label.frame) + MMHFloat(20) + MMHFloat(21) * i, MMHFloat(50), MMHFloat(12))];
        praisePeopleCount.font = MMHFontOfSize(12);
        praisePeopleCount.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        
        [self.peopleCountLabelArray addObject:praisePeopleCount];
        
        [self.contentView addSubview:praisePeopleCount];
        
        
    }
    
    
}

- (void)configCellWithCountOfPraise:(NSArray *)array {
    
  NSArray *sortedArray= (NSArray *)[[array reverseObjectEnumerator] allObjects];
    NSInteger totalCount = 0;
    NSInteger totalCore = 0;
    for (int i = 0; i < sortedArray.count; i++) {
        
        NSInteger peopleCount = [sortedArray[i] integerValue];
        totalCore  +=  ((i+1) * peopleCount);
        totalCount += peopleCount;
    }
    for (int i = 0; i < 5; i++) {
        MMHStarRatingView * progressView = self.progressViewArray[i];
        NSInteger peopleCount =[array[i] integerValue];
        UILabel *praiseCountL = self.peopleCountLabelArray[i];
        praiseCountL.text = [NSString stringWithFormat:@"%ld", peopleCount];
#ifdef DEBUG_FISH
   [progressView changeForeViewWithRate:0.5];
#else
        if (totalCount==0) {
            [progressView changeForeViewWithRate:0];
        }else{
            [progressView changeForeViewWithRate:peopleCount /(totalCount *1.0)];
        }
#endif
        
    }
     NSString *score = [NSString stringWithFormat:@"%0.01f", totalCore / (totalCount * 1.0)];
    switch (self.praiseType) {
        case GoodsPraseType:
            self.averageLabel.text = [NSString stringWithFormat:@"平均%@分", score];
            break;
        case ShopPraiseType:
            self.averageLabel.text = [NSString stringWithFormat:@"门店口碑%@分", score];
            break;
        default:
            break;
    }
    self.ratingView.rating = totalCore / (totalCount * 1.0);
}


@end
