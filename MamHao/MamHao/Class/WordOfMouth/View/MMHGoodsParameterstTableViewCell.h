//
//  MMHGoodsParameterstTableViewCell.h
//  MamHao
//
//  Created by fishycx on 15/7/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMHGoodsParameterstTableViewCell : UITableViewCell

@property (nonatomic, strong)NSString *parametersKey;
@property (nonatomic, strong)NSString *parametersValue;
@end

