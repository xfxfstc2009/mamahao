//
//  TQStarRatingView.h
//  TQStarRatingView
//
//  Created by fuqiang on 13-8-28.
//  Copyright (c) 2013年 TinyQ. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CXStarRatingView;

@protocol StarRatingViewDelegate <NSObject>

@optional
-(void)starRatingView:(CXStarRatingView *)view score:(float)score;

@end

@interface CXStarRatingView : UIView

- (id)initWithFrame:(CGRect)frame numberOfStar:(int)number padding:(CGFloat)padding;
@property (nonatomic, readonly) int numberOfStar;
@property (nonatomic, weak) id <StarRatingViewDelegate> delegate;

- (void)changeStarForegroundViewWithScore:(CGFloat)score;
@end
