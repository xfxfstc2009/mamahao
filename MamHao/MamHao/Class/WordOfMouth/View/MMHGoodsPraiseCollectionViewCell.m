//
//  MMHGoodsPraiseCollectionViewCell.m
//  MamHao
//
//  Created by 余传兴 on 15/5/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGoodsPraiseCollectionViewCell.h"
#import "MMHImageView.h"

@interface MMHGoodsPraiseCollectionViewCell ()<UIAlertViewDelegate>

@property (nonatomic, strong)UIImageView *imageView;
@property (nonatomic, strong)UIButton *iconButton; //删除图片按钮
@property (nonatomic, strong)UIButton *cancleBtn;//取消按钮

@end

@implementation MMHGoodsPraiseCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.imageView = [[MMHImageView alloc] initWithFrame:self.bounds];
        [self addSubview:_imageView];
        
      //  [self addCancelBtn];
    }
    return self;
}

- (void)addCancelBtn{

    
    UIButton *iconBtn= [UIButton buttonWithType:UIButtonTypeCustom];
    iconBtn.frame = CGRectMake(self.bounds.size.width - MMHFloat(25), MMHFloat(0), MMHFloat(25), MMHFloat(25));
    iconBtn.layer.cornerRadius = 10;
    self.cancleBtn = iconBtn;
    self.iconButton = iconBtn;
    //iconBtn.backgroundColor = [UIColor grayColor];
    [iconBtn addTarget:self action:@selector(handleBtnClickAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:iconBtn];

}

- (void)handleBtnClickAction:(UIButton *)sender {
    
//    UIView *viewview = [[UIView alloc] initWithFrame:CGRectMake(59, 59, 69, 96)];
    
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"" message:@"您确定要删除图片吗?" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    view.alertViewStyle = UIAlertActionStyleDefault;
    [view show];
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(CancelCell:)]) {
            [self.delegate CancelCell:self];
        }
    }
}
- (void)configIconViewOfBool:(BOOL)isAddBtn{
    
    
    
    if (isAddBtn) {
        
        [self.cancleBtn setHidden:YES];
        [self.cancleBtn setEnabled:NO];
    }else{
    
        [self.cancleBtn setEnabled:YES];
        [self.cancleBtn setHidden:NO];
    }
   
    
    [_iconButton setImage:[UIImage imageNamed:@"pic_btn_close"] forState:UIControlStateNormal];
    
}
- (void)configCellWithImage:(UIImage *)image {
    
    self.imageView.image = image;
    if (image == nil) {
        
        [self.iconButton removeFromSuperview];
        UIView *addView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MMHFloat(90), MMHFloat(90))];
        self.addView = addView;
        addView.layer.borderWidth = 1;
        addView.layer.borderColor = [UIColor colorWithCustomerName:@"白灰"].CGColor;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(MMHFloat(30), MMHFloat(17.5), MMHFloat(30), MMHFloat(30))];
        
        imageView.image = [UIImage imageNamed:@"address_btn_add"];
        [addView addSubview:imageView];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(15), MMHFloat(65), MMHFloat(60), MMHFloat(10))];
        label.text = @"添加晒单图片";
        label.font = MMHFontOfSize(10);
        label.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        [addView addSubview:label];
        [self addSubview:addView];
        
    }else{
        [self addSubview:self.iconButton];
    }
    
}
@end
