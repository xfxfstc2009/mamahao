//
//  MMHWordOfMouthTableViewCell.h
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHPublicPraiseFrameModel.h"
#import "MMHPublicPraiseModel.h"
@class MMHWordOfMouthTableViewCell;
typedef NS_ENUM(NSInteger, MMHWomTableViewCellType) {

    MMHWordOfMouthTableViewCellTypeOfGoodsDetail,
    MMHWordOfMouthTableViewCellTypeOfWordOfMouth,
};
@protocol MMHWomTableViewCellDelegate <NSObject>

- (void)cell:(MMHWordOfMouthTableViewCell *)cell tapImageView:(MMHImageView *)imageView;

@end

@interface MMHWordOfMouthTableViewCell : UITableViewCell

@property (nonatomic, weak)id<MMHWomTableViewCellDelegate> womCellDelegate;
@property (nonatomic, strong)MMHPublicPraiseFrameModel *frameModel;
@property (nonatomic, assign)MMHWomTableViewCellType type;


+(instancetype)cellWithTableView:(UITableView *)tableView cellType:(MMHWomTableViewCellType) type;
@end
