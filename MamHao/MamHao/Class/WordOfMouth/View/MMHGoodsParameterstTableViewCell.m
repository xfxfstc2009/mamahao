//
//  MMHGoodsParameterstTableViewCell.m
//  MamHao
//
//  Created by fishycx on 15/7/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGoodsParameterstTableViewCell.h"

@interface MMHGoodsParameterstTableViewCell ()

@property (nonatomic, strong)UILabel *goodsParametersKeyLabel;
@property (nonatomic, strong)UILabel *goodsParametersValueLabel;
@end
@implementation MMHGoodsParameterstTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
          
            self.goodsParametersKeyLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(10), 0, MMHFloat(115), self.height)];
    
            _goodsParametersKeyLabel.font = MMHF4;
            _goodsParametersKeyLabel.textColor = [UIColor colorWithHexString:@"666666"];
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            [self.contentView addSubview:_goodsParametersKeyLabel];
            self.goodsParametersValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(125), 0, self.bounds.size.width - MMHFloat(125), self.height)];
            _goodsParametersValueLabel.font = F3;
            _goodsParametersValueLabel.textColor = [UIColor colorWithHexString:@"333333"];
            [self.contentView addSubview:_goodsParametersValueLabel];
    }
    return self;
}
- (void)setParametersKey:(NSString *)parametersKey {
    self.goodsParametersKeyLabel.text = parametersKey;
    if ([parametersKey isEqualToString:@"规格参数"]) {
        self.goodsParametersKeyLabel.font = MMHF3;
        self.goodsParametersKeyLabel.textColor = [UIColor colorWithHexString:@"ff4d61"];
    }else{
        self.goodsParametersKeyLabel.font = MMHF4;
        self.goodsParametersKeyLabel.textColor = [UIColor colorWithHexString:@"666666"];
    }
}

- (void)setParametersValue:(NSString *)parametersValue{
    self.goodsParametersValueLabel.text = parametersValue;
   
}
@end
