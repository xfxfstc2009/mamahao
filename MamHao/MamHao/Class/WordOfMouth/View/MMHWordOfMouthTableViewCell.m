//
//  MMHWordOfMouthTableViewCell.m
//  MamHao
//
//  Created by 余传兴 on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHWordOfMouthTableViewCell.h"
#import "MMHImageView.h"
#import "MMHRatingView.h"
#import "CXStarRatingView.h"
@interface MMHWordOfMouthTableViewCell ()

@property (nonatomic, strong)MMHImageView *iconImageView;// 头像
@property (nonatomic, strong)UILabel *userNickLabel;//用户昵称
@property (nonatomic, strong)UILabel *ageLable;//宝宝年龄
@property (nonatomic, strong)UILabel *dataTimeLabel;//发布时间
@property (nonatomic, strong)UIView *starBackGroundView;//星星，imageView
@property (nonatomic, strong)UILabel *boughtGoodsState;//显示已经购买商品
@property (nonatomic, strong)UILabel *evvluationLable;//评价内容--文字
@property (nonatomic, strong)UIScrollView *imageViewScroll;//评价内容--宝贝图片
@property (nonatomic, strong)UILabel *shopReplyContent;//店铺回复
@property (nonatomic, strong)UILabel *bufferCommentContent;//追加评价
@property (nonatomic, strong)UILabel *bufferCommnetTime;//追加评价的时间
@property (nonatomic, strong)UIScrollView *bufferPicsImageScroll;//追加评价的图片
@property (nonatomic, strong)CXStarRatingView *ratingView; //星星。
@property (nonatomic, strong)NSMutableArray *praiseImageViews;
@property (nonatomic, strong)NSMutableArray *bufferPraiseImageViews;
@end

@implementation MMHWordOfMouthTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        //头像
        self.iconImageView = [[MMHImageView alloc] init];
        [self.contentView addSubview:self.iconImageView];
        self.iconImageView.layer.cornerRadius = MMHFloat(6);
        /**
         *  用户昵称
         */
        self.userNickLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.userNickLabel];
        self.userNickLabel.numberOfLines = 0;
        self.userNickLabel.font = MMHF5;
        self.userNickLabel.textAlignment = NSTextAlignmentLeft;
        self.userNickLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        
        /**
         *  宝宝年龄
         */
        UILabel *ageLable = [[UILabel alloc] init];
        self.ageLable = ageLable;
        _ageLable.numberOfLines = 0;
        _ageLable.font = MMHFontOfSize(12);
        _ageLable.textAlignment = NSTextAlignmentLeft;
        _ageLable.textColor = [UIColor colorWithCustomerName:@"灰"];
        [self.contentView addSubview:ageLable];
        
        
        /**
         *  发布时间
         */
        UILabel *dataTimeLabel = [[UILabel alloc] init];
        self.dataTimeLabel  = dataTimeLabel;
        _dataTimeLabel.font = MMHFontOfSize(12.);
        _dataTimeLabel.textAlignment = NSTextAlignmentRight;
        _dataTimeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        _dataTimeLabel.numberOfLines = 0;
        [self.contentView addSubview:dataTimeLabel];
        /**
         * 星星，imageView
         */
        UIView *starBackGroundView = [[UIView alloc] init];
        [self.contentView addSubview:starBackGroundView];
        self.starBackGroundView = starBackGroundView;
        //self.ratingView = [[MMHRatingView alloc] initWithEmptyImageName:@"store_icon_graystar" halfImageName:@"store_icon_halfstar" fullImageName:@"store_icon_smallstar"];
        self.ratingView = [[CXStarRatingView alloc] initWithFrame:CGRectMake(kScreenWidth - MMHFloat(15)*5-MMHFloat(10), MMHFloat(25), MMHFloat(15)*5, MMHFloat(15)) numberOfStar:5 padding:0.f];
        [self.contentView addSubview:_ratingView];
        
        /**
         *  显示已经购买商品
         */
        
        UILabel *boughtGoodsState = [[UILabel alloc] init];
        self.boughtGoodsState = boughtGoodsState;
        boughtGoodsState.textColor = [UIColor colorWithHexString:@"56a7df"];
        boughtGoodsState.font = MMHFontOfSize(12);
        _boughtGoodsState.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:boughtGoodsState];
        
        
        /**
         *  评价内容--文字
         */
        UILabel *evvluationLable = [[UILabel alloc] init];
        evvluationLable.numberOfLines = 0;
        evvluationLable.font = MMHFontOfSize(14.);
        evvluationLable.textAlignment = NSTextAlignmentLeft;
        evvluationLable.textColor = [UIColor colorWithCustomerName:@"黑"];
        
        [self.contentView addSubview:evvluationLable];
        self.evvluationLable = evvluationLable;
        
        /**
         *  评价内容--图片
         */
        UIScrollView *imageViewScroll = [[UIScrollView alloc] init];
        imageViewScroll.showsHorizontalScrollIndicator = NO;
        imageViewScroll.showsVerticalScrollIndicator = NO;
        [self.contentView addSubview:imageViewScroll];
        self.imageViewScroll = imageViewScroll;
        self.praiseImageViews = [NSMutableArray array];
        for (int i = 0; i < 9; i++) {
            MMHImageView *imageView = [[MMHImageView alloc] init];
            imageView.stringTag = [NSString stringWithFormat:@"imageView%d", i];
           [self.praiseImageViews addObject:imageView];
        }

        
        
        //门店回复
        self.shopReplyContent = [[UILabel alloc] init];
        _shopReplyContent.numberOfLines = 0;
        _shopReplyContent.font = MMHF2;
        _shopReplyContent.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_shopReplyContent];
        
        //追加评价
        self.bufferCommentContent= [[UILabel alloc] init];
        _bufferCommentContent.numberOfLines = 0;
        _bufferCommentContent.font = MMHF2;
        _bufferCommentContent.textAlignment = NSTextAlignmentLeft;
        _bufferCommentContent.textColor = [UIColor colorWithCustomerName:@"黑"];
        [self.contentView addSubview:_bufferCommentContent];
        
        //追加评价图片
        self.bufferPicsImageScroll = [[UIScrollView alloc] init];
        _bufferPicsImageScroll.showsHorizontalScrollIndicator = NO;
        _bufferPicsImageScroll.showsVerticalScrollIndicator = NO;
        self.bufferPraiseImageViews = [NSMutableArray array];
        [self.contentView addSubview:_bufferPicsImageScroll];
        
            for (int i = 0; i < 9; i++) {
                MMHImageView *imageView = [[MMHImageView alloc] init];
                imageView.stringTag = [NSString stringWithFormat:@"bufferPicsImageScroll%d", i];
                [self.bufferPraiseImageViews addObject:imageView];
        }

       
    }
    return self;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setType:(MMHWomTableViewCellType)type{
    _type = type;
}

+(instancetype)cellWithTableView:(UITableView *)tableView  cellType:(MMHWomTableViewCellType)type{
    
    static NSString *identifier = @"MMHPublicPraiseCell";
    MMHWordOfMouthTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    cell.type = type;
    if (!tableView) {
        cell = [[MMHWordOfMouthTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    return cell;
    
}

- (void)setFrameModel:(MMHPublicPraiseFrameModel *)frameModel {
    _frameModel = frameModel;
    // 1.设置数据
    [self settingData];
    
    // 2.设置frame；
    [self settingFrame];
}

- (void)settingData{
    //数据
    MMHPublicPraiseModel *praiseModel = self.frameModel.praiseModel;
    /**
     *  头像
     */
    [self.iconImageView updateViewWithImageAtURL:praiseModel.headPic];
    
    /**
     *  用户昵称
     */
    self.userNickLabel.text = praiseModel.name;
    
    
    /**
     *  宝宝年龄
     */
    self.ageLable.text = praiseModel.babyAge;
    
    
    /**
     *  发布时间
     */
    self.dataTimeLabel.text = praiseModel.commentTime;
    

    [self.ratingView changeStarForegroundViewWithScore:praiseModel.star];
       /**
     *  显示已经购买商品
     */
    if (praiseModel.isBuy) {
        
        self.boughtGoodsState.text = @"已购买该商品";
    }else{
        
        self.boughtGoodsState.text = @"";
    }
    
    
    /**
     *  评价内容--文字
     */
    self.evvluationLable.text = praiseModel.commentContent;
    
    /**
     *   评价内容--宝贝图片
     */
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    NSArray *array = praiseModel.pics;
    if(praiseModel.pics){
       [self.imageViewScroll removeAllSubviews];
        for (int i = 0;i < array.count; i++) {
            MMHImageView *imageView =  self.praiseImageViews[i];
            imageView.userInteractionEnabled = YES;
            [imageView addGestureRecognizer:tapGestureRecognizer];
            [imageView updateViewWithImage:[UIImage imageWithRenderColor:[UIColor whiteColor] renderSize:CGSizeMake(MMHFloat(90), MMHFloat(90))]];
            imageView.frame = CGRectMake((mmh_relative_float(90)+mmh_relative_float(10)) * i, 0, mmh_relative_float(90), mmh_relative_float(90));
            imageView.stringTag = [NSString stringWithFormat:@"imageView%d", i];
            [self.imageViewScroll addSubview:imageView];
            [imageView updateViewWithImageAtURL:array[i]];
        }
        self.imageViewScroll.contentSize = CGSizeMake(mmh_relative_float(10) + (array.count) * (mmh_relative_float(90) + mmh_relative_float(10)), mmh_relative_float(90));
    }

    //门店回复
    NSString *shopReplyStr = [NSString stringWithFormat:@"[门店回复]:%@", praiseModel.shopReplyContent];
    NSMutableAttributedString *shopReplyStr1 = [[NSMutableAttributedString alloc] initWithString:shopReplyStr];
    NSRange shopReplyRange = NSMakeRange(0, 7);
    [shopReplyStr1 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"56a7df"] range:shopReplyRange];
    if (praiseModel.shopReplyContent.length) {
        self.shopReplyContent.attributedText = shopReplyStr1;
    }
    
    //追加评价
    NSString *bufferCommentStr = [NSString stringWithFormat:@"[%@追评]:%@",praiseModel.bufferCommentTime ,praiseModel.bufferCommentContent];
    if (praiseModel.bufferCommentContent.length) {
        self.bufferCommentContent.text = bufferCommentStr;
        
    }
    //追加评价图片
    NSArray *bufferPicsArray = [NSArray arrayWithArray:praiseModel.bufferPics];
        [self.bufferPicsImageScroll removeAllSubviews];
        for (int i = 0; i<bufferPicsArray.count; i++) {
            MMHImageView *bufferPicsImageView = self.bufferPraiseImageViews[i];
            bufferPicsImageView.frame = CGRectMake((mmh_relative_float(90)+mmh_relative_float(10)) * i, 0, mmh_relative_float(90), mmh_relative_float(90));
            bufferPicsImageView.stringTag = [NSString stringWithFormat:@"bufferPicsImageScroll%d", i];
            [self.bufferPicsImageScroll addSubview:bufferPicsImageView];
            self.bufferPicsImageScroll.contentSize = CGSizeMake(mmh_relative_float(10) + (i + 1) * (mmh_relative_float(90) + mmh_relative_float(10)), mmh_relative_float(90));
            NSString *url = bufferPicsArray[i];
            [bufferPicsImageView updateViewWithImageAtURL:url];
        }
}

- (void)settingFrame{
    
    /**
     *  头像
     */
    self.iconImageView.frame = self.frameModel.iconImageViewF;
    
    
    /**
     *  用户昵称
     */
    self.userNickLabel.frame = self.frameModel.userNickLabelF;
    
    /**
     *  宝宝年龄
     */
    self.ageLable.frame = self.frameModel.ageLableF;
    
    
    /**
     *  发布时间
     */
    self.dataTimeLabel.frame = self.frameModel.dataTimeLabelF;
    
    
    
    /**
     * 星星，imageView
     */
    self.starBackGroundView.frame = self.frameModel.starBackGroundViewF;
    
    
    /**
     *  显示已经购买商品
     */
   
    self.boughtGoodsState.frame = self.frameModel.boughtGoodsStateF;

    
    /**
     *  评价内容--文字
     */
    self.evvluationLable.frame = self.frameModel.evvluationLableF;
    
    /**
     *   评价内容--宝贝图片
     */
    self.imageViewScroll.frame = self.frameModel.evvImageViewF;
    //门店回复
    self.shopReplyContent.frame = self.frameModel.shopReplyContentF;
    //追评
    self.bufferCommentContent.frame = self.frameModel.bufferCommentContentF;
    //追加评价的图片
    self.bufferPicsImageScroll.frame = self.frameModel.bufferPicsF;

}

- (void)handleTap:(UITapGestureRecognizer *)gesture{
   //通知代理去执行操作
    if (self.womCellDelegate && [self.womCellDelegate respondsToSelector:@selector(cell:tapImageView:)]) {
      // [self.womCellDelegate cell:self tapImageView:]
    }
}
@end
