//
//  MMHPraiseShowOrderTableViewCell.h
//  MamHao
//
//  Created by 余传兴 on 15/5/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHSingleProductWithShopCartModel.h"
#import "MMHImageView.h"


@class MMHPraiseShowOrderTableViewCell;

@protocol MMhPraiseShowOrderCellDelegate <NSObject>

-(void)praiseShowOrederCell:(MMHPraiseShowOrderTableViewCell *)cell;

@end
@interface MMHPraiseShowOrderTableViewCell : UITableViewCell

@property (nonatomic,strong)UIView *shopActivityView;           /**< 商店活动*/
@property (nonatomic,strong)MMHImageView *productImageView;     /**< 物品图片*/
@property (nonatomic,strong)UILabel *productNameLabel;          /**< 商品名称*/
@property (nonatomic,strong)UIView *skuView;                    /**< 保存sku的View*/
@property (nonatomic,strong)UILabel *priceLabel;                /**< 价格*/
@property (nonatomic,strong)UILabel *numberLabel;               /**< 数量*/
@property (nonatomic,strong)UIButton *orderTypeButton;                      /**< 状态按钮*/

// model
@property (nonatomic,strong)MMHSingleProductWithShopCartModel *singleProduct;  /**< 商品model */
@property (nonatomic, strong)UINavigationController *navi;



@property (nonatomic, assign)id<MMhPraiseShowOrderCellDelegate> delegate; //cell 的代理

@end
