//
//  MMHPraiseOrderTableViewCell2.m
//  MamHao
//
//  Created by 余传兴 on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPraiseOrderTableViewCell2.h"
#import "AMRatingControl.h"
#import "MMHNetworkAdapter+WordOfMouth.h"


@interface MMHPraiseOrderTableViewCell2 ()<UITextViewDelegate>

@property (nonatomic, strong)AMRatingControl *control1; //服务评价

@property (nonatomic, strong)UIButton *button;//按钮
@property (nonatomic, strong)UILabel *serviceLabel;//服务评价label
@property (nonatomic, strong)UIImageView *backImageView;//背景图片
// inputView
@property (nonatomic,strong)UITextView *inputExplainTextView;           /**< 输入框*/
@property (nonatomic,strong)UILabel *labelplaceHorder;                  /**< 输入框placeHolder*/
@property (nonatomic,strong)UILabel *textLimitLabel;                    /**< 文字长度*/

// imageBackgroundView
@property (nonatomic,strong)UIView *imageBackgroundView;                /**< 背景图片*/
@property (nonatomic,strong)NSMutableArray *photoImageMutableArr;       /**< 照片数组*/

@end
@implementation MMHPraiseOrderTableViewCell2
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        UILabel *label1 = [[UILabel alloc] init];
        self.serviceLabel = label1;
        label1.text = @"服务评价:";
        label1.textColor = [UIColor lightGrayColor];
        label1.font = MMHFontOfSize(15);
        CGSize size = [label1.text sizeWithCalcFont:label1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        label1.frame =  CGRectMake(MMHFloat(10), 20, size.width, size.height);
        AMRatingControl *control = [[AMRatingControl alloc] initWithLocation:CGPointMake(CGRectGetMaxX(label1.frame)+MMHFloat(15), 20) emptyImage:[UIImage imageNamed:@"score_icon_bigstar_us"] solidImage:[UIImage imageNamed:@"score_icon_bigstar_s"] andMaxRating:5];
        control.rating = 5;
        self.control1 = control;
        [self  addSubview:label1];
        [self  addSubview:control];
        
        
        
        UIImageView *backgroundImageView = [[UIImageView alloc]init];
        self.backImageView = backgroundImageView;
        backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
        backgroundImageView.frame =CGRectMake(MMHFloat(10), CGRectGetMaxY(self.serviceLabel.frame)+MMHFloat(20), kScreenWidth- 2 * MMHFloat(10) , MMHFloat(75));
        backgroundImageView.userInteractionEnabled = NO;
        [self.contentView addSubview:backgroundImageView];
        
        // 创建门店评价输入框
        self.inputExplainTextView = [[UITextView alloc]initWithFrame:CGRectMake(MMHFloat(10), CGRectGetMaxY(self.serviceLabel.frame)+MMHFloat(20), kScreenWidth- 2 * MMHFloat(10) , MMHFloat(75))];
        
        self.inputExplainTextView.delegate = self;
        self.inputExplainTextView.textColor = [UIColor blackColor];
        self.inputExplainTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        self.inputExplainTextView.returnKeyType = UIReturnKeyDefault;
        [self.inputExplainTextView setKeyboardType:UIKeyboardTypeDefault];
        self.inputExplainTextView.backgroundColor = [UIColor clearColor];
        self.inputExplainTextView.scrollEnabled = YES;
        
        // 创建placeHolder
        self.labelplaceHorder = [[UILabel alloc]init];
        self.labelplaceHorder.backgroundColor = [UIColor clearColor];
        self.labelplaceHorder.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        self.labelplaceHorder.textColor = [UIColor colorWithRed:200/256.0 green:200/256.0 blue:200/256.0 alpha:1];
        self.labelplaceHorder.numberOfLines = 0;
        self.labelplaceHorder.text = @"对门铺进行评价，帮助我们为您提供更好的服务！";
        CGSize labelPlaceHolderSize = [self.labelplaceHorder.text sizeWithCalcFont:self.labelplaceHorder.font constrainedToSize:CGSizeMake(kScreenWidth - 4 * MMHFloat(10), CGFLOAT_MAX)];
        self.labelplaceHorder.frame = CGRectMake(MMHFloat(10)*2,CGRectGetMinY(self.inputExplainTextView.frame)+ MMHFloat(10),kScreenBounds.size.width - 4 * MMHFloat(10),labelPlaceHolderSize.height);
        [self.contentView addSubview:self.inputExplainTextView];
        [self.contentView addSubview:self.labelplaceHorder];
       // [MMHTool calculateCharacterLengthForAres:self.inputExplainTextView.text];
        
        
        //发表评价按钮
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button = button;
        button.titleLabel.font = MMHFontOfSize(15);
        button.layer.cornerRadius = MMHFloat(3);
        button.layer.borderWidth = 1;
        button.layer.masksToBounds = YES;
        button.layer.borderColor =  [UIColor colorWithCustomerName:@"分割线"].CGColor;
        [button setTitleColor:[UIColor  blackColor] forState:UIControlStateNormal];
        button.layer.borderColor = [UIColor hexChangeFloat:@"dcdcdc"].CGColor;
        [button addTarget:self action:@selector(handleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        CGSize buttonContentSize = [@"发表评价" sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        self.button.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11)- (buttonContentSize.width + 2 * MMHFloat(10)),CGRectGetMaxY(self.inputExplainTextView.frame)+MMHFloat(10), buttonContentSize.width + 2 *MMHFloat(10), MMHFloat(30));
        [self.contentView addSubview:button];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orderPraiseContentEditDone:) name:KOrderPraiseContentEditDone object:nil];
        
    }
    return self;
}

- (void)orderPraiseContentEditDone:(NSNotification *)notification{
    
    [self.inputExplainTextView resignFirstResponder];
}

- (void)handleBtnClick:(UIButton *)sender{
    NSInteger severiceStar = self.control1.rating;
    //发表评价
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(praiseOrderWithOrderNo:serveStar:deliverySpeedStar:commentContent:)]) {
        [self.cellDelegate praiseOrderWithOrderNo:self.orderId serveStar:severiceStar deliverySpeedStar:10 commentContent:self.inputExplainTextView.text];
    }
}
- (void)setButtonStateWithBOOl:(BOOL)isCommented {
    [self setUserInteractionEnabled:!isCommented];
    NSString *buttonText;
    if (isCommented) {
        buttonText = @"已完成";
        [self.button setBackgroundColor:[UIColor hexChangeFloat:@"dcdcdc"]];
        [self setEditing:NO animated:YES];
        self.backImageView.frame = CGRectZero;
        self.inputExplainTextView.frame =  CGRectZero;
        self.labelplaceHorder.frame = CGRectZero;
        CGSize buttonContentSize = [@"发表评价" sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        self.button.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11)- (buttonContentSize.width + 2 * MMHFloat(10)),CGRectGetMaxY(self.serviceLabel.frame)-MMHFloat(30), buttonContentSize.width + 2 *MMHFloat(10), MMHFloat(30));
    }else{
        buttonText = @"发表评价";
    }
    [self.button setTitle:buttonText forState:UIControlStateNormal];
}

- (void)setStarControlRatingWithStarCount:(NSInteger)count StarControl:(NSInteger)StarControlIndex{
    if (count == 0) {
        count = 5;
    }
     self.control1.rating = count;
    
}




- (void)textViewDidBeginEditing:(UITextView *)textView {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(textViewBeginEdit:)]) {
        [self.cellDelegate textViewBeginEdit:textView];
    }
}


- (void)textViewDidEndEditing:(UITextView *)textView {
    if (self.cellDelegate && [self.cellDelegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [self.cellDelegate textViewDidEndEidt:textView];
    }
}


-(void)textViewDidChange:(UITextView *)textView {
    if (self.inputExplainTextView.text.length == 0) {
        self.labelplaceHorder.text = @"对门铺进行评价，帮助我们为您提供更好的服务！";
    }else{
        self.labelplaceHorder.text = @"";
    }
//    [self.textLimitLabel setText:[NSString stringWithFormat:@"%lu/140",(unsigned long)[MMHTool calculateCharacterLengthForAres:self.inputExplainTextView.text]]];
    //self.navigationItem.rightBarButtonItem.enabled=YES;
}
- (void)leaveEditMode{
    [self.inputExplainTextView resignFirstResponder];
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:KOrderPraiseContentEditDone object:nil];
}
@end
