//
//  TQStarRatingView.m
//  TQStarRatingView
//
//  Created by fuqiang on 13-8-28.
//  Copyright (c) 2013年 TinyQ. All rights reserved.
//

#import "CXStarRatingView.h"

@interface CXStarRatingView ()

@property (nonatomic, strong) UIView *starBackgroundView;
@property (nonatomic, strong) UIView *starForegroundView;
@property (nonatomic, assign) CGFloat padding;

@end

@implementation CXStarRatingView

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame numberOfStar:5 padding:self.padding];
}

- (id)initWithFrame:(CGRect)frame numberOfStar:(int)number padding:(CGFloat)padding
{
    self = [super initWithFrame:frame];
    if (self) {
        _padding = padding;
        _numberOfStar = number;
        self.starBackgroundView = [self buidlStarViewWithImageName:@"score_icon_bigstar_us"];
        self.starForegroundView = [self buidlStarViewWithImageName:@"score_icon_bigstar_s"];
        [self addSubview:self.starBackgroundView];
        [self addSubview:self.starForegroundView];
    }
    return self;
}

- (UIView *)buidlStarViewWithImageName:(NSString *)imageName
{
    CGRect frame = self.bounds;
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.clipsToBounds = YES;
    for (int i = 0; i < self.numberOfStar; i ++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imageName]];
        imageView.frame = CGRectMake(self.padding + MMHFloat(self.padding+15) * i, 0, MMHFloat(15), MMHFloat(15));
        [view addSubview:imageView];
    }
    return view;
}

- (void)changeStarForegroundViewWithScore:(CGFloat)score {
    
    CGFloat width = score / 5.0 * self.frame.size.width;
    self.starForegroundView.frame = CGRectMake(0, 0, width, self.frame.size.height);
}

- (void)changeStarForegroundViewWithPoint:(CGPoint)point
{
    CGPoint p = point;
    
    if (p.x < 0)
    {
        p.x = 0;
    }
    else if (p.x > self.frame.size.width)
    {
        p.x = self.frame.size.width;
    }
    
    NSString * str = [NSString stringWithFormat:@"%0.2f",p.x / self.frame.size.width];
    float score = [str floatValue];
    p.x = score * self.frame.size.width;
    self.starForegroundView.frame = CGRectMake(0, 0, p.x, self.frame.size.height);
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(starRatingView: score:)])
    {
        [self.delegate starRatingView:self score:score];
    }
}

@end
