//
//  MMHPraiseOrderTableViewCell.h
//  MamHao
//
//  Created by 余传兴 on 15/5/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#define KOrderPraiseContentEditDone  @"orderPraiseContentEditDone"

@protocol MMHPraiseOrderTableViewCell1Delegate<NSObject>

- (void)textViewDidEndEidt:(UITextView *)textView;
- (void)textViewBeginEdit:(UITextView *)textView;
- (void)cell1praiseOrderWithOrderNo:(NSString *)orderNo serveStar:(NSInteger)star deliverySpeedStar: (NSInteger)deliverSpeedStar commentContent:(NSString *)comment;
@end

@interface MMHPraiseOrderTableViewCell : UITableViewCell
@property (nonatomic, strong)NSString *orderId; //评价订单接口参数2 订单id
@property (nonatomic, weak)id<MMHPraiseOrderTableViewCell1Delegate>cellDelegate;
@property (nonatomic, strong)UIBarButtonItem *rightBarItem;
- (void)setStarControlRatingWithStarCount:(NSInteger)count StarControl:(NSInteger)StarControlIndex;

- (void)setButtonStateWithBOOl:(BOOL)isComment;

@end
