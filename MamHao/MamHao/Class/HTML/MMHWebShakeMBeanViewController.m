//
//  MMHWebShakeMBeanViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHWebShakeMBeanViewController.h"


#define MMHHTMLTag @"mamahao://"


@implementation MMHWebShakeMBeanViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (kNoneNetworkReachable) {
        [self.view showTipsOfNetworkUnreachable];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[NSString stringWithFormat:@"%@",request.URL] hasPrefix:@"mamahao://"]){               // 分享
        NSString *managerTag = [self getManagerTagWithUrl:request.URL];
        [self mamahaoHTMLManager:managerTag];
    }
    
    return true;
}

#pragma mark - 截取获取方法名称
-(NSString *)getManagerTagWithUrl:(NSURL *)url{
    NSString *tagURL = [NSString stringWithFormat:@"%@",url];
    NSArray *managerTagArr = [tagURL componentsSeparatedByString:MMHHTMLTag];
    NSString *managerTag = [managerTagArr lastObject];
    return managerTag;
}

@end

