//
//  MMHHTMLModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHHTMLModel : MMHFetchModel
@property (nonatomic,copy)NSString *title;                  /**< 主标题*/
@property (nonatomic,copy)NSString *desc;                   /**< 导读*/
@property (nonatomic,copy)NSString *content;                /**< 文章内容*/
@property (nonatomic,copy)NSString *url;                    /**< 连接*/
@property (nonatomic,copy)NSString *image;                 /**< 图片URL*/
@end
