//
//  MMHHTMLLinkURLManager.h
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMHHTMLLinkURLManager : NSObject

#pragma mark 声明
#define MMHHTML_Link_Announcement  @"http://h5.mamahao.com/safety/"         /**< 声明*/

#pragma mark 摇妈豆
#define MMHHTML_Link_BeanGame  @"http://h5.mamahao.com/beans/"              /**< 摇妈豆*/

#pragma mark 什么是妈豆
#define MMHHTML_Link_Beans  @"http://h5.mamahao.com/help/beans/"            /**< 什么是妈豆*/

#pragma mark 关于妈妈好
#define MMHHTML_Link_about @"http://h5.mamahao.com/about/"                  /**< 关于妈妈好*/

#pragma mark 积分说明
#define MMHHTML_Link_Integral @"http://h5.mamahao.com/help/integral/"       /**< 积分说明*/

#pragma mark 测试
#define MMHHTML_Link_Test @"http://h5.mamahao.com/sdk.html"                 /**< 测试*/


@end
