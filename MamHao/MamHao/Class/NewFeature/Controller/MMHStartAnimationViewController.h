//
//  MMHStartAnimationViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 开场动画
#import "AbstractViewController.h"

@interface MMHStartAnimationViewController : AbstractViewController
@property (nonatomic, copy) void (^startBlock)(BOOL isBundingBaby);         // 是否绑定宝贝
@end
