//
//  MMHStartAnimationViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHStartAnimationViewController.h"
#import "UIImage+ImageEffects.h"
#define kImageCount 4

@interface MMHStartAnimationViewController()<UIScrollViewDelegate>
@property (nonatomic,strong)UIButton *statusButton;
@property (nonatomic,strong)UIPageControl *pageControl;

@end

@implementation MMHStartAnimationViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createScrollView];
    
}

#pragma mark createScrollView
-(void)createScrollView{
    UIScrollView *scrollView = [[UIScrollView  alloc]init];
    scrollView.frame = kScreenBounds;
    scrollView.contentSize = CGSizeMake(kImageCount * kScreenBounds.size.width, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    // 创建imageView
    for (int i = 0 ; i < kImageCount ; i++){
        [self createIamgeViewWithIndex:i andSuperView:scrollView];
    }

    // 创建pageControl
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.center = CGPointMake(kScreenBounds.size.width * .5f, kScreenBounds.size.height * .9f);
    self.pageControl.bounds = CGRectMake(0, 0, 100, 0);
    self.pageControl.numberOfPages = kImageCount;
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.pageIndicatorTintColor=[UIColor colorWithCustomerName:@"浅灰"]; // 设置点的颜色
    self.pageControl.currentPageIndicatorTintColor=[UIColor colorWithCustomerName:@"黑"];
    [self.view addSubview:self.pageControl];
}

#pragma mark createImageView
-(void)createIamgeViewWithIndex:(NSInteger)index andSuperView:(UIView *)superView{
    // 1. 创建 imageView
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(index * kScreenBounds.size.width , 0, kScreenBounds.size.width, kScreenBounds.size.height);
    // 2. 设置图片
    LESScreenMode screenMode = [UIScreen currentScreenMode];
    NSString *name = [NSString stringWithFormat:@"guide_%ld_%ld", (long)screenMode, (long)index + 1];
    imageView.image = [UIImage imageNamed:name];
    [superView addSubview:imageView];
    
    if (index == kImageCount - 1){
        [self lastImageSettingInView:imageView];
    }
}

#pragma mark lastImageSetting
-(void)lastImageSettingInView:(UIView *)view{
    view.userInteractionEnabled = YES;
    
//    // 创建一个button
    self.statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.statusButton.selected = YES;
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [nextButton addTarget:self action:@selector(nextClick:) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setImage:[UIImage imageNamed:@"btn_guide_buy_n"] forState:UIControlStateNormal];
    [nextButton setImage:[UIImage imageNamed:@"btn_guide_buy_s"] forState:UIControlStateHighlighted];
    nextButton.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(174)) / 2., kScreenBounds.size.height - MMHFloat(48) - MMHFloat(26), MMHFloat(174), MMHFloat(48));
    [view addSubview:nextButton];
}

#pragma mark - actionClick
-(void)statusChooseButton:(UIButton *)sender{
    UIButton *statusChooseButton = (UIButton *)sender;
    statusChooseButton.selected = !statusChooseButton.selected;
    if (statusChooseButton.selected){           // 跳转到妈妈状态
        [self.statusButton setTitle:@"选中" forState:UIControlStateNormal];
    } else {
        [self.statusButton setTitle:@"没选中" forState:UIControlStateNormal];
    }
}

-(void)nextClick:(UIButton *)sender{
    if (self.startBlock){
        self.startBlock(self.statusButton.selected);
    }
}

#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x > 2 * scrollView.bounds.size.width){
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
    }
}
@end
