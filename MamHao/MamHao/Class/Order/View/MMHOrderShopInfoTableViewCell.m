//
//  MMHOrderShopInfoTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderShopInfoTableViewCell.h"

@interface MMHOrderShopInfoTableViewCell(){
//    UIImageView *phoneImageView;
}

@end

@implementation MMHOrderShopInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

-(void)setSinceTheMentionAddressModel:(MMHsinceTheMentionAddressModel *)sinceTheMentionAddressModel{
    UILabel *shopNameFixedLabel = (UILabel *)[self viewWithStringTag:@"fixedLabel0"];
    UILabel *shopAddFixedLabel = (UILabel *)[self viewWithStringTag:@"fixedLabel1"];
    UILabel *shopWorkTimeFixedLabel = (UILabel *)[self viewWithStringTag:@"fixedLabel2"];
    UILabel *shopholidayTimeFixedLabel = (UILabel *)[self viewWithStringTag:@"fixedLabel3"];
    UILabel *shopTelFixedLabel = (UILabel *)[self viewWithStringTag:@"fixedLabel4"];
    
    UILabel *shopNameLabel = (UILabel *)[self viewWithStringTag:@"dymicLabel0"];
    UILabel *shopAddLabel = (UILabel *)[self viewWithStringTag:@"dymicLabel1"];
    shopAddLabel.numberOfLines = 0;
    UILabel *shopWorkTimeLabel = (UILabel *)[self viewWithStringTag:@"dymicLabel2"];
    UILabel *shopholidayTimeLabel = (UILabel *)[self viewWithStringTag:@"dymicLabel3"];
    UILabel *shopTelLabel = (UILabel *)[self viewWithStringTag:@"dymicLabel4"];
    
    shopNameFixedLabel.orgin_x = _shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund ? shopNameFixedLabel.orgin_x +=10:shopNameFixedLabel.orgin_x;
    shopAddFixedLabel.orgin_x = _shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund ? shopAddFixedLabel.orgin_x +=10:shopAddFixedLabel.orgin_x;
    shopWorkTimeFixedLabel.orgin_x = _shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund ? shopWorkTimeFixedLabel.orgin_x +=10:shopWorkTimeFixedLabel.orgin_x;
    shopholidayTimeFixedLabel.orgin_x = _shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund ? shopholidayTimeFixedLabel.orgin_x +=10:shopholidayTimeFixedLabel.orgin_x;
    shopTelFixedLabel.orgin_x = _shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund ? shopTelFixedLabel.orgin_x +=10:shopTelFixedLabel.orgin_x;

    
    shopNameLabel.text = sinceTheMentionAddressModel.shopName.length?sinceTheMentionAddressModel.shopName:@"妈妈好平台";
    shopAddLabel.text = sinceTheMentionAddressModel.addr.length?sinceTheMentionAddressModel.addr:@"未获取到地址";
    shopWorkTimeLabel.text = sinceTheMentionAddressModel.workTime.length?[NSString stringWithFormat:@"工作日:%@",sinceTheMentionAddressModel.workTime]:@"未获取到营业时间信息";
    shopholidayTimeLabel.text = sinceTheMentionAddressModel.holiday.length?[NSString stringWithFormat:@"节假日:%@",sinceTheMentionAddressModel.holiday]:@"未获取到营业时间信息";
    shopTelLabel.hidden = YES;
    [self.phoneNumberButton setTitle:sinceTheMentionAddressModel.telephone.length?sinceTheMentionAddressModel.telephone:CustomerServicePhoneNumber forState:UIControlStateNormal];
    
    
    CGSize contentOfSize = [@"门店名称:" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"]  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    //
    CGSize shopAddressContentSize = [shopAddLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - contentOfSize.width - 2 * MMHFloat(25) - MMHFloat(10), CGFLOAT_MAX)];
    
    shopAddLabel.frame = CGRectMake(CGRectGetMaxX(shopAddFixedLabel.frame) + MMHFloat(10), shopAddFixedLabel.frame.origin.y, kScreenBounds.size.width - contentOfSize.width - 2 * MMHFloat(25) - MMHFloat(10), shopAddressContentSize.height);
    
    shopWorkTimeFixedLabel.orgin_y = CGRectGetMaxY(shopAddLabel.frame) + MMHFloat(10);
    shopWorkTimeLabel.orgin_y = CGRectGetMaxY(shopAddLabel.frame) + MMHFloat(10);
    
    shopholidayTimeFixedLabel.orgin_y = CGRectGetMaxY(shopWorkTimeFixedLabel.frame) + MMHFloat(10);
    shopholidayTimeLabel.orgin_y = CGRectGetMaxY(shopWorkTimeFixedLabel.frame) + MMHFloat(10);
    
    shopTelFixedLabel.orgin_y = CGRectGetMaxY(shopholidayTimeLabel.frame)+ MMHFloat(10);
    self.phoneNumberButton.orgin_y = CGRectGetMaxY(shopholidayTimeLabel.frame) + MMHFloat(10);
    // 计算phopTelLabelFrame
    CGSize phoneContentOfSize = [sinceTheMentionAddressModel.telephone sizeWithCalcFont:self.phoneNumberButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.phoneNumberButton.titleLabel.font])];
    self.phoneNumberButton.size_width = phoneContentOfSize.width;
    
    
    // 创建imageView
//    phoneImageView.frame = CGRectMake(CGRectGetMaxX(self.phoneNumberButton.frame) + MMHFloat(5), self.phoneNumberButton.frame.origin.y, MMHFloat(15), MMHFloat(16));
    
}

-(void)createView{
//    self = [[UIView alloc]init];
//    self.backgroundColor = [UIColor clearColor];
//    self.frame = self.bounds;
//    [self addSubview:self];
    
    CGSize contentOfSize = [@"门店名称:" sizeWithCalcFont:[UIFont  fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    
    self.propertiesArr = [self getAllProperties];
    for (int i = 0 ;i <self.propertiesArr.count;i++){
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.stringTag = [NSString stringWithFormat:@"fixedLabel%i",i];
        fixedLabel.font = [UIFont  fontWithCustomerSizeName:@"小正文"];
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        fixedLabel.frame = CGRectMake(self.shopInfoCellUsingClass == MMHShopInfoCellUsingClassRefund? MMHFloat(11) + MMHFloat(10):MMHFloat(11), MMHFloat(15) + i *(contentOfSize.height + MMHFloat(10)), contentOfSize.width, contentOfSize.height);
        
        [self addSubview:fixedLabel];
        
        // 创建dymicLabel
        UILabel *dymicLabel = [[UILabel alloc]init];
        dymicLabel.backgroundColor = [UIColor clearColor];
        dymicLabel.stringTag = [NSString stringWithFormat:@"dymicLabel%i",i];
        dymicLabel.font = [UIFont  fontWithCustomerSizeName:@"小正文"];
        dymicLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), MMHFloat(15) + i *(contentOfSize.height + MMHFloat(10)), kScreenBounds.size.width - contentOfSize.width - 2 * MMHFloat(25) - MMHFloat(10), contentOfSize.height);
        [self addSubview:dymicLabel];
        
        if (i == 0){
            fixedLabel.text = @"门店名称:";
        } else if (i == 1){
            fixedLabel.text = @"门店地址:";
        } else if (i == 2){
            fixedLabel.text = @"营业时间:";
        } else if (i == 3){
            fixedLabel.text = @" ";
        } else if (i == 4){
            fixedLabel.text = @"联系方式:";
            self.phoneNumberButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.phoneNumberButton.backgroundColor = [UIColor clearColor];
            [self.phoneNumberButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
            self.phoneNumberButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            self.phoneNumberButton.frame = dymicLabel.frame;
            [self addSubview:self.phoneNumberButton];
            
//            // 创建imageView
//            phoneImageView = [[UIImageView alloc]init];
//            phoneImageView.backgroundColor = [UIColor clearColor];
//            phoneImageView.image = [UIImage imageNamed:@"contact_icon_smallcall"];
//            phoneImageView.stringTag = @"phoneImageView";
//            [self addSubview:phoneImageView];

        }
    }
}

-(void)setShopInfoCellUsingClass:(MMHShopInfoCellUsingClass)shopInfoCellUsingClass{
    _shopInfoCellUsingClass = shopInfoCellUsingClass;
}

+ (CGFloat)cellHeightWith:(MMHsinceTheMentionAddressModel *)sinceTheMentionAddressModel andUsingClass:(MMHShopInfoCellUsingClass)shopInfousingClass{
    CGFloat cellHeight = 0;
    
    CGSize contentOfSize = [@"门店名称:" sizeWithCalcFont:[UIFont  fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGSize shopAddressContentSize = [sinceTheMentionAddressModel.addr.length?sinceTheMentionAddressModel.addr:@" " sizeWithCalcFont:[UIFont  fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11 )- contentOfSize.width, CGFLOAT_MAX)];
    
    
    cellHeight = 2 *MMHFloat(15) + 4 * (contentOfSize.height + MMHFloat(10)) - MMHFloat(10) + MMHFloat(10) + shopAddressContentSize.height;
    cellHeight +=(shopInfousingClass == MMHShopInfoCellUsingClassRefund? MMHFloat(11) + MMHFloat(10): MMHFloat(11));
    return cellHeight;
}


- (NSArray *)getAllProperties {
    u_int count;
    objc_property_t *properties  =class_copyPropertyList([MMHsinceTheMentionAddressModel class], &count);
    NSMutableArray *propertiesArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++) {
        const char* propertyName =property_getName(properties[i]);
        [propertiesArray addObject: [NSString stringWithUTF8String: propertyName]];
    }
    free(properties);
    return propertiesArray;
}


@end
