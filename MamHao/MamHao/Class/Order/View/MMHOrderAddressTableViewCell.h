//
//  MMHOrderAddressTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHOrderDetailDeliveryModel.h"

// 地址cell
@interface MMHOrderAddressTableViewCell : UITableViewCell

@property (nonatomic,strong)    UILabel *consigneeNameLabel;    /**< 收货人Label*/
@property (nonatomic,strong)    UILabel *phoneNumberLabel;      /**< 电话号码Label*/
@property (nonatomic,strong)    UILabel *addressLabel;          /**< 地址label*/

@property (nonatomic,strong)MMHOrderDetailDeliveryModel *addressSingleModel; /**< 单个地址model */

+ (CGFloat)cellHeightWith:(MMHOrderDetailDeliveryModel *)addressSingleModel;  /**< 返回cell 高度*/

@end
