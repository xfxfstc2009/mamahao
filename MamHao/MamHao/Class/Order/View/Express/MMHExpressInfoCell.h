//
//  MMHExpressInfoCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【物流详情cell】
#import <UIKit/UIKit.h>
#import "MMHOrderWithGoodsListModel.h"

@interface MMHExpressInfoCell : UITableViewCell

@property (nonatomic,strong)NSArray <MMHOrderWithGoodsListModel>*transferGoodsList;         // 商品列表

@end
