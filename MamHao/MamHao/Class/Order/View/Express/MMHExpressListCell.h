//
//  MMHExpressListCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHExpressContentModel.h"

@interface MMHExpressListCell : UITableViewCell
@property (nonatomic,strong)NSArray<MMHExpressContentModel> *data;
+(CGFloat)cellHeight:(NSArray<MMHExpressContentModel> *)data;

@end
