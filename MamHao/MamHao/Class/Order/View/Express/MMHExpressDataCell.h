//
//  MMHExpressDataCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHExpressContentModel.h"
@interface MMHExpressDataCell : UITableViewCell

@property (nonatomic,assign)BOOL isHltCell;
@property (nonatomic,strong)MMHExpressContentModel *expressContentModel;
@property (nonatomic,assign)CGFloat cellHeight;

+(CGFloat)cellHeight:(MMHExpressContentModel * )expressContentModel;
@end
