//
//  MMHExpressDataCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressDataCell.h"

@interface MMHExpressDataCell()
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UIView *cricleView;
@property (nonatomic,strong)UIImageView *hltImageView;

@end

@implementation MMHExpressDataCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建descLabel
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.font = F4;
    self.contentLabel.numberOfLines = 0;
    [self addSubview:self.contentLabel];
    
    // timeLabel
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = F2;
    [self addSubview:self.timeLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = C4;
    [self addSubview:self.lineView];
    
    // 创建圆点
    self.cricleView = [[UIView alloc]init];
    self.cricleView.backgroundColor = C4;
    
    [self addSubview: self.cricleView];
    
    // lineView
    self.hltImageView = [[UIImageView alloc]init];
    self.hltImageView.backgroundColor = [UIColor clearColor];
    self.hltImageView.image = [UIImage imageNamed:@"logistics_icon_point"];
    [self addSubview:self.hltImageView];
    self.hltImageView.hidden = YES;
}


-(void)setIsHltCell:(BOOL)isHltCell{
    _isHltCell = isHltCell;
}

-(void)setCellHeight:(CGFloat)cellHeight{
    _cellHeight = cellHeight;
}

-(void)setExpressContentModel:(MMHExpressContentModel *)expressContentModel{
    _expressContentModel = expressContentModel;
    
    self.contentLabel.text = expressContentModel.desc;
    CGSize contentOfSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(MMHFloat(56), MMHFloat(20), kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), contentOfSize.height);
    
    self.timeLabel.text = expressContentModel.time;
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(self.contentLabel.size_width, CGFLOAT_MAX)];
    self.timeLabel.frame = CGRectMake(self.contentLabel.orgin_x, CGRectGetMaxY(self.contentLabel.frame) + MMHFloat(8), self.contentLabel.size_width, timeSize.height);
    
    // 创建line
    if (self.isHltCell){
        self.lineView.frame = CGRectMake(self.contentLabel.orgin_x - MMHFloat(20) - MMHFloat(8), self.contentLabel.orgin_y + MMHFloat(8), 1, (self.cellHeight - MMHFloat(20) - MMHFloat(8)));
        self.contentLabel.textColor = [UIColor hexChangeFloat:@"5ccccc"];
        self.timeLabel.textColor = [UIColor hexChangeFloat:@"5ccccc"];
     } else {
         self.lineView.frame = CGRectMake(self.contentLabel.orgin_x - MMHFloat(20) - MMHFloat(8), 0, 1, self.cellHeight);
         self.contentLabel.textColor = C4;
         self.timeLabel.textColor = C4;
     }
    
    // 创建圆点
    self.cricleView.frame = CGRectMake(self.lineView.orgin_x - MMHFloat(5), MMHFloat(20) + ([MMHTool contentofHeight:self.contentLabel.font] - MMHFloat(10))/2., MMHFloat(10), MMHFloat(10));
    self.cricleView.layer.cornerRadius = MMHFloat(5);
    
    // 创建高亮点
    self.hltImageView.frame = CGRectMake(self.lineView.orgin_x - MMHFloat(8), self.cricleView.orgin_y - MMHFloat(3), MMHFloat(16), MMHFloat(16));
    if (self.isHltCell){
        self.hltImageView.hidden = NO;
    } else {
        self.hltImageView.hidden = YES;
    }
}

+(CGFloat)cellHeight:(MMHExpressContentModel * )expressContentModel{
    CGFloat cellHeight = 0;
    
    CGSize contentOfSize = [expressContentModel.desc sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
    
    CGSize timeSize = [expressContentModel.time sizeWithCalcFont:F2 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
    cellHeight  = MMHFloat(20) + contentOfSize.height + MMHFloat(8) + timeSize.height + MMHFloat(20);
    return cellHeight ;
}

@end
