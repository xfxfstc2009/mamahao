//
//  MMHExpressSingleProductCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHOrderWithGoodsListModel.h"
@interface MMHExpressSingleProductCell : UITableViewCell

@property (nonatomic,strong)MMHOrderWithGoodsListModel *orderWithGoodsListModel;


@end
