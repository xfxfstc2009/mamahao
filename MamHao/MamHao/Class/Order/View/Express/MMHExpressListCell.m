//
//  MMHExpressListCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressListCell.h"

@interface MMHExpressListCell()
@property (nonatomic,strong)UIImageView *hltImageView;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *contentLabel;
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation MMHExpressListCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 1. 创建descLabel
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.font = F4;
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor hexChangeFloat:@"5ccccc"];
    [self addSubview:self.contentLabel];
    
    // 2. 创建时间label
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = F2;
    self.timeLabel.textColor = C4;
    [self addSubview:self.timeLabel];
    
    // 3. 创建lineView
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = C4;
    [self addSubview:self.lineView];
    
    // 4. 创建hltImageView
    self.hltImageView = [[UIImageView alloc]init];
    self.hltImageView.backgroundColor = [UIColor clearColor];
    self.hltImageView.image = [UIImage imageNamed:@"logistics_icon_point"];
    [self addSubview:self.hltImageView];
}


-(void)setData:(NSArray<MMHExpressContentModel> *)data{
    if (data.count){
        MMHExpressContentModel *expressContentModel = [data objectAtIndex:0];
        self.contentLabel.text = expressContentModel.desc;
        CGSize contentOfSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
        self.contentLabel.frame = CGRectMake(MMHFloat(56), MMHFloat(20), kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), contentOfSize.height);
        
        self.timeLabel.text = expressContentModel.time;
        CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(self.contentLabel.size_width, CGFLOAT_MAX)];
        self.timeLabel.frame = CGRectMake(self.contentLabel.orgin_x, CGRectGetMaxY(self.contentLabel.frame) + MMHFloat(8), self.contentLabel.size_width, timeSize.height);
        
        self.lineView.frame = CGRectMake(self.contentLabel.orgin_x - MMHFloat(20) - MMHFloat(8), self.contentLabel.orgin_y + MMHFloat(8), 1, CGRectGetMaxY(self.timeLabel.frame) + MMHFloat(20) - (self.contentLabel.orgin_y + MMHFloat(8)));
        
        self.hltImageView.frame = CGRectMake(self.lineView.orgin_x - MMHFloat(8), self.lineView.orgin_y - MMHFloat(8), MMHFloat(16), MMHFloat(16));
    } else {
        self.contentLabel.text = @"暂无物流信息";
        CGSize contentOfSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
        self.contentLabel.frame = CGRectMake(MMHFloat(56), MMHFloat(20), kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), contentOfSize.height);
        
        self.lineView.frame = CGRectMake(self.contentLabel.orgin_x - MMHFloat(20) - MMHFloat(8), self.contentLabel.orgin_y + MMHFloat(8), 1, CGRectGetMaxY(self.timeLabel.frame) + MMHFloat(20) - (self.contentLabel.orgin_y + MMHFloat(8)));
        
        self.hltImageView.frame = CGRectMake(self.lineView.orgin_x - MMHFloat(8), self.lineView.orgin_x - MMHFloat(8), MMHFloat(16), MMHFloat(16));

    }
}


+(CGFloat)cellHeight:(NSArray<MMHExpressContentModel> *)data{
    CGFloat cellHeight = 0;
    if (data.count){
        MMHExpressContentModel *expressContentModel = [data objectAtIndex:0];
        CGSize contentOfSize = [expressContentModel.desc sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
        
        CGSize timeSize = [expressContentModel.time sizeWithCalcFont:F2 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(56) - MMHFloat(20), CGFLOAT_MAX)];
        
        cellHeight = MMHFloat(20) + MMHFloat(20) + contentOfSize.height + timeSize.height + MMHFloat(8);

    } else {
        cellHeight = 2 * MMHFloat(20) + [MMHTool contentofHeight:F4];
    }
    return cellHeight;

}

@end
