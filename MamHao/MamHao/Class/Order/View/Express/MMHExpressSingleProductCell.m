//
//  MMHExpressSingleProductCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressSingleProductCell.h"

@interface MMHExpressSingleProductCell()
@property (nonatomic,strong)MMHImageView *productImageView;
@property (nonatomic,strong)UILabel *productNameLabel;
@property (nonatomic,strong)UILabel *skuLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *numberLabel;

@end

@implementation MMHExpressSingleProductCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 创建imageView
    self.productImageView = [[MMHImageView alloc]init];
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.layer.masksToBounds = YES;
    [self.productImageView.layer setCornerRadius:MMHFloat(6.)];
    self.productImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.productImageView.layer.borderWidth = .5f;
    [self addSubview:self.productImageView];
    
    // productName
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.productNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.productNameLabel.numberOfLines = 2;
    [self addSubview:self.productNameLabel];
    
    // 创建颜色sku
    self.skuLabel = [[UILabel alloc]init];
    self.skuLabel.backgroundColor = [UIColor clearColor];
    self.skuLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.skuLabel];
    
    // priceLabel
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.priceLabel.numberOfLines = 1;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    // numberLabel
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.numberLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.numberLabel];

}

-(void)setOrderWithGoodsListModel:(MMHOrderWithGoodsListModel *)orderWithGoodsListModel{
    _orderWithGoodsListModel = orderWithGoodsListModel;

    // 1. 图片
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.frame = CGRectMake(MMHFloat(11), MMHFloat(15), MMHFloat(70), MMHFloat(70));
    [self.productImageView updateViewWithImageAtURL:orderWithGoodsListModel.itemPic];
    
    // 2. 商品名称
    CGSize shopNameSize = [@"我是商店" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width, CGFLOAT_MAX)];
    self.productNameLabel.text = orderWithGoodsListModel.itemName;
    
    CGSize productNameSize = [orderWithGoodsListModel.itemName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"]constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(10), CGFLOAT_MAX)];
    
    if (productNameSize.height <= shopNameSize.height){
        self.productNameLabel.numberOfLines = 1;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(18), shopNameSize.height);
    } else {
        self.productNameLabel.numberOfLines = 2;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(18), 2 * shopNameSize.height);
    }
    
    // 3.价格label
    self.priceLabel.frame = CGRectMake(CGRectGetMaxX(self.productNameLabel.frame) + 5, CGRectGetMinY(self.productImageView.frame), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70 + 10) - self.productNameLabel.bounds.size.width - MMHFloat(5), shopNameSize.height);
    self.priceLabel.text = [NSString stringWithFormat:@"￥99999.00"];
    
    // 4. 数量
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.productNameLabel.frame) + 5, MMHFloat(5) + CGRectGetMaxY(self.priceLabel.frame), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70 + 10) - self.productNameLabel.bounds.size.width - MMHFloat(5), 20);
    self.numberLabel.text = [NSString stringWithFormat:@"x%li",(long)orderWithGoodsListModel.quantity];
    
    // 5.sku
    NSString *skuTempString = @"";
    for (int i = 0 ; i < self.orderWithGoodsListModel.spec.count;i++){
        [skuTempString stringByAppendingString:[NSString stringWithFormat:@" %@",[[self.orderWithGoodsListModel.spec objectAtIndex:i] skuValue]]];
    }
    self.skuLabel.text = skuTempString;
    self.skuLabel.frame = CGRectMake(self.productNameLabel.orgin_x, CGRectGetMaxY(self.productNameLabel.frame) , self.productNameLabel.size_width, [MMHTool contentofHeight:self.skuLabel.font]);
}


@end
