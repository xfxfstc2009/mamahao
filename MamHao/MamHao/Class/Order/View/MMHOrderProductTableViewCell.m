//
//  MMHOrderProductTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderProductTableViewCell.h"

@implementation MMHOrderProductTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

-(void)createView{                                      /**< 创建view*/
    // shopActivityView
    self.shopActivityView = [[UIView alloc]init];
    self.shopActivityView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.shopActivityView];
    
    // 创建imageView
    self.productImageView = [[MMHImageView alloc]init];
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.layer.masksToBounds = YES;
    [self.productImageView.layer setCornerRadius:MMHFloat(6.)];
    self.productImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.productImageView.layer.borderWidth = .5f;
    [self addSubview:self.productImageView];
    
    // productName
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.productNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.productNameLabel.numberOfLines = 2;
    [self addSubview:self.productNameLabel];
    
    // 创建颜色sku
    self.skuView = [[UIView alloc]init];
    self.skuView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.skuView];
    
    // priceLabel
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.priceLabel.numberOfLines = 1;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    // numberLabel
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.numberLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.numberLabel];
    
    // 活动view
    self.activityView = [[UIView alloc]init];
    self.activityView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.activityView];
    
    // button
    self.orderTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.orderTypeButton.backgroundColor = [UIColor clearColor];
    self.orderTypeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.orderTypeButton.layer.cornerRadius = MMHFloat(6.);
    self.orderTypeButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    self.orderTypeButton.layer.borderWidth = 1;
    [self addSubview:self.orderTypeButton];
    
    // subButton
    self.orderSubTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.orderSubTypeButton.backgroundColor = [UIColor clearColor];
    self.orderSubTypeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.orderSubTypeButton.layer.cornerRadius = MMHFloat(6.);
    self.orderSubTypeButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    self.orderSubTypeButton.layer.borderWidth = 1;
    [self addSubview:self.orderSubTypeButton];
    
    // checkImageView
    self.checkImageView = [[UIImageView alloc]init];
    self.checkImageView.backgroundColor = [UIColor clearColor];
    self.checkImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - MMHFloat(20), (self.height - 20)/2., MMHFloat(20), MMHFloat(20));
    [self addSubview:self.checkImageView];

    
    
}

-(void)setOrderType:(MMHOrderType)orderType {
    _orderType = orderType;
}

-(void)setCellType:(MMHCellType)cellType{
    _cellType = cellType;
}

-(void)setSingleProduct:(MMHSingleProductWithShopCartModel *)singleProduct{
    _singleProduct = singleProduct;
    // 1. 图片
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.frame = CGRectMake(MMHFloat(11), MMHFloat(15), MMHFloat(70), MMHFloat(70));
    [self.productImageView updateViewWithImageAtURL:singleProduct.itemPic];
    
    // 2. 商品名称
   CGSize shopNameSize = [@"我是商店" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width, CGFLOAT_MAX)];
    self.productNameLabel.text = singleProduct.itemName;
    
    CGSize productNameSize = [singleProduct.itemName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"]constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(10), CGFLOAT_MAX)];
    
    if (productNameSize.height <= shopNameSize.height){
        self.productNameLabel.numberOfLines = 1;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(18), shopNameSize.height);
    } else {
        self.productNameLabel.numberOfLines = 2;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70) - MMHFloat(18), 2 * shopNameSize.height);
    }
    
    // 3.价格label
    self.priceLabel.frame = CGRectMake(CGRectGetMaxX(self.productNameLabel.frame) + 5, CGRectGetMinY(self.productImageView.frame), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70 + 10) - self.productNameLabel.bounds.size.width - MMHFloat(5), shopNameSize.height);
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",singleProduct.itemPrice];
    
    // 4. 数量
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.productNameLabel.frame) + 5, MMHFloat(5) + CGRectGetMaxY(self.priceLabel.frame), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70 + 10) - self.productNameLabel.bounds.size.width - MMHFloat(5), 20);
    self.numberLabel.text = [NSString stringWithFormat:@"x%li",(long)singleProduct.quantity];
    
    
    // SKU
    self.skuView.frame = CGRectMake(MMHFloat(10)+ MMHFloat(70 + 11 ), MMHFloat(10)+CGRectGetMaxY(self.productNameLabel.frame), 100, 20);
    CGFloat skuViewWidth = 0;
    
    CGFloat skuHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    if (!self.skuView.subviews.count){
        for (int i = 0 ; i < singleProduct.specSKU.count;i++){
            UILabel *skuLabel = [[UILabel alloc]init];
            skuLabel.backgroundColor = [UIColor clearColor];
            skuLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            skuLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            skuLabel.stringTag = [NSString stringWithFormat:@"skuLabel%i",i];
            skuLabel.textAlignment = NSTextAlignmentLeft;
            // 计算文字长度
            CGSize skuContentSize = [[[singleProduct.specSKU objectAtIndex:i] skuValue] sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, skuHeight)];
            skuLabel.frame = CGRectMake(MMHFloat(5) * i + skuViewWidth, 0, skuContentSize.width, skuHeight);
            skuViewWidth = skuContentSize.width;
            skuLabel.text = [[singleProduct.specSKU objectAtIndex:i] skuValue];
            [self.skuView addSubview:skuLabel];
        }
    }
    
    // 【商品活动】
    // 1. 判断是否有活动
    if (singleProduct.activity != nil){
        if (singleProduct.activity.count){
            if (!self.activityView.subviews.count){
                // 根据count 确定view 高度
                self.activityView.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.productImageView.frame) + MMHFloat(10), 200, MMHFloat(15) * singleProduct.activity.count + MMHFloat(5)*(singleProduct.activity.count - 1));
                self.activityView.backgroundColor = [UIColor yellowColor];
                // 1 加载label
                for (int i = 0 ; i < singleProduct.activity.count; i++){
                    MMHOrderProActivityModel *activityModel = [singleProduct.activity objectAtIndex:i];
                    // UILabel
                    UILabel *activityTypeLabel = [[UILabel alloc]init];
                    activityTypeLabel.backgroundColor = [UIColor grayColor];
                    activityTypeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
                    activityTypeLabel.textColor = [UIColor whiteColor];
                    activityTypeLabel.shadowOffset = CGSizeMake(.1, .1);
                    activityTypeLabel.shadowColor = [UIColor lightGrayColor];
                    // 计算长度
                    CGSize activitySize = [activityModel.type sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
                    activityTypeLabel.text = activityModel.type.length?activityModel.type:@"其他";
                    activityTypeLabel.frame = CGRectMake(0,i * (MMHFloat(5) + activitySize.height), activitySize.width, activitySize.height);
                    [self.activityView addSubview:activityTypeLabel];
                    
                    // 创建detail
                    UILabel *activityDetailLabel = [[UILabel alloc]init];
                    activityDetailLabel.backgroundColor = [UIColor clearColor];
                    activityDetailLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
                    activityDetailLabel.textAlignment = NSTextAlignmentLeft;
                    activityDetailLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                    activityDetailLabel.text = activityModel.desc;
                    activityDetailLabel.numberOfLines = 1;
                    // 计算长度
                    CGFloat detailHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
                    CGSize detailSize = [activityModel.desc sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"]constrainedToSize:CGSizeMake(CGFLOAT_MAX, detailHeight)];
                    activityDetailLabel.frame = CGRectMake(CGRectGetMaxX(activityTypeLabel.frame) + MMHFloat(8), i * (MMHFloat(5) + activitySize.height), detailSize.width, detailHeight);
                    [self.activityView addSubview:activityDetailLabel];
                }
            }
        }
    }
    
    // 增加按钮
    NSString *buttonText = @"";
    NSString *buttonSubText = @"";
    if (_orderType == MMHOrderTypeBeShipped){        // 待发货
        if (self.singleProduct.isRefunded){
            buttonText = @"追踪退款退货";
        } else {
            buttonText = @"申请退款";
        }
        [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        self.orderSubTypeButton.hidden = YES;
    } else if (_orderType == MMHOrderTypeBeReceipt){ // 待收货
        if (self.singleProduct.isRefunded){
            buttonText = @"追踪退款退货";
        } else {
            buttonText = @"申请退款";
        }
        if (self.receivingStateType == MMHReceivingStateTypeExpress){
            self.orderTypeButton.hidden = NO;
            self.orderSubTypeButton.hidden = NO;
            [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
            self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        } else if (self.receivingStateType == MMHReceivingStateTypeSelf){
            self.orderTypeButton.hidden = NO;
            self.orderSubTypeButton.hidden = YES;
            [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
            self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        } else if (self.receivingStateType == MMHReceivingStateTypeDelivery){
            self.orderSubTypeButton.hidden = YES;
            self.orderTypeButton.hidden = YES;
        } else {
            self.orderTypeButton.hidden = NO;
            self.orderSubTypeButton.hidden = NO;
        }
        self.orderTypeButton.hidden = YES;
        self.orderSubTypeButton.hidden = YES;
    } else if (_orderType == MMHOrderTypeBeEvaluation){// 待评价
        buttonText = @"再次购买";
        [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
        self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;

        if (self.singleProduct.isRefunded){
            buttonSubText = @"追踪退款退货";
        } else {
            buttonSubText = @"申请退货";
        }
        self.orderSubTypeButton.hidden = NO;
        [self.orderSubTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.orderSubTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    } else if (_orderType == MMHOrderTypeBeRefund){      // 待退货
        // 追踪退款退货
        if (self.singleProduct.isRefunded){
            buttonText = @"追踪退款退货";
        } else {
            buttonText = @"申请退款";
        }
        [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        self.orderSubTypeButton.hidden = YES;
    } else if (_orderType == MMHOrderTypeFinish){        // 已完成
        buttonText = @"再次购买";
        [self.orderTypeButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
        self.orderTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;
        
        // 追踪退款退货
        if (self.singleProduct.isRefunded){
            buttonSubText = @"追踪退款退货";
        } else {
            buttonSubText = @"申请退货";
        }
        self.orderSubTypeButton.hidden = NO;
        [self.orderSubTypeButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.orderSubTypeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    } else{       // 待付款
        self.orderTypeButton.hidden = YES;
        self.orderSubTypeButton.hidden = YES;
    }
    
    CGSize buttonContentSize = [buttonText sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    [self.orderTypeButton setTitle:buttonText forState:UIControlStateNormal];

    self.orderTypeButton.frame = CGRectMake(kScreenBounds.size.width -   MMHFloat(11) - (buttonContentSize.width + 2 * MMHFloat(10)), CGRectGetMaxY(self.numberLabel.frame) + MMHFloat(5), buttonContentSize.width + 2 * MMHFloat(10), MMHFloat(30));
    // 副按钮
    [self.orderSubTypeButton setTitle:buttonSubText forState:UIControlStateNormal];
    CGSize subTextSize = [buttonSubText sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(30))];
    self.orderSubTypeButton.frame = CGRectMake(CGRectGetMinX(self.orderTypeButton.frame) - MMHFloat(10) - (2 * MMHFloat(15) + subTextSize.width), self.orderTypeButton.frame.origin.y, subTextSize.width + 2 * MMHFloat(15), MMHFloat(30));
    
    
    if (self.cellType == MMHCellTypeChooseProduct){
        self.checkImageView.hidden = NO;
        self.orderSubTypeButton.hidden = YES;
        self.orderTypeButton.hidden = YES;
        self.numberLabel.hidden = YES;
    } else {
        self.checkImageView.hidden = YES;
    }
    
}

+(CGFloat)cellHeightWithSingleProductModel:(MMHSingleProductWithShopCartModel *)singleProduct{
    return MMHFloat(100);
}

- (void)setChecked:(BOOL)checked{
    if (checked) {
        [self animationWithCollectionWithButton:self.checkImageView];
    } else {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    self.isChecked = checked;
}

#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionView{
    collectionView.image = [UIImage imageNamed:@"address_icon_selected"];

    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    [collectionView.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}




@end
