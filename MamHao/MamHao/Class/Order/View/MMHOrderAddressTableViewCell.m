//
//  MMHOrderAddressTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderAddressTableViewCell.h"

@implementation MMHOrderAddressTableViewCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark 创建view
-(void)createView{
    self.backgroundView = [[UIImageView alloc] initWithImage:[self stretchImageWithName:@"order_bg_xinfeng"]];
    
    // 创建收货人信息
    self.consigneeNameLabel = [[UILabel alloc]init];
    self.consigneeNameLabel.backgroundColor = [UIColor clearColor];
    self.consigneeNameLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.consigneeNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.consigneeNameLabel];
    
    
    // 创建收货人手机号码
    self.phoneNumberLabel = [[UILabel alloc]init];
    self.phoneNumberLabel.backgroundColor = [UIColor clearColor];
    self.phoneNumberLabel.textAlignment = NSTextAlignmentRight;
    self.phoneNumberLabel.textColor = [UIColor blackColor];
    self.phoneNumberLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.phoneNumberLabel];
    
    // 创建addressLabel
    self.addressLabel = [[UILabel alloc]init];
    self.addressLabel.backgroundColor = [UIColor clearColor];
    self.addressLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    self.addressLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.addressLabel.numberOfLines = 0;
    [self addSubview:self.addressLabel];
}

#pragma mark setModel
-(void)setAddressSingleModel:(MMHOrderDetailDeliveryModel *)addressSingleModel{
    // 1. 收货人姓名
    self.consigneeNameLabel.text = [NSString stringWithFormat:@"收货人:  %@",addressSingleModel.deliveryName.length?addressSingleModel.deliveryName:@"收货人:  ***"];
    CGSize consigneeNameSize = [self.consigneeNameLabel.text sizeWithCalcFont:self.consigneeNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.consigneeNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(25), consigneeNameSize.width, consigneeNameSize.height);
    
    // 2. 收货人电话
    self.phoneNumberLabel.text = addressSingleModel.deliveryPhone.length?addressSingleModel.deliveryPhone:@"";
    CGSize phoneNumberSize = [self.phoneNumberLabel.text sizeWithCalcFont:self.phoneNumberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    self.phoneNumberLabel.frame = CGRectMake(CGRectGetMaxX(self.consigneeNameLabel.frame) + MMHFloat(10), MMHFloat(25), phoneNumberSize.width, phoneNumberSize.height);

    
    // 3. 地址label
    self.addressLabel.text = addressSingleModel.deliveryAddr.length?addressSingleModel.deliveryAddr:@"";
    CGSize addressSize = [self.addressLabel.text sizeWithCalcFont:self.addressLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), CGFLOAT_MAX)];
    self.addressLabel.frame = CGRectMake(MMHFloat(10), CGRectGetMaxY(self.consigneeNameLabel.frame) + MMHFloat(10), kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), addressSize.height);
}



// 拉伸
- (UIImage *)stretchImageWithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width topCapHeight:image.size.height * 0.5];
}

// 返回高度
+ (CGFloat)cellHeightWith:(MMHOrderDetailDeliveryModel *)addressSingleModel{
    CGFloat cellBoundsHeight = 0;
    if (addressSingleModel){
        // 1. 收货人height
        CGSize consigneeNameSize = [[NSString stringWithFormat:@"收货人:  %@",addressSingleModel.deliveryName] sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        // 2. 地址
        CGSize addressSize = [(addressSingleModel.deliveryAddr.length?addressSingleModel.deliveryAddr:@"") sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), CGFLOAT_MAX)];
        cellBoundsHeight = consigneeNameSize.height + MMHFloat(10)+ MMHFloat(20)+ MMHFloat(20)+addressSize.height;
    } else {
        cellBoundsHeight = 70;
    }
    return cellBoundsHeight;
}


@end
