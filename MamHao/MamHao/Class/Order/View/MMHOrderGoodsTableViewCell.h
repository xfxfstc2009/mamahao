//
//  MMHOrderGoodsTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 显示在商品列表的cell 
#import <UIKit/UIKit.h>
#import "MMHOrderDetailViewController.h"
#import "MMHOrderSingleModel.h"

@class MMHOrderDetailViewController;


@interface MMHOrderGoodsTableViewCell : UITableViewCell

@property (nonatomic,strong)UILabel *shopNameLabel;                 /**< 商店名称*/
@property (nonatomic,strong)UILabel *orderStylelabel;               /**< 订单状态*/
@property (nonatomic,strong)UIView *topLineView;                    /**< 顶部的线*/
@property (nonatomic,strong)UIView *goodsView;                      /**< 商品的view*/
@property (nonatomic,strong)UIView *bottomLineView;                 /**< 底部的line*/
@property (nonatomic,strong)UILabel *payLabel;                      /**< 支付label*/
@property (nonatomic,strong)UIButton *leftButton;                   /**< 左侧button*/
@property (nonatomic,strong)UIButton *rightButton;                  /**< 右侧button*/
@property (nonatomic,strong)UILabel *numberLabel;                   /**< numberLabel*/
@property (nonatomic,strong)UILabel *failureTimeLabel;              /**< 失效时间label*/

@property (nonatomic,assign)MMHReceivingStateType receivingStateType;
@property (nonatomic,strong)MMHOrderSingleModel *orderSingleModel;  /**< 单个订单*/
@property (nonatomic,assign)MMHOrderType orderType;


@end
