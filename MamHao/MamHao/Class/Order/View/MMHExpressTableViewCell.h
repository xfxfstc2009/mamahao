//
//  MMHExpressTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 物流信息cell 
#import <UIKit/UIKit.h>
#import "MMHExpressModel.h"
@interface MMHExpressTableViewCell : UITableViewCell

@property (nonatomic,strong)MMHExpressModel *transferExpressModel;

+(CGFloat)cellHeightWithExpressModel:(MMHExpressModel*)expressModel;

@end
