//
//  MMHOrderBillsTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderBillsTableViewCell.h"

@implementation MMHOrderBillsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

-(void)setOrderPriceArray:(NSArray<MMHOrderPriceModel> *)orderPriceArray{
    CGSize singleTxtSize = [@"商品" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    if (!self.priceView.subviews.count){
        // 1. priceView frame
        self.priceView.frame = CGRectMake(MMHFloat(15), MMHFloat(15), kScreenBounds.size.width - 2 *MMHFloat(11), singleTxtSize.height + (singleTxtSize.height + MMHFloat(12) * (orderPriceArray.count)));
        
        for (int i = 0 ; i < orderPriceArray.count ; i++){
            MMHOrderPriceModel *orderPriceModel =[orderPriceArray objectAtIndex:i];
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.text = orderPriceModel.entryName;
            fixedLabel.frame = CGRectMake(0, i * (singleTxtSize.height + MMHFloat(10)),100, singleTxtSize.height);
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [self.priceView addSubview:fixedLabel];
            
            // 创建右侧的price
            UILabel *priceLabel = [[UILabel alloc]init];
            priceLabel.backgroundColor = [UIColor clearColor];
            priceLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            priceLabel.textAlignment = NSTextAlignmentRight;
            priceLabel.text = [NSString stringWithFormat:@"￥%.2f",orderPriceModel.entryPrice];
            priceLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame), fixedLabel.frame.origin.y, self.priceView.bounds.size.width - fixedLabel.bounds.size.width - MMHFloat(3.), singleTxtSize.height);
            priceLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [self.priceView addSubview:priceLabel];
        }
    }
}

-(void)createView{
    self.priceView = [[UIView alloc]init];
    self.priceView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.priceView];
}


+ (CGFloat)cellHeightWithOrderPriceArray:(NSArray <MMHOrderPriceModel> *)orderPriceArray{
    CGFloat cellHeightWithPrice = 0;
    CGFloat singleLineHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeightWithPrice = singleLineHeight * orderPriceArray.count + (orderPriceArray.count + 1) * (MMHFloat(12));
    return cellHeightWithPrice;
}


@end
