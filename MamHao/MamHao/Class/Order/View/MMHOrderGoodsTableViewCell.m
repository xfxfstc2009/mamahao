//
//  MMHOrderGoodsTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderGoodsTableViewCell.h"
#import "MMHPopView.h"

@interface MMHOrderGoodsTableViewCell(){
    UILabel *receivingStateTypeLabel;
    UIImageView *timeImageView;
    UIImageView *arrowImageView;
    
    //
    MMHImageView *goodImageView1;
    MMHImageView *goodImageView2;
    MMHImageView *goodImageView3;
    UILabel *productNameLabel;
    UILabel *skuLabel;
}
@property (nonatomic,strong)NSTimer *orderTimer;
@end

@implementation MMHOrderGoodsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - 创建view
-(void)createView{
    // 创建bgView
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.stringTag = @"bgView";
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(190));
    [self addSubview:bgView];

    
    // 创建shopname
    self.shopNameLabel = [[UILabel alloc]init];
    [self customerLabelWithLabel:self.shopNameLabel labelColor:@"黑"];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    
    // 创建收货状态label
    self.orderStylelabel = [[UILabel alloc]init];
    [self customerLabelWithLabel:self.orderStylelabel labelColor:@"浅灰"];
    self.orderStylelabel.textAlignment = NSTextAlignmentRight;
    
    // 创建lineView
    self.topLineView = [[UIView alloc]init];
    self.topLineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self addSubview:self.topLineView];
    
    // 创建goodsView
    self.goodsView = [[UIView alloc]init];
    self.goodsView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.goodsView];

    // goodImage1
    goodImageView1 = [self createProductImageViewWithIndex:0];
    [self.goodsView addSubview:goodImageView1];

    // goodImage2
    goodImageView2 = [self createProductImageViewWithIndex:1];
    [self.goodsView addSubview:goodImageView2];
    
    // goodImage3
    goodImageView3 = [self createProductImageViewWithIndex:2];
    [self.goodsView addSubview:goodImageView3];
    
    productNameLabel = [[UILabel alloc]init];
    productNameLabel.backgroundColor = [UIColor clearColor];
    productNameLabel.stringTag = @"productNameLabel";
    productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    productNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    productNameLabel.numberOfLines = 2;
    [self.goodsView addSubview:productNameLabel];
    
    skuLabel = [[UILabel alloc]init];
    skuLabel.backgroundColor = [UIColor clearColor];
    skuLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    skuLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
    skuLabel.numberOfLines = 1;
    [self.goodsView addSubview:skuLabel];
    
    // 创建line
    self.bottomLineView = [[UIView alloc]init];
    self.bottomLineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self addSubview:self.bottomLineView];

    // 创建label
    self.payLabel = [[UILabel alloc]init];
    [self customerLabelWithLabel:self.payLabel labelColor:@"黑"];
    [self addSubview:self.payLabel];

    // 创建button
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.leftButton.layer.cornerRadius = MMHFloat(5.);
    self.leftButton.backgroundColor = [UIColor whiteColor];
    [self.leftButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    self.leftButton.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
    self.leftButton.layer.borderWidth = .5f;
    [self addSubview:self.leftButton];

    
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.rightButton.layer.cornerRadius = MMHFloat(5.);
    [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
    self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
    self.rightButton.layer.borderWidth = .5f;
    [self addSubview:self.rightButton];

    // 创建共1件
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    self.numberLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
    [self.goodsView addSubview:self.numberLabel];
    
    // 创建checkArrow
    arrowImageView = [[UIImageView alloc]init];
    arrowImageView.image = [UIImage imageNamed:@"tool_arrow"];
    [self.goodsView addSubview:arrowImageView];
    
    // 收货方式的label
    receivingStateTypeLabel = [[UILabel alloc]init];
    receivingStateTypeLabel.textAlignment = NSTextAlignmentLeft;
    receivingStateTypeLabel.backgroundColor = [UIColor clearColor];
    receivingStateTypeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    receivingStateTypeLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
    receivingStateTypeLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:receivingStateTypeLabel];
    
    // 失效时间的label
    self.failureTimeLabel = [[UILabel alloc]init];
    self.failureTimeLabel.backgroundColor = [UIColor clearColor];
    self.failureTimeLabel.textAlignment = NSTextAlignmentLeft;
    self.failureTimeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.failureTimeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.failureTimeLabel];
    
    // 时间imageView
    timeImageView = [[UIImageView alloc]init];
    timeImageView.backgroundColor = [UIColor clearColor];
    timeImageView.image = [UIImage imageNamed:@"orderlist_icon_time"];
    [self addSubview:timeImageView];
}

-(void)setOrderType:(MMHOrderType)orderType{
    _orderType = orderType;
}

-(void)setOrderSingleModel:(MMHOrderSingleModel *)orderSingleModel{
    _orderSingleModel = orderSingleModel;
    orderSingleModel.targetDate = [MMHTool getTargetDateTimeWithTimeInterval:orderSingleModel.remainMinutes];
    
    // 店名
    NSString *shopNameString = orderSingleModel.shopName.length?orderSingleModel.shopName:(orderSingleModel.orderStatus == MMHOrderTypePendingPay?[NSString stringWithFormat:@"订单编号:%@",orderSingleModel.orderNo]:@"妈妈好平台");
    // 待收货
    NSString *receivingStateString;
    
    CGSize shopNameSize = [shopNameString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    self.shopNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(13), shopNameSize.width,shopNameSize.height);
    self.shopNameLabel.text = shopNameString;
    
    self.orderStylelabel.frame = CGRectMake(CGRectGetMaxX(self.shopNameLabel.frame) + MMHFloat(10), self.shopNameLabel.frame.origin.y, kScreenBounds.size.width -  2 *MMHFloat(11) - self.shopNameLabel.bounds.size.width - MMHFloat(10), shopNameSize.height);
    
    // topLineView
    self.topLineView.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.shopNameLabel.frame) + MMHFloat(10), kScreenBounds.size.width - 2 * MMHFloat(11), .5f);
    
    // goodsView
    self.goodsView.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.topLineView.frame), kScreenBounds.size.width - 2 * MMHFloat(11), MMHFloat(100));
    

    // bottomLineView
    self.bottomLineView.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.goodsView.frame), kScreenBounds.size.width - 2 * MMHFloat(11), .5f);
    
    // payString
    CGFloat payPrice = orderSingleModel.payPrice;
    NSString *payString = [NSString stringWithFormat:@"实付: ￥%.2f",payPrice];
    CGSize payContentSize = [payString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width, CGFLOAT_MAX)];
    self.payLabel.text = payString;
    self.payLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(18), MMHFloat(150), payContentSize.height);
    
    // 【leftButton】
    self.leftButton.frame = CGRectMake(kScreenBounds.size.width - 2 *MMHFloat(85) -  2 * MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(10),MMHFloat(85), MMHFloat(30));
    
    // rightButton
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(85) - MMHFloat(11),CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(10), MMHFloat(85), MMHFloat(30));
    
    NSString *goodsNameString = [[orderSingleModel.goodsList lastObject]itemName];
    
    
    if (orderSingleModel.goodsList.count == 1){
        goodImageView1.hidden = NO;
        goodImageView2.hidden = YES;
        goodImageView3.hidden = YES;
        productNameLabel.text = goodsNameString;
        productNameLabel.hidden = NO;
        CGSize productNameSize = [goodsNameString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), CGFLOAT_MAX)];
        if (productNameSize.height <= shopNameSize.height){
            productNameLabel.numberOfLines = 1;
            productNameLabel.frame = CGRectMake(CGRectGetMaxX(goodImageView1.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), shopNameSize.height);
        } else {
            productNameLabel.numberOfLines = 2;
            productNameLabel.frame = CGRectMake(CGRectGetMaxX(goodImageView1.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), 2 * shopNameSize.height);
        }
        skuLabel.hidden = NO;
        CGFloat skuContentHeight = [MMHTool contentofHeight:skuLabel.font];
        skuLabel.frame = CGRectMake(CGRectGetMaxX(goodImageView1.frame) + MMHFloat(10), CGRectGetMaxY(productNameLabel.frame) + MMHFloat(10), productNameLabel.frame.size.width - MMHFloat(30), skuContentHeight);
        
        NSString *skuLabelString = @"";
        for (int i = 0 ; i < [[[orderSingleModel.goodsList lastObject] spec] count];i++){
            NSString *skuItemString = [[[[orderSingleModel.goodsList lastObject] spec] objectAtIndex:i] skuValue];
            if (skuItemString.length){
                skuLabelString = [skuLabelString stringByAppendingString:[NSString stringWithFormat:@"%@ ",skuItemString]];
            }
        }
        skuLabel.text = skuLabelString;
        // 获取到第一个goods
        MMHOrderWithGoodsListModel *orderGoodsModel = [orderSingleModel.goodsList objectAtIndex:0];
        [goodImageView1 updateViewWithImageAtURL:orderGoodsModel.itemPic];
    } else if (orderSingleModel.goodsList.count == 0){
        goodImageView1.hidden = YES;
        skuLabel.hidden = YES;
        goodImageView2.hidden = YES;
        goodImageView3.hidden = YES;
        productNameLabel.hidden = YES;
    } else if (orderSingleModel.goodsList.count >= 3){
        productNameLabel.hidden = YES;
        skuLabel.hidden = YES;
        goodImageView1.hidden = NO;
        goodImageView2.hidden = NO;
        goodImageView3.hidden = NO;
        for (int i = 0; i<3; i++) {
            MMHOrderWithGoodsListModel *orderGoodsModel = [orderSingleModel.goodsList objectAtIndex:i];
            if (i == 0){
                [goodImageView1 updateViewWithImageAtURL:orderGoodsModel.itemPic];
            } else if (i == 1){
                [goodImageView2 updateViewWithImageAtURL:orderGoodsModel.itemPic];
            } else if (i == 2){
                [goodImageView3 updateViewWithImageAtURL:orderGoodsModel.itemPic];
            }
        }
    } else if (orderSingleModel.goodsList.count == 2){
        goodImageView1.hidden = NO;
        goodImageView2.hidden = NO;
        goodImageView3.hidden = YES;
        skuLabel.hidden = YES;
        productNameLabel.hidden = YES;
        for (int i = 0; i < 2; i++) {
            MMHOrderWithGoodsListModel *orderGoodsModel = [orderSingleModel.goodsList objectAtIndex:i];
            if (i == 0){
                [goodImageView1 updateViewWithImageAtURL:orderGoodsModel.itemPic];
            } else if (i == 1){
                [goodImageView2 updateViewWithImageAtURL:orderGoodsModel.itemPic];
            }
        }
    }

    arrowImageView.frame = CGRectMake(self.goodsView.bounds.size.width - MMHFloat(11),(self.goodsView.bounds.size.height - MMHFloat(15))/2., MMHFloat(9), MMHFloat(15));
    
    // 计算长度
    self.numberLabel.frame = CGRectMake(CGRectGetMinX(arrowImageView.frame) - MMHFloat(60) - MMHFloat(10), (MMHFloat(100) - shopNameSize.height) / 2., MMHFloat(60), shopNameSize.height);
    self.numberLabel.text = [NSString stringWithFormat:@"共%lu件",(unsigned long)orderSingleModel.goodsList.count];
    
    
    if (self.orderType == MMHOrderTypeBeRefund){
        self.failureTimeLabel.hidden = YES;
        timeImageView.hidden = YES;
        receivingStateTypeLabel.hidden = YES;
        receivingStateString = @"退款退货";
        [self.leftButton setTitle:@"追踪退款退货" forState:UIControlStateNormal];
        self.rightButton.hidden = YES;
        self.leftButton.hidden = NO;
        self.leftButton.frame = self.rightButton.frame;
        // 计算文字长度
        CGSize contentOfSizeWithBtn = [self.leftButton.titleLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        
        // 【leftButton】
        self.leftButton.frame = CGRectMake(kScreenBounds.size.width  - (contentOfSizeWithBtn.width + 2 *MMHFloat(10)) -   MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(10),(contentOfSizeWithBtn.width + 2 *MMHFloat(10)), MMHFloat(30));
    } else {
        if (orderSingleModel.orderStatus == MMHOrderTypeFinish){                                            // 已完成
            receivingStateString = @"已完成";
            self.leftButton.hidden = YES;
            self.rightButton.hidden = NO;
            [self.rightButton setTitle:@"再次购买" forState:UIControlStateNormal];
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
            receivingStateTypeLabel.hidden = YES;
        } else if (orderSingleModel.orderStatus == MMHOrderTypeUnable){                                     // 已失效
            receivingStateString = @"已失效";
            self.rightButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.leftButton.frame = self.rightButton.frame;
            [self.leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
            receivingStateTypeLabel.hidden = YES;
        } else if (orderSingleModel.orderStatus == MMHOrderTypeCancel){                                     // 已取消
            receivingStateString = @"已取消";
            self.leftButton.frame = self.rightButton.frame;
            self.rightButton.hidden = YES;
            self.leftButton.hidden = NO;
            [self.leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
            receivingStateTypeLabel.hidden = YES;
        } else if (orderSingleModel.orderStatus == MMHOrderTypePendingPay){                                 // 【待付款】
            orderSingleModel.remainMinutes--;
            
            if (orderSingleModel.remainMinutes > 0){            // 倒计时
                self.failureTimeLabel.hidden = NO;
                timeImageView.hidden = NO;
                self.payLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(9), MMHFloat(150), payContentSize.height);
                
                // 创建clockImage
                timeImageView.frame = CGRectMake(self.payLabel.frame.origin.x, CGRectGetMaxY(self.payLabel.frame) + MMHFloat(6), MMHFloat(12), MMHFloat(12));
                
                CGSize timeSize = [[NSString stringWithFormat:@"123"] sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(MMHFloat(100), CGFLOAT_MAX)];
                
                self.orderTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeCountdown:) userInfo:nil repeats:YES];
                self.failureTimeLabel.frame = CGRectMake(CGRectGetMaxX(timeImageView.frame) + MMHFloat(5), CGRectGetMaxY(self.payLabel.frame)+ MMHFloat(4), 200, timeSize.height);
                
                
                receivingStateString = @"待付款";
                [self.rightButton setTitle:@"立即付款" forState:UIControlStateNormal];
                self.leftButton.hidden = YES;
                self.rightButton.hidden = NO;
                receivingStateTypeLabel.hidden = YES;
                [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
                self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
                self.rightButton.enabled = YES;
            } else {
                self.failureTimeLabel.text = @"订单已失效,请重新生成。";
                [self.rightButton setTitle:@"订单失效" forState:UIControlStateNormal];
                [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"失效"] forState:UIControlStateNormal];
                self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"失效"].CGColor;
                self.rightButton.enabled = NO;
                self.leftButton.hidden = YES;
                self.rightButton.hidden = NO;
            }
            
        } else if (orderSingleModel.orderStatus == MMHOrderTypeBeShipped){                                  // 【待发货】
            receivingStateString = @"待发货";
            self.rightButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.leftButton.frame = self.rightButton.frame;
            [self.leftButton setTitle:@"提醒发货" forState:UIControlStateNormal];
            receivingStateTypeLabel.hidden = YES;
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
        } else if (orderSingleModel.orderStatus == MMHOrderTypeBeReceipt){                                  // 【待收货】
            receivingStateString = @"待收货";
            self.leftButton.enabled = YES;
            self.leftButton.enabled = YES;
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
            [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
            self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
            
            if (orderSingleModel.deliveryId == MMHReceivingStateTypeDelivery){          // 货到付款
                [self.leftButton setTitle:@"查看门店信息" forState:UIControlStateNormal];
                receivingStateTypeLabel.text = @"(门店配送)";
                receivingStateTypeLabel.hidden = NO;
            } else if (orderSingleModel.deliveryId == MMHReceivingStateTypeSelf) {       // 自提
                [self.leftButton setTitle:@"查看门店信息" forState:UIControlStateNormal];
                receivingStateTypeLabel.text = @"(上门自提)";
                receivingStateTypeLabel.hidden = NO;
            } else if (orderSingleModel.deliveryId == MMHReceivingStateTypeExpress){      // 物流
                receivingStateTypeLabel.text = @"";
                [self.leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
                receivingStateTypeLabel.hidden = YES;
            }
            [self.rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
            // 计算文字长度
            CGSize contentOfSizeWithBtn = [self.leftButton.titleLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            
            // 【leftButton】
            self.leftButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(85) - (contentOfSizeWithBtn.width + 2 *MMHFloat(10)) -  2 * MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(10),(contentOfSizeWithBtn.width + 2 *MMHFloat(10)), MMHFloat(30));
            self.leftButton.hidden = NO;
            self.rightButton.hidden = NO;
            
        } else if (orderSingleModel.orderStatus == MMHOrderTypeBeEvaluation){                               // 【待评价】
            receivingStateString = @"待评价";
            [self.rightButton setTitle:@"评价晒单" forState:UIControlStateNormal];
            self.leftButton.hidden = YES;
            self.rightButton.hidden = NO;
            receivingStateTypeLabel.hidden = YES;
            
            [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
            self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
            self.rightButton.enabled = YES;
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
            
        } else if (orderSingleModel.orderStatus == MMHOrderTypeBeRefund){                                   // 【待退货】
            receivingStateTypeLabel.hidden = YES;
            receivingStateString = @"待退货";
            [self.leftButton setTitle:@"追踪退款退货" forState:UIControlStateNormal];
            self.rightButton.hidden = YES;
            self.leftButton.hidden = NO;
            self.leftButton.frame = self.rightButton.frame;
            // 计算文字长度
            CGSize contentOfSizeWithBtn = [self.leftButton.titleLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            
            // 【leftButton】
            self.leftButton.frame = CGRectMake(kScreenBounds.size.width  - (contentOfSizeWithBtn.width + 2 *MMHFloat(10)) -   MMHFloat(11), CGRectGetMaxY(self.bottomLineView.frame) + MMHFloat(10),(contentOfSizeWithBtn.width + 2 *MMHFloat(10)), MMHFloat(30));
            self.failureTimeLabel.hidden = YES;
            timeImageView.hidden = YES;
        }
    }
    
    self.orderStylelabel.text = receivingStateString;
    
    CGSize receivingStateSize = [receivingStateString sizeWithCalcFont:self.orderStylelabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.orderStylelabel.font])];
    self.orderStylelabel.orgin_x = kScreenBounds.size.width - MMHFloat(11) - receivingStateSize.width;
    self.orderStylelabel.size_width = receivingStateSize.width;
    self.shopNameLabel.size_width = (kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(5) - receivingStateSize.width);

    // 后期修改
    if (receivingStateTypeLabel.text.length && receivingStateTypeLabel.hidden == NO){
        CGSize receivingStateTypeLabelSize = [receivingStateTypeLabel.text sizeWithCalcFont:receivingStateTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:receivingStateTypeLabel.font])];
        self.shopNameLabel.size_width = (kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(5) - receivingStateSize.width - receivingStateTypeLabelSize.width);
        receivingStateTypeLabel.frame = CGRectMake(CGRectGetMaxX(self.shopNameLabel.frame), self.shopNameLabel.frame.origin.y, receivingStateTypeLabelSize.width, self.shopNameLabel.bounds.size.height);
        
    }
}

#pragma mark 倒计时
-(void)timeCountdown:(NSTimer *)timer {
    self.orderSingleModel.remainMinutes--;
    if (self.orderSingleModel.orderStatus == MMHOrderTypePendingPay){
        if (self.orderSingleModel.remainMinutes <= 0){
            self.failureTimeLabel.text = @"订单已失效,请重新生成。";
            [self.rightButton setTitle:@"订单失效" forState:UIControlStateNormal];
            [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"失效"] forState:UIControlStateNormal];
            self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"失效"].CGColor;
            self.rightButton.enabled = NO;
        } else {
            self.rightButton.enabled = YES;
            [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
            self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
            self.failureTimeLabel.text = [MMHTool getCountdownWithTargetDate:self.orderSingleModel.targetDate];
        }
    }
}

#pragma mark - customGoodsImage
-(MMHImageView *)createProductImageViewWithIndex:(NSInteger)index{
    MMHImageView *productImageView = [[MMHImageView alloc]init];
    productImageView.backgroundColor = [UIColor clearColor];
    productImageView.stringTag = [NSString stringWithFormat:@"productImageView%li",(long)index];
    productImageView.layer.masksToBounds = YES;
    [productImageView.layer setCornerRadius:MMHFloat(6.)];
    productImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    productImageView.layer.borderWidth = .5f;
    productImageView.frame = CGRectMake(0 + index * (MMHFloat(10) + MMHFloat(70)), MMHFloat(100 - 70) / 2., MMHFloat(70), MMHFloat(70));
    return productImageView;
}

#pragma mark - customerLabel
-(void)customerLabelWithLabel:(UILabel *)label labelColor:(NSString *)colorName{
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    label.textColor = [UIColor colorWithCustomerName:colorName];
    [self addSubview:label];
}

@end
