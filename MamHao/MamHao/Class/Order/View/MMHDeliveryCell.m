//
//  MMHDeliveryCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHDeliveryCell.h"

@interface MMHDeliveryCell(){
//    UIImageView *phoneImageView;
}
@end

@implementation MMHDeliveryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark createView
-(void)createView{
    // 1. 创建shopNameLabel
    self.shopNameLabel = [[UILabel alloc]init];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    self.shopNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.shopNameLabel.textAlignment = NSTextAlignmentLeft;
    self.shopNameLabel.numberOfLines = 0;
    self.shopNameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.shopNameLabel];
    
    // 2. 创建门店地址
    self.shopAddressLabel = [[UILabel alloc]init];
    self.shopAddressLabel.backgroundColor = [UIColor clearColor];
    self.shopAddressLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.shopAddressLabel.textAlignment = NSTextAlignmentLeft;
    self.shopAddressLabel.numberOfLines = 0;
    self.shopAddressLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview: self.shopAddressLabel];
    
    // 3. 创建送货人员
    self.shopDeliveryLabel = [[UILabel alloc]init];
    self.shopDeliveryLabel.backgroundColor = [UIColor clearColor];
    self.shopDeliveryLabel.font =[UIFont fontWithCustomerSizeName:@"小正文"];
    self.shopDeliveryLabel.textAlignment = NSTextAlignmentLeft;
    self.shopAddressLabel.numberOfLines = 1;
    self.shopDeliveryLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.shopDeliveryLabel];
    
    // 创建电话button
    self.shopDeliveryPhoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.shopDeliveryPhoneButton.backgroundColor = [UIColor clearColor];
    self.shopDeliveryPhoneButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.shopDeliveryPhoneButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
    [self addSubview:self.shopDeliveryPhoneButton];
    
    // 创建电话image
//    phoneImageView = [[UIImageView alloc]init];
//    phoneImageView.backgroundColor = [UIColor clearColor];
//    phoneImageView.image = [UIImage imageNamed:@"contact_icon_smallcall"];
//    phoneImageView.stringTag = @"phoneImageView";
//    [self addSubview:phoneImageView];
    
    // 创建到达时间label
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    self.timeLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    self.timeLabel.numberOfLines = 1;
    [self addSubview:self.timeLabel];
}

-(void)setDistributionInfo:(MMHDistributionModel *)distributionInfo{
    _distributionInfo = distributionInfo;
    // 商店名称
    NSString *shopNameString = [NSString stringWithFormat:@"门店名称:%@",distributionInfo.shopName];
    CGSize shopNameContentSize = [shopNameString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * MMHFloat(11), CGFLOAT_MAX)];
    self.shopNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(15), kScreenWidth - 2 * MMHFloat(11), shopNameContentSize.height);
    self.shopNameLabel.text = shopNameString;
    
    // 创建门店地址
    NSString *shopAddressString = [NSString stringWithFormat:@"门店地址:%@",distributionInfo.addr];
    CGSize shopAddressContentSize = [shopAddressString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * MMHFloat(11), CGFLOAT_MAX)];
    self.shopAddressLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.shopNameLabel.frame) + MMHFloat(10), kScreenWidth - 2 * MMHFloat(11), shopAddressContentSize.height);
    self.shopAddressLabel.text = shopAddressString;
    
    // 创建送货人员
    CGFloat numberHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    
    NSString *distributionString = [NSString stringWithFormat:@"送货人员:%@",distributionInfo.deliveryStaff];
    CGSize diliveryStaffSize = [distributionString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, numberHeight)];
    self.shopDeliveryLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.shopAddressLabel.frame) + MMHFloat(10), diliveryStaffSize.width, numberHeight);
    self.shopDeliveryLabel.text = distributionString;
    
    // 创建电话

    CGSize phoneNumberSize = [distributionInfo.phone sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, numberHeight)];
    [self.shopDeliveryPhoneButton setTitle:[NSString stringWithFormat:@"%@",distributionInfo.phone] forState:UIControlStateNormal];
    self.shopDeliveryPhoneButton.frame = CGRectMake(CGRectGetMaxX(self.shopDeliveryLabel.frame) + MMHFloat(5), self.shopDeliveryLabel.frame.origin.y,phoneNumberSize.width , numberHeight);
    
    // 创建电话icon
//    phoneImageView.frame = CGRectMake(CGRectGetMaxX(self.shopDeliveryPhoneButton.frame) + MMHFloat(3), self.shopDeliveryPhoneButton.frame.origin.y, MMHFloat(15), MMHFloat(16));
    
    // 创建到达时间
    NSString *timeString = [NSString stringWithFormat:@"预计到达时间:%@",distributionInfo.arriveTime];
    self.timeLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.shopDeliveryLabel.frame) + MMHFloat(10), kScreenWidth - 2 * MMHFloat(11), numberHeight);
    self.timeLabel.text = timeString;
}



+(CGFloat)cellHeightWithDistributionInfo:(MMHDistributionModel *)distributionInfo{
    CGFloat cellHeight = 0;
    NSString *shopNameString = [NSString stringWithFormat:@"门店名称:%@",distributionInfo.shopName];
    CGSize shopNameContentSize = [shopNameString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * MMHFloat(11), CGFLOAT_MAX)];
    cellHeight += shopNameContentSize.height + MMHFloat(15);
    
    
    // 创建门店地址
    NSString *shopAddressString = [NSString stringWithFormat:@"门店地址:%@",distributionInfo.addr];
    CGSize shopAddressContentSize = [shopAddressString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * MMHFloat(11), CGFLOAT_MAX)];
    cellHeight += shopAddressContentSize.height + MMHFloat(15);
    
    
    // 创建送货人员
    CGFloat numberHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += numberHeight + MMHFloat(15);
    
    
    // 创建到达时间
    cellHeight += numberHeight + MMHFloat(15) ;
    return cellHeight;
}


@end
