//
//  MMHExpressTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressTableViewCell.h"

@interface MMHExpressTableViewCell(){
    UIView *bgView;
    UILabel *expressTypeLabel;
    UILabel *expressNumLabel;
    UILabel *expressFixedLabel;
    UILabel *expressContentLabel;
    UILabel *expressTimeLabel;
    UIView *lineView2;
    UIImageView *hltImageView;
    UIImageView *arrowImageView;
}

@end

@implementation MMHExpressTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor =[UIColor clearColor];
    headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(90));
    [self addSubview:headerView];
    
    expressTypeLabel = [[UILabel alloc]init];
    expressTypeLabel.backgroundColor = [UIColor clearColor];
    expressTypeLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    expressTypeLabel.textAlignment = NSTextAlignmentLeft;
    expressTypeLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [headerView addSubview:expressTypeLabel];
    
    expressNumLabel = [[UILabel alloc]init];
    expressNumLabel.backgroundColor = [UIColor clearColor];
    expressNumLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    expressNumLabel.textAlignment = NSTextAlignmentRight;
    expressNumLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [headerView addSubview:expressNumLabel];
    
    // line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(MMHFloat(10), MMHFloat(50), kScreenBounds.size.width - 2 * MMHFloat(10), .5f);
    [headerView addSubview:lineView];
    
    // 物流详情
    expressFixedLabel = [[UILabel alloc]init];
    expressFixedLabel.backgroundColor = [UIColor clearColor];
    expressFixedLabel.textAlignment = NSTextAlignmentLeft;
    expressFixedLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    expressFixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    expressFixedLabel.text = @"物流详情";
    [headerView addSubview:expressFixedLabel];

    // 创建line
    lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = [UIColor colorWithRed:193/256. green:193/256. blue:193/256. alpha:1];
    [headerView addSubview:lineView2];

    
    hltImageView = [[UIImageView alloc]init];
    hltImageView.backgroundColor = [UIColor clearColor];
    hltImageView.image = [UIImage imageNamed:@"logistics_icon_point"];
    [headerView addSubview:hltImageView];

    // 创建物流信息
    expressContentLabel = [[UILabel alloc]init];
    expressContentLabel.backgroundColor = [UIColor clearColor];
    expressContentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressContentLabel.textAlignment = NSTextAlignmentLeft;
    expressContentLabel.numberOfLines = 0;
    expressContentLabel.textColor = [UIColor colorWithCustomerName:@"快递"];
    [headerView addSubview:expressContentLabel];
    
    // 创建物流时间
    expressTimeLabel = [[UILabel alloc]init];
    expressTimeLabel.backgroundColor = [UIColor clearColor];
    expressTimeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    expressTimeLabel.textAlignment = NSTextAlignmentLeft;
    expressTimeLabel.textColor = [UIColor colorWithCustomerName:@"快递"];
    [headerView addSubview:expressTimeLabel];
    
    arrowImageView = [[UIImageView alloc]init];
    arrowImageView.backgroundColor = [UIColor clearColor];
    arrowImageView.image = [UIImage imageNamed:@"icon_go_mid"];
    [headerView addSubview:arrowImageView];
}

-(void)setTransferExpressModel:(MMHExpressModel *)transferExpressModel{
    _transferExpressModel = transferExpressModel;
//    MMHExpressContentModel *contentModel = [transferExpressModel.data lastObject];
    
    // 1. 顶部信息
    expressTypeLabel.text = [NSString stringWithFormat:@"物流公司:%@",transferExpressModel.platformName] ;
    expressNumLabel.text = [NSString stringWithFormat:@"物流单号:%@",transferExpressModel.waybillNumber];
    
    CGSize contentOfSize = [expressTypeLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    expressTypeLabel.frame = CGRectMake(MMHFloat(11),0, contentOfSize.width, MMHFloat(50));
    
    // 物流编号
    expressNumLabel.frame = CGRectMake(CGRectGetMaxX(expressTypeLabel.frame) + MMHFloat(5), 0, kScreenBounds.size.width - 2 * MMHFloat(11) - contentOfSize.width - MMHFloat(5), MMHFloat(50));
    
    // 物流详情fixed
    expressFixedLabel.frame = CGRectMake(MMHFloat(10),MMHFloat(50) + MMHFloat(20), 100, contentOfSize.height);
    
//    if (!self.transferExpressModel.data.count){
//        expressContentLabel.text = @"暂无物流信息";
//        expressTimeLabel.hidden = YES;
//    } else {
//        expressContentLabel.text = contentModel.desc;
//        expressTimeLabel.text = contentModel.time;
//    }
    // 计算文字长度
    CGSize expressContentSize = [expressContentLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(11 +56), CGFLOAT_MAX)];
    expressContentLabel.frame = CGRectMake(MMHFloat(56.), CGRectGetMaxY(expressFixedLabel.frame) + MMHFloat(20),kScreenBounds.size.width - MMHFloat(11 + 56 + 20), expressContentSize.height);
    
    CGFloat timeLabelHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
    expressTimeLabel.frame = CGRectMake(MMHFloat(56.), CGRectGetMaxY(expressContentLabel.frame)+ MMHFloat(8), kScreenBounds.size.width - MMHFloat(11 + 56 + 20), timeLabelHeight);
    
    hltImageView.frame = CGRectMake(MMHFloat(20), CGRectGetMaxY(expressFixedLabel.frame) + MMHFloat(20), MMHFloat(16), MMHFloat(16));
    
    // lineView
    CGFloat lineView2Height = CGRectGetMaxY(expressTimeLabel.frame) + MMHFloat(20) - CGRectGetMinY(expressContentLabel.frame) - MMHFloat(12);
    lineView2.frame = CGRectMake(MMHFloat(27.5), CGRectGetMaxY(expressFixedLabel.frame) + MMHFloat(24.5), MMHFloat(2), lineView2Height);
    
    // arrowImageView
    arrowImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11 + 10),expressContentLabel.frame.origin.y + ( expressContentSize.height + MMHFloat(8) + MMHFloat(15) - MMHFloat(18)) / 2. , MMHFloat(10), MMHFloat(18));
}

+(CGFloat)cellHeightWithExpressModel:(MMHExpressModel*)expressModel{
    CGSize expressContentSize = [@"我是物流" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(11 + 56 + 20), CGFLOAT_MAX)];
    
    CGSize contentOfSize = [@"物流详情" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    
    CGFloat cellHeight = 0;
    CGFloat timeLabelHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight = MMHFloat(50) + MMHFloat(20) * 2 + contentOfSize.height + expressContentSize.height + timeLabelHeight + MMHFloat(20);
    return cellHeight;
}

@end
