//
//  MMHAddressSelectedTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHAddressSingleModel.h"
typedef void(^editBlock)(MMHAddressSingleModel *addressSingleModel);

typedef void(^deleteBlock)(MMHAddressSingleModel *addressSingleModel);

typedef void(^normolBtnClickBlock)(MMHAddressSingleModel *addressSingleModel);

@interface MMHAddressSelectedTableViewCell : UITableViewCell {
}

-(void)setChecked:(BOOL)checked;

@property (nonatomic,assign)    BOOL isChecked;
@property (nonatomic,strong)UIImageView *checkImageView;
// 编辑
@property (nonatomic,copy)editBlock editBlock;
// 删除
@property (nonatomic,copy)deleteBlock deleteBlock;
// 默认
@property (nonatomic,copy)normolBtnClickBlock normolBtnClickBlock;
// model
@property (nonatomic,strong)MMHAddressSingleModel *addressSingleModel;

@end
