//
//  MMHOrderListShopInfoCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderListShopInfoCell.h"

@interface MMHOrderListShopInfoCell()
@property (nonatomic,strong)UILabel *shopNameFixedLabel;
@property (nonatomic,strong)UILabel *shopNameDymicLabel;
@property (nonatomic,strong)UILabel *shopAddressFixedLabel;
@property (nonatomic,strong)UILabel *shopAddressDymicLable;
@property (nonatomic,strong)UILabel *shopWorkTimeFixedLabel;
@property (nonatomic,strong)UILabel *shopWorkTimeDymicLabel;
@property (nonatomic,strong)UILabel *shopHolidayTimeDymicLabel;
@property (nonatomic,strong)UILabel *shopPhoneFixedLabel;

@end

@implementation MMHOrderListShopInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark 创建view
-(void)createView{
    // 1. 创建门店名称
    self.shopNameFixedLabel = [self createCustomLabel];
    self.shopNameFixedLabel.text = @"门店名称: ";
    [self addSubview:self.shopNameFixedLabel];
    self.shopNameDymicLabel = [self createCustomLabel];
    self.shopNameDymicLabel.numberOfLines = 0;
    [self addSubview:self.shopNameDymicLabel];
    
    // 2. 创建门店地址
    self.shopAddressFixedLabel = [self createCustomLabel];
    self.shopAddressFixedLabel.text = @"门店地址: ";
    [self addSubview:self.shopAddressFixedLabel];
    self.shopAddressDymicLable = [self createCustomLabel];
    self.shopAddressDymicLable.numberOfLines = 0;
    [self addSubview:self.shopAddressDymicLable];
    
    // 3. 创建营业时间
    self.shopWorkTimeFixedLabel = [self createCustomLabel];
    self.shopWorkTimeFixedLabel.text = @"营业时间: ";
    [self addSubview:self.shopWorkTimeFixedLabel];
    self.shopWorkTimeDymicLabel = [self createCustomLabel];
    self.shopWorkTimeDymicLabel.numberOfLines = 0;
    [self addSubview:self.shopWorkTimeDymicLabel];
    
    // 4. 创建休息时间
    self.shopHolidayTimeDymicLabel = [self createCustomLabel];
    self.shopHolidayTimeDymicLabel.numberOfLines = 0;
    [self addSubview:self.shopHolidayTimeDymicLabel];
    
    // 5. 创建联系方式
    self.shopPhoneFixedLabel = [self createCustomLabel];
    self.shopPhoneFixedLabel.text = @"联系方式: ";
    [self addSubview:self.shopPhoneFixedLabel];
    
    self.phoneNumberButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.phoneNumberButton.backgroundColor = [UIColor clearColor];
    [self.phoneNumberButton setTitleColor:[UIColor colorWithCustomerName:@"蓝"] forState:UIControlStateNormal];
    self.phoneNumberButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.phoneNumberButton];
}

-(void)setViewWidth:(CGFloat)viewWidth{
    _viewWidth = viewWidth;
}

-(void)setTransferSinceTheMentionAddressModel:(MMHsinceTheMentionAddressModel *)transferSinceTheMentionAddressModel{
    _transferSinceTheMentionAddressModel = transferSinceTheMentionAddressModel;
    CGFloat origin_x = self.shopInfoCellUsingClass == MMHShopInfoCellUsingClass1Refund? MMHFloat(11) + MMHFloat(10):MMHFloat(11);

    // 门店名称
    CGSize shopNameContentSize = [self.shopNameFixedLabel.text sizeWithCalcFont:self.shopNameFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.shopNameFixedLabel.font])];
    self.shopNameFixedLabel.frame = CGRectMake(origin_x, MMHFloat(15), shopNameContentSize.width, [MMHTool contentofHeight:self.shopNameFixedLabel.font]);
    self.shopNameDymicLabel.text = transferSinceTheMentionAddressModel.shopName;
    CGSize shopNameDymicContentSize = [self.shopNameDymicLabel.text sizeWithCalcFont:self.shopNameDymicLabel.font constrainedToSize:CGSizeMake(self.viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width, CGFLOAT_MAX)];
    self.shopNameDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.shopNameFixedLabel.frame), self.shopNameFixedLabel.orgin_y, self.viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width, shopNameDymicContentSize.height);
    
    // 门店地址
    self.shopAddressFixedLabel.frame = CGRectMake(origin_x, CGRectGetMaxY(self.shopNameDymicLabel.frame) + MMHFloat(10), shopNameContentSize.width, [MMHTool contentofHeight:self.shopAddressFixedLabel.font]);
    self.shopAddressDymicLable.text = transferSinceTheMentionAddressModel.addr;
    CGSize shopAddressDymicSize = [self.shopAddressDymicLable.text sizeWithCalcFont:self.shopAddressDymicLable.font constrainedToSize:CGSizeMake(self.shopNameDymicLabel.size_width, CGFLOAT_MAX)];
    self.shopAddressDymicLable.frame = CGRectMake(CGRectGetMaxX(self.shopAddressFixedLabel.frame), self.shopAddressFixedLabel.orgin_y, self.shopNameDymicLabel.size_width, shopAddressDymicSize.height);
    
    // 营业时间
    self.shopWorkTimeFixedLabel.frame = CGRectMake(origin_x, CGRectGetMaxY(self.shopAddressDymicLable.frame) + MMHFloat(10),    self.shopNameFixedLabel.size_width, self.shopNameFixedLabel.size_height);
    self.shopWorkTimeDymicLabel.text = transferSinceTheMentionAddressModel.workTime.length?[NSString stringWithFormat:@"工作日:%@",transferSinceTheMentionAddressModel.workTime]:@"未获取到营业时间信息";
    CGSize shopWorkTimeDymicSize = [self.shopWorkTimeDymicLabel.text sizeWithCalcFont:self.shopWorkTimeDymicLabel.font constrainedToSize:CGSizeMake(self.shopNameDymicLabel.size_width, CGFLOAT_MAX)];
    self.shopWorkTimeDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.shopWorkTimeFixedLabel.frame), self.shopWorkTimeFixedLabel.orgin_y, self.shopNameDymicLabel.size_width, shopWorkTimeDymicSize.height);
    
    // 休息时间
    self.shopHolidayTimeDymicLabel.text = transferSinceTheMentionAddressModel.holiday.length?[NSString stringWithFormat:@"节假日:%@",transferSinceTheMentionAddressModel.holiday]:@"未获取到营业时间信息";
    CGSize holidayTimeDymicSize = [self.shopHolidayTimeDymicLabel.text sizeWithCalcFont:self.shopHolidayTimeDymicLabel.font constrainedToSize:CGSizeMake(self.shopNameDymicLabel.size_width, CGFLOAT_MAX)];
    self.shopHolidayTimeDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.shopWorkTimeFixedLabel.frame), CGRectGetMaxY(self.shopWorkTimeDymicLabel.frame) + MMHFloat(10), self.shopWorkTimeDymicLabel.size_width, holidayTimeDymicSize.height);
    
    // 联系方式
    self.shopPhoneFixedLabel.frame = CGRectMake(origin_x, CGRectGetMaxY(self.shopHolidayTimeDymicLabel.frame) + MMHFloat(10), self.shopNameFixedLabel.size_width, [MMHTool contentofHeight:self.shopPhoneFixedLabel.font]);
    CGSize phoneNumberSize = [transferSinceTheMentionAddressModel.telephone sizeWithCalcFont:self.phoneNumberButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.phoneNumberButton.titleLabel.font])];
    self.phoneNumberButton.frame = CGRectMake(CGRectGetMaxX(self.shopPhoneFixedLabel.frame), self.shopPhoneFixedLabel.orgin_y, phoneNumberSize.width, [MMHTool contentofHeight:self.shopPhoneFixedLabel.font]);
    [self.phoneNumberButton setTitle:transferSinceTheMentionAddressModel.telephone forState:UIControlStateNormal];
}


#pragma mark - CreateView
-(UILabel *)createCustomLabel{
    UILabel *tempLabel = [[UILabel alloc]init];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    tempLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    return tempLabel;
}

+(CGFloat)heightForCellWithShopInfo:(MMHsinceTheMentionAddressModel *)sinceTheMentionAddressModel UsingClass:(MMHShopInfoCellUsingClass1)shopInfoCellUsingClass{
    CGFloat cellHeight = 0;
    CGFloat viewWidth = kScreenBounds.size.width - 70.0f;
    CGSize shopNameContentSize = [@"门店名称: " sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    
    CGSize shopNameDymicContentSize = [sinceTheMentionAddressModel.shopName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width, CGFLOAT_MAX)];
    cellHeight += shopNameDymicContentSize.height;
    
    CGSize shopAddressDymicSize = [sinceTheMentionAddressModel.addr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake((viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width), CGFLOAT_MAX)];
    cellHeight += shopAddressDymicSize.height;
    
    NSString *shopWorkTimeDymicString = sinceTheMentionAddressModel.workTime.length?[NSString stringWithFormat:@"工作日:%@",sinceTheMentionAddressModel.workTime]:@"未获取到营业时间信息";
    CGSize shopWorkTimeDymicSize = [shopWorkTimeDymicString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake((viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width), CGFLOAT_MAX)];
    cellHeight += shopWorkTimeDymicSize.height;

    NSString *shopHolidayTimeDymicString = sinceTheMentionAddressModel.holiday.length?[NSString stringWithFormat:@"节假日:%@",sinceTheMentionAddressModel.holiday]:@"未获取到营业时间信息";
    CGSize holidayTimeDymicSize = [shopHolidayTimeDymicString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake((viewWidth - 2 * MMHFloat(11) - shopNameContentSize.width), CGFLOAT_MAX)];
    cellHeight += holidayTimeDymicSize.height;
    
    cellHeight += [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += MMHFloat(15);
    cellHeight += MMHFloat(10) * 4;
    cellHeight += MMHFloat(15);
    return cellHeight;
}

@end
