//
//  MMHOrderListShopInfoCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHsinceTheMentionAddressModel.h"

typedef NS_ENUM(NSInteger, MMHShopInfoCellUsingClass1) {
    MMHShopInfoCellUsingClass1Normol = 0,                /**< 默认使用*/
    MMHShopInfoCellUsingClass1Refund,                    /**< 退款*/
    
};

@interface MMHOrderListShopInfoCell : UITableViewCell

@property (nonatomic,assign)CGFloat viewWidth;              /**< 视图宽度*/
@property (nonatomic,strong)MMHsinceTheMentionAddressModel *transferSinceTheMentionAddressModel;
@property (nonatomic,strong)UIButton *phoneNumberButton;    /**< 电话按钮*/
@property (nonatomic,assign)MMHShopInfoCellUsingClass1 shopInfoCellUsingClass;

+(CGFloat)heightForCellWithShopInfo:(MMHsinceTheMentionAddressModel *)sinceTheMentionAddressModel UsingClass:(MMHShopInfoCellUsingClass1)shopInfoCellUsingClass;
@end
