//
//  MMHCustomerSlider.m
//  MamHao
//
//  Created by SmartMin on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCustomerSlider.h"

#define LEFT_OFFSET 25
#define RIGHT_OFFSET 25
#define TITLE_SELECTED_DISTANCE 5
#define TITLE_FADE_ALPHA .5f
#define TITLE_FONT [UIFont fontWithName:@"Optima" size:14]
#define TITLE_SHADOW_COLOR [UIColor lightGrayColor]
#define TITLE_COLOR [UIColor blackColor]

@interface MMHCustomerSlider(){
    UIButton *handler;
    CGPoint diffPoint;
    NSArray *titlesArr;
    CGFloat oneSlotSize;
}

@end

@implementation MMHCustomerSlider

-(id)initWithFrame:(CGRect)frame items:(NSArray *)items{
    if (self = [super initWithFrame:frame]){
        titlesArr = [[NSArray alloc] initWithArray:items];
        [self createView];                              // 创建view
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundColor = [UIColor clearColor];            // background
    self.progressColor = [UIColor redColor];                // progressColor
    
    // 添加手势
    UITapGestureRecognizer *tapgestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(itemSelected:)];
    [self addGestureRecognizer:tapgestureRecognizer];
    
    handler = [UIButton buttonWithType:UIButtonTypeCustom];
    handler.frame = CGRectMake(LEFT_OFFSET, MMHFloat(5), MMHFloat(18), MMHFloat(18));
    handler.adjustsImageWhenHighlighted = NO;
    handler.backgroundColor = [UIColor clearColor];
    [handler setImage:[UIImage imageNamed:@"icon_returns_progress"] forState:UIControlStateNormal];
    handler.center = CGPointMake(handler.center.x-(handler.frame.size.width/2.f), self.frame.size.height - MMHFloat(33));
    [handler addTarget:self action:@selector(touchDown:withEvent:) forControlEvents:UIControlEventTouchDown];
    [handler addTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    [handler addTarget:self action:@selector(touchMove:withEvent:) forControlEvents: UIControlEventTouchDragOutside | UIControlEventTouchDragInside];
    [self addSubview:handler];
    
    // 创建itemsLabel
    NSString *titleString;
    UILabel *tempLabel;
    oneSlotSize = 1.f * (self.frame.size.width - LEFT_OFFSET - RIGHT_OFFSET - 1) / (titlesArr.count - 1);
    for (int i = 0 ; i < titlesArr.count;i++){
        titleString = [titlesArr objectAtIndex:i];
        
        // 创建titleLabel
        tempLabel = [[UILabel alloc]init];
        tempLabel.frame = CGRectMake(0, 80, oneSlotSize, MMHFloat(25));
        tempLabel.text = titleString;
        tempLabel.font = TITLE_FONT;
        tempLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        tempLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        tempLabel.adjustsFontSizeToFitWidth = YES;
        tempLabel.minimumScaleFactor = 8;
        tempLabel.textAlignment = NSTextAlignmentCenter;
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.tag = i + 50;
        
        if (i){
            tempLabel.alpha = TITLE_FADE_ALPHA;
        }
        
        tempLabel.center = [self getCenterPointForIndex:i];
        [self addSubview:tempLabel];
    }
}

#pragma mark - drawRect
-(void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // 主要路径 main
    CGContextSetFillColorWithColor(context, self.progressColor.CGColor);
    CGContextFillRect(context, CGRectMake(LEFT_OFFSET, rect.size.height - MMHFloat(35), rect.size.width - RIGHT_OFFSET - LEFT_OFFSET, MMHFloat(3)));
    CGContextSaveGState(context);
    
    CGPoint centerPoint;
    for (int i = 0 ; i < titlesArr.count;i++){
        centerPoint = [self getCenterPointForIndex:i];
        
        //画圆
        CGContextSetFillColorWithColor(context, self.progressColor.CGColor);
        CGContextFillEllipseInRect(context, CGRectMake(centerPoint.x - MMHFloat(5),rect.size.height - MMHFloat(38.5), MMHFloat(10), MMHFloat(10)));
    }
    
}

#pragma mark - actionClick

-(void)touchDown:(UIButton *)button withEvent:(UIEvent *)event{
    CGPoint currPoint = [[[event allTouches] anyObject]locationInView:self];
    diffPoint = CGPointMake(currPoint.x - button.frame.origin.x, currPoint.y - button.frame.origin.y);
    [self sendActionsForControlEvents:UIControlEventTouchDown];
}

-(void)animateTitlesToIndex:(NSInteger)index{
    for (int i = 0 ; i < titlesArr.count ; i++){
        UILabel *tempLabel = (UILabel *)[self viewWithTag:50 + i];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        if (i == index){
            tempLabel.center = CGPointMake(tempLabel.center.x, self.frame.size.height - TITLE_SELECTED_DISTANCE + 5);
            tempLabel.alpha = 1;
            tempLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        } else {
            tempLabel.center = CGPointMake(tempLabel.center.x, self.frame.size.height + 5);
            tempLabel.alpha = TITLE_FADE_ALPHA;
            tempLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        }
        [UIView commitAnimations];
    }
}

-(void)animateHandlerToIndex:(NSInteger)index{
    CGPoint toPoint = [self getCenterPointForIndex:index];
    toPoint = CGPointMake( toPoint.x - (handler.frame.size.width / 2.f), handler.frame.origin.y);
    toPoint = [self fixFinalPoint:toPoint];
    
    [UIView beginAnimations:nil context:nil];
    handler.frame = CGRectMake(toPoint.x, toPoint.y, handler.frame.size.width, handler.frame.size.height);
    [UIView commitAnimations];
}

-(void)touchUp:(UIButton *)button{
    _selectedIndex = [self getSelectedTitleInPoint:button.center];
    [self animateHandlerToIndex:_selectedIndex];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)touchMove:(UIButton *)button  withEvent:(UIEvent *)event{
    CGPoint currPoint = [[[event allTouches] anyObject]locationInView:self];
    CGPoint toPoint = CGPointMake(currPoint.x - diffPoint.x, handler.frame.origin.y);
    toPoint = [self fixFinalPoint:toPoint];
    
    handler.frame = CGRectMake(toPoint.x, toPoint.y, handler.frame.size.width, handler.frame.size.height);
    NSInteger selectedIndex = [self getSelectedTitleInPoint:button.center];
    [self animateTitlesToIndex:selectedIndex];
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)moveToIndex:(NSInteger)index{
    CGPoint toPoint = [self getCenterPointForIndex:index];
    toPoint = CGPointMake( toPoint.x - (handler.frame.size.width / 2.f), handler.frame.origin.y);
    toPoint = [self fixFinalPoint:toPoint];
    
    handler.frame = CGRectMake(toPoint.x, toPoint.y, handler.frame.size.width, handler.frame.size.height);
    
    UILabel *tempLabel = (UILabel *)[self viewWithTag:50 + index];
    tempLabel.center = CGPointMake(tempLabel.center.x, self.frame.size.height - TITLE_SELECTED_DISTANCE + 5);
    tempLabel.alpha = 1;
    tempLabel.textColor = [UIColor colorWithCustomerName:@"红"];
  

}


#pragma mark setProperty
-(void)setTitlesColor:(UIColor *)color{
    for (int i = 0 ; i < titlesArr.count;i++){
        UILabel *tempLabel = (UILabel *)[self viewWithTag:50 + i];
        tempLabel.textColor = color;
    }
}

- (void)setTitlesFont:(UIFont *)font{
    for (int i = 0 ; i < titlesArr.count;i++){
        UILabel *tempLabel = (UILabel *)[self viewWithTag:50 + i];
        tempLabel.font = font;
    }
}

-(void)setSelectedIndex:(NSInteger)index{
    _selectedIndex = index;
    [self animateTitlesToIndex:index];
    [self animateHandlerToIndex:index];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

-(void)itemSelected:(UITapGestureRecognizer *)tap{
    _selectedIndex = [self getSelectedTitleInPoint:[tap locationInView:self]];
    [self setSelectedIndex:_selectedIndex];
    
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}



-(int)getSelectedTitleInPoint:(CGPoint)pnt{
    return round((pnt.x-LEFT_OFFSET)/oneSlotSize);
}



#pragma mark - 计算center
-(CGPoint)getCenterPointForIndex:(NSInteger)index{
    return CGPointMake((index/(CGFloat)(titlesArr.count-1)) * (self.frame.size.width-RIGHT_OFFSET-LEFT_OFFSET) + LEFT_OFFSET, index==0?self.frame.size.height + 5 -TITLE_SELECTED_DISTANCE:self.frame.size.height + 5);
}

-(CGPoint)fixFinalPoint:(CGPoint)pnt{
    if (pnt.x < LEFT_OFFSET-(handler.frame.size.width/2.f)) {
        pnt.x = LEFT_OFFSET-(handler.frame.size.width/2.f);
    }else if (pnt.x+(handler.frame.size.width/2.f) > self.frame.size.width-RIGHT_OFFSET){
        pnt.x = self.frame.size.width-RIGHT_OFFSET- (handler.frame.size.width/2.f);
    }
    return pnt;
}

-(void)dealloc{
    [handler removeTarget:self action:@selector(touchDown:withEvent:) forControlEvents:UIControlEventTouchDown];
    [handler removeTarget:self action:@selector(touchUp:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    [handler removeTarget:self action:@selector(touchMove:withEvent: ) forControlEvents: UIControlEventTouchDragOutside | UIControlEventTouchDragInside];
}


@end
