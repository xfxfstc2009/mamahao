//
//  MMHPickuperInfoCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 取件人信息
#import "MMHPickuperInfoCell.h"

@interface MMHPickuperInfoCell(){
    UILabel *pickuperNameLabel;             /**< 取件人名字*/
    UILabel *pickuperShopNameLabel;         /**< 取件人门店*/
    UILabel *pickuperPhoneNumberLabel;      /**< 取件人联系方式*/
    UILabel *pickuperDymicPhoneLabel;       /**< 取件人号码*/
    UILabel *pickuperTimeLabel;             /**< 取件人取件时间*/
}
@end

@implementation MMHPickuperInfoCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 取件人名字
    pickuperNameLabel = [[UILabel alloc]init];
    [self createCustomerLabel:pickuperNameLabel text:@"门店人员"];
    
    // 门店
    pickuperShopNameLabel = [[UILabel alloc]init];
    [self createCustomerLabel:pickuperShopNameLabel text:@"所属门店"];

    // 联系方式
    pickuperPhoneNumberLabel = [[UILabel alloc]init];
    [self createCustomerLabel:pickuperPhoneNumberLabel text:@"联系方式"];
    
    // 上门取件时间
    pickuperTimeLabel = [[UILabel alloc]init];
    [self createCustomerLabel:pickuperTimeLabel text:@"上门取件时间"];
}

-(void)createCustomerLabel:(UILabel *)label text:(NSString *)text{
    label.text = text;
    label.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentLeft;
    [self.bgView addSubview:label];
    // 计算frame
}

-(void)setModel:(NSString *)model{
    CGFloat contentHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    if (!self.bgView.subviews){
        // 取件人名字
        pickuperNameLabel.text = @"门店人员:张三";
        pickuperTimeLabel.frame = CGRectMake(MMHFloat(25), MMHFloat(12), kScreenWidth - 2 * MMHFloat(25), contentHeight);
        
        // 门店
        pickuperShopNameLabel.text = @"所属门店:好孩子武林门店";
        pickuperShopNameLabel.frame = CGRectMake(MMHFloat(25), MMHFloat(12) + contentHeight +MMHFloat(15), kScreenWidth - 2 * MMHFloat(25), contentHeight);
        
        // 联系方式
        CGSize contentOfPhoneNumberSize = [@"" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, contentHeight)];
        pickuperPhoneNumberLabel.text = @"联系方式:";
        pickuperPhoneNumberLabel.frame = CGRectMake(MMHFloat(25), MMHFloat(12) + contentHeight * 2 +MMHFloat(15) * 2,contentOfPhoneNumberSize.width, contentHeight);
        
        // 上门取件时间
        pickuperTimeLabel.text = @"上门取件时间:17:30钱";
        pickuperTimeLabel.frame = CGRectMake(MMHFloat(25), MMHFloat(12) + contentHeight * 3 +MMHFloat(15) * 3,kScreenWidth - 2 * MMHFloat(25), contentHeight);
    }
}
@end
