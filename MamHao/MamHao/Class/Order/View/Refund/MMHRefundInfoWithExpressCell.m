//
//  MMHRefundInfoWithExpressCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundInfoWithExpressCell.h"

@interface MMHRefundInfoWithExpressCell(){
    UILabel *expressNameLabel;              /**< 物流名称*/
    UILabel *expressNumberLabel;            /**< 物流单号*/
    UILabel *expressInfoLabel;              /**< 物流信息*/
    
    //fixed
    UILabel *expressFixedNameLabel;         /**< 物流名称fixed*/
    UILabel *expressFixedNumberLabel;       /**< 物流单号fixed*/
    UILabel *expressFixedInfoLabel;         /**< 物流信息fixed*/
}
@end
@implementation MMHRefundInfoWithExpressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.frame = self.bounds;
    [self addSubview:self.bgView];
    
    CGFloat contentHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    CGSize expressSize = [@"退货物流:" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, contentHeight)];
    
    // 创建物流信息
    // fixed
    expressFixedNameLabel = [[UILabel alloc]init];
    expressFixedNameLabel.numberOfLines = 1;
    expressFixedNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressFixedNameLabel.frame = CGRectMake(MMHFloat(10 + 15), MMHFloat(15), expressSize.width, contentHeight);
    expressFixedNameLabel.text = @"退货物流:";
    expressFixedNameLabel.textAlignment = NSTextAlignmentLeft;
    expressFixedNameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.bgView addSubview:expressFixedNameLabel];
    
    expressFixedNumberLabel = [[UILabel alloc]init];
    expressFixedNumberLabel.numberOfLines = 1;
    expressFixedNumberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressFixedNumberLabel.frame = CGRectMake(MMHFloat(10 + 15), CGRectGetMaxY(expressFixedNameLabel.frame) + MMHFloat(15), expressSize.width, contentHeight);
    expressFixedNumberLabel.text = @"物流单号:";
    expressFixedNumberLabel.textAlignment = NSTextAlignmentLeft;
    expressFixedNumberLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.bgView addSubview:expressFixedNumberLabel];
    
    expressFixedInfoLabel = [[UILabel alloc]init];
    expressFixedInfoLabel.numberOfLines = 1;
    expressFixedInfoLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressFixedInfoLabel.frame = CGRectMake(MMHFloat(10 + 15), CGRectGetMaxY(expressFixedNumberLabel.frame) + MMHFloat(15), expressSize.width, contentHeight);
    expressFixedInfoLabel.text = @"物流信息:";
    expressFixedInfoLabel.textAlignment = NSTextAlignmentLeft;
    expressFixedInfoLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.bgView addSubview:expressFixedInfoLabel];

    
    expressNameLabel = [[UILabel alloc]init];
    expressNameLabel.numberOfLines = 1;
    expressNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressNameLabel.frame = CGRectMake(CGRectGetMaxX(expressFixedNameLabel.frame) + MMHFloat(5), expressFixedNameLabel.frame.origin.y, kScreenBounds.size.width -  MMHFloat(15) - 10 - CGRectGetMaxX(expressFixedNameLabel.frame) - MMHFloat(5) - MMHFloat(20), contentHeight);
    expressNameLabel.textAlignment = NSTextAlignmentLeft;
    expressNameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.bgView addSubview:expressNameLabel];
    
    // 创建物流单号
    
    expressNumberLabel = [[UILabel alloc]init];
    expressNumberLabel.numberOfLines = 1;
    expressNumberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressNumberLabel.frame = CGRectMake(CGRectGetMaxX(expressFixedNumberLabel.frame) + MMHFloat(5),expressFixedNumberLabel.frame.origin.y, kScreenBounds.size.width - 2 * MMHFloat(25) - expressSize.width, contentHeight);
    expressNumberLabel.textAlignment = NSTextAlignmentLeft;
    expressNumberLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.bgView addSubview:expressNumberLabel];
    
    // 创建物流信息
    
    expressInfoLabel = [[UILabel alloc]init];
    expressInfoLabel.textAlignment = NSTextAlignmentLeft;
    expressInfoLabel.numberOfLines=0;
    expressInfoLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    expressInfoLabel.backgroundColor=[UIColor clearColor];
    [self.bgView addSubview:expressInfoLabel];
   
}

-(void)setExpressModel:(MMHExpressModel *)expressModel{
    _expressModel = expressModel;
    expressNameLabel.text = expressModel.platformName.length?expressModel.platformName:@"未获取到物流名称";
    expressNumberLabel.text = expressModel.waybillNumber.length?expressModel.waybillNumber:@"未获取到物流单号";

    if (!expressModel.data.count){
        expressInfoLabel.text = @"未获取到该物流信息";
        expressInfoLabel.frame=CGRectMake(CGRectGetMaxX(expressFixedInfoLabel.frame) + MMHFloat(5),expressFixedInfoLabel.frame.origin.y,kScreenBounds.size.width - (MMHFloat(15) + 10) - CGRectGetMaxX(expressFixedInfoLabel.frame),expressFixedInfoLabel.bounds.size.height);
    } else {
        NSString *expressTimeString = [[expressModel.data lastObject] time];
        NSString *expressDescString = [[expressModel.data lastObject]desc];
        NSString *expressInfoString = [NSString stringWithFormat:@" %@ %@",expressTimeString,expressDescString];
        
        CGSize contentOfSize = [expressInfoString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15) + 10), CGFLOAT_MAX)];
        expressInfoLabel.attributedText =  [MMHTool rangeLabelWithContent:expressInfoString hltContentArr:@[expressTimeString] hltColor:[UIColor colorWithCustomerName:@"蓝"] normolColor:[UIColor colorWithCustomerName:@"灰"]];;
        expressInfoLabel.frame=CGRectMake(CGRectGetMaxX(expressFixedInfoLabel.frame) + MMHFloat(5), CGRectGetMaxY(expressNumberLabel.frame) + MMHFloat(15), kScreenBounds.size.width - (MMHFloat(15) + 10) - (CGRectGetMaxX(expressFixedInfoLabel.frame) + MMHFloat(5)), contentOfSize.height);
    }
}


+(CGFloat)cellHeightWithModel:(MMHExpressModel *)expressModel{
    CGFloat cellHeight = 0;
    
    NSString *expressTimeString = [[expressModel.data lastObject] time];
    NSString *expressDescString = [[expressModel.data lastObject] desc];
    NSString *expressInfoString = @"";
    if (expressTimeString.length){
        expressInfoString = [NSString stringWithFormat:@" %@ %@",expressTimeString,expressDescString];
    } else {
        expressInfoString = @"未获取到物流信息";
    }
    
    CGFloat contentHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    CGSize expressSize = [@"退货物流:" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, contentHeight)];
    CGSize contentOfSize = [expressInfoString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *(MMHFloat(15) + 10) - (expressSize.width + MMHFloat(5)), CGFLOAT_MAX)];
    
    cellHeight += MMHFloat(15) * 2 + contentOfSize.height + MMHFloat(17) * 2 + 2 * [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    
    return cellHeight;
}
@end
