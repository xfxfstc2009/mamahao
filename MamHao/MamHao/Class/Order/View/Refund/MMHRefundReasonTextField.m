//
//  MMHRefundReasonTextField.m
//  MamHao
//
//  Created by 姚驰 on 15/7/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundReasonTextField.h"

@implementation MMHRefundReasonTextField

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(paste:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

@end
