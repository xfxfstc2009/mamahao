//
//  MMHPickuperInfoCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 取件人信息
#import <UIKit/UIKit.h>

@interface MMHPickuperInfoCell : UITableViewCell

@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)NSString *model;

@end
