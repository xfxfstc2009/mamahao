//
//  MMHRefundExamineCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 审核结果

#import <UIKit/UIKit.h>
#import "MMHRefundDetailViewController.h"
@class MMHRefundDetailViewController;
@interface MMHRefundExamineCell : UITableViewCell

@property (nonatomic,copy)NSString *channelString;              // 渠道string
@property (nonatomic,copy)NSString *subTitleString;             // 副标题string
@property (nonatomic,copy)NSString *arriveTimeString;           // 打款时间

@property (nonatomic,strong)UILabel *contentLabel;              // contentLabel
@property (nonatomic,strong)UIView *contentBackgrounView;       // 背景imageView
@property (nonatomic,strong)    UIImageView *signImageView;     // signImageView

@property (nonatomic,assign)MMHRefundApplyState refundApplyState;       // 审核结果
@property (nonatomic,assign)MMHRefundPayType refundPayType;             /**< 支付方式*/
@property (nonatomic,assign)MMHRefundType refundType;                   /**< 退款类型*/
@property (nonatomic,assign)MMHRefundShopType refundShopType;           /**< 自提-退货方式*/
@property (nonatomic,assign)MMHRefundState refundState ;                /**< 验证方式*/
@property (nonatomic,assign)MMHRefundTypeWithHome refundTypeWithHome;   /**< 退款-退货*/


// 返回高度
+(CGFloat)cellHeightWithchannelString:(NSString *)channelString arriveTimeString:(NSString *)arriveTimeString andSubTitleString:(NSString *)subTitleString refundApplyState:(MMHRefundApplyState) refundApplyState refundApplyPayType:(MMHRefundPayType)refundPayType refundType:(MMHRefundType)refundType refundShopType:(MMHRefundShopType)refundShopType refundState:(MMHRefundState)refundState refundTypeWithHome:(MMHRefundTypeWithHome)refundTypeWithHome; // 返回高度

@end
