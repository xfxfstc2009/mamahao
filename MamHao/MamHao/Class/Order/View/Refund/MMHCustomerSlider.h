//
//  MMHCustomerSlider.h
//  MamHao
//
//  Created by SmartMin on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHCustomerSlider : UIControl

@property(nonatomic, retain) UIColor *progressColor;
@property(nonatomic, readonly) NSInteger selectedIndex;

-(id) initWithFrame:(CGRect)frame items:(NSArray *) items;
-(void) setSelectedIndex:(NSInteger)index;
-(void)animateHandlerToIndex:(NSInteger)index;
-(void) setTitlesColor:(UIColor *)color;
-(void) setTitlesFont:(UIFont *)font;
-(void)moveToIndex:(NSInteger)index;
@end
