//
//  MMHRefundExamineCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundExamineCell.h"

@interface MMHRefundExamineCell(){
    UILabel *fixedContentLabel;
    UILabel *auditResultsLabel;                 /**< 审核结果-通过审核*/
    UIView *floatView;
}

@end

@implementation MMHRefundExamineCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    UIView *resultView = [[UIView alloc]init];
    resultView.backgroundColor = [UIColor clearColor];
    [self addSubview:resultView];
    
    // 创建imageView
    self.signImageView = [[UIImageView alloc]init];
    self.signImageView.stringTag = @"signImageView";
    self.signImageView.backgroundColor = [UIColor clearColor];
    self.signImageView.frame = CGRectMake(MMHFloat(150), MMHFloat(17), MMHFloat(33), MMHFloat(33));
    [resultView addSubview:self.signImageView];
    
    // 创建label
    auditResultsLabel = [[UILabel alloc]init];
    auditResultsLabel.backgroundColor = [UIColor clearColor];
    auditResultsLabel.textAlignment = NSTextAlignmentLeft;
    auditResultsLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    auditResultsLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    auditResultsLabel.stringTag = @"auditResultsLabel";
    [resultView addSubview:auditResultsLabel];
    
    // 创建 contentLabel
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.frame = CGRectMake(MMHFloat(25), CGRectGetMaxY(self.signImageView.frame) + MMHFloat(17), kScreenBounds.size.width - 2 * (MMHFloat(15)+ 10), 30);
    self.contentLabel.textAlignment = NSTextAlignmentLeft;
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [resultView addSubview:self.contentLabel];
    
    // 创建view
    self.contentBackgrounView = [[UIView alloc]init];
    self.contentBackgrounView.backgroundColor = [UIColor colorWithCustomerName:@"紫"];
    self.contentBackgrounView.layer.cornerRadius = MMHFloat(5.);
    [resultView addSubview:self.contentBackgrounView];
    
    // 创建label
    fixedContentLabel = [[UILabel alloc]init];
    fixedContentLabel.backgroundColor = [UIColor clearColor];
    fixedContentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    fixedContentLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    fixedContentLabel.numberOfLines = 0;
    
    [self.contentBackgrounView addSubview: fixedContentLabel];
}

-(void)setRefundApplyState:(MMHRefundApplyState)refundApplyState{
    _refundApplyState = refundApplyState;
}
-(void)setRefundPayType:(MMHRefundPayType)refundPayType{
    _refundPayType = refundPayType;
}
-(void)setRefundShopType:(MMHRefundShopType)refundShopType{
    _refundShopType = refundShopType;
}
-(void)setRefundState:(MMHRefundState)refundState{
    _refundState = refundState;
}

-(void)setRefundTypeWithHome:(MMHRefundTypeWithHome)refundTypeWithHome{
    _refundTypeWithHome = refundTypeWithHome;
}

// 【打款时间】
-(void)setArriveTimeString:(NSString *)arriveTimeString{
    _arriveTimeString = arriveTimeString;
}


-(void)setChannelString:(NSString *)channelString{
    
    
    // 【通过审核】
    if (self.refundApplyState == MMHRefundApplyStateFail){
        auditResultsLabel.text = @"未通过审核";
        self.signImageView.image = [UIImage imageNamed:@"icon_returns_wrong"];
    } else {
        self.signImageView.image = [UIImage imageNamed:@"icon_returns_correct"];
        if (self.refundState == MMHRefundStateInspection){
            auditResultsLabel.text = @"收到商品";
        } else {
            auditResultsLabel.text = @"通过审核";
        }
    }
    // 计算宽度
    CGSize titleOfSize = [auditResultsLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
    CGFloat titleContent = (kScreenBounds.size.width - titleOfSize.width - MMHFloat(12 + 33)) / 2. ;
    self.signImageView.orgin_x = titleContent;
    
    auditResultsLabel.frame =CGRectMake(CGRectGetMaxX(self.signImageView.frame) + MMHFloat(12), self.signImageView.frame.origin.y, MMHFloat(200), 30);
    
    if (channelString){             // 如果有数据
        NSString *tempString;
        if (self.refundApplyState == MMHRefundApplyStateSuccess){
            if ((self.refundType == MMHRefundTypeShop )|| ((self.refundType == MMHRefundTypeSelf) && self.refundShopType == MMHRefundShopTypeShopGet)){
                
                if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){
                    if (self.refundPayType){
                       tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核，请您保持电话畅通，%@人员将在%@之前上门取件",channelString,channelString,self.arriveTimeString];
                        if (self.refundState == MMHRefundStateSuccess){
                            tempString = [NSString stringWithFormat:@"您的退款资金已经打入您的支付账户。"];
                        }
                    }
                } else {
                    tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核",channelString];
                }
            } else if (self.refundType == MMHRefundTypeExpress || ((self.refundType == MMHRefundTypeSelf) && (self.refundShopType == MMHRefundShopTypeExpress))){
                if (self.refundState == MMHRefundStateInspection){
                    tempString = @"亲爱的麻麻，麻麻好平台已经收到您寄到的商品啦!";
                } else if (self.refundState == MMHRefundStateSuccess){
                    tempString = [NSString stringWithFormat:@"您的退款资金已经打入您的支付账户。"];
                } else {
                    tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核",channelString];
                }
            } else if (self.refundType == MMHRefundTypeSelf && self.refundShopType == MMHRefundShopTypeSelf){
                tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核",channelString];                
            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            tempString = [NSString stringWithFormat:@"您的退货退款申请没有通过%@的审核",channelString];
        }
        
        // contentLabel
        NSAttributedString *attributedStringText;
        NSArray *attributedTempArr;
        if (self.arriveTimeString){
            attributedTempArr = @[channelString,self.arriveTimeString];
        } else {
            attributedTempArr = @[channelString];
        }
        attributedStringText= [MMHTool rangeLabelWithContent:tempString hltContentArr:attributedTempArr hltColor:[UIColor colorWithCustomerName:@"红"] normolColor:[UIColor colorWithCustomerName:@"灰"]];
        self.contentLabel.attributedText = attributedStringText;
        self.contentLabel.numberOfLines=0;
        
        CGSize contentOfSize = [tempString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15) + 10),CGFLOAT_MAX)];
        self.contentLabel.frame=CGRectMake(MMHFloat(15) + 10, CGRectGetMaxY(self.signImageView.frame) + MMHFloat(17), kScreenBounds.size.width - 2 * (MMHFloat(15) + 10)  , contentOfSize.height);
        

        if (self.refundApplyState == MMHRefundApplyStateSuccess){
//            if (self.refundShopType == MMHRefundShopTypeNormol){
//                floatView.hidden = YES;
//                fixedContentLabel.hidden = YES;
//                self.contentBackgrounView.hidden = YES;
//            } else {
                //            }
            if (self.refundState == MMHRefundStateSuccess){
                self.contentLabel.textAlignment = NSTextAlignmentCenter;
            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            floatView.hidden = YES;
            fixedContentLabel.hidden = YES;
            self.contentBackgrounView.hidden = YES;
        }
    }
}


-(void)setSubTitleString:(NSString *)subTitleString{
    _subTitleString = subTitleString;
    // 【副标题】
    if (self.refundApplyState == MMHRefundApplyStateSuccess){
        if (subTitleString.length){
            CGSize fixedContentSize = [subTitleString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15)+10), CGFLOAT_MAX)];
            
            self.contentBackgrounView.frame = CGRectMake(10, CGRectGetMaxY(self.contentLabel.frame) + MMHFloat(17), kScreenBounds.size.width - 2 * 10, fixedContentSize.height + 2 * MMHFloat(17));
            
            fixedContentLabel.frame = CGRectMake(MMHFloat(15), MMHFloat(17), kScreenBounds.size.width - 2 * MMHFloat(15) - 2 * 10, fixedContentSize.height);
            fixedContentLabel.text = subTitleString;
            
            // 创建floatView
            floatView =[[UIView alloc]init];
            floatView.frame = CGRectMake(0, 0, self.contentBackgrounView.bounds.size.width, MMHFloat(10));
            floatView.backgroundColor = [UIColor colorWithCustomerName:@"紫"];
            [self.contentBackgrounView addSubview:floatView];
            
            floatView.hidden = NO;
            fixedContentLabel.hidden = NO;
            self.contentBackgrounView.hidden = NO;
            
        }
        if (self.refundState == MMHRefundStateSuccess){
            self.contentLabel.textAlignment = NSTextAlignmentCenter;
            floatView.hidden = YES;
            self.contentBackgrounView.hidden = YES;
            self.signImageView.image = [UIImage imageNamed:@"icon_returns_complete-1"];
            auditResultsLabel.text = @"退款完成";
            auditResultsLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
            
        }
    } else if (self.refundApplyState == MMHRefundApplyStateFail){
        floatView.hidden = YES;
        fixedContentLabel.hidden = YES;
        self.contentBackgrounView.hidden = YES;
    }
}


+(CGFloat)cellHeightWithchannelString:(NSString *)channelString arriveTimeString:(NSString *)arriveTimeString andSubTitleString:(NSString *)subTitleString refundApplyState:(MMHRefundApplyState) refundApplyState refundApplyPayType:(MMHRefundPayType)refundPayType refundType:(MMHRefundType)refundType refundShopType:(MMHRefundShopType)refundShopType refundState:(MMHRefundState)refundState refundTypeWithHome:(MMHRefundTypeWithHome)refundTypeWithHome{
    CGFloat cellHeight = 0;
    NSString *tempString;
    
    if (refundApplyState == MMHRefundApplyStateSuccess){
        tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核",channelString];
      if ((refundType == MMHRefundTypeShop )|| ((refundType == MMHRefundTypeSelf) && (refundShopType == MMHRefundShopTypeShopGet))){
            if (refundTypeWithHome == MMHRefundTypeWithHomeProduct){
                if (refundPayType){
                    tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核，请您保持电话畅通，%@人员将在%@之前上门取件",channelString,channelString,arriveTimeString];
                    if (refundState == MMHRefundStateSuccess){
                        tempString = [NSString stringWithFormat:@"您的退款资金已经打入您的支付账户。"];
                    }
                }
            }
        } else if ((refundApplyState == MMHRefundTypeExpress) || ((refundShopType == MMHRefundShopTypeExpress) && (refundType == MMHRefundTypeSelf))){
            if (refundState == MMHRefundStateInspection){
                tempString = @"亲爱的麻麻，麻麻好平台已经收到您寄到的商品啦!";
            } else if (refundState == MMHRefundStateSuccess){
                tempString = [NSString stringWithFormat:@"您的退款资金已经打入您的支付账户。"];
            } else {
                tempString = [NSString stringWithFormat:@"您的退货退款申请通过%@的审核",channelString];
            }
        }
    } else if (refundApplyState == MMHRefundApplyStateFail){
        tempString = [NSString stringWithFormat:@"您的退货退款申请没有通过%@的审核",channelString];
    }

    CGSize contentOfSize = [tempString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15) + 10), CGFLOAT_MAX)];
    
    NSString *tempString1 = subTitleString;
    CGSize fixedContentSize = [tempString1 sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15) + 10), CGFLOAT_MAX)];
    
    if (refundApplyState == MMHRefundApplyStateSuccess){
        if (refundState == MMHRefundStateSuccess){
            cellHeight = MMHFloat(17) + MMHFloat(33) + MMHFloat(17) + contentOfSize.height + MMHFloat(17);
        } else {
            cellHeight += 3 * MMHFloat(17) + MMHFloat(33) + contentOfSize.height +fixedContentSize.height + 2 * MMHFloat(17);
        }
    } else if (refundApplyState == MMHRefundApplyStateFail || refundState == MMHRefundStateSuccess){
        cellHeight = MMHFloat(17) + MMHFloat(33) + MMHFloat(17) + contentOfSize.height + MMHFloat(17);
    }
    return cellHeight;
}


@end
