//
//  MMHRefundInfoWithExpressCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHExpressModel.h"
@interface MMHRefundInfoWithExpressCell : UITableViewCell

@property (nonatomic,strong)UIView *bgView;                 /**< 背景view*/
@property (nonatomic,strong)MMHExpressModel *expressModel;  /**<快递model */ 


+(CGFloat)cellHeightWithModel:(MMHExpressModel *)expressModel;

@end
