//
//  MMHOrderShopInfoTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 商店信息
#import <UIKit/UIKit.h>
#import "MMHsinceTheMentionAddressModel.h"
#import <objc/runtime.h>

typedef NS_ENUM(NSInteger, MMHShopInfoCellUsingClass) {
    MMHShopInfoCellUsingClassNormol = 0,                /**< 默认使用*/
    MMHShopInfoCellUsingClassRefund,                    /**< 退款*/
};

@interface MMHOrderShopInfoTableViewCell : UITableViewCell

@property (nonatomic,strong)MMHsinceTheMentionAddressModel *sinceTheMentionAddressModel; /**< 门店信息*/
@property (nonatomic,strong)UIView *shopInfoView;           /**< 商店信息view*/
@property (nonatomic,strong)NSArray *propertiesArr;         /**< 属性数组*/

@property (nonatomic,strong)UIButton *phoneNumberButton;    /**< 电话按钮*/

@property (nonatomic,assign)MMHShopInfoCellUsingClass shopInfoCellUsingClass;             /**< 商店使用*/

+ (CGFloat)cellHeightWith:(MMHsinceTheMentionAddressModel *)sinceTheMentionAddressModel andUsingClass:(MMHShopInfoCellUsingClass)shopInfousingClass;  /**< 返回cell 高度*/

@end
