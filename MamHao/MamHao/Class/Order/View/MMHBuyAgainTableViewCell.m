//
//  MMHBuyAgainTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHBuyAgainTableViewCell.h"

@interface MMHBuyAgainTableViewCell(){
    MMHImageView *productImageView;
    UILabel * productNameLabel;
    UILabel *skuLabel;
}

@end

@implementation MMHBuyAgainTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


-(void)createView{
    productImageView = [[MMHImageView alloc]init];
    productImageView.backgroundColor = [UIColor clearColor];
    productImageView.stringTag = [NSString stringWithFormat:@"productImageView%li",(long)index];
    productImageView.layer.masksToBounds = YES;
    [productImageView.layer setCornerRadius:MMHFloat(6.)];
    productImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    productImageView.layer.borderWidth = .5f;
    productImageView.frame = CGRectMake(MMHFloat(15), MMHFloat(100 - 70) / 2., MMHFloat(70), MMHFloat(70));
    [self addSubview:productImageView];
    
    // 创建name 
    productNameLabel = [[UILabel alloc]init];
    productNameLabel.backgroundColor = [UIColor clearColor];
    productNameLabel.stringTag = @"productNameLabel";
    productNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    productNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    productNameLabel.numberOfLines = 2;
    [self addSubview:productNameLabel];
    
    // sku
    skuLabel = [[UILabel alloc]init];
    skuLabel.backgroundColor = [UIColor clearColor];
    skuLabel.font = [UIFont systemFontOfCustomeSize:13.];
    skuLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
    skuLabel.numberOfLines = 1;
    [self addSubview:skuLabel];
    
    self.checkImageView = [[UIImageView alloc]init];
    self.checkImageView.image = [UIImage imageNamed:@"address_icon_selected"];
    [self addSubview:self.checkImageView];
}


-(void)setSingleProductModel:(MMHOrderWithGoodsListModel *)singleProductModel{
    _singleProductModel = singleProductModel;
    [productImageView updateViewWithImageAtURL:singleProductModel.itemPic];
    
    // 商品名
    CGSize productNameSize = [singleProductModel.itemName sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), CGFLOAT_MAX)];
    CGFloat shopNameHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
    productNameLabel.text = singleProductModel.itemName;
    
    if (productNameSize.height <= shopNameHeight){
        productNameLabel.numberOfLines = 1;
        productNameLabel.frame = CGRectMake(CGRectGetMaxX(productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), shopNameHeight);
    } else {
        productNameLabel.numberOfLines = 2;
        productNameLabel.frame = CGRectMake(CGRectGetMaxX(productImageView.frame) + MMHFloat(10), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70) - MMHFloat(10) - MMHFloat(70), 2 * shopNameHeight);
    }

    // sku
    CGFloat skuContentHeight = [MMHTool contentofHeight:skuLabel.font];
    skuLabel.frame = CGRectMake(CGRectGetMaxX(productImageView.frame) + MMHFloat(10), CGRectGetMaxY(productImageView.frame) - skuContentHeight, productNameLabel.frame.size.width, skuContentHeight);
    NSString *skuLabelString = @"";
    for (int i = 0 ; i < [[singleProductModel spec] count];i++){
        NSString *skuItemString = [[[singleProductModel spec] objectAtIndex:i] skuValue];
        if (skuItemString.length){
            skuLabelString = [skuLabelString stringByAppendingString:[NSString stringWithFormat:@"%@ ",skuItemString]];
        }
    }
    skuLabel.text = skuLabelString;
 
    self.checkImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - MMHFloat(20), MMHFloat(100 - 20)/2. , MMHFloat(20), MMHFloat(20));

}

- (void)setChecked:(BOOL)checked{
    if (checked) {
        [self animationWithCollectionWithButton:self.checkImageView];
    } else {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    self.isChecked = checked;
}

#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionView{
    collectionView.image = [UIImage imageNamed:@"address_icon_selected"];
    
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    [collectionView.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}


@end
