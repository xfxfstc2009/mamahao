//
//  MMHBuyAgainTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHOrderWithGoodsListModel.h"
@interface MMHBuyAgainTableViewCell : UITableViewCell

@property (nonatomic,strong)MMHOrderWithGoodsListModel *singleProductModel;         /**< 单个商品model */

@property (nonatomic,strong)UIImageView *checkImageView;                        /**< 选择的view*/
@property (nonatomic,assign)    BOOL isChecked;                             /**< 是否打钩*/

-(void)setChecked:(BOOL)checked;
@end
