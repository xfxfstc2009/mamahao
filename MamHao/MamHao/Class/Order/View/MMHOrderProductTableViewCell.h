//
//  MMHOrderProductTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHSingleProductWithShopCartModel.h"                   /**< 单个商品model */
#import "MMHOrderSingleModel.h"
#import "MMHOrderDetailViewController.h"

typedef NS_ENUM(NSInteger, MMHCellType) {
    MMHCellTypeOrderDetail,
    MMHCellTypeChooseProduct,
};

@class MMHOrderSingleModel;
@class MMHOrderDetailViewController;
@interface MMHOrderProductTableViewCell : UITableViewCell


// View
@property (nonatomic,strong)UIView *shopActivityView;           /**< 商店活动*/
@property (nonatomic,strong)MMHImageView *productImageView;      /**< 物品图片*/
@property (nonatomic,strong)UILabel *productNameLabel;          /**< 商品名称*/
@property (nonatomic,strong)UIView *skuView;                    /**< 保存sku的View*/
@property (nonatomic,strong)UILabel *priceLabel;                /**< 价格*/
@property (nonatomic,strong)UILabel *numberLabel;               /**< 数量*/
@property (nonatomic,strong)UIView *activityView;               /**< 活动view*/
@property (nonatomic,assign)MMHReceivingStateType receivingStateType;       /**< 提货状态*/
@property (nonatomic,assign)MMHOrderType orderType;                         /**< 订单类型*/
@property (nonatomic,strong)UIButton *orderTypeButton;                      /**< 状态按钮*/
@property (nonatomic,strong)UIButton *orderSubTypeButton;                   /**< 副状态按钮*/

// typeOfMMHCellTypeChooseProduct
@property (nonatomic,strong)UIImageView *checkImageView;                    /**< 打钩按钮*/
@property (nonatomic,assign)    BOOL isChecked;                             /**< 是否打钩*/

// model
@property (nonatomic,strong)MMHSingleProductWithShopCartModel *singleProduct;  /**< 商品model */

+(CGFloat)cellHeightWithSingleProductModel:(MMHSingleProductWithShopCartModel *)singleProduct;

@property (nonatomic,assign)MMHCellType cellType;

-(void)setChecked:(BOOL)checked;

@end
