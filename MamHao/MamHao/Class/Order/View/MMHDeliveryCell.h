//
//  MMHDeliveryCell.h
//  MamHao
//
//  Created by SmartMin on 15/5/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHDistributionModel.h"                                // 收件人信息

@interface MMHDeliveryCell : UITableViewCell

@property (nonatomic,strong) UILabel *shopNameLabel;            /**< 商店名称*/
@property (nonatomic,strong) UILabel *shopAddressLabel;         /**< 商店地址*/
@property (nonatomic,strong) UILabel *shopDeliveryLabel;        /**< 送货人员*/
@property (nonatomic,strong) UIButton *shopDeliveryPhoneButton; /**< 送货电话*/
@property (nonatomic,strong) UILabel *timeLabel;                /**< 送货到达时间*/


@property (nonatomic,strong)MMHDistributionModel *distributionInfo;                 /**< 收件人信息*/

+(CGFloat)cellHeightWithDistributionInfo:(MMHDistributionModel *)distributionInfo;

@end
