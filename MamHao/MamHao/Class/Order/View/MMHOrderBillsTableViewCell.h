//
//  MMHOrderBillsTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHOrderPriceModel.h"                      // 订单账单流水
@interface MMHOrderBillsTableViewCell : UITableViewCell /**< 账单*/


@property (nonatomic,strong)NSArray<MMHOrderPriceModel> *orderPriceArray;

@property (nonatomic,strong)UIView *priceView;  /**< 价格流水view*/

+ (CGFloat)cellHeightWithOrderPriceArray:(NSArray <MMHOrderPriceModel> *)orderPriceArray;    /**< 返回cell高度*/
@end
