//
//  MMHScrollViewTypeTableViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^buttonClickBlock)(NSString *typeName);
@interface MMHScrollViewTypeTableViewCell : UITableViewCell

// 传入
@property (nonatomic,copy)NSString *typeName;
@property (nonatomic,strong)NSArray *typeArray;

@property (nonatomic,strong)UIView *bgView;

@property (nonatomic,strong)NSMutableArray *isSelectedMutableArr;               // 保存选中的Arr
@property (nonatomic,strong)NSMutableArray *isSelectedButtonMutableArr;         // 保存选中的按钮Arr

@property (nonatomic,copy)buttonClickBlock buttonClickBlock;
@end
