//
//  MMHConfirmationOrderAddressCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderAddressCell.h"

@interface MMHConfirmationOrderAddressCell()
@property (nonatomic,strong)UIImageView *addAddressImageView;               /**< 标志*/
@property (nonatomic,strong)UILabel *fixedLabel;                            /**< 收货地址*/
@property (nonatomic,strong)UILabel *consigneeNameLabel;                    /**< 收件人信息*/
@property (nonatomic,strong)UILabel *phoneNumberLabel;                      /**< 电话号码label*/
@property (nonatomic,strong)UILabel *addressLabel;                          /**< 地址label*/
@property (nonatomic,strong)UIImageView *disclosureIndicatorImageView;                         /**< 箭头符号*/

@end

@implementation MMHConfirmationOrderAddressCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundView = [[UIImageView alloc] initWithImage:[self stretchImageWithName:@"order_bg_xinfeng"]];
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 创建UIImageView
    self.addAddressImageView = [[UIImageView alloc]init];
    self.addAddressImageView.backgroundColor = [UIColor clearColor];
    self.addAddressImageView.image = [UIImage imageNamed:@"order_icon_add"];
    [self addSubview:self.addAddressImageView];
    
    // label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.text = @"填写收货地址";
    self.fixedLabel.font = F5;
    self.fixedLabel.textColor = C6;
    [self addSubview:self.fixedLabel];
    
    // 创建收货人信息
    self.consigneeNameLabel = [[UILabel alloc]init];
    self.consigneeNameLabel.backgroundColor = [UIColor clearColor];
    self.consigneeNameLabel.font = F5;
    self.consigneeNameLabel.textColor = C6;
    self.consigneeNameLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.consigneeNameLabel];
    
    // 创建收货人手机号码
    self.phoneNumberLabel = [[UILabel alloc]init];
    self.phoneNumberLabel.backgroundColor = [UIColor clearColor];
    self.phoneNumberLabel.textAlignment = NSTextAlignmentRight;
    self.phoneNumberLabel.font = F5;
    self.phoneNumberLabel.textColor = C6;
    [self addSubview:self.phoneNumberLabel];
    
    // 创建addressLabel
    self.addressLabel = [[UILabel alloc]init];
    self.addressLabel.backgroundColor = [UIColor clearColor];
    self.addressLabel.textColor = C6;
    self.addressLabel.textAlignment = NSTextAlignmentLeft;
    self.addressLabel.font = F5;
    self.addressLabel.numberOfLines = 0;
    self.addressLabel.stringTag = @"addressLabel";
    [self addSubview:self.addressLabel];
    
    self.disclosureIndicatorImageView = [[UIImageView alloc]init];
    self.disclosureIndicatorImageView.image = [UIImage imageNamed:@"tool_arrow"];
    self.disclosureIndicatorImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.disclosureIndicatorImageView];
}

-(void)setCellHeight:(CGFloat)cellHeight{
    _cellHeight = cellHeight;
}

-(void)setAddressSingleModel:(MMHAddressSingleModel *)addressSingleModel{
    _addressSingleModel = addressSingleModel;
    self.disclosureIndicatorImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(10), (self.cellHeight - MMHFloat(18))/2., MMHFloat(10), MMHFloat(18));
    // 赋值
    if (self.addressSingleModel.consignee.length){
        self.addAddressImageView.hidden = YES;
        self.fixedLabel.hidden = YES;
        
        // 1. 收货人姓名
        self.consigneeNameLabel.text = [NSString stringWithFormat:@"收货人:  %@",self.addressSingleModel.consignee];
        CGSize consigneeNameSize = [self.consigneeNameLabel.text sizeWithCalcFont:self.consigneeNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.self.consigneeNameLabel.font])];
        self.consigneeNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(23), consigneeNameSize.width, [MMHTool contentofHeight:self.self.consigneeNameLabel.font]);
        self.consigneeNameLabel.hidden = NO;
        
        // 2. 收货人电话
        self.phoneNumberLabel.text = self.addressSingleModel.phone;
        CGSize phoneNumberSize = [self.phoneNumberLabel.text sizeWithCalcFont:self.phoneNumberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.phoneNumberLabel.font])];
        self.phoneNumberLabel.frame = CGRectMake(CGRectGetMaxX(self.consigneeNameLabel.frame) + MMHFloat(10), MMHFloat(25), phoneNumberSize.width, [MMHTool contentofHeight:self.phoneNumberLabel.font]);
        self.phoneNumberLabel.hidden = NO;
        
        
        // 3. 地址label
        self.addressLabel.text = [NSString stringWithFormat:@"%@%@",self.addressSingleModel.gpsAddress,self.addressSingleModel.addrDetail];
        
        CGSize addressSize = [self.addressLabel.text sizeWithCalcFont:self.addressLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), CGFLOAT_MAX)];
        
        self.addressLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.consigneeNameLabel.frame) + MMHFloat(10), kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), addressSize.height);
        self.addressLabel.hidden = NO;
        
    } else {
        self.addAddressImageView.frame = CGRectMake(MMHFloat(10), (self.cellHeight - MMHFloat(17)) / 2., MMHFloat(17.), MMHFloat(17.));
        self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.addAddressImageView.frame) + MMHFloat(10), 0, 100, self.cellHeight);
        self.addAddressImageView.hidden = NO;
        self.fixedLabel.hidden = NO;
        self.consigneeNameLabel.hidden = YES;
        self.phoneNumberLabel.hidden = YES;
        self.addressLabel.hidden = YES;
    }
}


#pragma mark 返回当前cell高度
+(CGFloat)cellHeightWithAddressSingleModel:(MMHAddressSingleModel *)addressSingleModel{
    CGFloat cellHeight = 0;
    if (addressSingleModel){
        // 1. 收货人height
        CGSize consigneeNameSize = [[NSString stringWithFormat:@"收货人:  %@",addressSingleModel.consignee] sizeWithCalcFont:F5 constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:F5])];
        // 2. 地址
        
        CGSize addressSize = [[NSString stringWithFormat:@"%@%@",addressSingleModel.gpsAddress,addressSingleModel.addrDetail] sizeWithCalcFont:F5 constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *MMHFloat(11) - MMHFloat(30), CGFLOAT_MAX)];
        cellHeight = consigneeNameSize.height + MMHFloat(10)+ MMHFloat(23)+ MMHFloat(23)+addressSize.height;
        return cellHeight;
    } else {
        return MMHFloat(75);
    }
}





- (UIImage *)stretchImageWithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width topCapHeight:image.size.height * 0.5];
}
@end

