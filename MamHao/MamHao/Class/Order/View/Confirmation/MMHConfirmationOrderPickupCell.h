//
//  MMHConfirmationOrderPickupCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单 - 自提 - 店名称和地址信息】
#import <UIKit/UIKit.h>
#import "MMHsinceTheMentionAddressModel.h"
@interface MMHConfirmationOrderPickupCell : UITableViewCell


@property (nonatomic,strong)MMHsinceTheMentionAddressModel *shopInfo;               /**< 商店信息 用户自提*/
@end
