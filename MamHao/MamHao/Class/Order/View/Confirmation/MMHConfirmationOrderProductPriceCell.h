//
//  MMHConfirmationOrderProductPriceCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单 - 商品金额】
#import <UIKit/UIKit.h>
#import "MMHOrderPriceModel.h"

@interface MMHConfirmationOrderProductPriceCell : UITableViewCell

@property (nonatomic,strong)NSMutableArray <MMHOrderPriceModel> *price;                // 订单价格

@end
