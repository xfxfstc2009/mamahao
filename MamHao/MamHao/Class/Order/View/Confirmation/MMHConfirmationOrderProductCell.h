//
//  MMHConfirmationOrderProductCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单- 商品cell 】
#import <UIKit/UIKit.h>
#import "MMHSingleProductWithShopCartModel.h"
#import "MMHShopCartModel.h"
@interface MMHConfirmationOrderProductCell : UITableViewCell
@property (nonatomic,assign)NSInteger cellIndexRow;                 /**< 当前的cellIndex*/

@property (nonatomic,strong)MMHShopCartModel *shopCartModel;
@property (nonatomic,strong)MMHSingleProductWithShopCartModel *singleProduct;
@end
