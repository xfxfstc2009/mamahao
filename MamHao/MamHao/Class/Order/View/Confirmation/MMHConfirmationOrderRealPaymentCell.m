//
//  MMHConfirmationOrderRealPaymentCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderRealPaymentCell.h"

@interface MMHConfirmationOrderRealPaymentCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation MMHConfirmationOrderRealPaymentCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 1. 左侧文字
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = F5;
    self.fixedLabel.textAlignment = NSTextAlignmentLeft;
    self.fixedLabel.textColor = C6;
    self.fixedLabel.frame = CGRectMake(MMHFloat(10), 0, 100, MMHFloat(50));
    self.fixedLabel.text = @"实付款";
    [self addSubview:self.fixedLabel];
    
    // 2. 右侧文字
    // dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [F5 boldFont];
    self.dymicLabel.textColor = C21;
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicLabel];
}

-(void)setPayPrice:(CGFloat)payPrice{
    _payPrice = payPrice;
    self.dymicLabel.text = [NSString stringWithFormat:@"￥%.2f",payPrice];
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    self.dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - dymicSize.width, 0, dymicSize.width, MMHFloat(50));
}

@end
