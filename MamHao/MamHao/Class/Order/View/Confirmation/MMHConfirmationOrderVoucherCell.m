//
//  MMHConfirmationOrderVoucherCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderVoucherCell.h"

@interface MMHConfirmationOrderVoucherCell()<UITextFieldDelegate>
@property (nonatomic,strong)UILabel *fixedLabel;            /**< 固定label*/
@property (nonatomic,strong)UILabel *dymicLabel;            /**< 动态label*/
@property (nonatomic,strong)UITextField *inputTextField;    /**< 输入框textField*/
@end

@implementation MMHConfirmationOrderVoucherCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.fixedLabel.text = @"使用";
    [self addSubview:self.fixedLabel];
    
    // 创建textField
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.textColor = [UIColor blackColor];
    self.inputTextField.delegate = self;
    self.inputTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.inputTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.inputTextField.returnKeyType = UIReturnKeyDone;
    self.inputTextField.borderStyle = UITextBorderStyleRoundedRect;
    [self.inputTextField setKeyboardType:UIKeyboardTypeNamePhonePad];
    self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.inputTextField.textAlignment = NSTextAlignmentCenter;
    [self.inputTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.inputTextField];
    __weak typeof(self)weakSelf = self;
    [weakSelf.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex) {
        if (buttonIndex == 0){
            textField.text = @"";
            if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
                self.fixedLabel.text = @"妈豆";
            } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
                self.fixedLabel.text = @"GoodBaby积分";
            } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
                self.fixedLabel.text = @"Mothercare积分";
            }
            [self.dymicSwitch setOn:NO animated:YES];
        } else if (buttonIndex == 1){
            if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){            // MC积分
                if ([textField.text integerValue] % 300 != 0){
                    [self showTips:@"MotherCare积分300分为一个单位"];
                    textField.text = [NSString stringWithFormat:@"%li",(long)(([textField.text integerValue] / 300) * 300)];
                }
            }
        }
        [weakSelf autoLayoutWithCellWithState:self.dymicSwitch.on];
        [textField resignFirstResponder];
    }];
    
    // 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:self.dymicLabel];
    
    self.dymicSwitch = [[UISwitch alloc]init];
    [self.dymicSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
    self.dymicSwitch.onTintColor = [UIColor hexChangeFloat:@"FC687C"];
    self.dymicSwitch.on = NO;
    [self addSubview:self.dymicSwitch];
}

#pragma mark 传入高度
-(void)setCellHeigt:(CGFloat)cellHeigt{
    _cellHeigt = cellHeigt;
}

#pragma mark 传入妈豆支付数量
-(void)setTransferMBeanPayCount:(NSInteger)transferMBeanPayCount{
    _transferMBeanPayCount = transferMBeanPayCount;
}

#pragma mark 传入价格
-(void)setCutPrice:(CGFloat)cutPrice{
    _cutPrice = cutPrice;
}

#pragma mark 传入类型
-(void)setConfirmationRedirectionType:(MMHConfirmationRedirectionType)confirmationRedirectionType{
    _confirmationRedirectionType = confirmationRedirectionType;
    if (confirmationRedirectionType == MMHConfirmationRedirectionTypeMbean){
        if (self.transferMBeanPayCount != 0){
            self.dymicSwitch.enabled = NO;
            self.dymicSwitch.on = YES;
            self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.transferMBeanPayCount];
            self.inputTextField.enabled = NO;
            [self autoLayoutWithCellWithState:self.dymicSwitch.on];
        }
    }
}

#pragma mark 传入积分类型
-(void)setConfirmationOrderVoucherType:(MMHConfirmationOrderVoucherType)confirmationOrderVoucherType{
    _confirmationOrderVoucherType = confirmationOrderVoucherType;
}

#pragma mark 传入积分限制
-(void)setIndentationLevel:(NSInteger)indentationLevel{
    _integralLimit = indentationLevel;
}

#pragma mark 传入积分
-(void)setTransferVoucher:(long)transferVoucher{
    _transferVoucher = transferVoucher;
     self.dymicSwitch.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - 51,(self.cellHeigt - 31)/2. , 51, 31);
    
    if (self.dymicSwitch.on){                                       // 【打开状态】
        self.fixedLabel.text = @"使用";
        self.inputTextField.hidden = NO;
    } else {
        if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
            self.fixedLabel.text = @"妈豆";
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
            self.fixedLabel.text = @"GoodBaby积分";
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
            self.fixedLabel.text = @"Mothercare积分";
        }
        self.inputTextField.hidden = YES;
    }
    [self autoLayoutWithCellWithState:self.dymicSwitch.on];
}

-(void)setPayPrice:(CGFloat)payPrice{
    _payPrice = payPrice;
}

#pragma mark 自动
-(void)autoLayoutWithCellWithState:(BOOL)stateOn{
    // 1. fixedLabel
    CGSize fixedLabelSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(MMHFloat(11), (self.cellHeigt - fixedLabelSize.height)/2., fixedLabelSize.width, [MMHTool contentofHeight:self.fixedLabel.font]);
    
    // 2. dymic
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame), MMHFloat(2), 0, self.cellHeigt - MMHFloat(4));
    if (stateOn){
        NSString *dymicInputString = @"";
        if (self.inputTextField.text.length){
            dymicInputString = self.inputTextField.text;
        } else {
            dymicInputString = @"7";
        }
        CGSize inputTextFieldSize = [dymicInputString sizeWithCalcFont:self.inputTextField.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.inputTextField.size_height)];
        self.inputTextField.size_width = MMHFloat(30) + inputTextFieldSize.width;
    } else {
        self.dymicLabel.size_width = 0;
    }
    
    // 3. dymicLabel
    NSString *dymicString = @"";
    if (stateOn){
        NSInteger inputTextWithInteger = [self.inputTextField.text integerValue];
        if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){                     // 妈豆
            dymicString = [NSString stringWithFormat:@"妈豆,抵￥%.2f",((float)inputTextWithInteger / 100.)];
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){         // GB积分
            dymicString = [NSString stringWithFormat:@"GB积分,抵￥%.2f",((float)inputTextWithInteger / 100.)];
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){                // MC积分
            dymicString = [NSString stringWithFormat:@"MC积分,抵￥%.2f",((float)inputTextWithInteger / 300.)];
        }
        self.dymicLabel.text = dymicString;
    } else {
        if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){                     // 妈豆
            dymicString = [NSString stringWithFormat:@"可使用%li妈豆",self.transferVoucher];
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){         // GB积分
            dymicString = [NSString stringWithFormat:@"可使用%liGB积分",self.transferVoucher];
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){         // MC积分
            dymicString = [NSString stringWithFormat:@"可使用%liMC积分",self.transferVoucher];
        }
        self.dymicLabel.text = dymicString;
    }
    CGSize dymicLabelSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.inputTextField.frame), (self.cellHeigt - [MMHTool contentofHeight:self.dymicLabel.font]) / 2., dymicLabelSize.width, [MMHTool contentofHeight:self.dymicLabel.font]);
    
    
//    CGFloat tempCutPrice = 0;
//    if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
//        tempCutPrice = [self.inputTextField.text integerValue] / 100.;
//    } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
//        tempCutPrice = [self.inputTextField.text integerValue] / 100.;
//    } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
//        tempCutPrice = [self.inputTextField.text integerValue] / 300.;
//    }
//    
//    if (tempCutPrice > self.integralLimit){             // 输入的大于积分数量
//        if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
//            if (self.integralLimit != 0){
//                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 100];
//                [self showTips:@"亲爱的妈妈,该商品妈豆只能用那么多哦"];
//            }
//        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
//            self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 100];
//            [self showTips:@"亲爱的妈妈,该商品GB积分只能用那么多哦"];
//            return;
//        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
//            self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 300];
//            [self showTips:@"亲爱的妈妈,该商品MC积分只能用那么多哦"];
//            return;
//        }
//    } else {
//        if (self.payPrice - tempCutPrice < 0){                 // 价格小于0
//            if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
//                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 100];
//            } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
//                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 100];
//            } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
//                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.payPrice * 300];
//            }
//            [self showTips:@"亲爱的妈妈,实付款已经为零了哦。"];
//        }
//    }
}


-(void)switchAction:(UISwitch *)customSwitch{
    if (customSwitch.on == YES){
        [self.inputTextField becomeFirstResponder];
        self.fixedLabel.text = @"使用";
        self.inputTextField.hidden = NO;
        [self autoLayoutWithCellWithState:self.dymicSwitch.on];
    } else {
        self.dymicSwitchIsCloseBlock();
        if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMBean){
            self.fixedLabel.text = @"妈豆";
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeGBIntegral){
            self.fixedLabel.text = @"GoodBaby积分";
        } else if (self.confirmationOrderVoucherType == MMHConfirmationOrderVoucherTypeMCIntegral){
            self.fixedLabel.text = @"Mothercare积分";
        }
        
        self.inputTextField.text = @"";
        if ([self.inputTextField isFirstResponder]){
            [self.inputTextField resignFirstResponder];
        }
        self.inputTextField.hidden = YES;
    
        [self autoLayoutWithCellWithState:self.dymicSwitch.on];
    }
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    NSString *voucherWithString = [NSString stringWithFormat:@"%li",(long)self.transferVoucher];
    
    
    if(self.inputTextField == textField) {
        if([toBeString length] >voucherWithString.length){
            textField.text=[toBeString substringToIndex:voucherWithString.length];
            if([toBeString integerValue] > self.transferVoucher){               // 输入的积分比我原有积分高
                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.transferVoucher];
            }
            
            [self autoLayoutWithCellWithState:self.dymicSwitch.on];
            return NO;
        } else if([toBeString length] == voucherWithString.length){
            if([toBeString integerValue] >= self.transferVoucher){               // 输入的积分比我原有积分高
                self.inputTextField.text = [NSString stringWithFormat:@"%li",(long)self.transferVoucher];
            } else {
                self.inputTextField.text = toBeString;
            }
            [self autoLayoutWithCellWithState:self.dymicSwitch.on];
            return NO;
        }
    }    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    self.inputTextFieldBlock(self.dymicLabel,textField,[textField.text integerValue]);
}

-(void)textFieldDidChange:(UITextField *)inputTextField{
    [self autoLayoutWithCellWithState:self.dymicSwitch.on];
}

@end
