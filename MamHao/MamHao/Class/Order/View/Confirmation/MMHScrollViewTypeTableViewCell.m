//
//  MMHScrollViewTypeTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/21.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHScrollViewTypeTableViewCell.h"

@implementation MMHScrollViewTypeTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 0
        [self arrayWithInit];
        
        // 1. 创建label
        UILabel *typeNameLabel = [[UILabel alloc]init];
        typeNameLabel.backgroundColor = [UIColor clearColor];
        typeNameLabel.stringTag = @"typeNameLabel";
        typeNameLabel.font = F5;
        typeNameLabel.textColor = C6;
        typeNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(15), MMHFloat(100), [MMHTool contentofHeight:typeNameLabel.font]);
        [self addSubview:typeNameLabel];
        
        // 2. 创建scrollView
        self.bgView = [[UIView alloc]init];
        self.bgView.frame = CGRectMake(MMHFloat(11), MMHFloat(15) + CGRectGetMaxY(typeNameLabel.frame), kScreenBounds.size.width - 2  * MMHFloat(11), MMHFloat(36));
        self.bgView.backgroundColor = [UIColor clearColor];
        self.stringTag = @"scrollView";
        [self.contentView addSubview:self.bgView];
    }
    return self;
}

-(void)arrayWithInit{
    self.isSelectedMutableArr = [NSMutableArray array];                     // 记录数据
    self.isSelectedButtonMutableArr = [NSMutableArray array];               // 记录button
}

-(void)setTypeName:(NSString *)typeName{
    _typeName = typeName;
    UILabel *typeNameLabel = (UILabel *)[self viewWithStringTag:@"typeNameLabel"];
    typeNameLabel.text = typeName;
}

-(void)setTypeArray:(NSArray *)typeArray{
    _typeArray = typeArray;
    if (!self.bgView.subviews.count){
        for (int i = 0 ;i < typeArray.count ; i++){
            // 创建button
            UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
            customButton.layer.cornerRadius = MMHFloat(6);
            if ([self.isSelectedMutableArr containsObject:[typeArray objectAtIndex:i]]){
                customButton.layer.borderColor = C21.CGColor;
                [customButton setTitleColor:C21 forState:UIControlStateNormal];
            } else {
                customButton.layer.borderColor = C2.CGColor;
                [customButton setTitleColor:C5 forState:UIControlStateNormal];
            }
            customButton.layer.borderWidth = 1;
            customButton.titleLabel.font = F4;
            customButton.frame = CGRectMake(i *(MMHFloat(100+10)), 0, MMHFloat(100), MMHFloat(36));
            [customButton setTitle:[typeArray objectAtIndex:i] forState:UIControlStateNormal];
            [customButton addTarget:self action:@selector(distributionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            customButton.stringTag = [NSString stringWithFormat:@"customButton%i",i];
            [self.bgView addSubview:customButton];
        }
    }
}


-(void)distributionButtonClick:(UIButton *)sender{

    UIButton *customButton = (UIButton *)sender;
    // 1. 拿到tag
    NSArray *tempArr = [customButton.stringTag componentsSeparatedByString:@"customButton"];
    NSInteger buttonTag = 0;
    if (tempArr.count > 1){
        buttonTag = [[tempArr objectAtIndex:1] integerValue];
    }
    
    // 2. this btn hlt
    if (self.isSelectedMutableArr.count){
        if (![[self.isSelectedMutableArr lastObject] isEqualToString:[_typeArray objectAtIndex:buttonTag]]){         // 如果不相等
            // 1. 删除数据
            [self.isSelectedMutableArr removeAllObjects];
            [self.isSelectedMutableArr addObject:[_typeArray objectAtIndex:buttonTag]];
            
            // 2. 执行UI
            // 2. 上个btn灭
            if (self.isSelectedButtonMutableArr.count){
                    UIButton *lastButton = (UIButton *)[self.isSelectedButtonMutableArr lastObject];
                    if (lastButton != customButton){
                    lastButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    [lastButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                }
            }
        }
    } else {
        [self.isSelectedMutableArr addObject:[_typeArray objectAtIndex:buttonTag]];
    }
    // 2.2 当前的亮
    customButton.layer.borderColor = C20.CGColor;
    [customButton setTitleColor:C21 forState:UIControlStateNormal];
    
    // 2.3 记录当前的button
    [self.isSelectedButtonMutableArr removeLastObject];
    [self.isSelectedButtonMutableArr addObject:customButton];

//    // 4. block
    NSString *typeName = [self.typeArray objectAtIndex:buttonTag];
    
    self.buttonClickBlock(typeName);
}

@end
