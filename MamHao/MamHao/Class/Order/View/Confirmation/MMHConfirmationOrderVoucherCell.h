//
//  MMHConfirmationOrderVoucherCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单 - 抵用券- 妈豆，积分等】
#import <UIKit/UIKit.h>
#import "MMHConfirmationOrderViewController.h"

@class MMHConfirmationOrderViewController;
typedef void(^inputTextFieldBlock)(UILabel *dymicLabel,UITextField *inputTextField,NSInteger inputText);
typedef void(^dymicSwitchIsClose) ();

typedef NS_ENUM(NSInteger, MMHConfirmationOrderVoucherType) {
    MMHConfirmationOrderVoucherTypeMBean,                       /**< 妈豆抵用*/
    MMHConfirmationOrderVoucherTypeGBIntegral,                  /**< GB积分*/
    MMHConfirmationOrderVoucherTypeMCIntegral,                  /**< MC积分*/
};

@interface MMHConfirmationOrderVoucherCell : UITableViewCell

@property (nonatomic,assign)CGFloat cellHeigt;
@property (nonatomic,assign)long transferVoucher;                                                   /**< 传递过来的积分*/
@property (nonatomic,assign)MMHConfirmationOrderVoucherType confirmationOrderVoucherType;           /**< 积分类型*/

// 判断是否是妈豆商品
@property (nonatomic,assign)MMHConfirmationRedirectionType  confirmationRedirectionType;
@property (nonatomic,assign)NSInteger transferMBeanPayCount;                                        /**< 传递过来妈豆用多少*/
@property (nonatomic,assign)CGFloat payPrice;                                                       /**< 需要支付的价格*/
@property (nonatomic,copy)inputTextFieldBlock inputTextFieldBlock;                                  /**<block*/
@property (nonatomic,copy)dymicSwitchIsClose dymicSwitchIsCloseBlock;
@property (nonatomic,assign)CGFloat cutPrice;                                                       /**< 传入的价格*/
@property (nonatomic,strong)UISwitch *dymicSwitch;                                                  /**< switch*/
@property (nonatomic,assign)NSInteger integralLimit;                                                /**< 积分使用限制*/

-(void)autoLayoutWithCellWithState:(BOOL)stateOn;
@end
