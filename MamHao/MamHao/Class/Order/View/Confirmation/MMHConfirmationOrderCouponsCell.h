//
//  MMHConfirmationOrderCouponsCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单- 优惠券】
#import <UIKit/UIKit.h>
#import "MMHCouponsSingleModel.h"

@interface MMHConfirmationOrderCouponsCell : UITableViewCell

@property (nonatomic,assign)long transferCouponCount;
@property (nonatomic,strong)MMHCouponsSingleModel *singleCouponsModel;                 /**< 优惠券*/
@end
