//
//  MMHConfirmationOrderAddressCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单 第一行 填写收货地址】
#import <UIKit/UIKit.h>
#import "MMHAddressSingleModel.h"                   // 单个地址model
@interface MMHConfirmationOrderAddressCell : UITableViewCell

@property (nonatomic,strong)MMHAddressSingleModel *addressSingleModel;
@property (nonatomic,assign)CGFloat cellHeight;

+(CGFloat)cellHeightWithAddressSingleModel:(MMHAddressSingleModel *)addressSingleModel;

@end
