//
//  MMHConfirmationOrderProductPriceCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderProductPriceCell.h"

@interface MMHConfirmationOrderProductPriceCell()
@property (nonatomic,strong)UIView *priceView;

@end

@implementation MMHConfirmationOrderProductPriceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 创建view
    self.priceView = [[UIView alloc]init];
    self.priceView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.priceView];
}


-(void)setPrice:(NSMutableArray<MMHOrderPriceModel> *)price{
    if (self.priceView.subviews.count){
        [self.priceView removeAllSubviews];
    }
    if (!self.priceView.subviews.count){
        // 1. priceView frame
        self.priceView.frame = CGRectMake(MMHFloat(15), MMHFloat(15), kScreenBounds.size.width - 2 *MMHFloat(11), [MMHTool contentofHeight:F5] + ([MMHTool contentofHeight:F5] + MMHFloat(12) * (price.count)));
        
        for (int i = 0;i < price.count;i++){
            MMHOrderPriceModel *orderPriceModel = [price objectAtIndex:i];
            // 创建左侧label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.text = orderPriceModel.entryName;
            fixedLabel.frame = CGRectMake(0, i * ([MMHTool contentofHeight:F5] + MMHFloat(10)),100, [MMHTool contentofHeight:F5]);
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = C6;
            fixedLabel.font = F5;
            [self.priceView addSubview:fixedLabel];
            
            // 创建右侧的price
            UILabel *priceLabel = [[UILabel alloc]init];
            priceLabel.backgroundColor = [UIColor clearColor];
            priceLabel.textColor = C6;
            priceLabel.textAlignment = NSTextAlignmentRight;
            priceLabel.text = [NSString stringWithFormat:@"￥%.2f",orderPriceModel.entryPrice];
            priceLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame), fixedLabel.frame.origin.y, self.priceView.bounds.size.width - fixedLabel.bounds.size.width - MMHFloat(3.), [MMHTool contentofHeight:F5]);
            priceLabel.font = F5;
            [self.priceView addSubview:priceLabel];
        }
    }
}

@end
