//
//  MMHConfirmationOrderCouponsCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderCouponsCell.h"

@interface MMHConfirmationOrderCouponsCell()
@property (nonatomic,strong)UILabel *fixedLabel;                /**< fixedLabel*/
@property (nonatomic,strong)UILabel *numberLabel;               /**< 剩余几张优惠券*/
@property (nonatomic,strong)UILabel *dymicLabel;                /**< 活动label*/

@end

@implementation MMHConfirmationOrderCouponsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 1. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = F2;
    self.fixedLabel.text = @"优惠券";
    CGSize fixedLabelSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    self.fixedLabel.frame = CGRectMake(MMHFloat(11), 0, fixedLabelSize.width, MMHFloat(50));
    [self addSubview:self.fixedLabel];
    
    // 2. 创建numberLabel
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = F2;
    self.numberLabel.textColor = C4;
    [self addSubview:self.numberLabel];
    
    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = F2;
    self.dymicLabel.textColor = C5;
    [self addSubview:self.dymicLabel];
    
    self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
}

-(void)setSingleCouponsModel:(MMHCouponsSingleModel *)singleCouponsModel{
    _singleCouponsModel = singleCouponsModel;
    self.dymicLabel.text = [NSString stringWithFormat:@"-%li元",(long)singleCouponsModel.voucherAmount];
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
    self.dymicLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(40) - MMHFloat(30), 0, dymicSize.width, MMHFloat(50));
}

-(void)setTransferCouponCount:(NSInteger)transferCouponCount{
    _transferCouponCount = transferCouponCount;
    if (self.transferCouponCount != 0){
        NSString *couponCountString = [NSString stringWithFormat:@"%li张可用",self.transferCouponCount];
        self.numberLabel.text = couponCountString;
        CGSize couponSize = [couponCountString sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(50))];
        self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame), 0, couponSize.width, MMHFloat(50));
    }
}

@end
