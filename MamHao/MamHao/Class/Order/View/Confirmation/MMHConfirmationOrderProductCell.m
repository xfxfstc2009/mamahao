//
//  MMHConfirmationOrderProductCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/22.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHConfirmationOrderProductCell.h"

@interface MMHConfirmationOrderProductCell()
@property (nonatomic,strong)UIView *shopActivityView;               /**< 商店活动view*/
@property (nonatomic,strong)MMHImageView *productImageView;         /**< 商品view*/
@property (nonatomic,strong)UILabel *productNameLabel;              /**< 商品名称Label*/
@property (nonatomic,strong)UILabel *priceLabel;                    /**< 价格label*/
@property (nonatomic,strong)UILabel *numberLabel;                   /**< 商品数量label */
@property (nonatomic,strong)UIView *activityView;                   /**< 活动view*/
@property (nonatomic,strong)UILabel *skuLabel;                      /**< skuLabel*/
@end

@implementation MMHConfirmationOrderProductCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // shopActivityView
    self.shopActivityView = [[UIView alloc]init];
    self.shopActivityView.backgroundColor = [UIColor clearColor];
    self.shopActivityView.stringTag = @"shopActivityView";
    [self addSubview:self.shopActivityView];
    
    // 创建imageView
    self.productImageView = [[MMHImageView alloc]init];
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.layer.masksToBounds = YES;
    [self.productImageView.layer setCornerRadius:MMHFloat(6.)];
    self.productImageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.productImageView.layer.borderWidth = .5f;
    [self addSubview:self.productImageView];
    
    // productName
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.stringTag = @"productNameLabel";
    self.productNameLabel.font = F4;
    self.productNameLabel.textColor = C6;
    self.productNameLabel.numberOfLines = 2;
    [self addSubview:self.productNameLabel];
    
    // priceLabel
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.textColor = C6;
    self.priceLabel.font = F4;
    self.priceLabel.numberOfLines = 1;
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    // numberLabel
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    self.numberLabel.font = F4;
    self.numberLabel.textColor = C4;
    [self addSubview:self.numberLabel];
    
    // 活动view
    self.activityView = [[UIView alloc]init];
    self.activityView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.activityView];
    
    // 创建skuLabel
    self.skuLabel = [[UILabel alloc]init];
    self.skuLabel.backgroundColor = [UIColor clearColor];
    self.skuLabel.font = F4;
    self.skuLabel.textColor = C4;
    self.skuLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.skuLabel];
}

-(void)setShopCartModel:(MMHShopCartModel *)shopCartModel{
    _shopCartModel = shopCartModel;
}

-(void)setSingleProduct:(MMHSingleProductWithShopCartModel *)singleProduct{
    _singleProduct = singleProduct;
    
    // 【判断是否有商店活动】
    if (self.shopCartModel.shopActivity.count) {
        if (!self.shopActivityView.subviews.count){
            // 根据count 确定view 高度
            self.shopActivityView.frame = CGRectMake(MMHFloat(11), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(11), MMHFloat(15) * self.shopCartModel.shopActivity.count + MMHFloat(5)*(self.shopCartModel.shopActivity.count - 1));
            // 1 加载label
            for (int i = 0 ; i < self.shopCartModel.shopActivity.count; i++){
                MMHOrderActivityModel *shopActivityModel = [self.shopCartModel.shopActivity objectAtIndex:i];
                self.shopActivityView.backgroundColor = [UIColor clearColor];
                
                UILabel *typeLabel = [[UILabel alloc]init];
                typeLabel.backgroundColor = [UIColor hexChangeFloat:@"ff4d61"];
                typeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                if (shopActivityModel.type == 1){
                    typeLabel.text = @" 减";
                } else if (shopActivityModel.type == 2){
                    typeLabel.text = @" 折";
                }
                typeLabel.frame = CGRectMake(0, 0, [MMHTool contentofHeight:typeLabel.font] + MMHFloat(4), [MMHTool contentofHeight:typeLabel.font] + MMHFloat(4));
                typeLabel.textColor = [UIColor whiteColor];
                [self.shopActivityView addSubview:typeLabel];
                
                // UILabel
                UILabel *activityTypeLabel = [[UILabel alloc]init];
                activityTypeLabel.backgroundColor = [UIColor clearColor];
                activityTypeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                activityTypeLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
//                activityTypeLabel.shadowOffset = CGSizeMake(.1, .1);
//                activityTypeLabel.shadowColor = [UIColor lightGrayColor];
                // 计算长度
                CGSize activitySize = [shopActivityModel.title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"提示"]])];
                activityTypeLabel.text = shopActivityModel.title.length?shopActivityModel.title:@"其他";
                activityTypeLabel.frame = CGRectMake(CGRectGetMaxX(typeLabel.frame) + MMHFloat(5),([MMHTool contentofHeight:typeLabel.font] + MMHFloat(4) -(MMHFloat(5) + activitySize.height))/2., activitySize.width, activitySize.height);
                [self.shopActivityView addSubview:activityTypeLabel];
                
                // 创建detail
                UILabel *activityDetailLabel = [[UILabel alloc]init];
                activityDetailLabel.backgroundColor = [UIColor clearColor];
                activityDetailLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
                activityDetailLabel.textAlignment = NSTextAlignmentLeft;
                activityDetailLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
                activityDetailLabel.text = shopActivityModel.desc;
                activityDetailLabel.numberOfLines = 1;
                // 计算长度
                CGSize detailSize = [shopActivityModel.desc sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(12.))];
                activityDetailLabel.frame = CGRectMake(CGRectGetMaxX(activityTypeLabel.frame) + MMHFloat(8), i * (MMHFloat(5) + activitySize.height), detailSize.width, MMHFloat(12.));
                [self.shopActivityView addSubview:activityDetailLabel];
            }
        }
    }
    if (self.cellIndexRow == 1){
        self.shopActivityView.hidden = NO;
    } else {
        self.shopActivityView.hidden = YES;
    }
    
    
    // 1. 图片
    self.productImageView.backgroundColor = [UIColor clearColor];
    self.productImageView.frame = CGRectMake(MMHFloat(11), MMHFloat(15), MMHFloat(70), MMHFloat(70));
    if (self.cellIndexRow == 1){
        if (self.shopCartModel.shopActivity.count){
            self.productImageView.frame = CGRectMake(MMHFloat(11), MMHFloat(10) + CGRectGetMaxY(self.shopActivityView.frame), MMHFloat(70), MMHFloat(70));
        }
    }
    [self.productImageView updateViewWithImageAtURL:singleProduct.itemPic];
    
    // 3.商品价格
    self.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",singleProduct.itemPrice];
    CGFloat priceLabelWidth = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.priceLabel.font])].width;
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - priceLabelWidth - MMHFloat(10), self.productImageView.orgin_y, priceLabelWidth, [MMHTool contentofHeight:self.priceLabel.font]);
    
    // 2. 商品名称
    self.productNameLabel.text = singleProduct.itemName;
    CGFloat productNameLabelHeight = [self.productNameLabel.text sizeWithCalcFont:self.productNameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.productImageView.frame) -MMHFloat(10) - MMHFloat(10) - priceLabelWidth, CGFLOAT_MAX)].height;
    
    if (productNameLabelHeight > [MMHTool contentofHeight:self.productNameLabel.font]){
        self.productNameLabel.numberOfLines = 2;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), self.productImageView.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(self.productImageView.frame) -MMHFloat(10) - MMHFloat(10) - priceLabelWidth, [MMHTool contentofHeight:self.productNameLabel.font] * 2);
    } else {
        self.productNameLabel.numberOfLines = 1;
        self.productNameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + MMHFloat(10), self.productImageView.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(self.productImageView.frame) -MMHFloat(10) - MMHFloat(10) - priceLabelWidth, [MMHTool contentofHeight:self.productNameLabel.font]);
    }
    
    
    // 4. 数量
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.productNameLabel.frame) + 5, MMHFloat(5) + CGRectGetMaxY(self.priceLabel.frame), kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(70 + 10) - self.productNameLabel.bounds.size.width - MMHFloat(5),[MMHTool contentofHeight:self.numberLabel.font]);
    
    self.numberLabel.text = [NSString stringWithFormat:@"x%li",(long)singleProduct.quantity];
    NSString *skuTempString = @"";
    for (int i = 0 ; i < singleProduct.specSKU.count;i++){
        skuTempString = [skuTempString stringByAppendingString:[NSString stringWithFormat:@"%@ ",[[singleProduct.specSKU objectAtIndex:i] skuValue]]];
    }
    self.skuLabel.text = skuTempString;
    self.skuLabel.frame = CGRectMake(self.productNameLabel.frame.origin.x, CGRectGetMaxY(self.productNameLabel.frame) + MMHFloat(5), self.productNameLabel.bounds.size.width, [MMHTool contentofHeight:self.skuLabel.font]);
    
    // 【商品活动】
    // 1. 判断是否有活动
    if (singleProduct.activity != nil){
        if (singleProduct.activity.count){
            if (!self.activityView.subviews.count){
                // 根据count 确定view 高度
                self.activityView.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.productImageView.frame) + MMHFloat(10), 200, MMHFloat(15) * singleProduct.activity.count + MMHFloat(5)*(singleProduct.activity.count - 1));
                self.activityView.backgroundColor = [UIColor yellowColor];
                // 1 加载label
                for (int i = 0 ; i < singleProduct.activity.count; i++){
                    MMHOrderProActivityModel *activityModel = [singleProduct.activity objectAtIndex:i];
                    // UILabel
                    UILabel *activityTypeLabel = [[UILabel alloc]init];
                    activityTypeLabel.backgroundColor = [UIColor grayColor];
                    activityTypeLabel.font = [UIFont systemFontOfSize:MMHFloat(12.)];
                    activityTypeLabel.textColor = [UIColor whiteColor];
                    activityTypeLabel.shadowOffset = CGSizeMake(.1, .1);
                    activityTypeLabel.shadowColor = [UIColor lightGrayColor];
                    // 计算长度
                    CGSize activitySize = [activityModel.type sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
                    activityTypeLabel.text = activityModel.type.length?activityModel.type:@"其他";
                    activityTypeLabel.frame = CGRectMake(0,i * (MMHFloat(5) + activitySize.height), activitySize.width, activitySize.height);
                    [self.activityView addSubview:activityTypeLabel];
                    
                    // 创建detail
                    UILabel *activityDetailLabel = [[UILabel alloc]init];
                    activityDetailLabel.backgroundColor = [UIColor clearColor];
                    activityDetailLabel.font = [UIFont systemFontOfSize:MMHFloat(12.)];
                    activityDetailLabel.textAlignment = NSTextAlignmentLeft;
                    activityDetailLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                    activityDetailLabel.text = activityModel.desc;
                    activityDetailLabel.numberOfLines = 1;
                    // 计算长度
                    CGSize detailSize = [activityModel.desc sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(12.))];
                    activityDetailLabel.frame = CGRectMake(CGRectGetMaxX(activityTypeLabel.frame) + MMHFloat(8), i * (MMHFloat(5) + activitySize.height), detailSize.width, MMHFloat(12.));
                    [self.activityView addSubview:activityDetailLabel];
                }
            }
        }
    }

}
@end
