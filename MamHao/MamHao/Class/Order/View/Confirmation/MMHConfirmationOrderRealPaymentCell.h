//
//  MMHConfirmationOrderRealPaymentCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【确认订单 - 实付款】
#import <UIKit/UIKit.h>

@interface MMHConfirmationOrderRealPaymentCell : UITableViewCell

// 用户实付款价格
@property (nonatomic,assign)CGFloat payPrice;

@end
