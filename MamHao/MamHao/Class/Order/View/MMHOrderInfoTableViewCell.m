//
//  MMHOrderInfoTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderInfoTableViewCell.h"

@implementation MMHOrderInfoTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark 创建view
-(void)createView{
    CGSize contentOfSize = [@"内容" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 *MMHFloat(11), CGFLOAT_MAX)];
    
    self.orderNumberLabel = [[UILabel alloc]init];
    self.orderNumberLabel.backgroundColor = [UIColor clearColor];
    self.orderNumberLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.orderNumberLabel.frame = CGRectMake( MMHFloat(15), MMHFloat(15), kScreenBounds.size.width - 2 *MMHFloat(11), contentOfSize.height);
    self.orderNumberLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview: self.orderNumberLabel];
    
    // 下单时间
    self.orderingTimeLabel = [[UILabel alloc]init];
    self.orderingTimeLabel.backgroundColor = [UIColor clearColor];
    self.orderingTimeLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.orderingTimeLabel.frame = CGRectMake(MMHFloat(15), CGRectGetMaxY(self.orderNumberLabel.frame) + MMHFloat(10),kScreenBounds.size.width - 2 *MMHFloat(11), contentOfSize.height);
    self.orderingTimeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.orderingTimeLabel];
    
    self.orderStateLabel = [[UILabel alloc]init];
    self.orderStateLabel.font=[UIFont fontWithCustomerSizeName:@"正文"];
    self.orderStateLabel.frame=CGRectMake( MMHFloat(15), CGRectGetMaxY(self.orderingTimeLabel.frame) + MMHFloat(10), self.orderingTimeLabel.frame.size.width,self.orderingTimeLabel.frame.size.height);
    self.orderStateLabel.textAlignment = NSTextAlignmentLeft;
    self.orderStateLabel.numberOfLines=1;
    self.orderStateLabel.backgroundColor=[UIColor clearColor];
    [self addSubview:self.orderStateLabel];
}

// 下单时间
-(void)setOrderingTime:(NSString *)orderingTime{
    self.orderingTimeLabel.text = [NSString stringWithFormat:@"下单时间:%@",orderingTime];
}

// 订单编号
-(void)setOrderNumber:(NSString *)orderNumber{
    self.orderNumberLabel.text = [NSString stringWithFormat:@"订单编号:%@",orderNumber];
}

// 订单状态
-(void)setOrderState:(NSString *)orderState{
    NSString *string = [NSString stringWithFormat:@"订单状态:%@",orderState];
    NSRange range1 = [string rangeOfString:@"订单状态:"];
    NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"%@",orderState]];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"浅灰"] range:range1];
    [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"红"] range:range2];
    self.orderStateLabel.attributedText = str;
}

@end
