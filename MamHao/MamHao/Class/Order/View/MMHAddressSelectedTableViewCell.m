//
//  MMHAddressSelectedTableViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAddressSelectedTableViewCell.h"

@implementation MMHAddressSelectedTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // 获取当前cell 高度
        CGFloat cellHeight = MMHFloat(50.);
        
        // 0.创建勾选image
        if (self.checkImageView == nil) {
            self.checkImageView = [[UIImageView alloc] init];
            self.checkImageView.backgroundColor = [UIColor clearColor];
            self.checkImageView.frame = CGRectMake(MMHFloat(11), (cellHeight-20)/2., 20, 20);
            [self addSubview:self.checkImageView];
        }
        
        // 创建fixedTitle
        UILabel * fixedLabel = [[UILabel alloc]init];
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        fixedLabel.textAlignment = NSTextAlignmentLeft;
        fixedLabel.font = [UIFont systemFontOfSize:MMHFloat(13.)];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.text = @"默认";
        CGSize fixedContentOfSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.checkImageView.frame)+ MMHFloat(10),(cellHeight - fixedContentOfSize.height)/2.,fixedContentOfSize.width,fixedContentOfSize.height);
        [self.contentView addSubview:fixedLabel];
        
        
        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        deleteButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11 + 20) - 2 * MMHFloat(60), 0, MMHFloat(60), cellHeight);
        [deleteButton addTarget:self action:@selector(deleteButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [deleteButton setImage:[UIImage imageNamed:@"address_icon_delte"] forState:UIControlStateNormal];
        deleteButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        deleteButton.backgroundColor = [UIColor clearColor];
        deleteButton.adjustsImageWhenHighlighted = NO;
        [deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        deleteButton.titleLabel.font = [UIFont systemFontOfSize:MMHFloat(13.)];
        [deleteButton setTitleEdgeInsets:UIEdgeInsetsMake(MMHFloat(15),  MMHFloat(5), MMHFloat(15), 0)];
        [deleteButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, MMHFloat(20))];
        [deleteButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        [self.contentView addSubview:deleteButton];
        
        UIButton *editButton = [UIButton buttonWithType:UIButtonTypeCustom];
        editButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11 ) -  MMHFloat(60), 0, MMHFloat(60), cellHeight);
        [editButton addTarget:self action:@selector(editButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [editButton setImage:[UIImage imageNamed:@"address_icon_editor"] forState:UIControlStateNormal];
        editButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        editButton.backgroundColor = [UIColor clearColor];
        editButton.adjustsImageWhenHighlighted = NO;
        [editButton setTitle:@"编辑" forState:UIControlStateNormal];
        editButton.titleLabel.font = [UIFont systemFontOfSize:MMHFloat(13.)];
        [editButton setTitleEdgeInsets:UIEdgeInsetsMake(MMHFloat(15),  MMHFloat(5), MMHFloat(15), 0)];
        [editButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, MMHFloat(20))];
        [editButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        [self.contentView addSubview:editButton];
    
    }
    return self;
}

- (void)setChecked:(BOOL)checked{
    if (checked) {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        self.checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    self.isChecked = checked;
}


-(void)editButtonClick:(UIButton *)sender{
    self.editBlock(self.addressSingleModel);
}
-(void)deleteButtonClick:(UIButton *)sender{
    self.deleteBlock(self.addressSingleModel);
}

@end
