//
//  MMHQueryLogisticsSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHQueryLogisticsSingleModel <NSObject>

@end

@interface MMHQueryLogisticsSingleModel : MMHFetchModel

@property (nonatomic,copy)NSString *platform;               /**< 物流名称*/
@property (nonatomic,copy)NSString *platformCode;           /**< 物流单号*/
@end
