//
//  MMHOrderDetailModel.m
//  MamHao
//
//  Created by SmartMin on 15/5/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderDetailModel.h"

@implementation MMHOrderDetailModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"priceInfo": @"price"};
}

@end
