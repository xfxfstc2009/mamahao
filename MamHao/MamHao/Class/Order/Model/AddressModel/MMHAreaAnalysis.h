//
//  MMHAreaAnalysis.h
//  MamHao
//
//  Created by SmartMin on 15/4/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHAreaTempModel.h"
@interface MMHAreaAnalysis : NSObject
@property (nonatomic,strong)NSArray *areaTempArray1;                // 记录所有Arr
@property (nonatomic,strong)NSMutableArray *addressCountMutableArr;

#pragma mark 1.返回省array 
-(NSArray *)readFileBackProvinceArray;

#pragma mark 根据选择的地区名字返回地区数组
-(MMHAreaTempModel *)backAreaArrayWithAreaName:(NSString *)areaName andAreaTempModel:(MMHAreaTempModel *)areaTempModel;

#pragma mark 返回城市
-(MMHAreaTempModel *)backCityArrayWithCityName:(NSString *)cityName areaArray:(NSArray *)areaArray andAreaTempModel:(MMHAreaTempModel *)areaTempModel;

#pragma mark Address Tool
+ (NSString *)cleanAreaString:(NSString *)string;

#pragma mark Address Get Area   
+ (NSArray *)getAddress:(NSString *)string;

#pragma mark 
-(void)createAreaPlist;

@end
