//
//  MMHAreaTempModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

typedef NS_ENUM(NSUInteger, MMHAreaType) {
    MMHAreaTypeProvrice,
    MMHAreaTypeCity,
    MMHAreaTypeArea,
    MMHAreaTypeEnd
    
};

@interface MMHAreaTempModel : MMHFetchModel

@property (nonatomic,strong)NSMutableArray *areaArray;
@property (nonatomic,assign)MMHAreaType areaType;
@property (nonatomic,strong)NSMutableArray *areaTempArray;
@end
