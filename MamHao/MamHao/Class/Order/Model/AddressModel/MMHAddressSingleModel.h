//
//  MMHAddressSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHAddressSingleModel <NSObject>


@end

@interface MMHAddressSingleModel : MMHFetchModel

@property (nonatomic,copy)NSString *deliveryAddrId;               // 收货地址唯一id
@property (nonatomic,copy)NSString *province;                     // 省
@property (nonatomic,copy)NSString *city;                         // 市
@property (nonatomic,copy)NSString *area;                         // 区
@property (nonatomic,copy)NSString *addrDetail;                   // 详细地址
@property (nonatomic,copy)NSString *consignee;                    // 收货人名字
@property (nonatomic,copy)NSString *phone;                        // 手机号码
@property (nonatomic,copy)NSString *telephone;                    // 座机
@property (nonatomic,assign)BOOL isDefault;                       // 是否默认
@property (nonatomic,copy)NSString *areaId;                       // 地区id

@property (nonatomic,copy)NSString *prefixAddress;                // 地址前缀



@end
