//
//  MMHAreaAnalysis.m
//  MamHao
//
//  Created by SmartMin on 15/4/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHAreaAnalysis.h"

@implementation MMHAreaAnalysis


#pragma mark 首次进入读取省
-(NSArray *)readFileBackProvinceArray{
    NSString * plistSrcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_TXT_NAME ofType:@"txt"];
    NSError *error1;
    NSString *str3=[NSString stringWithContentsOfFile:plistSrcPath encoding:NSUTF8StringEncoding error:&error1];
    NSMutableArray *provinceArray = [NSMutableArray array];
    
    if(error1==nil) {
        NSLog(@":读取文件成功%@:",str3);
       [provinceArray addObjectsFromArray: [self getDataWithString:str3]];
    } else {
        NSLog(@"读取文件失败%@",error1);
    }
    return provinceArray;

    
    
//    // 寻找当前程序所在的沙河线面的document 文件夹
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    // 获取数组里面第一个路径
//    NSString *documentsParh  = [paths objectAtIndex:0];
//    // 附加plist名称
//    NSString *realPath = [documentsParh stringByAppendingPathComponent:ADDRESS_PLIST_NAME];
//    // 文件操作
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    BOOL isFind = [fileManager fileExistsAtPath:realPath];
//    NSString * plistSrcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_PLIST_NAME ofType:nil];
//    if (!isFind){           // 复制文件进入沙河
//        [fileManager copyItemAtPath:plistSrcPath toPath:realPath error:nil];
//    }
//    NSMutableDictionary *dataOfCountry = [NSMutableDictionary dictionaryWithContentsOfFile:plistSrcPath];
//    
//    
//    
//    return provriceMutableArray;

}

-(NSDictionary *)getAllAreaWithBackDic{
    NSMutableDictionary *areaMutableDic = [NSMutableDictionary dictionary];
    [areaMutableDic  addEntriesFromDictionary: [self dataOfAnalysis]];
    return areaMutableDic;
}

#pragma mark 2. 将所有数据转换为array
-(NSArray *)getDataWithString:(NSString *)string{
    NSArray *areaTempArry =[string componentsSeparatedByString:@"\n"];
    NSMutableArray *addressMutableArr = [NSMutableArray array];
    // 2. 【讲item 加入编号】
    for (int i = 0 ; i < areaTempArry.count;i++){
        NSString * addressName = [areaTempArry objectAtIndex:i];
        NSString *newAddressName = [addressName stringByAppendingString:[NSString stringWithFormat:@"%i",i+1]];
        if (!([newAddressName hasPrefix:@"[[["] || [newAddressName hasPrefix:@"[["] || [newAddressName hasPrefix:@"end"])){
            [addressMutableArr addObject:newAddressName];
        }
    }

    return addressMutableArr;
}

#pragma mark 根据选择的地区名字返回地区数组
-(MMHAreaTempModel *)backAreaArrayWithAreaName:(NSString *)areaName andAreaTempModel:(MMHAreaTempModel *)areaTempModel{
    NSMutableArray *backAreaMutableArr = [NSMutableArray array];
    NSMutableDictionary *areaMutableDic = [NSMutableDictionary dictionaryWithDictionary:[self getAllAreaWithBackDic]];
    id areaTemp = [areaMutableDic objectForKey:[MMHAreaAnalysis cleanAreaString:areaName]];
    if ([areaTemp isKindOfClass:[NSArray class]]){
        NSArray *areaTempArray1 = areaTemp;
        if ([[areaTempArray1 objectAtIndex:0] isKindOfClass:[NSString class]]){     // 【城市结尾】
            [backAreaMutableArr addObject:areaTempArray1];
            areaTempModel.areaArray = [NSMutableArray arrayWithArray:backAreaMutableArr];
            areaTempModel.areaType = MMHAreaTypeCity;
            areaTempModel.areaTempArray = [NSMutableArray arrayWithArray:areaTempArray1];
        } else if ([[areaTempArray1 objectAtIndex:0]isKindOfClass:[NSArray class]]){    // 【区结尾】
            for (int i = 0; i <areaTempArray1.count;i++){
                NSArray *areaTempArray2 = [areaTempArray1 objectAtIndex:i];
                NSString *cityName = [areaTempArray2 objectAtIndex:0];
                [backAreaMutableArr addObject:cityName];
            }
            areaTempModel.areaArray = backAreaMutableArr;
            areaTempModel.areaType = MMHAreaTypeArea;
            areaTempModel.areaTempArray = [NSMutableArray arrayWithArray:areaTempArray1];
        }
    }
    
    return areaTempModel;
}

#pragma mark 返回区列表
-(MMHAreaTempModel *)backCityArrayWithCityName:(NSString *)cityName areaArray:(NSArray *)areaArray andAreaTempModel:(MMHAreaTempModel *)areaTempModel{
    NSLog(@"%@",areaArray);
    NSLog(@"%@",cityName);
    if ([[areaArray objectAtIndex:0] isKindOfClass:[NSString class]]){          // 【市结尾】
        NSInteger cityIndex = 0;
        for (int i = 0 ; i<areaArray.count ;i++){
            NSString *areaString = [areaArray objectAtIndex:i];
            if ([areaString isEqualToString:cityName]){
                cityIndex = i;
            }
        }
        areaTempModel.areaType = MMHAreaTypeEnd;
        NSArray *backAreaArr = @[cityName];
        areaTempModel.areaArray = [NSMutableArray arrayWithObject:backAreaArr];
    } else if ([[areaArray objectAtIndex:0] isKindOfClass:[NSArray class]]){    // 【区结尾】
        NSInteger areaIndex = 0;
        for (int i = 0 ; i < areaArray.count;i++){
            NSString *areaString = [[areaArray objectAtIndex:i] objectAtIndex:0];
            if ([areaString isEqualToString:cityName]){
                areaIndex = i;
            }
        }
        areaTempModel.areaType = MMHAreaTypeArea;
        areaTempModel.areaArray = [[areaArray objectAtIndex:areaIndex] objectAtIndex:1];
    }
    
    return areaTempModel;
}


#pragma mark - 数据操作
-(NSMutableDictionary *)dataOfAnalysis{
    NSString * srcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_PLIST_NAME ofType:nil];
    NSMutableDictionary *dataWithAddress = [[NSMutableDictionary alloc] initWithContentsOfFile:srcPath];
    NSMutableDictionary *areaDic = [NSMutableDictionary dictionary];
    areaDic = [dataWithAddress objectForKey:@"country"];
    return areaDic;
}

#pragma mark cleanAreaString
+ (NSString *)cleanAreaString:(NSString *)string{
    string = [string stringByReplacingOccurrencesOfString:@"[" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    return string;
}

#pragma mark Address Get Area
+ (NSArray *)getAddress:(NSString *)string{
    NSArray *areaArr = [string componentsSeparatedByString:@"]"];
    return areaArr;
}

#pragma mark 创建plist 文件
-(void)createAreaPlist{
    // 寻找当前程序所在的沙河线面的document 文件夹
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 获取数组里面第一个路径
    NSString *documentsParh  = [paths objectAtIndex:0];
    // 附加plist名称
    NSString *realPath = [documentsParh stringByAppendingPathComponent:ADDRESS_PLIST_NAME];
    // 文件操作
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isFind = [fileManager fileExistsAtPath:realPath];
    NSString * plistSrcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_PLIST_NAME ofType:nil];
    if (!isFind){           // 复制文件进入沙河
        [fileManager copyItemAtPath:plistSrcPath toPath:realPath error:nil];
    }
    NSMutableDictionary *dataOfCountry = [NSMutableDictionary dictionaryWithContentsOfFile:plistSrcPath];
    NSMutableArray *provriceMutable = [NSMutableArray array];
    provriceMutable = [dataOfCountry objectForKey:@"country" ];
//    return provriceMutable;
}

//        // 加入数据 - 1. 读入txt
//        NSString *txtSrcPath= [[NSBundle mainBundle]pathForResource:ADDRESS_TXT_NAME ofType:@"txt"];
//        NSError *error1;
//        NSString *str3=[NSString stringWithContentsOfFile:txtSrcPath encoding:NSUTF8StringEncoding error:&error1];
//        self.addressCountMutableArr = [NSMutableArray array];
//        if(error1==nil) {
//            NSMutableDictionary *areaDic = [NSMutableDictionary dictionary];
//           [areaDic addEntriesFromDictionary: [self jiexiDataWithArrayWithString:str3]];
//            // 写入
//            NSMutableDictionary *data1 = [NSMutableDictionary dictionaryWithContentsOfFile:plistSrcPath];
//            [data1 setObject:areaDic forKey:@"country"];
//            [data1 writeToFile:plistSrcPath atomically:YES];
//
//        } else {
//            NSLog(@"读取文件失败%@",error1);
//        }


#pragma mark 解析数据
-(NSMutableDictionary *)jiexiDataWithArrayWithString:(NSString *)string {
    NSArray *addressTempArry =[string componentsSeparatedByString:@"\n"];
    NSMutableArray *addressMutableArr = [NSMutableArray array];
    // 2. 【讲item 加入编号】
    for (int i = 0 ; i < addressTempArry.count;i++){
        NSString * addressName = [addressTempArry objectAtIndex:i];
        NSString *newAddressName = [addressName stringByAppendingString:[NSString stringWithFormat:@"%i",i+1]];
        [addressMutableArr addObject:newAddressName];
    }
    
    [self.addressCountMutableArr addObjectsFromArray:addressMutableArr];

    //  【解析数据】
    NSMutableArray *shengMutableArr = [NSMutableArray array];
    NSMutableArray *OneShiArr = [NSMutableArray array];
    NSMutableArray *OneQuArr = [NSMutableArray array];
    NSMutableArray *twoShiMutableArr;
    NSMutableArray *shiMutableArr = [NSMutableArray array];
    NSMutableDictionary *countryMutableDic = [NSMutableDictionary dictionary];
    
    //    NSMutableArray
    for (int i = 0 ; i <self.addressCountMutableArr.count ; i++){            // [浙江省]  [[杭州市]] [[[萧山区]]]
        NSString *addressName = [self.addressCountMutableArr objectAtIndex:i];
        if (!([addressName hasPrefix:@"[["] || [addressName hasPrefix:@"[[["])){            //【省】
            [shengMutableArr addObject:addressName];
            // 保存最后一个市数组
            if (OneQuArr.count){
                twoShiMutableArr = [[NSMutableArray alloc]init];
                NSString *quName = [OneShiArr lastObject];
                [twoShiMutableArr addObject:[MMHAreaAnalysis cleanAreaString:quName]];
                [twoShiMutableArr addObject:OneQuArr];
                //                [TwoShiDic setObject:OneQuArr forKey:quName];
                [shiMutableArr addObject:twoShiMutableArr];
            } else {
                if (OneShiArr.count){
                    NSString *shengName  =[shengMutableArr objectAtIndex:shengMutableArr.count - 2];
                    [countryMutableDic setObject:OneShiArr forKey:[MMHAreaAnalysis cleanAreaString:shengName]];
                    shiMutableArr = [NSMutableArray array];
                    twoShiMutableArr = [NSMutableArray array];
                    OneQuArr = [NSMutableArray array];
                    OneShiArr =[NSMutableArray array];
                }
            }
            
            // 保存数组
            if(shiMutableArr.count){
                //                [shengMutableArr addObject:shiMutableArr];
                [countryMutableDic setObject:shiMutableArr forKey:[MMHAreaAnalysis cleanAreaString:[shengMutableArr objectAtIndex:shengMutableArr.count - 2]]];
                shiMutableArr = [NSMutableArray array];
                twoShiMutableArr = [NSMutableArray array];
                OneQuArr = [NSMutableArray array];
            }
        } else {            // 市   区
            if (![addressName hasPrefix:@"[[["]){                       // 【市】
                if (OneQuArr.count){
                    twoShiMutableArr = [NSMutableArray array];
                    NSString *quName = [OneShiArr lastObject];
                    //                    [TwoShiDic setObject:OneQuArr forKey:quName];
                    [twoShiMutableArr addObject: [MMHAreaAnalysis cleanAreaString:quName]];
                    [twoShiMutableArr addObject:OneQuArr];
                    [shiMutableArr addObject:twoShiMutableArr];
                }
                [OneShiArr addObject:[MMHAreaAnalysis cleanAreaString:addressName]];
                
                OneQuArr = [NSMutableArray array];
            } else {    // 【区】
                [OneQuArr addObject:[MMHAreaAnalysis cleanAreaString:addressName]];
            }
        }
    }
    NSLog(@"%@",countryMutableDic);
    return countryMutableDic;
}


@end
