//
//  MMHOrderWithReceiptTypeSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHOrderWithReceiptTypeSingleModel : MMHFetchModel

@property (nonatomic,copy)NSString *typeId;
@property (nonatomic,copy)NSString *name;
@end
