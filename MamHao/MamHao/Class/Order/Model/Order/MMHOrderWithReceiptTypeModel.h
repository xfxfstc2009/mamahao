//
//  MMHOrderWithReceiptTypeModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 收货方式
#import "MMHFetchModel.h"

@protocol MMHOrderWithReceiptTypeModel <NSObject>

@end

@interface MMHOrderWithReceiptTypeModel : MMHFetchModel
@property (nonatomic,copy)NSString *deliveryId;
@property (nonatomic,copy)NSString *deliveryName;
@end
