//
//  MMHOrderPriceModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderPriceModel.h"

@implementation MMHOrderPriceModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"entryName": @"name",@"entryPrice":@"price"};
}

@end
