//
//  MMHOrderWithReceiptTypeSingleModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderWithReceiptTypeSingleModel.h"

@implementation MMHOrderWithReceiptTypeSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"typeId": @"id"};
}
@end
