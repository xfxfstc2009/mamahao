//
//  MMHOrderWithReceiptTypeModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderWithReceiptTypeModel.h"

@implementation MMHOrderWithReceiptTypeModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"deliveryId": @"id",@"deliveryName":@"name"};
}
@end
