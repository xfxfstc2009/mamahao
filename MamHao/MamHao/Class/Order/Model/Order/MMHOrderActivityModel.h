//
//  MMHOrderActivityModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol  MMHOrderActivityModel<NSObject>

@end


@interface MMHOrderActivityModel : MMHFetchModel
@property (nonatomic,copy)NSString *activityId;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,copy)NSString *meet;
@property (nonatomic,copy)NSString *title;
@end
