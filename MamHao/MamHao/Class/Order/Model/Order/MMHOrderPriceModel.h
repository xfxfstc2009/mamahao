//
//  MMHOrderPriceModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHOrderPriceModel <NSObject>

@end

@interface MMHOrderPriceModel : MMHFetchModel

@property (nonatomic,copy)NSString *entryName;              // 条目名称
@property (nonatomic,assign)CGFloat entryPrice;                  // 价格

@end
