//
//  MMHShopCartModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHSingleProductWithShopCartModel.h"
#import "MMHOrderActivityModel.h"                           // 店活动
@protocol MMHShopCartModel <NSObject>

@end

@interface MMHShopCartModel : MMHFetchModel
@property (nonatomic,copy)NSString *shopId;
@property (nonatomic,copy)NSString *shopName;
@property (nonatomic,copy)NSString *companyId;
@property (nonatomic,strong)NSMutableArray <MMHSingleProductWithShopCartModel> *goodsList;

@property (nonatomic,strong)NSArray <MMHOrderActivityModel>*shopActivity;           // 商店活动
@end
