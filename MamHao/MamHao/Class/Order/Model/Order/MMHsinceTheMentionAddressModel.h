//
//  MMHsinceTheMentionAddressModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 自提地址
#import "MMHFetchModel.h"

@protocol MMHsinceTheMentionAddressModel <NSObject>


@end

@interface MMHsinceTheMentionAddressModel : MMHFetchModel

@property (nonatomic,copy)NSString *addr;                       /**< 商店地址*/
@property (nonatomic,copy)NSString *telephone;                  /**< 商店电话*/
@property (nonatomic,copy)NSString *workTime;                   /**< 作息时间*/
@property (nonatomic,copy)NSString *holiday;                    /**< 休息日作息*/
@property (nonatomic,copy)NSString *shopName;                   /**< 商店名称*/
@end
