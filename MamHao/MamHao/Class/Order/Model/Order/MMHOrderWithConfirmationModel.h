//
//  MMHOrderWithConfirmationModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 确认订单Model
#import "MMHFetchModel.h"
#import "MMHOrderPriceModel.h"                              // 订单价格
#import "MMHsinceTheMentionAddressModel.h"                  // 商店地址
#import "MMHOrderWithReceiptTypeModel.h"                    // 收货方式
#import "MMHShopCartModel.h"                                // 购物车

@interface MMHOrderWithConfirmationModel : MMHFetchModel

@property (nonatomic,strong)MMHsinceTheMentionAddressModel *shopInfo;           // 商店信息
@property (nonatomic,strong)NSMutableArray <MMHOrderPriceModel> *price;                // 订单价格
@property (nonatomic,strong)NSArray <MMHOrderWithReceiptTypeModel> *delivery;   // 收货方式
@property (nonatomic,strong)NSArray <MMHShopCartModel> *data;                   // 购物车内容
@property (nonatomic,assign)NSInteger mBean;                                    // 妈豆
@property (nonatomic,copy)NSString *payPrice;                                   // 总价
@property (nonatomic,assign)CGFloat mailPrice;                                  /**< 运费价格*/
@property (nonatomic,assign)NSInteger couponCount;                              /**< 优惠券数量*/
@property (nonatomic,copy)NSString *orderNo;                                    /**< 订单编号*/

@property (nonatomic,assign)NSInteger gbIntegralLimit;                          /**< 积分使用限制*/
@property (nonatomic,assign)NSInteger mcIntegralLimit;                          /**< mc积分限制*/
@property (nonatomic,assign)NSInteger mbeanLimit;                               /**< 妈豆积分限制*/
@property (nonatomic,assign)CGFloat cutPayPrice;                                /**< 折扣后的价格*/

@end

