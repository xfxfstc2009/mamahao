//
//  MMHSingleProductWithShopCartModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHOrderSKUModel.h"
#import "MMHOrderProActivityModel.h"
@protocol MMHSingleProductWithShopCartModel <NSObject>


@end

@interface MMHSingleProductWithShopCartModel : MMHFetchModel

@property (nonatomic, assign)NSInteger commentCount;            // 评价数量
@property (nonatomic, assign)NSInteger shopId;                  //商店Id
@property (nonatomic, copy)NSString *orderNo; 
@property (nonatomic,copy)NSString *itemId;
@property (nonatomic, copy)NSString *templateId;                //商品的模板id
@property (nonatomic,assign)NSInteger quantity;                 // 数量
@property (nonatomic, assign)NSInteger allocatedCount;          //不要删
@property (nonatomic,copy)NSString *itemName;                   // 商品名称
@property (nonatomic,copy)NSString *itemPic;                    // 商品图片
@property (nonatomic,assign)CGFloat itemPrice;                  // 商品价格
@property (nonatomic,strong)NSArray<MMHOrderSKUModel> *specSKU;    // sku
@property (nonatomic,strong)NSArray<MMHOrderProActivityModel> *activity;  // 活动
@property (nonatomic, assign)BOOL isBeCommented; //判断是否评论过
@property (nonatomic,assign)BOOL isRefunded;                    /**< 是否申请过退款,1直接跳详情 0不跳*/

@end
