//
//  MMHOrderProActivityModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHOrderProActivityModel <NSObject>

@end


@interface MMHOrderProActivityModel : MMHFetchModel
@property (nonatomic,copy)NSString *activityId;     // 活动id
@property (nonatomic,copy)NSString *type;           // 活动类型
@property (nonatomic,copy)NSString *desc;    // 活动详细
@end
