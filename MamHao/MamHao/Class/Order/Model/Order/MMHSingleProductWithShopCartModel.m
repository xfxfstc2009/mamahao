//
//  MMHSingleProductWithShopCartModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSingleProductWithShopCartModel.h"

@implementation MMHSingleProductWithShopCartModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"specSKU": @"spec"};
}
- (instancetype)initWithJSONDict:(NSDictionary *)dict {
    if(self = [super initWithJSONDict:dict])
    if ([dict hasKey:@"allocatedCount"]) {
        self.allocatedCount = [dict[@"allocatedCount"] integerValue];
    }
    return self;
}

@end
