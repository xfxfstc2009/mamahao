//
//  MMHOrderListPay2Model.h
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【订单列表 - 直接去支付】

#import "MMHFetchModel.h"

@interface MMHOrderListPay2Model : MMHFetchModel

@property (nonatomic,copy)NSString *goodName;               /**< 商品名称*/
@property (nonatomic,copy)NSString *desc;                   /**< 商品描述*/
@property (nonatomic,copy)NSString *price;                    /**< 商品价格*/
@property (nonatomic,assign)NSInteger goodCount;                /**< 商品数量*/

@end
