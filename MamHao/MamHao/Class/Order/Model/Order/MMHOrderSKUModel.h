//
//  MMHOrderSKUModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHOrderSKUModel <NSObject>


@end

@interface MMHOrderSKUModel : MMHFetchModel

@property (nonatomic,copy)NSString *skuKey;
@property (nonatomic,copy)NSString *skuValue;
@end
