//
//  MMHRefundDetailModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHRefundRecipientModel.h"
#import "MMHRefundPayRefundModel.h"
#import "MMHRefundDeliveryAddrModel.h"
#import "MMHsinceTheMentionAddressModel.h"
@interface MMHRefundDetailModel : MMHFetchModel

@property (nonatomic,assign)NSInteger status;
@property (nonatomic,assign)NSInteger deliveryWay;                  // 配送方式
@property (nonatomic,assign)NSInteger refundTypeWithHome;
@property (nonatomic,assign)NSInteger refundLineId;
@property (nonatomic,copy)NSString *shopName;                       /**< 商店名称*/
@property (nonatomic,copy)NSString *cause;                          /**< 退货原因*/
@property (nonatomic,assign)NSInteger dealingType;                  /**< 线上还是线下 - 1. 线下 0.线上*/
@property (nonatomic,copy)NSString *arriveTime;                     /**< 支付宝打款时间*/

@property (nonatomic,copy)NSString *waybillNumber;                  // 物流编号
@property (nonatomic,copy)NSString *platformCode;                   // 物流code 

@property (nonatomic,assign)NSInteger refundWay;                    /**< 是否是上门自提的退货方式*/

@property (nonatomic,strong)MMHRefundRecipientModel *recipient;
@property (nonatomic,strong)MMHRefundPayRefundModel *payRefund;
@property (nonatomic,strong)MMHRefundDeliveryAddrModel *deliveryAddr;
@property (nonatomic,strong)MMHsinceTheMentionAddressModel *shopInfo;
@end
