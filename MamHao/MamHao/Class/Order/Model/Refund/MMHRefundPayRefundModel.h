//
//  MMHRefundPayRefundModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHRefundPayRefundModel : MMHFetchModel

@property (nonatomic,assign)CGFloat price;          /**< 打款数量*/
@property (nonatomic,copy)NSString *refundWay;      /**< 打款对象信息*/
@property (nonatomic,copy)NSString *dateArrival;    /**< 打款时间- 今日起1-3个工作日*/
@end
