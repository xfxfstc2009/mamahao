//
//  MMHRefundRequestModel.h
//  MamHao
//
//  Created by SmartMin on 15/7/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHRefundRequestModel : MMHFetchModel

@property (nonatomic,copy)NSString *account;
@property (nonatomic,assign)CGFloat price;
@property (nonatomic,assign)NSInteger type;
@property (nonatomic,assign)NSInteger deliveryWay;          /**< 用户选择的退货方式 1 2 3 */
@property (nonatomic,assign)BOOL isRefund;                  /**< yes 退款 no 退货*/
@end
