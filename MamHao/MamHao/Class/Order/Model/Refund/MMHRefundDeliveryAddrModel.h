//
//  MMHRefundDeliveryAddrModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHRefundDeliveryAddrModel : MMHFetchModel

@property (nonatomic,copy)NSString *shopName;           /**< 寄送店名*/
@property (nonatomic,copy)NSString *telephone;          /**< 返回电话号码*/
@property (nonatomic,copy)NSString *zipCode;            /**< 有阵编码*/
@property (nonatomic,copy)NSString *addr;               /**< 商店地址*/
@property (nonatomic,copy)NSString *remark;             /**< 备注*/

@end
