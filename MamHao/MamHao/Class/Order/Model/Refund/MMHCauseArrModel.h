//
//  MMHCauseArrModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/19.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHCauseArrModel : MMHFetchModel

@property (nonatomic,strong)NSArray *cause;

@end
