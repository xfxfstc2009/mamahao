//
//  MMHRefundRecipientModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHRefundRecipientModel : MMHFetchModel

@property (nonatomic,copy)NSString *recipientName;              /**< 收件人姓名 */
@property (nonatomic,copy)NSString *recipientShopName;          /**< 收件人所属门店*/
@property (nonatomic,copy)NSString *recipientPhone;             /**< 收件人电话*/
@property (nonatomic,copy)NSString *recipientArriveTime;        /**< 收件人收件时间*/
@end
