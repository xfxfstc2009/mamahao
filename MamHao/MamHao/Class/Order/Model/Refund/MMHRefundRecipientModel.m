//
//  MMHRefundRecipientModel.m
//  MamHao
//
//  Created by SmartMin on 15/5/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundRecipientModel.h"

@implementation MMHRefundRecipientModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"recipientName": @"name",@"recipientShopName":@"shopName",@"recipientPhone":@"phone",@"recipientArriveTime":@"arriveTime"};
}

@end
