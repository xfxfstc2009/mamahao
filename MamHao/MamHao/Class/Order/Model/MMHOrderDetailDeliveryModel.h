//
//  MMHOrderDetailDeliveryModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 订单详情 - 公司信息

#import "MMHFetchModel.h"

@interface MMHOrderDetailDeliveryModel : MMHFetchModel

@property (nonatomic,copy)NSString *deliveryAddr;               /**< 公司地址*/
@property (nonatomic,copy)NSString *deliveryName;               /**< 公司名称*/
@property (nonatomic,copy)NSString *deliveryPhone;              /**< 公司电话*/
@end
