//
//  MMHOrderDetailModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHOrderDetailDeliveryModel.h"             // 公司信息
#import "MMHOrderPriceModel.h"                      // 信息条目
#import "MMHShopCartModel.h"
#import "MMHsinceTheMentionAddressModel.h"          // 商店信息
#import "MMHDistributionModel.h"                    // 收件人信息
#import "MMHExpressModel.h"

@interface MMHOrderDetailModel : MMHFetchModel
@property (nonatomic,copy)NSString *createTime;                                     /**< 创建时间*/
@property (nonatomic,strong)NSArray <MMHShopCartModel> *data;                       /**< 商品列表*/
@property (nonatomic,strong)MMHOrderDetailDeliveryModel *deliveryInfo;              /**< 公司信息*/
@property (nonatomic,copy)NSString *deliveryMode;                                   /**< 取货方式*/
@property (nonatomic,strong)MMHExpressModel*logisticsInfo;                          /**< 快递*/
@property (nonatomic,assign)NSInteger orderStatus;                                  /**< 订单状态*/
@property (nonatomic,assign)CGFloat payPrice;                                       /**< 支付价格*/
@property (nonatomic,strong)NSArray <MMHOrderPriceModel>*priceInfo;                 /**< 价格条目*/
@property (nonatomic,copy)NSString *orderNo;                                        /**< 订单编号*/
@property (nonatomic,assign)NSTimeInterval remainMinutes;                           /**< 失效时间*/
@property (nonatomic,strong)MMHsinceTheMentionAddressModel *shopInfo;               /**< 商店信息*/
@property (nonatomic,strong)MMHDistributionModel *distributionInfo;                 /**< 收件人信息*/
@end
