//
//  MMHOrderListModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHOrderSingleModel.h"
@interface MMHOrderListModel : MMHFetchModel
@property (nonatomic,assign)NSInteger page;         /**< 页码*/
@property (nonatomic,strong)NSArray <MMHOrderSingleModel>* rows;                /**< 行内容*/
@property (nonatomic,assign)NSInteger total;        /**< 总数量*/
@end
