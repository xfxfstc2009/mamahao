//
//  MMHExpressContentModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHExpressContentModel <NSObject>

@end

@interface MMHExpressContentModel : MMHFetchModel

@property (nonatomic,copy)NSString *time;
@property (nonatomic,copy)NSString *desc;

@end
