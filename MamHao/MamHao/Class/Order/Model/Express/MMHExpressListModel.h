//
//  MMHExpressListModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/23.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHExpressModel.h"
@interface MMHExpressListModel : MMHFetchModel
@property (nonatomic,strong) NSArray <MMHExpressModel>*logistics;
@end
