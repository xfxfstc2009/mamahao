//
//  MMHExpressModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHExpressContentModel.h"
#import "MMHSingleProductWithShopCartModel.h"
#import "MMHOrderWithGoodsListModel.h"

@protocol MMHExpressModel <NSObject>

@end

@interface MMHExpressModel : MMHFetchModel

@property (nonatomic,strong)NSArray<MMHExpressContentModel> *data;
@property (nonatomic,strong)NSArray <MMHOrderWithGoodsListModel>*goodsList;         // 商品列表
@property (nonatomic,strong)NSString *platformName;                       /**< 物流名称*/
@property (nonatomic,strong)NSString *waybillNumber;                      /**< 物流单号*/
@property (nonatomic,strong)NSString *platformCode;                       /**< 物流code*/
@end
