//
//  MMHConfirmationIntegralModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/24.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 确认订单 获取妈豆积分
#import "MMHFetchModel.h"

@interface MMHConfirmationIntegralModel : MMHFetchModel

@property (nonatomic,assign)NSInteger gbCount;                  /**< goodBoyCount*/
@property (nonatomic,assign)NSInteger mcCount;                  /**< mamCareCount*/

@end
