//
//  MMHOrderSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHOrderWithGoodsListModel.h"

typedef NS_ENUM(NSInteger, MMHReceivingStateType) {
    MMHReceivingStateTypeDelivery = 1,                                      /**< 门店配送*/
    MMHReceivingStateTypeSelf = 2,                                          /**< 上门自提*/
    MMHReceivingStateTypeExpress = 3,                                       /**< 快递*/
};

@protocol MMHOrderSingleModel<NSObject>

@end

@interface MMHOrderSingleModel : MMHFetchModel
@property (nonatomic,assign)NSInteger companyId;                                    /**< 公司id*/
@property (nonatomic,assign)MMHReceivingStateType deliveryId;                       /**< 收货类别*/
@property (nonatomic,strong)NSArray <MMHOrderWithGoodsListModel>*goodsList;         /**< 商品列表*/
@property (nonatomic,copy)NSString *orderNo;                                        /**< 订单编号*/
@property (nonatomic,assign)NSInteger orderStatus;                                  /**< 订单状态*/
@property (nonatomic,assign)CGFloat payPrice;                                       /**< 支付总价*/
@property (nonatomic,assign)NSInteger remainMinutes;                                /**< 倒计时*/
@property (nonatomic,copy)NSString *shopId;                                         /**< 商店编号*/
@property (nonatomic,copy)NSString *shopName;                                       /**< 商店名称*/

// 本地使用
@property (nonatomic,assign)NSTimeInterval targetDate;                              /**< 目标时间*/
@property (nonatomic,assign)NSInteger remainMinutesWithLocal;                  /**< 服务端返回时间*/
@end
