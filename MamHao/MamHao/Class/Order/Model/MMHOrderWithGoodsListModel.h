//
//  MMHOrderWithGoodsListModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHOrderSKUModel.h"

@protocol MMHOrderWithGoodsListModel <NSObject>

@end

@interface MMHOrderWithGoodsListModel : MMHFetchModel
@property (nonatomic,copy)NSString *itemId;             /**< itemId*/
@property (nonatomic,copy)NSString *itemName;           /**< itemName*/
@property (nonatomic,copy)NSString *itemPic;            /**< itemPicture*/
@property (nonatomic,assign)CGFloat itemPrice;          /**< itemPrice*/
@property (nonatomic,assign)NSInteger quantity;         /**< 数量*/
@property (nonatomic,strong)NSArray <MMHOrderSKUModel> *spec;   /**<skuArray */
@end
