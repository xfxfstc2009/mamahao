//
//  MMHDistributionModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/19.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHDistributionModel : MMHFetchModel
@property (nonatomic,copy)NSString *addr;                           /**< 门店地址 */
@property (nonatomic,copy)NSString *arriveTime;                     /**< 到达时间*/
@property (nonatomic,copy)NSString *deliveryStaff;                  /**< 送货人*/
@property (nonatomic,copy)NSString *invoice;                        /**< */
@property (nonatomic,copy)NSString *phone;                          /**< 电话*/
@property (nonatomic,copy)NSString *shopName;                       /**< 商店名称*/
@end
