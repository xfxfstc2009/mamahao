//
//  MMHOrderRootViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderRootViewController.h"
#import "MMHOrderDetailViewController.h"
#import "HTHorizontalSelectionList.h"               // segment
#import "MMHOrderGoodsTableViewCell.h"              // 显示在商品列表的cell
#import "UIScrollView+MJRefresh.h"                  // 上拉加载
#import "MJRefreshFooter.h"
#import "MMHPopView.h"
#import "MMHNetworkAdapter+Cart.h"                  // 购物车

#import "MMHPraiseShowOrderViewController.h"            // 评价订单
#import "MMHOrderBuyAgainViewController.h"              // 再次购买
#import "MMHConfirmationOrderViewController.h"          // 确认订单
#import "MMHRefundDetailViewController.h"               // 退款退货
#import "MMHExpressDetailViewController.h"              // 物流信息
#import "MMHExpressListViewController.h"                
#import "MMHpaySuccessViewController.h"                 // 支付成功
#import "MMHCartViewController.h"                       // 购物车

// 支付
#import "MMHSettingPaySettingModel.h"                   // 支付
#import "MMHPayView.h"                                  // 支付
#import "MMHNetworkAdapter+Center.h"
#import "MMHNetworkAdapter+Order.h"
#import "MMHPaySafetyPassCodeViewController.h"
#import "MMHPayOrderModel.h"
#import "MMHAliPayHandle.h"
#import "MMHNetworkAdapter+Express.h"


@class MMHOrderDetailViewController;

@interface MMHOrderRootViewController ()<HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource,UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) HTHorizontalSelectionList *segmentList;               /**< segment*/
@property (nonatomic, strong) NSArray *segmentItemArray;                            /**< segmentItemArraty*/

@property (nonatomic,strong) UITableView *orderTableView;                           /**< tableView*/
@property (nonatomic,assign) MMHOrderType orderType;
@property (nonatomic,strong) NSMutableArray *dataSourceMutableArray;                /**< tableViewDataSource*/
@property (nonatomic,assign) NSInteger orderListPage;
@property (nonatomic,strong) MMHOrderSingleModel *orderSingleTempModel;             // tempModel

@property (nonatomic,copy)NSString *paymentTempOrder;                               /**< 支付成功需要临时订单*/
@property (nonatomic,strong)UIButton *tempButton;                                   /**< 用户点击临时按钮*/
@end

@implementation MMHOrderRootViewController

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];                         // 页面设置
    [self createSegment];                       // 创建segment
    [self arrayWithInit];
    [self createTableView];                     // 创建tableView
    [self pageLoadSendrequest];                 // 首次进入数据请求
    [self addLoadMoreView];
}

-(void)arrayWithInit{
    self.dataSourceMutableArray = [NSMutableArray array];
}

-(void)pageSetting{
    self.barMainTitle = @"我的订单";
    self.orderListPage = 1;                                     // 默认订单page = 1
    self.orderType = MMHOrderTypeAll;
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popToRootViewControllerAnimated:YES];
    }];
}

#pragma mark - HTHorizontalSelectionList
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, 0, self.view.frame.size.width, 40);
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    self.segmentList.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentItemArray = @[@" 全部 ",@" 待付款 ",@" 待发货 ",@" 待收货 ",@" 待评价 ",@" 退款退货 "];
    [self.view addSubview:self.segmentList];
    if (self.transferItemSelected){
        [self.segmentList setSelectedButtonIndex:self.transferItemSelected animated:YES];
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentItemArray.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentItemArray objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    if (index == 0){                                                 // 全部
        self.orderType = MMHOrderTypeAll;
    } else if (index == 1 ){        //待付款
        self.orderType = MMHOrderTypePendingPay;
    } else if (index == 2 ){        //待发货
        self.orderType = MMHOrderTypeBeShipped;
    } else if (index == 3 ){        //待收货
        self.orderType = MMHOrderTypeBeReceipt;
    } else if (index == 4 ){        //待评价
        self.orderType = MMHOrderTypeBeEvaluation;
    } else if (index == 5 ){        //退款退货
        self.orderType = MMHOrderTypeBeRefund;
    }
    self.orderListPage = 1;
    [self.dataSourceMutableArray removeAllObjects];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetOrderListWithType:weakSelf.orderType isLoadMore:NO];
}

-(void)pageLoadSendrequest{
    __weak typeof(self)weakSelf = self;         // 第一次进入请求all 数据
    if (weakSelf.transferItemSelected){
        if (weakSelf.transferItemSelected == 1 ){        //待付款
            weakSelf.orderType = MMHOrderTypePendingPay;
        } else if (weakSelf.transferItemSelected == 2 ){        //待发货
            weakSelf.orderType = MMHOrderTypeBeShipped;
        } else if (weakSelf.transferItemSelected == 3 ){        //待收货
            weakSelf.orderType = MMHOrderTypeBeReceipt;
        } else if (weakSelf.transferItemSelected == 4 ){        //待评价
            weakSelf.orderType = MMHOrderTypeBeEvaluation;
        } else if (weakSelf.transferItemSelected == 5 ){        //退款退货
            weakSelf.orderType = MMHOrderTypeBeRefund;
        }
        [weakSelf sendRequestWithGetOrderListWithType:weakSelf.orderType isLoadMore:NO];
    } else {
        [weakSelf sendRequestWithGetOrderListWithType:MMHOrderTypeAll isLoadMore:NO];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    self.orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.segmentList.bounds.size.height, self.view.bounds.size.width, self.view.bounds.size.height - self.segmentList.bounds.size.height) style:UITableViewStylePlain];
    self.orderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.orderTableView.delegate = self;
    self.orderTableView.dataSource = self;
    self.orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.orderTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.orderTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentify = @"cellIdentify";
    MMHOrderGoodsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if(!cell){
        cell = [[MMHOrderGoodsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    MMHOrderSingleModel *orderSingleModel = [self.dataSourceMutableArray objectAtIndex:indexPath.section];
    cell.orderType = self.orderType;
    
    cell.orderSingleModel = orderSingleModel;
    
    cell.leftButton.stringTag = [NSString stringWithFormat:@"leftButton-%li",(long)indexPath.section];
    cell.rightButton.stringTag = [NSString stringWithFormat:@"rightButton-%li",(long)indexPath.section];

    [cell.leftButton addTarget:self action:@selector(floatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.rightButton addTarget:self action:@selector(floatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark - UITableViewDelegate
-(void)loadMore{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetOrderListWithType:weakSelf.orderType isLoadMore:YES];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MMHOrderDetailViewController *orderDetailViewController = [[MMHOrderDetailViewController alloc]init];
    MMHOrderSingleModel *orderSingleModel = [self.dataSourceMutableArray objectAtIndex:indexPath.section];
    orderDetailViewController.orderType = self.orderType;;
    __weak typeof(self)weakSelf = self;
    orderDetailViewController.cancelOrder = ^(MMHOrderSingleModel *singleOrderModel){               //  取消订单
        weakSelf.orderSingleTempModel = singleOrderModel;                                           // 记录当前订单
        if ([weakSelf.dataSourceMutableArray containsObject:singleOrderModel]){
            [weakSelf performSelector:@selector(cancelOrderPerform:) withObject:singleOrderModel afterDelay:.5f];
        }
    };
    orderDetailViewController.deleteOrderBlock = ^ (MMHOrderSingleModel *singleOrderModel){         // 删除订单
        weakSelf.orderSingleTempModel = singleOrderModel;                                           // 记录当前订单
        if ([weakSelf.dataSourceMutableArray containsObject:singleOrderModel]){
            [weakSelf performSelector:@selector(deleteOrderPerform:) withObject:singleOrderModel afterDelay:.5f];
        }
    };
    orderDetailViewController.confirmReceiptBlock = ^(MMHOrderSingleModel *singleOrderModel){       // 确认收货
        weakSelf.orderSingleTempModel = singleOrderModel;                                           // 记录当前订单
        if ([weakSelf.dataSourceMutableArray containsObject:singleOrderModel]){
            [weakSelf performSelector:@selector(confirmReceiptPerform:) withObject:singleOrderModel afterDelay:.5f];
        }
    };
    orderDetailViewController.payPassBlock = ^(MMHOrderSingleModel *singleOrderModel){
        weakSelf.orderSingleTempModel = singleOrderModel;
       [weakSelf performSelector:@selector(deleteOrderPerform:) withObject:singleOrderModel afterDelay:.5f];
    };
    orderDetailViewController.praiseAllDoneBlock = ^(MMHOrderSingleModel *singleOrderModel){
        weakSelf.orderSingleTempModel = singleOrderModel;
        if ([weakSelf.dataSourceMutableArray containsObject:orderSingleModel]){
            [weakSelf.dataSourceMutableArray removeObject:orderSingleModel];
            [weakSelf.orderTableView reloadData];
        }

    };
    orderDetailViewController.transferOrderSingleModel = orderSingleModel;
    [self.navigationController pushViewController:orderDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView  =[[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.dataSourceMutableArray.count - 1){
        return MMHFloat(20);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return  MMHFloat(190);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    [cell addSeparatorLineWithType:SeparatorTypeSingle];
}

#pragma mark - ActionClick
-(void)floatButtonClick:(UIButton *)sender{
    UIButton *clickButton = (UIButton *)sender;
    self.tempButton = clickButton;
    clickButton.enabled = NO;
    
    NSArray *tempArr = [clickButton.stringTag componentsSeparatedByString:@"-"];
    if (tempArr.count == 2){
        MMHOrderSingleModel *orderSingleModel = [self.dataSourceMutableArray objectAtIndex:[[tempArr objectAtIndex:1] integerValue]];
        __weak typeof(self)weakSelf = self;
        if ([[tempArr objectAtIndex:0] isEqualToString:@"leftButton"]){
            if (self.orderType == MMHOrderTypeBeRefund){
                clickButton.enabled = YES;
                MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                MMHOrderWithGoodsListModel *orderWithGoodsModel = [orderSingleModel.goodsList lastObject];
                refundDetailViewController.refundDetailPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
                refundDetailViewController.transferOrderNo = orderSingleModel.orderNo;
                refundDetailViewController.transferItemId = orderWithGoodsModel.itemId;
                [weakSelf.navigationController pushViewController:refundDetailViewController animated:YES];
            } else {
                if (orderSingleModel.orderStatus == MMHOrderTypeBeShipped){                             // 【待发货】
                    [weakSelf sendRequestToRemindDeliveryWithOrderNo:orderSingleModel.orderNo];
                } else if (orderSingleModel.orderStatus == MMHOrderTypeBeReceipt){                      // 【待收货】
                    [weakSelf actionClickWithBeReceiptWithOrderSingleModel:orderSingleModel];
                } else if (orderSingleModel.orderStatus == MMHOrderTypeBeRefund){                       // 【待退货】
                    MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                    [self.navigationController pushViewController:refundDetailViewController animated:YES];
                    clickButton.enabled = YES;
                } else if (orderSingleModel.orderStatus == MMHOrderTypeFinish){                         // 【已完成】
                    
                } else if (orderSingleModel.orderStatus == MMHOrderTypeUnable){                         // 【已失效】
                    [weakSelf sendRequestToDeleteOrderWithOrderNo:orderSingleModel.orderNo animateWith:orderSingleModel];
                } else if (orderSingleModel.orderStatus == MMHOrderTypeCancel){                         // 【已取消】
                    [weakSelf sendRequestToDeleteOrderWithOrderNo:orderSingleModel.orderNo animateWith:orderSingleModel];
                }
            }
        } else if ([[tempArr objectAtIndex:0] isEqualToString:@"rightButton"]){
            if (orderSingleModel.orderStatus == MMHOrderTypePendingPay){                            // 【待付款 - 跳转立即付款】
                [self pushToPayment:[[tempArr lastObject] integerValue]];
            } else if (orderSingleModel.orderStatus == MMHOrderTypeBeReceipt){                      // 【待收货 - 确认收货】
                [weakSelf sendRequestWithConfirmReceiptWithOrderNo:orderSingleModel.orderNo animateWith:orderSingleModel];
            } else if (orderSingleModel.orderStatus == MMHOrderTypeBeEvaluation){                   // 【待评价 - 评价】
                MMHPraiseShowOrderViewController *praiseShowOrderViewController = [[MMHPraiseShowOrderViewController alloc]init];
                praiseShowOrderViewController.orderSingleModel = orderSingleModel;
                __weak typeof(self)weakSelf = self;
                praiseShowOrderViewController.pingjiaCallBackBlock = ^(BOOL isAllPraise){
                    if (isAllPraise){
                        if ([weakSelf.dataSourceMutableArray containsObject:orderSingleModel]){
                            [weakSelf.dataSourceMutableArray removeObject:orderSingleModel];
                            [weakSelf.orderTableView reloadData];
                        }
                    }
                };
                [self.navigationController pushViewController:praiseShowOrderViewController animated:YES];
                clickButton.enabled = YES;
            } else if (orderSingleModel.orderStatus == MMHOrderTypeFinish){                         // 【已完成 - 跳转再次购买】
                if (orderSingleModel.goodsList.count == 1){
                    MMHOrderWithGoodsListModel *goodsModel = [orderSingleModel.goodsList lastObject];
                    [weakSelf buyAgainWithOrderNo:orderSingleModel.orderNo itemId:goodsModel.itemId];
                } else if (orderSingleModel.goodsList.count > 1){
                    MMHOrderBuyAgainViewController *buyAgainViewController = [[MMHOrderBuyAgainViewController alloc]init];
                    buyAgainViewController.transferOrderSingleModel = orderSingleModel;
                    [self.navigationController pushViewController:buyAgainViewController animated:YES];
                    clickButton.enabled = YES;
                }
            }
        }
    }
}

#pragma mark 待收货点击
-(void)actionClickWithBeReceiptWithOrderSingleModel:(MMHOrderSingleModel *)orderSingleModel{
    __weak typeof(self)weakSelf = self;
    if ((orderSingleModel.deliveryId == MMHReceivingStateTypeDelivery) || (orderSingleModel.deliveryId == MMHReceivingStateTypeSelf)){                                          // 货到付款 || 上门自提
        [weakSelf sendRequestWithQueryShopInfoWithOrderNo:orderSingleModel.orderNo shopId:orderSingleModel.shopId];
    } else if (orderSingleModel.deliveryId == MMHReceivingStateTypeExpress){  // 快递
        [weakSelf sendRequestToGetExpressInfoWithOrderNo:orderSingleModel.orderNo];
    }
}

#pragma mark 加载更多
-(void)addLoadMoreView{
    __weak typeof(self) weakSelf = self;
    [weakSelf.orderTableView addMMHInfiniteScrollingWithActionHandler:^{
        [weakSelf sendRequestWithGetOrderListWithType:weakSelf.orderType isLoadMore:YES];
    }];
}

#pragma mark - SendRequest
#pragma mark 请求订单列表
-(void)sendRequestWithGetOrderListWithType:(NSInteger)type isLoadMore:(BOOL)isLoadMore{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    
    NSString *tempType = @"";
    if (type == MMHOrderTypeAll){
        tempType = @"";
    } else {
        tempType = [NSString stringWithFormat:@"%li",(long)type];
    }
    
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetOrderListWithQueryType:tempType page:self.orderListPage from:nil  succeededHandler:^(MMHOrderListModel *orderListModel) {
        [weakSelf.view hideProcessingView];
        weakSelf.orderTableView.footer.hidden = NO;
        if (!weakSelf.orderTableView.isScrollEnabled){
            weakSelf.orderTableView.scrollEnabled = YES;
        }
        [weakSelf.orderTableView dismissPrompt];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.dataSourceMutableArray addObjectsFromArray:orderListModel.rows];
        // 过滤status = 8
        for (int i = 0 ; i < strongSelf.dataSourceMutableArray.count ; i++){
            MMHOrderSingleModel *singleOrderModel = [strongSelf.dataSourceMutableArray objectAtIndex:i];
            if (singleOrderModel.orderStatus == MMHOrderTypeDelete){
                [strongSelf.dataSourceMutableArray removeObject:singleOrderModel];
            }
        }
        
        strongSelf.orderListPage++;
        [strongSelf.orderTableView.footer endRefreshing];
        
        // 时间设置
        for (int i = 0 ; i <orderListModel.rows.count;i++){
            MMHOrderSingleModel *orderSingleModel = [orderListModel.rows objectAtIndex:i];
            [strongSelf timeSettingWithOrderSingleModel:orderSingleModel];
        }
        
        [strongSelf.orderTableView reloadData];
        if (!isLoadMore){
            if (strongSelf.dataSourceMutableArray.count){
            [strongSelf.orderTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
            }
        }
        if (orderListModel.total <= 15){
            [strongSelf.orderTableView.pullToRefreshView stopAnimating];
            [strongSelf.orderTableView.infiniteScrollingView stopAnimating];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        [strongSelf.orderTableView reloadData];
        strongSelf.orderTableView.footer.hidden = YES;
        
        if (strongSelf.dataSourceMutableArray.count){
            [strongSelf.orderTableView.footer endRefreshing];
        } else {
            [strongSelf.orderTableView showPrompt:@"没有数据" withImage:nil andImageAlignment:MMHPromptImageAlignmentTop];
            strongSelf.orderTableView.scrollEnabled = NO;
        }
        [strongSelf.orderTableView.pullToRefreshView stopAnimating];
        [strongSelf.orderTableView.infiniteScrollingView stopAnimating];
    }];
}

#pragma mark 确认收货
-(void)sendRequestWithConfirmReceiptWithOrderNo:(NSString *)orderNo animateWith:(MMHOrderSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchModelConfirmReceiptWithOrderNo:orderNo from:nil succeededHandler:^(MMHOrderDetailModel *orderListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:nil message:@"确认收货成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf confirmReceiptPerform:singleModel];
        }]show];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 再次购买
-(void)buyAgainWithOrderNo:(NSString *)orderNo itemId:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestBuyAgain:orderNo items:itemId from:nil succeededHandler:^(MMHExpressModel *expressModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        MMHCartViewController *cartViewController = [[MMHCartViewController alloc]init];
        [strongSelf.navigationController pushViewController:cartViewController animated:YES];
    } failedHandler:^(NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =  weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 查看门店信息
-(void)sendRequestWithQueryShopInfoWithOrderNo:(NSString *)orderNo shopId:(NSString *)shopId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelQueryShopInfoWithOrderNo:orderNo shopId:shopId from:nil succeededHandler:^(MMHsinceTheMentionAddressModel *shopInfoModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf shopInfoShowWithShopInfoModel:shopInfoModel];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 提醒发货
-(void)sendRequestToRemindDeliveryWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelToRemindDeliveryWithOrderNo:orderNo from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTips:@"妈妈好已为您提醒卖家发货了哟"];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark - 删除订单
-(void)sendRequestToDeleteOrderWithOrderNo:(NSString *)orderNo animateWith:(MMHOrderSingleModel *)singleModel{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchmodelDeleteOrderWithOrderNo:orderNo from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong  typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf performSelector:@selector(deleteOrderPerform:) withObject:singleModel afterDelay:.5f];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 立即付款
-(void)sendRequestToPayWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] sendRequestToPayWithOrderNo:orderNo from:nil succeededHandler:^(MMHOrderListPay2Model *payInfoModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        if ([payInfoModel.price floatValue] != 0){// 【跳转支付宝】
            [strongSelf weakUpAliPayWithPrice:payInfoModel.price orderNo:orderNo productName:payInfoModel.goodName productDesc:payInfoModel.desc];
        } else {                                  // 【跳转妈妈好】
            [strongSelf sendRequestWithGetSettingStatusWithOrderNo:orderNo];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 请求接口判断是否注册支付密码
-(void)sendRequestWithGetSettingStatusWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestGetIsSettingPayPasswordFrom:nil succeededHandler:^(MMHSettingPaySettingModel *paySettedModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (paySettedModel.isSetted){               // 设置了
            MMHPayView *view = [[MMHPayView alloc]init];
            view.passCodeBlock = ^(NSString *passcode){
                [strongSelf sendRequestWithVerificationPayPassCode:passcode andOrderNo:orderNo];
            };
            view.resetRedirectBlock = ^(){
                MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
                paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
                paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeReset;
                [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
            };
            [view payViewShow];
        } else {                                            // 【提示去设置】
            [[UIAlertView alertViewWithTitle:@"支付安全" message:@"您当前没有设置妈豆支付密码，是否立即设置" buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1){
                    MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
                    paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
                    paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeOrderHome;
                    [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
                } else {
                    [strongSelf.view showTips:@"您已取消设置安全密码"];
                }
            }]show];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 验证支付密码
-(void)sendRequestWithVerificationPayPassCode:(NSString *)payPassCode andOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithCheckPayPassword:payPassCode from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:nil message:@"成功" buttonTitles:@[@"确定"] callback:nil]show];
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestMBeanCallbackWithOrderNo:orderNo];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}


#pragma mark - OtherManager
#pragma mark 取消订单效果
-(void)cancelOrderPerform:(MMHOrderSingleModel *)singleOrderModel{
    if ([self.dataSourceMutableArray containsObject:singleOrderModel]){
        NSInteger singleOrderModelSection = [self.dataSourceMutableArray indexOfObject:singleOrderModel];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:singleOrderModelSection];
        [self.dataSourceMutableArray removeObject:singleOrderModel];
        [self.orderTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        if (self.segmentList.selectedButtonIndex == 0){
            [self.orderTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
            [self performSelector:@selector(cancelOrderToCanceledOrderPerform:) withObject:self.orderSingleTempModel afterDelay:1];
        }
    } else {
        [self.view showTips:@"取消订单失败，请稍后再试"];
    }
}

#pragma mark 删除订单效果
-(void)deleteOrderPerform:(MMHOrderSingleModel *)singleOrderModel{
    if ([self.dataSourceMutableArray containsObject:singleOrderModel]){
        NSInteger singleOrderModelSection = [self.dataSourceMutableArray indexOfObject:singleOrderModel];
        [self.dataSourceMutableArray removeObject:singleOrderModel];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:singleOrderModelSection];
        [self.orderTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        [self performSelector:@selector(reloadTableView) withObject:nil afterDelay:.4f];
    } else {
        [self.view showTips:@"删除失败，请稍后再试"];
    }
}

-(void)reloadTableView{
    if (self.dataSourceMutableArray.count){
        [self.dataSourceMutableArray removeAllObjects];
    }
    __weak typeof(self)weakSelf = self;
        weakSelf.orderListPage = 1;
    if (self.transferItemSelected == 0){
        [weakSelf sendRequestWithGetOrderListWithType:MMHOrderTypeAll isLoadMore:NO];
    } else {
        [weakSelf sendRequestWithGetOrderListWithType:weakSelf.orderType isLoadMore:NO];
    }
}

#pragma mark 取消订单 To 已取消订单
-(void)cancelOrderToCanceledOrderPerform:(MMHOrderSingleModel *)singleOrderModel{
    // 3. 增加退货地址
    self.orderSingleTempModel.orderStatus = MMHOrderTypeAll;
    [self.dataSourceMutableArray insertObject:singleOrderModel atIndex:0];
    NSMutableIndexSet *tuihuodizhiSet = [[NSMutableIndexSet alloc]initWithIndex:0];
    [self.orderTableView insertSections:tuihuodizhiSet withRowAnimation:UITableViewRowAnimationRight];
    [self performSelector:@selector(reloadTableView) withObject:nil afterDelay:.4f];
}

#pragma mark  确认收货效果
-(void)confirmReceiptPerform:(MMHOrderSingleModel *)singleOrderModel{
    if ([self.dataSourceMutableArray containsObject:singleOrderModel]){
        NSInteger singleOrderModelSection = [self.dataSourceMutableArray indexOfObject:singleOrderModel];
        [self.dataSourceMutableArray removeObject:singleOrderModel];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:singleOrderModelSection];
        [self.orderTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        [self performSelector:@selector(reloadTableView) withObject:nil afterDelay:.4f];
    } else {
        [self.view showTips:@"操作失败，请稍后再试"];
    }
}

#pragma mark  立即付款
-(void)pushToPayment:(NSInteger)index{
    MMHOrderSingleModel *orderSingleModel = [self.dataSourceMutableArray objectAtIndex:index];
    __weak typeof(self)weakSelf = self;
    self.paymentTempOrder = orderSingleModel.orderNo;
    [weakSelf sendRequestToPayWithOrderNo:orderSingleModel.orderNo];
}

#pragma mark 妈豆付款回调
-(void)sendRequestMBeanCallbackWithOrderNo:(NSString *)orderNo{
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithCallBackWithMBeanOrder:orderNo from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [UIAlertView alertViewWithTitle:nil message:@"妈豆支付成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            for (int i = 0 ; i < strongSelf.dataSourceMutableArray.count;i++){
                MMHOrderSingleModel *orderSingleModel = [strongSelf.dataSourceMutableArray objectAtIndex:i];
                if ([orderSingleModel.orderNo isEqualToString:orderNo]){
                    NSInteger orderIndex = [strongSelf.dataSourceMutableArray indexOfObject:orderSingleModel];
                    [strongSelf.dataSourceMutableArray removeObject:orderSingleModel];
                    [strongSelf.orderTableView deleteSections:[NSIndexSet indexSetWithIndex:orderIndex] withRowAnimation:UITableViewRowAnimationLeft];
                }
            }
        }];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}


#pragma mark 唤起支付宝进行支付
-(void)weakUpAliPayWithPrice:(NSString *)price orderNo:(NSString *)orderNo productName:(NSString *)productName productDesc:(NSString *)productDesc {
    MMHPayOrderModel *payOrderModel = [[MMHPayOrderModel alloc]init];
    payOrderModel.amount = price;
    payOrderModel.tradeNO = orderNo;
    payOrderModel.productName = productName;
    payOrderModel.productDescription = productDesc;
    [MMHAliPayHandle goPayWithOrder:payOrderModel];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(aliPayNotification:) name:MMHAlipayNotification object:nil];
}

#pragma mark 支付宝支付通知
-(void)aliPayNotification:(NSNotification *)note{
    NSString *payNotificationState = [note.userInfo objectForKey:@"resultStatus"];
    if([payNotificationState integerValue] == 6001){            // 【用户中途取消】
        [self.view showTips:@"用户中途取消"];
    } else if ([payNotificationState integerValue] == 6002){    // 【网络连接出错】
        [self.view showTips:@"网络连接出错"];
    } else if ([payNotificationState integerValue] == 4000){    // 【订单支付失败】
        [self.view showTips:@"订单支付失败"];
    } else if ([payNotificationState integerValue] == 8000){     // 【订单正在处理中】
        [self.view showTips:@"订单正在处理中"];
    } else if ([payNotificationState integerValue] == 9000){     // 【订单支付成功】
        [[UIAlertView alertViewWithTitle:@"成功" message:@"订单支付成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            MMHpaySuccessViewController *paysuccessViewController = [[MMHpaySuccessViewController alloc]init];
            paysuccessViewController.paySuccessPageUsingType = MMHPaySuccessPageUsingTypeOrderRoot;
            UINavigationController *paySuccessNavController = [[UINavigationController alloc]initWithRootViewController:paysuccessViewController];
            __weak typeof(self)weakSelf = self;
            paysuccessViewController.redirectionPageBlock = ^(){
                for (int i = 0 ; i < weakSelf.dataSourceMutableArray.count ; i++){
                    MMHOrderSingleModel *tempOrderSingleModel = [weakSelf.dataSourceMutableArray objectAtIndex:i];
                    if ([tempOrderSingleModel.orderNo isEqualToString:weakSelf.paymentTempOrder]){
                        [weakSelf.dataSourceMutableArray removeObject:tempOrderSingleModel];
                        NSIndexSet *deleteSet = [NSIndexSet indexSetWithIndex:[weakSelf.dataSourceMutableArray indexOfObject:tempOrderSingleModel]];
                        [weakSelf.orderTableView deleteSections:deleteSet withRowAnimation:UITableViewRowAnimationLeft];
                    }
                }
            };
            paysuccessViewController.transfetOrderNo = self.paymentTempOrder;
            [self presentViewController:paySuccessNavController animated:YES completion:nil];
        }]show];
    }
}

-(void)shopInfoShowWithShopInfoModel:(MMHsinceTheMentionAddressModel *)shopInfoModel{
    CGFloat xWidth = self.view.bounds.size.width - 70.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    MMHPopView *poplistview = [[MMHPopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.shopInfoModel = shopInfoModel;
    poplistview.usingType = MMHUsingTypeOrderShopInfo;
    
    
    poplistview.callBlock = ^(NSString *phoneNumber){                       // 拨打电话
        __weak MMHOrderRootViewController *weakViewController = self;
        [MMHTool callCustomerServer:weakViewController.view phoneNumber:phoneNumber];
    };
    [poplistview viewShow];
}

#pragma mark 时间初始化
-(void)timeSettingWithOrderSingleModel:(MMHOrderSingleModel *)orderSingleModel{         // 失效时间
    if (orderSingleModel.orderStatus == MMHOrderTypePendingPay){
        if (orderSingleModel.remainMinutes){                                            // 获取到未来时间
            orderSingleModel.targetDate = [MMHTool getTargetDateTimeWithTimeInterval:orderSingleModel.remainMinutes];
        }
    }
}


#pragma mark 物流
-(void)sendRequestToGetExpressInfoWithOrderNo:(NSString *)orderNo{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestToGetExpressOrderWithOrderNo:orderNo from:nil succeededHandler:^(MMHExpressListModel *expressListModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (expressListModel.logistics.count == 1){
            strongSelf.tempButton.enabled = YES;
            MMHExpressDetailViewController *expressViewController = [[MMHExpressDetailViewController alloc]init];
            expressViewController.transferExpressModel = [expressListModel.logistics lastObject];
            [strongSelf.navigationController pushViewController:expressViewController animated:YES];
        } else if (expressListModel.logistics.count > 1){
            MMHExpressListViewController *expressListViewController = [[MMHExpressListViewController alloc]init];
            expressListViewController.logistics = expressListModel.logistics;
            [strongSelf.navigationController pushViewController:expressListViewController animated:YES];
            strongSelf.tempButton.enabled = YES;
        }
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

@end
