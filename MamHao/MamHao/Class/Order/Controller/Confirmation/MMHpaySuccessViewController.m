//
//  MMHpaySuccessViewController.m
//  MamHao
//
//  Created by SmartMin on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHpaySuccessViewController.h"
#import "MMHNetworkAdapter+Order.h"
#import "MMHGuessYouLikeCell.h"                 // 猜你喜欢
#import "MMHNetworkAdapter+Product.h"           // 猜你喜欢接口
#import "MMHNavigationController.h"
#import "MMHScanningViewController.h"
#import "MMHOrderRootViewController.h"
#import "MMHTabBarController.h"
#import "MMHProductDetailViewController1.h"


@class MMHOrderRootViewController;

@interface MMHpaySuccessViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *paySuccessTableView;
@property (nonatomic,strong)NSMutableArray *paySuccessMutableArr;

@property (nonatomic,strong)UIButton *orderButton;
@property (nonatomic,strong)UIButton *homeButton;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,copy)NSString *invoiceType;
@property (nonatomic,copy)NSString *invoiceString;
@property (nonatomic,strong)UIButton *confirmButton;
@property (nonatomic,strong)UILabel *confirmLabel;
@property (nonatomic,assign)BOOL isSuccess;
@property (nonatomic, strong) MMHGuessYouLikeListModel *guessYouLikeListModel;          /**< 猜你喜欢列表*/

@end

@implementation MMHpaySuccessViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self dataWithInit];
    [self createTableView];
    [self createNotifi];
    [self sendRequestWithGuessYouLike];
    
}

-(void)pageSetting{
    self.barMainTitle = @"交易详情";
    UIButton *leftButton = [self leftBarButtonWithTitle:nil barNorImage:nil barHltImage:nil action:^{
        
    }];
    leftButton.hidden = YES;
    
}

-(void)dataWithInit{
    self.paySuccessMutableArr = [NSMutableArray array];
    NSArray *tempArr = @[@[@"付款成功",@"发票"]];
    [self.paySuccessMutableArr addObjectsFromArray:tempArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.paySuccessTableView){
        self.paySuccessTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.paySuccessTableView setHeight:self.view.bounds.size.height];
        self.paySuccessTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.paySuccessTableView.delegate = self;
        self.paySuccessTableView.dataSource = self;
        self.paySuccessTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.paySuccessTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.paySuccessTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.paySuccessMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.paySuccessMutableArr objectAtIndexedSubscript:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString * cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            // 创建付款成功
            UILabel *successLabel = [[UILabel alloc]init];
            successLabel.backgroundColor = [UIColor clearColor];
            successLabel.font = [UIFont systemFontOfCustomeSize:24.];
            successLabel.textAlignment = NSTextAlignmentCenter;
            successLabel.textColor = [UIColor colorWithCustomerName:@"粉"];
            successLabel.frame = CGRectMake(0, MMHFloat(26), kScreenBounds.size.width, [MMHTool contentofHeight:successLabel.font]);
            successLabel.text = @"付款成功啦！";
            successLabel.stringTag = @"successLabel";
            [cellWithRowOne addSubview:successLabel];
            
            // 创建subLabel
            UILabel *subLabel = [[UILabel alloc]init];
            subLabel.backgroundColor = [UIColor clearColor];
            subLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            subLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            subLabel.textAlignment = NSTextAlignmentCenter;
            subLabel.frame = CGRectMake(0, CGRectGetMaxY(successLabel.frame) + MMHFloat(16), kScreenBounds.size.width, [MMHTool contentofHeight:subLabel.font]);
            subLabel.stringTag = @"subLabel";
            subLabel.text = @"我们即刻开始准备您的宝贝，请耐心等待！";
            [cellWithRowOne addSubview:subLabel];
            
            // 创建button
            self.orderButton = [self createButtonWithIndex:0];
            self.orderButton.frame = CGRectMake(2 * MMHFloat(16), CGRectGetMaxY(subLabel.frame) + MMHFloat(25), (kScreenBounds.size.width - 6 * MMHFloat(16)) / 2., MMHFloat(50));
            [self.orderButton addTarget:self action:@selector(orderDetail:) forControlEvents:UIControlEventTouchUpInside];
            [cellWithRowOne addSubview:self.orderButton];
            
            self.homeButton = [self createButtonWithIndex:1];
            self.homeButton.frame = CGRectMake(kScreenBounds.size.width - (kScreenBounds.size.width - 6 * MMHFloat(16)) / 2. - 2 * MMHFloat(16), self.orderButton.orgin_y, (kScreenBounds.size.width - 6 * MMHFloat(16)) / 2., MMHFloat(50));
            [self.homeButton addTarget:self action:@selector(homeButtonClick) forControlEvents:UIControlEventTouchUpInside];
            [cellWithRowOne addSubview:self.homeButton];
        }
        return cellWithRowOne;
    } else if (indexPath.section == 0 && indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            
            UILabel *invoiceLabel = [[UILabel alloc]init];
            invoiceLabel.backgroundColor = [UIColor clearColor];
            invoiceLabel.text = @"发票信息";
            invoiceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            invoiceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            invoiceLabel.stringTag = @"invoiceLabel";
            CGSize invoiceSize = [invoiceLabel.text sizeWithCalcFont:invoiceLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width, [MMHTool contentofHeight:invoiceLabel.font])];
            invoiceLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(16) - invoiceSize.width, (cellHeight - [MMHTool contentofHeight:invoiceLabel.font]) / 2., invoiceSize.width, [MMHTool contentofHeight:invoiceLabel.font]);
            [cellWithRowTwo addSubview:invoiceLabel];
            
            UIImageView *payInvoiceImageView = [[UIImageView alloc]init];
            payInvoiceImageView.backgroundColor = [UIColor clearColor];
            payInvoiceImageView.image = [UIImage imageNamed:@"icon_pay_invoice"];
            payInvoiceImageView.frame = CGRectMake( invoiceLabel.orgin_x - MMHFloat(8) - MMHFloat(12), (cellHeight - MMHFloat(12))/2. , MMHFloat(12), MMHFloat(12));
            [cellWithRowTwo addSubview:payInvoiceImageView];
        }
        return cellWithRowTwo;
    } else if (indexPath.section == 0 && indexPath.row == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
         
            //
            UILabel *invoiceLaebl = [[UILabel alloc]init];
            invoiceLaebl.backgroundColor = [UIColor clearColor];
            invoiceLaebl.text = @"发票类型:";
            invoiceLaebl.stringTag = @"invoiceLaebl";
            invoiceLaebl.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            CGSize invoiceSize = [invoiceLaebl.text sizeWithCalcFont:invoiceLaebl.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:invoiceLaebl.font])];
            invoiceLaebl.frame = CGRectMake(MMHFloat(16), MMHFloat(12), invoiceSize.width, [MMHTool contentofHeight:invoiceLaebl.font]);
            [cellWithRowThr addSubview:invoiceLaebl];
            
            // 1. 创建label
            UIView *view1 = [self createSingleButtonWithTag:@"个人"];
            view1.stringTag = @"view1";
            view1.orgin_x =  CGRectGetMaxX(invoiceLaebl.frame) + MMHFloat(10);
            view1.orgin_y = MMHFloat(12) - (MMHFloat(20) - [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])/2.;
            [cellWithRowThr addSubview:view1];
            
            // 2. 创建view2
            UIView *view2 = [self createSingleButtonWithTag:@"公司"];
            view2.stringTag = @"view2";
            view2.orgin_x = CGRectGetMaxX(view1.frame) + MMHFloat(24);
            view2.orgin_y =  MMHFloat(12) - (MMHFloat(20) - [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]])/2.;
            [cellWithRowThr addSubview:view2];
            
            // 3. 创建发票类型fixedLaebl
            UILabel *invoiceTypeLaebl = [[UILabel alloc]init];
            invoiceTypeLaebl.backgroundColor =[UIColor clearColor];
            invoiceTypeLaebl.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            invoiceTypeLaebl.text = @"发票抬头:";
            invoiceTypeLaebl.stringTag = @"invoiceTypeLaebl";
            invoiceTypeLaebl.frame = CGRectMake(invoiceLaebl.orgin_x, CGRectGetMaxY(invoiceLaebl.frame) + MMHFloat(20) + (MMHFloat(24) - [MMHTool contentofHeight:invoiceLaebl.font]) / 2., invoiceLaebl.size_width, invoiceSize.height);
            [cellWithRowThr addSubview:invoiceTypeLaebl];
            
            // 4. 创建输入框
            self.inputTextField = [[UITextField alloc]init];
            self.inputTextField.backgroundColor = [UIColor hexChangeFloat:@"f6f6f6"];
            self.inputTextField.placeholder = @"请输入发票抬头";
            self.inputTextField.frame = CGRectMake(CGRectGetMaxX(invoiceTypeLaebl.frame) + MMHFloat(10), CGRectGetMaxY(invoiceLaebl.frame) + MMHFloat(20), kScreenBounds.size.width - MMHFloat(16) - invoiceTypeLaebl.size_width - MMHFloat(10) - MMHFloat(8) - MMHFloat(75) - MMHFloat(16), MMHFloat(34));
            [self.inputTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    self.inputTextField.text = @"";
                }
                [self.inputTextField resignFirstResponder];
            }];
            self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            [cellWithRowThr addSubview:self.inputTextField];
        
            // 5. 创建确定按钮
            self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.confirmButton.backgroundColor = [UIColor whiteColor];
            [self.confirmButton setTitle:@"确定" forState:UIControlStateNormal];
            self.confirmButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            [self.confirmButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
            self.confirmButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
            self.confirmButton.layer.borderWidth = .5f;
            [self.confirmButton addTarget:self action:@selector(confirmButtonClick) forControlEvents:UIControlEventTouchUpInside];
            self.confirmButton.frame = CGRectMake(CGRectGetMaxX(self.inputTextField.frame) + MMHFloat(8), self.inputTextField.orgin_y, MMHFloat(75), MMHFloat(34));
            [cellWithRowThr addSubview:self.confirmButton];
            
            //6. 创建label
            self.confirmLabel = [[UILabel alloc]init];
            self.confirmLabel.text = @"点击确定后不能修改";
            self.confirmLabel.font = [UIFont systemFontOfCustomeSize:10];
            self.confirmLabel.frame = CGRectMake(self.inputTextField.orgin_x, CGRectGetMaxY(self.inputTextField.frame) + MMHFloat(8), self.inputTextField.size_width, [MMHTool contentofHeight:self.confirmLabel.font]);
            self.confirmLabel.backgroundColor = [UIColor clearColor];
            self.confirmLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            [cellWithRowThr addSubview:self.confirmLabel];
            
            // 创建成功后的label
            UILabel *dymicLabel1 = [[UILabel alloc]init];
            dymicLabel1.backgroundColor = [UIColor clearColor];
            dymicLabel1.stringTag = @"dymicLabel1";
            dymicLabel1.font = invoiceLaebl.font;
            [cellWithRowThr addSubview:dymicLabel1];
            
            // 创建成功后的label2
            UILabel *dymicLabel2 = [[UILabel alloc]init];
            dymicLabel2.stringTag = @"dymicLabel2";
            dymicLabel2.font = invoiceLaebl.font;
            dymicLabel2.numberOfLines = 0;
            dymicLabel2.backgroundColor = [UIColor clearColor];
            [cellWithRowThr addSubview:dymicLabel2];
        }
        UIView *view1 = (UIView *)[cellWithRowThr viewWithStringTag:@"view1"];
        UIView *view2 = (UIView *)[cellWithRowThr viewWithStringTag:@"view2"];
        UILabel *dymicLabel1 = (UILabel *)[cellWithRowThr viewWithStringTag:@"dymicLabel1"];
        UILabel *dymicLabel2 = (UILabel *)[cellWithRowThr viewWithStringTag:@"dymicLabel2"];
        UILabel *invoiceLaebl = (UILabel *)[cellWithRowThr viewWithStringTag:@"invoiceLaebl"];
        UILabel *invoiceTypeLaebl = (UILabel *)[cellWithRowThr viewWithStringTag:@"invoiceTypeLaebl"];
        if (self.isSuccess){
            dymicLabel1.hidden = NO;
            dymicLabel2.hidden = NO;
            view1.hidden = YES;
            view2.hidden = YES;
            self.confirmButton.hidden = YES;
            self.confirmLabel.hidden = YES;
            self.inputTextField.hidden = YES;
            dymicLabel1.text = self.invoiceType;
            dymicLabel1.frame = CGRectMake(CGRectGetMaxX(invoiceLaebl.frame) + MMHFloat(10), invoiceLaebl.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(invoiceLaebl.frame) - MMHFloat(11) - MMHFloat(10), [MMHTool contentofHeight:dymicLabel1.font]);
            
            dymicLabel2.text = self.invoiceString;
            CGSize dymicSize = [self.invoiceString sizeWithCalcFont:self.inputTextField.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(invoiceTypeLaebl.frame) - MMHFloat(11), CGFLOAT_MAX)];

            dymicLabel2.frame = CGRectMake(CGRectGetMaxX(invoiceTypeLaebl.frame) + MMHFloat(10), invoiceTypeLaebl.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(invoiceTypeLaebl.frame) - MMHFloat(11) - MMHFloat(10), dymicSize.height);

        } else {
            dymicLabel1.hidden = YES;
            dymicLabel2.hidden = YES;
            view2.hidden = NO;
            view1.hidden = NO;
        }
        
        return cellWithRowThr;
    } else if (indexPath.section == 1 && indexPath.row == 0){
        static NSString *cellIdentifyWithSecSix = @"cellIdentifyWithSecSix";
        MMHGuessYouLikeCell *cellWithSecSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecSix];
        if (!cellWithSecSix){
            cellWithSecSix = [[MMHGuessYouLikeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecSix];
            cellWithSecSix.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithSecSix.guessYouLikeListModel = self.guessYouLikeListModel;
        __weak typeof(self) weakSelf = self;
        cellWithSecSix.guessYouLikeBlock = ^(MMHGuessYouLikeSingleModel *guessYouLikeSingleModel){
            [weakSelf guessYouLikeInterfaceWith:guessYouLikeSingleModel];
        };

        return cellWithSecSix;
    }
    return nil;
}

-(void)guessYouLikeInterfaceWith:(MMHGuessYouLikeSingleModel *)singleModel{
    MMHProductDetailViewController1 * productDetailViewController = [[MMHProductDetailViewController1 alloc]initWithProduct:singleModel];
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        [cell addSeparatorLineWithTypeWithAres:SeparatorTypeMiddle andUseing:@"examineResult"];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return MMHFloat(10);
    } else if (section == 1){
        return MMHFloat(10);
    } else {
        return 0;
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 2){
        return MMHFloat(120);
    } else if (indexPath.section == 0 && indexPath.row == 0){
        CGFloat cellHeight = 0;
        cellHeight = MMHFloat(26) + [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:24]] + MMHFloat(16) + [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:12.]] + MMHFloat(25) + MMHFloat(50) + MMHFloat(28);
        return cellHeight;
    } else if (indexPath.section == 1){
        return [MMHGuessYouLikeCell contentOfHeight];
    } else {
        return MMHFloat(44);
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 1){
        if ([[self.paySuccessMutableArr objectAtIndex:0] count] <=2){
            [self.paySuccessMutableArr replaceObjectAtIndex:0 withObject:@[@"付款成功",@"发票",@"发票信息"]];
            [tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

#pragma mark CreateButton
-(UIButton *)createButtonWithIndex:(NSInteger)index{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    if (index == 0){                // 查看订单
        [button setTitle:@"查看订单" forState:UIControlStateNormal];
        button.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        button.backgroundColor = [UIColor whiteColor];
        [button setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        button.layer.borderWidth = .5f;
    } else {                        // 再逛逛
        [button setTitle:@"再逛逛" forState:UIControlStateNormal];
        button.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
        [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    }
    button.layer.cornerRadius = MMHFloat(6);
    return button;
}

#pragma mark createSingleButton
-(UIView *)createSingleButtonWithTag:(NSString *)stringTag{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    
    // 创建按钮
    UIImageView *checkImageView = [[UIImageView alloc]init];
    checkImageView.backgroundColor = [UIColor clearColor];
    checkImageView.image = [UIImage imageNamed:@"address_icon_untick"];
    checkImageView.frame = CGRectMake(0, 0, MMHFloat(20), MMHFloat(20));
    checkImageView.stringTag = @"checkImageView";
    [bgView addSubview:checkImageView];
    
    // 创建label
    UILabel *checkLabel = [[UILabel alloc]init];
    checkLabel.backgroundColor = [UIColor clearColor];
    checkLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    checkLabel.stringTag = @"checkLabel";
    checkLabel.text = stringTag;
    CGSize checkLabelSize = [stringTag sizeWithCalcFont:checkLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:checkLabel.font])];
    checkLabel.frame = CGRectMake(CGRectGetMaxX(checkImageView.frame) + MMHFloat(4), (MMHFloat(20) - [MMHTool contentofHeight:checkLabel.font]) / 2., checkLabelSize.width, [MMHTool contentofHeight:checkLabel.font]);

    [bgView addSubview:checkLabel];
    
    bgView.frame = CGRectMake(0, 0, CGRectGetMaxX(checkImageView.frame) + MMHFloat(4) + checkLabelSize.width, MMHFloat(20));
    
    UIButton *clickButton = [UIButton buttonWithType:UIButtonTypeCustom];
    clickButton.backgroundColor = [UIColor clearColor];
    clickButton.frame = bgView.frame;
    if ([stringTag isEqualToString:@"个人"]){
        clickButton.stringTag = @"clickButton1";
    } else {
        clickButton.stringTag = @"clickButton2";
    }
    [clickButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:clickButton];
    return bgView;
}




-(void)buttonClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    UITableViewCell *cell = [self.paySuccessTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
    UIView *view1 = (UIView *)[cell viewWithStringTag:@"view1"];
    UIView *view2 = (UIView *)[cell viewWithStringTag:@"view2"];
    UIImageView *checkImageView1 = (UIImageView *)[view1 viewWithStringTag:@"checkImageView"];
    UIImageView *checkImageView2 = (UIImageView *)[view2 viewWithStringTag:@"checkImageView"];
    if ([button.stringTag isEqualToString:@"clickButton1"]){     //个人
        [self animationWithCollectionWithButton:checkImageView1 collection:YES];
        checkImageView2.image = [UIImage imageNamed:@"address_icon_untick"];
        self.invoiceType = @"1";
    } else {
        [self animationWithCollectionWithButton:checkImageView2 collection:YES];
        checkImageView1.image = [UIImage imageNamed:@"address_icon_untick"];
        self.invoiceType = @"2";
    }
}

-(void)confirmButtonClick{
    if (!self.inputTextField.text.length){
        [self.view showTips:@"请输入发票抬头"];
    } else {
        __weak typeof(self)weakSelf = self;
        NSString *fapiaoString = [NSString stringWithFormat:@"\n发票抬头为:%@\n",self.inputTextField.text];
        [[UIAlertView alertViewWithTitle:@"点击确认后不可修改" message:fapiaoString buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1){          // 确定
                //执行接口
                [weakSelf sendRequestToGetInvoice];
            }
        }] show];
    }
}

#pragma mark check动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection{
    if (isCollection){
        collectionButton.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        collectionButton.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    isCollection = !isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}

#pragma mark - 通知
#pragma mark 手势通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.paySuccessTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {
    NSDictionary* userInfo = [notification userInfo];
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.paySuccessTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)successShow{
    self.isSuccess = YES;
    self.invoiceString = self.inputTextField.text;
    [self.paySuccessTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0] ] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark 接口
-(void)sendRequestToGetInvoice{
    NSMutableDictionary *parameteDic = [NSMutableDictionary dictionary];
    if (!self.transfetOrderNo.length){
        [self.view showTips:@"系统错误，请联系客服开发票"];
        return;
    }
    [parameteDic setObject:self.transfetOrderNo forKeyedSubscript:@"orderBatchNo"];
    if (self.invoiceType == 0){
        [self.view showTips:@"请选择发票类型"];
        return;
    }
    [parameteDic setObject:self.invoiceType forKeyedSubscript:@"invoiceType"];
    if (!self.inputTextField.text.length){
        [self.view showTips:@"请输入发票抬头"];
        return;
    }
    [parameteDic setObject:self.inputTextField.text forKeyedSubscript:@"invoceTitle"];
    
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestToGetInvoiceWithParameterDic:parameteDic from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTips:@"发票录入成功"];
        [strongSelf successShow];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        [[UIAlertView alertViewWithTitle:@"失败" message:error.localizedDescription buttonTitles:@[@"确定"] callback:nil]show];
    }];
}

#pragma mark 猜你喜欢
-(void)sendRequestWithGuessYouLike{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailGuessYouLikeWithProduct:nil from:self succeededHandler:^(MMHGuessYouLikeListModel *guessYouLikeListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.guessYouLikeListModel = guessYouLikeListModel;
        if (strongSelf.guessYouLikeListModel.data.count){
            [strongSelf.paySuccessMutableArr addObject:@[@"猜你喜欢"]];
            NSIndexSet *wordOfMouthSet = [NSIndexSet indexSetWithIndex:1];
            [strongSelf.paySuccessTableView insertSections:wordOfMouthSet withRowAnimation:UITableViewRowAnimationLeft];
            [strongSelf.paySuccessTableView reloadData];
        }
        
    } failedHandler:^(NSError *error) {
        
    }];
}



#pragma mark 跳转到订单首页
-(void)orderDetail:(UIButton *)sender{
    __weak typeof(self)weakSelf = self;
    if (self.paySuccessPageUsingType == MMHPaySuccessPageUsingTypeConfirmation){
        MMHOrderRootViewController *orderRootView = [[MMHOrderRootViewController alloc]init];
        [MMHTabBarController redirectToCenterWithController:orderRootView];
        [weakSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
    } else if (self.paySuccessPageUsingType == MMHPaySuccessPageUsingTypeOrderRoot){
        if (self.redirectionPageBlock){
            self.redirectionPageBlock();
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark 跳转到首页
-(void)homeButtonClick{
    if (self.paySuccessPageUsingType == MMHPaySuccessPageUsingTypeConfirmation){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        [MMHTabBarController redirectToHomeWithAnimation];
    } else  if (self.paySuccessPageUsingType == MMHPaySuccessPageUsingTypeOrderRoot){
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        [MMHTabBarController redirectToHome];
    }
}

@end
