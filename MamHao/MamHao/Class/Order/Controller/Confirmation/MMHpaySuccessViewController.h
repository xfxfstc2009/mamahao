//
//  MMHpaySuccessViewController.h
//  MamHao
//
//  Created by SmartMin on 15/7/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 支付成功页面
#import "AbstractViewController.h"
#import "MMHMamhaoViewController.h"


typedef NS_ENUM(NSInteger, MMHPaySuccessPageUsingType) {
    MMHPaySuccessPageUsingTypeConfirmation,                         /**< 确认订单页面使用*/
    MMHPaySuccessPageUsingTypeOrderRoot,                            /**< 订单首页使用*/
};

typedef void(^redirectionPageBlock)();
@class MMHMamhaoViewController;

@interface MMHpaySuccessViewController : AbstractViewController

@property (nonatomic,copy)NSString *transfetOrderNo;
@property (nonatomic,assign)MMHPaySuccessPageUsingType  paySuccessPageUsingType;
@property (nonatomic,copy)redirectionPageBlock redirectionPageBlock;                /**< 页面跳转回调*/
@end
