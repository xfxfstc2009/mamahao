//
//  MMHConfirmationOrderViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 确认订单
#import "AbstractViewController.h"
#import "MMHOrderWithConfirmationModel.h"

typedef NS_ENUM(NSInteger, MMHConfirmationRedirectionType) {
    MMHConfirmationRedirectionTypeCart = 1,                         /**< 购物车购买 */
    MMHConfirmationRedirectionTypeDirect = 2,                       /**< 直接购买 */
    MMHConfirmationRedirectionTypeUpdateAddress = 3,                /**< 切换地址*/
    MMHConfirmationRedirectionTypeMbean = 4,                        /**< 妈豆商品*/
    MMHConfirmationRedirectionTypeScanning = 5,                     /**< 扫码进入*/
};

typedef NS_ENUM(NSInteger, MMHProductSaleType) {
    MMHProductSaleTypeRMB,                                      /**< 人命币支付*/
    MMHProductSaleTypeMbean,                                    /**< 妈豆支付*/
    MMHProductSaleTypeIntegral,                                 /**< 积分支付*/
};
@class MMHCartData;


@interface MMHConfirmationOrderViewController : AbstractViewController

@property (nonatomic,assign)MMHConfirmationRedirectionType confirmationRedirectionType;
@property (nonatomic,assign)MMHProductSaleType productSaleType;     /**< 商品出售方式*/
@property (nonatomic,assign)NSInteger MBeanPayCount;                /**< 妈豆支付价格-妈豆商品传入*/
@property (nonatomic,strong)MMHOrderWithConfirmationModel *orderWithConfirmationModel;   /**< 订单model*/

- (instancetype)initWithCartData:(MMHCartData *)cartDaa;
- (id)initWithProductJSONTerm:(NSDictionary *)jsonTerm bindsShop:(BOOL)bindsShop inlet:(MMHConfirmationRedirectionType)inlet;
@end
