//
//  MMHOrderRootViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"

@interface MMHOrderRootViewController : AbstractViewController
@property (nonatomic,assign)NSInteger transferItemSelected;                 // 传递过来的item选中
@end
