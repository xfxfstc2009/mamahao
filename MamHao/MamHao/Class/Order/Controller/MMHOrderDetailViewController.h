//
//  MMHOrderDetailViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHOrderSingleModel.h"

#pragma mark - Enum
typedef NS_ENUM(NSInteger, MMHOrderType) {
    MMHOrderTypeFinish          = 0,             /**< 已完成*/
    MMHOrderTypeUnable          = 1,             /**< 已失效*/
    MMHOrderTypeCancel          = 2,             /**< 已取消*/
    MMHOrderTypePendingPay      = 3,             /**< 待付款*/
    MMHOrderTypeBeShipped       = 4,             /**< 待发货*/
    MMHOrderTypeBeReceipt       = 5,             /**< 待收货*/
    MMHOrderTypeBeEvaluation    = 6,             /**< 待评价*/
    MMHOrderTypeComplete        = 7,             /**< 初始状态*/
    MMHOrderTypeDelete          = 8,             /**< 删除状态*/
    MMHOrderTypeAll             = 9,             /**< 全部*/
    MMHOrderTypeBeRefund        = -1,            /**< 待退货*/
};

#pragma mark - Block
typedef void(^cancelOrderBlock)(MMHOrderSingleModel *orderSingleModel); /**< 取消订单*/
typedef void(^deleteOrderBlock)(MMHOrderSingleModel *orderSingleModel); /**< 删除订单*/
typedef void(^confirmReceiptBlock)(MMHOrderSingleModel *orderSingleModel); /**< 确认收货*/
typedef void(^payPassBlock)(MMHOrderSingleModel *orderSingleModel);          /**< 支付成功*/
typedef void(^praiseAllDone)(MMHOrderSingleModel *orderSingleModel);         /**<评价成功*/

@interface MMHOrderDetailViewController : AbstractViewController

@property (nonatomic,assign)MMHOrderType orderType;
@property (nonatomic,strong)MMHOrderSingleModel *transferOrderSingleModel;

@property (nonatomic,copy)cancelOrderBlock cancelOrder;                         // 取消订单
@property (nonatomic,copy)deleteOrderBlock deleteOrderBlock;                    // 删除订单
@property (nonatomic,copy)confirmReceiptBlock confirmReceiptBlock;              // 确认收货
@property (nonatomic,copy)payPassBlock payPassBlock;                            // 支付成功
@property (nonatomic,copy)praiseAllDone praiseAllDoneBlock;                     // 评价完成
@end
