//
//  MMHExpressListViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHExpressModel.h"
@interface MMHExpressListViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferOrderNO;                /**< 传递过来的订单编号*/
@property (nonatomic,assign)NSInteger numberOfExpress;              /**< 多少个物流*/
@property (nonatomic,strong) NSArray <MMHExpressModel>*logistics;
@end
