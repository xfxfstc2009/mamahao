//
//  MMHExpressListViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressListViewController.h"
#import "MMHOrderProductTableViewCell.h"                            // cell
#import "MMHShopCartModel.h"                                        // model
#import "MMHExpressContentModel.h"                                  // expressModel
#import "MMHNetworkAdapter+Express.h"                               // 物流接口
#import "MMHExpressInfoCell.h"                                      // cell
#import "MMHExpressDetailViewController.h"                         // 物流详情
#import "MMHExpressListCell.h"

@interface MMHExpressListViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *expressListTableView;                  /**< tableView*/
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;           /**< dataSource*/

@end

@implementation MMHExpressListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self.dataSourceMutableArr addObjectsFromArray:self.logistics];
    [self.expressListTableView reloadData];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"查看物流";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
    
}


#pragma mark - UITableView
-(void)createTableView{
    if (!self.expressListTableView){
        self.expressListTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.expressListTableView setHeight:self.view.bounds.size.height];
        self.expressListTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.expressListTableView.delegate = self;
        self.expressListTableView.dataSource = self;
        self.expressListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.expressListTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.expressListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithExpressRowOne = @"cellIdentifyWithExpressRowOne";
        UITableViewCell *cellWithExpressRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithExpressRowOne];
        if (!cellWithExpressRowOne){
            cellWithExpressRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithExpressRowOne];
            cellWithExpressRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 1.fixedLabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.font = F4;
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithExpressRowOne addSubview:fixedLabel];
        }
        // fixedLabel
        UILabel *fixedLabel = (UILabel *)[cellWithExpressRowOne viewWithStringTag:@"fixedLabel"];
        MMHExpressModel *expressModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        fixedLabel.text = [NSString stringWithFormat:@"物流公司:%@ 物流单号:%@",expressModel.platformName,expressModel.waybillNumber];
        fixedLabel.frame = CGRectMake(15, 0, kScreenBounds.size.width, MMHFloat(44));
        return cellWithExpressRowOne;
    } else if (indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        MMHExpressInfoCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[MMHExpressInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferGoodsList = [[self.dataSourceMutableArr objectAtIndex:indexPath.section]goodsList];
        
        return cellWithRowTwo;
    } else if (indexPath.row == 2){
        static NSString *cellidentifyWithRowThr = @"cellIdentifyWithRowThr";
        MMHExpressListCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellidentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[MMHExpressListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellidentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        MMHExpressModel *expressModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        cellWithRowThr.data = expressModel.data;
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return MMHFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return MMHFloat(44);
    } else if (indexPath.row == 1){
        return MMHFloat(100);
    } else if (indexPath.row == 2){
        MMHExpressModel *expressModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        return  [MMHExpressListCell cellHeight:expressModel.data];
    }
    return MMHFloat(44);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MMHExpressDetailViewController *expressDetailViewController1 = [[MMHExpressDetailViewController alloc]init];
    MMHExpressModel *expressModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
    expressDetailViewController1.transferExpressModel = expressModel;
    [self.navigationController pushViewController:expressDetailViewController1 animated:YES];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.expressListTableView) {
        if ( [indexPath row] == 0) {
            [cell addSeparatorLineWithType:SeparatorTypeHead];
        } else if ([indexPath row] == 2) {
            [cell addSeparatorLineWithType:SeparatorTypeBottom];
        } else {
            [cell addSeparatorLineWithType:SeparatorTypeMiddle];
        }
    }
}

@end
