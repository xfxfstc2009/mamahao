//
//  MMHExpressDetailViewController1.h
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHExpressModel.h"                     // 物流信息
@interface MMHExpressDetailViewController : AbstractViewController
@property (nonatomic,strong)MMHExpressModel *transferExpressModel;          /**< 传递过来的物流model*/
@end
