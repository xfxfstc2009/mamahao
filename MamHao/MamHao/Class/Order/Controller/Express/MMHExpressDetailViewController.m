//
//  MMHExpressDetailViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHExpressDetailViewController.h"
#import "MMHExpressSingleProductCell.h"
#import "MMHExpressDataCell.h"
@interface MMHExpressDetailViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;
@property (nonatomic,strong)UITableView *expressListTableView;
@property (nonatomic,assign)BOOL isShowAll;                             /**< 是否显示全部*/
@end

@implementation MMHExpressDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.isShowAll = NO;
    self.barMainTitle = @"物流详情";
}

-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
    NSMutableArray *productListTempMutableArr = [NSMutableArray array];
    [productListTempMutableArr addObject:[self.transferExpressModel.goodsList objectAtIndex:0]];
    [productListTempMutableArr addObject:@"显示其余"];
    [self.dataSourceMutableArr addObject:productListTempMutableArr];
    [self.dataSourceMutableArr addObject:self.transferExpressModel.data];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.expressListTableView){
        self.expressListTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.expressListTableView setHeight:self.view.bounds.size.height];
        self.expressListTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.expressListTableView.delegate = self;
        self.expressListTableView.dataSource = self;
        self.expressListTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.expressListTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.expressListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){                    // 【显示商品信息】
        if ((indexPath.row == ([[self.dataSourceMutableArr objectAtIndex:indexPath.section] count] - 1)) && [[self.dataSourceMutableArr objectAtIndex:indexPath.section] count] > 1){            // 显示剩余2条
            static NSString *cellIdentifyWithShowOther = @"cellIdentifyWithShowOther";
            UITableViewCell *cellWithShowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithShowOther];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithShowOther){
                cellWithShowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithShowOther];
                // 创建fixedLabel
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor =[ UIColor clearColor];
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.stringTag = @"fixedLabel";
                fixedLabel.textAlignment = NSTextAlignmentCenter;
                fixedLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, cellHeight);
                [cellWithShowOther addSubview:fixedLabel];
            }
            UILabel *fixedLabel = (UILabel *)[cellWithShowOther viewWithStringTag:@"fixedLabel"];
            if (self.isShowAll){
                fixedLabel.text = @"隐藏商品";
            } else{
                fixedLabel.text = [NSString stringWithFormat:@"显示其余%lu件",(self.transferExpressModel.goodsList.count - 1)];
            }
            return cellWithShowOther;
        } else {                                    // 显示商品
            static NSString *cellIdentifyWithProduct = @"cellIdentifyWithProduct";
            MMHExpressSingleProductCell *cellWithProduct = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithProduct];
            if (!cellWithProduct){
                cellWithProduct = [[MMHExpressSingleProductCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithProduct];
                cellWithProduct.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            MMHOrderWithGoodsListModel *orderWIthGoodsListModel = [self.transferExpressModel.goodsList objectAtIndex:indexPath.row];
            cellWithProduct.orderWithGoodsListModel = orderWIthGoodsListModel;
            
            return cellWithProduct;
        }
    } else {                                        // 【显示物流】
        if (indexPath.row == 0 || indexPath.row == 1){
            static NSString *cellIdentifyWithExpressRowOne = @"cellIdentifyWithExpressRowOne";
            UITableViewCell *cellWithExpressRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithExpressRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithExpressRowOne){
                cellWithExpressRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithExpressRowOne];
                cellWithExpressRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.font = F5;
                fixedLabel.textColor = C6;
                fixedLabel.textAlignment = NSTextAlignmentLeft;
                fixedLabel.stringTag = @"fixedLabel";
                [cellWithExpressRowOne addSubview:fixedLabel];
            }
            UILabel *fixedLabel = (UILabel *)[cellWithExpressRowOne viewWithStringTag:@"fixedLabel"];
            if (indexPath.row == 0){
                fixedLabel.text = [NSString stringWithFormat:@"物流公司:%@  物流单号:%@",self.transferExpressModel.platformName,self.transferExpressModel.waybillNumber];
                fixedLabel.frame = CGRectMake(MMHFloat(11), 0, kScreenBounds.size.width - MMHFloat(11), cellHeight);
            } else {
                fixedLabel.text = @"物流详情";
                fixedLabel.frame = CGRectMake(MMHFloat(11), cellHeight - [MMHTool contentofHeight:F5], kScreenBounds.size.width, [MMHTool contentofHeight:F5]);
            }
            
            return cellWithExpressRowOne;
        } else {
            static NSString *cellIdetntify = @"cellIdentify";
            MMHExpressDataCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdetntify];
            if (!cell){
                cell = [[MMHExpressDataCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdetntify];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.backgroundColor = [UIColor whiteColor];
                
            }
            MMHExpressContentModel *expressContentModel = [[self.dataSourceMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row - 2];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (indexPath.row == 2){
                cell.isHltCell = YES;
            } else {
                cell.isHltCell = NO;
            }
            cell.cellHeight = cellHeight;
            cell.expressContentModel = expressContentModel;
            
            return cell;
        }
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == [[self.dataSourceMutableArr objectAtIndex:indexPath.section]count] - 1){
            return MMHFloat(44);
        } else {
            return MMHFloat(90);
        }
    } else {
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else if (indexPath.row == 1){
            return MMHFloat(20) + [MMHTool contentofHeight:F5];
        } else {
            MMHExpressContentModel *expressContentModel = [[self.dataSourceMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return [MMHExpressDataCell cellHeight:expressContentModel];
        }
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1){
        return MMHFloat(20);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return MMHFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([[self.dataSourceMutableArr objectAtIndex:0] count] > 1){
        if (indexPath.section == 0 && indexPath.row == ([[self.dataSourceMutableArr objectAtIndex:0] count] - 1)){
            if(self.isShowAll){
                // 1. 删除掉第一组内容
                [self.dataSourceMutableArr removeObjectAtIndex:0];
                // 2. 加入新内容
                NSMutableArray  *tempArr = [NSMutableArray array];
                [tempArr addObject:[self.transferExpressModel.goodsList objectAtIndex:0]];
                [tempArr addObject:@"显示内容"];
                
                [self.dataSourceMutableArr insertObject:tempArr atIndex:0];
                // 3. 刷新第一组
                self.isShowAll = !self.isShowAll;
                NSIndexSet *indexSetWithSectionZero = [NSIndexSet indexSetWithIndex:0];
                [self.expressListTableView reloadSections:indexSetWithSectionZero withRowAnimation:UITableViewRowAnimationFade];
            } else {                                                        //
                // 1. 替换第一组内容
                NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.transferExpressModel.goodsList];
                [tempArr addObject:@"隐藏内容"];
                [self.dataSourceMutableArr replaceObjectAtIndex:0 withObject:tempArr];
                
                // 2. 刷新第一组
                self.isShowAll = !self.isShowAll;
                NSIndexSet *indexSetWithSectionZero = [NSIndexSet indexSetWithIndex:0];
                [self.expressListTableView reloadSections:indexSetWithSectionZero withRowAnimation:UITableViewRowAnimationFade];
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.expressListTableView) {
        if (indexPath.section == 0){
            if (indexPath.row == 0){
                [cell addSeparatorLineWithTypeWithAres:SeparatorTypeHead andUseing:@"expressDetail"];
            } else {
                [cell addSeparatorLineWithTypeWithAres:SeparatorTypeBottom andUseing:@"expressDetail"];
            }
        } else {
            if (indexPath.row == 0){
                [cell addSeparatorLineWithType:SeparatorTypeMiddle];
            } else if (indexPath.row == 1){
                
            } else {
                [cell addSeparatorLineWithTypeWithAres:SeparatorTypeBottom andUseing:@"expressData"];
            }
        }
    }
}

@end
