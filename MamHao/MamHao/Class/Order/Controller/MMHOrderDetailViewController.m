//
//  MMHOrderDetailViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHOrderDetailViewController.h"
#import "MMHOrderAddressTableViewCell.h"                    // 收货地址
#import "MMHOrderBillsTableViewCell.h"                      // 账单流水
#import "MMHOrderInfoTableViewCell.h"                       // 订单信息
#import "MMHOrderShopInfoTableViewCell.h"                   // 商店信息
#import "MMHExpressTableViewCell.h"                         // 快递信息
#import "MMHDeliveryCell.h"                                 // 送货人信息
#import "MMHExpressDetailViewController.h"                  // 物流详情
#import "MMHNetworkAdapter+Order.h"                         // 接口
#import "MMHOrderProductTableViewCell.h"                    // 商品信息
#import "MMHRefundInfoViewController.h"                     // 待退款
#import "MMHRefundHomeViewController.h"                     // 退款首页
#import "MMHOrderBuyAgainViewController.h"                  // 再次购买
#import "MMHPraiseShowOrderViewController.h"                // 去评价
#import "MMHConfirmationOrderViewController.h"              // 去购买
#import "MMHExpressListViewController.h"                    // 跳转物流
#import "MMHChattingViewController.h"                       // 联系客服
#import "MMHOrderListShopInfoCell.h"
#import "MMHCartViewController.h"                           // 购物车

// model
#import "MMHExpressModel.h"                                 // 物流model
#import "MMHOrderDetailModel.h"                             // 订单详情model
#import "MMHPayOrderModel.h"                                // 支付
#import "MMHAliPayHandle.h"                                 // 支付宝
#import "MMHPayView.h"                                      // 妈豆支付
#import "MMHSettingPaySettingModel.h"                       // 支付
#import "MMHNetworkAdapter+Center.h"                        // 个人中心
#import "MMHPaySafetyPassCodeViewController.h"              // 支付安全
#import "MMHNetworkAdapter+Express.h"                       // 物流接口
#import "AbstractViewController+Chatting.h"                 // 联系客服
#import "MMHNetworkAdapter+Cart.h"                          // 加入购物车接口


@interface MMHOrderDetailViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong)UITableView *orderDetailTableView;         /**< tableView*/
@property (nonatomic, strong)NSMutableArray *dataSourceMutableArray;    /**< 数据源数组*/
@property (nonatomic,strong)UIView *floatView;                          /**< 浮层*/
@property (nonatomic,strong)UILabel *failureTimeLabel;                       /**< 失效时间*/
@property (nonatomic,assign)NSInteger failureTime;

@property (nonatomic,strong)NSMutableArray *priceModel;
@property (nonatomic,strong)MMHsinceTheMentionAddressModel *sinceTheMentionAddressModel;
@property (nonatomic,strong)MMHOrderDetailModel *orderDetailModel;         /**< 订单详情model */
@property (nonatomic,strong)NSTimer *loopTimer;                             /**< 计时器*/
@property (nonatomic,strong)UIButton *floatTempButton;                      /**< 浮层临时使用按钮*/
@property (nonatomic,assign)BOOL isPraiseAllDone;                           /**<评价是否完成*/
@end

@implementation MMHOrderDetailViewController

-(void)dealloc{
    NSLog(@"释放了");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.loopTimer) {
        [self.loopTimer invalidate];
        self.loopTimer = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.orderDetailTableView){
        [self.orderDetailTableView reloadData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];                       //  数组初始化
    [self createTableView];                     //  创建tableView

    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetOrderDetail];
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.isPraiseAllDone) {
            //回调删除数据
            strongSelf.praiseAllDoneBlock(strongSelf.transferOrderSingleModel);
        
        }
        [strongSelf popWithAnimation];
    }];
}

-(void)arrayWithInit{
    self.dataSourceMutableArray = [NSMutableArray array];
}

-(void)arrayWithSetting{
    NSArray *tempArray;
    if (self.orderDetailModel.orderStatus == MMHOrderTypeBeShipped){           // 待发货
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypePendingPay){   // 待付款
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeReceipt){    // 待收货
        if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeSelf){
            tempArray = @[@[@"物流信息"],@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
        } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeExpress){
            tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
        } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeDelivery){
            tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
        }
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation){ // 待评价
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeRefund){     // 待退货
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeFinish){       // 已完成
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeCancel){       // 已取消
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    } else if (self.orderDetailModel.orderStatus == MMHOrderTypeUnable){       // 已失效
        tempArray = @[@[@"收货信息"],@[@"商品金额",@"实付款",@"订单编号"]];
    }
    [self.dataSourceMutableArray addObjectsFromArray:tempArray];
}

-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    if (self.orderType == MMHOrderTypeBeRefund){
        self.barMainTitle = @"退款退货";
    } else {
        if (self.orderDetailModel.orderStatus == MMHOrderTypeFinish){                              // 已完成
            self.barMainTitle = @"已完成";
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeUnable){                       // 已失效
            self.barMainTitle = @"已失效";
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeCancel){                       // 已取消
            self.barMainTitle = @"已取消";
            [weakSelf rightBarButtonWithTitle:@"删除订单" barNorImage:nil barHltImage:nil action:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(self)strongSelf = weakSelf;
                [[UIAlertView alertViewWithTitle:@"您确定删除订单" message:nil buttonTitles:@[@"取消",@"删除"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 1){
                        [strongSelf sendRequestToDeleteOrderWithOrderNo:strongSelf.transferOrderSingleModel.orderNo];
                    }
                }]show];
            }];
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypePendingPay){                   // 待付款
            self.barMainTitle = @"待付款";
            [weakSelf rightBarButtonWithTitle:@"取消订单" barNorImage:nil barHltImage:nil action:^{
                if (!weakSelf){
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sendRequestWithCancelOrderWithOrderNo:strongSelf.transferOrderSingleModel.orderNo];
            }];
            if (self.transferOrderSingleModel.remainMinutes >= 0){
                self.loopTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerFireMethod:) userInfo:nil repeats:YES];
            } else {
                self.failureTimeLabel.text = @"订单已失效,请重新生成。";
                UIButton *rightButton = (UIButton *)[self.floatView viewWithStringTag: [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus]];
                [rightButton setTitle:@"订单失效" forState:UIControlStateNormal];
                [rightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
                rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
                rightButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
                rightButton.enabled = NO;
            }
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeShipped){                    // 待发货
            self.barMainTitle = @"待发货";
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeReceipt){                    // 待收货
            self.barMainTitle = @"待收货";
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation){                 // 待评价
            self.barMainTitle = @"待评价";
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeRefund){                     // 待退货
            self.barMainTitle = @"待退货";
        }
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.orderDetailTableView){
        self.orderDetailTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        [self.orderDetailTableView setHeight:self.view.bounds.size.height];
        self.orderDetailTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.orderDetailTableView.delegate = self;
        self.orderDetailTableView.dataSource = self;
        self.orderDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.orderDetailTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.orderDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceMutableArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"收货信息"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        MMHOrderAddressTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[MMHOrderAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.addressSingleModel = self.orderDetailModel.deliveryInfo;
        return cellWithRowOne;
        
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"商品金额"]) && (indexPath.row == [self cellIndexPathRowWithcellData:@"商品金额"])){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        MMHOrderBillsTableViewCell *cellWothRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWothRowThr){
            cellWothRowThr = [[MMHOrderBillsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWothRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWothRowThr.orderPriceArray = self.orderDetailModel.priceInfo;
        
        return cellWothRowThr;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"实付款"]) && (indexPath.row == [self cellIndexPathRowWithcellData:@"实付款"])){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWothRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        CGSize cellSize = [tableView rectForRowAtIndexPath:indexPath].size;
        if (!cellWothRowFour){
            cellWothRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWothRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cellWothRowFour.textLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            cellWothRowFour.textLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            
            // 创建右侧labek
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.backgroundColor = [UIColor clearColor];
            dymicLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            dymicLabel.stringTag = @"dymicLabel";
            dymicLabel.textAlignment = NSTextAlignmentRight;
            [cellWothRowFour addSubview:dymicLabel];
        }
        cellWothRowFour.textLabel.text = @"实付款";
        
        UILabel *dymicLabel = (UILabel *)[cellWothRowFour viewWithStringTag:@"dymicLabel"];
        dymicLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        CGFloat price = self.orderDetailModel.payPrice;
        CGSize contentOfDymicSize = [[NSString stringWithFormat:@"￥ %.2f",price] sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellSize.height)];
        
        dymicLabel.frame = CGRectMake(tableView.bounds.size.width - contentOfDymicSize.width - MMHFloat(11), 0, contentOfDymicSize.width, cellSize.height);
        dymicLabel.text = [NSString stringWithFormat:@"￥ %.2f",price];
        
        return cellWothRowFour;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"订单编号"]) && (indexPath.row == [self cellIndexPathRowWithcellData:@"订单编号"])){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        MMHOrderInfoTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[MMHOrderInfoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowFiv.orderNumber = self.orderDetailModel.orderNo;
        cellWithRowFiv.orderingTime = self.orderDetailModel.createTime;
        cellWithRowFiv.orderState = self.barMainTitle;
        
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"物流信息"]){
        if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeExpress){      //【快递-显示物流信息】
            static NSString *cellIdentifyWithExpress = @"cellIdentifyWithExpress";
            MMHExpressTableViewCell *expressCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithExpress];
            if (!expressCell){
                expressCell = [[MMHExpressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithExpress];
                expressCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            expressCell.transferExpressModel = self.orderDetailModel.logisticsInfo;
            return expressCell;
        } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeDelivery){              // 【货到付款 - 显示送货人信息】
            if (self.transferOrderSingleModel.orderStatus == MMHOrderTypeBeEvaluation){         // 【待评价 - 商店信息】
                static NSString *cellIdentifyWithShopExpress = @"cellIdentifyWithShopExpress1";
                MMHOrderListShopInfoCell *shopExpressCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithShopExpress];
                if (!shopExpressCell){
                    shopExpressCell = [[MMHOrderListShopInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithShopExpress];
                    shopExpressCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    shopExpressCell.phoneNumberButton.stringTag = @"shopExpressPhoneButton";
                    
                }
                [shopExpressCell.phoneNumberButton addTarget:self action:@selector(callButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                
                shopExpressCell.viewWidth = kScreenBounds.size.width;
                shopExpressCell.transferSinceTheMentionAddressModel = self.orderDetailModel.shopInfo;
                return shopExpressCell;
            } else {
                static NSString *cellIdengtifyWithDelivery = @"cellIdentifyWithDelivery";
                MMHDeliveryCell *cellWithDelivery = [tableView dequeueReusableCellWithIdentifier:cellIdengtifyWithDelivery];
                if (!cellWithDelivery){
                    cellWithDelivery = [[MMHDeliveryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengtifyWithDelivery];
                    cellWithDelivery.selectionStyle = UITableViewCellSelectionStyleNone;
                    [cellWithDelivery.shopDeliveryPhoneButton addTarget:self action:@selector(callButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                    cellWithDelivery.shopDeliveryPhoneButton.stringTag = @"shopDeliveryPhoneButton";
                }
                cellWithDelivery.distributionInfo = self.orderDetailModel.distributionInfo;
                
                return cellWithDelivery;
            }
        } else {                                                                     // 【自提-显示商店信息】
            static NSString *cellIdentifyWithShopExpress = @"cellIdentifyWithShopExpress";
            MMHOrderListShopInfoCell *shopExpressCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithShopExpress];
            if (!shopExpressCell){
                shopExpressCell = [[MMHOrderListShopInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithShopExpress];
                shopExpressCell.selectionStyle = UITableViewCellSelectionStyleNone;
                shopExpressCell.phoneNumberButton.stringTag = @"shopExpressPhoneButton";
                
            }
            [shopExpressCell.phoneNumberButton addTarget:self action:@selector(callButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            
            shopExpressCell.viewWidth = kScreenBounds.size.width;
            shopExpressCell.transferSinceTheMentionAddressModel = self.orderDetailModel.shopInfo;
            return shopExpressCell;
        }
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdenfityWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdenfityWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdenfityWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                
                // 1. 创建shopNameLaebl
                UILabel *shopNameLabel = [[UILabel alloc]init];
                shopNameLabel.backgroundColor = [UIColor clearColor];
                shopNameLabel.stringTag = @"shopNameLabel";
                shopNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                shopNameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
                [cellWithRowTwo addSubview:shopNameLabel];
                
                // 创建button
                UIButton *customerServerButton = [UIButton buttonWithType:UIButtonTypeCustom];
                customerServerButton.backgroundColor = [UIColor clearColor];
                [customerServerButton addTarget:self action:@selector(customerServerClick:) forControlEvents:UIControlEventTouchUpInside];
                [customerServerButton setTitle:@"联系客服" forState:UIControlStateNormal];
                customerServerButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [customerServerButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
                [customerServerButton setImage:[UIImage imageNamed:@"order_free_chat_typing"] forState:UIControlStateNormal];
                customerServerButton.adjustsImageWhenHighlighted =NO;
                customerServerButton.stringTag = @"customerServerButton";
                customerServerButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
                [customerServerButton setImageEdgeInsets:UIEdgeInsetsMake(0, - 2, 0, 0)];
                [customerServerButton setTitleEdgeInsets:UIEdgeInsetsMake(0, - MMHFloat(16) - MMHFloat(5), 0, - MMHFloat(77 - 37 -5))];
                [cellWithRowTwo addSubview:customerServerButton];
            }
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            // 1. fixedLabel
            UILabel *shopNameLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"shopNameLabel"];
            UIButton *customerServerButton = (UIButton *)[cellWithRowTwo viewWithStringTag:@"customerServerButton"];
            
            if (self.orderDetailModel.data.count == 1){
                NSString *shopName = [[[self.orderDetailModel.data lastObject] shopName] length]?[[self.orderDetailModel.data lastObject] shopName]:@"妈妈好平台";
               shopNameLabel.text = shopName;
            } else {
                NSInteger sectionOfReceipt = [self cellIndexPathSectionWithcellData:@"收货信息"];
                MMHShopCartModel *shopCartModel = [self.orderDetailModel.data objectAtIndex:(indexPath.section - 1 - sectionOfReceipt)];
                shopNameLabel.text = shopCartModel.shopId.length?shopCartModel.shopName:@"未获取商店";
            }
            customerServerButton.frame = CGRectMake(tableView.bounds.size.width - MMHFloat(77) - MMHFloat(11), 0, MMHFloat(77), cellHeight);
            shopNameLabel.frame = CGRectMake(MMHFloat(11), 0, kScreenBounds.size.width - 2 * MMHFloat(11) - MMHFloat(77), cellHeight);
            
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithGoods = @"cellIdentifyWithGoods";
            MMHOrderProductTableViewCell *orderProductTableViewCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithGoods];
            if (!orderProductTableViewCell){
                orderProductTableViewCell = [[MMHOrderProductTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithGoods];
                orderProductTableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            NSInteger sectionOfReceipt = [self cellIndexPathSectionWithcellData:@"收货信息"];
            MMHShopCartModel *shopCartModel = [self.orderDetailModel.data objectAtIndex:(indexPath.section - 1 - sectionOfReceipt)];
            MMHSingleProductWithShopCartModel *singleProductModel = [shopCartModel.goodsList objectAtIndex: indexPath.row - 1];
            orderProductTableViewCell.receivingStateType = self.transferOrderSingleModel.deliveryId;
            orderProductTableViewCell.orderType = self.orderDetailModel.orderStatus;
            orderProductTableViewCell.singleProduct = singleProductModel;
            
            [orderProductTableViewCell.orderTypeButton addTarget:self action:@selector(typeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            
            [orderProductTableViewCell.orderSubTypeButton addTarget:self action:@selector(typeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            
            orderProductTableViewCell.orderTypeButton.stringTag = [NSString stringWithFormat:@"orderTypeButton-%li-%li",(long)indexPath.section,(long)indexPath.row];
            orderProductTableViewCell.orderSubTypeButton.stringTag = [NSString stringWithFormat:@"orderSubTypeButton-%li-%li",(long)indexPath.section,(long)indexPath.row];
            
            return orderProductTableViewCell;
        }
    }
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if([self cellIndexIsHasData:@"物流信息"]){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"物流信息"]){
            if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeExpress){      // 快递
                return [MMHExpressTableViewCell cellHeightWithExpressModel:self.orderDetailModel.logisticsInfo];
            } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeDelivery){      // 店员送货
                return [MMHDeliveryCell cellHeightWithDistributionInfo:self.orderDetailModel.distributionInfo];
            } else {
                return [MMHOrderShopInfoTableViewCell cellHeightWith:self.orderDetailModel.shopInfo andUsingClass:MMHShopInfoCellUsingClassNormol];
            }
        }
    }
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"收货信息"]){
        return [MMHOrderAddressTableViewCell cellHeightWith:self.orderDetailModel.deliveryInfo];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品金额"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"商品金额"]){
            return  [MMHOrderBillsTableViewCell cellHeightWithOrderPriceArray:self.orderDetailModel.priceInfo];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"实付款"]){
            return MMHFloat(50);
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"订单编号"]){
            return MMHFloat(150);
        }
    } else {
        if (indexPath.row == 0){
            return MMHFloat(40);
        } else {
            return MMHFloat(100);
        }
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.orderDetailModel.data.count == 1){
        if (self.orderDetailTableView) {
            SeparatorType separatorType;
            [cell addSeparatorLineWithType:separatorType];
        }
        if ([[self.dataSourceMutableArray objectAtIndex:indexPath.section] count] == 1) {
            SeparatorType separatorType = SeparatorTypeSingle;
            [cell addSeparatorLineWithType:separatorType];
        }
    } else {
        if (self.orderDetailTableView) {
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( indexPath.row == 0) {
                separatorType = SeparatorTypeHead;
            } else if (indexPath.row == 1){
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([[self.dataSourceMutableArray objectAtIndex:indexPath.section] count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.orderDetailModel.data.count == 1){
        NSInteger sectionOfProductPrice = [self cellIndexPathSectionWithcellData:@"商品金额"];
        if (section == sectionOfProductPrice){
            return 0;
        } else {
            return MMHFloat(10);
        }
    } else {
        return MMHFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((self.orderType == MMHOrderTypeBeReceipt) || (self.orderType == MMHOrderTypeBeEvaluation) || (self.orderType == MMHOrderTypeBeRefund)){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"物流信息"]){
            if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeExpress){      // 快递
                MMHExpressDetailViewController *expressDetailViewController = [[MMHExpressDetailViewController alloc]init];
                [self.navigationController pushViewController:expressDetailViewController animated:YES];
            }
        }
    }
    
    NSInteger beginSection = [self cellIndexPathSectionWithcellData:@"收货信息"];
    NSInteger endSection = [self cellIndexPathSectionWithcellData:@"商品金额"];
    if ((indexPath.section > beginSection) && (indexPath.section < endSection)){
        if (indexPath.row != 0) {
//            MMHProductDetailViewController *productDetailViewController =[[MMHProductDetailViewController alloc]init];
//            [self.navigationController pushViewController:productDetailViewController animated:YES];
        }
    }
}



#pragma mark - ActionClick
-(void)callButtonClick:(UIButton *)button{
    UIButton *callButton = (UIButton *)button;
    __weak MMHOrderDetailViewController *weakViewController = self;
    if ([callButton.stringTag isEqualToString:@"shopDeliveryPhoneButton"]){         // 送货人电话
        [MMHTool callCustomerServer:weakViewController.view phoneNumber:self.orderDetailModel.distributionInfo.phone];
    } else if ([callButton.stringTag isEqualToString:@"shopExpressPhoneButton"]){
        [MMHTool callCustomerServer:weakViewController.view phoneNumber:self.orderDetailModel.shopInfo.telephone];
    }
}

-(void)customerServerClick:(UIButton *)sender{              /**< 联系客服*/
    [self startChattingWithContext:nil];
}

-(void)floatButtonClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    button.enabled = NO;
    self.floatTempButton = button;
    __weak typeof(self)weakSelf = self;
    if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeBeShipped]]){
        // 【待发货】
        [weakSelf sendRequestToRemindDeliveryWithOrderNo:weakSelf.transferOrderSingleModel.orderNo];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeBeReceipt]]){
        // 【待收货 - 跳物流】
        [weakSelf pushToExpress];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeBeEvaluation]]){
        [weakSelf pushToExpress];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeUnable]]){
        [[UIAlertView alertViewWithTitle:@"您确定删除订单" message:nil buttonTitles:@[@"取消",@"删除"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1){
                [weakSelf sendRequestToDeleteOrderWithOrderNo:weakSelf.transferOrderSingleModel.orderNo];
            }
        }]show];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeBeRefund]]){    // 【退款退货】
        [weakSelf pushToExpress];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeFinish]]){
        [weakSelf pushToExpress];
    }
    
    
    
    if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"rightButton%li",(long)MMHOrderTypePendingPay]]){
        // 【待付款】
        [weakSelf payMoment];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"rightButton%li",(long)MMHOrderTypeBeReceipt]]){  // 【待收货】
        [weakSelf sendRequestWithConfirmReceiptWithOrderNo:weakSelf.transferOrderSingleModel.orderNo];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"rightButton%li",(long)MMHOrderTypeBeEvaluation]]){  //【待评价】
        [weakSelf praiseShowWithOrder:weakSelf.transferOrderSingleModel];
    } else if ([button.stringTag isEqualToString:[NSString stringWithFormat:@"rightButton%li",(long)MMHOrderTypeFinish]]){  // 已完成
        if (self.transferOrderSingleModel.goodsList.count == 1){
            MMHOrderWithGoodsListModel *goodsModel = [weakSelf.transferOrderSingleModel.goodsList lastObject];
            [weakSelf buyAgainWithOrderNo:weakSelf.transferOrderSingleModel.orderNo itemId:goodsModel.itemId];
        } else if (self.transferOrderSingleModel.goodsList.count > 1){
            weakSelf.floatTempButton.enabled = YES;
            MMHOrderBuyAgainViewController *buyAgainViewController = [[MMHOrderBuyAgainViewController alloc]init];
            buyAgainViewController.transferOrderSingleModel = weakSelf.transferOrderSingleModel;
            [weakSelf.navigationController pushViewController:buyAgainViewController animated:YES];
        }
    }
}

-(void)typeButtonClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    
    NSArray *tempArr = [button.stringTag componentsSeparatedByString:@"-"];

    if (tempArr.count == 3){
        NSString *buttonString = [tempArr objectAtIndex:0];
        NSInteger indexPathWithSection = [[tempArr objectAtIndex:1] integerValue];
        NSInteger indexPathWithRow = [[tempArr objectAtIndex:2] integerValue];
        NSInteger sectionOfReceipt = [self cellIndexPathSectionWithcellData:@"收货信息"];
        MMHShopCartModel *shopCartModel = [self.orderDetailModel.data objectAtIndex:(indexPathWithSection - 1 - sectionOfReceipt)];
        MMHSingleProductWithShopCartModel *singleProductModel = [shopCartModel.goodsList objectAtIndex: indexPathWithRow - 1];

        __weak typeof(self)weakSelf = self;
        if ([buttonString isEqualToString:@"orderSubTypeButton"]){                      // 【待评价 - 申请退款】
            if (singleProductModel.isRefunded){
                MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                refundDetailViewController.refundDetailPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
                refundDetailViewController.transferOrderNo = weakSelf.orderDetailModel.orderNo;
                refundDetailViewController.transferItemId = singleProductModel.itemId;
                refundDetailViewController.transferProductModel = singleProductModel;
                [weakSelf.navigationController pushViewController:refundDetailViewController animated:YES];
            } else {
                [weakSelf sendRequestWithApplyRefundWithOrderNo:weakSelf.orderDetailModel.orderNo itemId:singleProductModel];
            }
        } else if ([buttonString isEqualToString:@"orderTypeButton"]){
            if (self.orderDetailModel.orderStatus == MMHOrderTypeBeShipped){                               // 【待发货 - 申请退款】
                if (singleProductModel.isRefunded){
                    MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                    refundDetailViewController.refundDetailPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
                    refundDetailViewController.transferOrderNo = weakSelf.orderDetailModel.orderNo;
                    refundDetailViewController.transferItemId = singleProductModel.itemId;
                    refundDetailViewController.transferProductModel = singleProductModel;
                    [weakSelf.navigationController pushViewController:refundDetailViewController animated:YES];
                } else {
                    [weakSelf sendRequestWithApplyRefundWithOrderNo:weakSelf.orderDetailModel.orderNo itemId:singleProductModel];
                }
            } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeReceipt){                        // 【待收货 - 申请退款】
                if (singleProductModel.isRefunded){
                    MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                    refundDetailViewController.refundDetailPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
                    refundDetailViewController.transferOrderNo = weakSelf.orderDetailModel.orderNo;
                    refundDetailViewController.transferItemId = singleProductModel.itemId;
                    refundDetailViewController.transferProductModel = singleProductModel;
                    [weakSelf.navigationController pushViewController:refundDetailViewController animated:YES];
                } else {
                    [weakSelf sendRequestWithApplyRefundWithOrderNo:weakSelf.orderDetailModel.orderNo itemId:singleProductModel];
                }
            } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation){                     // 【待评价】
                [weakSelf buyAgainWithOrderNo:weakSelf.orderDetailModel.orderNo itemId:singleProductModel.itemId];
                
            } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeRefund){
                if (singleProductModel.isRefunded){
                    MMHRefundDetailViewController *refundDetailViewController = [[MMHRefundDetailViewController alloc]init];
                    refundDetailViewController.refundDetailPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
                    refundDetailViewController.transferOrderNo = weakSelf.orderDetailModel.orderNo;
                    refundDetailViewController.transferItemId = singleProductModel.itemId;
                    refundDetailViewController.transferProductModel = singleProductModel;
                    [weakSelf.navigationController pushViewController:refundDetailViewController animated:YES];
                } else {
                    [weakSelf sendRequestWithApplyRefundWithOrderNo:weakSelf.orderDetailModel.orderNo itemId:singleProductModel];
                }
            }else if (self.orderDetailModel.orderStatus == MMHOrderTypeFinish){
                MMHOrderWithGoodsListModel *goodsModel = [self.transferOrderSingleModel.goodsList objectAtIndex:(indexPathWithRow - 1)];
                [weakSelf buyAgainWithOrderNo:self.transferOrderSingleModel.orderNo itemId:goodsModel.itemId];
            }
        }
    }
}

#pragma mark 跳转物流
-(void)pushToExpress{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetExpressInfoWithOrderNo:weakSelf.orderDetailModel.orderNo];
}

#pragma mark 再次购买
-(void)buyAgainWithOrderNo:(NSString *)orderNo itemId:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestBuyAgain:orderNo items:itemId from:nil succeededHandler:^(MMHExpressModel *expressModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        MMHCartViewController *cartViewController = [[MMHCartViewController alloc]init];
        [strongSelf.navigationController pushViewController:cartViewController animated:YES];
    } failedHandler:^(NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =  weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 创建浮层
-(void)createFloatView{
    self.floatView = [[UIView alloc]init];
    self.floatView.backgroundColor = [UIColor whiteColor];
    self.floatView.frame = CGRectMake(0,self.view.bounds.size.height - MMHFloat(48),self.view.bounds.size.width,MMHFloat(48));
    [self.view addSubview:self.floatView];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, 0, self.floatView.bounds.size.width, .5f);
    [self.floatView addSubview:lineView];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:MMHFloat(16.)];
    [rightButton addTarget:self action:@selector(floatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    rightButton.layer.cornerRadius = MMHFloat(6.);
    rightButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [rightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(self.view.bounds.size.width - MMHFloat(100) - MMHFloat(11), MMHFloat(4), MMHFloat(100), MMHFloat(40));
    [self.floatView addSubview:rightButton];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.titleLabel.font = [UIFont systemFontOfSize:MMHFloat(16.)];
    [leftButton addTarget:self action:@selector(floatButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    leftButton.layer.cornerRadius = MMHFloat(6.);
    leftButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    leftButton.layer.borderWidth = .5f;
    [leftButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(self.view.bounds.size.width - 2 * MMHFloat(100) -  2 * MMHFloat(11), MMHFloat(4), MMHFloat(100), MMHFloat(40));
    [self.floatView addSubview:leftButton];
    
    if (self.orderType == MMHOrderTypeBeRefund){
        if (self.transferOrderSingleModel.deliveryId == MMHRefundTypeExpress){
            [leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
            leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)MMHOrderTypeBeRefund];
            leftButton.hidden = NO;
            leftButton.frame = rightButton.frame;
            rightButton.hidden = YES;
            self.floatView.hidden = NO;
        } else {
            self.floatView.hidden = YES;
        }
    } else {
        if (self.orderDetailModel.orderStatus == MMHOrderTypeBeShipped){                   // 待发货
            rightButton.hidden = YES;
            leftButton.frame = rightButton.frame;
            [leftButton setTitle:@"提醒发货" forState:UIControlStateNormal];
            [leftButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
            leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)self.orderDetailModel.orderStatus];
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation){         // 待评价
            [rightButton setTitle:@"评价晒单" forState:UIControlStateNormal];
            rightButton.stringTag = [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus];
            
            if (self.transferOrderSingleModel.deliveryId == MMHRefundTypeExpress){
                [leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
                leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)self.orderDetailModel.orderStatus];
                leftButton.hidden = NO;
            } else {
                leftButton.hidden = YES;
            }
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeBeReceipt){            // 待收货
            if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeDelivery){             //货到付款
                leftButton.hidden = YES;
            } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeSelf){          // 上门自提
                leftButton.hidden = YES;
            } else if (self.transferOrderSingleModel.deliveryId == MMHReceivingStateTypeExpress){       // 物流
                leftButton.hidden = NO;
                [leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
                leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)self.orderDetailModel.orderStatus];
            }
                
            [rightButton setTitle:@"确认收货" forState:UIControlStateNormal];
            rightButton.stringTag = [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus];
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeComplete){             // 已完成
            leftButton.frame = rightButton.frame;
            [leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
            leftButton.stringTag = @"orderTypeComplete";
            
            rightButton.hidden = YES;
        } else if(self.orderDetailModel.orderStatus == MMHOrderTypeBeRefund){               // 待退款
            self.floatView.hidden = YES;
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypePendingPay){           //  待付款
            [rightButton setTitle:@"立即付款" forState:UIControlStateNormal];
            leftButton.hidden = YES;
            // 创建失效label
            UIImageView *failureImageView = [[UIImageView alloc]init];
            failureImageView.backgroundColor = [UIColor clearColor];
            failureImageView.image = [UIImage imageNamed:@"orderdetails_icon_time"];
            failureImageView.frame = CGRectMake(MMHFloat(10), (MMHFloat(48) - MMHFloat(15)) / 2., MMHFloat(15), MMHFloat(15));
            [self.floatView addSubview:failureImageView];
            
            // 创建失效时间
            self.failureTimeLabel = [[UILabel alloc]init];
            self.failureTimeLabel.frame = CGRectMake(CGRectGetMaxX(failureImageView.frame) + MMHFloat(5), MMHFloat(48 - 20)/2., MMHFloat(200), MMHFloat(20));
            self.failureTimeLabel.backgroundColor = [UIColor clearColor];
            self.failureTimeLabel.font = [UIFont systemFontOfSize:15.];
            self.failureTimeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            [self.floatView addSubview:self.failureTimeLabel];
            
            rightButton.stringTag = [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus];
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeUnable){                // 已失效
            rightButton.hidden = YES;
            leftButton.frame = rightButton.frame;
            [leftButton setTitle:@"删除订单" forState:UIControlStateNormal];
            leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)self.orderDetailModel.orderStatus];
        } else if (self.orderDetailModel.orderStatus == MMHOrderTypeFinish){                // 已完成
            if (self.transferOrderSingleModel.deliveryId == MMHRefundTypeExpress){
                [leftButton setTitle:@"查看物流" forState:UIControlStateNormal];
                leftButton.stringTag = [NSString stringWithFormat:@"leftButton%li",(long)self.orderDetailModel.orderStatus];
                leftButton.hidden = NO;
            } else {
                leftButton.hidden = YES;
            }
            
            [rightButton setTitle:@"再次购买" forState:UIControlStateNormal];
            rightButton.stringTag = [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus];
        } else if (self.orderDetailModel.orderStatus ==MMHOrderTypeCancel){                 // 已取消
            self.floatView.hidden = YES;
        }
    }
}

#pragma mark - typeAction
#pragma mark 代付款 计时器
-(void)timerFireMethod:(NSTimer *)timer{
    self.transferOrderSingleModel.remainMinutes--;
    if (self.transferOrderSingleModel.remainMinutes <= 0){
        self.failureTimeLabel.text = @"订单已失效,请重新生成。";
        UIButton *rightButton = (UIButton *)[self.floatView viewWithStringTag: [NSString stringWithFormat:@"rightButton%li",(long)self.orderDetailModel.orderStatus]];
        [rightButton setTitle:@"订单失效" forState:UIControlStateNormal];
        [rightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        rightButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
        rightButton.enabled = NO;
    } else {
       self.failureTimeLabel.text = [MMHTool getCountdownWithTargetDate:self.transferOrderSingleModel.targetDate];
    }
}


#pragma mark 去评价
-(void)praiseShowWithOrder:(MMHOrderSingleModel *)orderSingleModel{
    MMHPraiseShowOrderViewController *praiseShowOrderViewController = [[MMHPraiseShowOrderViewController alloc]init];

    __weak typeof(self)weakSelf = self;
    praiseShowOrderViewController.orderSingleModel = orderSingleModel;
    praiseShowOrderViewController.pingjiaCallBackBlock = ^(BOOL isAllPraise){
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isAllPraise){
            strongSelf.isPraiseAllDone = YES;
            //评价完成
            
            
        }
    };

    [self.navigationController pushViewController:praiseShowOrderViewController animated:YES];
    self.floatTempButton.enabled = YES;
}



#pragma mark - SendRequest
#pragma makr 获取订单详情
-(void)sendRequestWithGetOrderDetail{
    [self.view showProcessingView];
    self.orderDetailModel = [[MMHOrderDetailModel alloc]init];
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchmodelGetOrderDetailWithOrderNo:weakSelf.transferOrderSingleModel.orderNo queryType:weakSelf.orderType from:nil succeededHandler:^(MMHOrderDetailModel *orderDetailModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.orderDetailModel = orderDetailModel;
        [strongSelf createFloatView];                     // 【创建浮层】
        [strongSelf pageSetting];                         //  页面设置
        [strongSelf arrayWithSetting];                  // 【初始化数据源】
        [strongSelf.orderDetailTableView reloadData];
        
        if (orderDetailModel.data.count){
            // 1. 查找收货信息的section
            NSInteger sectionOfReceipt = [strongSelf cellIndexPathSectionWithcellData:@"收货信息"];
            if (sectionOfReceipt != -1){
                for (int i = 0 ; i <orderDetailModel.data.count;i++){
                    MMHShopCartModel *shopCartModel = [orderDetailModel.data objectAtIndex:i];
                    NSMutableArray *tempArr = [NSMutableArray array];
                    if (shopCartModel.shopId.length){
                        if (shopCartModel.shopName.length){
                            [tempArr addObject:shopCartModel.shopName];
                        } else {
                            [tempArr addObject:@"无法获取到商店名称"];
                        }
                    } else {
                        [tempArr addObject:@"妈妈好平台"];
                    }
                    
                    for (int j = 0 ; j < shopCartModel.goodsList.count;j++){
                        [tempArr addObject:[NSString stringWithFormat:@"%i",j]];
                    }
                    [strongSelf.dataSourceMutableArray insertObject:tempArr atIndex:sectionOfReceipt + 1 + i];
                    NSMutableIndexSet *productInfoSet = [[NSMutableIndexSet alloc]initWithIndex:(sectionOfReceipt + 1 + i)];
                    [strongSelf.orderDetailTableView insertSections:productInfoSet withRowAnimation:UITableViewRowAnimationFade];
                }
            }
        }
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.orderDetailTableView showTipsWithError:error];
        strongSelf.orderDetailTableView.scrollEnabled = NO;
    }];
}

#pragma mark 申请退款
-(void)sendRequestWithApplyRefundWithOrderNo:(NSString *)orderNo itemId:(MMHSingleProductWithShopCartModel *)productModel{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelWithApplyRefundWithOrderNo:orderNo itemId:productModel.itemId from:nil succeededHandler:^(MMHRefundRequestModel *refundRequestModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ((strongSelf.orderDetailModel.orderStatus == MMHOrderTypeBeReceipt) || (strongSelf.orderDetailModel.orderStatus == MMHOrderTypeFinish)){                 // 待收货
            MMHRefundInfoViewController *refundHomeViewController = [[MMHRefundInfoViewController alloc]init];
            refundHomeViewController.transferItemId = productModel.itemId;
            refundHomeViewController.transferOrderNo = orderNo;
            refundHomeViewController.refundTypeWithHome = MMHRefundTypeWithHomeProduct;
            refundHomeViewController.transferRequestModel = refundRequestModel;
            refundHomeViewController.transferProductModel = productModel;
            
            [strongSelf.navigationController pushViewController:refundHomeViewController animated:YES];
        } else if ((strongSelf.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation) || (strongSelf.orderDetailModel.orderStatus == MMHOrderTypeBeShipped)){              // 待评价 || 待发货
            MMHRefundInfoViewController *refundInfoViewController = [[MMHRefundInfoViewController alloc]init];
            refundInfoViewController.transferItemId = productModel.itemId;
            refundInfoViewController.transferOrderNo = orderNo;
            refundInfoViewController.transferRefundType = refundRequestModel.deliveryWay;
            refundInfoViewController.transferRequestModel = refundRequestModel;
            refundInfoViewController.transferProductModel = productModel;
            if(strongSelf.orderDetailModel.orderStatus == MMHOrderTypeBeEvaluation){           // 退货
                refundInfoViewController.refundTypeWithHome = MMHRefundTypeWithHomeProduct;
            } else {
                refundInfoViewController.refundTypeWithHome = MMHRefundTypeWithHomeMoney;
            }
            [strongSelf.navigationController pushViewController:refundInfoViewController animated:YES];
        }
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 取消订单
-(void)sendRequestWithCancelOrderWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [weakSelf.view showProcessingView];
    [[MMHNetworkAdapter sharedAdapter] fetchModelCancelOrderWithOrderNo:orderNo from:nil succeededHandler:^{
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"成功取消订单" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf.cancelOrder(strongSelf.transferOrderSingleModel);               // 取消订单
            [strongSelf.navigationController popViewControllerAnimated:YES];
            
        }] show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 提醒发货
-(void)sendRequestToRemindDeliveryWithOrderNo:(NSString *)orderNo{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelToRemindDeliveryWithOrderNo:orderNo from:nil succeededHandler:^{
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTips:@"妈妈好已为您提醒卖家发货了哟"];
        
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 删除订单
-(void)sendRequestToDeleteOrderWithOrderNo:(NSString *)orderNo {
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchmodelDeleteOrderWithOrderNo:orderNo from:nil succeededHandler:^{
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong  typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [[UIAlertView alertViewWithTitle:nil message:@"删除成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf.deleteOrderBlock(self.transferOrderSingleModel);
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }]show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 确认收货
-(void)sendRequestWithConfirmReceiptWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [weakSelf.view showProcessingView];
    [[MMHNetworkAdapter sharedAdapter] fetchModelConfirmReceiptWithOrderNo:orderNo from:nil succeededHandler:^(MMHOrderDetailModel *orderListModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [[UIAlertView alertViewWithTitle:nil message:@"确认收货成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf.confirmReceiptBlock(self.transferOrderSingleModel);
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }]show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}


#pragma mark - OtherManager
#pragma mark BackIndexPathSection
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.dataSourceMutableArray.count ; i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

#pragma mark BackIndexPathIndexPathRow
-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = 0;
    for (int i = 0 ; i < self.dataSourceMutableArray.count ; i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark IsHasPrefix
-(BOOL)cellIndexIsHasData:(NSString *)string{
    BOOL isHas = NO;
    for (int i = 0 ; i <self.dataSourceMutableArray.count;i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ;j <dataTempArr.count;j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                isHas = YES;
                break;
            }
        }
    }
    return isHas;
}

#pragma mark  立即付款
-(void)payMoment{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToPayWithOrderNo:weakSelf.orderDetailModel.orderNo];
}


#pragma mark 立即付款
-(void)sendRequestToPayWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] sendRequestToPayWithOrderNo:orderNo from:nil succeededHandler:^(MMHOrderListPay2Model *payInfoModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        if ([payInfoModel.price floatValue] != 0){// 【跳转支付宝】
            [strongSelf weakUpAliPayWithPrice:payInfoModel.price orderNo:orderNo productName:payInfoModel.goodName productDesc:payInfoModel.desc];
        } else {                                  // 【跳转妈妈好】
            [strongSelf sendRequestWithGetSettingStatusWithOrderNo:orderNo];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 唤起支付宝进行支付
-(void)weakUpAliPayWithPrice:(NSString *)price orderNo:(NSString *)orderNo productName:(NSString *)productName productDesc:(NSString *)productDesc {
    MMHPayOrderModel *payOrderModel = [[MMHPayOrderModel alloc]init];
    payOrderModel.amount = price;
    payOrderModel.tradeNO = orderNo;
    payOrderModel.productName = productName;
    payOrderModel.productDescription = productDesc;
    [MMHAliPayHandle goPayWithOrder:payOrderModel];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(aliPayNotification:) name:MMHAlipayNotification object:nil];
}

#pragma mark 支付宝支付通知
-(void)aliPayNotification:(NSNotification *)note{
    NSString *payNotificationState = [note.userInfo objectForKey:@"resultStatus"];
    if([payNotificationState integerValue] == 6001){            // 【用户中途取消】
        [self.view showTips:@"用户中途取消"];
    } else if ([payNotificationState integerValue] == 6002){    // 【网络连接出错】
        [self.view showTips:@"网络连接出错"];
    } else if ([payNotificationState integerValue] == 4000){    // 【订单支付失败】
        [self.view showTips:@"订单支付失败"];
    } else if ([payNotificationState integerValue] == 8000){     // 【订单正在处理中】
        [self.view showTips:@"订单正在处理中"];
    } else if ([payNotificationState integerValue] == 9000){     // 【订单支付成功】
        [[UIAlertView alertViewWithTitle:@"成功" message:@"订单支付成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            
        }]show];
    }
}


#pragma mark 请求接口判断是否注册支付密码
-(void)sendRequestWithGetSettingStatusWithOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestGetIsSettingPayPasswordFrom:nil succeededHandler:^(MMHSettingPaySettingModel *paySettedModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (paySettedModel.isSetted){               // 设置了
            MMHPayView *view = [[MMHPayView alloc]init];
            view.passCodeBlock = ^(NSString *passcode){
                [strongSelf sendRequestWithVerificationPayPassCode:passcode andOrderNo:orderNo];
            };
            view.resetRedirectBlock = ^(){
                MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
                paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
                paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeReset;
                [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
            };
            [view payViewShow];
        } else {                                            // 【提示去设置】
            [[UIAlertView alertViewWithTitle:@"支付安全" message:@"您当前没有设置妈豆支付密码，是否立即设置" buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1){
                    MMHPaySafetyPassCodeViewController *paySafetyPassCodeViewController = [[MMHPaySafetyPassCodeViewController alloc]init];
                    paySafetyPassCodeViewController.transferPhoneNumber = paySettedModel.phone;
                    paySafetyPassCodeViewController.paySafetPageType = MMHPaySafetyPageTypeOrderDetail;
                    [strongSelf.navigationController pushViewController:paySafetyPassCodeViewController animated:YES];
                } else {
                    [strongSelf.view showTips:@"您已取消设置安全密码"];
                }
            }]show];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 验证支付密码
-(void)sendRequestWithVerificationPayPassCode:(NSString *)payPassCode andOrderNo:(NSString *)orderNo{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithCheckPayPassword:payPassCode from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestMBeanCallbackWithOrderNo:orderNo];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 妈豆付款回调
-(void)sendRequestMBeanCallbackWithOrderNo:(NSString *)orderNo{
    __weak typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestWithCallBackWithMBeanOrder:orderNo from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf= weakSelf;
        [[UIAlertView alertViewWithTitle:nil message:@"使用妈豆支付成功" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf.payPassBlock(strongSelf.transferOrderSingleModel);
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }] show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 物流
-(void)sendRequestToGetExpressInfoWithOrderNo:(NSString *)orderNo{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestToGetExpressOrderWithOrderNo:orderNo from:nil succeededHandler:^(MMHExpressListModel *expressListModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        if (expressListModel.logistics.count == 1){
            MMHExpressDetailViewController *expressViewController = [[MMHExpressDetailViewController alloc]init];
            expressViewController.transferExpressModel = [expressListModel.logistics lastObject];
            [strongSelf.navigationController pushViewController:expressViewController animated:YES];
        } else if (expressListModel.logistics.count > 1){
            MMHExpressListViewController *expressListViewController = [[MMHExpressListViewController alloc]init];
            expressListViewController.logistics = expressListModel.logistics;
            [strongSelf.navigationController pushViewController:expressListViewController animated:YES];
        }
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.floatTempButton.enabled = YES;
        [strongSelf.view showTipsWithError:error];
    }];
}

@end
