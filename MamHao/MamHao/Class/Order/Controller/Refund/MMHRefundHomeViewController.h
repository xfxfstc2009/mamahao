//
//  MMHRefundHomeViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHOrderSingleModel.h"

typedef NS_ENUM(NSInteger, MMHRefundTypeWithHome) {
    MMHRefundTypeWithHomeMoney = 2,                 /**< 退款*/
    MMHRefundTypeWithHomeProduct = 1,               /**< 退货*/
};

@interface MMHRefundHomeViewController : AbstractViewController

@property (nonatomic,assign)MMHRefundTypeWithHome refundTypeWithHome;           // 退款类型
@property (nonatomic,assign)CGFloat transferPrice;                              // 上个页面传递的价格
@property (nonatomic,strong)MMHOrderSingleModel *transferOrderSingleModel;      // singleModel
@property (nonatomic,copy)NSString *transferItemId;
@property (nonatomic,copy)NSString *transferOrderNo;
@end
