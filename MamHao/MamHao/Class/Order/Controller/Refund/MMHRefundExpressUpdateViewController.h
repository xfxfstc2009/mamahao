//
//  MMHRefundExpressUpdateViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 修改物流
#import "AbstractViewController.h"
#import "MMHExpressModel.h"
typedef void(^updateExpressBlock)(NSString *expressName,NSString *expressCode, NSString *expressNumber);

@interface MMHRefundExpressUpdateViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferRefundLineId;

@property (nonatomic,copy)updateExpressBlock updateExpressBlock;


@end
