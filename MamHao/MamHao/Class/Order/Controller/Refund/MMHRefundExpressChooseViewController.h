//
//  MMHRefundExpressChooseViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHQueryLogisticsSingleModel.h"

typedef void(^refundExpressChooseBlock)(MMHQueryLogisticsSingleModel *expressSingleModel);

@interface MMHRefundExpressChooseViewController : AbstractViewController


@property (nonatomic,copy)refundExpressChooseBlock refundExpressChooseBlock;
@end
