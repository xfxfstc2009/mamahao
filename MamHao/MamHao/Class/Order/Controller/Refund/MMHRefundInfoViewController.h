//
//  MMHRefundInfoViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 提交审核
#import "AbstractViewController.h"
#import "MMHOrderSingleModel.h"
#import "MMHRefundHomeViewController.h"
#import "MMHRefundDetailViewController.h"
#import "MMHRefundRequestModel.h"
#import "MMHSingleProductWithShopCartModel.h"

typedef void(^refundReloadBlock)();

#pragma mark 判断进入页面
typedef NS_ENUM(NSInteger, MMHRefundInfoPageUsingType) {
    MMHRefundInfoPageUsingTypeNormal,                               /**< 正常调用*/
    MMHRefundInfoPageUsingTypeRefundDetail,                         /**< 退款退货详情调用*/
};


@class MMHRefundHomeViewController;
@class MMHRefundDetailViewController;
@interface MMHRefundInfoViewController : AbstractViewController
//@property (nonatomic,assign)CGFloat transferPrice;                         // 上个页面传递的价格
@property (nonatomic,assign)MMHRefundTypeWithHome refundTypeWithHome;        /**< 退款方式*/
@property (nonatomic,copy)NSString *transferItemId;                          /**< 退款商品*/
@property (nonatomic,copy)NSString *transferOrderNo;                         /**< 退款订单编号*/
@property (nonatomic,assign)MMHRefundType transferRefundType;                /**< 退款送货类型*/
@property (nonatomic,strong)MMHRefundRequestModel *transferRequestModel;     /**< 上个页面传递的请求Model*/
@property (nonatomic,assign)MMHRefundInfoPageUsingType refundInfoPageUsingType;          /**< 退款页面类型*/
@property (nonatomic,copy)refundReloadBlock refundReloadBlock;

@property (nonatomic,strong)MMHSingleProductWithShopCartModel *transferProductModel;
@end
