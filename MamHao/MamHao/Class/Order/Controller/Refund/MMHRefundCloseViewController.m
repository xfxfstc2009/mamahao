//
//  MMHRefundCloseViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 退款关闭
#import "MMHRefundCloseViewController.h"
#import "AbstractViewController+Chatting.h"


@interface MMHRefundCloseViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *refundCloseTableView;
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;
@property (nonatomic,copy)NSString *customString;
@end

@implementation MMHRefundCloseViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self customTextSetting];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

-(void)customTextSetting{
    if (self.refundCloseType == MMHRefundApplyStateCloseNormol){              // 正常关闭
        self.customString = @"您的退款退货申请已经关闭";
    } else if (self.refundCloseType == MMHRefundApplyStateCloseNotPass){      // 未通过关闭
        self.customString = @"您的退款退货申请没有通过妈妈好平台的审核，超时退款关闭";
    }
}

-(void)pageSetting{
    self.barMainTitle = @"退款退货详情";
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"取消 " barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerWithAnimation];
    }];
}
-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    self.refundCloseTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.refundCloseTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.refundCloseTableView.delegate = self;
    self.refundCloseTableView.dataSource = self;
    self.refundCloseTableView.scrollEnabled = NO;
    self.refundCloseTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.refundCloseTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.refundCloseTableView];
}

#pragma mark - UITableViewDataSouce
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        static NSString *cellIdentify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // titleLabel
            UILabel *titleLabel = [[UILabel alloc]init];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.stringTag = @"titleLabel";
            titleLabel.frame = CGRectMake(0, MMHFloat(26), tableView.bounds.size.width, 50);
            titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:titleLabel];
            
            // 创建subTitle
            UILabel *subTitleLabel = [[UILabel alloc]init];
            subTitleLabel.backgroundColor = [UIColor clearColor];
            subTitleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            subTitleLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            subTitleLabel.numberOfLines = 0;
            subTitleLabel.stringTag = @"subTitleLabel";
            [cell addSubview:subTitleLabel];
            
            // 创建button
            UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
            leftButton.backgroundColor = [UIColor clearColor];
            [leftButton addTarget:self action:@selector(chatCusomerClick) forControlEvents:UIControlEventTouchUpInside];
            [leftButton setTitle:@"联系客服" forState:UIControlStateNormal];
            [leftButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
            leftButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            leftButton.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
            leftButton.layer.borderWidth = .5f;
            leftButton.layer.cornerRadius = MMHFloat(5.);
            leftButton.stringTag = @"leftButton";
            [cell addSubview:leftButton];
            
            // 创建右侧按钮
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightButton.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
            [rightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(refundAgainClick) forControlEvents:UIControlEventTouchUpInside];
            rightButton.stringTag = @"rightButton";
            rightButton.layer.cornerRadius = MMHFloat(5.);
            [rightButton setTitle:@"重新申请退款" forState:UIControlStateNormal];
            rightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [cell addSubview:rightButton];
        }
        // 赋值
        UILabel *titlelabel = (UILabel *)[cell viewWithStringTag:@"titleLabel"];
        titlelabel.text = @"退款关闭";
        CGSize titleLabelContentofSize = [titlelabel.text sizeWithCalcFont:titlelabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        titlelabel.size_height = titleLabelContentofSize.height;
        
        // subtitle
        UILabel *subTitleLabel = (UILabel *)[cell viewWithStringTag:@"subTitleLabel"];
        subTitleLabel.text = self.customString;
        if (self.refundCloseType == MMHRefundApplyStateCloseNormol){
            subTitleLabel.textAlignment = NSTextAlignmentCenter;
        } else if (self.refundCloseType == MMHRefundApplyStateCloseNotPass){
            subTitleLabel.textAlignment = NSTextAlignmentLeft;
        }
        CGSize subtitleLabelContentOfSize = [subTitleLabel.text sizeWithCalcFont:subTitleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(20), CGFLOAT_MAX)];
        subTitleLabel.frame = CGRectMake(MMHFloat(20), CGRectGetMaxY(titlelabel.frame) + MMHFloat(16), kScreenBounds.size.width - 2 * MMHFloat(20), subtitleLabelContentOfSize.height);
        
        // leftButton
        UIButton *leftButton = (UIButton *)[cell viewWithStringTag:@"leftButton"];
        leftButton.frame = CGRectMake(MMHFloat(16), CGRectGetMaxY(subTitleLabel.frame) + MMHFloat(22), (kScreenBounds.size.width - 2 * MMHFloat(16) - MMHFloat(15))/2., MMHFloat(44));
        
        // rightButton
        UIButton *rightButton = (UIButton *)[cell viewWithStringTag:@"rightButton"];
        rightButton.frame = CGRectMake(CGRectGetMaxX(leftButton.frame) + MMHFloat(15), leftButton.frame.origin.y, (kScreenBounds.size.width - 2 * MMHFloat(16) - MMHFloat(15))/2., leftButton.bounds.size.height);
        
        return cell;
    } else {
        static NSString *cellIdentifyWithRowother = @"cellIdentifyWithRowother";
        UITableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowother];
        if (!cellWithRowOther){
            cellWithRowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowother];
            cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // bgImageView
            cellWithRowOther.backgroundColor = [UIColor clearColor];
            
            UILabel *headerLabel = [[UILabel alloc]init];
            NSString *string = [NSString stringWithFormat:@"如遇到问题你可以联系客服%@",CustomerServicePhoneNumber];
            NSRange range1 = [string rangeOfString:@"如遇到问题你可以"];
            NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"联系客服%@",CustomerServicePhoneNumber]];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"白灰"] range:range1];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"447ed8"] range:range2];
            CGSize contentOfSize = [string sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            headerLabel.attributedText = str;
            headerLabel.font=[UIFont systemFontOfSize:mmh_relative_float(15.)];
            headerLabel.frame=CGRectMake(MMHFloat(20), MMHFloat(10), contentOfSize.width, contentOfSize.height);
            headerLabel.textAlignment = NSTextAlignmentLeft;
            headerLabel.numberOfLines=1;
            headerLabel.backgroundColor=[UIColor clearColor];
            [cellWithRowOther addSubview:headerLabel];
            
            // 创建imageView
            UIImageView *phoneImageView = [[UIImageView alloc]init];
            phoneImageView.backgroundColor = [UIColor clearColor];
            phoneImageView.image = [UIImage imageNamed:@"contact_icon_smallcall"];
            phoneImageView.frame = CGRectMake(CGRectGetMaxX(headerLabel.frame) + MMHFloat(5), headerLabel.frame.origin.y, MMHFloat(15), MMHFloat(16));
            [cellWithRowOther addSubview:phoneImageView];
            
        }
        return cellWithRowOther;
        
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 1){
        [self clauseClick];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0 ){
        CGFloat cellheight = MMHFloat(26 + 16 + 22 + 25 + 44);
        CGSize titleLabelContentofSize = [@"退款关闭" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        cellheight += titleLabelContentofSize.height;
        CGSize subtitleLabelContentOfSize = [self.customString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(16), CGFLOAT_MAX)];
        cellheight += subtitleLabelContentOfSize.height;
        return cellheight;
    } else{
        return MMHFloat(30);
    }
}

#pragma mark - actionClick
-(void)chatCusomerClick{            // 联系客服
    [self startChattingWithContext:nil];
}

-(void)refundAgainClick{            // 重新退款
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    self.reApplyBlock();
}

#pragma mark - actionClick
-(void)clauseClick{                  // 拨打号码
    __weak MMHRefundCloseViewController *weakViewController = self;
    [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
}

@end
