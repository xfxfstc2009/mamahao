//
//  MMHRefundDetailViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// controller
#import "MMHRefundDetailViewController.h"
#import "MMHRefundExpressUpdateViewController.h"
#import "MMHRefundExpressChooseViewController.h"        // 选择物流
#import "MMHRefundCloseViewController.h"                // 退款关闭页面
#import "MMHExpressDetailViewController.h"              // 物流详情页面
#import "MMHRefundInfoViewController.h"                 // 重新申请页面

// view
#import "MMHRefundInfoWithExpressCell.h"                // 退货信息
#import "MMHPickuperInfoCell.h"                         // 取件人信息
#import "MMHRefundExamineCell.h"                        // 审核结果cell
#import "MMHOrderShopInfoTableViewCell.h"               // 退货地址cell
#import "MMHCustomerSlider.h"
#import "MMHOrderListShopInfoCell.h"

// model
#import "MMHsinceTheMentionAddressModel.h"              // 商店退货地址
#import "MMHNetworkAdapter+Order.h"
#import "AbstractViewController+Chatting.h"


@interface MMHRefundDetailViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *refundTableView;
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArray;
// slider
@property (nonatomic,strong)MMHCustomerSlider *customerSlider;
@property (nonatomic,strong)NSArray *refundStateArray;
// 退货信息
// 自提 - 退货方式选择
@property (nonatomic,strong)NSMutableArray *refundWithSelfTypeMutableArray;     // 自提-退货方式MutableArr;
@property (nonatomic,strong)MMHQueryLogisticsSingleModel *expressSingleModel;   // 退货方式的model
@property (nonatomic,copy)NSString *waybillNumber;                              // 物流单号
@property (nonatomic,strong)MMHExpressModel *expressModel;                      // 物流信息
// 浮层右侧按钮
@property (nonatomic,strong)UIButton *floatRightButton;                         // 浮层右侧按钮
@property (nonatomic,strong)MMHRefundDetailModel *refundDetailModel;            // 接口内容

@end

@implementation MMHRefundDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                 // 页面
    [self arrayWithInit];               // 数组初始化
    [self createTableView];             // 创建tableView
    [self createNotifi];                // 添加键盘通知
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestGetRefundInfoWithOrderNo:weakSelf.transferOrderNo itemId:weakSelf.transferItemId];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"退款详情";
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
            UIViewController *tempViewConteoller;
        for (UIViewController *viewController in weakSelf.navigationController.viewControllers){
            if ([viewController isKindOfClass:[MMHOrderDetailViewController class]]) {
                tempViewConteoller = viewController;
            }
        }
        if (tempViewConteoller){
            [self.navigationController popToViewController:tempViewConteoller animated:YES];
        } else{
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

-(void)arrayWithInit{
    self.dataSourceMutableArray = [NSMutableArray array];
}

#pragma mark - dataWithInit
#pragma mark 自提
-(NSArray *)refundSettingWithSelf:(NSArray *)tempArr{
    if (self.refundApplyState == MMHRefundApplyStateNormol){                // 审核中
        tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"客服电话"]];
    } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
        tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退货地址",@"退货地址1"],@[@"客服电话"]];
        if (self.refundDetailModel.status == 4){
            tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"],@[@"客服电话"]];
        }
    } else if (self.refundApplyState == MMHRefundApplyStateFail){
        tempArr = @[@[@"退款状态"],@[@"原因",@"原因详情"],@[@"客服电话"]];
    }
    return tempArr;
}

#pragma mark 物流配送
-(NSArray *)refundSettingWithExpress:(NSArray *)tempArr{
    if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){                                                    // 退货
        if (self.refundApplyState == MMHRefundApplyStateNormol){                // 审核中
            tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){        // 成功
            if (self.refundState == MMHRefundStateInspection){                      // 验货
                if (self.refundIsGoodsReceipt == MMHRefundIsGoodsReceiptYes){           // 收到货
                    tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"客服电话"]];
                } else {                                                                // 没有收到货
                    tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"寄件地址信息",@"收件人:",@"电话:",@"邮编:",@"地址:",@"备注:"],@[@"退货信息",@"退货信息内容",@"更改退货物流"],@[@"客服电话"]];
                }
            } else if (self.refundState == MMHRefundStateApply){
                if (self.refundDetailModel.waybillNumber.length){
                  tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"寄件地址信息",@"收件人:",@"电话:",@"邮编:",@"地址:",@"备注:"],@[@"退货信息",@"退货信息内容",@"更改退货物流"],@[@"客服电话"]];
                    // 获取物流跟踪信息
                    __weak typeof(self)weakSelf = self;
                    [weakSelf sendRequestWithGetLogisticsTrackingWaybillNumber:weakSelf.refundDetailModel.waybillNumber platformCode:weakSelf.refundDetailModel.platformCode];
                } else {
                    tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"寄件地址信息",@"收件人:",@"电话:",@"邮编:",@"地址:",@"备注:"],@[@"退货信息填写",@"退货物流",@"物流单号"],@[@"客服电话"]];
                }
            } else if (self.refundState == MMHRefundStateSuccess){
              tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"]];
            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){           // 失败
            tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"原因",@"原因详情"],@[@"客服电话"]];
        }
    } else {                                                                    // 退款
        if (self.refundApplyState == MMHRefundApplyStateNormol){
            tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
            tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            tempArr = @[@[@"退款状态"],@[@"原因",@"原因详情"],@[@"客服电话"]];
        }
    }
    return tempArr;
}

#pragma mark 门店配送
-(NSArray *)refundSettingWithShop:(NSArray *)tempArr{
    if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){                                                    // 退货
        if (self.refundApplyState == MMHRefundApplyStateNormol){                    // 审核中
            tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
            if (self.refundIsGoodsReceipt == MMHRefundIsGoodsReceiptNo){
                self.refundPayType = MMHRefundPayTypeOnLine;
                tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"取件人信息",@"所属门店",@"联系方式",@"上门取件时间"] ,@[@"客服电话"]];
            } else {
               tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"取件人信息",@"所属门店",@"联系方式",@"上门取件时间"] ,@[@"客服电话"]];
            }
            if (self.refundDetailModel.status == 4){
                   tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"] ,@[@"客服电话"]];
            }
            
            //            if (self.refundState == MMHRefundStateSuccess){
//                tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"],@[@"客服电话"]];
//            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"原因",@"原因详情"],@[@"客服电话"]];
        }
    } else{                                                                     // 退款
        if (self.refundApplyState == MMHRefundApplyStateNormol){                    // 审核中
            tempArr = @[@[@"退款状态"],@[@"申请审核",@"申请审核内容"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
            tempArr = @[@[@"退款状态"],@[@"退款完成状态",@"退款完成状态详情"],@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"],@[@"客服电话"]];
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            tempArr = @[@[@"退款状态"],@[@"原因",@"原因详情"],@[@"客服电话"]];
        }
    }
    return tempArr;
}

#pragma mark 其他设置
-(NSArray *)refundSetting{
    NSArray *tempArr;
    if (self.refundType == MMHRefundTypeExpress){                                   // 【物流】
        tempArr = [NSArray arrayWithArray:[self refundSettingWithExpress:tempArr]];
    } else if (self.refundType == MMHRefundTypeSelf){                               // 【自提】
        if (self.refundShopType == MMHRefundShopTypeExpress){                           // 物流
            tempArr = [NSArray arrayWithArray:[self refundSettingWithExpress:tempArr]];
        } else if (self.refundShopType == MMHRefundShopTypeShopGet){
            tempArr = [NSArray arrayWithArray:[self refundSettingWithShop:tempArr]];
        } else if (self.refundShopType == MMHRefundShopTypeSelf){
            tempArr = [NSArray arrayWithArray:[self refundSettingWithSelf:tempArr]];
        }
    } else if (self.refundType == MMHRefundTypeShop){                               // 【门店配送】
        tempArr = [NSArray arrayWithArray:[self refundSettingWithShop:tempArr]];
    }
    [self refundExamineSetting];                                                    // 设置状态栏
    if ((self.refundApplyState == MMHRefundApplyStateCloseNotPass) || (self.refundApplyState == MMHRefundApplyStateCloseNormol)){
        tempArr = @[@[@"退款关闭"],@[@"客服电话"]];
    }
    return tempArr;
}

#pragma mark 物流状态设置
-(void)refundExamineSetting{
    if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){                                                    // 退货
        if ((self.refundType == MMHRefundTypeExpress) || ((self.refundShopType == MMHRefundShopTypeExpress) && self.refundType == MMHRefundTypeSelf)){                                      // 【物流】
                self.refundStateArray = @[@"申请退货",@"平台审核",@"平台验货",@"退款完成"];
        } else if ((self.refundType == MMHRefundTypeShop) || ((self.refundType == MMHRefundTypeSelf) && (self.refundShopType == MMHRefundShopTypeShopGet))){                               // 【门店配送】
            self.refundStateArray = @[@"申请退款",@"门店审核",@"退款完成"];
        } else if (self.refundShopType == MMHRefundShopTypeSelf && self.refundType == MMHRefundTypeSelf){
            self.refundStateArray = @[@"申请退款",@"门店审核",@"退款完成"];
        }
    } else {
        if ((self.refundApplyState == MMHRefundApplyStateNormol) || (self.refundApplyState == MMHRefundApplyStateSuccess)){
            self.refundStateArray = @[@"门店审核",@"退款完成"];
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            self.refundStateArray = @[@"门店审核",@"退款失败"];
        }
    }
}

#pragma mark 物流修改
-(void)refundExamineChange{
    if ((self.refundType == MMHRefundTypeExpress) || ((self.refundType == MMHRefundTypeSelf) && self.refundShopType == MMHRefundShopTypeExpress)){                                   // 【物流】
        if (self.refundApplyState == MMHRefundApplyStateNormol){                            // 审核中
            [self.customerSlider moveToIndex:1];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){                    // 审核成功
            if (self.refundState == MMHRefundStateInspection){
                [self.customerSlider moveToIndex:2];
            } else if (self.refundState == MMHRefundStateSuccess){
                [self.customerSlider moveToIndex:4];
            } else {
                [self.customerSlider moveToIndex:1];
            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){                       // 审核失败
            [self.customerSlider moveToIndex:1];
        }
    }  else if ((self.refundType == MMHRefundTypeShop) || ((self.refundShopType == MMHRefundShopTypeShopGet) && (self.refundType == MMHRefundTypeSelf))){                               // 【门店配送】
        if (self.refundApplyState == MMHRefundApplyStateNormol){                            // 审核中
            [self.customerSlider moveToIndex:1];
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){                    // 审核成功
            if (self.refundState == MMHRefundStateSuccess){
                [self.customerSlider moveToIndex:2];
            } else {
                [self.customerSlider moveToIndex:1];
            }
        } else if (self.refundApplyState == MMHRefundApplyStateFail){                       // 审核失败
            [self.customerSlider moveToIndex:1];
        }
    } else if (self.refundShopType == MMHRefundShopTypeSelf && self.refundType == MMHRefundTypeSelf){
        if (self.refundDetailModel.status == 4){
            [self.customerSlider moveToIndex:2];
        } else {
            [self.customerSlider moveToIndex:1];
        }
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.refundTableView){
        self.refundTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - MMHFloat(48)) style:UITableViewStylePlain];
        self.refundTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.refundTableView.delegate = self;
        self.refundTableView.dataSource = self;
        self.refundTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.refundTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.refundTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArray = [self.dataSourceMutableArray objectAtIndex:section];
    return sectionOfArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款状态"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 创建slider
            self.customerSlider = [[MMHCustomerSlider alloc]initWithFrame:CGRectMake(MMHFloat(20), MMHFloat(-10), tableView.bounds.size.width - 2 * MMHFloat(20), MMHFloat(80)) items:self.refundStateArray];
            self.customerSlider.enabled = YES;
            self.customerSlider.progressColor = [UIColor colorWithCustomerName:@"分割线"];
            [self.customerSlider setTitlesColor:[UIColor colorWithCustomerName:@"浅灰"]];
            self.customerSlider.enabled = NO;
            [cellWithRowOne addSubview: self.customerSlider];
        }
        [self refundExamineChange];
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"审核结果"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowTwo = @"cellIdenityfWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                
                [self createCustomerViewWithSubView:cellWithRowTwo cellHeight:cellHeight signImage:@"icon_returns_check" title:@"审核结果"];
            }
            
            
            // 创建bgImagView
            UIImageView *bgImageView = (UIImageView *)cellWithRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"fixedLabel"];
            if (self.refundState == MMHRefundStateInspection){
                fixedLabel.text = @"平台验货";
            }
            
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowTwoTwo = @"cellIdentifyWithRowTwoTwo";
            MMHRefundExamineCell *cellWithRowTwoTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwoTwo];
            if (!cellWithRowTwoTwo){
                cellWithRowTwoTwo = [[MMHRefundExamineCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwoTwo];
                cellWithRowTwoTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithRowTwoTwo.backgroundView = bgImageView;
            }
            cellWithRowTwoTwo.refundApplyState = self.refundApplyState;
            cellWithRowTwoTwo.refundPayType = self.refundPayType;
            cellWithRowTwoTwo.refundType = self.refundType;
            cellWithRowTwoTwo.refundShopType = self.refundShopType;
            cellWithRowTwoTwo.arriveTimeString = self.refundDetailModel.arriveTime;
            cellWithRowTwoTwo.refundState = self.refundState;
            cellWithRowTwoTwo.refundTypeWithHome = self.refundTypeWithHome;
            cellWithRowTwoTwo.channelString = self.refundDetailModel.shopName.length?self.refundDetailModel.shopName:@"妈妈好平台";
            if ((self.refundType == MMHRefundTypeShop )|| ((self.refundType == MMHRefundTypeSelf) && self.refundShopType)){
                // 【门店配送】
                if (self.refundTypeWithHome == 1){                  // 退货
                    if (self.refundPayType == MMHRefundPayTypeOnLine){                  // [线上]
                        cellWithRowTwoTwo.subTitleString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                    } else if (self.refundPayType == MMHRefundPayTypeCashOnDelivery){   // [线下]
                        cellWithRowTwoTwo.subTitleString = @"您购买的时候是货到付款，门店人员上门取件的时候会当场退还支付款，请注意查收";
                    }
                } else {                                            // 退款
                    cellWithRowTwoTwo.subTitleString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                }
            } else if (self.refundType == MMHRefundTypeExpress){
                if (self.refundTypeWithHome == MMHRefundTypeWithHomeMoney){
                    cellWithRowTwoTwo.subTitleString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                } else if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){
                    if (self.refundState == MMHRefundStateInspection){
                        cellWithRowTwoTwo.subTitleString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                    } else {
                        cellWithRowTwoTwo.subTitleString = @"请您退货并填写物流信息，请在7天内填写，逾期将自动取消退货";
                    }
                }
            }
            
            
            // 创建bgImagView
            UIImageView *bgImageView = (UIImageView *)cellWithRowTwoTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithRowTwoTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"寄件地址信息"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            
            UIImageView *bgImageView = [[UIImageView alloc]init];
            cellWithRowThr.backgroundView = bgImageView;
            
            // signImageView
            UIImageView *signImageView = [[UIImageView alloc]init];
            signImageView.backgroundColor = [UIColor clearColor];
            signImageView.image = [UIImage imageNamed:@"icon_returns_sent"];
            signImageView.frame = CGRectMake(MMHFloat(16) + MMHFloat(10), (cellHeight - MMHFloat(14)) / 2., MMHFloat(14), MMHFloat(14));
            signImageView.hidden = YES;
            signImageView.stringTag = @"signImageView";
            [cellWithRowThr addSubview:signImageView];
            
            // fixedLabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.numberOfLines = 1;
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            [cellWithRowThr addSubview:fixedLabel];
            
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.backgroundColor = [UIColor clearColor];
            dymicLabel.stringTag = @"dymicLabel";
            dymicLabel.numberOfLines = 0;
            dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            [cellWithRowThr addSubview:dymicLabel];
            
        }
        // 赋值
        UIImageView *signImageView = (UIImageView *)[cellWithRowThr viewWithStringTag:@"signImageView"];
        UILabel *fixedLabel = (UILabel *)[cellWithRowThr viewWithStringTag:@"fixedLabel"];
        UILabel *dymicLabel = (UILabel *)[cellWithRowThr viewWithStringTag:@"dymicLabel"];
        
        fixedLabel.text = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([fixedLabel.text isEqualToString:@"收件人:"]){
            dymicLabel.text = self.refundDetailModel.deliveryAddr.shopName.length?self.refundDetailModel.deliveryAddr.shopName:@"未获取到寄件人信息";
        } else if ([fixedLabel.text isEqualToString:@"电话:"]){
            dymicLabel.text = self.refundDetailModel.deliveryAddr.telephone.length?self.refundDetailModel.deliveryAddr.telephone:@"未获取到寄件信息电话";
        } else if ([fixedLabel.text isEqualToString:@"邮编:"]){
            dymicLabel.text = self.refundDetailModel.deliveryAddr.zipCode.length?self.refundDetailModel.deliveryAddr.zipCode:@"未获取到寄件邮编信息";
        } else if ([fixedLabel.text isEqualToString:@"地址:"]){
            dymicLabel.text = self.refundDetailModel.deliveryAddr.addr.length?self.refundDetailModel.deliveryAddr.addr:@"未获取到寄件地址信息";
        } else if ([fixedLabel.text isEqualToString:@"备注:"]){
            dymicLabel.text = self.refundDetailModel.deliveryAddr.remark.length?self.refundDetailModel.deliveryAddr.remark:@"未获取到寄件备注信息";
        }
        
        if (indexPath.row == 0){
            signImageView.hidden = NO;
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(10), 0 ,200, cellHeight);
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            dymicLabel.hidden = YES;
        } else {
            signImageView.hidden = YES;
            CGSize contentOfSize = [fixedLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
            fixedLabel.frame = CGRectMake(signImageView.frame.origin.x, MMHFloat(10) ,contentOfSize.width, contentOfSize.height);
            
            CGSize dymicOfSize = [dymicLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * (10 + MMHFloat(15)) - contentOfSize.width - MMHFloat(15), CGFLOAT_MAX)];
            dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(15), fixedLabel.frame.origin.y,kScreenWidth - 2 * (10 + MMHFloat(15)) - contentOfSize.width - MMHFloat(15) , dymicOfSize.height);
            
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            dymicLabel.hidden = NO;
        }
        
        // 创建bgImagView
        UIImageView *bgImageView = (UIImageView *)cellWithRowThr.backgroundView;
        bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息填写"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFor = @"cellIdentifyWithRowFor";
            UITableViewCell *cellWithRowFor = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFor];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithRowFor){
                cellWithRowFor = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFor];
                cellWithRowFor.selectionStyle = UITableViewCellSelectionStyleNone;
                
                [self createCustomerViewWithSubView:cellWithRowFor cellHeight:cellHeight signImage:@"icon_returns_info" title:@"退货信息"];
                
            }
            // 创建bgImagView
            UIImageView *bgImageView = (UIImageView *)cellWithRowFor.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            return cellWithRowFor;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            UITableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithRowFiv){
                cellWithRowFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
                
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithRowFiv.backgroundView = bgImageView;
                
                // 创建leftLabel
                UILabel *leftLabel = [[UILabel alloc]init];
                leftLabel.backgroundColor = [UIColor clearColor];
                leftLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                leftLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
                leftLabel.stringTag = @"leftLabel";
                leftLabel.textAlignment = NSTextAlignmentCenter;
                [cellWithRowFiv addSubview:leftLabel];
                
                // 背景
                UIImageView *backgroundImageView = [[UIImageView alloc]init];
                backgroundImageView.stringTag = @"backgroundImageView";
                backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
                [cellWithRowFiv addSubview:backgroundImageView];
                
                
                // userNameTextField
                UITextField *expressTextField = [[UITextField alloc] init];
                [expressTextField setBackgroundColor:[UIColor clearColor]];
                expressTextField.textAlignment=NSTextAlignmentLeft;
                expressTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
                expressTextField.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
                expressTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                expressTextField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
                expressTextField.returnKeyType=UIReturnKeyDone;
                expressTextField.delegate=self;
                expressTextField.tag = 77;
                [cellWithRowFiv addSubview:expressTextField];
                
                // 创建imageView
                UIImageView *dropImageView = [[UIImageView alloc]init];
                dropImageView.backgroundColor = [UIColor clearColor];
                dropImageView.image = [UIImage imageNamed:@"productList_down"];
                dropImageView.stringTag = @"dropImageView";
                [cellWithRowFiv addSubview:dropImageView];
                
            }
            
            // 创建bgImagView
            UIImageView *bgImageView = (UIImageView *)cellWithRowFiv.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            // fixedLabel
            UILabel *fixedLabel = (UILabel *)[cellWithRowFiv viewWithStringTag:@"leftLabel"];
            UITextField *expressTextField = (UITextField *)[cellWithRowFiv viewWithTag:77];
            UIImageView *dropImageView = (UIImageView *)[cellWithRowFiv viewWithStringTag:@"dropImageView"];
            CGSize expressTypeContentOfSize = [@"退货物流:" sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
            fixedLabel.frame = CGRectMake(MMHFloat(25),0, expressTypeContentOfSize.width, cellHeight);
            if (indexPath.row == 1){
                fixedLabel.text = @"退货物流:";
                expressTextField.placeholder = @"请选择您退货物流";
                expressTextField.stringTag = @"expressTextTypeField";
                expressTextField.enabled = NO;
                dropImageView.hidden = NO;
            } else if (indexPath.row == 2){
                fixedLabel.text = @"物流单号:";
                expressTextField.placeholder = @"请填写您的物流单号";
                expressTextField.stringTag = @"expressTextNumberField";
                expressTextField.keyboardType = UIKeyboardTypeDefault;
                expressTextField.enabled = YES;
                dropImageView.hidden = YES;
            }
            
            // lay
            UIImageView *backgroundImageView = (UIImageView *)[cellWithRowFiv viewWithStringTag:@"backgroundImageView"];
            backgroundImageView.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10),MMHFloat(10), tableView.bounds.size.width - 2 * MMHFloat(10) - fixedLabel.frame.size.width -MMHFloat(15 + 13 + 12), MMHFloat(44));
            
            // textfield
            expressTextField.frame = CGRectMake(backgroundImageView.frame.origin.x + MMHFloat(10),MMHFloat(10),backgroundImageView.bounds.size.width - 2 * MMHFloat(10),MMHFloat(44));
            
            dropImageView.frame = CGRectMake(CGRectGetMaxX(backgroundImageView.frame) - MMHFloat(14 + 10), (cellHeight - 14) / 2., 14, 14);
            
            
            return cellWithRowFiv;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"客服电话"]){
        static NSString *cellIdentifyWithRowother = @"cellIdentifyWithRowother";
        UITableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowother];
        if (!cellWithRowOther){
            cellWithRowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowother];
            cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // bgImageView
            cellWithRowOther.backgroundColor = [UIColor clearColor];
            
            
            UILabel *headerLabel = [[UILabel alloc]init];
            NSString *string = [NSString stringWithFormat:@"如遇到问题你可以联系客服%@",CustomerServicePhoneNumber];
            NSRange range1 = [string rangeOfString:@"如遇到问题你可以"];
            NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"联系客服%@",CustomerServicePhoneNumber]];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"白灰"] range:range1];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"蓝"] range:range2];
            CGSize contentOfSize = [string sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            headerLabel.attributedText = str;
            headerLabel.font=[UIFont systemFontOfSize:mmh_relative_float(15.)];
            headerLabel.frame=CGRectMake(MMHFloat(20), 0, contentOfSize.width, contentOfSize.height);
            headerLabel.textAlignment = NSTextAlignmentLeft;
            headerLabel.numberOfLines=1;
            headerLabel.backgroundColor=[UIColor clearColor];
            [cellWithRowOther addSubview:headerLabel];
            
            // 创建imageView
            UIImageView *phoneImageView = [[UIImageView alloc]init];
            phoneImageView.backgroundColor = [UIColor clearColor];
            phoneImageView.image = [UIImage imageNamed:@"contact_icon_smallcall"];
            phoneImageView.frame = CGRectMake(CGRectGetMaxX(headerLabel.frame) + MMHFloat(5), headerLabel.frame.origin.y, MMHFloat(15), MMHFloat(16));
            [cellWithRowOther addSubview:phoneImageView];
            
        }
        return cellWithRowOther;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货方式"]){
        static NSString *cellIdentifyWithRowSix = @"cellidentifyWithRowSix";
        UITableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowSix){
            cellWithRowSix = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
            // 创建titleNav
            [self createCustomerViewWithSubView:cellWithRowSix cellHeight:cellHeight signImage:@"icon_returns_goods" title:@"退货方式"];
        }
        UIImageView *signImageView = (UIImageView *)[cellWithRowSix viewWithStringTag:@"signImageView"];
        UILabel *fixedLabel = (UILabel *)[cellWithRowSix viewWithStringTag:@"fixedLabel"];
        
        if (indexPath.row == 0){
            signImageView.hidden = NO;
            fixedLabel.text = @"退货方式";
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(10), 0 ,200, cellHeight);
        } else if (indexPath.row == 1){
            signImageView.hidden = YES;
            fixedLabel.text = @"自己送货到门店退货";
            fixedLabel.frame = CGRectMake(MMHFloat(16) + MMHFloat(10), 0 ,kScreenBounds.size.width - 2 * MMHFloat(26), cellHeight);
        }
        // 创建bgImagView
        UIImageView *bgImageView = (UIImageView *)cellWithRowSix.backgroundView;
        bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
        
        return cellWithRowSix;
    } else if (indexPath.section  == [self cellIndexPathSectionWithcellData:@"退货地址"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionSevRowOne = @"cellIdentifyWithSectionSevRowOne";
            UITableViewCell *cellWithSectionSevRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionSevRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionSevRowOne){
                cellWithSectionSevRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionSevRowOne];
                
                [self createCustomerViewWithSubView:cellWithSectionSevRowOne cellHeight:cellHeight signImage:@"icon_returns_sent" title:@"退货地址"];
            }
            // 创建bgImagView
            UIImageView *bgImageView = (UIImageView *)cellWithSectionSevRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionSevRowOne;
        } else {
            static NSString *cellIdentifyWithSectionSevRowTwo = @"cellIdentifyWithSectionSevRowTwo";
            MMHOrderListShopInfoCell *cellWithSectionSevRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionSevRowTwo];
            if (!cellWithSectionSevRowTwo){
                cellWithSectionSevRowTwo = [[MMHOrderListShopInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionSevRowTwo];
                cellWithSectionSevRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionSevRowTwo.backgroundView = bgImageView;
            }
            cellWithSectionSevRowTwo.viewWidth = kScreenBounds.size.width - 2 * MMHFloat(11 + 10);
            cellWithSectionSevRowTwo.shopInfoCellUsingClass = MMHShopInfoCellUsingClassRefund;
            cellWithSectionSevRowTwo.transferSinceTheMentionAddressModel = self.refundDetailModel.shopInfo;
            UIImageView *bgImageView = (UIImageView *)cellWithSectionSevRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionSevRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款信息"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionEigRowOne = @"cellIdentifyWithSectionEigRowOne";
            UITableViewCell *cellWithSectionEigRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionEigRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionEigRowOne){
                cellWithSectionEigRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionEigRowOne];
                cellWithSectionEigRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionEigRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionEigRowOne cellHeight:cellHeight signImage:@"icon_returns_info" title:@"退款信息"];
            }
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionEigRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionEigRowOne;
        } else {
            static NSString *cellIdentifyWithSectionEigRowTwo = @"cellIdentifyWithSectionEighRowTwo";
            UITableViewCell *cellWithSectionEigRowTow = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionEigRowTwo];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionEigRowTow){
                cellWithSectionEigRowTow = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionEigRowTwo];
                cellWithSectionEigRowTow.selectionStyle = UITableViewCellSelectionStyleBlue;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionEigRowTow.backgroundView = bgImageView;
                
                // fixedLabel
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.frame = CGRectMake(MMHFloat(10) + MMHFloat(15), 0, tableView.bounds.size.width - 2 * MMHFloat(25), cellHeight);
                fixedLabel.stringTag = @"fixedLabel";
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                [cellWithSectionEigRowTow addSubview:fixedLabel];
            }
            // 赋值
            UILabel *fixedLabel = (UILabel *)[cellWithSectionEigRowTow viewWithStringTag:@"fixedLabel"];
            NSString *fixedLabelString = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            if ([fixedLabelString isEqualToString:@"退款金额"]){
                fixedLabelString =[fixedLabelString stringByAppendingString:[NSString stringWithFormat:@":%.2f",self.refundDetailModel.payRefund.price]];
            } else if ([fixedLabelString isEqualToString:@"退款方式"]){
                fixedLabelString =[fixedLabelString stringByAppendingString:[NSString stringWithFormat:@":%@",self.refundDetailModel.payRefund.refundWay]];
            } else if ([fixedLabelString isEqualToString:@"到款日期"]){
                fixedLabelString =[fixedLabelString stringByAppendingString:[NSString stringWithFormat:@":%@",self.refundDetailModel.payRefund.dateArrival]];
            }
            fixedLabel.text = fixedLabelString;
            
            
            
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionEigRowTow.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionEigRowTow;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"申请审核"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionNeiRowOne = @"cellIdentifyWithSectionNeiRowOne";
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            UITableViewCell *cellWithSectionNeiRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionNeiRowOne];
            if (!cellWithSectionNeiRowOne){
                cellWithSectionNeiRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionNeiRowOne];
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionNeiRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionNeiRowOne cellHeight:cellHeight signImage:@"icon_returns_check" title:@"退货申请已经完成，正在审核中"];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionNeiRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            UILabel *fixedLabel = (UILabel *)[cellWithSectionNeiRowOne viewWithStringTag:@"fixedLabel"];
            if (self.refundType == MMHRefundTypeExpress){           // 物流
                if (self.refundState == MMHRefundStateInspection){          // 验货
                    fixedLabel.text = @"平台验货";
                }
            }
            
            return cellWithSectionNeiRowOne;
        } else if (indexPath.row == 1){
            static NSString *cellIdentifyWithSectionNeiRowTwo = @"cellIdentifyWithSectionNeiRowTwo";
            UITableViewCell *cellWithSectionNeiRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionNeiRowTwo];
            if (!cellWithSectionNeiRowTwo){
                cellWithSectionNeiRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionNeiRowTwo];
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionNeiRowTwo.backgroundView = bgImageView;
                
                // 创建signImageView
                UIImageView *signImageView = [[UIImageView alloc]init];
                signImageView.backgroundColor = [UIColor clearColor];
                signImageView.frame = CGRectMake(MMHFloat(15) + MMHFloat(10), MMHFloat(22), MMHFloat(22), MMHFloat(22));
                signImageView.image = [UIImage imageNamed:@"icon_returns_prompt"];
                signImageView.stringTag = @"signImageView";
                [cellWithSectionNeiRowTwo addSubview:signImageView];
                
                // 创建title
                UILabel *contentLabel = [[UILabel alloc]init];
                contentLabel.stringTag = @"contentLabel";
                contentLabel.backgroundColor = [UIColor clearColor];
                contentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                contentLabel.numberOfLines = 0;
                contentLabel.frame = CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(15), MMHFloat(15), kScreenBounds.size.width - 2 * MMHFloat(10 + 15) - MMHFloat(22) - MMHFloat(15), 200);
                [cellWithSectionNeiRowTwo addSubview:contentLabel];
                
                // 创建button
                UIButton *cancelRefundButton = [UIButton buttonWithType:UIButtonTypeCustom];
                cancelRefundButton.backgroundColor = [UIColor clearColor];
                [cancelRefundButton setTitle:@"取消退货申请" forState:UIControlStateNormal];
                cancelRefundButton.stringTag = @"cancelRefundButton";
                [cancelRefundButton addTarget:self action:@selector(cancelRefundClick) forControlEvents:UIControlEventTouchUpInside];
                cancelRefundButton.layer.cornerRadius = MMHFloat(3.);
                cancelRefundButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [cancelRefundButton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
                cancelRefundButton.layer.borderWidth = 1.;
                cancelRefundButton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
                [cellWithSectionNeiRowTwo addSubview:cancelRefundButton];
                
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionNeiRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            // 创建申请退货内容
            UILabel *contentLabel = (UILabel *)[cellWithSectionNeiRowTwo viewWithStringTag:@"contentLabel"];
            
            NSString *rangeString;
            if ((self.refundType == MMHRefundTypeExpress) || ((self.refundShopType == MMHRefundShopTypeExpress) && (self.refundType == MMHRefundTypeSelf))){           // 物流
                if (self.refundState == MMHRefundStateInspection){          // 验货
                    rangeString = [NSString stringWithFormat:@"平台还未收到货，一旦收到货，我们会在1个小时内完成验货，请耐心等待"];
                } else {
                    rangeString = [NSString stringWithFormat:@"我们会在1个小时内完成申请审核，请耐心等待"];
                }
            } else {
                rangeString = [NSString stringWithFormat:@"我们会在1个小时内完成申请审核，请耐心等待"];
            }
            
            CGSize contentOfSize = [rangeString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(10 + 15) - MMHFloat(22) - MMHFloat(15), CGFLOAT_MAX)];
            contentLabel.attributedText = [MMHTool rangeLabelWithContent:rangeString hltContentArr:@[@"1个小时内"] hltColor:[UIColor colorWithCustomerName:@"红"] normolColor:[UIColor colorWithCustomerName:@"白灰"]];
            contentLabel.size_height = contentOfSize.height;
            [cellWithSectionNeiRowTwo addSubview:contentLabel];
            
            
            
            
            // 创建取消退货按钮
            CGSize buttonContentOfSize = [@"取消退货申请" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(35))];
            UIButton *cancelRefundButton = (UIButton *)[cellWithSectionNeiRowTwo viewWithStringTag:@"cancelRefundButton"];
            cancelRefundButton.frame =CGRectMake(kScreenBounds.size.width - (2 * MMHFloat(10) + buttonContentOfSize.width ) -  10 - MMHFloat(15), CGRectGetMaxY(contentLabel.frame) + MMHFloat(10), buttonContentOfSize.width + 2 * MMHFloat(10), MMHFloat(35));
            
            // aignImageView
            UIImageView *signImageView = (UIImageView *)[cellWithSectionNeiRowTwo viewWithStringTag:@"signImageView"];
            signImageView.frame = CGRectMake(MMHFloat(15) + MMHFloat(10),contentOfSize.height / 2.+contentLabel.frame.origin.y-MMHFloat(22)/2., MMHFloat(22), MMHFloat(22));
            
            
            if ((self.refundType == MMHRefundTypeShop) ||((self.refundType == MMHRefundTypeSelf) && self.refundShopType == MMHRefundShopTypeShopGet)){
                if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){                                                        // 是否收货
                    
                } else {
                    cancelRefundButton.hidden = NO;
                }
            } else if ((self.refundType == MMHRefundTypeExpress) || ((self.refundShopType == MMHRefundShopTypeExpress) && self.refundType == MMHRefundTypeSelf)){
                if (self.refundState == MMHRefundStateInspection){
                    cancelRefundButton.hidden = YES;
                } else {
                    cancelRefundButton.hidden = NO;
                }
            }
            return cellWithSectionNeiRowTwo;
        }
    }
    else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionTenRowOne = @"cellIdentifyWithSectionTenRowOne";
            UITableViewCell *cellWithSectionTenRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTenRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionTenRowOne){
                cellWithSectionTenRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTenRowOne];
                cellWithSectionTenRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionTenRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionTenRowOne cellHeight:cellHeight signImage:@"icon_returns_goods" title:@"退货信息"];
            }
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionTenRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionTenRowOne;
        }
        else if (indexPath.row == 1){
            static NSString *cellIdentifyWithSectionTenRowTwo = @"cellIdentifyWithSectionTenRowTwo";
            MMHRefundInfoWithExpressCell *cellWithSectionTenRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTenRowTwo];
            if (!cellWithSectionTenRowTwo){
                cellWithSectionTenRowTwo = [[MMHRefundInfoWithExpressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTenRowTwo];
                cellWithSectionTenRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionTenRowTwo.backgroundView = bgImageView;
            }
            // 赋值
            cellWithSectionTenRowTwo.expressModel = self.expressModel;
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionTenRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionTenRowTwo;
        } else {
            static NSString *cellIdentifyWithSectionTenRowThr = @"cellIdentifyWithSectionTenRowThr";
            UITableViewCell *cellWithSectionTenRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTenRowThr];
            if (!cellWithSectionTenRowThr){
                cellWithSectionTenRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTenRowThr];
                cellWithSectionTenRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionTenRowThr.backgroundView = bgImageView;
                
                // 创建按钮
                UIButton *updateExpressButton = [UIButton buttonWithType:UIButtonTypeCustom];
                updateExpressButton.backgroundColor = [UIColor clearColor];
                [updateExpressButton setTitle:@"更改退货物流" forState:UIControlStateNormal];
                updateExpressButton.stringTag = @"updateExpressButton";
                [updateExpressButton addTarget:self action:@selector(updateExpressClick) forControlEvents:UIControlEventTouchUpInside];
                updateExpressButton.layer.cornerRadius = MMHFloat(3.);
                updateExpressButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [updateExpressButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
                updateExpressButton.layer.borderWidth = 1.;
                updateExpressButton.layer.borderColor = [UIColor colorWithCustomerName:@"白灰"].CGColor;
                [cellWithSectionTenRowThr addSubview:updateExpressButton];
                
            }
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionTenRowThr.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            CGSize buttonContentOfSize = [@"更改退货物流" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(30))];
            UIButton *updateExpressButton = (UIButton *)[cellWithSectionTenRowThr viewWithStringTag:@"updateExpressButton"];
            updateExpressButton.frame =CGRectMake(kScreenBounds.size.width - (2 * MMHFloat(10) + buttonContentOfSize.width ) -  10 - MMHFloat(15),0 , buttonContentOfSize.width + 2 * MMHFloat(10), MMHFloat(35));
            return cellWithSectionTenRowThr;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"原因"]){
        if (indexPath.row == 0){
            static NSString *celldentifyWithSectionEleRowOne = @"celldentifyWithSectionEleRowOne";
            UITableViewCell *cellWithSectionEleRowOne = [tableView dequeueReusableCellWithIdentifier:celldentifyWithSectionEleRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionEleRowOne){
                cellWithSectionEleRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:celldentifyWithSectionEleRowOne];
                cellWithSectionEleRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionEleRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionEleRowOne cellHeight:cellHeight signImage:@"icon_returns_reason" title:@"原因"];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionEleRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            return cellWithSectionEleRowOne;
        } else if (indexPath.row == 1){
            static NSString *celldentifyWithSectionEleRowTwo = @"celldentifyWithSectionEleRowTwo";
            UITableViewCell *cellWithSectionEleRowTwo = [tableView dequeueReusableCellWithIdentifier:celldentifyWithSectionEleRowTwo];
            if (!cellWithSectionEleRowTwo){
                cellWithSectionEleRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:celldentifyWithSectionEleRowTwo];
                cellWithSectionEleRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionEleRowTwo.backgroundView = bgImageView;
                
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.stringTag = @"fixedLabel";
                fixedLabel.frame = CGRectMake(MMHFloat(15 + 10), MMHFloat(12), kScreenBounds.size.width - 2 * MMHFloat(25), 100);
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.textAlignment = NSTextAlignmentLeft;
                fixedLabel.numberOfLines = 0;
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                [cellWithSectionEleRowTwo addSubview:fixedLabel];
            }
            // 赋值
            UILabel *fixedLabel = (UILabel *)[cellWithSectionEleRowTwo viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = self.refundDetailModel.cause;
            CGSize contentOfSize = [fixedLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(25), CGFLOAT_MAX)];
            fixedLabel.size_height = contentOfSize.height;
            
            UIImageView *bgImageView = (UIImageView *)cellWithSectionEleRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            return cellWithSectionEleRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"取件人信息"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionTrwRowOne = @"cellIdentifyWithSectionTrwRowOne";
            UITableViewCell *cellWithSectionTrwRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTrwRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionTrwRowOne){
                cellWithSectionTrwRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTrwRowOne];
                cellWithSectionTrwRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionTrwRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionTrwRowOne cellHeight:cellHeight signImage:@"icon_returns_reason" title:@"取件人信息"];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionTrwRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionTrwRowOne;
        } else {
            static NSString *cellIdentifyWithSectionTrwRowTwo = @"cellIdentifyWithSectionTrwRowTwo";
            UITableViewCell *cellWithSectionTrwRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTrwRowTwo];
            CGFloat contentOfHeight = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
            if (!cellWithSectionTrwRowTwo){
                cellWithSectionTrwRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTrwRowTwo];
                cellWithSectionTrwRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionTrwRowTwo.backgroundView = bgImageView;
                
                
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.stringTag = @"fixedLabel";
                fixedLabel.frame = CGRectMake(MMHFloat(25), MMHFloat(12), kScreenWidth - 2 * MMHFloat(25), contentOfHeight);
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                [cellWithSectionTrwRowTwo addSubview:fixedLabel];
                
                // 创建dymicLabel
                UILabel *dymicLabel = [[UILabel alloc]init];
                dymicLabel.backgroundColor = [UIColor clearColor];
                dymicLabel.stringTag = @"dymicLabel";
                dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [cellWithSectionTrwRowTwo addSubview:dymicLabel];
                
                // 创建 imageView
                UIImageView *phoneImageView = [[UIImageView alloc]init];
                phoneImageView.backgroundColor = [UIColor clearColor];
                phoneImageView.stringTag = @"phoneImageView";
                phoneImageView.image = [UIImage imageNamed:@"contact_icon_smallcall"];
                [cellWithSectionTrwRowTwo addSubview:phoneImageView];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionTrwRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            // fixedLabel
            UILabel *fixedLabel = (UILabel *)[cellWithSectionTrwRowTwo viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, contentOfHeight)];
            fixedLabel.size_width = fixedSize.width;
            
            // dymicLabel
            UILabel *dymicLabel = (UILabel *)[cellWithSectionTrwRowTwo viewWithStringTag:@"dymicLabel"];
            UIImageView *phoneImageView = (UIImageView *)[cellWithSectionTrwRowTwo viewWithStringTag:@"phoneImageView"];
            //            if (indexPath.row == 1){
            //                dymicLabel.text = self.refundDetailModel.recipient.recipientName.length?self.refundDetailModel.recipient.recipientName:@"未获取到取件人信息";
            //                dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            //                phoneImageView.hidden = YES;
            //            } else
            if (indexPath.row == 1){
                dymicLabel.text = self.refundDetailModel.recipient.recipientShopName.length?self.refundDetailModel.recipient.recipientShopName:@"未获取到取件人门店信息";
                dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                phoneImageView.hidden = YES;
            } else if (indexPath.row == 2){
                dymicLabel.text = self.refundDetailModel.recipient.recipientPhone.length?self.refundDetailModel.recipient.recipientPhone:@"未获取到取件人号码信息";
                dymicLabel.textColor = [UIColor colorWithCustomerName:@"蓝"];
                phoneImageView.hidden = NO;
            } else if (indexPath.row == 3){
                dymicLabel.text = [NSString stringWithFormat:@"%@",self.refundDetailModel.recipient.recipientArriveTime.length?self.refundDetailModel.recipient.recipientArriveTime:@"未获取到取件时间"];
                dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                phoneImageView.hidden = YES;
            }
            CGSize dymicContentSize = [dymicLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
            dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), fixedLabel.frame.origin.y, dymicContentSize.width, fixedLabel.frame.size.height);
            
            // phoneImageView
            phoneImageView.frame = CGRectMake(CGRectGetMaxX(dymicLabel.frame) + MMHFloat(3), dymicLabel.frame.origin.y, MMHFloat(15), MMHFloat(16));
            
            return cellWithSectionTrwRowTwo;
            
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货方式选择"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSectionThirteenRowOne = @"cellIdentifyWithSectionThirteenRowOne";
            UITableViewCell *cellWithSectionThirteenRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionThirteenRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionThirteenRowOne){
                cellWithSectionThirteenRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionThirteenRowOne];
                cellWithSectionThirteenRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionThirteenRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithSectionThirteenRowOne cellHeight:cellHeight signImage:@"icon_returns_info" title:@"退货方式"];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionThirteenRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithSectionThirteenRowOne;
        } else {
            static NSString *cellIdentifyWithSectionThirteenRowTwo = @"cellIdentifyWithSectionThirteenRowTwo";
            UITableViewCell *cellWithSectionThirteenRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionThirteenRowTwo];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionThirteenRowTwo){
                cellWithSectionThirteenRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionThirteenRowTwo];
                cellWithSectionThirteenRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithSectionThirteenRowTwo.backgroundView = bgImageView;
                
                // signImageView
                UIImageView *signImageView = [[UIImageView alloc]init];
                signImageView.backgroundColor = [UIColor clearColor];
                signImageView.frame = CGRectMake(MMHFloat(16) + MMHFloat(10), (cellHeight - MMHFloat(14)) / 2., MMHFloat(14), MMHFloat(14));
                signImageView.stringTag = @"signImageView";
                [cellWithSectionThirteenRowTwo addSubview:signImageView];
                
                // fixedLabel
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.stringTag = @"fixedLabel";
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
                fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
                fixedLabel.frame = CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(10), 0 ,200, cellHeight);
                [cellWithSectionThirteenRowTwo addSubview:fixedLabel];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithSectionThirteenRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            
            NSString *cellContent = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            // signImageView
            UIImageView *singnImageView = (UIImageView *)[cellWithSectionThirteenRowTwo viewWithStringTag:@"signImageView"];
            
            UILabel *fixedLabel = (UILabel *)[cellWithSectionThirteenRowTwo viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = cellContent;
            if ([self.refundWithSelfTypeMutableArray containsObject:cellContent]){
                singnImageView.image = [UIImage imageNamed:@"btn_radiot_s"];
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            } else {
                singnImageView.image = [UIImage imageNamed:@"btn_radiot_n"];
                fixedLabel.textColor = [UIColor colorWithCustomerName:@"白灰"];
            }
            return cellWithSectionThirteenRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款关闭"]){
        static NSString *cellIdentify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // titleLabel
            UILabel *titleLabel = [[UILabel alloc]init];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.stringTag = @"titleLabel";
            titleLabel.frame = CGRectMake(0, MMHFloat(26), tableView.bounds.size.width, 50);
            titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:titleLabel];
            
            // 创建subTitle
            UILabel *subTitleLabel = [[UILabel alloc]init];
            subTitleLabel.backgroundColor = [UIColor clearColor];
            subTitleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            subTitleLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            subTitleLabel.numberOfLines = 0;
            subTitleLabel.stringTag = @"subTitleLabel";
            [cell addSubview:subTitleLabel];
            
            // 创建button
            UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
            leftButton.backgroundColor = [UIColor clearColor];
            [leftButton addTarget:self action:@selector(chatCustomerClick) forControlEvents:UIControlEventTouchUpInside];
            [leftButton setTitle:@"联系客服" forState:UIControlStateNormal];
            [leftButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
            leftButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            leftButton.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
            leftButton.layer.borderWidth = .5f;
            leftButton.layer.cornerRadius = MMHFloat(5.);
            leftButton.stringTag = @"leftButton";
            [cell addSubview:leftButton];
            
            // 创建右侧按钮
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightButton.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
            [rightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            [rightButton addTarget:self action:@selector(refundAgainClick) forControlEvents:UIControlEventTouchUpInside];
            rightButton.stringTag = @"rightButton";
            rightButton.layer.cornerRadius = MMHFloat(5.);
            [rightButton setTitle:@"重新申请退款" forState:UIControlStateNormal];
            rightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [cell addSubview:rightButton];
            
        }
        // 赋值
        UILabel *titlelabel = (UILabel *)[cell viewWithStringTag:@"titleLabel"];
        titlelabel.text = @"退款关闭";
        CGSize titleLabelContentofSize = [titlelabel.text sizeWithCalcFont:titlelabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
        titlelabel.size_height = titleLabelContentofSize.height;
        
        // subtitle
        UILabel *subTitleLabel = (UILabel *)[cell viewWithStringTag:@"subTitleLabel"];
        if (self.refundApplyState == MMHRefundApplyStateCloseNormol){
            subTitleLabel.textAlignment = NSTextAlignmentCenter;
            subTitleLabel.text = @"您的退款退货申请已经关闭";
        } else if (self.refundApplyState == MMHRefundApplyStateCloseNotPass){
            subTitleLabel.textAlignment = NSTextAlignmentLeft;
            subTitleLabel.text = @"您的退款退货申请没有通过妈妈好平台的审核，超时退款关闭";
        }
        CGSize subtitleLabelContentOfSize = [subTitleLabel.text sizeWithCalcFont:subTitleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(20), CGFLOAT_MAX)];
        subTitleLabel.frame = CGRectMake(MMHFloat(20), CGRectGetMaxY(titlelabel.frame) + MMHFloat(16), kScreenBounds.size.width - 2 * MMHFloat(20), subtitleLabelContentOfSize.height);
        
        // leftButton
        UIButton *leftButton = (UIButton *)[cell viewWithStringTag:@"leftButton"];
        leftButton.frame = CGRectMake(MMHFloat(16), CGRectGetMaxY(subTitleLabel.frame) + MMHFloat(22), (kScreenBounds.size.width - 2 * MMHFloat(16) - MMHFloat(15))/2., MMHFloat(44));
        
        // rightButton
        UIButton *rightButton = (UIButton *)[cell viewWithStringTag:@"rightButton"];
        rightButton.frame = CGRectMake(CGRectGetMaxX(leftButton.frame) + MMHFloat(15), leftButton.frame.origin.y, (kScreenBounds.size.width - 2 * MMHFloat(16) - MMHFloat(15))/2., leftButton.bounds.size.height);
        
        return cell;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款完成状态"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRefundStateRowOne = @"cellIdentifyWithRefundStateRowOne";
            UITableViewCell *cellWithRefundStateRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRefundStateRowOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithRefundStateRowOne){
                cellWithRefundStateRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRefundStateRowOne];
                cellWithRefundStateRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithRefundStateRowOne.backgroundView = bgImageView;
                
                [self createCustomerViewWithSubView:cellWithRefundStateRowOne cellHeight:cellHeight signImage:@"icon_returns_check" title:@"退款状态"];
            }
            UIImageView *bgImageView = (UIImageView *)cellWithRefundStateRowOne.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithRefundStateRowOne;
            
        } else {
            static NSString *cellIdentifyWithRefundStateWithRowTwo = @"cellIdentifyWithRefundStateWithRowTwo";
            UITableViewCell *cellWithRefundStateWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRefundStateWithRowTwo];
            if (!cellWithRefundStateWithRowTwo){
                cellWithRefundStateWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRefundStateWithRowTwo];
                cellWithRefundStateWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                // bgImageView
                UIImageView *bgImageView = [[UIImageView alloc]init];
                cellWithRefundStateWithRowTwo.backgroundView = bgImageView;
                
                // 创建imageView
                UIImageView *signImageView = [[UIImageView alloc]init];
                signImageView.stringTag = @"signImageView";
                signImageView.image = [UIImage imageNamed:@"icon_returns_complete"];
                signImageView.backgroundColor = [UIColor clearColor];
                signImageView.frame = CGRectMake(MMHFloat(150), MMHFloat(17), MMHFloat(33), MMHFloat(33));
                [cellWithRefundStateWithRowTwo addSubview:signImageView];
                
                // 创建label
                UILabel *auditResultsLabel = [[UILabel alloc]init];
                auditResultsLabel.backgroundColor = [UIColor clearColor];
                auditResultsLabel.textAlignment = NSTextAlignmentLeft;
                auditResultsLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
                auditResultsLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
                auditResultsLabel.stringTag = @"auditResultsLabel";
                [cellWithRefundStateWithRowTwo addSubview:auditResultsLabel];
                
                // 创建 contentLabel
                UILabel *contentLabel = [[UILabel alloc]init];
                contentLabel.frame = CGRectMake(MMHFloat(25), CGRectGetMaxY(signImageView.frame) + MMHFloat(17), kScreenBounds.size.width - 2 * (MMHFloat(15)+ 10), 30);
                contentLabel.textAlignment = NSTextAlignmentLeft;
                contentLabel.numberOfLines = 0;
                contentLabel.stringTag = @"contentLabel";
                contentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [cellWithRefundStateWithRowTwo addSubview:contentLabel];
            }
            // 赋值
            UILabel *auditResultsLabel = (UILabel *)[cellWithRefundStateWithRowTwo viewWithStringTag:@"auditResultsLabel"];
            auditResultsLabel.text = @"退款完成";
            auditResultsLabel.textAlignment = NSTextAlignmentLeft;
            
            UILabel *contentLabel = (UILabel *)[cellWithRefundStateWithRowTwo viewWithStringTag:@"contentLabel"];
            contentLabel.text = @"您的退款资金即将打入您的支付账户";
            contentLabel.textAlignment = NSTextAlignmentCenter;
            CGSize contentOfSize = [contentLabel.text sizeWithCalcFont:contentLabel.font constrainedToSize:CGSizeMake(contentLabel.width, CGFLOAT_MAX)];
            
            UIImageView *signImageView = (UIImageView *)[cellWithRefundStateWithRowTwo viewWithStringTag:@"signImageView"];
            CGSize titleOfSize = [auditResultsLabel.text sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            CGFloat titleContent = (kScreenBounds.size.width - titleOfSize.width - MMHFloat(12 + 33)) / 2. ;
            signImageView.orgin_x = titleContent;
            
            auditResultsLabel.frame =CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(12), signImageView.frame.origin.y, MMHFloat(200), 30);
            
            contentLabel.frame=CGRectMake(MMHFloat(15) + 10, CGRectGetMaxY(signImageView.frame) + MMHFloat(17), kScreenBounds.size.width - 2 * (MMHFloat(15) + 10)  , contentOfSize.height);

            UIImageView *bgImageView = (UIImageView *)cellWithRefundStateWithRowTwo.backgroundView;
            bgImageView.image = [MMHTool addBackgroundImageViewWithRefundCellWithDataArray:self.dataSourceMutableArray indexPath:indexPath];
            return cellWithRefundStateWithRowTwo;
        }
    }
    else {
        static NSString *celldentifyWithSectionSixRowOne = @"cellIdentifyWithSectionSixRowOne";
        UITableViewCell *cellWithSectionSixRowOne = [tableView dequeueReusableCellWithIdentifier:celldentifyWithSectionSixRowOne];
        if (!cellWithSectionSixRowOne){
            cellWithSectionSixRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:celldentifyWithSectionSixRowOne];
            cellWithSectionSixRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            
        }
        return cellWithSectionSixRowOne;
    }
    return nil;
}


#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息填写"]){
        if (indexPath.row == 1){
            UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            UITextField *expressTextTypeField = (UITextField *)[cell viewWithStringTag:@"expressTextTypeField"];
            MMHRefundExpressChooseViewController *refundExpressChooseViewController = [[MMHRefundExpressChooseViewController alloc]init];
            __weak typeof(self)weakSelf = self;
            refundExpressChooseViewController.refundExpressChooseBlock = ^(MMHQueryLogisticsSingleModel *expressSingleModel){
                weakSelf.expressSingleModel = expressSingleModel;
                expressTextTypeField.text = expressSingleModel.platform.length?expressSingleModel.platform:@"未获取到快递信息";
            };
            UINavigationController *tempNav = [[UINavigationController alloc]initWithRootViewController:refundExpressChooseViewController];
            [self.navigationController presentViewController:tempNav animated:YES completion:nil];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"客服电话"]){
        [self clauseClick];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"取件人信息"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"联系方式"]){
            UIActionSheet *sheet = [UIActionSheet actionSheetWithTitle:@"是否拨打电话致取件人?" buttonTitles:@[@"取消",@"确定"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    if (self.refundDetailModel.recipient.recipientPhone.length){
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.refundDetailModel.recipient.recipientPhone]]];
                    } else {
                        [[UIAlertView alertViewWithTitle:@"错误" message:@"未获取到取件人号码信息" buttonTitles:@[@"确定"] callback:nil]show];
                    }
                }
            }];
            [sheet showInView:self.view];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息"]){
        if (indexPath.row == 1 ){
            MMHExpressDetailViewController *expressDetailViewController = [[MMHExpressDetailViewController alloc]init];
            UINavigationController *refundExpressDetailNav = [[UINavigationController alloc]initWithRootViewController:expressDetailViewController];
//            expressDetailViewController.expressDetailUsing = MMHExpressDetailUsingRefund;
            expressDetailViewController.transferExpressModel = self.expressModel;
            [self.navigationController presentViewController:refundExpressDetailNav animated:YES completion:nil];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货方式选择"]){
        if (indexPath.row != 0){
            NSString *cellContent = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            if (![self.refundWithSelfTypeMutableArray containsObject:cellContent]){
                if(self.refundWithSelfTypeMutableArray.count){
                    [self.refundWithSelfTypeMutableArray removeAllObjects];
                }
                [self.refundWithSelfTypeMutableArray addObject:cellContent];
                
                NSInteger expressAddressInfo = [self cellIndexPathSectionWithcellData:@"退货方式选择"];
                NSMutableIndexSet *expressAddressInfoSet = [[NSMutableIndexSet alloc]initWithIndex:(expressAddressInfo)];
                [self.refundTableView reloadSections:expressAddressInfoSet withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款状态"]){
        return MMHFloat(100);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"审核结果"]) {
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else if (indexPath.row == 1){
            NSString *subString = @"";
            if ((self.refundType == MMHRefundTypeShop )|| ((self.refundType == MMHRefundTypeSelf) && self.refundShopType)){
                // 【门店配送】
                if (self.refundTypeWithHome == 1){                  // 退货
                    if (self.refundPayType == MMHRefundPayTypeOnLine){                  // [线上]
                        subString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                    } else if (self.refundPayType == MMHRefundPayTypeCashOnDelivery){   // [线下]
                        subString = @"您购买的时候是货到付款，门店人员上门取件的时候会当场退还支付款，请注意查收";
                    }
                } else {                                            // 退款
                    subString = @"我们将会在7日内将货款退到您支付宝账号，请注意查收!";
                }
            } else if (self.refundType == MMHRefundTypeExpress){
                subString = @"请您退货并填写物流信息，请在7天内填写，逾期将自动取消退货";
            }
            
            return [MMHRefundExamineCell cellHeightWithchannelString:self.refundDetailModel.shopName arriveTimeString:self.refundDetailModel.arriveTime.length?self.refundDetailModel.arriveTime:@"" andSubTitleString:subString refundApplyState:self.refundApplyState refundApplyPayType:self.refundPayType refundType:self.refundType refundShopType:self.refundShopType refundState:self.refundState refundTypeWithHome:self.refundTypeWithHome];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"寄件地址信息"]){
        if (indexPath.row == 0){
            return  MMHFloat(44);
        } else {
            NSString *fixedString;
            NSString *dymicString;
            fixedString = [[self.dataSourceMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            if ([fixedString isEqualToString:@"收件人:"]){
                dymicString = self.refundDetailModel.deliveryAddr.shopName.length?self.refundDetailModel.deliveryAddr.shopName:@"未获取到寄件人信息";
            } else if ([fixedString isEqualToString:@"电话:"]){
                dymicString = self.refundDetailModel.deliveryAddr.telephone.length?self.refundDetailModel.deliveryAddr.telephone:@"未获取到寄件信息电话";
            } else if ([fixedString isEqualToString:@"邮编:"]){
                dymicString = self.refundDetailModel.deliveryAddr.zipCode.length?self.refundDetailModel.deliveryAddr.zipCode:@"未获取到寄件邮编信息";
            } else if ([fixedString isEqualToString:@"地址:"]){
                dymicString = self.refundDetailModel.deliveryAddr.addr.length?self.refundDetailModel.deliveryAddr.addr:@"未获取到寄件地址信息";
            } else if ([fixedString isEqualToString:@"备注:"]){
                dymicString = self.refundDetailModel.deliveryAddr.remark.length?self.refundDetailModel.deliveryAddr.remark:@"未获取到寄件备注信息";
            }
            
            CGFloat cellHeigth = [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小正文"]];
            CGSize contentOfSize = [fixedString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeigth)];
            
            CGSize dymicOfSize = [dymicString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenWidth - 2 * (10 + MMHFloat(15)) - contentOfSize.width - MMHFloat(15), CGFLOAT_MAX)];
            return dymicOfSize.height + MMHFloat(10) * 2;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息填写"]){
        if (indexPath.row == 1){
            return MMHFloat(10 + 55);
        } else if (indexPath.row == 2){
            return MMHFloat(10 + 55);
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货地址"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else {
            return [MMHOrderShopInfoTableViewCell cellHeightWith:self.refundDetailModel.shopInfo andUsingClass:MMHShopInfoCellUsingClassRefund];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"申请审核"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else {
            CGFloat cellheight = 0;
            NSString *rangeString = @"";
            if ((self.refundType == MMHRefundTypeExpress) || ((self.refundShopType == MMHRefundShopTypeExpress) && (self.refundType == MMHRefundTypeSelf))){           // 物流
                if (self.refundState == MMHRefundStateInspection){          // 验货
                    rangeString = [NSString stringWithFormat:@"平台还未收到货，一旦收到货，我们会在1个小时内完成验货，请耐心等待"];
                } else {
                    rangeString = [NSString stringWithFormat:@"我们会在1个小时内完成申请审核，请耐心等待"];
                }
            } else {
                rangeString = [NSString stringWithFormat:@"我们会在1个小时内完成申请审核，请耐心等待"];
            }
            
            
            CGSize contentOfSize = [rangeString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(10 + 15) - MMHFloat(22) - MMHFloat(15), CGFLOAT_MAX)];
            
            if (self.refundState == MMHRefundStateInspection){
                cellheight = MMHFloat(15 + 10) + contentOfSize.height;
            } else {
                cellheight = MMHFloat(15 + 12 + 35 + 10) + contentOfSize.height;
            }
            return cellheight;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else if (indexPath.row == 1){
            return [MMHRefundInfoWithExpressCell cellHeightWithModel:self.expressModel];
        } else if (indexPath.row == 2){
            return MMHFloat(44);
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"原因"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else if (indexPath.row == 1){
            CGSize contentOfSize = [self.refundDetailModel.cause sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(25), CGFLOAT_MAX)];
            return contentOfSize.height + 2 * MMHFloat(12);
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"取件人信息"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else {
            if (indexPath.row == [[self.dataSourceMutableArray objectAtIndex:indexPath.section] count] - 1){
                return MMHFloat(44);
            } else {
                return MMHFloat(35);
            }
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款信息"]){
        if (indexPath.section == 0){
            return MMHFloat(44);
        } else {
            return MMHFloat(35);
        }
    }  else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款关闭"]){
        CGFloat cellheight = MMHFloat(26 + 16 + 22 + 25 + 44);
        NSString *customString = @"";
        if (self.refundApplyState == MMHRefundApplyStateCloseNormol){              // 正常关闭
            customString = @"您的退款退货申请已经关闭";
        } else if (self.refundApplyState == MMHRefundApplyStateCloseNotPass){      // 未通过关闭
            customString = @"您的退款退货申请没有通过妈妈好平台的审核，超时退款关闭";
        }
        cellheight += [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"标题"]];
        CGSize subtitleLabelContentOfSize = [customString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(16), CGFLOAT_MAX)];
        cellheight += subtitleLabelContentOfSize.height;
        return cellheight;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款完成状态"]){
        if (indexPath.row == 0){
            return MMHFloat(44);
        } else {
            NSString *contentOfString = @"您的退款资金即将打入您的支付账户";
            CGSize contentOfSize = [contentOfString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (MMHFloat(15)+ 10), CGFLOAT_MAX)];
            return  MMHFloat(17) + MMHFloat(33) + MMHFloat(17) + contentOfSize.height + MMHFloat(17);
        }
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return MMHFloat(20);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClick)];
    [headerView addGestureRecognizer:tapGesture];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.refundTableView) {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"审核结果"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"寄件地址信息"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货方式"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货地址"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款信息"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"申请审核"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"原因"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"取件人信息"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货方式选择"]){
            if (indexPath.row == 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
            }
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退货信息填写"]){
            SeparatorType separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退款完成状态"]){
            SeparatorType separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"examineResult"];
        }
    }
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField.returnKeyType == UIReturnKeyNext){       //显示下一个
        
    }
    else if(textField.returnKeyType == UIReturnKeyDone){
        NSInteger cellWithRefundInfo = [self cellIndexPathSectionWithcellData:@"退货信息填写"];
        NSIndexPath *indexPathWithRefundInfo = [NSIndexPath indexPathForRow:2 inSection:cellWithRefundInfo];
        UITableViewCell *cellWithReundInfoCell = [self.refundTableView cellForRowAtIndexPath:indexPathWithRefundInfo];
        UITextField *expressNumbertextField = (UITextField *)[cellWithReundInfoCell viewWithStringTag:@"expressTextNumberField"];
        self.waybillNumber = expressNumbertextField.text;
        if (expressNumbertextField.isFirstResponder == YES){
            [expressNumbertextField resignFirstResponder];
        }
    }
    return YES;
}


#pragma mark - actionClick
-(void)tapGestureClick{
    NSInteger cellWithRefundInfo = [self cellIndexPathSectionWithcellData:@"退货信息填写"];
    NSIndexPath *indexPathWithRefundInfo = [NSIndexPath indexPathForRow:2 inSection:cellWithRefundInfo];
    UITableViewCell *cellWithReundInfoCell = [self.refundTableView cellForRowAtIndexPath:indexPathWithRefundInfo];
    UITextField *expressNumbertextField = (UITextField *)[cellWithReundInfoCell viewWithStringTag:@"expressTextNumberField"];
    self.waybillNumber = expressNumbertextField.text;
    if (expressNumbertextField.isFirstResponder == YES){
        [expressNumbertextField resignFirstResponder];
    }
}

-(void)refundAgainClick{                    // 重新退款
    if (self.refundDetailPageUsingType == MMHRefundDetailPageUsingTypeNormal){
        __weak typeof(self)weakSelf = self;
        for (UIViewController *viewController in weakSelf.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[MMHRefundInfoViewController class]]) {
                [weakSelf.navigationController popToViewController:viewController animated:NO];
            }
        }
    } else if (self.refundDetailPageUsingType == MMHRefundDetailPageUsingTypeRefundDetail){
        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestWithApplyRefund];
    }
}

-(void)clauseClick{                  // 拨打号码
    __weak MMHRefundDetailViewController *weakViewController = self;
    [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
}

-(void)cancelRefundClick{           // 取消退货申请
    
    NSString *messageString;
    if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){
        messageString = @"您是否要取消申请退款退货";
    } else {
        messageString = @"您是否要取消申请退款";
    }
    [[UIAlertView alertViewWithTitle:nil message:messageString buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1){
            __weak typeof(self)weakSelf = self;
            [weakSelf sendRequestWithCancelRefundWithRefundLuneId:weakSelf.refundDetailModel.refundLineId];
        }
    }]show];
}

-(void)updateExpressClick{          // 修改退货物流
    __weak typeof(self)weakSelf = self;
    MMHRefundExpressUpdateViewController *refundExpressUpdateViewController = [[MMHRefundExpressUpdateViewController alloc]init];
    refundExpressUpdateViewController.transferRefundLineId = [NSString stringWithFormat:@"%li",(long)weakSelf.refundDetailModel.refundLineId];
    refundExpressUpdateViewController.updateExpressBlock = ^(NSString *expressName,NSString *expressCode,NSString *expressNumber){
        [weakSelf sendRequestWithGetLogisticsTrackingWaybillNumber:expressNumber platformCode:expressCode];
    };
    UINavigationController *refundExpressNav = [[UINavigationController alloc]initWithRootViewController:refundExpressUpdateViewController];
    [self.navigationController presentViewController:refundExpressNav animated:YES completion:nil];
    
}

-(void)chatCustomerClick{           // 联系客服
    [self startChattingWithContext:nil];
}

-(void)receivablesClick:(UIButton *)sender{            // 确认收款
    UIButton *receivablesButton = (UIButton *)sender;
    if ([receivablesButton.stringTag isEqualToString:@"提交物流信息"]){
        __weak typeof(self)weakSelf = self;
        [weakSelf JudgeWithCommitLogisticsWithFloatButton:receivablesButton];
    } else if ([receivablesButton.stringTag isEqualToString:@"确认收款"]){
        [[UIAlertView alertViewWithTitle:nil message:@"亲爱的麻麻，您确定收到退款了吗？\n一旦确定，结果将不会更改" buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1){
                __weak typeof(self)weakSelf = self;
                [weakSelf sendRequestWithConfirmReceivablesWithRefundLineId:[NSString stringWithFormat:@"%li",(long)weakSelf.refundDetailModel.refundLineId]];
            }
        }]show];
    } else if ([receivablesButton.stringTag isEqualToString:@"退货方式确认"]){
        if (self.refundWithSelfTypeMutableArray.count){
            [[UIAlertView alertViewWithTitle:nil message:@"确认退款方式" buttonTitles:@[@"取消",@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1){
                    [self cellMoveWithSelfGetWithStringTag:[self.refundWithSelfTypeMutableArray lastObject]];
                }
            }]show];
        } else {
            [self.view showTips:@"请选择退货方式"];
        }
    } else if ([receivablesButton.stringTag isEqualToString:@"确认收货"]){
        NSLog(@"确认收货");
    }
}

#pragma mark - cellRemove
// 平台审核-> 平台验货
-(void)cellMoveWithExpressExamineToInspection{
    self.refundState = MMHRefundStateInspection;                       // 修改审核状态
    NSInteger shenhejieguo = [self cellIndexPathSectionWithcellData:@"审核结果"];
    NSInteger tuihuoxinxitianxie = [self cellIndexPathSectionWithcellData:@"退货信息填写"];
    if ((shenhejieguo != 0)&&(tuihuoxinxitianxie != 0)){
        // 移除
        [self.dataSourceMutableArray removeObjectAtIndex:shenhejieguo];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:shenhejieguo];
        [self.refundTableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        // 添加
        NSInteger pingtaiyanhuo = [self cellIndexPathSectionWithcellData:@"退款状态"];
        [self.dataSourceMutableArray insertObject:@[@"申请审核",@"申请审核内容"] atIndex:(pingtaiyanhuo + 1)];
        NSMutableIndexSet *pingtaiyanhuoSet = [[NSMutableIndexSet alloc]initWithIndex:(pingtaiyanhuo + 1)];
        [self.refundTableView insertSections:pingtaiyanhuoSet withRowAnimation:UITableViewRowAnimationRight];
        
        // [2]
        // 移除
        [self.dataSourceMutableArray removeObjectAtIndex:tuihuoxinxitianxie];
        NSIndexSet *tuihuoxinxitianxieIndexSet = [NSIndexSet indexSetWithIndex:tuihuoxinxitianxie];
        [self.refundTableView deleteSections:tuihuoxinxitianxieIndexSet withRowAnimation:UITableViewRowAnimationLeft];
        // 添加
        NSInteger expressAddressInfo = [self cellIndexPathSectionWithcellData:@"寄件地址信息"];
        [self.dataSourceMutableArray insertObject:@[@"退货信息",@"退货信息内容",@"更改退货物流"] atIndex:(expressAddressInfo + 1)];
        NSMutableIndexSet *expressAddressInfoSet = [[NSMutableIndexSet alloc]initWithIndex:(expressAddressInfo + 1)];
        [self.refundTableView insertSections:expressAddressInfoSet withRowAnimation:UITableViewRowAnimationRight];
    }
    [self.customerSlider setSelectedIndex:2];
    
    // 获取物流信息
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetLogisticsTrackingWaybillNumber:weakSelf.waybillNumber platformCode:weakSelf.expressSingleModel.platformCode];
}

// 自提 - 退货方式 -
-(void)cellMoveWithSelfGetWithStringTag:(NSString *)stringTag{
    if ([stringTag isEqualToString:@"门店上门取件退货"]){
        // 1. 刷新审核结果
        NSInteger shenhejieguo = [self cellIndexPathSectionWithcellData:@"审核结果"];
        NSIndexSet *shenhejieguoSet = [NSIndexSet indexSetWithIndex:shenhejieguo];
        [self.refundTableView reloadSections:shenhejieguoSet withRowAnimation:UITableViewRowAnimationNone];
        
        // 2. 移除退货方式
        NSInteger refundTypeChoose = [self cellIndexPathSectionWithcellData:@"退货方式选择"];
        [self.dataSourceMutableArray removeObjectAtIndex:refundTypeChoose];
        NSIndexSet *refundTypeChooseSet = [NSIndexSet indexSetWithIndex:refundTypeChoose];
        [self.refundTableView deleteSections:refundTypeChooseSet withRowAnimation:UITableViewRowAnimationLeft];
        
        // 3. 增加取件人信息
        [self.dataSourceMutableArray insertObject:@[@"取件人信息",@"门店人员",@"所属门店",@"联系方式",@"上门取件时间"] atIndex:(shenhejieguo+1)];
        NSMutableIndexSet *qujianrenSet = [[NSMutableIndexSet alloc]initWithIndex:(shenhejieguo + 1)];
        [self.refundTableView insertSections:qujianrenSet withRowAnimation:UITableViewRowAnimationRight];
        
        // 4.增加退款信息
        NSInteger refundInfo = [self cellIndexPathSectionWithcellData:@"取件人信息"];
        if (self.refundPayType == MMHRefundPayTypeOnLine){                      // 线上支付
            [self.dataSourceMutableArray insertObject:@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"] atIndex:(refundInfo + 1)];
        } else if (self.refundPayType == MMHRefundPayTypeCashOnDelivery){
            [self.dataSourceMutableArray insertObject:@[@"退款信息",@"退款金额",@"退款方式"] atIndex:(refundInfo + 1)];
        }
        NSMutableIndexSet *refundInfoSet = [[NSMutableIndexSet alloc]initWithIndex:(refundInfo + 1)];
        [self.refundTableView insertSections:refundInfoSet withRowAnimation:UITableViewRowAnimationRight];
        
        // 5. 修改浮层右侧按钮
        [self.floatRightButton setTitle:@"确认收款" forState:UIControlStateNormal];
        self.floatRightButton.stringTag = @"确认收款";
        
    } else if ([stringTag isEqualToString:@"自己送货到门店退货"]){
        // 1. 刷新审核结果
        NSInteger shenhejieguo = [self cellIndexPathSectionWithcellData:@"审核结果"];
        NSIndexSet *shenhejieguoSet = [NSIndexSet indexSetWithIndex:shenhejieguo];
        [self.refundTableView reloadSections:shenhejieguoSet withRowAnimation:UITableViewRowAnimationNone];
        
        // 2. 移除退货方式
        NSInteger refundTypeChoose = [self cellIndexPathSectionWithcellData:@"退货方式选择"];
        [self.dataSourceMutableArray removeObjectAtIndex:refundTypeChoose];
        NSIndexSet *refundTypeChooseSet = [NSIndexSet indexSetWithIndex:refundTypeChoose];
        [self.refundTableView deleteSections:refundTypeChooseSet withRowAnimation:UITableViewRowAnimationLeft];
        
        // 3. 增加退货地址
        [self.dataSourceMutableArray insertObject:@[@"退货地址",@"退货地址详细"] atIndex:(shenhejieguo+1)];
        NSMutableIndexSet *tuihuodizhiSet = [[NSMutableIndexSet alloc]initWithIndex:(shenhejieguo + 1)];
        [self.refundTableView insertSections:tuihuodizhiSet withRowAnimation:UITableViewRowAnimationRight];
        
        // 4. 增加退款信息
        NSInteger refundAddress = [self cellIndexPathSectionWithcellData:@"退货地址"];
        [self.dataSourceMutableArray insertObject:@[@"退款信息",@"退款金额",@"退款方式",@"到款日期"] atIndex:(refundAddress+1)];
        NSMutableIndexSet *refundInfoSet = [[NSMutableIndexSet alloc]initWithIndex:(refundAddress + 1)];
        [self.refundTableView insertSections:refundInfoSet withRowAnimation:UITableViewRowAnimationRight];
        
        // 5. 修改浮层右侧按钮
        [self.floatRightButton setTitle:@"确认收款" forState:UIControlStateNormal];
        self.floatRightButton.stringTag = @"确认收款";
    } else if ([stringTag isEqualToString:@"快递上门取件退货"]){
        //        // 1. 移除dataSource
        //        [self.dataSourceMutableArray removeAllObjects];
        //        // 2. 添加数据
        //        NSArray *tempArr = @[@[@"退款状态"],@[@"审核结果",@"审核结果信息"],@[@"寄件地址信息",@"收件人",@"电话",@"邮编",@"地址",@"备注"],@[@"退货信息填写",@"退货物流",@"物流单号"],@[@"客服电话"]];
        //        [self.dataSourceMutableArray addObjectsFromArray:tempArr];
        //        // 3.修改状态信息
        //        self.refundStateArray = @[@"申请退货",@"平台审核",@"平台验货",@"退款完成"];
        //        // 4. reloadData
        //        [self.refundTableView reloadData];
        //        // 5.scroll
        //        [self.refundTableView scrollsToTop];
        //        // 6. 修改审核状态
        //        [self.customerSlider setSelectedIndex:2];
        
        
        // 2. 移除退货方式
        NSInteger refundTypeChoose = [self cellIndexPathSectionWithcellData:@"退款状态"];
        [self.dataSourceMutableArray removeObjectAtIndex:refundTypeChoose];
        NSIndexSet *refundTypeChooseSet = [NSIndexSet indexSetWithIndex:refundTypeChoose];
        [self.refundTableView deleteSections:refundTypeChooseSet withRowAnimation:UITableViewRowAnimationLeft];
        
        self.refundStateArray = @[@"申请退货",@"平台审核",@"平台验货",@"退款完成"];
        [self.dataSourceMutableArray insertObject:@[@"退款状态"] atIndex:0];
        NSMutableIndexSet *tuihuodizhiSet = [[NSMutableIndexSet alloc]initWithIndex:0];
        [self.refundTableView insertSections:tuihuodizhiSet withRowAnimation:UITableViewRowAnimationRight];
        
        NSIndexPath *index = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.refundTableView scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
    }
}


//         1. 提货状态
//        self.refundType = MMHRefundTypeSelf;                     // 配送方式
//        self.refundState = MMHRefundStateApply;                     // 进程状态
//        self.refundApplyState = MMHRefundApplyStateSuccess;         // 审核结果
//        self.refundTypeWithHome = MMHRefundTypeWithHomeMoney;
//        self.refundPayType = MMHRefundPayTypeOnLine;                // 支付方式
//        self.refundIsGoodsReceipt = MMHRefundIsGoodsReceiptYes;     // 是否收货
//        self.refundShopType = MMHRefundShopTypeExpress;           // 自提退货方式

-(void)analyticalWithDataStatus{
    if (self.refundDetailModel){
        if (self.refundDetailModel.status == 1){                            // 平台审核中
            self.refundState = MMHRefundStateApply;
            self.refundApplyState = MMHRefundApplyStateNormol;
        } else if (self.refundDetailModel.status == 2){                     // 平台审核通过
            self.refundState = MMHRefundStateApply;
            self.refundApplyState = MMHRefundApplyStateSuccess;
            if (self.refundDetailModel.deliveryWay == 1 ){
                self.refundIsGoodsReceipt = MMHRefundIsGoodsReceiptNo;
            }
        } else if (self.refundDetailModel.status == 3){                     // 平台收货
            self.refundApplyState = MMHRefundApplyStateSuccess;
            self.refundState = MMHRefundStateInspection;
            self.refundIsGoodsReceipt = MMHRefundIsGoodsReceiptYes;
        } else if (self.refundDetailModel.status == 4){                     // 退款完成
            self.refundState = MMHRefundStateSuccess;
            self.refundApplyState = MMHRefundApplyStateSuccess;
        } else if (self.refundDetailModel.status == 5){                     // 审核未通过
            self.refundState = MMHRefundStateApply;
            self.refundApplyState = MMHRefundApplyStateFail;
        } else if (self.refundDetailModel.status == 6){                     // 取消申请
            self.refundApplyState = MMHRefundApplyStateCloseNormol;
        } else if (self.refundDetailModel.status == 7){                     // 待平台收货
            self.refundApplyState = MMHRefundApplyStateSuccess;
            self.refundState = MMHRefundStateInspection;
            self.refundIsGoodsReceipt = MMHRefundIsGoodsReceiptNo;
        } else if (self.refundDetailModel.status == 8){
            self.refundApplyState = MMHRefundApplyStateCloseNotPass;
        }
        
        //   【配送方式】
        if (self.refundDetailModel.deliveryWay){
            if (self.refundDetailModel.deliveryWay == 1){                       // 门店配送
                self.refundType = MMHRefundTypeShop;
            } else if (self.refundDetailModel.deliveryWay == 2){                // 物流配送
                self.refundType = MMHRefundTypeSelf;
            } else if (self.refundDetailModel.deliveryWay == 3){                // 自提
                self.refundType = MMHRefundTypeExpress;
            }
        }
        
        // 【退货类型】
        if (self.refundDetailModel.refundTypeWithHome){
            self.refundTypeWithHome = self.refundDetailModel.refundTypeWithHome;
        }
        
        // 【线上还是线下】
        if (self.refundDetailModel.dealingType){
            if (self.refundDetailModel.dealingType == 1){               // 1. 线下 0 线上
                self.refundPayType = MMHRefundPayTypeOnLine;
            } else if (self.refundDetailModel.dealingType == 2){
                self.refundPayType = MMHRefundPayTypeCashOnDelivery;
            }
        }
        
        // 【是否是上门自提】
        if(self.refundDetailModel.refundWay){
            self.refundShopType = self.refundDetailModel.refundWay;
        }
    }
}

-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.dataSourceMutableArray.count ; i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = 0;
    for (int i = 0 ; i < self.dataSourceMutableArray.count ; i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

-(BOOL)cellIndexIsHasData:(NSString *)string{
    BOOL isHas = NO;
    for (int i = 0 ; i <self.dataSourceMutableArray.count;i++){
        NSArray *dataTempArr = [self.dataSourceMutableArray objectAtIndex:i];
        for (int j = 0 ;j <dataTempArr.count;j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                isHas = YES;
                break;
            }
        }
    }
    return isHas;
}

#pragma mark 创建浮层view
-(void)createFloatView{
    UIView *floatView = [[UIView alloc]init];
    floatView.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    floatView.frame = CGRectMake(0, self.view.bounds.size.height  - MMHFloat(48), kScreenBounds.size.width, MMHFloat(48));
    [self.view addSubview:floatView];
    
    if (self.refundState == MMHRefundStateSuccess){
        floatView.hidden = YES;
    }
    
    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, 0, floatView.bounds.size.width, .5f);
    [floatView addSubview:lineView];
    
    // 创建联系客服
    UIImageView *chatImageView = [[UIImageView alloc]init];
    chatImageView.backgroundColor = [UIColor clearColor];
    chatImageView.image = [UIImage imageNamed:@"order_free_chat_typing"];
    chatImageView.frame = CGRectMake(MMHFloat(25), (floatView.bounds.size.height - MMHFloat(15)) / 2, MMHFloat(16), MMHFloat(15));
    [floatView addSubview:chatImageView];
    
    // 创建联系客服
    UILabel *chatLabel = [[UILabel alloc]init];
    chatLabel.backgroundColor = [UIColor clearColor];
    chatLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    chatLabel.text = @"联系客服";
    chatLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    chatLabel.frame = CGRectMake(CGRectGetMaxX(chatImageView.frame) + MMHFloat(7), 0, 100, MMHFloat(48));
    [floatView addSubview:chatLabel];
    
    // 创建leftbutton
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.backgroundColor = [UIColor clearColor];
    [leftButton addTarget:self action:@selector(chatCustomerClick) forControlEvents:UIControlEventTouchUpInside];
    leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2. - MMHFloat(30), MMHFloat(48));
    [floatView addSubview:leftButton];
    
    // 创建右侧按钮
    self.floatRightButton= [UIButton buttonWithType:UIButtonTypeCustom];
    self.floatRightButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.floatRightButton.layer.cornerRadius = MMHFloat(3.);
    self.floatRightButton.frame = CGRectMake(kScreenBounds.size.width - 100 - 10, MMHFloat(4), 100, MMHFloat(40));
    [self.floatRightButton addTarget:self action:@selector(receivablesClick:) forControlEvents:UIControlEventTouchUpInside];
    NSString *titleString = @"";
    if ((self.refundType == MMHRefundTypeExpress) || ((self.refundShopType == MMHRefundShopTypeExpress) && (self.refundType == MMHRefundTypeSelf))){                       //快递
        if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){
            if (self.refundApplyState == MMHRefundApplyStateNormol){
                self.floatRightButton.hidden = YES;
            } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
                if (self.refundState == MMHRefundStateApply){
                    self.floatRightButton.hidden = NO;
                    titleString = @"提交物流信息";
                    self.floatRightButton.stringTag = @"提交物流信息";
                    if (self.refundDetailModel.waybillNumber.length){
                        self.floatRightButton.hidden = YES;
                    }
                } else if (self.refundState == MMHRefundStateInspection){
                    if (self.refundIsGoodsReceipt == MMHRefundIsGoodsReceiptYes){
                        self.floatRightButton.hidden = YES;
                        self.floatRightButton.stringTag = @"确认收款";
                        titleString = @"确认收款";
                    } else {
                        self.floatRightButton.hidden = YES;
                    }
                }
            } else if (self.refundApplyState == MMHRefundApplyStateFail){
                self.floatRightButton.hidden = YES;
            }
        } else {
            if (self.refundApplyState == MMHRefundApplyStateNormol){
                self.floatRightButton.hidden = YES;
            } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
                floatView.hidden = YES;
                self.refundTableView.size_height = self.view.bounds.size.height;
            } else if (self.refundApplyState == MMHRefundApplyStateFail){
                floatView.hidden = NO;
                titleString = @"申请退货";
                self.floatRightButton.stringTag = @"申请退货";
                self.floatRightButton.hidden = NO;
            }
        }
    }  else if ((self.refundType == MMHRefundTypeShop) || ((self.refundType == MMHRefundTypeSelf) && (self.refundShopType == MMHRefundShopTypeShopGet))){                   // 门店配送
        if (self.refundTypeWithHome == MMHRefundTypeWithHomeProduct){
            if (self.refundApplyState == MMHRefundApplyStateNormol){                // 普通状态
                self.floatRightButton.hidden = YES;
            } else if (self.refundApplyState == MMHRefundApplyStateSuccess){        // 成功
                self.floatRightButton.hidden = YES;
                self.floatRightButton.stringTag = @"确认收款";
                titleString = @"确认收款";
            } else if (self.refundApplyState == MMHRefundApplyStateFail){           // 失败
                self.floatRightButton.hidden = YES;
            }
        } else {
            if (self.refundApplyState == MMHRefundApplyStateNormol){
                self.floatRightButton.hidden = YES;
            } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
                floatView.hidden = YES;
                self.refundTableView.size_height = self.view.bounds.size.height;
            } else if (self.refundApplyState == MMHRefundApplyStateFail){
                floatView.hidden = NO;
                titleString = @"申请退货";
                self.floatRightButton.stringTag = @"申请退货";
                self.floatRightButton.hidden = NO;
            }
        }
    } else if (self.refundType == MMHRefundTypeSelf && self.refundShopType == MMHRefundShopTypeSelf){
        if (self.refundApplyState == MMHRefundApplyStateNormol){
            self.floatRightButton.hidden = YES;
        } else if (self.refundApplyState == MMHRefundApplyStateSuccess){
            floatView.hidden = YES;
            self.refundTableView.size_height = self.view.bounds.size.height;
        } else if (self.refundApplyState == MMHRefundApplyStateFail){
            floatView.hidden = NO;
            titleString = @"申请退货";
            self.floatRightButton.stringTag = @"申请退货";
            self.floatRightButton.hidden = NO;
        }
    }
    if ((self.refundApplyState == MMHRefundApplyStateCloseNotPass) || (self.refundApplyState == MMHRefundApplyStateCloseNormol)){
        floatView.hidden = YES;
        self.refundTableView.scrollEnabled = NO;
    }
    
    [self.floatRightButton setTitle:titleString forState:UIControlStateNormal];
    [self.floatRightButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.floatRightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [floatView addSubview:self.floatRightButton];
}

#pragma mark customeCell
-(void)createCustomerViewWithSubView:(UITableViewCell *)subCell cellHeight:(CGFloat )cellHeight signImage:(NSString *)signImage title:(NSString *)title{
    // bgImageView
    UIImageView *bgImageView = [[UIImageView alloc]init];
    subCell.backgroundView = bgImageView;
    
    // signImageView
    UIImageView *signImageView = [[UIImageView alloc]init];
    signImageView.backgroundColor = [UIColor clearColor];
    signImageView.image = [UIImage imageNamed:signImage];
    signImageView.frame = CGRectMake(MMHFloat(16) + MMHFloat(10), (cellHeight - MMHFloat(14)) / 2., MMHFloat(14), MMHFloat(14));
    signImageView.stringTag = @"signImageView";
    [subCell addSubview:signImageView];
    
    // fixedLabel
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.stringTag = @"fixedLabel";
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    fixedLabel.text = title;
    fixedLabel.frame = CGRectMake(CGRectGetMaxX(signImageView.frame) + MMHFloat(10), 0 ,kScreenBounds.size.width - CGRectGetMaxX(signImageView.frame) - 2 * MMHFloat(10) - 10, cellHeight);
    [subCell addSubview:fixedLabel];
}

#pragma mark - keyBoardNotif

-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.refundTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.refundTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - 接口
#pragma mark 获取退货详情
-(void)sendRequestGetRefundInfoWithOrderNo:(NSString *)orderNo itemId:(NSString *)itemId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelGetRefundInfoWithOrderNo:orderNo andItemId:itemId from:nil succeededHandler:^(MMHRefundDetailModel *refundDetailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.refundDetailModel= refundDetailModel;
        [strongSelf analyticalWithDataStatus];                              // 解析状态
        
        if (strongSelf.dataSourceMutableArray.count){
            [strongSelf.dataSourceMutableArray removeAllObjects];
        }
        [strongSelf.dataSourceMutableArray addObjectsFromArray:[strongSelf refundSetting]];
        
        [strongSelf.refundTableView reloadData];
        if (!strongSelf.floatRightButton){
            [strongSelf createFloatView];
        }
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.refundTableView showTipsWithError:error];
        strongSelf.refundTableView.scrollEnabled = NO;
    }];
}

#pragma mark 申请退款
-(void)sendRequestWithApplyRefund{
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelWithApplyRefundWithOrderNo:weakSelf.transferOrderNo itemId:weakSelf.transferItemId from:nil succeededHandler:^(MMHRefundRequestModel *refundRequestModel) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        MMHRefundInfoViewController *refundInfoController = [[MMHRefundInfoViewController alloc]init];
        refundInfoController.refundInfoPageUsingType = MMHRefundDetailPageUsingTypeRefundDetail;
        refundInfoController.transferItemId = strongSelf.transferItemId;
        refundInfoController.refundTypeWithHome = strongSelf.refundTypeWithHome;
        refundInfoController.transferOrderNo = strongSelf.transferOrderNo;
        refundInfoController.transferRequestModel = refundRequestModel;
        UINavigationController *refundNav = [[UINavigationController alloc]initWithRootViewController:refundInfoController];
        refundInfoController.refundReloadBlock = ^(){                   // 数据请求
            [strongSelf sendRequestGetRefundInfoWithOrderNo:strongSelf.transferOrderNo itemId:strongSelf.transferItemId];
        };
        [strongSelf.navigationController presentViewController:refundNav animated:YES completion:nil];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        [strongSelf.view showTipsWithError:error];
    }];
}

#pragma mark 取消退货申请
-(void)sendRequestWithCancelRefundWithRefundLuneId:(NSInteger)refundLineId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchMOdelCancelApplyWithRefundLineId:[NSString stringWithFormat:@"%li",(long)refundLineId] from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        MMHRefundCloseViewController *refundCloseViewController = [[MMHRefundCloseViewController alloc]init];
        refundCloseViewController.refundCloseType = strongSelf.refundApplyState;
        refundCloseViewController.reApplyBlock = ^(){
            __weak typeof(self)weakSelf = self;
            for (UIViewController *viewController in weakSelf.navigationController.viewControllers) {
                if ([viewController isKindOfClass:[MMHRefundInfoViewController class]]) {
                    [weakSelf.navigationController popToViewController:viewController animated:NO];
                }
            }
        };
        UINavigationController *refundCloseNav = [[UINavigationController alloc]initWithRootViewController:refundCloseViewController];
        [strongSelf.navigationController presentViewController:refundCloseNav animated:YES completion:nil];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 确认收款
-(void)sendRequestWithConfirmReceivablesWithRefundLineId:(NSString *)refundLineId{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelConfirmReceivablesWithRefundLineId:refundLineId from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        //        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:nil message:@"确认收款" buttonTitles:@[@"确定"] callback:nil]show];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
    }];
}

#pragma mark  提交物流信息
-(void)sendRequestWithCommitLogisticsWithRefundLineId:(NSString *)refundLineId waybillNumber:(NSString *)waybillNumber platformCode:(NSString *)platformCode andAnimationButton:(UIButton *)floatButton{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelWithCommitLogisticeWithRefundLineId:refundLineId waybillNumber:waybillNumber platformCode:platformCode from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf cellMoveWithExpressExamineToInspection];
        // 隐藏floatButton
        [UIView animateWithDuration:.7f animations:^{
            floatButton.alpha = 0;
        } completion:^(BOOL finished) {
            floatButton.hidden = YES;
        }];
        
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        [weakSelf.view showTipsWithError:error];
    }];
}

#pragma mark 提交物流信息判断
-(void)JudgeWithCommitLogisticsWithFloatButton:(UIButton *)floatButton{
    // 物流单号
    NSInteger cellWithRefundInfo = [self cellIndexPathSectionWithcellData:@"退货信息填写"];
    NSIndexPath *indexPathWithRefundInfo = [NSIndexPath indexPathForRow:2 inSection:cellWithRefundInfo];
    UITableViewCell *cellWithReundInfoCell = [self.refundTableView cellForRowAtIndexPath:indexPathWithRefundInfo];
    UITextField *expressNumbertextField = (UITextField *)[cellWithReundInfoCell viewWithStringTag:@"expressTextNumberField"];
    
    // 物流信息
    NSIndexPath *indexPathWithRefundName = [NSIndexPath indexPathForRow:1 inSection:cellWithRefundInfo];
    UITableViewCell *cell = [self.refundTableView cellForRowAtIndexPath:indexPathWithRefundName];
    UITextField *expressTextTypeField = (UITextField *)[cell viewWithStringTag:@"expressTextTypeField"];
    
    if (!expressTextTypeField.text.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请选择退货物流" buttonTitles:@[@"确定"] callback:nil]show];
    } else if (!expressNumbertextField.text.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请填写退货物流单号" buttonTitles:@[@"确定"] callback:nil]show];
    } else {
        __weak typeof(self)weakSelf = self;
        weakSelf.waybillNumber = expressNumbertextField.text;
        
        [weakSelf sendRequestWithCommitLogisticsWithRefundLineId:[NSString stringWithFormat:@"%li",(long)weakSelf.refundDetailModel.refundLineId] waybillNumber:weakSelf.waybillNumber platformCode:weakSelf.expressSingleModel.platformCode andAnimationButton:floatButton];
    }
}

#pragma mark 获取物流跟踪信息
-(void)sendRequestWithGetLogisticsTrackingWaybillNumber:(NSString *)waybillNumber platformCode:(NSString *)platformCode{
    __weak typeof(self)weakSelf = self;
    self.expressModel = [[MMHExpressModel alloc]init];
    [[MMHNetworkAdapter sharedAdapter]fetchModelWithLogisticeTrackingWithWaybillNumber:waybillNumber platformCode:platformCode from:nil succeededHandler:^(MMHExpressModel *expressModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.expressModel = expressModel;
        
        // 刷新行
        NSInteger refundExpressInfo = [self cellIndexPathSectionWithcellData:@"退货信息"];
        NSIndexSet *refundExpressInfoSet = [NSIndexSet indexSetWithIndex:refundExpressInfo];
        [self.refundTableView reloadSections:refundExpressInfoSet withRowAnimation:UITableViewRowAnimationNone];
        
    } failedHandler:^(NSError *error) {
        
    }];
    
}



#pragma mark - test
-(void)test:(MMHRefundDetailModel *)refundDetailModel{
    // 退款 退货
    NSString *refundType = refundDetailModel.refundTypeWithHome == 1? @"退货":@"退款";
    NSString *deliveryWay = @"";
    if (refundDetailModel.deliveryWay == 1){
        deliveryWay = @"门店配送";
    } else if (refundDetailModel.deliveryWay == 2){
        deliveryWay = @"物流配送";
    } else if (refundDetailModel.deliveryWay == 3){
        deliveryWay = @"上门自提";
    }
    NSString *refundWay = @"";
    if (refundDetailModel.refundWay == 1){
        refundWay = @"门店取货";
    } else if (refundDetailModel.refundWay == 2){
        refundWay = @"自提";
    } else if (refundDetailModel.refundWay == 3){
        refundWay = @"物流快递取货";
    }
    
    NSString *refundStatue = @"";
    if (refundDetailModel.status == 1){
        refundStatue = @"审核中";
    } else if (refundDetailModel.status == 2){
        refundStatue = @"平台审核成功";
    } else if (refundDetailModel.status == 3){
        refundStatue = @"平台收货，等待买家处理";
    } else if (refundDetailModel.status == 4){
        refundStatue = @"退款完成，同意退款";
    } else if (refundDetailModel.status == 5){
        refundStatue = @"审核部通过，退款关闭";
    } else if (refundDetailModel.status == 6){
        refundStatue = @"取消申请，退款关闭";
    } else if (refundDetailModel.status == 7){
        refundStatue = @"等待平台收货";
    }
    NSString *smart  = [NSString stringWithFormat:@"退款类型refundType:%@\n,退货方式deliveryWay:%@\n,取货方式refundWay:%@\n审核状态status:%@",refundType,deliveryWay,refundWay,refundStatue];
    [[UIAlertView alertViewWithTitle:nil message:smart buttonTitles:@[@"确定"] callback:nil]show];
}
@end
