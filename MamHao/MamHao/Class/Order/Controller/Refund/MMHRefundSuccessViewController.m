//
//  MMHRefundSuccessViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundSuccessViewController.h"

@implementation MMHRefundSuccessViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"退款完成";
}

@end
