//
//  MMHRefundHomeViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundHomeViewController.h"
#import "MMHRefundInfoViewController.h"
@interface MMHRefundHomeViewController()

@end

@implementation MMHRefundHomeViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                             //  pageSetting
    [self createView];                                              //  创建view
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"申请退货";
}

#pragma mark - CustomerView
-(void)createView{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, MMHFloat(90), kScreenBounds.size.width, MMHFloat(130));
    [self.view addSubview:bgView];
    
    UIImageView *leftImageView = [[UIImageView alloc]init];
    leftImageView.backgroundColor = [UIColor clearColor];
    leftImageView.frame = CGRectMake(MMHFloat(30) + ((kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2 - MMHFloat(40))/2., MMHFloat(25), MMHFloat(40), MMHFloat(40));
    leftImageView.image = [UIImage imageNamed:@"icon_returns_refund"];
    [bgView addSubview:leftImageView];
    
    // 创建label
    UILabel *leftTitleLabel = [[UILabel alloc]init];
    leftTitleLabel.backgroundColor = [UIColor clearColor];
    leftTitleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    leftTitleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    leftTitleLabel.frame = CGRectMake(MMHFloat(30), CGRectGetMaxY(leftImageView.frame) + MMHFloat(20),  (kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2, 20);
    leftTitleLabel.text = @"退款";
    leftTitleLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview: leftTitleLabel];
    
    // leftButton
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.stringTag = @"leftButton";
    leftButton.frame = CGRectMake(MMHFloat(30), 0, (kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2, MMHFloat(130));
    leftButton.backgroundColor = [UIColor clearColor];
    [leftButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:leftButton];

    
    UIImageView *rightImageView = [[UIImageView alloc]init];
    rightImageView.backgroundColor = [UIColor clearColor];
    rightImageView.frame = CGRectMake(((kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2 - MMHFloat(40)) / 2. + kScreenBounds.size.width / 2. + MMHFloat(20) , MMHFloat(25), MMHFloat(40), MMHFloat(40));
    rightImageView.image = [UIImage imageNamed:@"icon_returns_return"];
    [bgView addSubview:rightImageView];
    
    // 创建label
    UILabel *rightTitleLabel = [[UILabel alloc]init];
    rightTitleLabel.backgroundColor = [UIColor clearColor];
    rightTitleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    rightTitleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    rightTitleLabel.frame = CGRectMake(MMHFloat(20) + kScreenBounds.size.width / 2., CGRectGetMaxY(leftImageView.frame) + MMHFloat(20),  (kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2, 20);
    rightTitleLabel.text = @"退货退款";
    rightTitleLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview: rightTitleLabel];
    
    // leftButton
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.stringTag = @"rightButton";
    rightButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(30) - (kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2, 0, (kScreenBounds.size.width - 2 * (MMHFloat(30 + 20))) / 2, MMHFloat(130));
    rightButton.backgroundColor = [UIColor clearColor];
    [rightButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:rightButton];

    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(kScreenBounds.size.width / 2. - .5f, 0, .5f, MMHFloat(130));
    [bgView addSubview:lineView];
    
}

#pragma mark customerButton

#pragma mark - actionClick
-(void)buttonClicked:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    if ([button.stringTag isEqualToString:@"leftButton"]){
        NSLog(@"退款");
        MMHRefundInfoViewController *refundInfoViewController = [[MMHRefundInfoViewController alloc]init];
//        refundInfoViewController.transferPrice = self.transferPrice;
        refundInfoViewController.refundTypeWithHome = MMHRefundTypeWithHomeMoney;
        refundInfoViewController.transferOrderNo = self.transferOrderNo;
        [self.navigationController pushViewController:refundInfoViewController animated:YES];
    } else if ([button.stringTag isEqualToString:@"rightButton"]){
        NSLog(@"退货退款");
        MMHRefundInfoViewController *refundInfoViewController = [[MMHRefundInfoViewController alloc]init];
//        refundInfoViewController.transferPrice = self.transferPrice;
        refundInfoViewController.refundTypeWithHome = MMHRefundTypeWithHomeProduct;
        refundInfoViewController.transferOrderNo = self.transferOrderNo;
        [self.navigationController pushViewController:refundInfoViewController animated:YES];
    }
    
}

@end
