//
//  MMHRefundCloseViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 退款关闭
#import "AbstractViewController.h"
#import "MMHRefundDetailViewController.h"
#import "MMHRefundInfoViewController.h"
@class MMHRefundDetailViewController;
@class MMHRefundInfoViewController;


typedef void(^reApplyBlock)();
@interface MMHRefundCloseViewController : AbstractViewController

//@property (nonatomic,assign)MMHRefundApplyState transferRefundApplyState;           /**< 审核状态- 正在审核*/

@property (nonatomic,copy)reApplyBlock reApplyBlock;                                // 重新申请
@property (nonatomic,assign)MMHRefundApplyState refundCloseType;                    // 关闭状态
@end
