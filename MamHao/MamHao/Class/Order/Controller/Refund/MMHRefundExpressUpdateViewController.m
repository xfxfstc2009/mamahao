//
//  MMHRefundExpressUpdateViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRefundExpressUpdateViewController.h"
#import "MMHRefundExpressChooseViewController.h"
#import "MMHNetworkAdapter+Order.h"

@interface MMHRefundExpressUpdateViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic,strong)UITableView *refundExpressTableView;            // 退款物流详情
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;           // 数组

@property (nonatomic,strong)UITextField *expressTypeTextField;              // 物流textField
@property (nonatomic,strong)UITextField *expressNumberTextField;            // 物流单号textField
@property (nonatomic,copy)NSString *expressCode;                            /**< 物流code */

@end

@implementation MMHRefundExpressUpdateViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                 // 页面设置
    [self arrayWithInit];               // 数组初始化
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"修改物流";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerWithAnimation];
    }];
}

-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
    NSArray *tempArr = @[@[@"退货物流"],@[@"物流单号"],@[@"确定修改"]];
    [self.dataSourceMutableArr addObjectsFromArray:tempArr];
    
}

-(void)createTableView{
    if (!self.refundExpressTableView){
        self.refundExpressTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.refundExpressTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.refundExpressTableView.delegate = self;
        self.refundExpressTableView.dataSource = self;
        self.refundExpressTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.refundExpressTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.refundExpressTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSeparatorStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            
            // 创建leftLabel
            UILabel *leftLabel = [[UILabel alloc]init];
            leftLabel.backgroundColor = [UIColor clearColor];
            leftLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            leftLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            leftLabel.stringTag = @"leftLabel";
            leftLabel.frame = CGRectMake(MMHFloat(16), 0, 100, MMHFloat(44));
            leftLabel.textAlignment = NSTextAlignmentCenter;
            [cellWithRowOne addSubview:leftLabel];
            
            // 背景
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
            backgroundImageView.frame = CGRectMake(CGRectGetMaxX(leftLabel.frame) + MMHFloat(10), 0, tableView.bounds.size.width - 2 * MMHFloat(16) - leftLabel.frame.size.width , mmh_relative_float(44));
            [cellWithRowOne addSubview:backgroundImageView];

            
            // userNameTextField
            self.expressTypeTextField = [[UITextField alloc] init];
            self.expressTypeTextField.frame = CGRectMake(backgroundImageView.frame.origin.x + MMHFloat(10),0,backgroundImageView.bounds.size.width - 2 * MMHFloat(10),MMHFloat(44));
            [self.expressTypeTextField setBackgroundColor:[UIColor clearColor]];
            self.expressTypeTextField.textAlignment=NSTextAlignmentLeft;
            self.expressTypeTextField.placeholder = @"请选择您的物流";
            self.expressTypeTextField.userInteractionEnabled = NO;
            self.expressTypeTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            self.expressTypeTextField.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
            self.expressTypeTextField.font = kStyleFont;
            self.expressTypeTextField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
            self.expressTypeTextField.returnKeyType=UIReturnKeyDone;
            self.expressTypeTextField.delegate=self;
            [cellWithRowOne addSubview:self.expressTypeTextField];
        }
        // 赋值
        UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"leftLabel"];
        fixedLabel.text = [[self.dataSourceMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if(!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSeparatorStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
            
            // 创建leftLabel
            UILabel *leftLabel = [[UILabel alloc]init];
            leftLabel.backgroundColor = [UIColor clearColor];
            leftLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            leftLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
            leftLabel.stringTag = @"leftLabel";
            leftLabel.frame = CGRectMake(MMHFloat(16), 0, 100, MMHFloat(44));
            leftLabel.textAlignment = NSTextAlignmentCenter;
            [cellWithRowTwo addSubview:leftLabel];
            
            // 背景
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
            backgroundImageView.frame = CGRectMake(CGRectGetMaxX(leftLabel.frame) + MMHFloat(10), 0, tableView.bounds.size.width - 2 * MMHFloat(16) - leftLabel.frame.size.width , mmh_relative_float(44));
            [cellWithRowTwo addSubview:backgroundImageView];
            
            
            // userNameTextField
            self.expressNumberTextField = [[UITextField alloc] init];
            self.expressNumberTextField.frame = CGRectMake(backgroundImageView.frame.origin.x + MMHFloat(10),0,backgroundImageView.bounds.size.width - 2 * MMHFloat(10),MMHFloat(44));
            [self.expressNumberTextField setBackgroundColor:[UIColor clearColor]];
            self.expressNumberTextField.textAlignment=NSTextAlignmentLeft;
            self.expressNumberTextField.placeholder = @"请输入您的物流编号";
            self.expressNumberTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            self.expressNumberTextField.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
            self.expressNumberTextField.font = kStyleFont;
            self.expressNumberTextField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
            self.expressNumberTextField.returnKeyType=UIReturnKeyDone;
            self.expressNumberTextField.delegate=self;
            self.expressNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
            [cellWithRowTwo addSubview:self.expressNumberTextField];
        }
        // 赋值
        UILabel *fixedLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"leftLabel"];
        fixedLabel.text = [[self.dataSourceMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        return cellWithRowTwo;

        
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 创建button
            UIButton *successButton = [UIButton buttonWithType:UIButtonTypeCustom];
            successButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
            [successButton addTarget:self action:@selector(successButtonClick) forControlEvents:UIControlEventTouchUpInside];
            successButton.frame = CGRectMake(MMHFloat(16), 0, tableView.bounds.size.width - 2 * MMHFloat(16), MMHFloat(48));
            [successButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            [successButton setTitle:@"确定修改" forState:UIControlStateNormal];
            [cellWithRowTwo addSubview:successButton];
        }
        
        return cellWithRowTwo;
    }
}

#pragma mark - UItableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return MMHFloat(24);
    } else if (section == 1){
        return MMHFloat(14);
    } else if (section == 2){
        return MMHFloat(28);
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){
        __weak typeof(self)weakSelf = self;
        MMHRefundExpressChooseViewController *refundExpressChooseViewController = [[MMHRefundExpressChooseViewController alloc]init];

        refundExpressChooseViewController.refundExpressChooseBlock = ^(MMHQueryLogisticsSingleModel *expressSingleModel){
            weakSelf.expressTypeTextField.text = expressSingleModel.platform;
            weakSelf.expressCode = expressSingleModel.platformCode;
        };
        UINavigationController *refundExpressChooseNav = [[UINavigationController alloc]initWithRootViewController:refundExpressChooseViewController];
        [self.navigationController presentViewController:refundExpressChooseNav animated:YES completion:nil];
    }
}


#pragma mark - ActionClick
-(void)successButtonClick{
    if (!self.expressTypeTextField.text.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请选择物流" buttonTitles:@[@"确定"] callback:nil]show];
    } else if (!self.expressNumberTextField.text.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请填写物流单号" buttonTitles:@[@"确定"] callback:nil]show];
    } else {
        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestToUpdateExpressInfoWithrefundLineId:weakSelf.transferRefundLineId wayBillNumber:weakSelf.expressNumberTextField.text platformCode:weakSelf.expressCode];
    }
}

#pragma mark - 修改物流信息
-(void)sendRequestToUpdateExpressInfoWithrefundLineId:(NSString *)refundLineId wayBillNumber:(NSString *)wayBillNumber platformCode:(NSString *)platformCode{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]sendRequestToUpdateExpressInfoWithRefundLineId:refundLineId wayBillNumber:wayBillNumber platformCode:platformCode from:nil succeededHandler:^(MMHExpressModel *expressModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.updateExpressBlock(strongSelf.expressTypeTextField.text,strongSelf.expressCode,strongSelf.expressNumberTextField.text);
        [strongSelf.navigationController dismissViewControllerWithAnimation];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view showTipsWithError:error];
    }];
}

@end
