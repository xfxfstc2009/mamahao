//
//  MMHOrderBuyAgainViewController.h
//  MamHao
//
//  Created by SmartMin on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHOrderSingleModel.h"

@interface MMHOrderBuyAgainViewController : AbstractViewController
@property (nonatomic,strong)MMHOrderSingleModel *transferOrderSingleModel;                  // 上个界面传递过来的model
@end
