//
//  MMHScanningProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHScanningProduct.h"


@interface MMHScanningProduct ()

@property (nonatomic, copy) NSString *barcode;
@end


@implementation MMHScanningProduct


- (instancetype)initWithBarcode:(NSString *)barcode
{
    self = [self init];
    if (self) {
        self.barcode = barcode;
    }
    return self;
}


- (MMHProductModule)module
{
    return MMHProductModuleScanning;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleScanning) {
        return @{};
    }

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    dictionary[@"barCode"] = self.barcode;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"deviceId"] = [UIDevice deviceID];

    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"bean special parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
