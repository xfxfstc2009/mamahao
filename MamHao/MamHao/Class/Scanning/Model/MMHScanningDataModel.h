//
//  MMHScanningDataModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHScanningDataModel <NSObject>

@end

@interface MMHScanningDataModel : MMHFetchModel
@property (nonatomic,copy)NSString *tmlNo;              /**< 二维码*/
@property (nonatomic,copy)NSString *shopId;             /**<商店id */
@end
