//
//  MMHScanningModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/29.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHScanningDataModel.h"
@interface MMHScanningModel : MMHFetchModel

@property (nonatomic,copy)NSString *identify;                   /**< 唯一编码*/
@property (nonatomic,assign)NSInteger type;                     /**< 扫码类型*/
@property (nonatomic,strong)MMHScanningDataModel *data;         /**< data*/


@end
