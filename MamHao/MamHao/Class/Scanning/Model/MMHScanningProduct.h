//
//  MMHScanningProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/4.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProduct.h"


@interface MMHScanningProduct : MMHProduct <MMHProductProtocol>

- (instancetype)initWithBarcode:(NSString *)barcode;

@end
