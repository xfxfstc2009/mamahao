//
//  MMHScanningViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【扫描二维码】
#import "AbstractViewController.h"

typedef void (^ScanBlock) (NSString *scanResult);
typedef void (^closeCameraBlock)();

@interface MMHScanningViewController : AbstractViewController

@property (nonatomic, copy) ScanBlock scanFinished;
@property (nonatomic,copy) closeCameraBlock closeCameraBlock;
@end
