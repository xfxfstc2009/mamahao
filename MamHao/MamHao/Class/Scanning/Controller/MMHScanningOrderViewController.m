//
//  MMHScanningOrderViewController.m
//  MamHao
//
//  Created by SmartMin on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHScanningOrderViewController.h"
#import "MMHShopCartModel.h"

@interface MMHScanningOrderViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *orderDataSourceMutableArr;
@property (nonatomic,strong)UITableView *scanningOrderTableView;
@property (nonatomic,strong)NSMutableArray *isSelectedMutableArr;

@end

@implementation MMHScanningOrderViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"选择订单";
}

-(void)arrayWithInit{
    self.orderDataSourceMutableArr = [NSMutableArray array];
    for (int i = 0 ; i<5 ;i++){
        [self.orderDataSourceMutableArr addObject: @[@"1"]];
    }
    
    self.isSelectedMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.scanningOrderTableView){
        self.scanningOrderTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.scanningOrderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.scanningOrderTableView.delegate = self;
        self.scanningOrderTableView.dataSource = self;
        self.scanningOrderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.scanningOrderTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.scanningOrderTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.orderDataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == self.orderDataSourceMutableArr.count - 1){
        return 1;
    } else {
        return 3;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == self.orderDataSourceMutableArr.count - 1){
        static NSString *cellIdentifyWithRowLast = @"cellIdentifyWithRowLast";
        UITableViewCell *cellWithRowLast = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowLast];
        if (!cellWithRowLast){
            cellWithRowLast = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowLast];
            cellWithRowLast.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowLast.backgroundColor = kStyleBackgroundColor;
           
            // 创建一个button
            UIButton *sendExamineButton = [UIButton buttonWithType:UIButtonTypeCustom];
            sendExamineButton.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
            [sendExamineButton addTarget:self action:@selector(sendExamineButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [sendExamineButton setTitle:@"提交审核" forState:UIControlStateNormal];
            [sendExamineButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            sendExamineButton.frame = CGRectMake(MMHFloat(16), 0, tableView.bounds.size.width - 2 * MMHFloat(16), MMHFloat(48));
            sendExamineButton.userInteractionEnabled = YES;
            sendExamineButton.layer.cornerRadius = MMHFloat(5);
            [cellWithRowLast addSubview:sendExamineButton];
        }
        return cellWithRowLast;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRonOne = @"cellIdentifyWithRowOne";
            UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRonOne];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithRowOne){
                cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRonOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleGray;
                // 1. 创建checkImageView
                UIImageView *checkImageView = [[UIImageView alloc]init];
                checkImageView.backgroundColor = [UIColor clearColor];
                checkImageView.frame = CGRectMake(MMHFloat(10), (cellHeight - MMHFloat(18))/2., MMHFloat(18), MMHFloat(18));
                checkImageView.stringTag = @"checkImageView";
                [cellWithRowOne addSubview:checkImageView];
                
                // 创建编号
                UILabel *orderNoLabel = [[UILabel alloc]init];
                orderNoLabel.backgroundColor = [UIColor clearColor];
                orderNoLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                orderNoLabel.frame = CGRectMake(CGRectGetMaxX(checkImageView.frame) + MMHFloat(5), 0, kScreenBounds.size.width - 2 * MMHFloat(10) - MMHFloat(18) - MMHFloat(5), cellHeight);
                orderNoLabel.stringTag = @"orderNoLabel";
                [cellWithRowOne addSubview:orderNoLabel];
            }
            // 赋值
            UIImageView *checkImageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"checkImageView"];
            if ([self.isSelectedMutableArr containsObject:@"包含"]){
                checkImageView.image = [UIImage imageNamed:@"cart_btn_select_s"];
            } else {
                checkImageView.image = [UIImage imageNamed:@"cart_btn_select_n"];
            }
            
            // orderLabel
            UILabel *orderNoLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"orderNoLabel"];
            orderNoLabel.text = [NSString stringWithFormat:@"订单编号:%@",@"E000000000000123"];
            return cellWithRowOne;
        } else if (indexPath.row == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
//                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                // 创建shopName
                UILabel *shopNameLabel = [[UILabel alloc]init];
                shopNameLabel.backgroundColor = [UIColor clearColor];
                shopNameLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                shopNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(10), MMHFloat(kScreenBounds.size.width - 2 * MMHFloat(10)), [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"提示"]]);
                shopNameLabel.stringTag = @"shopNameLabel";
                [cellWithRowTwo addSubview:shopNameLabel];
                
                // imageView
                for (int itemIndex = 0 ; itemIndex < 3 ; itemIndex++){
                    MMHImageView *productImageView = [[MMHImageView alloc]init];
                    productImageView.backgroundColor = [UIColor clearColor];
                    productImageView.stringTag = [NSString stringWithFormat:@"productImageView%li",(long)itemIndex];
                    productImageView.layer.masksToBounds = YES;
                    [productImageView.layer setCornerRadius:MMHFloat(6.)];
                    productImageView.layer.borderColor = [UIColor lightGrayColor].CGColor;
                    productImageView.layer.borderWidth = .5f;
                    productImageView.frame = CGRectMake(MMHFloat(10) + itemIndex * (MMHFloat(10) + MMHFloat(70)),CGRectGetMaxY(shopNameLabel.frame) + MMHFloat(10), MMHFloat(70), MMHFloat(70));
                    [cellWithRowTwo addSubview:productImageView];
                }
                
                // 创建priceLabel
                UILabel *priceLabel = [[UILabel alloc]init];
                priceLabel.textAlignment = NSTextAlignmentRight;
                priceLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                priceLabel.stringTag = @"priceLabel";
                [cellWithRowTwo addSubview:priceLabel];
                
                // 创建priceLabel
                UILabel *numberLabel = [[UILabel alloc]init];
                numberLabel.backgroundColor = [UIColor clearColor];
                numberLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                numberLabel.textAlignment = NSTextAlignmentRight;
                numberLabel.stringTag = @"numberLabel";
                numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [cellWithRowTwo addSubview:numberLabel];
                
                // 创建title
                UILabel *contentLabel = [[UILabel alloc]init];
                contentLabel.backgroundColor = [UIColor clearColor];
                contentLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
                contentLabel.stringTag = @"contentLabel";
                contentLabel.textAlignment = NSTextAlignmentLeft;
                [cellWithRowTwo addSubview:contentLabel];
                
                // 创建skuLabel
                UILabel *skuLabel = [[UILabel alloc]init];
                skuLabel.backgroundColor = [UIColor clearColor];
                skuLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
                skuLabel.stringTag = @"skuLabel";
                skuLabel.textAlignment = NSTextAlignmentLeft;
                skuLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                [cellWithRowTwo addSubview:skuLabel];
                
                // 创建productCountLabel
                UILabel *productCountLabel = [[UILabel alloc]init];
                productCountLabel.backgroundColor = [UIColor clearColor];
                productCountLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                productCountLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
                productCountLabel.stringTag = @"productCountLabel";
                [cellWithRowTwo addSubview:productCountLabel];
                
                // 创建arrowImageView
                UIImageView *arrowImageView = [[UIImageView alloc]init];
                arrowImageView.backgroundColor = [UIColor clearColor];
                arrowImageView.image = [UIImage imageNamed:@"tool_arrow"];
                arrowImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - MMHFloat(9),(MMHFloat(70) - MMHFloat(15))/2. + CGRectGetMaxY(shopNameLabel.frame) + MMHFloat(10), MMHFloat(9), MMHFloat(15));
                arrowImageView.stringTag = @"arrowImageView";
                [cellWithRowTwo addSubview:arrowImageView];
                
            }
            // 赋值
            UILabel *shopNameLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"shopNameLabel"];
            shopNameLabel.text = @"武林门好孩子门店";
            
            MMHImageView *productImageView0 = (MMHImageView *)[cellWithRowTwo viewWithStringTag:@"productImageView0"];
            MMHImageView *productImageView1 = (MMHImageView *)[cellWithRowTwo viewWithStringTag:@"productImageView1"];
            MMHImageView *productImageView2 = (MMHImageView *)[cellWithRowTwo viewWithStringTag:@"productImageView2"];
            [productImageView0 updateViewWithImageAtURL:@"123"];
            [productImageView1 updateViewWithImageAtURL:@"123"];
            [productImageView2 updateViewWithImageAtURL:@"123"];
            
            UIImageView *arrowImageView = (UIImageView *)[cellWithRowTwo viewWithStringTag:@"arrowImageView"];
            UILabel *productCountLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"productCountLabel"];
            productCountLabel.text = [NSString stringWithFormat:@"共%li件商品",(long)8];
            CGFloat productCountWidth = [productCountLabel.text sizeWithCalcFont:productCountLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)].width;
            productCountLabel.frame = CGRectMake(CGRectGetMinX(arrowImageView.frame) - MMHFloat(10) - productCountWidth, arrowImageView.orgin_y, productCountWidth, arrowImageView.bounds.size.height);
            
            
            //test
            productImageView1.hidden = YES;
            productImageView2.hidden = YES;
            productCountLabel.hidden = YES;
            arrowImageView.hidden = YES;
            
            UILabel *contentLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"contentLabel"];
            // 【priceLabel】
            UILabel *priceLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"priceLabel"];
            priceLabel.text = @"￥30099.00";
            CGFloat priceWidth = [priceLabel.text sizeWithCalcFont:priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)].width;
            priceLabel.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - priceWidth, productImageView0.frame.origin.y, priceWidth, [MMHTool contentofHeight:priceLabel.font]);
            //【number】
            UILabel *numberLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"numberLabel"];
            numberLabel.text = [NSString stringWithFormat:@"X%li",(long)1];
            numberLabel.textAlignment = NSTextAlignmentRight;
            numberLabel.frame = CGRectMake(priceLabel.frame.origin.x, CGRectGetMaxY(priceLabel.frame) + MMHFloat(5), priceLabel.bounds.size.width, priceLabel.bounds.size.height);
            
            // 【content】
            contentLabel.text = @"日本原装进口花王日本原日本原装进口花王日本原日本原装进口花王日本原日本原装进口花王日本原日本原装进口花王日本原王";
            CGFloat contentHeight = [contentLabel.text sizeWithCalcFont:contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(productImageView0.frame) - MMHFloat(10) * 2 - MMHFloat(priceLabel.bounds.size.width) - MMHFloat(10), CGFLOAT_MAX)].height;
            contentLabel.frame = CGRectMake(CGRectGetMaxX(productImageView0.frame) + MMHFloat(10), productImageView0.frame.origin.y, kScreenBounds.size.width - CGRectGetMaxX(productImageView0.frame) - MMHFloat(10) * 2 - MMHFloat(priceLabel.bounds.size.width) - MMHFloat(10), contentHeight);
            
            if (contentHeight > [MMHTool contentofHeight:contentLabel.font]){
                contentLabel.numberOfLines = 2;
                contentLabel.size_height = [MMHTool contentofHeight:contentLabel.font] * 2;
            } else {
                contentLabel.numberOfLines = 1;
                contentLabel.size_height = [MMHTool contentofHeight:contentLabel.font];
            }
            
            // 【sku】
            UILabel *skuLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"skuLabel"];
            skuLabel.text = @"123 12312";
            skuLabel.frame = CGRectMake(contentLabel.orgin_x, CGRectGetMaxY(contentLabel.frame) + MMHFloat(5), contentLabel.bounds.size.width, [MMHTool contentofHeight:skuLabel.font]);
            
            return cellWithRowTwo;
            
        } else if (indexPath.row == 2){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                // 创建label
                UILabel *payLabel = [[UILabel alloc]init];
                payLabel.frame = CGRectMake(MMHFloat(10), 0, kScreenBounds.size.width - 2 * MMHFloat(10), cellHeight);
                payLabel.stringTag = @"payLabel";
                payLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
                [cellWithRowThr addSubview:payLabel];
            }
            UILabel *payLabel = (UILabel *)[cellWithRowThr viewWithStringTag:@"payLabel"];
            NSString *payString = [NSString stringWithFormat:@"实付款:%@",@"2222"];
            payLabel.attributedText = [MMHTool rangeLabelWithContent:payString hltContentArr:@[@"2222"] hltColor:[UIColor colorWithCustomerName:@"红"] normolColor:[UIColor colorWithCustomerName:@"黑"]];
            return cellWithRowThr;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0){
        UITableViewCell *indexCell = [tableView cellForRowAtIndexPath:indexPath];
        UIImageView *checkImageView = (UIImageView *)[indexCell viewWithStringTag:@"checkImageView"];
        [self animationWithCollectionWithButton:checkImageView collection:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return MMHFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.orderDataSourceMutableArr.count - 1){
        return MMHFloat(10);
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == self.orderDataSourceMutableArr.count - 1){
        return MMHFloat(48);
    } else {
        if (indexPath.row == 0){
            return MMHFloat(30);
        } else if (indexPath.row == 1){
            return MMHFloat(10) * 2 + MMHFloat(70) + MMHFloat(13) +[MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"提示"]] ;
        } else if (indexPath.row == 2){
            return MMHFloat(27);
        } else {
            return 0;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.scanningOrderTableView) {
        if (indexPath.section != (self.orderDataSourceMutableArr.count - 1)){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == 2){
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

#pragma mark - 动画
-(void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection{
    if (isCollection){
        collectionButton.image = [UIImage imageNamed:@"address_icon_selected"];
    } else {
        collectionButton.image = [UIImage imageNamed:@"address_icon_untick"];
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    isCollection = !isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}


#pragma mark - actionClick
-(void)sendExamineButtonClick:(UIButton *)sender{
    NSLog(@"发送");
}


@end
