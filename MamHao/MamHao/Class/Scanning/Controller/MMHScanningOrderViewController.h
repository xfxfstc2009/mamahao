//
//  MMHScanningOrderViewController.h
//  MamHao
//
//  Created by SmartMin on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【扫描二维码后订单跳转】
#import "AbstractViewController.h"

@interface MMHScanningOrderViewController : AbstractViewController

@end
