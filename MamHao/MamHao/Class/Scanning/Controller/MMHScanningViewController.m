//
//  MMHScanningViewController.m
//  MamHao
//
//  Created by SmartMin on 15/5/19.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHScanningViewController.h"
#import "MMHAssetLibraryViewController.h"
#import "MMHScanningOrderViewController.h"      // 跳转到扫描页面
#import "MMHConfirmationOrderViewController.h"  // 去结算
#import "MMHWebViewController.h"
#import "MMHProductDetailViewController1.h"

#import "MMHScanningModel.h"
#import "MMHNetworkAdapter+Scanning.h"          // 请求接口
#import "MMHAccountSession.h"
#import "MMHSingleProductModel.h"
#import "MMHScanningProduct.h"

#import "ZBarSDK.h"
#import "ZBarCameraSimulator.h"


@interface MMHScanningViewController()<ZBarReaderViewDelegate,ZBarReaderDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>{
    int num;
    BOOL upOrdown;
    NSTimer *timer;
    UIButton *rightButton;
    UIImageView *scanningTopBackgroundImageView;
    UIImageView *scanningBottomBackgroundImageView;
    UIImageView *scanningMiddeMiddleBackgroundImageView;
    UIButton *inputBarCodeButton;
    UIButton *lightButton;
    UIButton *chooseAssetButton;
    UILabel *labelBarcodeTitle;
}

@property (nonatomic,strong)UIView *viewLine;
@property (nonatomic,strong)UITextField *inputBarCodeTextField;
@property (nonatomic,assign)BOOL isShowAll;

@property (nonatomic,assign)NSInteger closeCameraTime;
@property (nonatomic,strong)NSTimer *closeCameraTimer;
@property (nonatomic,assign)BOOL isRedirect;
@end

@implementation MMHScanningViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    // set time
    num = 0;
    upOrdown = NO;
    timer = [NSTimer scheduledTimerWithTimeInterval:.02f target:self selector:@selector(animation) userInfo:nil repeats:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 开启zBar
    ZBarReaderView *readerView = (ZBarReaderView *)[self.view viewWithStringTag:@"readerView"];

    [readerView start];
    if (self.closeCameraTimer){
        self.closeCameraTime = 60;
        self.closeCameraTimer = [NSTimer scheduledTimerWithTimeInterval:self.closeCameraTime target:self selector:@selector(closeCameraAnimation) userInfo:nil repeats:YES];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
    if (self.closeCameraTimer){
        [self.closeCameraTimer invalidate];
        self.closeCameraTimer = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createZBar];
    [self createNavBar];
    [self createView];
}

#pragma mark 关闭相机
-(void)closeCameraAnimation{
    if (self.closeCameraBlock){
        self.closeCameraBlock();
    }
    [self popWithAnimation];
}

#pragma mark - CreateView
-(void)createView{
    // 创建输入条形码按钮
    inputBarCodeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    inputBarCodeButton.backgroundColor = [UIColor clearColor];
    [inputBarCodeButton addTarget:self action:@selector(inputBarCodeClick) forControlEvents:UIControlEventTouchUpInside];
    [inputBarCodeButton setTitle:@"输入条形码" forState:UIControlStateNormal];
    if (kScreenBounds.size.height >= 480){
        inputBarCodeButton.frame = CGRectMake(MMHFloat(40), kScreenBounds.size.height - MMHFloat(85) - MMHFloat(40), MMHFloat(125), MMHFloat(40));
    }
    [self.view addSubview:inputBarCodeButton];
    inputBarCodeButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
    inputBarCodeButton.layer.borderWidth = 1;
    inputBarCodeButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [inputBarCodeButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
    [inputBarCodeButton makeRoundedRectangleShape];
    
    // 1. 创建light button
    lightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    lightButton.backgroundColor = [UIColor clearColor];
    [lightButton setImage:[UIImage imageNamed:@"scan-code_btn_flashlight"] forState:UIControlStateNormal];
    [lightButton addTarget:self action:@selector(lightButtonClick) forControlEvents:UIControlEventTouchUpInside];
    if (kScreenBounds.size.height <= 480){
        lightButton.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(51)) / 2. , scanningBottomBackgroundImageView.frame.origin.y + MMHFloat(32), MMHFloat(51), MMHFloat(51));
        
        inputBarCodeButton.frame = CGRectMake(MMHFloat(40), CGRectGetMaxY(lightButton.frame) + MMHFloat(32), MMHFloat(125), MMHFloat(40));
    } else {
        lightButton.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(51)) / 2. , CGRectGetMinY(inputBarCodeButton.frame) - MMHFloat(32) - MMHFloat(51), MMHFloat(51), MMHFloat(51));
    }
    lightButton.stringTag = @"lightButton";
    [self.view addSubview:lightButton];

    // 创建选择相册按钮
    chooseAssetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseAssetButton.backgroundColor = [UIColor clearColor];
    [chooseAssetButton addTarget:self action:@selector(chooseAlAssetClick) forControlEvents:UIControlEventTouchUpInside];
    [chooseAssetButton setTitle:@"选择相册按钮" forState:UIControlStateNormal];
    chooseAssetButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(40) - MMHFloat(125), inputBarCodeButton.frame.origin.y, MMHFloat(125), MMHFloat(40));
    [self.view addSubview:chooseAssetButton];
    chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
    chooseAssetButton.layer.borderWidth = 1;
    chooseAssetButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
    [chooseAssetButton makeRoundedRectangleShape];
    
    self.inputBarCodeTextField = [[UITextField alloc]init];
    self.inputBarCodeTextField.backgroundColor = [UIColor whiteColor];
    self.inputBarCodeTextField.placeholder = @"请输入条形码";
    self.inputBarCodeTextField.delegate = self;
    self.inputBarCodeTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.inputBarCodeTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.inputBarCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.inputBarCodeTextField.frame = CGRectMake((kScreenBounds.size.width - MMHFloat(265))/ 2., scanningTopBackgroundImageView.bounds.size.height - MMHFloat(13), MMHFloat(265), 38);
    self.inputBarCodeTextField.font = [UIFont fontWithName:@"Helvetica-Bold" size:MMHFloat(15 + 2)];
    self.inputBarCodeTextField.hidden = YES;
    self.inputBarCodeTextField.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self.view addSubview:self.inputBarCodeTextField];
    self.isShowAll = YES;
}

#pragma mark CreatezBar
-(void)createZBar{
    //实例化条形码扫描SDK
    if (![UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
        [[UIAlertView alertViewWithTitle:@"提示" message:@"当前设备不可用 " buttonTitles:@[@"确认"] callback:nil]show];
        return ;
    } else {
        ZBarReaderView *readerView = [[ZBarReaderView alloc] init];
        readerView.frame = kScreenBounds;
        readerView.torchMode = 0;               // 0 不开摄像头 1 开
        readerView.readerDelegate = self;
        readerView.stringTag = @"readerView";
        [self.view addSubview:readerView];
        [readerView start];
    }
    
    // set backImageView
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    imageView.image =[UIImage imageNamed:@"Image_bg_rectangle"];
    [self.view addSubview:imageView];
    
    upOrdown = NO;
    num = 0;
    // set imageLine
    self.viewLine = [[UIImageView alloc] init];
    self.viewLine.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [self.view addSubview:self.viewLine];
    
}

-(void)createNavBar{
    
    scanningTopBackgroundImageView = [[UIImageView alloc]init];
    scanningTopBackgroundImageView.backgroundColor = [UIColor clearColor];
    scanningTopBackgroundImageView.image = [UIImage imageNamed:@"scan-code_img_on"];
    scanningTopBackgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(150));
    [self.view addSubview:scanningTopBackgroundImageView];
    
    scanningMiddeMiddleBackgroundImageView = [[UIImageView alloc]init];
    scanningMiddeMiddleBackgroundImageView.backgroundColor = [UIColor clearColor];
    scanningMiddeMiddleBackgroundImageView.image = [UIImage imageNamed:@"scan-code_img_middle"];
    scanningMiddeMiddleBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(scanningTopBackgroundImageView.frame), kScreenBounds.size.width, MMHFloat(265) - 20);
    [self.view addSubview:scanningMiddeMiddleBackgroundImageView];
    
    scanningBottomBackgroundImageView = [[UIImageView alloc]init];
    scanningBottomBackgroundImageView.backgroundColor = [UIColor clearColor];
    scanningBottomBackgroundImageView.userInteractionEnabled = YES;
    scanningBottomBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(scanningMiddeMiddleBackgroundImageView.frame),kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(scanningMiddeMiddleBackgroundImageView.frame));
    UIImage *bottomImage = [UIImage imageNamed:@"scan-code_img_down"];
    UIImage *stretchableImage = [bottomImage stretchableImageWithLeftCapWidth:0 topCapHeight:bottomImage.size.height * 0.99];
    scanningBottomBackgroundImageView.image = stretchableImage;
    [self.view addSubview:scanningBottomBackgroundImageView];
    
    // set init label
    labelBarcodeTitle = [[UILabel alloc] init];
    labelBarcodeTitle.font = [UIFont systemFontOfCustomeSize:14.];
    labelBarcodeTitle.textColor = [UIColor colorWithCustomerName:@"白"];
    [labelBarcodeTitle setText:@"请将二维码/条形码放到框内即可进行扫描"];
    CGSize contentOfSize = [labelBarcodeTitle.text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:14.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    NSLog(@"%.2f",scanningTopBackgroundImageView.bounds.size.height - MMHFloat(6) - 20);
    labelBarcodeTitle.frame = CGRectMake((kScreenBounds.size.width - contentOfSize.width) / 2., scanningTopBackgroundImageView.bounds.size.height - MMHFloat(6) - 40, contentOfSize.width, 20);
    [self.view addSubview:labelBarcodeTitle];
    
    // 创建返回按钮
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dismissBtn setImage:[UIImage imageNamed:@"basc_nav_back"] forState:UIControlStateNormal];
    [[dismissBtn titleLabel] setFont:[UIFont systemFontOfSize:14.]];
    dismissBtn.frame = CGRectMake(10, 20 + (44 - MMHFloat(40)) / 2. , MMHFloat(40), MMHFloat(40));
    [dismissBtn addTarget:self action:@selector(actionDismiss) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:dismissBtn];
    
    // 创建扫一扫title
    UILabel *scanningTitle = [[UILabel alloc]init];
    scanningTitle.backgroundColor = [UIColor clearColor];
    scanningTitle.text = @"扫一扫";
    scanningTitle.font = [UIFont boldSystemFontOfSize:16.];
    scanningTitle.frame = CGRectMake((kScreenBounds.size.width - 100) / 2., 20, 100, 44);
    scanningTitle.textColor = [UIColor whiteColor];
    scanningTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:scanningTitle];
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnceHideKeyBoard)];
    [scanningBottomBackgroundImageView addGestureRecognizer:tapForHideKeyBoard];
}


#pragma mark - ActionClick
-(void)tapOnceHideKeyBoard{
    if ([self.inputBarCodeTextField isFirstResponder]){
        [self.inputBarCodeTextField resignFirstResponder];
    }
}

-(void)actionDismiss{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)lightButtonClick{
    ZBarReaderView *readerView = (ZBarReaderView *)[self.view viewWithStringTag:@"readerView"];
    readerView.torchMode = 1 - readerView.torchMode;
    if (readerView.torchMode == 0){
        [lightButton setImage:[UIImage imageNamed:@"scan-code_btn_flashlight"] forState:UIControlStateNormal];
    } else {
        [lightButton setImage:[UIImage imageNamed:@"scan-code_btn_click"] forState:UIControlStateNormal];
    }
}

-(void)inputBarCodeClick{
    ZBarReaderView *readerView = (ZBarReaderView *)[self.view viewWithStringTag:@"readerView"];
    if (self.isShowAll){
        self.viewLine.hidden = YES;
        [readerView stop];
    } else {
        self.viewLine.hidden = NO;
        [readerView start];
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf inputBarCodeAnimation];
    
    self.isShowAll = !self.isShowAll;
}

#pragma mark - ZBarReaderViewDelegate
- (void)readerView:(ZBarReaderView*)readerView didReadSymbols:(ZBarSymbolSet*)symbols fromImage:(UIImage*)image {
    // 得到扫描的条码内容
    const zbar_symbol_t *symbol = zbar_symbol_set_first_symbol(symbols.zbarSymbolSet);
    NSString *symbolStr = [NSString stringWithUTF8String: zbar_symbol_get_data(symbol)];
    //    self.scanFinished(symbolStr);
    [readerView stop];
    [self directionalWithCode:symbolStr];
    
    // set pop to root view controller animated
    //    [self.navigationController popViewControllerAnimated:YES];
    // remove from superview
    //    [readerView removeFromSuperview];
}

#pragma mark - UItextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (self.inputBarCodeTextField == textField) {
        if ([toBeString length] > 0) {
            if (toBeString.length >= 30){
                textField.text = [toBeString substringToIndex:30];
                return NO;
            }
            [chooseAssetButton setBackgroundColor:[UIColor colorWithCustomerName:@"粉"]];
            [chooseAssetButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
            chooseAssetButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;
            return YES;
        } else {
            [chooseAssetButton setBackgroundColor:[UIColor clearColor]];
            [chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
            chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            return YES;
        }
    }
    return YES;
}

-(void)directionalWithCode:(NSString *)symbolStr{               // 解析
    ZBarReaderView *readerView = (ZBarReaderView *)[self.view viewWithStringTag:@"readerView"];
    if (symbolStr.length){
        NSDictionary *dataSource = [NSJSONSerialization JSONObjectWithData:[symbolStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
        MMHScanningModel *shareModel = [[MMHScanningModel alloc]initWithJSONDict:dataSource];
        if ([symbolStr hasPrefix:@"http://"]){  // 跳转h5
            MMHWebViewController *webViewController  = [[MMHWebViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:symbolStr];
            [self.navigationController pushViewController:webViewController animated:YES];
        }
        
        if (shareModel.type){
            NSDictionary *parameterDic = [self scanningParameterManager:shareModel];
            __weak typeof(self)weakSelf = self;
            if (weakSelf.isRedirect == NO){
                weakSelf.isRedirect = YES;
                [readerView stop];
                [weakSelf sendRequestWithParameter:parameterDic];
            }
        } else {
            if (self.isRedirect == NO){
                self.isRedirect = YES;
                [self redirectionToProductDetailWithBarCode:symbolStr];
            }
        }
    } else {
        [UIAlertView alertViewWithTitle:nil message:@"扫描不到信息" buttonTitles:@[@"确定"] callback:nil];
    }
}

#pragma mark - 接口
-(void)sendRequestWithParameter:(NSDictionary *)parameter{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter]fetchModelScanningWithParameter:parameter from:nil succeededHandler:^(MMHOrderWithConfirmationModel *orderWithConfirmationModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =  weakSelf;
        
        // 跳转到去结算
        MMHConfirmationOrderViewController *confirmationOrderViewController = [[MMHConfirmationOrderViewController alloc]init];
        confirmationOrderViewController.productSaleType = MMHProductSaleTypeRMB;        // 人命币支付
        confirmationOrderViewController.confirmationRedirectionType = MMHConfirmationRedirectionTypeScanning; // 扫码进入
        confirmationOrderViewController.orderWithConfirmationModel = orderWithConfirmationModel;
        strongSelf.isRedirect = YES;
        [strongSelf.navigationController pushViewController:confirmationOrderViewController animated:YES];
    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.isRedirect = NO;
        [strongSelf.view showTipsWithError:error];
        ZBarReaderView *readerView = (ZBarReaderView *)[strongSelf.view viewWithStringTag:@"readerView"];
        [readerView start];
    }];
}

// 字典拼接
-(NSDictionary *)scanningParameterManager:(MMHScanningModel *)shareModel{
    NSMutableDictionary *parameterDic = [NSMutableDictionary dictionary];
    NSString *phone = [[MMHAccountSession currentSession] phone];
    NSString *type = [NSString stringWithFormat:@"%li",(long)shareModel.type];
    if (shareModel.type == 2){          // 小票订单
        [parameterDic setObject:phone forKey:@"phone"];
        [parameterDic setObject:type forKey:@"type"];
        [parameterDic setObject:shareModel.identify forKey:@"identify"];
        [parameterDic setObject:shareModel.data.tmlNo forKey:@"tmlNumId"];
    }
    return parameterDic;
}

// 重定向到商品详情
-(void)redirectionToProductDetailWithBarCode:(NSString *)barCode{
    MMHScanningProduct *product = [[MMHScanningProduct alloc] initWithBarcode:barCode];
    MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
    [self pushViewController:productDetailViewController];
    self.isRedirect = NO;
}

#pragma mark - OtherManager
// 条形码动画
-(void)inputBarCodeAnimation{
    [UIView animateWithDuration:.5f animations:^{
        if (self.isShowAll){
            scanningMiddeMiddleBackgroundImageView.size_height = 10;
            self.inputBarCodeTextField.alpha = 1;
            lightButton.alpha = 0;
            labelBarcodeTitle.text = @"请输入商品条码";
            labelBarcodeTitle.textAlignment = NSTextAlignmentCenter;
            [inputBarCodeButton setTitle:@"切换到扫码" forState:UIControlStateNormal];
            inputBarCodeButton.orgin_y = CGRectGetMaxY(scanningMiddeMiddleBackgroundImageView.frame) + MMHFloat(20) + MMHFloat(30);
            [chooseAssetButton setTitle:@"确定" forState:UIControlStateNormal];
            // 关闭关闭摄像头
            if (self.closeCameraTimer){
                [self.closeCameraTimer invalidate];
                self.closeCameraTimer = nil;
            }
            if (self.inputBarCodeTextField.text.length){
                [chooseAssetButton setBackgroundColor:[UIColor colorWithCustomerName:@"粉"]];
                [chooseAssetButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
                chooseAssetButton.layer.borderColor = [UIColor colorWithCustomerName:@"粉"].CGColor;
            } else {
                [chooseAssetButton setBackgroundColor:[UIColor clearColor]];
                [chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
                chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            }
        } else {
            labelBarcodeTitle.text = @"请将二维码/条形码放到框内即可进行扫描";
            labelBarcodeTitle.textAlignment = NSTextAlignmentCenter;
            scanningMiddeMiddleBackgroundImageView.size_height = MMHFloat(265) - 20;
            self.inputBarCodeTextField.alpha = 0;
            lightButton.alpha = 1;
            [inputBarCodeButton setTitle:@"输入条形码" forState:UIControlStateNormal];
            if (kScreenBounds.size.height > 480){
                inputBarCodeButton.frame = CGRectMake(MMHFloat(40), kScreenBounds.size.height - MMHFloat(85) - MMHFloat(40), MMHFloat(125), 40);
            } else {
                inputBarCodeButton.frame = CGRectMake(MMHFloat(40), CGRectGetMaxY(lightButton.frame) + MMHFloat(32), MMHFloat(125), 40);
            }
            [chooseAssetButton setTitle:@"选择相册按钮" forState:UIControlStateNormal];
            [chooseAssetButton setBackgroundColor:[UIColor clearColor]];
            [chooseAssetButton setTitleColor:[UIColor hexChangeFloat:@"c9c9c9"] forState:UIControlStateNormal];
            chooseAssetButton.layer.borderColor = [UIColor hexChangeFloat:@"9d9d9d"].CGColor;
            
            num = 0;
            upOrdown = NO;
            // 重新开启关闭摄像头
            self.closeCameraTime = 60;
            self.closeCameraTimer = [NSTimer scheduledTimerWithTimeInterval:self.closeCameraTime target:self selector:@selector(closeCameraAnimation) userInfo:nil repeats:YES];
        }
        chooseAssetButton.orgin_y = inputBarCodeButton.frame.origin.y;
        scanningBottomBackgroundImageView.frame = CGRectMake(0, CGRectGetMaxY(scanningMiddeMiddleBackgroundImageView.frame),kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(scanningMiddeMiddleBackgroundImageView.frame));
    } completion:^(BOOL finished) {
        if (self.isShowAll){
            self.inputBarCodeTextField.hidden = YES;
            lightButton.hidden = NO;
            if ([self.inputBarCodeTextField isFirstResponder]){
                [self.inputBarCodeTextField resignFirstResponder];
            }
        } else {
            self.inputBarCodeTextField.hidden = NO;
            lightButton.hidden = YES;
            [self.inputBarCodeTextField becomeFirstResponder];
        }
    }];
}

// 选择相册内容
-(void)chooseAlAssetClick{                  // 选择相册内容
    if (self.isShowAll){
        MMHAssetLibraryViewController *assetLibraryViewController = [[MMHAssetLibraryViewController alloc]init];
        assetLibraryViewController.numberOfCouldSelectingAssets = 1;
        __weak typeof(self) weakSelf = self;
        assetLibraryViewController.selectAssetsBlock = ^(NSArray *imageArr,NSArray *assetArr){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *chooseImage = [imageArr lastObject];
            CGImageRef chooseImageWithRef = chooseImage.CGImage;
            ZBarReaderController *reader = [[ZBarReaderController alloc]init];
            ZBarSymbol *symbol = nil;
            for (symbol in [reader scanImage:chooseImageWithRef])
                break;
            [strongSelf directionalWithCode:symbol.data];
        };
        UINavigationController *alassetLibraryNavigation = [[UINavigationController alloc]initWithRootViewController:assetLibraryViewController];
        [self.navigationController presentViewController:alassetLibraryNavigation animated:YES completion:nil];
        num = 0;
    } else {
        [self directionalWithCode:self.inputBarCodeTextField.text];
    }
}

// Animation
- (void)animation {
    if (upOrdown == NO) {
        num++;
        [self.viewLine setFrame:CGRectMake((kScreenBounds.size.width - MMHFloat(265))/2. + MMHFloat(10), CGRectGetMaxY(scanningTopBackgroundImageView.frame) + 2 * num , MMHFloat(265 - 20) , 1)];
        if (2 * num >= scanningMiddeMiddleBackgroundImageView.bounds.size.height) {
            upOrdown = YES;
        }
    } else {
        num--;
        [self.viewLine setFrame:CGRectMake((kScreenBounds.size.width - MMHFloat(265))/2. + MMHFloat(10), CGRectGetMaxY(scanningTopBackgroundImageView.frame) + 2 * num , MMHFloat(265 - 20), 1)];
        if (num == 0) {
            upOrdown = NO;
        }
    }
}

-(void)close{
    if (self.closeCameraBlock){
        self.closeCameraBlock();
    }
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
