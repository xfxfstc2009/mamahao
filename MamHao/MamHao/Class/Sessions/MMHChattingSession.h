//
//  MMHChattingSession.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@class MMHChattingSession;


@protocol MMHChattingSessionUnreadCountDelegate <NSObject>

- (void)chattingSessionUnreadCountChanged:(MMHChattingSession *)chattingSession;

@end



@interface MMHChattingSession : NSObject

+ (MMHChattingSession *)currentSession;

@property (nonatomic, weak) id<MMHChattingSessionUnreadCountDelegate> unreadCountDelegate;

- (NSInteger)unreadMessageCount;

@end
