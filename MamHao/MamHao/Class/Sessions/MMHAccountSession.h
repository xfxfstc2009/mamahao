//
//  MMHAccountSession.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@class MMHAccount;
@class MMHMemberCenterInfoModel;


@interface MMHAccountSession : NSObject

@property (nonatomic) BOOL isLoggingIn;

+ (MMHAccountSession *)currentSession;

+ (void)start;

- (void)startLogin;
- (void)accountDidLogin:(MMHAccount *)account;
- (BOOL)hasAnyAccountLoggedInOrLoggingIn;
- (BOOL)alreadyLoggedIn;

- (NSString *)memberID;
- (NSString *)phone;

- (NSString *)token;

- (void)logoutWithCompletion:(void (^)(BOOL succeeded))completion;

- (void)updateMemberInfoWithMemberCenterInfo:(MMHMemberCenterInfoModel *)memberCenterInfo;

- (NSString *)avatar;
@end


@interface MMHAccountSession (Cart)

@property (nonatomic) MMHID cartID;

@end


@interface MMHAccountSession (IM)

//@property (nonatomic) NSString *chattingUserID;

- (BOOL)isReadyToChat;
- (void)loginChattingAccountWithWaiterID:(NSString *)waiterID succeededHandler:(void (^)())succeededHandler failedHander:(void (^)(NSError *error))failedHandler;

+ (NSString *)chattingUserID;
+ (NSDictionary *)chattingUserInfo;
+ (void)setChattingUserInfo:(NSDictionary *)chattingUserInfo;
@end
