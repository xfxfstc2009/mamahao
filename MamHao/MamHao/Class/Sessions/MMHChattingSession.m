//
//  MMHChattingSession.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/25.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <EaseMobSDK/EaseMob.h>
#import "MMHChattingSession.h"
#import "MMHChattingViewController+Preparation.h"


@implementation MMHChattingSession


+ (MMHChattingSession *)currentSession
{
    static MMHChattingSession *_currentSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _currentSession = [[self alloc] init];
        [[EaseMob sharedInstance].chatManager addDelegate:(id <EMChatManagerDelegate>)_currentSession delegateQueue:nil];
    });
    return _currentSession;
}


- (NSInteger)unreadMessageCount
{
    id <IChatManager> chatManager = [EaseMob sharedInstance].chatManager;
    if (!chatManager.isLoggedIn) {
        return 0;
    }
    
    NSArray *conversations = chatManager.conversations;
    if (conversations.count == 0) {
        return 0;
    }

    NSInteger count = 0;
    for (EMConversation *conversation in conversations) {
        if ([MMHChattingViewController waiterID].length != 0) {
            if ([conversation.chatter isEqualToString:[MMHChattingViewController waiterID]]) {
                conversation.enableUnreadMessagesCountEvent = YES;
                count += conversation.unreadMessagesCount;
            }
        }
    }
    return count;
}


- (void)didUnreadMessagesCountChanged
{
    LZLog(@"unread message count changed");
    LZLog(@"count: %ld", [self unreadMessageCount]);
    id <MMHChattingSessionUnreadCountDelegate> delegate = self.unreadCountDelegate;
    if ([delegate respondsToSelector:@selector(chattingSessionUnreadCountChanged:)]) {
        [delegate chattingSessionUnreadCountChanged:self];
    }
}


- (void)didReceiveMessage:(EMMessage *)message
{
    LZLog(@"%s", sel_getName(_cmd));
}


- (void)willAutoLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    LZLog(@"%s", sel_getName(_cmd));
}


- (void)didAutoLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    LZLog(@"%s", sel_getName(_cmd));
    id <MMHChattingSessionUnreadCountDelegate> delegate = self.unreadCountDelegate;
    if ([delegate respondsToSelector:@selector(chattingSessionUnreadCountChanged:)]) {
        [delegate chattingSessionUnreadCountChanged:self];
    }
}


- (void)didLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    LZLog(@"%s", sel_getName(_cmd));
    id <MMHChattingSessionUnreadCountDelegate> delegate = self.unreadCountDelegate;
    if ([delegate respondsToSelector:@selector(chattingSessionUnreadCountChanged:)]) {
        [delegate chattingSessionUnreadCountChanged:self];
    }
}


@end
