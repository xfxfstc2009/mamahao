//
//  MMHAccountSession.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <EaseMobSDK/EaseMob.h>
#import "MMHAccountSession.h"
#import "MMHAccount.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Login.h"
#import "MMHNetworkAdapter+CommonSync.h"
#import "MMHNetworkAdapter+Chatting.h"
#import "MMHMemberCenterInfoModel.h"


#define MMHAccountSessionAccountPath [kPathDocuments stringByAppendingPathComponent:@"account.plist"]


NSString *const MMHAccountSessionChattingUserInfo = @"MMHAccountSessionChattingUserInfo";


@interface MMHAccountSession ()

@property (nonatomic, strong) MMHAccount *account;
@end


@implementation MMHAccountSession


+ (MMHAccountSession *)currentSession
{
    static MMHAccountSession *_currentSession = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _currentSession = [[self alloc] init];
    });
    return _currentSession;
}


+ (void)start
{
    MMHAccount *lastAccount = [self lastAccount];
    if (lastAccount == nil) {
        return;
    }
    
    if ([lastAccount isActivated]) {
        [self currentSession].account = lastAccount;
    }

//    if ([lastAccount shouldAutoLogin]) {
//        [self currentSession].account = lastAccount;
//        [[self currentSession] startLogin];
//        return;
//    }
}


- (void)startLogin
{
    self.isLoggingIn = YES;
    [[MMHNetworkAdapter sharedAdapter] loginWithPhoneNumber:self.account.phoneNumber
                                                   password:self.account.password
                                                       from:self
                                           succeededHandler:^(MMHAccount *account) {
                                           } failedHandler:^(NSError *error) {
                NSLog(@"login failed: %@", error);
            }];
}


- (void)saveLastAccount:(MMHAccount *)account
{
    if (account == nil) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:MMHAccountSessionAccountPath]) {
            [[NSFileManager defaultManager] removeItemAtPath:MMHAccountSessionAccountPath error:nil];
        }
        return;
    }

    NSString *path = MMHAccountSessionAccountPath;
    [NSKeyedArchiver archiveRootObject:account toFile:path];
}


+ (MMHAccount *)lastAccount
{
    NSString *path = MMHAccountSessionAccountPath;
    if (![[NSFileManager defaultManager] fileExistsAtPath:MMHAccountSessionAccountPath]) {
        return nil;
    }
    MMHAccount *account = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    return account;
}


- (void)accountDidLogin:(MMHAccount *)account
{
    [self saveLastAccount:account];
    self.account = account;
    self.isLoggingIn = NO;
}


- (BOOL)hasAnyAccountLoggedInOrLoggingIn
{
    if (!self.account) {
        return NO;
    }
    if ([self.account isActivated]) {
        return YES;
    }
    return self.isLoggingIn;
}


- (BOOL)alreadyLoggedIn
{
    if (!self.account) {
        return NO;
    }
    if ([self.account isActivated]) {
        return YES;
    }
    return !self.isLoggingIn;
}


- (NSString *)memberID
{
    if (self.account) {
        return self.account.memberID;
    }
    return nil;
}


- (NSString *)phone
{
    if (self.account) {
        return self.account.phoneNumber;
    }
    return nil;
}


- (NSString *)avatar {
    if (self.account) {
        return self.account.avatar;
    }
    return nil;
}


- (NSString *)token
{
    if (self.account) {
        return self.account.token;
    }
    return nil;
}
- (void)logoutWithCompletion:(void (^)(BOOL succeeded))completion
{
    self.account = nil;
    [self saveLastAccount:nil];
    [[self class] setChattingUserInfo:nil];
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
        if (completion) {
            completion(YES);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:MMHUserDidLogoutNotification
                                                            object:nil
                                                          userInfo:nil];
    } onQueue:nil];
}


- (void)updateMemberInfoWithMemberCenterInfo:(MMHMemberCenterInfoModel *)memberCenterInfo {
    if (!self.account) {
        return;
    }
    if (memberCenterInfo.memberId.length == 0) {
        return;
    }

    if (![self.account.memberID isEqualToString:memberCenterInfo.memberId]) {
        return;
    }

    self.account.avatar = memberCenterInfo.headPic;
    [self saveLastAccount:self.account];
}
@end


@implementation MMHAccountSession (Cart)


- (MMHID)cartID
{
    if (!self.account) {
        return 0;
    }
    
    if (self.isLoggingIn) {
        return 0;
    }
    
    return self.account.cartID;
}


- (void)setCartID:(MMHID)cartID
{
    if (!self.account) {
        return;
    }
    
    if (self.isLoggingIn) {
        return;
    }
    
    if (self.account.cartID != cartID) {
        self.account.cartID = cartID;
        [self saveLastAccount:self.account];
    }
}


@end


@implementation MMHAccountSession (IM)


//- (NSString *)chattingUserID
//{
//    if (!self.account) {
//        <#statements#>
//    }
//}


- (BOOL)isReadyToChat
{
    LZLog(@" login info is: %@", [[EaseMob sharedInstance].chatManager loginInfo]);
    return [EaseMob sharedInstance].chatManager.isLoggedIn;
}


- (void)loginChattingAccountWithWaiterID:(NSString *)waiterID succeededHandler:(void (^)())succeededHandler failedHander:(void (^)(NSError *error))failedHandler
{
    if (self.isReadyToChat) {
        succeededHandler();
        return;
    }

    NSDictionary *savedUserInfo = [[self class] chattingUserInfo];
    if (savedUserInfo.count == 0) {
        NSString *generatedUserID = [[self class] generateChattingUserID];
        [[EaseMob sharedInstance].chatManager asyncRegisterNewAccount:generatedUserID
                                                             password:generatedUserID
                                                       withCompletion:^(NSString *username, NSString *password, EMError *error) {
                                                           if (error) {
                                                               if (error.errorCode == EMErrorServerDuplicatedAccount) {
                                                                   [self loginChattingAccountWithUserName:username password:password waiterID:waiterID succeededHandler:succeededHandler failedHandler:failedHandler];
                                                                   return;
                                                               }
                                                               if (failedHandler) {
                                                                   NSError *e = [[NSError alloc] initWithDomain:@"EMError" code:error.errorCode userInfo:@{NSLocalizedDescriptionKey: [error description]}];
                                                                   failedHandler(e);
                                                               }
                                                               return;
                                                           }
//                                                           [[self class] setChattingUserID:username];
                                                           [self loginChattingAccountWithUserName:username password:password waiterID:waiterID succeededHandler:succeededHandler failedHandler:failedHandler];
//
                                                       } onQueue:nil];
    }
else {
    NSString *savedUserName = savedUserInfo[@"username"];
    NSString *savedPassword = savedUserInfo[@"password"];
        [self loginChattingAccountWithUserName:savedUserName
                                      password:savedPassword
                                      waiterID:waiterID
                              succeededHandler:succeededHandler
                                 failedHandler:failedHandler];
    }
//
//    [self loginChattingAccountWithUserName:userID
//                                  password:<#(NSString *)password#> succeededHandler:<#(void (^)())succeededHandler#> failedHandler:<#(void (^)(NSError *error))failedHandler#>];

}


- (void)loginChattingAccountWithUserName:(NSString *)userName password:(NSString *)password waiterID:(NSString *)waiterID succeededHandler:(void (^)())succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:userName
                                                        password:password
                                                      completion:^(NSDictionary *loginInfo, EMError *error) {
                                                          if (!error && loginInfo) {
                                                              [[self class] setChattingUserInfo:loginInfo];
                                                              [[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
                                                              [[MMHNetworkAdapter sharedAdapter] addFriendWithMyUserID:loginInfo[@"username"] waiterUserID:waiterID from:self succeededHandler:nil failedHandler:nil];
                                                              if (succeededHandler) {
                                                                  succeededHandler();
                                                              }
                                                          }
                                                          else {
                                                              if (failedHandler) {
                                                                  NSError *e = [[NSError alloc] initWithDomain:@"EMError" code:error.errorCode userInfo:@{NSLocalizedDescriptionKey: [error description]}];
                                                                  failedHandler(e);
                                                              }
                                                          }
                                                      } onQueue:nil];
}


+ (NSString *)chattingUserID
{
    NSDictionary *chattingUserInfo = [self chattingUserInfo];
    if (chattingUserInfo.count == 0) {
        return nil;
    }
    
    return chattingUserInfo[@"username"];
}


+ (NSDictionary *)chattingUserInfo
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:MMHAccountSessionChattingUserInfo];
}


+ (void)setChattingUserInfo:(NSDictionary *)chattingUserInfo {
    if (chattingUserInfo == nil) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:MMHAccountSessionChattingUserInfo];
        return;
    }
    [[NSUserDefaults standardUserDefaults] setObject:chattingUserInfo forKey:MMHAccountSessionChattingUserInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


+ (NSString *)generateChattingUserID
{
    NSString *memberID = [[self currentSession] memberID];
    if (memberID.length != 0) {
        return memberID;
    }

    NSString *string = [UIDevice deviceID];
    if ([string length] == 0) {
        string = [[[NSUUID UUID] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }

    string = [string stringByAppendingFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
    string = [string lowercaseString];
    return string;
}


@end
