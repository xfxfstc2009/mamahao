//
//  MMHMamhaoProductHeaderView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoProductHeaderView.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoDataManager.h"
#import "MamHao-Swift.h"
#import "MMHLogger.h"


@interface MMHMamhaoProductHeaderView () <HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate>

@property (nonatomic, strong) UIView *indicatorView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *monthAgeLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicatorImageView;
@property (nonatomic, strong) HTHorizontalSelectionList *horizontalSelectionListView;
@property (nonatomic, strong) MMHMamhaoData *mamhaoData;
@property (nonatomic) NSInteger selectedIndex;
@end


@implementation MMHMamhaoProductHeaderView


- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
    self.mamhaoData = mamhaoData;
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 40.0f)];
    [self.contentView addSubview:topView];
    topView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
    [topView addGestureRecognizer:tapGestureRecognizer];
    
    if (self.indicatorView == nil) {
        UIView *indicatorView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 6.0f, 20.0f)];
        indicatorView.backgroundColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:indicatorView];
        self.indicatorView = indicatorView;
    }
    
    if (self.titleLabel == nil) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.indicatorView.right + 5.0f, self.indicatorView.top, 0.0f, self.indicatorView.height)];
        titleLabel.textColor = [MMHAppearance blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [titleLabel setSingleLineText:@"为您优选"];
        titleLabel.centerY = titleLabel.centerY;
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;
    }
    
    if (self.disclosureIndicatorImageView == nil) {
        UIImageView *disclosureIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_bluedown"]];
        disclosureIndicatorImageView.centerY = self.indicatorView.centerY;
        [disclosureIndicatorImageView moveToRight:mmh_screen_width() - 10.0f];
        [self.contentView addSubview:disclosureIndicatorImageView];
        self.disclosureIndicatorImageView = disclosureIndicatorImageView;
    }
    
    if (self.monthAgeLabel == nil) {
        UILabel *monthAgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        monthAgeLabel.textColor = [UIColor colorWithHexString:@"136cbe"];
        monthAgeLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:monthAgeLabel];
        self.monthAgeLabel = monthAgeLabel;
    }

    NSString *monthAgeString = [mamhaoData.monthAgeForProducts displayName];
    [self.monthAgeLabel setSingleLineText:[NSString stringWithFormat:@"%@", monthAgeString]];
    self.monthAgeLabel.centerY = self.indicatorView.centerY;
    [self.monthAgeLabel attachToLeftSideOfView:self.disclosureIndicatorImageView byDistance:5.0f];

    if (self.horizontalSelectionListView == nil) {
        HTHorizontalSelectionList *horizontalSelectionListView = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0.0f, 40.0f, mmh_screen_width(), 35.0f)];
        horizontalSelectionListView.backgroundColor = [MMHAppearance backgroundColor];
        horizontalSelectionListView.bottomTrimHidden = YES;
        horizontalSelectionListView.dataSource = self;
        horizontalSelectionListView.delegate = self;
        [horizontalSelectionListView setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        horizontalSelectionListView.font = [UIFont systemFontOfSize:16.0f];
        horizontalSelectionListView.selectionIndicatorColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:horizontalSelectionListView];
        self.horizontalSelectionListView = horizontalSelectionListView;
    }
}


#pragma mark - HTHorizontalSelectionList data source and delegate methods


- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList
{
    return [[MMHMamhaoDataManager sharedDataManager] productTypeNames].count;
}


- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index
{
    return [[[MMHMamhaoDataManager sharedDataManager] productTypeNames] nullableObjectAtIndex:index];
}


- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
    MMHMamhaoProductType productType = (MMHMamhaoProductType)index;
    NSInteger productIndex = [[MMHMamhaoDataManager sharedDataManager] productIndexOfType:productType];
    if (productIndex == -1) {
        [selectionList setSelectedButtonIndex:self.selectedIndex animated:YES];
    }
    else {
        self.selectedIndex = index;
    }

    id<MMHMamhaoProductHeaderViewDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(productHeaderView:didSelectProductType:)]) {
        [delegate productHeaderView:self didSelectProductType:productType];
    }
}


#pragma mark - Actions


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
//    CGPoint location = [tapGestureRecognizer locationInView:tapGestureRecognizer.view];
//    if (self.monthAgeLabel) {
//        if ((location.x >= self.monthAgeLabel.left) && (location.y <= 40.0f)) {
            id<MMHMamhaoProductHeaderViewDelegate> delegate = self.delegate;
            if ([delegate respondsToSelector:@selector(productHeaderViewWillChangeMonthAge:)]) {
                [delegate productHeaderViewWillChangeMonthAge:self];
            }
//        }
//    }
}


- (void)setSelectedProductType:(MMHMamhaoProductType)type {
//    if (self.selectedIndex != type) {
        [self.horizontalSelectionListView setSelectedButtonIndex:type animated:NO];
//    }
}
@end
