//
//  MMHMamhaoFeaturedProductCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoFeaturedProductCell.h"
#import "MMHMamhaoProduct.h"
#import "MamHao-Swift.h"
#import "UIView+Extension.h"


@interface MMHMamhaoFeaturedProductCell ()

@property (nonatomic, strong) MMHImageView *productImageView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) NSMutableArray *productTagViews;
@property (nonatomic, strong) UIView *bottomLine;
@property (nonatomic, strong) MamhaoProductTagView *platformQualityInspectedView;
@property (nonatomic, strong) MamhaoProductTagView *shopIndicatorView;
@end


@implementation MMHMamhaoFeaturedProductCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


- (void)setProduct:(MMHMamhaoProduct *)product
{
    _product = product;
    
    if (self.productImageView == nil) {
        MMHImageView *productImageView = [[MMHImageView alloc] initWithFrame:CGRectMake(10.0f, 5.0f, mmh_screen_width() - 20.0f, MMHFloatWithPadding(175.0f, 20.0f))];
        [self.contentView addSubview:productImageView];
        self.productImageView = productImageView;
    }
    [self.productImageView updateViewWithImageAtURL:product.featuredImageURLString];

    if (self.priceLabel == nil) {
        UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, 0.0f, 0.0f)];
        priceLabel.textColor = [UIColor colorWithHexString:@"ea0f0f"];
        priceLabel.font = [F2 boldFont];
        [self.contentView addSubview:priceLabel];
        self.priceLabel = priceLabel;
    }
    [self.priceLabel setSingleLineText:[NSString stringWithPriceWithoutDecimal:product.price]];
    self.priceLabel.centerY = self.productImageView.bottom + MMHFloat(20.0f);
    
    if (self.productNameLabel == nil) {
        UILabel *productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.priceLabel.right + MMHFloat(12.0f), 0.0f, 0.0f, 0.0f)];
        productNameLabel.textColor = [UIColor colorWithHexString:@"333333"];
        productNameLabel.font = F4;
        [self.contentView addSubview:productNameLabel];
        self.productNameLabel = productNameLabel;
    }
    self.productNameLabel.left = self.priceLabel.right + 15.0f;
    [self.productNameLabel setSingleLineText:[product displayNameAsFeaturedProduct]];
    self.productNameLabel.centerY = self.priceLabel.centerY;
//    [self.productNameLabel setMaxX:245.0f]; // look down

    if (self.shopIndicatorView == nil) {
        MamhaoProductTagView *shopIndicatorView = [[MamhaoProductTagView alloc] initWithProductTag:MamhaoProductTagShop];
        [shopIndicatorView moveToRight:mmh_screen_width() - 10.0f];
        shopIndicatorView.centerY = self.productNameLabel.centerY;
        [self.contentView addSubview:shopIndicatorView];
        self.shopIndicatorView = shopIndicatorView;
    }
    self.shopIndicatorView.hidden = !product.isShop;

    if (self.platformQualityInspectedView == nil) {
        MamhaoProductTagView *platformQualityInspectedView = [[MamhaoProductTagView alloc] initWithProductTag:MamhaoProductTagQualityInspected];
        [platformQualityInspectedView moveToRight:self.shopIndicatorView.left - 7.0f];
        platformQualityInspectedView.centerY = self.productNameLabel.centerY;
        [self.contentView addSubview:platformQualityInspectedView];
        self.platformQualityInspectedView = platformQualityInspectedView;
    }
    self.platformQualityInspectedView.hidden = !product.platformQualityInspected;

    [self.productNameLabel setMaxX:self.platformQualityInspectedView.left - 5.0f];

//    for (MamhaoProductTagView *productTagView in self.productTagViews) {
//        [productTagView removeFromSuperview];
//    }
//    self.productTagViews = [NSMutableArray array];
//    CGFloat productTagViewX = 10.0f + MMHFloat(215.0f);
//    for (NSNumber *productTagNumber in product.tags) {
//        MamhaoProductTag productTag = (MamhaoProductTag)[productTagNumber integerValue];
//        MamhaoProductTagView *productTagView = [[MamhaoProductTagView alloc] initWithProductTag:productTag];
//        productTagView.left = productTagViewX;
//        productTagViewX = productTagView.right + MMHFloat(19.0f);
//        productTagView.centerY = self.priceLabel.centerY;
//        [self.contentView addSubview:productTagView];
//        [self.productTagViews addObject:productTagView];
//    }
    
    if (self.bottomLine == nil) {
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(10.0f, self.productImageView.bottom + MMHFloat(40.0f), mmh_screen_width() - 20.0f, 1.0f / mmh_screen_scale())];
        bottomLine.backgroundColor = [MMHAppearance blackColor];
        [self.contentView addSubview:bottomLine];
        self.bottomLine = bottomLine;
    }
}


+ (CGFloat)heightForProduct:(MMHMamhaoProduct *)product
{
    CGFloat height = 0.0f;
    height += 5.0f;
    height += MMHFloatWithPadding(175.0f, 20.0f);
    height += 40.0f;
    height += 5.0f;
    return height;
}


@end
