//
//  MMHMonthAgeSelectionView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MMHMonthAgeSelectionView.h"
#import "MamHao-Swift.h"


@interface MMHMonthAgeLabel: UILabel
@property (nonatomic, strong) MonthAge *monthAge;
@end

@implementation MMHMonthAgeLabel
@end


@interface MMHMonthAgeSelectionView ()

@property (nonatomic, strong) UIView *tipsView;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UIView *monthAgeLabelBackgroundView;
@property (nonatomic, strong) NSMutableArray *monthAgeLabels;
@property (nonatomic, strong) MonthAge *selectedMonthAge;
@end


@implementation MMHMonthAgeSelectionView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [C7 colorWithAlphaComponent:0.65f];
//        self.alpha = 0.65f;
//        UIView *backgroundView = [[UIView alloc] initWithFrame:self.bounds];
//        backgroundView.backgroundColor = [C7 colorWithAlphaComponent:0.95f];
//        [self addSubview:backgroundView];
    
        UIView *tipsView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bounds.size.width, (40.0f))];
        tipsView.backgroundColor = [UIColor whiteColor];
        [self addSubview:tipsView];
        self.tipsView = tipsView;
    
        UILabel *tipsLabel = [[UILabel alloc] initWithFrame:self.tipsView.bounds];
        tipsLabel.textColor = [UIColor colorWithHexString:@"888888"];
        tipsLabel.font = F5;
        tipsLabel.text = @"切换年龄";
        tipsLabel.textAlignment = NSTextAlignmentCenter;
        [self.tipsView addSubview:tipsLabel];
        self.tipsLabel = tipsLabel;

        [self.tipsView addBottomSeparatorLine];
    
        UIView *monthAgeLabelBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.tipsView.bottom, self.bounds.size.width, MMHFloat(88.0f))];
        monthAgeLabelBackgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.95];
        [self addSubview:monthAgeLabelBackgroundView];
        self.monthAgeLabelBackgroundView = monthAgeLabelBackgroundView;
    
        NSMutableArray *monthAgeLabels = [NSMutableArray array];
        NSArray *monthAges = @[@"-2", @"0-3", @"3-6", @"6-12", @"12-36", @"36-84", @"84-10000", @"-1-0"]; // TODO: - Louis - > 7 years old
        for (NSInteger i = 0; i < monthAges.count; i++) {
            NSString *monthAgeString = monthAges[i];
            if ([monthAgeString isKindOfClass:[NSNull class]]) {
                continue;
            }

            CGRect frame = [self frameOfMonthAgeAtIndex:i];
            MMHMonthAgeLabel *label = [[MMHMonthAgeLabel alloc] initWithFrame:frame];
            label.textColor = C5;
            label.font = F3;
            label.textAlignment = NSTextAlignmentCenter;
            MonthAge *monthAge = [[MonthAge alloc] initWithString:monthAgeString];
            label.text = [monthAge displayName];
            label.monthAge = monthAge;
            [self addSubview:label];
            [monthAgeLabels addObject:label];
        }
        self.monthAgeLabels = monthAgeLabels;

        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}


- (CGRect)frameOfMonthAgeAtIndex:(NSInteger)index
{
    CGFloat width = mmh_screen_width() / (4.0f);
    CGFloat height = MMHFloat(44.0f);
    CGRect result = CGRectMake(width * (CGFloat)(index % 4), height * (CGFloat)(index / 4) + self.tipsView.bottom, width, height);
    return result;
}


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    CGPoint location = [tapGestureRecognizer locationInView:self];
    if (CGRectContainsPoint(self.tipsView.frame, location)) {
        return;
    }

    MMHMonthAgeLabel *monthAgeLabel = [self.monthAgeLabels lastObject];
    if (location.y > monthAgeLabel.bottom) {
        LZLog(@"out out");
    }
    else {
        if (self.selectedMonthAge) {
            id <MMHMonthAgeSelectionViewDelegate> delegate = self.delegate;
            if ([delegate respondsToSelector:@selector(monthAgeSelectionView:didSelectMonthAge:)]) {
                [delegate monthAgeSelectionView:self didSelectMonthAge:self.selectedMonthAge];
            }
            self.selectedMonthAge = nil;
        }
    }

    [self removeFromSuperview];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];

    [self updateTextColorsWithLocation:location];
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];

    [self updateTextColorsWithLocation:location];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.selectedMonthAge) {
        id <MMHMonthAgeSelectionViewDelegate> delegate = self.delegate;
        if ([delegate respondsToSelector:@selector(monthAgeSelectionView:didSelectMonthAge:)]) {
            [delegate monthAgeSelectionView:self didSelectMonthAge:self.selectedMonthAge];
        }
        self.selectedMonthAge = nil;
        [self removeFromSuperview];
    }
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self];

    [self resetTextColorsWithLocation:location];
}


- (void)updateTextColorsWithLocation:(CGPoint)location
{
    self.selectedMonthAge = nil;
    for (MMHMonthAgeLabel *monthAgeLabel in self.monthAgeLabels) {
        if (CGRectContainsPoint(monthAgeLabel.frame, location)) {
            monthAgeLabel.textColor = C22;
            self.selectedMonthAge = monthAgeLabel.monthAge;
        }
        else {
            monthAgeLabel.textColor = C5;
        }
    }
}


- (void)resetTextColorsWithLocation:(CGPoint)location
{
    self.selectedMonthAge = nil;
    for (MMHMonthAgeLabel *monthAgeLabel in self.monthAgeLabels) {
        monthAgeLabel.textColor = C5;
    }
}


@end
