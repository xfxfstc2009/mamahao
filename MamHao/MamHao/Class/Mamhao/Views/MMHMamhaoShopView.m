//
//  MMHMamhaoShopView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoShopView.h"
#import "MMHMamhaoShop.h"


@interface MMHMamhaoShopView ()

@property (nonatomic, strong) MMHImageView *backgroundImageView;
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UILabel *shopNameLabel;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) NSMutableArray *displayedBrandArr;
@property (nonatomic, strong) NSMutableArray *noDisplayBrandArr;
@property (nonatomic, strong) NSMutableArray *brandViewArr;
@property (nonatomic) BOOL brandAnimationFlag;
@property (nonatomic) int changeIndex;

@end


@implementation MMHMamhaoShopView

- (id)initWithShop:(MMHMamhaoShop *)shop
{
    self = [self initWithFrame:MMHRectMake(0.0f, 0.0f, [[self class] defaultWidth], 250.0f)];
    if (self) {
        self.shop = shop;
        self.changeIndex = 0;
        [self configureViews];
    }
    return self;
}


- (void)configureViews
{
    MMHImageView *backgroundImageView = [[MMHImageView alloc] initWithFrame:self.bounds];
    [backgroundImageView updateViewWithImageAtURL:self.shop.imageURLString];
    [self addSubview:backgroundImageView];
    self.backgroundImageView = backgroundImageView;
    
    [self configureBrandViews];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.bounds.size.height - MMHFloat(30.0f), self.bounds.size.width, MMHFloat(30.0f))];
    titleView.backgroundColor = [[UIColor colorWithHexString:@"010101"] colorWithAlphaComponent:0.75f];
    [self addSubview:titleView];
    self.titleView = titleView;
    
    UILabel *shopNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(5.0f, 0.0f, 0.0f, titleView.bounds.size.height)];
    shopNameLabel.textColor = [UIColor whiteColor];
    shopNameLabel.font = [UIFont systemFontOfSize:15.0f];
    [titleView addSubview:shopNameLabel];
    self.shopNameLabel = shopNameLabel;
    
    UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 0.0f, titleView.bounds.size.height)];
    [distanceLabel moveToRight:(CGRectGetMaxX(self.titleView.bounds) - 5.0f)];
    distanceLabel.textColor = [UIColor whiteColor];
    distanceLabel.font = [UIFont systemFontOfSize:15.0f];
    distanceLabel.textAlignment = NSTextAlignmentRight;
    [titleView addSubview:distanceLabel];
    self.distanceLabel = distanceLabel;

    NSString *distanceString = [NSString stringWithFormat:@"距您<%ldm", (long)self.shop.distance];
    if (self.shop.distance >= 1000) {
        CGFloat km = (CGFloat)(self.shop.distance / 1000.0f);
        distanceString = [NSString stringWithFormat:@"距您<%.1fkm", km];
    }
    if (self.shop.cityName.length != 0) {
        distanceString = self.shop.cityName;
    }
    [self.distanceLabel setSingleLineText:distanceString keepingHeight:YES];
    NSString *shopNameString = self.shop.name;
    CGFloat shopNameLabelWidth = self.distanceLabel.left - 10.0f;
    [self.shopNameLabel setSingleLineText:shopNameString constrainedToWidth:shopNameLabelWidth keepingHeight:YES];
    [self.shopNameLabel setMaxX:self.distanceLabel.left - 5.0f];

    // TODO: - Louis - add the tag view
//    UIImageView *tagImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_mark"]];
//    tagImageView.x = -2.5f;
//    tagImageView.y = -2.5f;
//    [self addSubview:tagImageView];
}

- (void)configureBrandViews {
    self.displayedBrandArr = [[NSMutableArray alloc] init];
    self.brandViewArr = [[NSMutableArray alloc] init];
    
    CGFloat brandViewWidth = (self.bounds.size.width - MMHFloat(17.5f) * 2 - MMHFloat(10.0f) * 3) / 4;
    for (int i = 0; i < 4 && i < self.shop.brands.count; ++i) {
        MMHMamhaoShopBrand *brand = [self.shop.brands objectAtIndex:i];
        [self.displayedBrandArr addObject:brand];
        
        MMHImageView *brandView = [[MMHImageView alloc] initWithFrame:CGRectMake(MMHFloat(17.5f) + i * (brandViewWidth + MMHFloat(10.0f)), MMHFloat(26.0f), brandViewWidth, brandViewWidth)];
        brandView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.8];
        brandView.alpha = 0.8;
        [brandView updateViewWithImageAtURL:brand.imageURLString];
        [self addSubview:brandView];
        [self.brandViewArr addObject:brandView];
    }
    
    if (self.shop.brands.count > 4) {
        self.noDisplayBrandArr = [[NSMutableArray alloc] init];
        for (int i = 4; i < self.shop.brands.count; ++i) {
            [self.noDisplayBrandArr addObject:[self.shop.brands objectAtIndex:i]];
        }
        
        [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerChangeBrandView:) userInfo:nil repeats:NO];
    }
}

+ (CGFloat)defaultWidth
{
    return 325.0f;
}

#pragma mark - Action

- (void)timerChangeBrandView:(NSTimer *)timer {
    if (self.brandAnimationFlag == NO) {
        self.brandAnimationFlag = YES;
        NSArray *brands = self.shop.brands;
        if (brands.count == 5) {
            int toChangeIndex = arc4random() % 4;
            MMHImageView *toChangeBrandView = [self.brandViewArr objectAtIndex:toChangeIndex];
            MMHMamhaoShopBrand *toChangeBrand = [self.displayedBrandArr objectAtIndex:toChangeIndex];
            MMHMamhaoShopBrand *fromChangeBrand = self.noDisplayBrandArr.firstObject;
            
            [UIView animateWithDuration:2 animations:^{
                toChangeBrandView.alpha = 0;
                
                self.displayedBrandArr[toChangeIndex] = fromChangeBrand;
                [self.noDisplayBrandArr removeObject:fromChangeBrand];
                [self.noDisplayBrandArr addObject:toChangeBrand];
            } completion:^(BOOL finished) {
                [toChangeBrandView updateViewWithImageAtURL:fromChangeBrand.imageURLString];
                [UIView animateWithDuration:2 animations:^{
                    toChangeBrandView.alpha = 0.8;
                } completion:^(BOOL finished) {
                    self.brandAnimationFlag = NO;
                    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerChangeBrandView:) userInfo:nil repeats:NO];
                }];
            }];
        } else {
            int toChangeIndex1 = self.changeIndex;
            int toChangeIndex2 = self.changeIndex + 2;
            
            int fromChangeIndex1 = arc4random() % self.noDisplayBrandArr.count;
            int fromChangeIndex2 = arc4random() % self.noDisplayBrandArr.count;
            if (fromChangeIndex2 == fromChangeIndex1) {
                if (fromChangeIndex2 != self.noDisplayBrandArr.count - 1) {
                    fromChangeIndex2 += 1;
                } else {
                    fromChangeIndex2 -= 1;
                }
            }
            MMHImageView *toChangeBrandView1 = self.brandViewArr[toChangeIndex1];
            MMHImageView *tochangeBrandView2 = self.brandViewArr[toChangeIndex2];
            MMHMamhaoShopBrand *toChangeBrand1 = self.displayedBrandArr[toChangeIndex1];
            MMHMamhaoShopBrand *toChangeBrand2 = self.displayedBrandArr[toChangeIndex2];
            MMHMamhaoShopBrand *fromChangeBrand1 = self.noDisplayBrandArr[fromChangeIndex1];
            MMHMamhaoShopBrand *fromChangeBrand2 = self.noDisplayBrandArr[fromChangeIndex2];
            
            [UIView animateWithDuration:2 animations:^{
                toChangeBrandView1.alpha = 0;
                tochangeBrandView2.alpha = 0;
                
                self.displayedBrandArr[toChangeIndex1] = fromChangeBrand1;
                [self.noDisplayBrandArr removeObject:fromChangeBrand1];
                [self.noDisplayBrandArr addObject:toChangeBrand1];
                
                self.displayedBrandArr[toChangeIndex2] = fromChangeBrand2;
                [self.noDisplayBrandArr removeObject:fromChangeBrand2];
                [self.noDisplayBrandArr addObject:toChangeBrand2];
            } completion:^(BOOL finished) {
                [toChangeBrandView1 updateViewWithImageAtURL:fromChangeBrand1.imageURLString];
                [tochangeBrandView2 updateViewWithImageAtURL:fromChangeBrand2.imageURLString];
                
                [UIView animateWithDuration:2 animations:^{
                    toChangeBrandView1.alpha = 0.8;
                    tochangeBrandView2.alpha = 0.8;
                } completion:^(BOOL finished) {
                    if (self.changeIndex == 0) {
                        self.changeIndex = 1;
                    } else {
                        self.changeIndex = 0;
                    }
                    self.brandAnimationFlag = NO;
                    [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerChangeBrandView:) userInfo:nil repeats:NO];
                }];
            }];
        }
    }
}

@end
