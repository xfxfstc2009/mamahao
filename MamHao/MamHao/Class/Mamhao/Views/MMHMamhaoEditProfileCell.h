//
//  MMHMamhaoEditProfileCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoPromotionCell.h"


@interface MMHMamhaoEditProfileCell : MMHMamhaoPromotionCell

+ (BOOL)childConfiguredWithMamhaoData:(MMHMamhaoData *)mamhaoData;

@end
