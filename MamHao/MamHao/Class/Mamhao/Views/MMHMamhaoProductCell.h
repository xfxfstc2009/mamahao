//
//  MMHMamhaoProductCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoProduct;
@class MMHMamhaoProductCell;


@protocol MMHMamhaoProductCellDelegate <NSObject>

- (void)mamhaoProductCellWillAddToCart:(MMHMamhaoProductCell *)mamhaoProductCell;

@end


@interface MMHMamhaoProductCell : UITableViewCell

@property (nonatomic, weak) id<MMHMamhaoProductCellDelegate> delegate;

@property (nonatomic, strong) MMHMamhaoProduct *product;

+ (CGFloat)heightForProduct:(MMHMamhaoProduct *)product;
@end
