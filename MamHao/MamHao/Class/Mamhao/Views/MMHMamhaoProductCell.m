//
//  MMHMamhaoProductCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoProductCell.h"
#import "MMHMamhaoProduct.h"
#import "MamHao-Swift.h"


@interface MMHMamhaoProductCell ()

@property (nonatomic, strong) MMHImageView *productImageView;
@property (nonatomic, strong) UIView *separatorLine;
@property (nonatomic, strong) UILabel *productNameLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *soldCountLabel;
@property (nonatomic, strong) UIButton *cartButton;
@property (nonatomic, strong) UIView *bottomSeparatorView;
@property (nonatomic, strong) NSMutableArray *productTagViews;
@property (nonatomic, strong) UIImageView *locationImageView;
@property (nonatomic, strong) UILabel *distanceLabel;
@property (nonatomic, strong) MamhaoProductTagView *platformQualityInspectedView;
@property (nonatomic, strong) MamhaoProductTagView *shopIndicatorView;
@end


@implementation MMHMamhaoProductCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


- (void)setProduct:(MMHMamhaoProduct *)product
{
    _product = product;

    if (self.productImageView == nil) {
        MMHImageView *productImageView = [[MMHImageView alloc] initWithFrame:MMHRectMake(15.0f, 0.0f, 125.0f, 125.0f)];
        [self.contentView addSubview:productImageView];
        self.productImageView = productImageView;
    }
    [self.productImageView updateViewWithImageAtURL:product.imageURLString];

//    if (self.locationImageView == nil) {
//        UIImage *locationIndicatorImage = [UIImage imageNamed:@"home_icon_location"];
//        UIImageView *locationImageView = [[UIImageView alloc] initWithImage:locationIndicatorImage];
//        locationImageView.left = 10.0f;
//        [locationImageView moveToBottom:MMHFloat(125.0f) - 3.0f];
//        [self.contentView addSubview:locationImageView];
//        self.locationImageView = locationImageView;
//    }
//
//    if (self.distanceLabel == nil) {
//        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.locationImageView.right + 2.0f, 0.0f, 0.0f, 0.0f)];
//        distanceLabel.textColor = [UIColor colorWithHexString:@"999999"];
//        distanceLabel.font = [UIFont systemFontOfSize:9.0f];
//        [self.contentView addSubview:distanceLabel];
//        self.distanceLabel = distanceLabel;
//    }
//    [self.distanceLabel setSingleLineText:[NSString stringWithFormat:@"距您%ldm", (long)product.distance]];
//    self.distanceLabel.centerY = self.locationImageView.centerY;
    
    if (self.separatorLine == nil) {
        UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(self.productImageView.right, 0.0f, 1.0f, MMHFloat(135.0f))];
        separatorLine.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_line_dashline"]];
        [self.contentView addSubview:separatorLine];
        self.separatorLine = separatorLine;
    }

    if (self.productNameLabel == nil) {
        UILabel *productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.productImageView.right + MMHFloat(17.0f), MMHFloat(15.0f), 0.0f, 0.0f)];
        productNameLabel.textColor = [UIColor colorWithHexString:@"333333"];
        productNameLabel.font = [UIFont boldSystemFontOfCustomeSize:14.0f];
        [productNameLabel setMaxX:(mmh_screen_width() - MMHFloat(15.0f))];
        [self.contentView addSubview:productNameLabel];
        self.productNameLabel = productNameLabel;
    }
    [self.productNameLabel setText:product.name constrainedToLineCount:2];

    if (self.priceLabel == nil) {
        UILabel *priceLabel = [[UILabel alloc] initWithFrame:MMHRectMake(0.0f, 60.0f, 0.0f, 0.0f)];
        priceLabel.left = self.productNameLabel.left;
        priceLabel.textColor = [UIColor colorWithHexString:@"cc0000"];
        priceLabel.font = [UIFont boldSystemFontOfCustomeSize:14.0f];
        [self.contentView addSubview:priceLabel];
        self.priceLabel = priceLabel;
    }
    NSString *priceString = [NSString stringWithPrice:product.price];
    [self.priceLabel setSingleLineText:priceString];
    [self.priceLabel sizeToFit];

    if (self.soldCountLabel == nil) {
        UILabel *soldCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        soldCountLabel.textColor = [UIColor colorWithHexString:@"999999"];
        soldCountLabel.font = MMHFontOfSize(9.0f);
        [self.contentView addSubview:soldCountLabel];
        self.soldCountLabel = soldCountLabel;
    }
    [self.soldCountLabel setSingleLineText:[NSString stringWithFormat:@"已售%ld件", product.soldCount]];
    [self.soldCountLabel sizeToFit];
    [self.soldCountLabel attachToRightSideOfView:self.priceLabel byDistance:MMHFloat(7.0f)];
    [self.soldCountLabel moveToBottom:self.priceLabel.bottom - 2.0f];

//    if (self.cartButton == nil) {
//        UIImage *cartImage = [UIImage imageNamed:@"home_icon_redshop"];
//        UIButton *cartButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, cartImage.size.width, cartImage.size.height)];
//        cartButton.top = MMHFloat(64.0f);
//        [cartButton moveToRight:mmh_screen_width() - MMHFloat(26.0f)];
//        [cartButton setImage:cartImage forState:UIControlStateNormal];
//        [cartButton addTarget:self action:@selector(cartButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:cartButton];
//        self.cartButton = cartButton;
//    }

    if (self.platformQualityInspectedView == nil) {
        MamhaoProductTagView *platformQualityInspectedView = [[MamhaoProductTagView alloc] initWithProductTag:MamhaoProductTagQualityInspected];
        platformQualityInspectedView.left = self.productNameLabel.left + MMHFloat(4.0f);
        platformQualityInspectedView.top = self.productImageView.bottom - MMHFloat(5.0f) - platformQualityInspectedView.height;
        [self.contentView addSubview:platformQualityInspectedView];
        self.platformQualityInspectedView = platformQualityInspectedView;
    }
    self.platformQualityInspectedView.hidden = !product.platformQualityInspected;

    if (self.shopIndicatorView == nil) {
        MamhaoProductTagView *shopIndicatorView = [[MamhaoProductTagView alloc] initWithProductTag:MamhaoProductTagShop];
        shopIndicatorView.left = self.platformQualityInspectedView.right + MMHFloat(18.0f);
        shopIndicatorView.top = self.productImageView.bottom - MMHFloat(5.0f) - shopIndicatorView.height;
        [self.contentView addSubview:shopIndicatorView];
        self.shopIndicatorView = shopIndicatorView;
    }
    self.shopIndicatorView.hidden = !product.isShop;

    UIView *bottomSeparatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.productImageView.bottom, mmh_screen_width(), 10.0f)];
    bottomSeparatorView.backgroundColor = [MMHAppearance backgroundColor];
    [self.contentView addSubview:bottomSeparatorView];
    self.bottomSeparatorView = bottomSeparatorView;
}


+ (CGFloat)heightForProduct:(MMHMamhaoProduct *)product
{
    if (product == nil) {
        return 0.0f;
    }
    return MMHFloat(125.0f) + 10.0f;
}


- (void)cartButtonTapped:(UIButton *)cartButton
{
    id <MMHMamhaoProductCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(mamhaoProductCellWillAddToCart:)]) {
        [delegate mamhaoProductCellWillAddToCart:self];
    }
}


@end
