//
//  MamhaoProductTagView.swift
//  MamHao
//
//  Created by Louis Zhu on 15/6/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


@objc
enum MamhaoProductTag: Int {
    case QualityInspected
    case Shop
}


extension MamhaoProductTag {
    func imageName() -> String {
        switch self {
        case .QualityInspected:
            return "home_icon_qc"
        case .Shop:
            return "home_icon_productTag_shop"
        }
    }
}


extension MamhaoProductTag {
    func displayName() -> String {
        switch self {
        case .QualityInspected:
            return "平台质检"
        case .Shop:
            return "实体店"
        }
    }
}


class MamhaoProductTagView: UIView {
    
    var productTag: MamhaoProductTag?
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(productTag: MamhaoProductTag) {
        self.productTag = productTag;
        super.init(frame: CGRectZero)
        
        self.configureViews()
    }
    
    
    func configureViews() {
        if let productTag = self.productTag {
            let imageName = productTag.imageName()
            let image = UIImage(named: imageName)
            let imageView = UIImageView(image: image)
            self.addSubview(imageView)
            
            let displayName = productTag.displayName()
            let label = UILabel()
            label.textColor = UIColor(hexString: "666666")
            label.font = UIFont.systemFontOfSize(10)
            label.setSingleLineText(displayName)
            label.setX(imageView.right() + 4)
            label.setCenterY(imageView.centerY())
            self.addSubview(label)
            
            self.frame = CGRectMake(0, 0, label.right(), imageView.height())
        }
    }

}
