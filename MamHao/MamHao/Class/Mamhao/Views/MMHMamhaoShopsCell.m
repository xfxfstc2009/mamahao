//
//  MMHMamhaoShopsCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoShopsCell.h"
#import "MMHMamhaoShop.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoShopView.h"
#import "MMHMamhaoCustomersView.h"
#import "UIView+Extension.h"
#import "MMHConstants.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Mamhao.h"
#import "MMHMamhaoShopCustomersInfo.h"


@interface MMHMamhaoShopsCell () <UIScrollViewDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) NSMutableArray *shopViews;
@property (nonatomic, strong) UIScrollView *shopsScrollView;
@property (nonatomic, strong) MMHMamhaoCustomersView *customersView;
@property (nonatomic, strong) UIImageView *disclosureIndicator;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic, strong) MMHMamhaoData *mamhaoData;
@end


@implementation MMHMamhaoShopsCell


- (void)setCurrentPage:(NSInteger)currentPage
{
    if (_currentPage == currentPage) {
        return;
    }

    _currentPage = currentPage;

    [self fetchCustomers];
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];

        UIView *indicatorView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 6.0f, 20.0f)];
        indicatorView.backgroundColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:indicatorView];
    
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(indicatorView.right + 5.0f, indicatorView.top, 0.0f, indicatorView.height)];
        titleLabel.textColor = [MMHAppearance blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [titleLabel setSingleLineText:@"附近实体店"];
        titleLabel.centerY = titleLabel.centerY;
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;
    
        UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.titleLabel.right + 5.0f, 0.0f, 0.0f, 0.0f)];
        addressLabel.textColor = [UIColor colorWithHexString:@"666666"];
        addressLabel.font = [UIFont systemFontOfSize:14.0f];
        [addressLabel setSingleLineText:@"定位中..."];
        addressLabel.centerY = self.titleLabel.centerY;
        addressLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressLabelTapped:)];
        [addressLabel addGestureRecognizer:tapGestureRecognizer];
        [self.contentView addSubview:addressLabel];
        self.addressLabel = addressLabel;

        UIImageView *disclosureIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_down"]];
        [disclosureIndicator attachToRightSideOfView:self.addressLabel byDistance:1.0f];
        disclosureIndicator.centerY = self.addressLabel.centerY;
        [self.contentView addSubview:disclosureIndicator];
        self.disclosureIndicator = disclosureIndicator;
    
        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.contentView.bounds.size.height - 15.0f, mmh_screen_width(), 15.0f)];
        separatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        separatorView.backgroundColor = [MMHAppearance backgroundColor];
        [self.contentView addSubview:separatorView];
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.shops.count == 0) {
        self.contentView.hidden = YES;
        return;
    }
    else {
        self.mamhaoData = mamhaoData;
        self.contentView.hidden = NO;
    }
    
    NSString *street = [[MMHCurrentLocationModel sharedLocation] streetAddress];
    [self.addressLabel setSingleLineText:street];
    CGFloat maxX = mmh_screen_width() - 11.0f - self.disclosureIndicator.width;
    [self.addressLabel setMaxX:MIN(maxX, self.addressLabel.right)];

    [self.disclosureIndicator attachToRightSideOfView:self.addressLabel byDistance:1.0f];
    
    [self.shopsScrollView removeAllSubviews];
    [self.shopViews removeAllObjects];

    if (self.shopsScrollView == nil) {
        UIScrollView *shopsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 40.0f, mmh_screen_width(), MMHFloat(250.0f))];
        shopsScrollView.delegate = self;
        [self.contentView addSubview:shopsScrollView];
        self.shopsScrollView = shopsScrollView;
    }

    NSMutableArray *shopViews = [NSMutableArray array];
    CGFloat x = 10.0f;
    for (MMHMamhaoShop *shop in mamhaoData.shops) {
        MMHMamhaoShopView *shopView = [[MMHMamhaoShopView alloc] initWithShop:shop];
        shopView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shopViewTapped:)];
        [shopView addGestureRecognizer:tapGestureRecognizer];
        [self.shopsScrollView addSubview:shopView];
        [shopViews addObject:shopView];
        shopView.left = x;
        x += (shopView.width + 10.0f);
    }
    self.shopsScrollView.contentSize = CGSizeMake(x, self.shopsScrollView.bounds.size.height);
    self.shopViews = shopViews;
    
    if (self.customersView == nil) {
        MMHMamhaoCustomersView *customersView = [[MMHMamhaoCustomersView alloc] initWithFrame:CGRectMake(0.0f, self.shopsScrollView.bottom, mmh_screen_width(), MMHFloat(130.0f))];
        [self.contentView addSubview:customersView];
        self.customersView = customersView;
    }
//    [self.customersView updateViewsWithMamhaoData:mamhaoData];
    [self fetchCustomers];
}


- (void)addressLabelTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    id <MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidSelectLocation:)]) {
        [delegate mamhaoPromotionCellDidSelectLocation:self];
    }
}


- (void)shopViewTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    MMHMamhaoShopView *shopView = (MMHMamhaoShopView *)tapGestureRecognizer.view;
    MMHMamhaoShop *shop = shopView.shop;

    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(mamhaoPromotionCell:didSelectShop:)]) {
        [delegate mamhaoPromotionCell:self didSelectShop:shop];
    }
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.shops.count == 0) {
        return 0.0f;
    }

    CGFloat height = 0.0f;
    height += 40.0f;
    height += MMHFloat(250.0f);
    height += MMHFloat(130.0f);
    height += 15.0f;
    return height;
}


#pragma mark - UIScrollView delegate


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat widthPerPage = [MMHMamhaoShopView defaultWidth] + 10.0f;
    CGFloat pageNumber = scrollView.contentOffset.x / widthPerPage;
    CGFloat page = floorf(pageNumber);
    if (pageNumber - page > 0.5f) {
        page += 1.0f;
    }
    NSInteger integerPage = (NSInteger)page;
    self.currentPage = integerPage;
//    CGFloat offsetX = page * widthPerPage;
//    [scrollView setContentOffset:CGPointMake(offsetX, 0.0f) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self adjustShopViewPosition:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (decelerate == NO) {
        [self adjustShopViewPosition:scrollView];
    }
}

- (void)adjustShopViewPosition:(UIScrollView *)scrollView {
    CGFloat shopViewWidth = MMHFloat([MMHMamhaoShopView defaultWidth]);
    int baseIndex = (int)scrollView.contentOffset.x / (10 + (int)shopViewWidth);
    int baseX = (baseIndex + 1) * (10 + shopViewWidth) - scrollView.contentOffset.x;
    if (baseX + 5 < kScreenWidth / 2.0) {
        CGFloat x = (baseIndex + 1) * (10 + shopViewWidth);
        if (x > scrollView.contentSize.width - kScreenWidth) {
            x = scrollView.contentSize.width - kScreenWidth;
        }
        [scrollView setContentOffset:CGPointMake(x, 0) animated:YES];
    } else {
        [scrollView setContentOffset:CGPointMake(baseIndex * (10 + shopViewWidth), 0) animated:YES];
    }
}

- (void)fetchCustomers
{
    MMHMamhaoShop *shop = [self.mamhaoData.shops nullableObjectAtIndex:self.currentPage];
    if (shop == nil) {
        return;
    }

    __weak __typeof(self) weakSelf = self;
    [[MMHMamhaoShopCustomersInfo sharedInfo] fetchMamhaoCustomersNearShop:shop
                                                         completion:^(BOOL succeeded, long count, NSArray *customers, NSError *error) {
                                                             if (succeeded) {
                                                                 [weakSelf.customersView updateViewsWithCount:count customers:customers shop:shop];
                                                             }
                                                         }];
}


@end
