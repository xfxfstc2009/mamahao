//
//  MMHMamhaoPromotionCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, MMHMamhaoPromotionCellRow) {
    MMHMamhaoPromotionCellRowSearchBar = 0,
    MMHMamhaoPromotionCellRowAnnouncement,
    MMHMamhaoPromotionCellRowGames,
    MMHMamhaoPromotionCellRowBeanSpecial,
    MMHMamhaoPromotionCellRowShops,
//    MMHMamhaoPromotionCellRowMamhaoFeatured,
    MMHMamhaoPromotionCellRowEditProfile,
    MMHMamhaoPromotionCellRowUserFeatured,

    MMHMamhaoPromotionCellRowMinimum = MMHMamhaoPromotionCellRowSearchBar,
    MMHMamhaoPromotionCellRowMaximum = MMHMamhaoPromotionCellRowUserFeatured,
};


@class MMHMamhaoDataManager;
@class MMHMamhaoData;
@class MMHMamhaoPromotionCell;
@class MMHMamhaoShop;
@class MMHMamhaoUserFeaturedProduct;


@protocol MMHMamhaoPromotionCellDelegate <NSObject>

- (void)mamhaoPromotionCellDidBeginSearching:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;

//- (void)mamhaoPromotionCellDidSelectAnnouncement:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;

- (void)mamhaoPromotionCellDidSelectBeanGame:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;
- (void)mamhaoPromotionCellDidSelectRedEnvelopGame:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;
- (void)mamhaoPromotionCellDidSelectCategory:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;

- (void)mamhaoPromotionCellDidSelectBeanGuide:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;

- (void)mamhaoPromotionCellDidSelectLocation:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;
- (void)mamhaoPromotionCell:(MMHMamhaoPromotionCell *)mamhaoPromotionCell didSelectShop:(MMHMamhaoShop *)shop;
- (void)productHeaderViewDidSelectMonthAge:(MMHMamhaoPromotionCell *)mamhaoPromotionCell;
- (void)mamhaoPromotionCell:(MMHMamhaoPromotionCell *)mamhaoPromotionCell didSelectUserFeaturedProduct:(MMHMamhaoUserFeaturedProduct *)product;

@end



@protocol MMHMamhaoPromotionCell <NSObject>

+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData;
- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData;

@end


@interface MMHMamhaoPromotionCell : UITableViewCell <MMHMamhaoPromotionCell>

@property (nonatomic, weak) id<MMHMamhaoPromotionCellDelegate> delegate;

+ (Class<MMHMamhaoPromotionCell>)classForRow:(MMHMamhaoPromotionCellRow)row;

+ (void)registerCellClassesForTableView:(UITableView *)tableView;

+ (NSString *)cellIdentifierForRow:(MMHMamhaoPromotionCellRow)row;

@end
