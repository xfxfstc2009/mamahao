//
//  MMHMamhaoShopView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoShop;


@interface MMHMamhaoShopView : UIView

@property (nonatomic, strong) MMHMamhaoShop *shop;

- (id)initWithShop:(MMHMamhaoShop *)shop;

+ (CGFloat)defaultWidth;
@end
