//
//  MMHMamhaoPromotionCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoPromotionCell.h"
#import "MMHMamhaoDataManager.h"
#import "MMHMamhaoSearchBarCell.h"
#import "MMHMamhaoAnnouncementCell.h"
#import "MMHMamhaoGamesCell.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHMamhaoMBeanSpecialCell.h"
#import "MMHMamhaoShopsCell.h"
#import "MMHMamhaoMamhaoFeaturedCell.h"
#import "MMHMamhaoUserFeaturedCell.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoShop.h"
#import "MMHMamhaoUserFeaturedProduct.h"
#import "MMHMamhaoEditProfileCell.h"


@implementation MMHMamhaoPromotionCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [MMHAppearance backgroundColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    return 0.0f;
}


+ (Class <MMHMamhaoPromotionCell>)classForRow:(MMHMamhaoPromotionCellRow)row
{
    switch (row) {
        case MMHMamhaoPromotionCellRowSearchBar:
            return [MMHMamhaoSearchBarCell class];
            break;
        case MMHMamhaoPromotionCellRowAnnouncement:
            return [MMHMamhaoAnnouncementCell class];
            break;
        case MMHMamhaoPromotionCellRowGames:
            return [MMHMamhaoGamesCell class];
            break;
        case MMHMamhaoPromotionCellRowBeanSpecial:
            return [MMHMamhaoMBeanSpecialCell class];
            break;
        case MMHMamhaoPromotionCellRowShops:
            return [MMHMamhaoShopsCell class];
            break;
//        case MMHMamhaoPromotionCellRowMamhaoFeatured:
//            return [MMHMamhaoMamhaoFeaturedCell class];
//            break;
        case MMHMamhaoPromotionCellRowEditProfile:
            return [MMHMamhaoEditProfileCell class];
        case MMHMamhaoPromotionCellRowUserFeatured:
            return [MMHMamhaoUserFeaturedCell class];
            break;
        default:
            break;
    }
    return self;
}


+ (void)registerCellClassesForTableView:(UITableView *)tableView
{
    for (MMHMamhaoPromotionCellRow row = MMHMamhaoPromotionCellRowMinimum; row <= MMHMamhaoPromotionCellRowMaximum; row++) {
        [tableView registerClass:[self classForRow:row] forCellReuseIdentifier:[self cellIdentifierForRow:row]];
    }
}


+ (NSString *)cellIdentifierForRow:(MMHMamhaoPromotionCellRow)row
{
    return [NSString stringWithFormat:@"MMHMamhaoPromotionCellIdentifier%ld", (long)row];
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{

}
@end
