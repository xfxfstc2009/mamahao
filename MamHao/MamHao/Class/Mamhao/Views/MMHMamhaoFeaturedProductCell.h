//
//  MMHMamhaoFeaturedProductCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoProduct;


@interface MMHMamhaoFeaturedProductCell : UITableViewCell

@property (nonatomic, strong) MMHMamhaoProduct *product;

+ (CGFloat)heightForProduct:(MMHMamhaoProduct *)product;
@end
