//
//  MMHMamhaoSearchBarCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHMamhaoPromotionCell.h"

@class MMHSearchViewController;
@protocol MMHMamahaoDelegate <NSObject>

-(void)mamahaoViewController:(MMHSearchViewController *)mamahaoViewController;

@end

@interface MMHMamhaoSearchBarCell : MMHMamhaoPromotionCell

@property (nonatomic, assign)id<MMHMamahaoDelegate> searchDelegate;

@end
