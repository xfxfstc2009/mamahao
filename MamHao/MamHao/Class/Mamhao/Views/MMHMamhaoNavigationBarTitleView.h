//
//  MMHMamhaoNavigationBarTitleView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoNavigationBarTitleView;


@protocol MMHMamhaoNavigationBarTitleViewDelegate <NSObject>

- (void)titleViewTapped:(MMHMamhaoNavigationBarTitleView *)titleView;

@end



@interface MMHMamhaoNavigationBarTitleView : UIView

@property (nonatomic, weak) id<MMHMamhaoNavigationBarTitleViewDelegate> delegate;

@property (nonatomic, copy) NSString *title;

- (instancetype)initWithTitle:(NSString *)title;
@end
