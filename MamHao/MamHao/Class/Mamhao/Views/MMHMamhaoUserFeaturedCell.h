//
//  MMHMamhaoUserFeaturedCell.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHMamhaoPromotionCell.h"


@class MMHMamhaoData;
@class MMHMamhaoUserFeaturedData;
@class MonthAge;


@interface MMHMamhaoUserFeaturedCell : MMHMamhaoPromotionCell

@end
