//
//  MMHMamhaoProductHeaderView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/6.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHMamhaoDataManager.h"
#import "MMHLogger.h"


@class MMHMamhaoData;
@class MMHMamhaoProductHeaderView;


@protocol MMHMamhaoProductHeaderViewDelegate <NSObject>

- (void)productHeaderViewWillChangeMonthAge:(MMHMamhaoProductHeaderView *)mamhaoProductHeaderView;
- (void)productHeaderView:(MMHMamhaoProductHeaderView *)mamhaoProductHeaderView didSelectProductType:(MMHMamhaoProductType)productType;

@end


@interface MMHMamhaoProductHeaderView : UITableViewHeaderFooterView

@property (nonatomic, weak) id<MMHMamhaoProductHeaderViewDelegate> delegate;

- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData;

- (void)setSelectedProductType:(MMHMamhaoProductType)type;
@end
