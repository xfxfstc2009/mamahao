//
//  MMHMonthAgeSelectionView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMonthAgeSelectionView;
@class MonthAge;


@protocol MMHMonthAgeSelectionViewDelegate <NSObject>

- (void)monthAgeSelectionView:(MMHMonthAgeSelectionView *)monthAgeSelectionView didSelectMonthAge:(MonthAge *)monthAge;

@end


@interface MMHMonthAgeSelectionView : UIView

@property (nonatomic, weak) id<MMHMonthAgeSelectionViewDelegate> delegate;

@end
