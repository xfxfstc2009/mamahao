//
//  MMHMamhaoMamhaoFeaturedCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoMamhaoFeaturedCell.h"
#import "MMHMamhaoData.h"


@interface MMHMamhaoMamhaoFeaturedCell ()

@property (nonatomic, strong) MMHMamhaoData *mamhaoData;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) MMHImageView *backgroundImageView;
@end


@implementation MMHMamhaoMamhaoFeaturedCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        UIView *indicatorView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 6.0f, 20.0f)];
        indicatorView.backgroundColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:indicatorView];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(indicatorView.right + 5.0f, indicatorView.top, 0.0f, indicatorView.height)];
        titleLabel.textColor = [MMHAppearance blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [titleLabel setSingleLineText:@"万众瞩目"];
        titleLabel.centerY = titleLabel.centerY;
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;

//        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.contentView.bounds.size.height - 15.0f, mmh_screen_width(), 15.0f)];
//        separatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
//        separatorView.backgroundColor = [MMHAppearance backgroundColor];
//        [self.contentView addSubview:separatorView];
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (self.mamhaoData == mamhaoData) {
        return;
    }
    
    self.mamhaoData = mamhaoData;
    
    MMHMamhaoMamhaoFeaturedProduct *product = [mamhaoData.mamhaoFeatured firstObject];
    
    if (product == nil) {
        self.contentView.hidden = YES;
        return;
    }
    else {
        self.contentView.hidden = NO;
    }

    if (self.backgroundImageView == nil) {
        MMHImageView *backgroundImageView = [[MMHImageView alloc] initWithFrame:CGRectMake(10.0f, 40.0f, mmh_screen_width() - 20.0f, MMHFloatWithPadding(200.0f, 20.0f))];
        [backgroundImageView setBorderColor:[MMHAppearance separatorColor] cornerRadius:2.0f];
        [self.contentView addSubview:backgroundImageView];
        self.backgroundImageView = backgroundImageView;
    }
    [self.backgroundImageView updateViewWithImageAtURL:product.imageURLString];
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.mamhaoFeatured.count == 0) {
        return 0.0f;
    }

    CGFloat height = 0.0f;
    height += 40.0f;
    height += MMHFloatWithPadding(200.0f, 20.0f);
    height += 15.0f;
//    height += 15.0f;
    return height;
}

@end
