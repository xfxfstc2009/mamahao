//
//  MMHMamhaoAnnouncementCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoAnnouncementCell.h"


@implementation MMHMamhaoAnnouncementCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        MMHImageView *imageView = [[MMHImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), MMHFloat(140.0f))];
        imageView.image = [UIImage imageNamed:@"home_banner_banner"];
        [self.contentView addSubview:imageView];
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    return MMHFloat(140.0f);
}


@end
