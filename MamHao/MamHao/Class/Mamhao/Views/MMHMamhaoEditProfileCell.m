//
//  MMHMamhaoEditProfileCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/30.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoEditProfileCell.h"
#import "MMHAppearance.h"
#import "UIView+Extension.h"
#import "MMHAssistant.h"
#import "MMHMamhaoData.h"
#import "MMHProfile.h"


@interface MMHMamhaoEditProfileCell ()

@property (nonatomic, strong) UIImageView *babyPortraitView;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UIImageView *editImageView;
@end


@implementation MMHMamhaoEditProfileCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [MMHAppearance backgroundColor];
    
        UIImageView *babyPortraitView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_baby"]];
        babyPortraitView.left = 12.0f;
        babyPortraitView.centerY = 30.0f;
        [self.contentView addSubview:babyPortraitView];
        self.babyPortraitView = babyPortraitView;
    
        UILabel *tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), 60.0f)];
        tipsLabel.textColor = C6;
        tipsLabel.font = F2;
        tipsLabel.text = @"快来完善宝宝档案，会发现惊喜呦！";
        tipsLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:tipsLabel];
        self.tipsLabel = tipsLabel;
    
        UIImageView *editImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_pen"]];
        [editImageView moveToRight:mmh_screen_width() - 25.0f];
        editImageView.centerY = babyPortraitView.centerY;
        [self.contentView addSubview:editImageView];
        self.editImageView = editImageView;
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
#if 0
    if (product == nil) {
        self.contentView.hidden = YES;
        return;
    }
    else {
        self.contentView.hidden = NO;
    }
#endif
    if ([[self class] childConfiguredWithMamhaoData:mamhaoData]) {
        self.babyPortraitView.hidden = YES;
        self.tipsLabel.hidden = YES;
        self.editImageView.hidden = YES;
    }
    else {
        self.babyPortraitView.hidden = NO;
        self.tipsLabel.hidden = NO;
        self.editImageView.hidden = NO;
    }
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData {
    if ([self childConfiguredWithMamhaoData:mamhaoData]) {
        return 15.0f;
    }
    return 60.0f;
}


+ (BOOL)childConfiguredWithMamhaoData:(MMHMamhaoData *)mamhaoData {
    if (mamhaoData.profile && [mamhaoData.profile hasAnyChild]) {
        return YES;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"BabyMaMState"] != nil) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"BabyNickname"] != nil) {
            return YES;
        }
        return NO;
    }
    return NO;
}

@end
