//
//  MMHMamhaoUserFeaturedProductView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoUserFeaturedProduct;


@interface MMHMamhaoUserFeaturedProductView : UIView

@property (nonatomic, strong) MMHMamhaoUserFeaturedProduct *product;

- (id)initWithProduct:(MMHMamhaoUserFeaturedProduct *)product height:(CGFloat)height;

+ (CGFloat)productImageViewLength;
@end
