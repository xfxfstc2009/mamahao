//
//  MMHMamhaoSearchBarCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoSearchBarCell.h"
#import "MMHSearchViewController.h"


@interface MMHMamhaoSearchBarCell ()<UISearchBarDelegate, UITextFieldDelegate>

@end

@implementation MMHMamhaoSearchBarCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        UITextField *searchField = [[UITextField alloc] initWithFrame:CGRectMake(15, 5.0f, kScreenWidth-30, 30)];
        searchField.delegate = self;
        searchField.leftViewMode = UITextFieldViewModeAlways;
        searchField.rightViewMode = UITextFieldViewModeAlways;
        searchField.leftView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 150, 30)];
        searchField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 150, 30)];
        label.font = F2;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [searchField addGestureRecognizer:tap];
        label.textColor = [UIColor colorWithHexString:@"999999"];
        label.text =  @"搜索妈妈好平台商品";
        [searchField.leftView addSubview:label];
        label.userInteractionEnabled = YES;
        searchField.layer.cornerRadius = 3;
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mmh_icon_search"]];
        imageView.userInteractionEnabled = YES;
        imageView.frame = CGRectMake(0, 5, 20, 20);
        searchField.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
        [searchField.rightView addSubview:imageView];
        [self.contentView addSubview:searchField];
        searchField.layer.borderWidth = 0.5;
        
      }
    return self;
}
- (void)handleTap:(UITapGestureRecognizer *)gestureRecognizer{
    NSLog(@"ddd");
   [self searchBarDidBegin];
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self searchBarDidBegin];
    return NO;
}

- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    return 40.0f;
}


- (void)searchBarDidBegin
{
    // TODO: laoyu
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidBeginSearching:)]) {
        [delegate mamhaoPromotionCellDidBeginSearching:self];
    }
}


@end
