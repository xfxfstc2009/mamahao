//
//  MMHMamhaoUserFeaturedProductView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <AGCommon/AGCommon.h>
#import "MMHMamhaoUserFeaturedProductView.h"
#import "MMHMamhaoUserFeaturedProduct.h"


@interface MMHMamhaoUserFeaturedProductView ()

@property (nonatomic, strong) MMHImageView *avatarView;
@property (nonatomic, strong) UILabel *nicknameLabel;
@property (nonatomic, strong) UILabel *monthAgeLabel;
@property (nonatomic, strong) UILabel *postDateLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *boughtCountLabel;
@property (nonatomic, strong) UIImageView *favoriteIndicatorImageView;
@property (nonatomic, strong) UILabel *favoriteCountLabel;
@end


@implementation MMHMamhaoUserFeaturedProductView


- (id)initWithProduct:(MMHMamhaoUserFeaturedProduct *)product height:(CGFloat)height
{
    self = [self initWithFrame:CGRectMake(0.0f, 0.0f, mmh_screen_width(), height)];
    if (self) {
        self.product = product;
        [self configureViews];
    }
    return self;
}


- (void)configureViews
{
    self.backgroundColor = [UIColor whiteColor];

    // 0. avatar view
    MMHImageView *avatarView = [[MMHImageView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 40.0f, 40.0f)];
    [avatarView setBorderColor:[MMHAppearance separatorColor] cornerRadius:0.0f];
    [avatarView makeRoundedRectangleShape];
    [avatarView updateViewWithImageAtURL:self.product.avatarURLString];
    [self addSubview:avatarView];
    self.avatarView = avatarView;

    // 1. nickname label, month age label and post date label
    UILabel *nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.avatarView.right + 10.0f, 0.0f, 0.0f, 0.0f)];
    nicknameLabel.textColor = [UIColor colorWithHexString:@"666666"];
    nicknameLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    [nicknameLabel setSingleLineText:self.product.nickname];
    nicknameLabel.centerY = self.avatarView.centerY;
    [self addSubview:nicknameLabel];
    self.nicknameLabel = nicknameLabel;

    UILabel *monthAgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    monthAgeLabel.backgroundColor = [UIColor colorWithHexString:@"ff9999"];
    monthAgeLabel.textColor = [UIColor whiteColor];
    monthAgeLabel.font = [UIFont systemFontOfSize:12.0f];
    monthAgeLabel.textAlignment = NSTextAlignmentCenter;
    [monthAgeLabel setSingleLineText:[NSString stringWithMonthAge:self.product.monthAge] constrainedToWidth:CGFLOAT_MAX withEdgeInsets:UIEdgeInsetsMake(0.0f, 10.0f, 0.0f, 10.0f)];
    [monthAgeLabel makeRoundedRectangleShape];
    monthAgeLabel.centerY = self.avatarView.centerY;
    [self addSubview:monthAgeLabel];
    self.monthAgeLabel = monthAgeLabel;

    UILabel *postDateLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [postDateLabel moveToRight:mmh_screen_width() - 10.0f];
    postDateLabel.textAlignment = NSTextAlignmentRight;
    postDateLabel.textColor = [UIColor colorWithHexString:@"313131"];
    postDateLabel.font = [UIFont systemFontOfSize:12.0f];
    [postDateLabel setSingleLineText:self.product.dateString];
    postDateLabel.centerY = self.avatarView.centerY;
    [self addSubview:postDateLabel];
    self.postDateLabel = postDateLabel;

    CGFloat width = mmh_screen_width() - 60.0f - 10.0f - self.postDateLabel.width - 10.0f - self.monthAgeLabel.width - 10.0f;
    self.nicknameLabel.width = MIN(self.nicknameLabel.width, width);
    [self.monthAgeLabel attachToRightSideOfView:self.nicknameLabel byDistance:10.0f];

    // 2. content label
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(60.0f, 50.0f, mmh_screen_width() - 60.0f - 10.0f, 0.0f)];
    contentLabel.textColor = [UIColor colorWithHexString:@"999999"];
    contentLabel.font = [UIFont systemFontOfSize:12.0f];
    [contentLabel setText:self.product.content constrainedToLineCount:2];
    [self addSubview:contentLabel];
    self.contentLabel = contentLabel;
    
    // 3. image views
//    CGFloat imageViewBottom = 88.0f;
    if (self.product.imageURLStrings.count != 0) {
        NSString *string = self.product.imageURLStrings.firstObject;
        CGFloat length = [[self class] productImageViewLength];
        MMHImageView *imageView = [[MMHImageView alloc] initWithFrame:CGRectMake(60.0f, 88.0f, length, length)];
        [imageView setBorderColor:[UIColor colorWithHexString:@"eae6e6"] cornerRadius:0.0f];
        [imageView updateViewWithImageAtURL:string];
        [self addSubview:imageView];
//        imageViewBottom = imageView.bottom;
    
        string = [self.product.imageURLStrings nullableObjectAtIndex:1];
        if (string) {
            CGRect frame = CGRectMake(imageView.right + 5.0f, 88.0f, length, length);
            imageView = [[MMHImageView alloc] initWithFrame:frame];
            [imageView setBorderColor:[UIColor colorWithHexString:@"eae6e6"] cornerRadius:0.0f];
            [imageView updateViewWithImageAtURL:string];
            [self addSubview:imageView];
        } 
    }

    // 4. other info views
    UILabel *boughtCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    boughtCountLabel.top = 88.0f + [[self class] productImageViewLength] + 8.0f;
    boughtCountLabel.textColor = [UIColor colorWithHexString:@"999999"];
    boughtCountLabel.font = [UIFont systemFontOfSize:11.0f];
    [boughtCountLabel setSingleLineText:[NSString stringWithFormat:@"%ld人已购买", self.product.boughtCount]];
    [self addSubview:boughtCountLabel];
    self.boughtCountLabel = boughtCountLabel;
    
    UIImage *image = [UIImage imageNamed:@"home_icon_heart"];
    UIImageView *favoriteIndicatorImageView = [[UIImageView alloc] initWithImage:image];
    favoriteIndicatorImageView.centerY = self.boughtCountLabel.centerY;
    [self addSubview:favoriteIndicatorImageView];
    self.favoriteIndicatorImageView = favoriteIndicatorImageView;

    UILabel *favoriteCountLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    favoriteCountLabel.top = self.boughtCountLabel.top;
    favoriteCountLabel.textColor = [UIColor colorWithHexString:@"999999"];
    favoriteCountLabel.font = [UIFont systemFontOfSize:11.0f];
    [favoriteCountLabel setSingleLineText:[NSString stringWithFormat:@"%ld喜欢", self.product.likedCount]];
    [self addSubview:favoriteCountLabel];
    self.favoriteCountLabel = favoriteCountLabel;

    [self.favoriteCountLabel moveToRight:CGRectGetMaxX(self.bounds) - 10.0f];
    [self.favoriteIndicatorImageView attachToLeftSideOfView:self.favoriteCountLabel byDistance:3.0f];
    [self.boughtCountLabel attachToLeftSideOfView:self.favoriteIndicatorImageView byDistance:10.0f];
}


+ (CGFloat)productImageViewLength
{
    CGFloat length = (mmh_screen_width() - 60.0f - 10.0f - 5.0f) * 0.5f;
    return length;
}


@end
