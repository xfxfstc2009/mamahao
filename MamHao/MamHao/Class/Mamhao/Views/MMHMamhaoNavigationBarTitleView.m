//
//  MMHMamhaoNavigationBarTitleView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/1.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoNavigationBarTitleView.h"


@interface MMHMamhaoNavigationBarTitleView ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicator;
@end


@implementation MMHMamhaoNavigationBarTitleView


- (void)setTitle:(NSString *)title
{
    _title = title;

    NSString *displayTitle = title;
    if ([displayTitle length] > 4) {
        displayTitle = [[title substringToIndex:3] stringByAppendingString:@"..."];
    }
    
    [self.titleLabel setSingleLineText:displayTitle];
    [self.disclosureIndicator attachToRightSideOfView:self.titleLabel byDistance:1.0f];
}


- (instancetype)initWithTitle:(NSString *)title
{
    self = [self init];
    if (self) {
        _title = title;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_logo"]];
        [self addSubview:imageView];
        self.imageView = imageView;

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.imageView.right + 3.0f, 0.0f, 0.0f, 0.0f)];
        titleLabel.textColor = [UIColor colorWithHexString:@"666666"];
        titleLabel.font = [UIFont systemFontOfSize:12.0f];
        NSString *displayTitle = title;
        if ([displayTitle length] > 4) {
            displayTitle = [[title substringToIndex:3] stringByAppendingString:@"..."];
        }
        [titleLabel setSingleLineText:displayTitle constrainedToWidth:CGFLOAT_MAX];
        titleLabel.centerY = imageView.centerY;
        [self addSubview:titleLabel];
        self.titleLabel = titleLabel;
    
        UIImageView *disclosureIndicator = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_down"]];
        disclosureIndicator.left = self.titleLabel.right + 1.0f;
        disclosureIndicator.centerY = self.imageView.centerY;
        [self addSubview:disclosureIndicator];
        self.disclosureIndicator = disclosureIndicator;

        self.frame = CGRectMake(0.0f, 0.0f, self.disclosureIndicator.right, self.imageView.height);

        self.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [self addGestureRecognizer:tapGestureRecognizer];
    }
    return self;
}


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    id <MMHMamhaoNavigationBarTitleViewDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(titleViewTapped:)]) {
        [delegate titleViewTapped:self];
    }
}


@end
