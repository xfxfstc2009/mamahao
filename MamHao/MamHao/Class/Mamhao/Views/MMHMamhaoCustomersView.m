//
//  MMHMamhaoCustomersView.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>
#import "MMHMamhaoCustomersView.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoCustomer.h"
#import "MMHMamhaoShop.h"
#import "UIView+Extension.h"


@interface MMHMamhaoCustomersView ()

@property (nonatomic) long count;
@property (nonatomic, strong) NSArray *customers;
@property (nonatomic, strong) MMHMamhaoShop *shop;
@property (nonatomic, strong) NSMutableArray *avatarViews;
@property (nonatomic, strong) NSMutableArray *distanceLabels;
@property (nonatomic, strong) UILabel *customerCountLabel;
@property (nonatomic, strong) NSTimer *avatarViewsScrollTimer;
@property (nonatomic) long nextCustomerIndex;
//@property (nonatomic) CGFloat neighborAvatarViewCenterXDiff;
@property (nonatomic) BOOL timerIsRun;
@property (nonatomic) BOOL waitTimerFinish;

@end


@implementation MMHMamhaoCustomersView


- (void)updateViewsWithCount:(long)count customers:(NSArray *)customers shop:(MMHMamhaoShop *)shop
{
    self.count = count;
    self.customers = customers;
    self.shop = shop;
    
    [self stopScrollAvatarViews];
    if (self.timerIsRun) {
        self.waitTimerFinish = YES;
        return;
    }
    
    for (MMHImageView *avatarView in self.avatarViews) {
        [avatarView removeFromSuperview];
    }
    [self.avatarViews removeAllObjects];
    
    NSMutableArray *avatarViews = [NSMutableArray array];
    for (NSInteger i = 0; i < 7; i++) {
        MMHMamhaoCustomer *customer = [customers nullableObjectAtIndex:i];
        if (customer == nil) {
            break;
        }
        
        CGRect frame = [self frameOfCustomerAvatarViewAtIndex:i];
        MMHImageView *avatarView = [[MMHImageView alloc] initWithFrame:frame];
        avatarView.placeholderImageName = [self randomPlaceholderImageName];
        [avatarView setBorderColor:[MMHAppearance separatorColor] cornerRadius:0.0f];
        [avatarView makeRoundedRectangleShape];
        [avatarView updateViewWithImageAtURL:customer.avatarURLString];
        [self addSubview:avatarView];
        [avatarViews addObject:avatarView];
    }
    self.avatarViews = avatarViews;
    
    for (UILabel *distanceLabel in self.distanceLabels) {
        [distanceLabel removeFromSuperview];
    }
    [self.distanceLabels removeAllObjects];
    
    NSMutableArray *distanceLabels = [NSMutableArray array];
    for (NSInteger i = 0; i < 7; i++) {
        MMHMamhaoCustomer *customer = [customers nullableObjectAtIndex:i];
        if (customer == nil) {
            break;
        }
        
        CGRect frame = [self frameOfCustomerDistanceLabelAtIndex:i];
        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:frame];
        [distanceLabel makeRoundedRectangleShape];
        distanceLabel.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
        distanceLabel.textColor = [UIColor colorWithHexString:@"959595"];
        distanceLabel.font = [UIFont systemFontOfSize:10.0f];
        distanceLabel.textAlignment = NSTextAlignmentCenter;
        CLLocationDistance distanceFromShop = [customer distanceFromShop:self.shop];
        NSString *distanceText = [NSString stringWithFormat:@"%ldm", (long) distanceFromShop];
        if (distanceFromShop >= 1000) {
            CGFloat km = (CGFloat)(distanceFromShop / 1000.0f);
            distanceText = [NSString stringWithFormat:@"%.1fkm", km];
        }
        [distanceLabel setSingleLineText:distanceText keepingHeight:YES];
        distanceLabel.width = distanceLabel.width + 6.0f;
        if (distanceLabel.width < frame.size.width) {
            distanceLabel.width = frame.size.width;
        }
        distanceLabel.centerX = CGRectGetMidX(frame);
        [self addSubview:distanceLabel];
        [distanceLabels addObject:distanceLabel];
    }
    self.distanceLabels = distanceLabels;
    
    if (self.customerCountLabel == nil) {
        UILabel *customerCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 92.0f, 0.0f, 0.0f)];
        [customerCountLabel moveToRight:(mmh_screen_width() - 10.0f)];
        customerCountLabel.textColor = [MMHAppearance pinkColor];
        customerCountLabel.font = [UIFont boldSystemFontOfCustomeSize:12.0f];
        customerCountLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:customerCountLabel];
        self.customerCountLabel = customerCountLabel;
    }
    NSString *customerCountString = [NSString stringWithFormat:@"%ld人在逛", count];
    [self.customerCountLabel setSingleLineText:customerCountString];
    
//    self.neighborAvatarViewCenterXDiff = ((UIView *)avatarViews[1]).centerX - ((UIView *)avatarViews[0]).centerX;
    if (self.customers.count <= 7) {
        [self adjustAvatarViewAndDistanceLabels];
    }
    else {
        self.nextCustomerIndex = 7;
        [self startScrollAvatarViews];
    }
}


- (void)adjustAvatarViewAndDistanceLabels {
    if (self.customers.count > 7) {
        return;
    }

    for (NSInteger i = 0; i < self.avatarViews.count; i++) {
        MMHImageView *avatarView = self.avatarViews[i];
        CGRect frame = [self frameOfCustomerAvatarViewAtIndex:i + 1];
        avatarView.frame = frame;
    }
    for (NSInteger i = 0; i < self.distanceLabels.count; i++) {
        UILabel *distanceLabel = self.distanceLabels[i];
        CGRect frame = [self frameOfCustomerDistanceLabelAtIndex:i + 1];
        distanceLabel.centerX = CGRectGetMidX(frame);
    }
}


- (void)startScrollAvatarViews {
    if (self.customers.count <= 7) {
        return;
    }
    self.avatarViewsScrollTimer = [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(scrollAvatarViewsTimer) userInfo:nil repeats:YES];
}

- (void)stopScrollAvatarViews {
    if (self.avatarViewsScrollTimer != nil) {
        [self.avatarViewsScrollTimer invalidate];
        self.avatarViewsScrollTimer = nil;
    }
}

- (NSString *)randomPlaceholderImageName
{
    long index = (long)(arc4random() % 12);
    NSString *imageName = [NSString stringWithFormat:@"home_defaultavatar_%ld", index];
    return imageName;
}


- (CGRect)frameOfCustomerAvatarViewAtIndex:(NSInteger)i
{
    CGFloat headerAndTailing = 10.0f;
    CGFloat width = MMHFloat(40.0f);
    CGFloat padding = (mmh_screen_width() - (headerAndTailing * 2.0f) - (width * 6.0f)) / 5.0f;
    CGFloat x = headerAndTailing + (width + padding) * (CGFloat)(i - 1);
    CGRect result = CGRectMake(x, MMHFloat(20.0f), width, width);
    return result;
}


- (CGRect)frameOfCustomerDistanceLabelAtIndex:(NSInteger)i
{
    CGFloat headerAndTailing = 10.0f;
    CGFloat width = MMHFloat(40.0f);
    CGFloat padding = (mmh_screen_width() - (headerAndTailing * 2.0f) - (width * 6.0f)) / 5.0f;
    CGFloat x = headerAndTailing + (width + padding) * (CGFloat)(i - 1);
    CGRect result = CGRectMake(x, MMHFloat(20.0f + 40.0f + 3.0f), width, MMHFloat(12.0f));
    return result;
}


#pragma mark - Action


- (void)scrollAvatarViewsTimer {
    self.timerIsRun = YES;
    
    MMHMamhaoCustomer *customer = [self.customers nullableObjectAtIndex:self.nextCustomerIndex];
    if (customer != nil) {
//        CGRect avatarViewFrame = [self frameOfCustomerAvatarViewAtIndex:6];
//        MMHImageView *avatarView = [[MMHImageView alloc] initWithFrame:avatarViewFrame];
//        avatarView.placeholderImageName = [self randomPlaceholderImageName];
//        [avatarView setBorderColor:[MMHAppearance separatorColor] cornerRadius:0.0f];
//        [avatarView makeRoundedRectangleShape];
//        [avatarView updateViewWithImageAtURL:customer.avatarURLString];
//        [self addSubview:avatarView];
//
//        CGRect distanceLabelFrame = [self frameOfCustomerDistanceLabelAtIndex:6];
//        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:distanceLabelFrame];
//        [distanceLabel makeRoundedRectangleShape];
//        distanceLabel.backgroundColor = [UIColor colorWithHexString:@"eeeeee"];
//        distanceLabel.textColor = [UIColor colorWithHexString:@"959595"];
//        distanceLabel.font = [UIFont systemFontOfSize:10.0f];
//        distanceLabel.textAlignment = NSTextAlignmentCenter;
//        NSString *distanceText = [NSString stringWithFormat:@"%ldm", (long) [customer distanceFromShop:self.shop]];
//        [distanceLabel setSingleLineText:distanceText keepingHeight:YES];
//        distanceLabel.width = distanceLabel.width + 6.0f;
//        if (distanceLabel.width < distanceLabelFrame.size.width) {
//            distanceLabel.width = distanceLabelFrame.size.width;
//        }
//        distanceLabel.centerX = CGRectGetMidX(distanceLabelFrame);
//        [self addSubview:distanceLabel];
        if (self.customers.count <= 7) {
            return;
        }

        NSInteger avatarViewsCount = self.avatarViews.count;
        MMHImageView *firstAvatarView = [self.avatarViews firstObject];
        firstAvatarView.frame = [self frameOfCustomerAvatarViewAtIndex:avatarViewsCount];
        [self.avatarViews moveObject:firstAvatarView toIndex:avatarViewsCount - 1];

        NSInteger distanceLabelsCount = self.distanceLabels.count;
        UILabel *firstDistanceLabel = [self.distanceLabels firstObject];
        CGRect distanceLabelFrame = [self frameOfCustomerDistanceLabelAtIndex:distanceLabelsCount];
        firstDistanceLabel.frame = distanceLabelFrame;
        [self.distanceLabels moveObject:firstDistanceLabel toIndex:distanceLabelsCount - 1];

        [firstAvatarView updateViewWithImageAtURL:customer.avatarURLString];
        CLLocationDistance distanceFromShop = [customer distanceFromShop:self.shop];
        NSString *distanceText = [NSString stringWithFormat:@"%ldm", (long) distanceFromShop];
        if (distanceFromShop >= 1000) {
            CGFloat km = (CGFloat)(distanceFromShop / 1000.0f);
            distanceText = [NSString stringWithFormat:@"%.1fkm", km];
        }
        [firstDistanceLabel setSingleLineText:distanceText keepingHeight:YES];
        firstDistanceLabel.width = firstDistanceLabel.width + 6.0f;
        if (firstDistanceLabel.width < distanceLabelFrame.size.width) {
            firstDistanceLabel.width = distanceLabelFrame.size.width;
        }
        firstDistanceLabel.centerX = CGRectGetMidX(distanceLabelFrame);

        __weak __typeof(self) weakSelf = self;
        [UIView animateWithDuration:1 animations:^{
            __weak __typeof(self) strongSelf = weakSelf;
            if (strongSelf.avatarViews.count < 7) {
                return;
            }

            for (NSInteger i = 0; i < strongSelf.avatarViews.count; i++) {
                MMHImageView *avatarView = [strongSelf.avatarViews nullableObjectAtIndex:i];
                if (avatarView) {
                    CGRect frame = [strongSelf frameOfCustomerAvatarViewAtIndex:i];
                    avatarView.centerX = CGRectGetMidX(frame);
                }
                UILabel *distanceLabel = [strongSelf.distanceLabels nullableObjectAtIndex:i];
                if (distanceLabel) {
                    CGRect frame = [strongSelf frameOfCustomerDistanceLabelAtIndex:i];
                    distanceLabel.centerX = CGRectGetMidX(frame);
                }
            }
//            avatarView.centerX -= weakSelf.neighborAvatarViewCenterXDiff;
//            distanceLabel.centerX -= weakSelf.neighborAvatarViewCenterXDiff;
//
//            for (int i = 5; i >= 0; --i) {
//                ((UIView *) weakSelf.avatarViews[i]).centerX -= weakSelf.neighborAvatarViewCenterXDiff;
//                ((UIView *) weakSelf.distanceLabels[i]).centerX -= weakSelf.neighborAvatarViewCenterXDiff;
//            }
        }                completion:^(BOOL finished) {
//            [(UIView *) weakSelf.avatarViews[0] removeFromSuperview];
//            [(UIView *) weakSelf.distanceLabels[0] removeFromSuperview];
//
//            for (int i = 0; i < 5; ++i) {
//                weakSelf.avatarViews[i] = weakSelf.avatarViews[i + 1];
//                weakSelf.distanceLabels[i] = weakSelf.distanceLabels[i + 1];
//            }
//
//            weakSelf.avatarViews[5] = avatarView;
//            weakSelf.distanceLabels[5] = distanceLabel;
            weakSelf.nextCustomerIndex++;
            if (weakSelf.nextCustomerIndex >= weakSelf.customers.count) {
                weakSelf.nextCustomerIndex = 0;
            }

            weakSelf.timerIsRun = NO;
            if (weakSelf.waitTimerFinish) {
                weakSelf.waitTimerFinish = NO;
                [self updateViewsWithCount:weakSelf.count customers:weakSelf.customers shop:weakSelf.shop];
            }
        }];
    }
    
}

@end
