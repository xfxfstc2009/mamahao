//
//  MMHMamhaoUserFeaturedCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoUserFeaturedCell.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoUserFeaturedProduct.h"
#import "MMHMamhaoUserFeaturedProductView.h"
#import "MMHMamhaoUserFeaturedData.h"
#import "MamHao-Swift.h"
#import "MMHSingleProductModel.h"


@interface MMHMamhaoUserFeaturedCell () <UIScrollViewDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) MMHMamhaoData *mamhaoData;
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UILabel *monthAgeLabel;
@property (nonatomic, strong) UIImageView *disclosureIndicatorImageView;
@property (nonatomic, strong) UIView *indicatorView;
@property (nonatomic, strong) MMHMamhaoUserFeaturedData *userFeaturedData;
@property (nonatomic, strong) MonthAge *monthAgeForProducts;
@end


@implementation MMHMamhaoUserFeaturedCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];

        UIView *indicatorView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 6.0f, 20.0f)];
        indicatorView.backgroundColor = [MMHAppearance pinkColor];
        [self.contentView addSubview:indicatorView];
        self.indicatorView = indicatorView;

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(indicatorView.right + 5.0f, indicatorView.top, 0.0f, indicatorView.height)];
        titleLabel.textColor = [MMHAppearance blackColor];
        titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
        [titleLabel setSingleLineText:@"同龄妈妈热购"];
        titleLabel.centerY = titleLabel.centerY;
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;

        if (self.disclosureIndicatorImageView == nil) {
            UIImageView *disclosureIndicatorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_icon_bluedown"]];
            disclosureIndicatorImageView.centerY = self.indicatorView.centerY;
            [disclosureIndicatorImageView moveToRight:mmh_screen_width() - 10.0f];
            [self.contentView addSubview:disclosureIndicatorImageView];
            self.disclosureIndicatorImageView = disclosureIndicatorImageView;
        }

        if (self.monthAgeLabel == nil) {
            UILabel *monthAgeLabel = [[UILabel alloc] initWithFrame:CGRectZero];
            monthAgeLabel.textColor = [UIColor colorWithHexString:@"136cbe"];
            monthAgeLabel.font = [UIFont systemFontOfSize:12.0f];
            monthAgeLabel.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
            [self.contentView addGestureRecognizer:tapGestureRecognizer];
            [self.contentView addSubview:monthAgeLabel];
            self.monthAgeLabel = monthAgeLabel;
        }

        CGFloat separatorLineHeight = 1.0f / mmh_screen_scale();
        UIView *separatorLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 40.0f - separatorLineHeight, mmh_screen_width(), separatorLineHeight)];
        separatorLine.backgroundColor = [MMHAppearance separatorColor];
        [self.contentView addSubview:separatorLine];

        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.contentView.bounds.size.height - 15.0f, mmh_screen_width(), 15.0f)];
        separatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
        separatorView.backgroundColor = [MMHAppearance backgroundColor];
        [self.contentView addSubview:separatorView];
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (self.userFeaturedData == mamhaoData.userFeaturedData) {
        return;
    }

    self.userFeaturedData = mamhaoData.userFeaturedData;
    self.monthAgeForProducts = mamhaoData.monthAgeForProducts;
    
    if ([mamhaoData.userFeaturedData.products firstObject] == nil) {
        self.contentView.hidden = YES;
        [self.contentScrollView removeAllSubviews];
        return;
    }
    else {
        self.contentView.hidden = NO;
    }
    
    NSString *monthAgeString = [mamhaoData.monthAgeForProducts displayName];
    if (monthAgeString.length == 0) {
        [self.monthAgeLabel setSingleLineText:@"全部"];
    }
    else {
        [self.monthAgeLabel setSingleLineText:[NSString stringWithFormat:@"%@", monthAgeString]];
    }
    self.monthAgeLabel.centerY = self.indicatorView.centerY;
    [self.monthAgeLabel attachToLeftSideOfView:self.disclosureIndicatorImageView byDistance:5.0f];
    
    if (self.contentScrollView == nil) {
        UIScrollView *contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0.0f, 40.0f, mmh_screen_width(), 88.0f + [MMHMamhaoUserFeaturedProductView productImageViewLength] + 38.0f)];
        contentScrollView.pagingEnabled = YES;
        contentScrollView.backgroundColor = [UIColor whiteColor];
        contentScrollView.delegate = self;
        [self.contentView addSubview:contentScrollView];
        self.contentScrollView = contentScrollView;
    }

    [self.contentScrollView removeAllSubviews];
    CGFloat x = 0.0f;
    for (MMHMamhaoUserFeaturedProduct *product in mamhaoData.userFeaturedData.products) {
        MMHMamhaoUserFeaturedProductView *userFeaturedProductView = [[MMHMamhaoUserFeaturedProductView alloc] initWithProduct:product height:self.contentScrollView.height];
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productViewTapped:)];
        [userFeaturedProductView addGestureRecognizer:tapGestureRecognizer];
        [self.contentScrollView addSubview:userFeaturedProductView];
        userFeaturedProductView.x = x;
        x = userFeaturedProductView.right;
    }
    self.contentScrollView.contentSize = CGSizeMake(x, self.contentScrollView.height);

    if (self.pageControl == nil) {
        UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0.0f, self.contentScrollView.bottom, mmh_screen_width(), 4.0f)];
        pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"eb6877"];
        pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"a0a0a0"];
        pageControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        pageControl.contentMode = UIViewContentModeCenter;
        [self.contentView addSubview:pageControl];
        self.pageControl = pageControl;
    }
    if (self.mamhaoData == nil) {
        [self.pageControl removeFromSuperview];
        self.pageControl = nil;
    }
    else {
        self.pageControl.centerX = mmh_screen_width() * 0.5f;
        self.pageControl.numberOfPages = mamhaoData.userFeaturedData.products.count;
        [self updatePageControlSelectedIndex];
    }
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.userFeaturedData == nil) {
        return 0.0f;
    }
    
    if (mamhaoData.userFeaturedData.products.count == 0) {
        return 0.0f;
    }
    
    CGFloat height = 0.0f;
    height += 40.0f;
    height += 88.0f;
    height += [MMHMamhaoUserFeaturedProductView productImageViewLength];
    height += 56.0f;
    height += 15.0f;
    return height;
}


#pragma mark - UIScrollView delegate methods


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updatePageControlSelectedIndex];
}


- (void)updatePageControlSelectedIndex
{
    CGPoint contentOffset = self.contentScrollView.contentOffset;
    CGFloat index = contentOffset.x / mmh_screen_width();
    self.pageControl.currentPage = (NSInteger)index;
}


- (void)productViewTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    MMHMamhaoUserFeaturedProductView *userFeaturedProductView = (MMHMamhaoUserFeaturedProductView *) tapGestureRecognizer.view;
    MMHMamhaoUserFeaturedProduct *product = userFeaturedProductView.product;
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(mamhaoPromotionCell:didSelectUserFeaturedProduct:)]) {
        [delegate mamhaoPromotionCell:self didSelectUserFeaturedProduct:product];
    }
}


#pragma mark - Actions


- (void)tapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    CGPoint location = [tapGestureRecognizer locationInView:tapGestureRecognizer.view];
    if (self.monthAgeLabel) {
        if ((location.x >= self.monthAgeLabel.left) && (location.y <= 40.0f)) {
            id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
            if ([delegate respondsToSelector:@selector(productHeaderViewDidSelectMonthAge:)]) {
                [delegate productHeaderViewDidSelectMonthAge:self];
            }
        }
    }
}



@end
