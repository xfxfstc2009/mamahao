//
//  MMHMamhaoMBeanSpecialCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoMBeanSpecialCell.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHAccountSession.h"


@interface MMHMamhaoMBeanSpecialCell ()

@property (nonatomic, strong) MMHMamhaoBeanSpecialProduct *product;
@property (nonatomic, strong) UIView *productContentView;
@property (nonatomic, strong) UILabel *tipsLabel;
@property (nonatomic, strong) UILabel *countdownLabel;
@property (nonatomic, strong) NSTimer *countdownTimer;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) MMHImageView *productImageView;
@property (nonatomic, strong) UILabel *beanBalanceLabel;
@property (nonatomic, strong) UILabel *beanGuideLabel;
@property (nonatomic, strong) UILabel *smallTipsLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@end


@implementation MMHMamhaoMBeanSpecialCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.beanSpecials.count == 0) {
        [self.productContentView removeAllSubviews];
        [self.productContentView removeFromSuperview];

        self.productContentView = nil;
        self.tipsLabel = nil;
        self.smallTipsLabel = nil;
        self.countdownLabel = nil;
        self.statusLabel = nil;
        [self.countdownTimer invalidate];
        self.countdownTimer = nil;
        self.messageLabel = nil;
        self.productImageView = nil;
        [self.beanGuideLabel removeFromSuperview];
        self.beanGuideLabel = nil;
        [self.beanBalanceLabel removeFromSuperview];
        self.beanBalanceLabel = nil;
        return;
    }

    MMHMamhaoBeanSpecialProduct *product = [mamhaoData.beanSpecials firstObject];
    self.product = product;

    UIView *productContentView = self.productContentView;
    if (self.productContentView == nil) {
        productContentView = [[UIView alloc] initWithFrame:CGRectMake(10.0f, 0.0f, mmh_screen_width() - 20.0f, MMHFloat(140.0f))];
        productContentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:productContentView];
        self.productContentView = productContentView;
    }

    if (self.tipsLabel == nil) {
        UILabel *tipsLabel = [[UILabel alloc] initWithFrame:MMHRectMake(14.0f, 10.0f, 0.0f, 0.0f)];
        tipsLabel.textColor = C6;
        tipsLabel.font = [UIFont boldSystemFontOfSize:MMHFontSize(16.0f)];
        [tipsLabel setSingleLineText:@"妈豆尊享"];
        [productContentView addSubview:tipsLabel];
        self.tipsLabel = tipsLabel;
    }

    if (self.smallTipsLabel == nil) {
        UILabel *smallTipsLabel = [[UILabel alloc] initWithFrame:MMHRectMake(14.0f, 37.0f, 0.0f, 0.0f)];
        smallTipsLabel.textColor = C6;
        smallTipsLabel.font = F2;
        [productContentView addSubview:smallTipsLabel];
        self.smallTipsLabel = smallTipsLabel;
    }
    if (self.countdownLabel == nil) {
        UILabel *countdownLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(14.0f), MMHFloat(57.0f), 0.0f, 0.0f)];
        countdownLabel.textColor = [UIColor colorWithHexString:@"dd2727"];
        countdownLabel.font = [UIFont boldSystemFontOfSize:MMHFontSize(24.0f)];
        [productContentView addSubview:countdownLabel];
        self.countdownLabel = countdownLabel;
    }

    if (self.statusLabel == nil) {
        UILabel *statusLabel = [[UILabel alloc] initWithFrame:MMHRectMake(220.0f, 30.0f, 80.0f, 80.0f)];
        statusLabel.backgroundColor = [C7 colorWithAlphaComponent:0.65f];
        statusLabel.textColor = [UIColor whiteColor];
        statusLabel.font = [MMHF8 boldFont];
        statusLabel.textAlignment = NSTextAlignmentCenter;
        [statusLabel makeRoundedRectangleShape];
        statusLabel.hidden = YES;
        [productContentView addSubview:statusLabel];
        self.statusLabel = statusLabel;
    }
    
    [self updateSmallTipsAndCountdownLabel];

    if (self.countdownTimer == nil) {
        self.countdownTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.countdownTimer forMode:NSRunLoopCommonModes];
    }
    
    if (self.messageLabel == nil) {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(14.0f), MMHFloat(96.0f), MMHFloat(175.0f - 14.0f), 0.0f)];
        messageLabel.textColor = [UIColor colorWithHexString:@"a0a0a0"];
        messageLabel.font = MMHF2;
        [productContentView addSubview:messageLabel];
        self.messageLabel = messageLabel;
    }
    [self.messageLabel setText:[product message] constrainedToLineCount:2];

    if (self.productImageView == nil) {
        MMHImageView *productImageView = [[MMHImageView alloc] initWithFrame:MMHRectMake(175.0f, 0.0f, 180.0f, 140.0f)];
        [productImageView moveToRight:CGRectGetMaxX(self.productContentView.bounds)];
        [productContentView addSubview:productImageView];
        [self.statusLabel bringToFront];
        self.productImageView = productImageView;
    }
    [self.productImageView updateViewWithImageAtURL:product.imageURLString];

    if (self.beanBalanceLabel == nil) {
        UILabel *beanBalanceLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        beanBalanceLabel.textColor = [UIColor colorWithHexString:@"434343"];
        beanBalanceLabel.font = MMHFontOfSize(12.0f);
        [beanBalanceLabel attachToBottomSideOfView:productContentView byDistance:MMHFloat(5.0f)];
        [self.contentView addSubview:beanBalanceLabel];
        self.beanBalanceLabel = beanBalanceLabel;
    }
    [self.beanBalanceLabel setSingleLineText:[NSString stringWithFormat:@"我的妈豆:%ld", mamhaoData.beanBalance]];
    if ([[MMHAccountSession currentSession] alreadyLoggedIn]) {
        self.beanBalanceLabel.hidden = NO;
    }
    else {
        self.beanBalanceLabel.hidden = YES;
    }

    if (self.beanGuideLabel == nil) {
        UILabel *beanGuideLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        beanGuideLabel.textColor = [UIColor colorWithHexString:@"136cbe"];
        beanGuideLabel.font = MMHFontOfSize(12.0f);
        [beanGuideLabel attachToBottomSideOfView:productContentView byDistance:MMHFloat(5.0f)];
        beanGuideLabel.right = productContentView.right;
        beanGuideLabel.textAlignment = NSTextAlignmentRight;
        beanGuideLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(beanGuideLabelTapped:)];
        [beanGuideLabel addGestureRecognizer:tapGestureRecognizer];
        [self.contentView addSubview:beanGuideLabel];
        self.beanGuideLabel = beanGuideLabel;
    }
    [self.beanGuideLabel setSingleLineText:@"我要更多妈豆？"];
    [self.beanBalanceLabel attachToLeftSideOfView:self.beanGuideLabel byDistance:MMHFloat(9.0f)];
}


- (void)beanGuideLabelTapped:(UITapGestureRecognizer *)tapGestureRecognizer
{
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if (delegate) {
        if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidSelectBeanGuide:)]) {
            [delegate mamhaoPromotionCellDidSelectBeanGuide:self];
        }
    }
}


- (void)timerFired:(id)timerFired
{
    [self updateSmallTipsAndCountdownLabel];
}


- (void)updateSmallTipsAndCountdownLabel
{
    if (self.product == nil) {
        self.smallTipsLabel.text = @"";
        self.countdownLabel.text = @"";
    }
    else {
        MMHMamhaoBeanSpecialProductInfo *info = self.product.info;

        switch (info.status) {
            case MMHMamhaoBeanSpecialProductStatusNotStartedYet: {
                [self.smallTipsLabel setSingleLineText:@"距活动开始"];
                self.countdownLabel.textColor = [UIColor colorWithHexString:@"dd2727"];
                self.statusLabel.hidden = YES;
                self.statusLabel.text = @"";
                break;
            }
            case MMHMamhaoBeanSpecialProductStatusOnSale: {
                [self.smallTipsLabel setSingleLineText:@"距活动结束"];
                self.countdownLabel.textColor = [UIColor colorWithHexString:@"dd2727"];
                if ([info isSoldOut]) {
                    self.statusLabel.hidden = NO;
                    self.statusLabel.text = @"已抢光";
                }
                else {
                    self.statusLabel.hidden = YES;
                    self.statusLabel.text = @"";
                }
                break;
            }
            case MMHMamhaoBeanSpecialProductStatusExpired: {
                [self.smallTipsLabel setSingleLineText:@"今日活动已结束"];
                self.countdownLabel.textColor = [UIColor colorWithHexString:@"828282"];
                self.statusLabel.hidden = NO;
                self.statusLabel.text = @"活动结束";

                [self.countdownTimer invalidate];
                self.countdownTimer = nil;
                break;
            }
            default: {
                break;
            }
        }

        [self.countdownLabel setSingleLineText:info.displayTimeRemaining];
    }
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    if (mamhaoData.beanSpecials.count == 0) {
        return 0.0f;
    }
    return MMHFloat(176.0f);
}


@end
