//
//  MMHMamhaoGamesCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoGamesCell.h"


@interface MMHMamhaoGamesCell ()

@property (nonatomic, strong) UIButton *beanButton;
@property (nonatomic, strong) UIButton *redEnvelopButton;
@property (nonatomic, strong) UIButton *categoryButton;
@end


@implementation MMHMamhaoGamesCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];

        CGRect defaultButtonFrame = CGRectMake(0.0f, 0.0f, 90.0f, 75.0f);
        UIButton *beanButton = [[UIButton alloc] initWithFrame:defaultButtonFrame];
        [beanButton setImage:[UIImage imageNamed:@"home_icon_beans"] forState:UIControlStateNormal];
        [beanButton setTitle:@"摇妈豆" forState:UIControlStateNormal];
        [beanButton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        beanButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        beanButton.imageEdgeInsets = UIEdgeInsetsWithRight(5.0f);
        beanButton.titleEdgeInsets = UIEdgeInsetsWithLeft(5.0f);
        [beanButton addTarget:self action:@selector(beanButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:beanButton];
        self.beanButton = beanButton;
    
//        UIButton *redEnvelopButton = [[UIButton alloc] initWithFrame:defaultButtonFrame];
//        [redEnvelopButton setImage:[UIImage imageNamed:@"home_icon_red-packets"] forState:UIControlStateNormal];
//        [redEnvelopButton setTitle:@"抢红包" forState:UIControlStateNormal];
//        [redEnvelopButton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
//        redEnvelopButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
//        redEnvelopButton.imageEdgeInsets = UIEdgeInsetsWithRight(5.0f);
//        redEnvelopButton.titleEdgeInsets = UIEdgeInsetsWithLeft(5.0f);
//        [redEnvelopButton addTarget:self action:@selector(redEnvelopButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//        [self.contentView addSubview:redEnvelopButton];
//        self.redEnvelopButton = redEnvelopButton;
    
        UIButton *categoryButton = [[UIButton alloc] initWithFrame:defaultButtonFrame];
        [categoryButton setImage:[UIImage imageNamed:@"home_icon_classify"] forState:UIControlStateNormal];
        [categoryButton setTitle:@"分类" forState:UIControlStateNormal];
        [categoryButton setTitleColor:[UIColor colorWithHexString:@"333333"] forState:UIControlStateNormal];
        categoryButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        categoryButton.imageEdgeInsets = UIEdgeInsetsWithRight(5.0f);
        categoryButton.titleEdgeInsets = UIEdgeInsetsWithLeft(5.0f);
        [categoryButton addTarget:self action:@selector(categoryButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:categoryButton];
        self.categoryButton = categoryButton;

//        redEnvelopButton.centerX = mmh_screen_width() * 0.5f;
//        CGFloat padding = floorf((redEnvelopButton.left - beanButton.width) * 0.5f);
//        [beanButton attachToLeftSideOfView:redEnvelopButton byDistance:padding];
//        [categoryButton attachToRightSideOfView:redEnvelopButton byDistance:padding];
        
        CGFloat padding = floorf((mmh_screen_width() - beanButton.width - categoryButton.width) / 3.0f);
        beanButton.left = padding;
        [categoryButton attachToRightSideOfView:beanButton byDistance:padding];
        
        UIView *middleSeparatorLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, mmh_pixel(), 35.0f)];
        middleSeparatorLine.center = CGPointMake(mmh_screen_width() * 0.5f, 75.0f * 0.5f);
        middleSeparatorLine.backgroundColor = [MMHAppearance separatorColor];
        [self.contentView addSubview:middleSeparatorLine];

        UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, defaultButtonFrame.size.height, mmh_screen_width(), 15.0f)];
        separatorView.backgroundColor = [MMHAppearance backgroundColor];
        [self.contentView addSubview:separatorView];
    }
    return self;
}


- (void)updateViewsWithMamhaoData:(MMHMamhaoData *)mamhaoData
{
}


+ (CGFloat)heightForMamhaoData:(MMHMamhaoData *)mamhaoData
{
    return 75.0f + 15.0f;
}


- (void)beanButtonTapped:(UIButton *)button
{
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if (delegate) {
        if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidSelectBeanGame:)]) {
            [delegate mamhaoPromotionCellDidSelectBeanGame:self];
        }
    }
}


- (void)redEnvelopButtonTapped:(UIButton *)button
{
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if (delegate) {
        if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidSelectRedEnvelopGame:)]) {
            [delegate mamhaoPromotionCellDidSelectRedEnvelopGame:self];
        }
    }
}


- (void)categoryButtonTapped:(UIButton *)button
{
    id<MMHMamhaoPromotionCellDelegate> delegate = self.delegate;
    if (delegate) {
        if ([delegate respondsToSelector:@selector(mamhaoPromotionCellDidSelectCategory:)]) {
            [delegate mamhaoPromotionCellDidSelectCategory:self];
        }
    }
}


@end
