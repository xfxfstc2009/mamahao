//
//  MMHMamhaoCustomersView.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/5.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>


@class MMHMamhaoData;
@class MMHMamhaoShop;


@interface MMHMamhaoCustomersView : UIView

- (void)updateViewsWithCount:(long)count customers:(NSArray *)customers shop:(MMHMamhaoShop *)shop;
@end
