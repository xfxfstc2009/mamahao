//
//  MMHMamhaoViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHMamhaoNavigationBarTitleView;
@class MMHMamhaoProductHeaderView;


@interface MMHMamhaoViewController : AbstractViewController

@property (nonatomic, strong) MMHMamhaoProductHeaderView *productHeaderView;

- (void)updateViews;
@end
