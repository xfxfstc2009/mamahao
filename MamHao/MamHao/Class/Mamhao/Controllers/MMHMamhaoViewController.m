//
//  MMHMamhaoViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/5/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <MJRefresh/UIScrollView+MJRefresh.h>
#import "MMHMamhaoViewController.h"
#import "MMHMamhaoNavigationBarTitleView.h"
#import "MMHMessageBarButtonItem.h"
#import "MMHMamhaoDataManager.h"
#import "MMHMamhaoProductHeaderView.h"
#import "MMHMamhaoData.h"
#import "MMHMamhaoPromotionCell.h"
#import "MMHMamhaoProduct.h"
#import "MMHMamhaoProductCell.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHShopDetailViewController.h"
#import "MMHMamhaoShop.h"
#import "MMHMamhaoUserFeaturedProduct.h"
#import "MMHMamhaoProductHeaderView.h"
#import "MMHMamhaoFeaturedProductCell.h"
#import "MMHMonthAgeSelectionView.h"
#import "MamHao-Swift.h"
#import "MMHPullDownToRefresh.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "MMHClassificationViewController.h"
#import "MMHSearchViewController.h"
#import "MMHScanningViewController.h"
#import "MMHProductDetailViewController1.h"
#import "MMHWebViewController.h"
#import "MMHSearchAddressViewController.h"

#import "MMHWriteBabyInfoAnimationViewController.h"                     // showBabyInfo
#import "MMHWebShakeMBeanViewController.h"                              // 摇妈豆
#import "MMHEditBabyInfoWithHomeViewController.h"
#import "MMHAccountSession.h"
#import "MMHTabBarController.h"
#import "MMHChattingConversationViewController.h"
#import "MMHChattingViewController.h"
#import "MMHChattingViewController+Preparation.h"
#import "AbstractViewController+Chatting.h"
#import "MMHChangeAddressViewController.h"                              // 修改地址
#import "MMHMamhaoEditProfileCell.h"


NSString * const MMHMamhaoProductHeaderViewIdentifier = @"MMHMamhaoProductHeaderViewIdentifier";
NSString * const MMHMamhaoFeaturedProductCellIdentifier = @"MMHMamhaoFeaturedProductCellIdentifier";
NSString * const MMHMamhaoProductCellIdentifier = @"MMHMamhaoProductCellIdentifier";


CGFloat MMHMamhaoViewControllerPadding() {
    return MMHFloat(15.0f);
}


CGFloat MMHMamhaoViewControllerProductsPadding() {
    return MMHFloat(10.0f);
}


typedef NS_ENUM(NSInteger, MMHMamhaoViewControllerSection) {
    MMHMamhaoViewControllerSectionPromotion,
    MMHMamhaoViewControllerSectionProduct,
};


@interface MMHMamhaoViewController () <UITableViewDataSource, UITableViewDelegate, MMHMamhaoPromotionCellDelegate, MMHMamhaoProductHeaderViewDelegate, MMHMamhaoProductCellDelegate, MMHMonthAgeSelectionViewDelegate, MMHMamhaoNavigationBarTitleViewDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) MMHMamhaoNavigationBarTitleView *titleView;
@end


@implementation MMHMamhaoViewController


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


- (instancetype)init {
    self = [super init];
    if (self) {
        self.title = @"妈妈好";
        self.tabBarItem.image = [UIImage imageNamed:@"mmhtab_icon_heart_nor"];
        self.tabBarItem.selectedImage = [UIImage imageNamed:@"mmhtab_icon_heart_hlt"];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [MMHLocationManager getArea];
    [MMHLocationManager getNormalAddress];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBar.barTintColor = [UIColor colorWithHexString:@"efeff0"];

    MMHMamhaoNavigationBarTitleView *titleView = [[MMHMamhaoNavigationBarTitleView alloc] initWithTitle:@"杭州"];
    titleView.delegate = self;
    self.navigationItem.titleView = titleView;
    self.titleView = titleView;

    UIBarButtonItem *scanBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home_icon_scan"]
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(scan:)];
    scanBarButtonItem.tintColor = [UIColor colorWithHexString:@"666666"];
    self.navigationItem.leftBarButtonItem = scanBarButtonItem;
    
    MMHMessageBarButtonItem *messageBarButtonItem = [[MMHMessageBarButtonItem alloc] initWithBadgeNumber:0
                                                                                                  target:self
                                                                                                  action:@selector(viewMessages)];
    self.navigationItem.rightBarButtonItem = messageBarButtonItem;

    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    tableView.backgroundColor = [MMHAppearance backgroundColor];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    UIView *tableViewBackgroundView = [[UIView alloc] initWithFrame:CGRectZero];
//    tableViewBackgroundView.backgroundColor = [UIColor colorWithHexString:@"f2f2f2"];
//    tableView.backgroundView = tableViewBackgroundView;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [MMHMamhaoPromotionCell registerCellClassesForTableView:tableView];
    [tableView registerClass:[MMHMamhaoProductHeaderView class] forHeaderFooterViewReuseIdentifier:MMHMamhaoProductHeaderViewIdentifier];
    [tableView registerClass:[MMHMamhaoFeaturedProductCell class] forCellReuseIdentifier:MMHMamhaoFeaturedProductCellIdentifier];
    [tableView registerClass:[MMHMamhaoProductCell class] forCellReuseIdentifier:MMHMamhaoProductCellIdentifier];
    tableView.dataSource = self;
    tableView.delegate = self;
    [self.view addSubview:tableView];
    self.tableView = tableView;

    [self.tableView addPullDownToRefreshHeaderWithRefreshingTarget:self refreshingAction:@selector(refresh)];
    [self.tableView triggerPullToRefresh];

    [self fetchData];
    
    // ares 选择宝宝信息
    [self showBabyInfo];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogout:) name:MMHUserDidLogoutNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(childInfoEdited:) name:MMHChildInfoEditedNotification object:nil];
}


- (void)refresh
{
    [self fetchData];
}


- (void)fetchData
{
    [[MMHMamhaoDataManager sharedDataManager] fetchActivityImagesCompletion:^(BOOL succeeded, NSString *beanGameImageURLString, NSString *categoryImageURLString, NSError *error) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:MMHMamhaoPromotionCellRowGames inSection:MMHMamhaoViewControllerSectionPromotion];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];

    if ([MMHMamhaoDataManager sharedDataManager].data) {
        [self updateViews];
    }
//    [self.view showProcessingView];
//    self.tableView.userInteractionEnabled = NO;
    if ([MMHMamhaoDataManager sharedDataManager].fetching) {
        return;
    }

    __weak __typeof(self) weakSelf = self;
    [[MMHMamhaoDataManager sharedDataManager] fetchMamhaoDataCompletion:^(BOOL succeeded, MMHMamhaoData *data, NSError *error) {
//        [weakSelf.view hideProcessingView];
        if (succeeded) {
            [weakSelf updateViews];
        }
//        weakSelf.tableView.userInteractionEnabled = YES;
        [weakSelf.tableView.header endRefreshing];
    }];
}


- (void)updateViews
{
    [self.tableView reloadData];
    [self updateTitleView];
}


- (void)updateTitleView
{
    NSString *city = [MMHCurrentLocationModel sharedLocation].city;
    self.titleView.title = city;
}


#pragma mark - Table view data source and delegate methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([[MMHMamhaoDataManager sharedDataManager] hasProducts]) {
        return 2;
    }
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case MMHMamhaoViewControllerSectionPromotion: {
            if ([[MMHMamhaoDataManager sharedDataManager] isDataFetched]) {
                return MMHMamhaoPromotionCellRowMaximum + 1;
            }
            else {
                return 3;
            }
        }
        case MMHMamhaoViewControllerSectionProduct: {
            return [[MMHMamhaoDataManager sharedDataManager] productCount];
        }
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case MMHMamhaoViewControllerSectionPromotion: {
            Class<MMHMamhaoPromotionCell> cellClass = [MMHMamhaoPromotionCell classForRow:indexPath.row];
            return [cellClass heightForMamhaoData:[MMHMamhaoDataManager sharedDataManager].data];
        }
        case MMHMamhaoViewControllerSectionProduct: {
            MMHMamhaoProduct *product = [[MMHMamhaoDataManager sharedDataManager] productAtIndex:indexPath.row];
            if ([[MMHMamhaoDataManager sharedDataManager] isIndexFeatured:indexPath.row]) {
                return [MMHMamhaoFeaturedProductCell heightForProduct:product];
            }
            return [MMHMamhaoProductCell heightForProduct:product];
        }
    }

    return 0.0f;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ((MMHMamhaoViewControllerSection)(indexPath.section)) {
        case MMHMamhaoViewControllerSectionPromotion: {
            NSString *cellIdentifier = [MMHMamhaoPromotionCell cellIdentifierForRow:indexPath.row];
            MMHMamhaoPromotionCell *cell = (MMHMamhaoPromotionCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            [cell updateViewsWithMamhaoData:[MMHMamhaoDataManager sharedDataManager].data];
            cell.delegate = self;
            return cell;
            break;
        }
        case MMHMamhaoViewControllerSectionProduct: {
            MMHMamhaoProduct *product = [[MMHMamhaoDataManager sharedDataManager] productAtIndex:indexPath.row];
            if ([[MMHMamhaoDataManager sharedDataManager] isIndexFeatured:indexPath.row]) {
                MMHMamhaoFeaturedProductCell *cell = [tableView dequeueReusableCellWithIdentifier:MMHMamhaoFeaturedProductCellIdentifier];
                cell.product = product;
                return cell;
            }            
            MMHMamhaoProductCell *cell = [tableView dequeueReusableCellWithIdentifier:MMHMamhaoProductCellIdentifier];
            cell.product = product;
            cell.delegate = self;
            return cell;
            break;
        }
    }
    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch ((MMHMamhaoViewControllerSection) (section)) {
        case MMHMamhaoViewControllerSectionPromotion:
            return 0.0f;
        case MMHMamhaoViewControllerSectionProduct:
            return 75.0f;
    }
    return 0.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    switch ((MMHMamhaoViewControllerSection) (section)) {
        case MMHMamhaoViewControllerSectionPromotion:
            return nil;
        case MMHMamhaoViewControllerSectionProduct: {
            MMHMamhaoProductHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:MMHMamhaoProductHeaderViewIdentifier];
            [headerView updateViewsWithMamhaoData:[MMHMamhaoDataManager sharedDataManager].data];
            headerView.delegate = self;
            self.productHeaderView = headerView;
            return headerView;
        }

    }
    return nil;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ((MMHMamhaoViewControllerSection) (indexPath.section)) {
        case MMHMamhaoViewControllerSectionPromotion: {
            switch ((MMHMamhaoPromotionCellRow) (indexPath.row)) {
                case MMHMamhaoPromotionCellRowAnnouncement: {
                    MMHWebShakeMBeanViewController *webViewController = [[MMHWebShakeMBeanViewController alloc] initWithURLString:MMHHTML_Link_Announcement title:nil];
                    [self pushViewController:webViewController];
                    break;
                }
                case MMHMamhaoPromotionCellRowBeanSpecial: {
                    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoBeanSpecial];
                    MMHMamhaoBeanSpecialProduct *beanSpecialProduct = [[MMHMamhaoDataManager sharedDataManager].data.beanSpecials firstObject];
                    if (beanSpecialProduct) {
                        if (beanSpecialProduct.templateId.length != 0) {
                            MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:beanSpecialProduct];
                            [self.navigationController pushViewController:productDetailViewController animated:YES];
                        }
                    }
                    break;
                }
//                case MMHMamhaoPromotionCellRowMamhaoFeatured: {
//                    MMHMamhaoMamhaoFeaturedProduct *mamhaoFeaturedProduct = [[MMHMamhaoDataManager sharedDataManager].data.mamhaoFeatured firstObject];
//                    if (mamhaoFeaturedProduct) {
//                        NSString *urlString = mamhaoFeaturedProduct.contentURLString;
//                        MMHWebViewController *webViewController = [[MMHWebViewController alloc] initWithURLString:urlString title:@"万众瞩目"];
//                        [self pushViewController:webViewController];
//                    }
//                    
//                    break;
//                }
                case MMHMamhaoPromotionCellRowEditProfile: {
                    if ([MMHMamhaoEditProfileCell childConfiguredWithMamhaoData:[[MMHMamhaoDataManager sharedDataManager] data]]) {
                        return;
                    }
                    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoEditProfile];
                    MMHEditBabyInfoWithHomeViewController *editBabyInfoWithHomeViewController = [[MMHEditBabyInfoWithHomeViewController alloc] init];
                    MMHNavigationController *navigationController = [[MMHNavigationController alloc] initWithRootViewController:editBabyInfoWithHomeViewController];
                    [self presentViewController:navigationController animated:YES completion:^{

                    }];
                    break;
                }
                default: {
                    break;
                }
            }
            break;
        }
        case MMHMamhaoViewControllerSectionProduct: {
            [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoFeaturedProducts];
            if ([[MMHMamhaoDataManager sharedDataManager] isIndexFeatured:indexPath.row]) {
                MMHMamhaoFeaturedProductCell *cell = (MMHMamhaoFeaturedProductCell *)[tableView cellForRowAtIndexPath:indexPath];
                MMHMamhaoProduct *product = cell.product;
                MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
                [self pushViewController:productDetailViewController];
            }
            else {
                MMHMamhaoProductCell *cell = (MMHMamhaoProductCell *)[tableView cellForRowAtIndexPath:indexPath];
                MMHMamhaoProduct *product = cell.product;
                MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
                [self pushViewController:productDetailViewController];
            }
            break;
        }
        default: {
            break;
        }
    }
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    NSLog(@"--->>> 111");
    if (decelerate) {
        return;
    }

    [self updateProductHeaderViewSelections];
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"--->>> 222");

    [self updateProductHeaderViewSelections];

}


- (void)updateProductHeaderViewSelections {
//    UITableViewCell *cell = [self.tableView.visibleCells firstObject];
//    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    CGPoint contentOffset = self.tableView.contentOffset;
    contentOffset.y += 75.0f;
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:contentOffset];
    NSLog(@"got indexpath: %d, %d" , indexPath.section, indexPath.row);
    
    if (indexPath.section != MMHMamhaoViewControllerSectionProduct) {
        return;
    }

    MMHMamhaoProductType productType = [[MMHMamhaoDataManager sharedDataManager] productTypeAtIndex:indexPath.row];
    [self.productHeaderView setSelectedProductType:productType];
}


#pragma mark - MMHMamhaoNavigationBarTitleView delegate


- (void)titleViewTapped:(MMHMamhaoNavigationBarTitleView *)titleView
{
    if ([MMHMamhaoDataManager sharedDataManager].fetching) {
        return;
    }

//    MMHSearchAddressViewController *searchAddressViewController = [[MMHSearchAddressViewController alloc] init];
//    searchAddressViewController.transferAddressSingleModel = [[MMHAddressSingleModel alloc] init];
//    searchAddressViewController.locationModelBlock = ^(MMHAddressSingleModel *addressParameterModel) {
//        [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = addressParameterModel;
//        [self refreshLocationBasedData];
//    };
    
    MMHChangeAddressViewController *changeAddressViewController = [[MMHChangeAddressViewController alloc]init];
    changeAddressViewController.changeAddressPageUsingType = MMHChangeAddressPageUsingTypeHome;
    changeAddressViewController.changedAddressBlock = ^(MMHAddressSingleModel *addressSingleModel){
//        [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = addressSingleModel;
        [self refreshLocationBasedData];
        
    };
    [self pushViewController:changeAddressViewController];
}


#pragma mark - MMHMamhaoPromotionCellDelegate


- (void)mamhaoPromotionCellDidBeginSearching:(MMHMamhaoPromotionCell *)mamhaoPromotionCell
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoSearch];
    MMHSearchViewController *searchViewController = [[MMHSearchViewController alloc] init];
    [self.navigationController pushViewController:searchViewController animated:YES];
}


- (void)mamhaoPromotionCellDidSelectBeanGame:(MMHMamhaoPromotionCell *)mamhaoPromotionCell {
    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        __weak __typeof(self) weakSelf = self;
        [self presentLoginViewControllerWithSucceededHandler:^() {
            [weakSelf pushBeanGameViewController];
        }];
    }
    else {
        [self pushBeanGameViewController];
    }
}


- (void)pushBeanGameViewController
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoBeanGame];
    NSString *remoteURL = MMHHTML_Link_BeanGame;
    NSString *memberID = [MMHAccountSession currentSession].memberID;
    NSString *encryptMemberID = [memberID encryptString];
    NSString *fullURLString = [NSString stringWithFormat:@"%@?memberId=%@&memberKey=%@", remoteURL, memberID, encryptMemberID];
//    NSLog(@"xxx: %@", fullURLString);
    MMHWebShakeMBeanViewController * smartVC = [[MMHWebShakeMBeanViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:fullURLString];
    [self.navigationController pushViewController:smartVC animated:YES];
}


- (void)mamhaoPromotionCellDidSelectRedEnvelopGame:(MMHMamhaoPromotionCell *)mamhaoPromotionCell
{

}


- (void)mamhaoPromotionCellDidSelectCategory:(MMHMamhaoPromotionCell *)mamhaoPromotionCell {
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoCategory];
    MMHClassificationViewController *classificationViewController = [[MMHClassificationViewController alloc] init];
    [self.navigationController pushViewController:classificationViewController animated:YES];
}


- (void)mamhaoPromotionCellDidSelectBeanGuide:(MMHMamhaoPromotionCell *)mamhaoPromotionCell
{
    MMHWebViewController *webViewController = [[MMHWebViewController alloc] initWithURLString:MMHHTML_Link_Beans title:nil];
    [self pushViewController:webViewController];
}


- (void)mamhaoPromotionCellDidSelectLocation:(MMHMamhaoPromotionCell *)mamhaoPromotionCell
{
    MMHChangeAddressViewController *changeAddressViewController = [[MMHChangeAddressViewController alloc]init];
    changeAddressViewController.changeAddressPageUsingType = MMHChangeAddressPageUsingTypeHome;
    changeAddressViewController.changedAddressBlock = ^(MMHAddressSingleModel *addressSingleModel){
        [self refreshLocationBasedData];
        
    };
    [self pushViewController:changeAddressViewController];
}


- (void)mamhaoPromotionCell:(MMHMamhaoPromotionCell *)mamhaoPromotionCell didSelectShop:(MMHMamhaoShop *)shop
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoShop];
    MMHShopDetailViewController *shopDetailViewController = [[MMHShopDetailViewController alloc] initWithShopId:shop.shopId];
    [self pushViewController:shopDetailViewController];
}



- (void)productHeaderViewDidSelectMonthAge:(MMHMamhaoPromotionCell *)mamhaoPromotionCell
{
    [self selectMonthAge];
}


- (void)mamhaoPromotionCell:(MMHMamhaoPromotionCell *)mamhaoPromotionCell didSelectUserFeaturedProduct:(MMHMamhaoUserFeaturedProduct *)product
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoUserFeatured];
    MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
    [self pushViewController:productDetailViewController];
}


#pragma mark - MMHMamhaoProductHeaderViewDelegate


- (void)productHeaderViewWillChangeMonthAge:(MMHMamhaoProductHeaderView *)mamhaoProductHeaderView
{
    [self selectMonthAge];
}


- (void)productHeaderView:(MMHMamhaoProductHeaderView *)mamhaoProductHeaderView didSelectProductType:(MMHMamhaoProductType)productType
{
    NSInteger index = [[MMHMamhaoDataManager sharedDataManager] productIndexOfType:productType];
    if (index == -1) {
        [self.view showTips:@"该栏目商品正在上架中，敬请期待"];
        return;
    }

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:MMHMamhaoViewControllerSectionProduct];
    CGRect rect = [self.tableView rectForRowAtIndexPath:indexPath];
//    CGRect rect = [self.tableView rectForHeaderInSection:MMHMamhaoViewControllerSectionProduct];
    CGPoint contentOffset = rect.origin;
    contentOffset.y -= 75.0f;
    contentOffset.y = MIN(contentOffset.y, self.tableView.contentSize.height - self.tableView.frame.size.height);
    [self.tableView setContentOffset:contentOffset animated:YES];
    return;
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:0
                     animations:^{
                         self.tableView.contentOffset = rect.origin;
                     } completion:^(BOOL finished) {
//                MMHMonthAgeSelectionView *monthAgeSelectionView = [[MMHMonthAgeSelectionView alloc] initWithFrame:self.view.bounds];
//                monthAgeSelectionView.delegate = self;
//                [self.view addSubview:monthAgeSelectionView];
            }];


//    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:YES];
}


#pragma mark - MMHMamhaoProductCellDelegate


- (void)mamhaoProductCellWillAddToCart:(MMHMamhaoProductCell *)mamhaoProductCell
{
    // add ni ma bi, qi lai high
}


#pragma mark - MMHMonthSelectionViewDelegate


- (void)monthAgeSelectionView:(MMHMonthAgeSelectionView *)monthAgeSelectionView didSelectMonthAge:(MonthAge *)monthAge
{
    if ([MMHMamhaoDataManager sharedDataManager].fetching) {
        return;
    }

    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHMamhaoDataManager sharedDataManager] fetchDataWithMonthAge:monthAge
                                                             completion:^(BOOL succeeded, MMHMamhaoData *data, NSError *error) {
                                                                 if (succeeded) {
                                                                     [weakSelf updateViews];
                                                                 }
                                                                 [weakSelf.view hideProcessingView];
                                                                 if (error) {
                                                                     [weakSelf.view showTipsWithError:error];
                                                                 }
                                                             }];
}


#pragma mark - Actions


- (void)selectMonthAge
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoChangeMonthAge];
    if ([MMHMamhaoDataManager sharedDataManager].fetching) {
        return;
    }

//    if (self.tableView.numberOfSections < 2) {
        MMHMonthAgeSelectionView *monthAgeSelectionView = [[MMHMonthAgeSelectionView alloc] initWithFrame:self.view.bounds];
        monthAgeSelectionView.delegate = self;
        [self.view addSubview:monthAgeSelectionView];
        return;
//    }
//
//    CGRect rect = [self.tableView rectForHeaderInSection:MMHMamhaoViewControllerSectionProduct];
//    [UIView animateWithDuration:0.2
//                          delay:0.0
//                        options:UIViewAnimationOptionCurveEaseOut
//                     animations:^{
//                         self.tableView.contentOffset = rect.origin;
//                     } completion:^(BOOL finished) {
//                MMHMonthAgeSelectionView *monthAgeSelectionView = [[MMHMonthAgeSelectionView alloc] initWithFrame:self.view.bounds];
//                monthAgeSelectionView.delegate = self;
//                [self.view addSubview:monthAgeSelectionView];
//            }];
}


- (void)scan:(UIBarButtonItem *)scanBarButtonItem {
    scanBarButtonItem.enabled = NO;
    MMHScanningViewController *scanningViewController = [[MMHScanningViewController alloc]init];
    scanningViewController.closeCameraBlock = ^(){
        [[UIAlertView alertViewWithTitle:@"亲，您长时间未使用摄像头，妈妈好已为您关闭，避免机器损坏" message:nil buttonTitles:@[@"确定"] callback:nil] show];
    };
    [self.navigationController pushViewController:scanningViewController animated:YES];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        scanBarButtonItem.enabled = YES;
    });
}


- (void)viewMessages
{
    [MMHLogbook logEventType:MMHBuriedPointTypeMamhaoMessages];
    self.navigationController.navigationItem.rightBarButtonItem.enabled = NO;
    [self startChattingWithContext:nil];
}


- (void)refreshLocationBasedData
{
    if ([MMHMamhaoDataManager sharedDataManager].fetching) {
        return;
    }

    [self.view showProcessingView];
    __weak __typeof(self) weakSelf = self;
    [[MMHMamhaoDataManager sharedDataManager] fetchLocationBasedDataCompletion:^(BOOL succeeded, MMHMamhaoData *data, NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!succeeded) {
            if (error) {
                [weakSelf.view showTipsWithError:error];
            }
            return;
        }
        [weakSelf updateViews];
    }];
}


- (void)userDidLogout:(NSNotification *)notification
{
    [self fetchData];
}


- (void)childInfoEdited:(NSNotification *)notification
{
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:MMHMamhaoPromotionCellRowEditProfile inSection:MMHMamhaoViewControllerSectionPromotion];
//    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    [self.tableView reloadData];
}



#pragma mark - Ares
-(void)showBabyInfo{
    NSString *isShowAnimation = [MMHTool userDefaultGetWithKey:@"isShowAnimation"];
    if ([isShowAnimation isEqualToString:@"Yes"]){
        MMHWriteBabyInfoAnimationViewController *writeBabyInfoAnimationViewController = [[MMHWriteBabyInfoAnimationViewController alloc]init];
        UINavigationController *animationBabyViewController = [[UINavigationController alloc]initWithRootViewController:writeBabyInfoAnimationViewController];
        [self.navigationController presentViewController:animationBabyViewController animated:YES completion:nil];
        [MMHTool userDefaulteWithKey:@"isShowAnimation" Obj:@"No"];
    }
}


@end
