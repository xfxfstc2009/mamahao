//
//  MMHMamhaoUserFeaturedProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoUserFeaturedProduct.h"


@implementation MMHMamhaoUserFeaturedProduct


- (instancetype)initWithJSONDict:(NSDictionary *)dict
{
    self = [super initWithJSONDict:dict];
    if (self) {
        self.nickname = dict[@"nickName"];
        self.avatarURLString = dict[@"headPic"];
        self.monthAge = [dict[@"babyAge"] integerValue];
        self.dateString = dict[@"commentTime"];
        // content is just ready
        NSArray *thePicture = dict[@"pictures"];
        if ([thePicture isKindOfClass:[NSString class]]) {
            self.imageURLStrings = @[thePicture];
        }
        else {
            self.imageURLStrings = thePicture;
        }
        self.boughtCount = [dict[@"totalSale"] integerValue];
        self.likedCount = [dict[@"totalCollect"] integerValue];
        
//        NSTimeInterval timeInterval = [dict doubleForKey:@"timestamp"];
//        self.date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
//        self.imageURLStrings = dict[@"images"];
    }
    return self;
}


//- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
//{
//    self = [super initWithJSONDict:dict keyMap:keyMap];
//    if (self) {
//        self.avatarURLString = dict[@"avatar"];
//        NSTimeInterval timeInterval = [dict doubleForKey:@"timestamp"];
//        self.date = [NSDate dateWithTimeIntervalSince1970:timeInterval];
//        self.imageURLStrings = dict[@"images"];
//    }
//    return self;
//}


- (MMHProductModule)module
{
    return MMHProductModuleMamhaoUserFeatured;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleMamhaoUserFeatured) {
        return @{};
    }

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    dictionary[@"templateId"] = self.templateId;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    dictionary[@"deviceId"] = [UIDevice deviceID];

    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"user featured parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
