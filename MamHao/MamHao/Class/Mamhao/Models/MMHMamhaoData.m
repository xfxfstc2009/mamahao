//
//  MMHMamhaoData.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoData.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHMamhaoShop.h"
#import "MMHMamhaoCustomer.h"
#import "MMHMamhaoUserFeaturedProduct.h"
#import "MMHMamhaoProduct.h"
#import "MamHao-Swift.h"
#import "MMHMamhaoUserFeaturedData.h"
#import "MMHProfile.h"


@implementation MMHMamhaoData


- (void)setBeanSpecials:(NSArray *)beanSpecials
{
    _beanSpecials = beanSpecials;
    self.beanSpecialsConfigured = YES;
}


- (void)setShops:(NSArray *)shops
{
    _shops = shops;
    self.shopsConfigured = YES;
}


- (void)setMamhaoFeatured:(NSArray *)mamhaoFeatured
{
    _mamhaoFeatured = mamhaoFeatured;
    self.mamhaoFeaturedConfigured = YES;
}


- (void)setUserFeaturedData:(MMHMamhaoUserFeaturedData *)userFeaturedData
{
    _userFeaturedData = userFeaturedData;
    self.userFeaturedDataConfigured = YES;
}


- (void)setProducts:(NSArray *)products
{
    _products = products;
    self.productsConfigured = YES;
}


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.beanSpecials = [dict[@"mbeanSpecials"] modelArrayOfClass:[MMHMamhaoBeanSpecialProduct class]];
        self.shops = [dict[@"shops"] modelArrayOfClass:[MMHMamhaoShop class]];
        self.customers = [dict[@"customers"] modelArrayOfClass:[MMHMamhaoCustomer class]];
        self.mamhaoFeatured = [dict[@"mamhaoFeatured"] modelArrayOfClass:[MMHMamhaoMamhaoFeaturedProduct class]];
        self.userFeaturedData = [[MMHMamhaoUserFeaturedData alloc] initWithJSONDict:dict[@"userFeatured"] keyMap:nil];

        NSDictionary *productsDictionary = dict[@"products"];
        [self configureProductsAndMonthAgeWithDictionary:productsDictionary];
    }
    return self;
}


- (void)configureProductsAndMonthAgeWithDictionary:(NSDictionary *)dictionary
{
    if ([dictionary count] == 0) {
        self.products = nil;
        return;
    }

    NSString *monthAgeString = [dictionary stringForKey:@"applyAge"];
    if (monthAgeString.length == 0) {
        self.monthAgeForProducts = [[MonthAge alloc] initWithString:@"-2"];
    }
    else {
        self.monthAgeForProducts = [[MonthAge alloc] initWithString:monthAgeString];
    }
    NSArray *featured = [dictionary[@"featured"] modelArrayOfClass:[MMHMamhaoProduct class]];
    NSArray *pureUsing = [dictionary[@"using"] modelArrayOfClass:[MMHMamhaoProduct class]];
    NSArray *wear = [dictionary[@"wear"] modelArrayOfClass:[MMHMamhaoProduct class]];
    NSArray *forMom = [dictionary[@"forMom"] modelArrayOfClass:[MMHMamhaoProduct class]];

    NSArray *food = [dictionary[@"food"] modelArrayOfClass:[MMHMamhaoProduct class]];
    NSArray *toy = [dictionary[@"toy"] modelArrayOfClass:[MMHMamhaoProduct class]];
    NSArray *education = [dictionary[@"education"] modelArrayOfClass:[MMHMamhaoProduct class]];
    
    NSArray *using = [pureUsing arrayByAddingObjectsFromArray:toy];

    NSMutableArray *products = [NSMutableArray array];
    self.startIndexOfFeatured = products.count;
    [products addObjectsFromArray:featured];
    self.endIndexOfFeatured = products.count;
    self.startIndexOfFood = -1;
//    [products addObjectsFromArray:food];
    self.startIndexOfToy = -1;
//    [products addObjectsFromArray:toy];
    self.startIndexOfUsing = products.count;
    [products addObjectsFromArray:using];
    self.startIndexOfWear = products.count;
    [products addObjectsFromArray:wear];
    self.startIndexOfEducation = -1;
//    [products addObjectsFromArray:education];
    self.startIndexOfForMom = products.count;
    [products addObjectsFromArray:forMom];
    self.products = products;
}


- (void)configureUserInfoWithDictionary:(NSDictionary *)dictionary
{
    NSLog(@"the dictionary: %@", dictionary);
    self.beanBalance = [dictionary integerForKey:@"mbeanCount"];
    MMHUserIdentity userIdentity = (MMHUserIdentity)[dictionary integerForKey:@"memberStatus"];
    NSDictionary *babyDictionary = dictionary[@"baby"];
    MMHProfile *profile = [[MMHProfile alloc] initWithUserIdentity:userIdentity childDictionary:babyDictionary];
    self.profile = profile;

    self.userInfoConfigured = YES;
}


- (BOOL)isReady
{
    if (!self.beanSpecialsConfigured) {
        return NO;
    }

//    if (!self.mamhaoFeaturedConfigured) {
//        return NO;
//    }

    if (!self.userFeaturedDataConfigured) {
        return NO;
    }

    if (!self.shopsConfigured) {
        return NO;
    }

    if (!self.productsConfigured) {
        return NO;
    }

    if (!self.userInfoConfigured) {
        return NO;
    }

    return YES;
}
@end
