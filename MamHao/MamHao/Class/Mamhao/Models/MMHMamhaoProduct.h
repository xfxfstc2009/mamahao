//
//  MMHMamhaoProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@class MonthAge;


@interface MMHMamhaoProduct : MMHProduct <MMHProductProtocol>

//@property (nonatomic, copy) NSString *templateId;
@property (nonatomic, copy) NSString *itemId;
@property (nonatomic, copy) NSString *featuredImageURLString;
@property (nonatomic, copy) NSString *imageURLString;
@property (nonatomic, copy) NSString *name;
@property (nonatomic) MMHPrice price;
@property (nonatomic) long soldCount;

@property (nonatomic, strong) MonthAge *monthAge;
@property (nonatomic, copy) NSString *brandId;

@property (nonatomic) BOOL platformQualityInspected;
@property (nonatomic) BOOL isShop;

@property (nonatomic, copy) NSString *title;
- (NSString *)displayNameAsFeaturedProduct;

@end
