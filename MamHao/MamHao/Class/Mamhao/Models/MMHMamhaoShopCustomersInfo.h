//
//  MMHMamhaoShopCustomersInfo.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"


@class MMHMamhaoShop;


@interface MMHMamhaoShopCustomersInfo : MMHFetchModel

+ (MMHMamhaoShopCustomersInfo *)sharedInfo;

- (void)fetchMamhaoCustomersNearShop:(MMHMamhaoShop *)shop completion:(void (^)(BOOL succeeded, long count, NSArray *customers, NSError *error))completion;
@end
