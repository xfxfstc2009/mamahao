//
//  MMHMamhaoDataManager.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSInteger, MMHMamhaoProductType) {
    MMHMamhaoProductTypeFeatured,
    MMHMamhaoProductTypeUsing,
    MMHMamhaoProductTypeWear,
    MMHMamhaoProductTypeForMom,

    MMHMamhaoProductTypeFood,
    MMHMamhaoProductTypeToy,
    MMHMamhaoProductTypeEducation,
};


@class MMHMamhaoData;
@class MMHMamhaoProduct;
@class MonthAge;
@class MMHMamhaoDataManager;


@protocol MMHMamhaoDataManagerDelegate <NSObject>

- (void)dataManager:(MMHMamhaoDataManager *)dataManager dataChanged:(MMHMamhaoData *)data;

@end


@interface MMHMamhaoDataManager : NSObject

@property (nonatomic, weak) id<MMHMamhaoDataManagerDelegate> delegate;

+ (MMHMamhaoDataManager *)sharedDataManager;
@property (nonatomic, strong, readonly) MMHMamhaoData *data;

- (void)fetchMamhaoDataCompletion:(void (^)(BOOL succeeded, MMHMamhaoData *mamhaoData, NSError *error))completion;
@property (nonatomic, readonly, getter=isDataFetched) BOOL dataFetched;
@property (nonatomic, readonly) BOOL fetching;

- (BOOL)hasProducts;
- (NSInteger)productCount;
- (MMHMamhaoProduct *)productAtIndex:(NSInteger)index;

- (NSArray *)productTypeNames;

- (NSInteger)productIndexOfType:(MMHMamhaoProductType)productType;

- (BOOL)isIndexFeatured:(NSInteger)index;

- (void)fetchDataWithMonthAge:(MonthAge *)monthAge completion:(void (^)(BOOL succeeded, MMHMamhaoData *mamhaoData, NSError *error))completion;
- (void)fetchLocationBasedDataCompletion:(void (^)(BOOL, MMHMamhaoData *, NSError *))completion;


@property (nonatomic, copy) NSString *beanGameImageURLString;
@property (nonatomic, copy) NSString *categoryImageURLString;
- (void)fetchActivityImagesCompletion:(void (^)(BOOL, NSString *, NSString *, NSError *))completion;


- (MMHMamhaoProductType)productTypeAtIndex:(NSInteger)index;
@end
