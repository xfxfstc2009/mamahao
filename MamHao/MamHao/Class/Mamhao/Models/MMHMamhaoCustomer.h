//
//  MMHMamhaoCustomer.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
@import CoreLocation;@class MMHMamhaoShop;


@interface MMHMamhaoCustomer : MMHFetchModel

@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *deviceId;
@property (nonatomic) CLLocation *location;
@property (nonatomic, copy) NSString *avatarURLString;
@property (nonatomic, strong) NSDate *lastUpdatedDate;

- (CLLocationDistance)distanceFromShop:(MMHMamhaoShop *)shop;
@end
