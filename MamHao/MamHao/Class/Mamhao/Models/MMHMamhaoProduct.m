//
//  MMHMamhaoProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoProduct.h"
#import "MamHao-Swift.h"


@implementation MMHMamhaoProduct

- (instancetype)initWithJSONDict:(NSDictionary *)dict
{
    self = [super initWithJSONDict:dict];
    if (self) {
        self.templateId = [dict stringForKey:@"styleNumId"];
        self.monthAge = [[MonthAge alloc] initWithString:[dict stringForKey:@"applyAge"]];
        self.brandId = [dict stringForKey:@"brandId"];
        self.imageURLString = dict[@"pic"];
        self.name = dict[@"itemName"];
        self.soldCount = [dict[@"totalSale"] longValue];
        self.platformQualityInspected = [dict[@"checkStatus"] boolValue];
        if (![dict hasKey:@"price"]) {
            self.price = [dict[@"minPrice"] priceValue];
        }
        self.featuredImageURLString = [dict stringForKey:@"activityPic"];
    }
    return self;
}


- (NSString *)displayNameAsFeaturedProduct
{
    if (self.title.length != 0) {
        return self.title;
    }
    
    return self.name;
}


- (MMHProductModule)module
{
    return MMHProductModuleMamhaoFeatured;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleMamhaoFeatured) {
        return @{};
    }

    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];

    dictionary[@"templateId"] = self.templateId;
    if (self.itemId) {
        
        dictionary[@"itemId"] = self.itemId;
    }
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    dictionary[@"deviceId"] = [UIDevice deviceID];

    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"mamhao product parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
