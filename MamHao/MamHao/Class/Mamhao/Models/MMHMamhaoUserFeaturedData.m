//
//  MMHMamhaoUserFeaturedData.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoUserFeaturedData.h"
#import "MMHMamhaoUserFeaturedProduct.h"


@implementation MMHMamhaoUserFeaturedData


//- (instancetype)initWithJSONDict:(NSDictionary *)dict
//{
//    self = [super initWithJSONDict:dict];
//    if (self) {
//        NSArray *productDictionaries = dict[@"data"];
//        if ([productDictionaries isKindOfClass:[NSArray class]]) {
//            self.products = [productDictionaries modelArrayOfClass:[MMHMamhaoUserFeaturedProduct class]];
//        }
//        self.age = [dict integerForKey:@"babyAge"];
//    }
//    return self;
//}


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.age = [dict integerForKey:@"babyAge"];
        NSArray *productDictionaries = dict[@"data"];
        if ([productDictionaries isKindOfClass:[NSArray class]]) {
            self.products = [productDictionaries modelArrayOfClass:[MMHMamhaoUserFeaturedProduct class]];
        }
        for (MMHMamhaoUserFeaturedProduct *product in self.products) {
            product.monthAge = self.age;
        }
    }
    return self;
}


@end
