//
//  MMHMamhaoShop.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoShop.h"
#import "MMHCurrentLocationModel.h"
@import CoreLocation;


@implementation MMHMamhaoShopBrand

- (instancetype)initWithJSONDict:(NSDictionary *)dict
{
    self = [super initWithJSONDict:dict];
    if (self) {
        self.imageURLString = [dict stringForKey:@"brandPic"];
    }
    return self;
}

@end


@implementation MMHMamhaoShop


- (double)distance
{
    CLLocation *myLocation = [[MMHCurrentLocationModel sharedLocation] myGeographicalLocation];
    return [self.location distanceFromLocation:myLocation];
}


- (instancetype)initWithJSONDict:(NSDictionary *)dict
{
    self = [super initWithJSONDict:dict];
    if (self) {
        self.shopId = [dict stringForKey:@"shopId"];
        self.imageURLString = dict[@"outPageLogoPic"];
        self.brands = [dict[@"brands"] modelArrayOfClass:[MMHMamhaoShopBrand class]];
        self.areaNumId = [dict stringForKey:@"areaNumId"];
        self.cityNumId = [dict stringForKey:@"cityNumId"];
        NSArray *location = dict[@"gps"];
        if (location.count == 2) {
            CLLocationDegrees longitude = [location[0] doubleValue];
            CLLocationDegrees latitude = [location[1] doubleValue];
            self.location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
        }
        self.shopDetailImageURLString = [dict stringForKey:@"indexLogoPic"];
        self.shopType = [dict integerForKey:@"subUnitType"];
        self.entityType = [dict integerForKey:@"type"];
        
        NSString *name = [dict stringForKey:@"onlineName"];
        if (name.length == 0) {
            name = [dict stringForKey:@"name"];
        }
        self.name = name;
    }
    return self;
}


@end
