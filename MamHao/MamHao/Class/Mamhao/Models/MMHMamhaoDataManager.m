//
//  MMHMamhaoDataManager.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/2.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoDataManager.h"
#import "MMHMamhaoData.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Mamhao.h"
#import "MMHMamhaoProduct.h"
#import "UIImage+RenderedImage.h"
#import "UIFont+MMHCustomerFont.h"
#import "MMHNetworkAdapter+Center.h"
#import "MMHNetworkAdapter+Product.h"
#import "MMHLogger.h"


@interface MMHMamhaoDataManager ()

@property (nonatomic, strong) MMHMamhaoData *data;
@property (nonatomic) BOOL dataFetched;
@property (nonatomic, copy) void (^completion)(BOOL, MMHMamhaoData *, NSError *);
@property (nonatomic, readwrite) BOOL fetching;
@end


@implementation MMHMamhaoDataManager


+ (MMHMamhaoDataManager *)sharedDataManager
{
    static MMHMamhaoDataManager *sharedDataManager_;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedDataManager_ = [[MMHMamhaoDataManager alloc] init];
    });
    return sharedDataManager_;
}


- (void)fetchMamhaoDataCompletion:(void (^)(BOOL succeeded, MMHMamhaoData *mamhaoData, NSError *error))completion
{
    if (self.fetching) {
        NSError *error = [[NSError alloc] initWithDomain:MMHErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"正在刷新中"}];
        completion(NO, nil, error);
        return;
    }

    self.completion = completion;
    self.fetching = YES;
    MMHMamhaoData *data = [[MMHMamhaoData alloc] init];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoBeanSpecialDataFrom:self
                                                           completion:^(BOOL succeeded, MMHMamhaoBeanSpecialProduct *beanSpecialProduct, NSError *error) {
                                                               if (beanSpecialProduct == nil) {
                                                                   data.beanSpecials = nil;
                                                               }
                                                               else {
                                                                   data.beanSpecials = @[beanSpecialProduct];
                                                               }
                                                               [weakSelf tryToReportFinishWithMamhaoData:data];
                                                           }];

//    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoMamhaoFeaturedProductFrom:self
//                                                           completion:^(BOOL succeeded, MMHMamhaoMamhaoFeaturedProduct *mamhaoMamhaoFeaturedProduct, NSError *error) {
//                                                               if (mamhaoMamhaoFeaturedProduct == nil) {
//                                                                   data.mamhaoFeatured = nil;
//                                                               }
//                                                               else {
//                                                                   data.mamhaoFeatured = @[mamhaoMamhaoFeaturedProduct];
//                                                               }
//                                                               [weakSelf tryToReportFinishWithMamhaoData:data];
//                                                           }];
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoShopsFrom:self
                                                 completion:^(BOOL succeeded, NSArray *shops, NSError *error) {
                                                     data.shops = shops;
                                                     [weakSelf tryToReportFinishWithMamhaoData:data];
                                                 }];
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoUserFeaturedDataWithMonthAge:nil
                                                                          from:self
                                                                    completion:^(BOOL succeeded, MMHMamhaoUserFeaturedData *userFeaturedData, NSError *error) {
                                                                        data.userFeaturedData = userFeaturedData;
                                                                        [weakSelf tryToReportFinishWithMamhaoData:data];
                                                                    }];
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoFeaturedProductsForMonthAge:nil
                                                                         from:self
                                                                   completion:^(BOOL succeeded, NSDictionary *dictionary, NSError *error) {
                                                                       [data configureProductsAndMonthAgeWithDictionary:dictionary];
                                                                       [weakSelf tryToReportFinishWithMamhaoData:data];
                                                                   }];

    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoUserInfoFrom:self
                                                    completion:^(BOOL succeeded, NSDictionary *dictionary, NSError *error) {
                                                        [data configureUserInfoWithDictionary:dictionary];
                                                        [weakSelf tryToReportFinishWithMamhaoData:data];
                                                    }];
//    [[MMHNetworkAdapter sharedAdapter] fetchModelGetMamBeanInfoWithPage:1
//                                                               pageSize:20
//                                                                   from:self
//                                                       succeededHandler:^(MMHBearListModel *beanListModel) {
//                                                           data.beanBalance = beanListModel.mbeanCount;
//                                                           [weakSelf tryToReportFinishWithMamhaoData:data];
//                                                       } failedHandler:^(NSError *error) {
//                data.beanBalance = 0;
//                [weakSelf tryToReportFinishWithMamhaoData:data];
//            }];

//    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoDataFrom:self
//                                          succeededHandler:^(MMHMamhaoData *data) {
//                                              self.data = data;
//                                              self.dataFetched = YES;
//
//                                              [self fetchProductsWithMonthAge:nil
//                                                                   completion:^(BOOL succeeded, MMHMamhaoData *mamhaoData, NSError *error) {
//                                                                       if (completion) {
//                                                                           completion(YES, data, nil);
//                                                                       }
//                                                                   }];
//
//                                          } failedHandler:^(NSError *error) {
//                if (completion) {
//                    completion(NO, self.data, error);
//                }
//            }];
}


- (void)tryToReportFinishWithMamhaoData:(MMHMamhaoData *)data
{
    if ([data isReady]) {
        self.data = data;
        self.dataFetched = YES;
        if (self.completion) {
            self.completion(YES, data, nil);
        }
        self.completion = nil;
        self.fetching = NO;
    }
}


- (BOOL)hasProducts
{
    return self.data.products.count != 0;
}


- (NSInteger)productCount
{
    return self.data.products.count;
}


- (MMHMamhaoProduct *)productAtIndex:(NSInteger)index
{
    return [self.data.products nullableObjectAtIndex:index];
}


- (NSArray *)productTypeNames
{
    return @[@"推荐", @"用什么", @"穿什么", @"宝妈专区", @"吃什么", @"玩什么", @"爱学习"];
}


- (NSInteger)productIndexOfType:(MMHMamhaoProductType)productType
{
    NSInteger index = 0;
    switch (productType) {
        case MMHMamhaoProductTypeFeatured:
            index = self.data.startIndexOfFeatured;
        break;
        case MMHMamhaoProductTypeFood:
            index = self.data.startIndexOfFood;
        break;
        case MMHMamhaoProductTypeToy:
            index = self.data.startIndexOfToy;
        break;
        case MMHMamhaoProductTypeUsing:
            index = self.data.startIndexOfUsing;
        break;
        case MMHMamhaoProductTypeWear:
            index = self.data.startIndexOfWear;
        break;
        case MMHMamhaoProductTypeEducation:
            index = self.data.startIndexOfEducation;
        break;
        case MMHMamhaoProductTypeForMom:
            index = self.data.startIndexOfForMom;
        break;
        default:
        break;
    }
    if (index == self.data.products.count) {
        index = MAX(index - 1, 0);
    }
    return index;
}


- (MMHMamhaoProductType)productTypeAtIndex:(NSInteger)index {
//    if (index >= self.data.startIndexOfEducation) {
//        return MMHMamhaoProductTypeEducation;
//    }
//    if (index >= self.data.startIndexOfToy) {
//        return MMHMamhaoProductTypeToy;
//    }
//    if (index >= self.data.startIndexOfFood) {
//        return MMHMamhaoProductTypeFood;
//    }
    if (index >= self.data.startIndexOfForMom) {
        return MMHMamhaoProductTypeForMom;
    }
    if (index >= self.data.startIndexOfWear) {
        return MMHMamhaoProductTypeWear;
    }
    if (index >= self.data.startIndexOfUsing) {
        return MMHMamhaoProductTypeUsing;
    }
    if (index >= self.data.startIndexOfFeatured) {
        return MMHMamhaoProductTypeFeatured;
    }
    return MMHMamhaoProductTypeFeatured;
}


- (BOOL)isIndexFeatured:(NSInteger)index
{
    return index < self.data.endIndexOfFeatured;
}


- (void)fetchDataWithMonthAge:(MonthAge *)monthAge completion:(void (^)(BOOL succeeded, MMHMamhaoData *mamhaoData, NSError *error))completion
{
    if (self.fetching) {
        completion(NO, nil, nil);
        return;
    }

    self.completion = completion;
    self.fetching = YES;
    self.data.userFeaturedDataConfigured = NO;
    self.data.productsConfigured = NO;
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoUserFeaturedDataWithMonthAge:monthAge
                                                                          from:self
                                                                    completion:^(BOOL succeeded, MMHMamhaoUserFeaturedData *userFeaturedData, NSError *error) {
                                                                        weakSelf.data.userFeaturedData = userFeaturedData;
                                                                        [weakSelf tryToReportFinishWithMamhaoData:self.data];
                                                                    }];
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoFeaturedProductsForMonthAge:monthAge
                                                                         from:self
                                                                   completion:^(BOOL succeeded, NSDictionary *dictionary, NSError *error) {
                                                                       [weakSelf.data configureProductsAndMonthAgeWithDictionary:dictionary];
                                                                       [weakSelf tryToReportFinishWithMamhaoData:self.data];
                                                                   }];
}


- (void)fetchLocationBasedDataCompletion:(void (^)(BOOL, MMHMamhaoData *, NSError *))completion
{
    if (self.fetching) {
        completion(NO, nil, nil);
        return;
    }

    self.completion = completion;
    self.fetching = YES;
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoShopsFrom:self
                                                 completion:^(BOOL succeeded, NSArray *shops, NSError *error) {
                                                     weakSelf.data.shops = shops;
                                                     [weakSelf tryToReportFinishWithMamhaoData:weakSelf.data];
                                                 }];
}


- (void)fetchActivityImagesCompletion:(void (^)(BOOL, NSString *, NSString *, NSError *))completion
{
    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoActivityImagesFrom:self
                                                          completion:^(BOOL succeeded, NSString *beanGameImageURLString, NSString *categoryImageURLString, NSError *error) {
                                                              NSLog(@"oh got: %@ and %@", beanGameImageURLString, categoryImageURLString);
                                                          }];
}
@end
