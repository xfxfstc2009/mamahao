//
//  MMHMamhaoMamhaoFeaturedProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoMamhaoFeaturedProduct.h"


@implementation MMHMamhaoMamhaoFeaturedProduct


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.imageURLString = dict[@"pic"];
        self.contentURLString = dict[@"link"];
    }
    return self;
}


@end
