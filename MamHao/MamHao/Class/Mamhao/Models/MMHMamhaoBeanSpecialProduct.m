//
//  MMHMamhaoBeanSpecialProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHFilter.h"
#import "MMHAssistant.h"
#import "MMHCurrentLocationModel.h"
#import "MMHLogger.h"


@implementation MMHMamhaoBeanSpecialProductInfo
@end


@implementation MMHMamhaoBeanSpecialProduct


- (MMHProductModule)module
{
    return MMHProductModuleMamhaoBeanSpecial;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return YES;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeMBean;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleMamhaoBeanSpecial) {
        return @{};
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    dictionary[@"templateId"] = self.templateId;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    
    dictionary[@"deviceId"] = [UIDevice deviceID];
    
    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"bean special parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


- (MMHMamhaoBeanSpecialProductInfo *)info
{
    MMHMamhaoBeanSpecialProductInfo *info = [[MMHMamhaoBeanSpecialProductInfo alloc] init];

    NSDate *beginDate = self.beginDate;
    NSDate *endDate = self.endDate;
    NSDate *now = [NSDate date];

    NSTimeInterval timeRemaining = 0.0;
    if ([now earlierThan:beginDate]) {
        timeRemaining = [beginDate timeIntervalSinceDate:now];
        info.status = MMHMamhaoBeanSpecialProductStatusNotStartedYet;
    }
    else if ([now earlierThan:endDate]) {
        timeRemaining = [endDate timeIntervalSinceDate:now];
        info.status = MMHMamhaoBeanSpecialProductStatusOnSale;
    }
    else {
        timeRemaining = [endDate timeIntervalSinceDate:now];
        info.status = MMHMamhaoBeanSpecialProductStatusExpired;
    }

    timeRemaining = MAX(timeRemaining, 0.0);
    long hour = (long)timeRemaining / 3600;
    long minute = ((long)timeRemaining / 60) % 60;
    long second = (long)timeRemaining % 60;
    info.displayTimeRemaining = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", hour, minute, second];

    info.soldOut = [self soldOut];
    info.stock = self.stock;

    return info;
}


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    NSMutableDictionary *realKeyMap = [[NSMutableDictionary alloc] init];
    realKeyMap[@"beanPrice"] = @"beans";
    realKeyMap[@"beanBalance"] = @"beans"; // TODO: - Louis -
    realKeyMap[@"cashPrice"] = @"money";
    realKeyMap[@"imageURLString"] = @"pic";
    realKeyMap[@"displayBeginDate"] = @"activeStartTime";
    self = [super initWithJSONDict:dict keyMap:realKeyMap];
    if (self) {
        NSTimeInterval currentTimestamp = [dict[@"currentTime"] doubleValue] / 1000.0;
        NSTimeInterval beginTimestamp = [dict[@"beginTime"] doubleValue] / 1000.0;
        NSTimeInterval endTimestamp = [dict[@"endTime"] doubleValue] / 1000.0;

        self.beginDate = [NSDate dateWithTimeIntervalSinceNow:(beginTimestamp - currentTimestamp)];
        self.endDate = [NSDate dateWithTimeIntervalSinceNow:(endTimestamp - currentTimestamp)];

//        self.beginDate = [NSDate dateWithTimestamp:beginTimestamp];
//        self.endDate = [NSDate dateWithTimestamp:endTimestamp];
//        self.currentDate = [NSDate dateWithTimestamp:currentTimestamp];
//        self.deviceDate = [NSDate date];

//        if (![dict hasKey:@"templateId"]) {
//            self.templateId = [dict stringForKey:@"goodsId"];
//        }
    }
    return self;
}


- (NSString *)message
{
    long firstStock = self.firstStock;
    NSString *openDate = self.displayBeginDate;
    if (openDate.length == 0) {
        openDate = @"";
    }
    NSString *message = [NSString stringWithFormat:@"限量%ld件\n每天%@开放!", firstStock, openDate];
    return message;
}


- (BOOL)soldOut
{
    return self.stock == 0;
}
@end
