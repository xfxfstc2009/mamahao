//
//  MMHMamhaoShop.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
@import CoreLocation;


@interface MMHMamhaoShopBrand : MMHFetchModel

@property (nonatomic, copy) NSString *brandId;
@property (nonatomic, copy) NSString *brandname;
@property (nonatomic, copy) NSString *imageURLString;

@end


@interface MMHMamhaoShop : MMHFetchModel

@property (nonatomic, copy) NSString *shopId;
@property (nonatomic, copy) NSString *imageURLString;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, strong) NSArray *brands;
@property (nonatomic, copy) NSString *areaNumId;
@property (nonatomic, copy) NSString *cityNumId;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, copy) NSString *shopDetailImageURLString;
@property (nonatomic) long status;
@property (nonatomic) NSInteger shopType; // mothercare, gb, starstation, sports
@property (nonatomic) NSInteger entityType; // 0 for shop, 1 for warehouse
@property (nonatomic, copy) NSString *cityName;

@property (nonatomic) NSInteger tag;

@property (nonatomic) double distance;

@end
