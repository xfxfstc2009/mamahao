//
//  MMHMamhaoBeanSpecialProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHFilter.h"
#import "MMHProduct.h"


typedef NS_ENUM(NSInteger, MMHMamhaoBeanSpecialProductStatus) {
    MMHMamhaoBeanSpecialProductStatusNotStartedYet,
    MMHMamhaoBeanSpecialProductStatusOnSale,
    MMHMamhaoBeanSpecialProductStatusExpired,
};


@interface MMHMamhaoBeanSpecialProductInfo: NSObject

@property (nonatomic) MMHMamhaoBeanSpecialProductStatus status;
@property (nonatomic, getter=isSoldOut) BOOL soldOut;
@property (nonatomic, copy) NSString *displayTimeRemaining;
@property (nonatomic) long stock;
@end


@interface MMHMamhaoBeanSpecialProduct : MMHProduct <MMHProductProtocol>

@property (nonatomic) MMHPrice beanPrice;
@property (nonatomic) MMHPrice cashPrice;
@property (nonatomic) MMHPrice beanBalance;
@property (nonatomic) long firstStock;
@property (nonatomic) long stock;
//@property (nonatomic, copy) NSString *templateId;
@property (nonatomic, copy) NSString *imageURLString;

@property (nonatomic, strong) NSDate *beginDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSDate *currentDate;

@property (nonatomic, strong) NSDate *deviceDate;


@property (nonatomic, copy) NSString *displayBeginDate;
@property (nonatomic, readonly) MMHProductModule module;

@property (nonatomic, readonly) BOOL bindsShop;

- (NSDictionary *)parameters;


//@property (nonatomic, strong) NSDate *expirationDate;
//@property (nonatomic, copy) NSString *message;
//@property (nonatomic) NSInteger beanPrice;
//@property (nonatomic) NSInteger beanBalance;
//@property (nonatomic) BOOL soldOut;

- (NSString *)message;

@property (nonatomic, strong, readonly) MMHMamhaoBeanSpecialProductInfo *info;

@end
