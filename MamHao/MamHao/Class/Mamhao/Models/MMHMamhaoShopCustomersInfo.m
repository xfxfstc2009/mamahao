//
//  MMHMamhaoShopCustomersInfo.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHMamhaoShopCustomersInfo.h"
#import "MMHMamhaoShop.h"
#import "MMHMamhaoShopsCell.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Mamhao.h"
#import "MMHMamhaoCustomer.h"


@interface MMHMamhaoShopCustomersInfo ()

@property (nonatomic, strong) NSMutableDictionary *counts;
@property (nonatomic, strong) NSMutableDictionary *customers;
@end


@implementation MMHMamhaoShopCustomersInfo


- (NSMutableDictionary *)counts
{
    if (_counts == nil) {
        _counts = [[NSMutableDictionary alloc] init];
    }
    return _counts;
}


- (NSMutableDictionary *)customers
{
    if (_customers == nil) {
        _customers = [[NSMutableDictionary alloc] init];
    }
    return _customers;
}


+ (MMHMamhaoShopCustomersInfo *)sharedInfo
{
    static MMHMamhaoShopCustomersInfo *_info = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _info = [[MMHMamhaoShopCustomersInfo alloc] init];
    });
    return _info;
}


- (void)fetchMamhaoCustomersNearShop:(MMHMamhaoShop *)shop completion:(void (^)(BOOL succeeded, long count, NSArray *customers, NSError *error))completion
{
    long count = [self.counts integerForKey:shop.shopId];
    NSArray *customers = self.customers[shop.shopId];
    if (customers) {
        if (count == 0) {
            count = (long)customers.count;
        }
        completion(YES, count, customers, nil);
        return;
    }

    [[MMHNetworkAdapter sharedAdapter] fetchMamhaoCustomersNearShop:shop
                                                               from:self
                                                         completion:^(BOOL succeeded, NSDictionary *dictionary, NSError *error) {
                                                             if (succeeded) {
                                                                 long c = [dictionary integerForKey:@"totalCount"];
                                                                 NSArray *m = [dictionary[@"members"] modelArrayOfClass:[MMHMamhaoCustomer class]];
                                                                 self.counts[shop.shopId] = @(c);
                                                                 self.customers[shop.shopId] = m;
                                                                 completion(YES, c, m, nil);
                                                             }
                                                             else {
                                                                 completion(NO, 0, nil, error);
                                                             }
                                                         }];
}
@end
