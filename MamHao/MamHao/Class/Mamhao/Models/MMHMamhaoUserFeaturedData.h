//
//  MMHMamhaoUserFeaturedData.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"


@interface MMHMamhaoUserFeaturedData : MMHFetchModel

@property (nonatomic, strong) NSArray *products;
@property (nonatomic) long age;

@end
