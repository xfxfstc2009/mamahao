//
//  MMHMamhaoMamhaoFeaturedProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"


@interface MMHMamhaoMamhaoFeaturedProduct : MMHFetchModel

@property (nonatomic, copy) NSString *imageURLString;
@property (nonatomic, copy) NSString *contentURLString;
//@property (nonatomic) NSInteger type;


@end
