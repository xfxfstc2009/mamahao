//
//  MMHLoginViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <TTTAttributedLabel/TTTAttributedLabel.h>
#import "MMHLoginViewController.h"
#import "TTTAttributedLabel.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Login.h"
#import "MMHRegisterWithPhoneNumberViewController.h"
#import "MMHShareSDKMethod.h"
#import "MMHResetPasswordViewController.h"
#import "MMHAccountSession.h"


@interface MMHLoginViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic, copy) void (^succeededHandler)();
@property (nonatomic, copy) void (^failedHandler)();

@property (nonatomic,strong) UITextField *phoneNumberTextField;
@property (nonatomic,strong) UITextField *passwordTextField;
@property (nonatomic,strong) NSMutableArray *shareMutableArray;             // 分享类型
@property (nonatomic,strong) MMHShareSDKMethod *shareSDKMethod;             // 分享参数
@property (nonatomic,strong) UITableView *loginTableView;                   // tableView
@property (nonatomic,strong) NSArray *tableViewDataSourceArray;             // tableViewDataSource
@property (nonatomic,strong) UILabel *errorLabel;
@property (nonatomic,assign)BOOL userNameIsSuccess;                         // 账号是否成功
@property (nonatomic,assign)BOOL passwordIsSuccess;                         // 密码是否成功

@property (nonatomic,strong) NSArray *authorizeImageArray;

@end

@implementation MMHLoginViewController



- (instancetype)initWithSucceededHandler:(void (^)())succeededHandler failedHandler:(void (^)())failedHandler {
    self = [self init];
    if (self) {
        self.succeededHandler = succeededHandler;
        self.failedHandler = failedHandler;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];                               // 数组初始化
    [self pageSetting];                                 // 页面设置
    [self createTableView];                             // 创建tableView
    [self shareSDK];                                    // shareSDK 初始化
    [self createThirdLoginView];                        // 创建三方登录
}



#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"登录";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __weak typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.failedHandler) {
            strongSelf.failedHandler();
        }
        
        [strongSelf.view hideProcessingView];
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:nil];
    }];
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.tableViewDataSourceArray = @[@[@"",@""],@[@""],@[@""]];
    self.authorizeImageArray = @[@"login_thirdLogin_weibo",@"login_thirdLogin_qq",@"login_thirdLogin_wechat"];
}

#pragma mark - UITableView
-(void)createTableView{
    self.loginTableView = [[UITableView alloc] initWithFrame:CGRectMake(mmh_relative_float(kTableView_Margin), 0, self.view.bounds.size.width - (2 * mmh_relative_float(kTableView_Margin)), self.view.bounds.size.height) style:UITableViewStylePlain];
    [self.loginTableView setHeight:self.view.bounds.size.height];
    self.loginTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.loginTableView.delegate = self;
    self.loginTableView.dataSource = self;
    self.loginTableView.scrollEnabled = NO;
    self.loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.loginTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.loginTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.tableViewDataSourceArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArray = [self.tableViewDataSourceArray objectAtIndex:section];
    return sectionOfArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = kStyleBackgroundColor;
            
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            cellWithRowOne.backgroundView = backgroundImageView;
            
            // 创建logo
            UIImageView *logoImageView = [[UIImageView alloc]init];
            logoImageView.backgroundColor = [UIColor clearColor];
            logoImageView.frame = CGRectMake(17,(48 - 14)/2., 14 , 14);
            logoImageView.stringTag = @"logoImageView";
            [cellWithRowOne addSubview:logoImageView];
            
            // 创建textField
            UITextField *textField = [[UITextField alloc]init];
            textField.backgroundColor = [UIColor clearColor];
            textField.tag = 1;
            textField.textAlignment = NSTextAlignmentLeft;
            textField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
            textField.font = kStyleFont;
            textField.frame = CGRectMake(CGRectGetMaxX(logoImageView.frame) + 10, 0, tableView.bounds.size.width - MMHFloat(11), 48);
            textField.delegate = self;
            [cellWithRowOne addSubview:textField];

            [logoImageView autoLayoutWithRect];
            [textField autoLayoutWithRect];
}
    
        // 赋值
        UIImageView *backgroundImageView = (UIImageView *)cellWithRowOne.backgroundView;
        backgroundImageView.image = [MMHTool addBackgroundImageViewWithCellWithDataArray:self.tableViewDataSourceArray indexPath:indexPath];

        UIImageView *logoImageView = (UIImageView *)[cellWithRowOne viewWithStringTag:@"logoImageView"];
        UITextField *textField = (UITextField *)[cellWithRowOne viewWithTag:1];
        if (indexPath.row == 0){
            textField.placeholder = @"请输入您的账号";
            textField.stringTag = @"phoneNumberTextField";
            textField.keyboardType=UIKeyboardTypeNumberPad;
            logoImageView.image = [UIImage imageNamed:@"login_sign_user"];
            
        } else {
            textField.placeholder = @"请输入您的密码";
            textField.stringTag = @"passwordTextField";
            textField.clearButtonMode=UITextFieldViewModeAlways;
            logoImageView.image = [UIImage imageNamed:@"login_sign_password"];
            textField.secureTextEntry=YES;
        }
        
        return cellWithRowOne;
    } else if ((indexPath.section == 1) || (indexPath.section == 2)){
        static NSString *cellIdentifyWithSectionTwo = @"cellIdentifyWithSectionTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = kStyleBackgroundColor;
            
            // 创建button
            UIButton *clickButton = [UIButton buttonWithType:UIButtonTypeCustom];
            clickButton.backgroundColor = [UIColor clearColor];
            clickButton.tag = 2;
            clickButton.frame = CGRectMake(0, 0, tableView.bounds.size.width, mmh_relative_float(44));
            clickButton.layer.cornerRadius = mmh_relative_float(3.0f);
            [clickButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [cellWithRowTwo addSubview:clickButton];
        }
        // 赋值
        UIButton *clickButton = (UIButton *)[cellWithRowTwo viewWithTag:2];
        if (indexPath.section == 1){
            [clickButton setTitle:@"登录" forState:UIControlStateNormal];
            clickButton.backgroundColor = [UIColor whiteColor];
            clickButton.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
            clickButton.layer.borderWidth = .5f;
            clickButton.stringTag = @"login";
            [clickButton setTitleColor:[UIColor hexChangeFloat:@"454545"] forState:UIControlStateNormal];
        } else if (indexPath.section == 2){
            [clickButton setTitle:@"注册" forState:UIControlStateNormal];
            clickButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
            clickButton.stringTag = @"register";
        }
        return cellWithRowTwo;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return mmh_relative_float(35);
    } else if (section == 1){
        return mmh_relative_float(60);
    } else if (section == 2){
        return mmh_relative_float(20);
    } else if (section == 3){
        return 150;
    } else {
        return 20;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return mmh_relative_float(48);
    } else if ((indexPath.section == 1 ) || (indexPath.section == 2)){
        return mmh_relative_float(44);
    } else if (indexPath.section == 3){
        return 120;
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *gestureTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gesttureTap)];
    [headerView addGestureRecognizer:gestureTap];
    
    if (section == 1){
        self.errorLabel = [[UILabel alloc]init];
        self.errorLabel.backgroundColor = [UIColor clearColor];
        self.errorLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15)];
        self.errorLabel.frame = CGRectMake(0, 0, tableView.bounds.size.width - 90, mmh_relative_float(60));
        self.errorLabel.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
        self.errorLabel.text = @"账号或密码错误，请重新输入";
        self.errorLabel.hidden = YES;
        [headerView addSubview:self.errorLabel];
        
        UIButton *resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
        resetButton.backgroundColor = [UIColor clearColor];
        [resetButton addTarget:self action:@selector(resetButtonClick) forControlEvents:UIControlEventTouchUpInside];
        NSString *Titlestring = @"忘记密码";
        CGSize contentOfSize = [Titlestring sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(270, CGFLOAT_MAX)];
        [resetButton setTitle:Titlestring forState:UIControlStateNormal];
        resetButton.frame = CGRectMake(tableView.bounds.size.width - (contentOfSize.width + 20), 0, (contentOfSize.width + 20), mmh_relative_float(60));
        [resetButton setTitleColor:[UIColor hexChangeFloat:@"447ed8"] forState:UIControlStateNormal];
        resetButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
        [headerView addSubview:resetButton];
    }
    return headerView;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.loginTableView) {
        if (indexPath.section == 0 && indexPath.row == 0){
            SeparatorType separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"login"];
        }
    }
}



#pragma mark thirdSourceLogin
-(void)shareSDK{
    _shareSDKMethod = [[MMHShareSDKMethod alloc]init];
    self.shareMutableArray = [NSMutableArray arrayWithArray:[_shareSDKMethod addNotification]];
}

-(void)dealloc{
    [_shareSDKMethod removeWithShareSDK];
}

#pragma mark - customWithView
-(void)customWithAuthorizeButtonWithBgView:(UIScrollView *)scrollView{
    CGFloat marginWidth = (self.view.bounds.size.width - mmh_relative_float(self.authorizeImageArray.count * 60)) / (CGFloat)(self.authorizeImageArray.count + 1);
    for (int i = 0 ; i <self.authorizeImageArray.count ; i++){
        UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
        customButton.frame = CGRectMake(marginWidth + (marginWidth + mmh_relative_float(60)) * i, 0 , mmh_relative_float(60),mmh_relative_float(60));
        [customButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        customButton.backgroundColor = [UIColor clearColor];
        customButton.adjustsImageWhenHighlighted = NO;
        [customButton setImage:[UIImage imageNamed:[self.authorizeImageArray objectAtIndex:i]]forState:UIControlStateNormal];
        customButton.stringTag = [self.authorizeImageArray objectAtIndex:i];
        [scrollView addSubview:customButton];
    }
}

// 创建三方登录的view
-(void)createThirdLoginView{
    UIView *lineBackgroundView = [[UIView alloc]init];
    lineBackgroundView.backgroundColor = kStyleBackgroundColor;
    lineBackgroundView.frame = CGRectMake(0, self.view.bounds.size.height - mmh_relative_float(62 + 140) - 10 , self.view.bounds.size.width, 140);
    [self.view addSubview:lineBackgroundView];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1];
    lineView.frame = CGRectMake(mmh_relative_float(kTableView_Margin), 15, self.view.bounds.size.width - 2 * mmh_relative_float(kTableView_Margin), .5f);
    [lineBackgroundView addSubview:lineView];
    
    // label
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = kStyleBackgroundColor;
    fixedLabel.text = @"使用第三方登录";
    fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
    fixedLabel.frame = CGRectMake((self.view.bounds.size.width - mmh_relative_float(114))/2. ,0,mmh_relative_float(114),30);
    fixedLabel.textColor = [UIColor hexChangeFloat:@"939393"];
    fixedLabel.textAlignment = NSTextAlignmentCenter;
    [lineBackgroundView addSubview:fixedLabel];
    
    // scrollView
    UIScrollView *backgroundScrollView = [[UIScrollView alloc]init];
    backgroundScrollView.backgroundColor = kStyleBackgroundColor;
    backgroundScrollView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame) + mmh_relative_float(35),self.view.bounds.size.width, mmh_relative_float(60));
    backgroundScrollView.stringTag = @"backgroundScrollView";
    [self customWithAuthorizeButtonWithBgView:backgroundScrollView];
    [lineBackgroundView addSubview:backgroundScrollView];
    
}

#pragma mark - actionClick
-(void)buttonClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    if ([button.stringTag isEqualToString:@"login"]){
        [self verificationWithPhoneNumber];
    } else if ([button.stringTag isEqualToString:@"register"]){             // 注册
        MMHRegisterWithPhoneNumberViewController *registerWithPhoneNumberViewController = [[MMHRegisterWithPhoneNumberViewController alloc]init];
        [self.navigationController pushViewController:registerWithPhoneNumberViewController animated:YES];
    }
}

-(void)buttonClicked:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    if ([button.stringTag isEqualToString:[self.authorizeImageArray objectAtIndex:0]]){             // 微博
        __weak typeof(self)weakSelf = self;
        [_shareSDKMethod authorizeWithIndex:MMHAuthorizeTypeQQ andBlock:^(BOOL success) {
            if (!weakSelf){
                return ;
            }
            if (success){
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf->_shareSDKMethod authorizeWithType:ShareTypeSinaWeibo andBlock:^(MMHShareSDKUserDetailModel *userDetailModel) {
                    MMHResetPasswordViewController *authorizeViewController = [[MMHResetPasswordViewController alloc]init];
                    authorizeViewController.accountType = MMHAccountTypeBinding;
                    authorizeViewController.userIdentify = [[strongSelf->_shareSDKMethod showCredentialWithType:ShareTypeSinaWeibo] uid];
                    authorizeViewController.shareSDKUserDetailModel = userDetailModel;
                    [strongSelf.navigationController pushViewController:authorizeViewController animated:YES];
                }];
    
            } else {
                [MMHTool alertWithMessage:@"授权失败"];
            }
        }];
        
        } else if ([button.stringTag isEqualToString:[self.authorizeImageArray objectAtIndex:1]]){               // QQ
            __weak typeof(self)weakSelf = self;
        [_shareSDKMethod authorizeWithIndex:MMHAuthorizeTypeWeChat andBlock:^(BOOL success) {
            if (success){
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf->_shareSDKMethod authorizeWithType:ShareTypeQQSpace andBlock:^(MMHShareSDKUserDetailModel *userDetailModel) {
                    MMHResetPasswordViewController *authorizeViewController = [[MMHResetPasswordViewController alloc]init];
                    authorizeViewController.accountType = MMHAccountTypeBinding;
                    authorizeViewController.userIdentify = [[strongSelf->_shareSDKMethod showCredentialWithType:ShareTypeQQSpace] uid];
                    authorizeViewController.shareSDKUserDetailModel = userDetailModel;
                    [strongSelf.navigationController pushViewController:authorizeViewController animated:YES];
                }];
            } else {
                [MMHTool alertWithMessage:@"授权失败"];
            }
        }];
    } else if ([button.stringTag isEqualToString:[self.authorizeImageArray objectAtIndex:2]]){
        __weak typeof(self)weakSelf =self;
        [_shareSDKMethod authorizeWithIndex:MMHAuthorizeTypeWeibo andBlock:^(BOOL success) {
            if (success){
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf->_shareSDKMethod authorizeWithType:ShareTypeWeixiSession andBlock:^(MMHShareSDKUserDetailModel *userDetailModel) {
                    MMHResetPasswordViewController *authorizeViewController = [[MMHResetPasswordViewController alloc]init];
                    authorizeViewController.accountType = MMHAccountTypeBinding;
                    authorizeViewController.userIdentify = [[strongSelf->_shareSDKMethod showCredentialWithType:ShareTypeWeixiSession] uid];
                    for (int i = 0 ; i < strongSelf.shareMutableArray.count ; i++){
                      NSDictionary *item = [strongSelf.shareMutableArray objectAtIndex:i];
                        if ((long)[[item objectForKey:@"type"] integerValue] == 22){
                            userDetailModel.screen_name = [item objectForKey:@"username"];
                            authorizeViewController.shareSDKUserDetailModel = userDetailModel;
                          [strongSelf.navigationController pushViewController:authorizeViewController animated:YES];
                        }
                    }
                }];
                
            } else {
                [MMHTool alertWithMessage:@"授权失败"];
            }
        }];
    }
}





-(void)resetButtonClick{                    // 忘记密码
    MMHResetPasswordViewController *resetPasswordViewController = [[MMHResetPasswordViewController alloc]init];
    resetPasswordViewController.accountType = MMHAccountTypeReset;
    [self.navigationController pushViewController:resetPasswordViewController animated:YES];
}

-(void)gesttureTap{
    NSIndexPath *indexPathWithOne = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cellWithRowOne = [self.loginTableView cellForRowAtIndexPath:indexPathWithOne];
    UITextField *phoneNumberTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"phoneNumberTextField"];
    NSIndexPath *indexPathWithTwo = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cellWithRowTwo = [self.loginTableView cellForRowAtIndexPath:indexPathWithTwo];
    UITextField *passwordTextField = (UITextField *)[cellWithRowTwo viewWithStringTag:@"passwordTextField"];
    if ([phoneNumberTextField becomeFirstResponder]){
        [phoneNumberTextField resignFirstResponder];
    } else if ([passwordTextField becomeFirstResponder]){
        [passwordTextField resignFirstResponder];
    }
}

#pragma mark 判断

-(void)verificationWithPhoneNumber{
    NSIndexPath *indexPathWithOne = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cellWithRowOne = [self.loginTableView cellForRowAtIndexPath:indexPathWithOne];
    UITextField *phoneNumberTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"phoneNumberTextField"];
    
    NSIndexPath *indexPathWithTwo = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cellWithRowTwo = [self.loginTableView cellForRowAtIndexPath:indexPathWithTwo];
    UITextField *passwordTextField = (UITextField *)[cellWithRowTwo viewWithStringTag:@"passwordTextField"];
    
    if (!phoneNumberTextField.text.length){
        [MMHTool lockAnimationForView:phoneNumberTextField];
    } else {
        if (![MMHTool validateMobile:phoneNumberTextField.text]){
            [MMHTool alertWithMessage:@"输入的手机号码不合法"];
        } else {                        // 跳转页面
            // 判断密码
            if (!passwordTextField.text.length){
                [MMHTool lockAnimationForView:passwordTextField];
            }else if ((passwordTextField.text.length >= 6) && (passwordTextField.text.length <= 16)){
                __weak typeof(self)weakSelf = self;
                [weakSelf sendRequestWithLoginWithPhoneNumber:phoneNumberTextField.text andPassword:passwordTextField.text];
            } else {
                [[UIAlertView alertViewWithTitle:@"密码错误" message:@"亲，密码不对哟" buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [passwordTextField becomeFirstResponder];
                }] show];
            }
        }
    }
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSIndexPath *indexPathWithOne = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cellWithRowOne = [self.loginTableView cellForRowAtIndexPath:indexPathWithOne];
    UITextField *phoneNumberTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"phoneNumberTextField"];
    
    NSIndexPath *indexPathWithTwo = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell *cellWithRowTwo = [self.loginTableView cellForRowAtIndexPath:indexPathWithTwo];
    UITextField *passwordTextField = (UITextField *)[cellWithRowTwo viewWithStringTag:@"passwordTextField"];
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if(phoneNumberTextField == textField) {
        if([toBeString length]>11)  {
            textField.text=[toBeString substringToIndex:11];
            return NO;
        }
    } else if (passwordTextField == textField) {
        if (![MMHTool validatePassword:toBeString]){
            if (toBeString.length){
                textField.text = [toBeString substringToIndex:(toBeString.length - 1)];
            } else {
                textField.text = [textField.text substringToIndex:(textField.text.length - 1)];
            }
            return NO;
        }
        if([toBeString length]>16)  {
            textField.text=[toBeString substringToIndex:16];
            return NO;
        }
    }
    return YES;
}




#pragma mark  接口
- (void)sendRequestWithLoginWithPhoneNumber:(NSString *)phoneNumber andPassword:(NSString *)password{
    [self.view showProcessingViewWithMessage:@"登录中"];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] loginWithPhoneNumber:phoneNumber password:password from:nil succeededHandler:^(MMHAccount *account) {
        [weakSelf.view hideProcessingView];
//        if (self.succeededHandler) {
//            self.succeededHandler();
//        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"登录成功" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        }] show];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
//        if (self.failedHandler) {
//            self.failedHandler();
//        }
        [[UIAlertView alertViewWithTitle:@"登录失败" message:[error localizedDescription] buttonTitles:@[@"确定"] callback:nil] show];
    }];
}



@end
