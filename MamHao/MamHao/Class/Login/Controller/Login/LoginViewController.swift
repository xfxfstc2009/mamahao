//
//  LoginViewController.swift
//  MamHao
//
//  Created by Louis Zhu on 15/5/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class LoginViewController: AbstractViewController, VerificationCodeTimerDelegate, UITextFieldDelegate {
    
    let textFont = MMHFontOfSize(14)

    var phoneNumberBackgroundView: UIView?
    var phoneNumberTipsLabel: UILabel?
    var verificationCodeBackgroundView: UIView?
    var verificationCodeTipsLabel: UILabel?
    
    @IBOutlet var phoneNumberField: UITextField?
    @IBOutlet var verificationCodeField: UITextField?
    @IBOutlet var sendVerificationCodeButton: UIButton?
    @IBOutlet var loginButton: UIButton?
    
    var succeededHandler: ((account: MMHAccount) -> Void)?
    
//    convenience init((succeededHandler: (account: MMHAccount) -> Void) {
//        self.succeededHandler = succeededHandler
//        self.init(nibName: "LoginViewController", bundle: NSBundle.mainBundle())
//    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "登录"
        let dismissItem = UIBarButtonItem(title: "取消", style: .Plain, target: self, action: "dismiss:");
        self.navigationItem.leftBarButtonItem = dismissItem
        
        self.configureViews()
        
        self.phoneNumberField?.becomeFirstResponder()
        
        VerificationCodeTimer.sharedTimer.delegate = self
        self.updateSendVerificationButtonWithTimer(VerificationCodeTimer.sharedTimer)
    }
    
    func configureViews() {
        var phoneNumberBackgroundView = UIView(frame: CGRectMake(0, MMHFloat(10), mmh_screen_width(), MMHFloat(44)))
        phoneNumberBackgroundView.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(phoneNumberBackgroundView)
        self.phoneNumberBackgroundView = phoneNumberBackgroundView
        
        var phoneNumberTipsLabel = UILabel(frame: CGRectMake(MMHFloat(10), 0, 0, self.phoneNumberBackgroundView!.bounds.size.height))
        phoneNumberTipsLabel.textColor = UIColor.blackColor()
        phoneNumberTipsLabel.font = textFont
        phoneNumberTipsLabel.setSingleLineText("手机号", keepingHeight: true)
        self.phoneNumberBackgroundView?.addSubview(phoneNumberTipsLabel)
        self.phoneNumberTipsLabel = phoneNumberTipsLabel
        
        var phoneNumberField = UITextField(frame: CGRectMake(MMHFloat(72), 0, 0, self.phoneNumberBackgroundView!.bounds.size.height))
        phoneNumberField.setMaxX(mmh_screen_width())
        phoneNumberField.font = textFont
        phoneNumberField.textColor = UIColor.blackColor()
        phoneNumberField.borderStyle = .None
        phoneNumberField.placeholder = "请输入手机号"
        phoneNumberField.delegate = self
        self.phoneNumberBackgroundView?.addSubview(phoneNumberField)
        self.phoneNumberField = phoneNumberField
        
        var verificationCodeBackgroundView = UIView(frame: CGRectMake(0, 0, mmh_screen_width(), MMHFloat(44)))
        verificationCodeBackgroundView.attachToBottomSideOfView(self.phoneNumberBackgroundView!, byDistance: MMHFloat(10))
        verificationCodeBackgroundView.backgroundColor = UIColor.whiteColor()
        self.view.addSubview(verificationCodeBackgroundView)
        self.verificationCodeBackgroundView = verificationCodeBackgroundView
        
        var verificationCodeTipsLabel = UILabel(frame: CGRectMake(MMHFloat(10), 0, 0, self.verificationCodeBackgroundView!.bounds.size.height))
        verificationCodeTipsLabel.textColor = UIColor.blackColor()
        verificationCodeTipsLabel.font = textFont
        verificationCodeTipsLabel.setSingleLineText("验证码", keepingHeight: true)
        self.verificationCodeBackgroundView?.addSubview(verificationCodeTipsLabel)
        self.verificationCodeTipsLabel = verificationCodeTipsLabel
        
        var verificationCodeField = UITextField(frame: CGRectMake(MMHFloat(72), 0, 0, self.verificationCodeBackgroundView!.bounds.size.height))
        verificationCodeField.setMaxX(MMHFloat(240))
        verificationCodeField.font = textFont
        verificationCodeField.textColor = UIColor.blackColor()
        verificationCodeField.borderStyle = .None
        verificationCodeField.placeholder = "请输入验证码"
        verificationCodeField.delegate = self
        self.verificationCodeBackgroundView?.addSubview(verificationCodeField)
        self.verificationCodeField = verificationCodeField

        var sendVerificationCodeButton = UIButton(frame: CGRectMake(MMHFloat(240), 0, 0, self.verificationCodeBackgroundView!.bounds.size.height))
        sendVerificationCodeButton.setMaxX(mmh_screen_width())
        sendVerificationCodeButton.titleLabel?.font = textFont
        sendVerificationCodeButton.setTitle("发送验证码", forState: .Normal)
        sendVerificationCodeButton.setTitleColor(MMHAppearance.pinkColor(), forState: .Normal)
        sendVerificationCodeButton.setTitleColor(MMHAppearance.separatorColor(), forState: .Disabled)
        sendVerificationCodeButton.addTarget(self, action: Selector("sendVerificationCode:"), forControlEvents: .TouchUpInside)
        self.verificationCodeBackgroundView?.addSubview(sendVerificationCodeButton)
        self.sendVerificationCodeButton = sendVerificationCodeButton
        
        var separatorLine = UIView(frame: CGRectMake(MMHFloat(240), 0, mmh_pixel(), self.verificationCodeBackgroundView!.bounds.size.height))
        separatorLine.backgroundColor = MMHAppearance.separatorColor()
        self.verificationCodeBackgroundView?.addSubview(separatorLine)
        
        var loginButton = UIButton(frame: CGRectMake(MMHFloat(15), 0, mmh_screen_width() - MMHFloat(30), 50))
        loginButton.attachToBottomSideOfView(self.verificationCodeBackgroundView!, byDistance: MMHFloat(10))
        loginButton.setBackgroundImage(UIImage.patternImageWithColor(MMHAppearance.pinkColor()), forState: .Normal)
        loginButton.setBackgroundImage(UIImage.patternImageWithColor(MMHAppearance.separatorColor()), forState: .Disabled)
        loginButton.titleLabel?.font = MMHFontOfSize(18)
        loginButton.setTitle("登录", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.layer.cornerRadius = 8
        loginButton.clipsToBounds = true
        loginButton.addTarget(self, action: Selector("login:"), forControlEvents: .TouchUpInside)
        loginButton.enabled = false
        self.view.addSubview(loginButton)
        self.loginButton = loginButton
    }
    
    func dismiss(item: UIBarButtonItem) {
        self.navigationController?.dismissViewControllerWithAnimation()
    }

    @IBAction func sendVerificationCode(sender: UIButton) {
        let phoneNumber = self.phoneNumberField!.text
        if !MMHTool.validateMobile(phoneNumber) {
            let alertView = UIAlertView(title: "", message: "错误的手机号码", delegate: nil, cancelButtonTitle: "确定")
            alertView.show()
            return
        }
        
        MMHNetworkAdapter.sharedAdapter().sendSmsForAllWithPhoneNumber(phoneNumber, from: self, succeededHandler: { () -> Void in
            self.view.showTips("验证码已发送")
            }) { (error: NSError!) -> Void in
                self.view.showTips("未能发送验证码，请重试")
        }
        
        VerificationCodeTimer.sharedTimer.start()
    }
    
    @IBAction func login(sender: UIButton) {
        let phoneNumber = self.phoneNumberField!.text
        if !MMHTool.validateMobile(phoneNumber) {
            let alertView = UIAlertView(title: "", message: "错误的手机号码", delegate: nil, cancelButtonTitle: "确定")
            alertView.show()
            return
        }
        
        let verificationCode = self.verificationCodeField!.text
        if count(verificationCode) == 0 {
            self.showAlertViewWithTitle("", message: "请输入验证码", cancelButtonTitle: "确定")
            return
        }
        
        self.view.showProcessingViewWithMessage("验证中")
        MMHNetworkAdapter.sharedAdapter().loginWithPhoneNumber(phoneNumber, verificationCode: verificationCode, from: self, succeededHandler: { (account: MMHAccount!) -> Void in
            self.view.hideProcessingView()
            if self.succeededHandler != nil {
                self.succeededHandler!(account: account)
                self.dismissViewController()
            }
            }) { (error: NSError!) -> Void in
                self.view.hideProcessingView()
            self.showAlertViewWithError(error)
        }
    }
    
    
    func verificationCodeTimerStepped(timer: VerificationCodeTimer!) {
        self.updateSendVerificationButtonWithTimer(timer)
    }
    
    func verificationCodeTimerFinished(timer: VerificationCodeTimer!) {
        self.updateSendVerificationButtonWithTimer(timer)
    }
    
    func updateSendVerificationButtonWithTimer(timer: VerificationCodeTimer) {
        let remainingTime = timer.remainingTime()
        if remainingTime > 0 {
            self.sendVerificationCodeButton!.enabled = false
            let title = NSString(format: "重发(%d)秒", Int(remainingTime))
            self.sendVerificationCodeButton!.setTitle(title as String, forState: .Disabled)
        }
        else {
            self.sendVerificationCodeButton!.enabled = (self.phoneNumberField!.textLengh() == 11)
//            self.sendVerificationCodeButton.setTitle("发送验证码", forState: .Normal)
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var text = textField.text as NSString
        text = text.stringByReplacingCharactersInRange(range, withString: string)
        textField.text = text as String
        if self.phoneNumberField!.textLengh() != 11 {
            self.sendVerificationCodeButton?.enabled = false
            self.loginButton?.enabled = false
            return false
        }
        
        self.updateSendVerificationButtonWithTimer(VerificationCodeTimer.sharedTimer)
        
        if self.verificationCodeField!.textLengh() != 6 {
            self.loginButton?.enabled = false
            return false
        }
        self.loginButton?.enabled = true
        return false
    }
    
}


class LoginButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 2
        self.titleLabel?.font = UIFont.systemFontOfSize(15)
        self.setBackgroundImage(UIImage.patternImageWithColor(MMHAppearance.pinkColor()), forState: .Normal)
        self.setBackgroundImage(UIImage.patternImageWithColor(MMHAppearance.separatorColor()), forState: .Disabled)
    }
}

class LoginRoundCornerView: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 2
        self.layer.borderColor = MMHAppearance.separatorColor().CGColor
        self.layer.borderWidth = 0.5
    }
}
