//
//  MMHShareSDKMethod.h
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AGCommon/UIDevice+Common.h>
#import <AGCommon/UIImage+Common.h>
#import <AGCommon/UINavigationBar+Common.h>
#import <ShareSDK/ShareSDK.h>
#import <AGCommon/NSString+Common.h>
#import "MMHShareSDKUserInfoModel.h"
#import "MMHShareSDKUserDetailModel.h"
#import "MMHHTMLModel.h"

typedef void (^authorizeBlock)(BOOL success);
typedef void (^authorizeWithUserInfoBlock)(MMHShareSDKUserInfoModel *userInfoModel);
typedef void (^authorizeWithUserDetailBlock)(MMHShareSDKUserDetailModel *userDetailModel);

typedef NS_ENUM (NSInteger,MMHAuthorizeType){
    MMHAuthorizeTypeQQ,
    MMHAuthorizeTypeWeChat,
    MMHAuthorizeTypeWeibo,
};


@interface MMHShareSDKMethod : NSObject
@property (nonatomic,copy)authorizeBlock block;

-(NSMutableArray *)addNotification;                     // 添加监听
-(void)authorizeWithIndex:(MMHAuthorizeType)shareType andBlock:(authorizeBlock)block;  // 授权
- (void)removeWithShareSDK;                         //dealloc
-(void)cancelAuthWithType:(MMHAuthorizeType)shareType;  // 取消授权

- (MMHShareSDKUserInfoModel *)showCredentialWithType:(ShareType)type;    // 显示授权信息


#pragma mark 授权信息
-(void)authorizeWithType:(ShareType)type;                               // 获取用户详细信息
-(void)authorizeWithType:(ShareType)type andBlock:(authorizeWithUserDetailBlock)userDetailBlock;



#pragma mark  -------- 分享 -------
// 新浪微博
- (void)shareToSinaWeiboWithModel:(MMHHTMLModel *)shareModel;

// 微信
- (void)shareAppWithWechatWithModel:(MMHHTMLModel *)shareModel;            // 分享app-微信
- (void)shareNewsWithWechatWithModel:(MMHHTMLModel *)shareModel;           // 分享新闻-微信


// 朋友圈

- (void)sharePhotoWithFriendsCircleWithModel:(MMHHTMLModel *)shareModel;     // 分享照片到朋友圈
- (void)shareAppContentWithFriendsCricleWithModel:(MMHHTMLModel *)shareModel;  // 分享App内容到朋友圈

// QQ
-(void)shareNewsMessageWithQQWithModel:(MMHHTMLModel *)shareModel;           // 分享新闻消息到QQ

// QQSpace
-(void)shareToQQSpaceWithModel:(MMHHTMLModel *)shareModel;
// SMS
- (void)shareBySMSWithModel:(MMHHTMLModel *)shareModel;

// 拷贝
- (void)copyShareContentWithModel:(MMHHTMLModel *)shareModel;
@end
