//
//  MMHLoginViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class TTTAttributedLabel;


@interface MMHLoginViewController : AbstractViewController

- (instancetype)initWithSucceededHandler:(void (^)())succeededHandler failedHandler:(void (^)())failedHandler;
@end
