//
//  MMHShareSDKMethod.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShareSDKMethod.h"
#import <ShareSDK/ShareSDK.h>


#define BUFFER_SIZE 1024 * 100

@interface MMHShareSDKMethod(){
    NSMutableArray *shareTypeArray;
    NSMutableDictionary *_infoDict;
}
@property (nonatomic,strong)    MMHShareSDKUserDetailModel *shareSDKUserDetailModel;
@end

@implementation MMHShareSDKMethod

-(NSMutableArray *)addNotification{
    //监听用户信息变更
    [ShareSDK addNotificationWithName:SSN_USER_INFO_UPDATE
                               target:self
                               action:@selector(userInfoUpdateHandler:)];

    shareTypeArray = [[NSMutableArray alloc] init];
    
    NSArray *shareTypes = [ShareSDK connectedPlatformTypes];
    for (int i = 0; i < [shareTypes count]; i++) {
        NSNumber *typeNum = [shareTypes objectAtIndex:i];
        ShareType type = (ShareType)[typeNum integerValue];
        id<ISSPlatformApp> app = [ShareSDK getClientWithType:type];
        
        if ([app isSupportOneKeyShare] || type == ShareTypeInstagram || type == ShareTypeGooglePlus || type == ShareTypeQQSpace || type == ShareTypeWeixiSession) {
            [shareTypeArray addObject:[NSMutableDictionary dictionaryWithObject:[shareTypes objectAtIndex:i] forKey:@"type"]];
        }
    }
    
    
    NSArray *authList = [NSArray arrayWithContentsOfFile:[NSString stringWithFormat:@"%@/authListCache.plist",NSTemporaryDirectory()]];
    if (authList == nil) {
        [shareTypeArray writeToFile:[NSString stringWithFormat:@"%@/authListCache.plist",NSTemporaryDirectory()] atomically:YES];
    } else {
        for (int i = 0; i < [authList count]; i++) {
            NSDictionary *item = [authList objectAtIndex:i];
            for (int j = 0; j < [shareTypeArray count]; j++) {
                if ([[[shareTypeArray objectAtIndex:j] objectForKey:@"type"] integerValue] == [[item objectForKey:@"type"] integerValue])  {
                    [shareTypeArray replaceObjectAtIndex:j withObject:[NSMutableDictionary dictionaryWithDictionary:item]];
                    break;
                    
                    
                }
            }
        }
    }
    return shareTypeArray;
}



- (void)removeWithShareSDK {
    [ShareSDK removeNotificationWithName:SSN_USER_INFO_UPDATE target:self];
}

- (void)userInfoUpdateHandler:(NSNotification *)notif {
    NSInteger plat = [[[notif userInfo] objectForKey:SSK_PLAT] integerValue];
    id<ISSPlatformUser> userInfo = [[notif userInfo] objectForKey:SSK_USER_INFO];
    
    for (int i = 0; i < [shareTypeArray count]; i++) {
        NSMutableDictionary *item = [shareTypeArray objectAtIndex:i];
        ShareType type = (ShareType)[[item objectForKey:@"type"] integerValue];
        if (type == plat) {
            [item setObject:[userInfo nickname] forKey:@"username"];
        }
    }
}

-(void)authorizeWithIndex:(MMHAuthorizeType)shareType andBlock:(authorizeBlock)authorizeBlock{
    if (shareType < shareTypeArray.count){
        NSMutableDictionary *item = [shareTypeArray objectAtIndex:shareType];
        //用户用户信息
        ShareType type = (ShareType)[[item objectForKey:@"type"] integerValue];
        
        id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                             allowCallback:YES
                                                             authViewStyle:SSAuthViewStyleFullScreenPopup
                                                              viewDelegate:nil
                                                   authManagerViewDelegate:nil];
        
        //在授权页面中添加关注官方微博
        [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                        SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                        [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                        SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                        nil]];
        
        [ShareSDK getUserInfoWithType:type authOptions:authOptions
                               result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
                                   if (result) {
                                       // 写入plist
                                       [item setObject:[userInfo nickname] forKey:@"username"];
                                       [shareTypeArray writeToFile:[NSString stringWithFormat:@"%@/authListCache.plist",NSTemporaryDirectory()] atomically:YES];
                                       authorizeBlock(YES);
                                       
                                   }  else {
                                       authorizeBlock(NO);
                                   }
                               }];
    }
}

-(void)cancelAuthWithType:(MMHAuthorizeType)shareType{
    NSMutableDictionary *item = [shareTypeArray objectAtIndex:shareType];
    [ShareSDK cancelAuthWithType:(ShareType)[[item objectForKey:@"type"] integerValue]];
}


#pragma mark 获取授权信息方法
- (MMHShareSDKUserInfoModel *)showCredentialWithType:(ShareType)type {
    id<ISSPlatformCredential> credential = [ShareSDK getCredentialWithType:type];
    
    MMHShareSDKUserInfoModel *shareSDKUserInfoModel = [[MMHShareSDKUserInfoModel alloc]init];
    shareSDKUserInfoModel.uid = credential.uid;
    shareSDKUserInfoModel.token = credential.token;
    shareSDKUserInfoModel.secret = credential.secret;
    shareSDKUserInfoModel.expired = credential.expired;
    shareSDKUserInfoModel.extInfo = credential.extInfo;
    
    return shareSDKUserInfoModel;
}


-(void)authorizeWithType:(ShareType)type {
    
    _infoDict = [[NSMutableDictionary alloc]init];
    self.shareSDKUserDetailModel = [[MMHShareSDKUserDetailModel alloc]init];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:Nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];

        [ShareSDK getUserInfoWithType:type authOptions:authOptions result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
            if (result){
                switch (type) {
                    case ShareTypeSinaWeibo:
                        //新浪微博
                        [self fillSinaWeiboUser:userInfo];
                        break;
                    case ShareTypeQQSpace:
                        //QQ空间
//                            [self fillQQSpaceUser:userInfo];
                        break;
                    default:
                        break;
                }
            } else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"提示"
                                                                    message:error.errorDescription
                                                                   delegate:nil
                                                          cancelButtonTitle:@"知道了"
                                                          otherButtonTitles: nil];
                [alertView show];
            }
        }];
}





- (MMHShareSDKUserDetailModel *)fillSinaWeiboUser:(id<ISSPlatformUser>)userInfo{

    NSArray *keys = [[userInfo sourceData] allKeys];
    for (int i = 0; i < [keys count]; i++) {
        NSString *keyName = [keys objectAtIndex:i];
        id value = [[userInfo sourceData] objectForKey:keyName];
        if (![value isKindOfClass:[NSString class]]) {
            if ([value respondsToSelector:@selector(stringValue)]) {
                value = [value stringValue];
            } else {
                value = @"";
            }
        }
        
        // 记录头像
        self.shareSDKUserDetailModel.profileImage = [userInfo profileImage];
        
        if ([keyName isEqualToString:@"id"]) {
            [_infoDict setObject:value forKey:@"UID"];
        }
        else if([keyName isEqualToString:@"idstr"]) {
            [_infoDict setObject:value forKey:@"字符串型UID"];
        }
        else if([keyName isEqualToString:@"screen_name"]) {
            self.shareSDKUserDetailModel.screen_name = value;
        } else if([keyName isEqualToString:@"name"]) {
            [_infoDict setObject:value forKey:@"显示名称"];
        } else if([keyName isEqualToString:@"province"]){
            [_infoDict setObject:value forKey: @"省级ID"];
        } else if([keyName isEqualToString:@"city"]) {
            [_infoDict setObject:value forKey:@"城市ID"];
        } else if([keyName isEqualToString:@"location"]){
            [_infoDict setObject:value forKey:@"所在地"];
        } else if([keyName isEqualToString:@"description"]) {
            [_infoDict setObject:value forKey: @"个人描述"];
        } else if([keyName isEqualToString:@"url"]) {
            [_infoDict setObject:value forKey:@"博客地址"];
        } else if([keyName isEqualToString:@"profile_image_url"]) {
            [_infoDict setObject:value forKey: @"头像地址"];
        } else if([keyName isEqualToString:@"profile_url"]) {
            [_infoDict setObject:value forKey:@"微博统一URL地址"];
        } else if([keyName isEqualToString:@"domain"]) {
            [_infoDict setObject:value forKey:@"个性化域名"];
        } else if([keyName isEqualToString:@"weihao"]) {
            [_infoDict setObject:value forKey: @"微号"];
        } else if([keyName isEqualToString:@"gender"]) {
            [_infoDict setObject:value forKey:@"性别"];
        } else if([keyName isEqualToString:@"followers_count"]) {
            [_infoDict setObject:value forKey:@"粉丝数"];
        } else if([keyName isEqualToString:@"friends_count"]) {
            [_infoDict setObject:value forKey:@"关注数"];
        } else if([keyName isEqualToString:@"statuses_count"]) {
            [_infoDict setObject:value forKey: @"微博数"];
        } else if([keyName isEqualToString:@"favourites_count"])
        {
            [_infoDict setObject:value forKey: @"收藏数"];
        }
        else if([keyName isEqualToString:@"created_at"])
        {
            [_infoDict setObject:value forKey: @"注册时间"];
        }
        else if([keyName isEqualToString:@"following"])
        {
            [_infoDict setObject:value forKey:@"following"];
        }
        else if([keyName isEqualToString:@"allow_all_act_msg"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_ALLOW_SEND_LETTER", @"是否允许给我发私信")];
        }
        else if([keyName isEqualToString:@"geo_enabled"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_ALLOW_GEO", @"是否允许标识地理位置")];
        }
        else if([keyName isEqualToString:@"verified"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_IS_WEIBO_VERIFIED_USER", @"是否是微博认证用户")];
        }
        else if([keyName isEqualToString:@"verified_type"])
        {
            [_infoDict setObject:value forKey:@"verified_type"];
        }
        else if([keyName isEqualToString:@"remark"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_REMARK", @"备注信息")];
        }
        else if([keyName isEqualToString:@"allow_all_comment"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_ALLOW_COMMENT_STATUS", @"是否允许对我的微博进行评论")];
        }
        else if([keyName isEqualToString:@"avatar_large"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_LARGE_AVATAR_URL", @"大头像地址")];
        }
        else if([keyName isEqualToString:@"verified_reason"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_VERIFIED_REASON", @"认证原因")];
        }
        else if([keyName isEqualToString:@"follow_me"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_FOLLOW_ME", @"是否关注我")];
        }
        else if([keyName isEqualToString:@"online_status"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_ONLINE_STATUS", @"在线状态")];
        }
        else if([keyName isEqualToString:@"bi_followers_count"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_BI_FOLLOWER_COUNT", @"互粉数")];
        }
        else if([keyName isEqualToString:@"lang"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_LANG", @"语言版本")];
        }
    }
    return self.shareSDKUserDetailModel;
}

- ( MMHShareSDKUserDetailModel *)fillQQSpaceUser:(id<ISSPlatformUser>)userInfo {
    NSArray *keys = [[userInfo sourceData] allKeys];
    for (int i = 0; i < [keys count]; i++) {
        NSString *keyName = [keys objectAtIndex:i];
        id value = [[userInfo sourceData] objectForKey:keyName];
        if (![value isKindOfClass:[NSString class]]) {
            if ([value respondsToSelector:@selector(stringValue)]) {
                value = [value stringValue];
            } else {
                value = @"";
            }
        }
        
        // 记录头像
        self.shareSDKUserDetailModel.profileImage = [userInfo profileImage];

        if ([keyName isEqualToString:@"figureurl"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_FIGURE_URL", @"30×30头像URL")];
        }
        else if ([keyName isEqualToString:@"figureurl_1"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_FIGURE_URL_1", @"50×50头像URL")];
        }
        else if ([keyName isEqualToString:@"figureurl_2"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_FIGURE_URL_2", @"100×100头像URL")];
        }
        else if ([keyName isEqualToString:@"gender"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_GENDER", @"性别")];
        }
        else if ([keyName isEqualToString:@"is_yellow_year_vip"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_IS_YELLOW_YEAR_VIP", @"是否为年费黄钻用户")];
        }
        else if ([keyName isEqualToString:@"level"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_YELLOW_LEVEL", @"黄钻等级")];
        }
        else if ([keyName isEqualToString:@"nickname"])
        {   //  昵称
            self.shareSDKUserDetailModel.screen_name = value;
        }
        else if ([keyName isEqualToString:@"vip"])
        {
            [_infoDict setObject:value forKey:NSLocalizedString(@"TEXT_YELLOW_VIP", @"是否为黄钻用户")];
        }
    }
    return self.shareSDKUserDetailModel;
}


#pragma mark -custom
-(void)authorizeWithType:(ShareType)type andBlock:(authorizeWithUserDetailBlock)userDetailBlock{
    _infoDict = [[NSMutableDictionary alloc]init];
    self.shareSDKUserDetailModel = [[MMHShareSDKUserDetailModel alloc]init];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:Nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    [ShareSDK getUserInfoWithType:type authOptions:authOptions result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
        if (result){
            switch (type) {
                case ShareTypeSinaWeibo:
                    //新浪微博
                    userDetailBlock([self fillSinaWeiboUser:userInfo]);
                    break;
                case ShareTypeQQSpace:
                    //QQ空间
                    userDetailBlock ([self fillQQSpaceUser:userInfo]);
                    break;
                case ShareTypeWeixiSession:
                    self.shareSDKUserDetailModel.profileImage = [userInfo profileImage];
                    userDetailBlock(self.shareSDKUserDetailModel);
                default:
                    break;
            }
        } else {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"提示"
                                                                message:error.errorDescription
                                                               delegate:nil
                                                      cancelButtonTitle:@"知道了"
                                                      otherButtonTitles: nil];
            [alertView show];
        }
    }];
}


#pragma mark - **********************分享**********************

#pragma mark 分享到新浪微博
- (void)shareToSinaWeiboWithModel:(MMHHTMLModel *)shareModel{
    //创建分享内容
    id<ISSCAttachment> image = [ShareSDK imageWithUrl:shareModel.url];
    id<ISSContent> publishContent = [ShareSDK content:shareModel.content
                                       defaultContent:nil
                                                image:image
                                                title:shareModel.title
                                                  url:shareModel.url
                                          description:shareModel.desc
                                            mediaType:SSPublishContentMediaTypeText];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
//    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    //显示分享菜单
    [ShareSDK showShareViewWithType:ShareTypeSinaWeibo
                          container:container
                            content:publishContent
                      statusBarTips:YES
                        authOptions:authOptions
                       shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
                                                           oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                            qqButtonHidden:NO
                                                     wxSessionButtonHidden:NO
                                                    wxTimelineButtonHidden:NO
                                                      showKeyboardOnAppear:NO
                                                         shareViewDelegate:nil
                                                       friendsViewDelegate:nil
                                                     picViewerViewDelegate:nil]
                             result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                 
                                 if (state == SSPublishContentStateSuccess)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
                                 }
                                 else if (state == SSPublishContentStateFail)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                                 }
                             }];
}


#pragma mark 发送App内容给微信
- (void)shareAppWithWechatWithModel:(MMHHTMLModel *)shareModel{
    Byte* pBuffer = (Byte *)malloc(BUFFER_SIZE);
    memset(pBuffer, 0, BUFFER_SIZE);
    NSData* data = [NSData dataWithBytes:pBuffer length:BUFFER_SIZE];
    free(pBuffer);
    
    id<ISSContent> content = [ShareSDK content:shareModel.content
                                defaultContent:@"【妈妈好】"
                                         image:[ShareSDK jpegImageWithImage:[UIImage imageNamed:@"res2.jpg"] quality:1]
                                         title:shareModel.title
                                           url:shareModel.url
                                   description:@"人人"
                                     mediaType:SSPublishContentMediaTypeApp];
    [content addWeixinSessionUnitWithType:INHERIT_VALUE
                                  content:@"我是测试内容"
                                    title:@"我时测试标题"
                                      url:INHERIT_VALUE
                                    image:INHERIT_VALUE
                             musicFileUrl:nil
                                  extInfo:@"<xml>test</xml>"
                                 fileData:data
                             emoticonData:nil];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    [ShareSDK shareContent:content
                      type:ShareTypeWeixiSession
               authOptions:authOptions
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        
                        if (state == SSPublishContentStateSuccess) {
                            NSLog(@"success");
                        } else if (state == SSResponseStateCancel){
                            NSLog(@"取消");
                        }
                        else if (state == SSPublishContentStateFail) {
                            if ([error errorCode] == -22003) {
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"提示"
                                                                                    message:[error errorDescription]
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"知道了"
                                                                          otherButtonTitles:nil];
                                [alertView show];
                            }
                        }
                    }];
}

#pragma mark 发送新闻信息
- (void)shareNewsWithWechatWithModel:(MMHHTMLModel *)shareModel{
    id<ISSCAttachment> image = [ShareSDK imageWithUrl:shareModel.image];
    
    id<ISSContent> content = [ShareSDK content:shareModel.content
                                defaultContent:nil
                                         image:image
                                         title: shareModel.title
                                           url: shareModel.url
                                   description:shareModel.desc
                                     mediaType:SSPublishContentMediaTypeNews];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    [ShareSDK shareContent:content
                      type:ShareTypeWeixiSession
               authOptions:authOptions
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        
                        if (state == SSPublishContentStateSuccess)  {
                            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:@"wechat" forKey:@"shareStatus"];
                            [[NSNotificationCenter defaultCenter]postNotificationName:MMHConfirmationNotificationWithShare object:nil userInfo:dic];
                            
                        } else if (state == SSPublishContentStateFail) {
                            if ([error errorCode] == -22003) {
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                                    message:[error errorDescription]
                                                                                   delegate:nil
                                                                          cancelButtonTitle:@"知道了"
                                                                          otherButtonTitles:nil];
                                [alertView show];
                            }
                        }
                    }];
}

#pragma mark - 朋友圈
#pragma mark 分享照片给微信朋友圈 - (1 张照片)
- (void)sharePhotoWithFriendsCircleWithModel:(MMHHTMLModel *)shareModel{
    id<ISSCAttachment> image =[ShareSDK imageWithUrl:shareModel.image];
    
    //发送内容给微信
    id<ISSContent> content = [ShareSDK content:shareModel.content
                                defaultContent:nil
                                         image:image
                                         title:shareModel.title
                                           url:shareModel.url
                                   description:nil
                                     mediaType:SSPublishContentMediaTypeImage];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    [ShareSDK shareContent:content
                      type:ShareTypeWeixiTimeline
               authOptions:authOptions
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        
                        if (state == SSPublishContentStateSuccess) {
                            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:@"pengyouquan" forKey:@"shareStatus"];
                            [[NSNotificationCenter defaultCenter]postNotificationName:MMHConfirmationNotificationWithShare object:nil userInfo:dic];
                        }
                        else if (state == SSPublishContentStateFail)
                        {
//                            if ([error errorCode] == -22003)
//                            {
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"提示"
                                                                                    message:[error errorDescription]
                                                                                   delegate:nil
                                                                          cancelButtonTitle: @"知道了"
                                                                          otherButtonTitles:nil];
                                [alertView show];
//                            }
                        }
                    }];
}




#pragma mark 分享app消息
- (void)shareAppContentWithFriendsCricleWithModel:(MMHHTMLModel *)shareModel{
    id<ISSCAttachment> image = [ShareSDK imageWithUrl:shareModel.image];
    
    // 发送内容给微信
    Byte* pBuffer = (Byte *)malloc(BUFFER_SIZE);
    memset(pBuffer, 0, BUFFER_SIZE);
    NSData* data = [NSData dataWithBytes:pBuffer length:BUFFER_SIZE];
    free(pBuffer);
    
    id<ISSContent> content = [ShareSDK content: shareModel.content
                                defaultContent:shareModel.desc
                                         image:image
                                         title:shareModel.title
                                           url:shareModel.url
                                   description:shareModel.desc
                                     mediaType:SSPublishContentMediaTypeApp];
    [content addWeixinTimelineUnitWithType:INHERIT_VALUE
                                   content:INHERIT_VALUE
                                     title:INHERIT_VALUE
                                       url:INHERIT_VALUE
                                     image:INHERIT_VALUE
                              musicFileUrl:nil
                                   extInfo:@"<xml>test</xml>"
                                  fileData:data
                              emoticonData:nil];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    [ShareSDK shareContent:content
                      type:ShareTypeWeixiTimeline
               authOptions:authOptions
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        
                        if (state == SSPublishContentStateSuccess) {
                            NSLog(@"success");
                        } else if (state == SSResponseStateCancel){
                            NSLog(@"分享成功");
                        }
                        else if (state == SSPublishContentStateFail)
                        {
                            if ([error errorCode] == -22003)
                            {
                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"TEXT_TIPS", @"提示")
                                                                                    message:[error errorDescription]
                                                                                   delegate:nil
                                                                          cancelButtonTitle:NSLocalizedString(@"TEXT_KNOW", @"知道了")
                                                                          otherButtonTitles:nil];
                                [alertView show];
                            }
                        }
                    }];
}


#pragma mark - 分享到QQ
- (void)shareNewsMessageWithQQWithModel:(MMHHTMLModel *)shareModel {
//    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"demo.jpg"];
//    NSData* data = [NSData dataWithContentsOfFile:path];
    //    UIImage *image = [[UIImage alloc] initWithData:data];
    
    id<ISSCAttachment> image =  [ShareSDK imageWithUrl:shareModel.image];
    id<ISSContent> content = [ShareSDK content:shareModel.content
                                defaultContent:nil
                                         image:image
                                         title:shareModel.title
                                           url:shareModel.url
                                   description:nil
                                     mediaType:SSPublishContentMediaTypeNews];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    [ShareSDK shareContent:content
                      type:ShareTypeQQ
               authOptions:authOptions
             statusBarTips:YES
                    result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                        
                        if (state == SSPublishContentStateSuccess)
                        {
                            NSLog(@"success");
                        }
                        else if (state == SSPublishContentStateFail)
                        {
                            NSLog(@"fail");
                        }
                    }];
}


#pragma mark 分享到QQ空间
- (void)shareToQQSpaceWithModel:(MMHHTMLModel *)shareModel{
    //创建分享内容
//    NSString *imagePath = [[NSBundle mainBundle] pathForResource:IMAGE_NAME ofType:IMAGE_EXT];
        id<ISSCAttachment> image = [ShareSDK imageWithUrl:shareModel.image];
    id<ISSContent> publishContent = [ShareSDK content:shareModel.content
                                       defaultContent:nil
                                                image:image
                                                title:shareModel.title
                                                  url:shareModel.url
                                          description:nil
                                            mediaType:SSPublishContentMediaTypeText];
    
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
//    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    //显示分享菜单
    [ShareSDK showShareViewWithType:ShareTypeQQSpace
                          container:container
                            content:publishContent
                      statusBarTips:YES
                        authOptions:authOptions
                       shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
                                                           oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                            qqButtonHidden:NO
                                                     wxSessionButtonHidden:NO
                                                    wxTimelineButtonHidden:NO
                                                      showKeyboardOnAppear:NO
                                                         shareViewDelegate:nil
                                                       friendsViewDelegate:nil
                                                     picViewerViewDelegate:nil]
                             result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                 
                                 if (state == SSPublishContentStateSuccess)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_SUC", @"发表成功"));
                                 }
                                 else if (state == SSPublishContentStateFail)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                                 }
                             }];
}

#pragma mark 分享短信
- (void)shareBySMSWithModel:(MMHHTMLModel *)shareModel {
    
    NSString *shareContent = [NSString stringWithFormat:@"%@%@",shareModel.content,shareModel.url];
    //创建分享内容
    id<ISSContent> publishContent = [ShareSDK content:shareContent
                                       defaultContent:nil
                                                image:nil
                                                title:shareModel.title
                                                  url:shareModel.url
                                          description:shareModel.desc
                                            mediaType:SSPublishContentMediaTypeText];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
//    [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    //显示分享菜单
    [ShareSDK showShareViewWithType:ShareTypeSMS
                          container:container
                            content:publishContent
                      statusBarTips:YES
                        authOptions:authOptions
                       shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
                                                           oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                            qqButtonHidden:NO
                                                     wxSessionButtonHidden:NO
                                                    wxTimelineButtonHidden:NO
                                                      showKeyboardOnAppear:NO
                                                         shareViewDelegate:nil
                                                       friendsViewDelegate:nil
                                                     picViewerViewDelegate:nil]
                             result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                 
                                 if (state == SSPublishContentStateSuccess){
                                     NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:@"message" forKey:@"shareStatus"];
                                [[NSNotificationCenter defaultCenter]postNotificationName:MMHConfirmationNotificationWithShare object:nil userInfo:dic];
                                 }
                                 else if (state == SSPublishContentStateFail)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                                 }
                             }];
}


#pragma mark 拷贝
- (void)copyShareContentWithModel:(MMHHTMLModel *)shareModel {
    //创建分享内容
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:shareModel.url ofType:@"123"];
    id<ISSContent> publishContent = [ShareSDK content:[NSString stringWithFormat:@"%@%@",shareModel.content,shareModel.url]
                                       defaultContent:nil
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:shareModel.title
                                                  url:shareModel.url
                                          description:shareModel.desc
                                            mediaType:SSPublishContentMediaTypeText];
    
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
//    [container setIPadContainerWithView:[self.view viewWithTag:10001] arrowDirect:UIPopoverArrowDirectionUp];
    
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                         allowCallback:YES
                                                         authViewStyle:SSAuthViewStyleFullScreenPopup
                                                          viewDelegate:nil
                                               authManagerViewDelegate:nil];
    
    //在授权页面中添加关注官方微博
    [authOptions setFollowAccounts:[NSDictionary dictionaryWithObjectsAndKeys:
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeSinaWeibo),
                                    [ShareSDK userFieldWithType:SSUserFieldTypeName value:@"ShareSDK"],
                                    SHARE_TYPE_NUMBER(ShareTypeTencentWeibo),
                                    nil]];
    
    //显示分享菜单
    [ShareSDK showShareViewWithType:ShareTypeCopy
                          container:container
                            content:publishContent
                      statusBarTips:YES
                        authOptions:authOptions
                       shareOptions:[ShareSDK defaultShareOptionsWithTitle:nil
                                                           oneKeyShareList:[NSArray defaultOneKeyShareList]
                                                            qqButtonHidden:NO
                                                     wxSessionButtonHidden:NO
                                                    wxTimelineButtonHidden:NO
                                                      showKeyboardOnAppear:NO
                                                         shareViewDelegate:nil
                                                       friendsViewDelegate:nil
                                                     picViewerViewDelegate:nil]
                             result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                 
                                 if (state == SSPublishContentStateSuccess) {
                                     NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObject:@"copy" forKey:@"shareStatus"];
                                     [[NSNotificationCenter defaultCenter]postNotificationName:MMHConfirmationNotificationWithShare object:nil userInfo:dic];
                                 }
                                 else if (state == SSPublishContentStateFail)
                                 {
                                     NSLog(NSLocalizedString(@"TEXT_SHARE_FAI", @"发布失败!error code == %d, error code == %@"), [error errorCode], [error errorDescription]);
                                 }
                             }];
}

@end

