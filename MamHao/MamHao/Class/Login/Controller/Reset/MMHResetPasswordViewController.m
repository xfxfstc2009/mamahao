//
//  MMHResetPasswordViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHResetPasswordViewController.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Login.h"
#import "MMHShareSDKMethod.h"

@interface MMHResetPasswordViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *resetPasswordTableView;
@property (nonatomic,strong)NSArray *dataSourceArray;
@property (nonatomic,strong)UITextField *phoneNumberTextField;
@property (nonatomic,strong)UITextField *verificationTextField;
@property (nonatomic,strong)UITextField *passwordTextField;
@property (nonatomic,strong)UIButton *timingButton;
@property (nonatomic,assign)NSInteger timingWithNumber;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *actionNextButton;
@property (nonatomic,assign)BOOL isFirstIn;
@end

@implementation MMHResetPasswordViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.phoneNumberTextField becomeFirstResponder];
    [self timingWithInit];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)pageSetting{
    if (self.accountType == MMHAccountTypeBinding){
        self.barMainTitle = @"绑定账号";
    } else if (self.accountType == MMHAccountTypeReset){
        self.barMainTitle = @"找回密码";
    }
}

#pragma mark -arrayWithInit
-(void)arrayWithInit{
    self.dataSourceArray = @[@[@""],@[@""],@[@""],@[@""]];
    self.isFirstIn = YES;
}

#pragma mark - UITableView
-(void)createTableView{
    _resetPasswordTableView = [[UITableView alloc] initWithFrame:CGRectMake(mmh_relative_float(kTableView_Margin), 0, self.view.bounds.size.width - (2 * mmh_relative_float(kTableView_Margin)), self.view.bounds.size.height) style:UITableViewStylePlain];
    _resetPasswordTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    _resetPasswordTableView.delegate = self;
    _resetPasswordTableView.dataSource = self;
    _resetPasswordTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _resetPasswordTableView.backgroundColor = [UIColor clearColor];
    [_resetPasswordTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.view addSubview:_resetPasswordTableView];
    if (self.accountType != MMHAccountTypeBinding){
        _resetPasswordTableView.scrollEnabled = NO;
    }
}

#pragma mark  UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArray = [self.dataSourceArray objectAtIndex:section];
    return sectionOfArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = kStyleBackgroundColor;
            
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            cellWithRowOne.backgroundView = backgroundImageView;
            
            // 创建logo
            UIImageView *logoImageView = [[UIImageView alloc]init];
            logoImageView.backgroundColor = [UIColor clearColor];
            logoImageView.frame = CGRectMake(17,(55 - 14)/2., 14 , 14);
            logoImageView.stringTag = @"logoImageView";
            logoImageView.image = [UIImage imageNamed:@"login_sign_phone"];
            [cellWithRowOne addSubview:logoImageView];

            self.phoneNumberTextField = [[UITextField alloc]init];
            [self customWithCreateTextField:self.phoneNumberTextField placeHolder:@"请输入您的手机号" rectMake:CGRectMake(CGRectGetMaxX(logoImageView.frame) + 10, 0, 250, 55)];
            self.phoneNumberTextField.keyboardType = UIKeyboardTypeNumberPad;
            [cellWithRowOne addSubview:self.phoneNumberTextField];
            
            [logoImageView autoLayoutWithRect];
            [self.phoneNumberTextField autoLayoutWithRect];
        }
        UIImageView *backgroundImageView = (UIImageView *)cellWithRowOne.backgroundView;
        // 计算文件名
        backgroundImageView.image = [MMHTool addBackgroundImageViewWithCellWithDataArray:self.dataSourceArray indexPath:indexPath];
    
        
        return cellWithRowOne;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = kStyleBackgroundColor;
            
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            backgroundImageView.image = [MMHTool stretchImageWithName:@"login_frame_single"];
            backgroundImageView.frame =  CGRectMake(0, 0, tableView.bounds.size.width - mmh_relative_float(140), mmh_relative_float(55));
            [cellWithRowTwo addSubview:backgroundImageView];

            self.verificationTextField = [[UITextField alloc]init];
            [self customWithCreateTextField:self.verificationTextField placeHolder:@"请输入验证码" rectMake:CGRectMake(mmh_relative_float(14), 0, 150, mmh_relative_float(55))];
            self.verificationTextField.keyboardType = UIKeyboardTypeNumberPad;
            [cellWithRowTwo addSubview:self.verificationTextField];
            
            // button
            self.timingButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.timingButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
            self.timingButton.frame = CGRectMake(CGRectGetMaxX(backgroundImageView.frame) + mmh_relative_float(10), 0, mmh_relative_float(130), mmh_relative_float(55));
            NSString *timingButtonTitle = @"";
            if (_timingWithNumber == 0){
                timingButtonTitle = self.isFirstIn?@"获取验证码":@"重新获取";
            } else {
                timingButtonTitle = [NSString stringWithFormat:@"%li 秒后重新获取",(long)_timingWithNumber];
            }
            self.timingButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
            [self.timingButton setTitle:timingButtonTitle forState:UIControlStateNormal];
            [self.timingButton setTitleColor:[UIColor hexChangeFloat:@"FFFFFF"] forState:UIControlStateNormal];
            [self.timingButton addTarget:self action:@selector(timingButtonClick) forControlEvents:UIControlEventTouchUpInside];
            self.timingButton.layer.cornerRadius = mmh_relative_float(5.f);
            [cellWithRowTwo addSubview:self.timingButton];

        }
        return cellWithRowTwo;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleGray;
            cellWithRowThr.backgroundColor = kStyleBackgroundColor;
            
            UIImageView *backgroundImageView = [[UIImageView alloc]init];
            cellWithRowThr.backgroundView = backgroundImageView;
            
            self.passwordTextField = [[UITextField alloc]init];
            [self customWithCreateTextField:self.passwordTextField placeHolder:@"请输入您的密码" rectMake:CGRectMake(mmh_relative_float(14), 0, tableView.bounds.size.width - 20, mmh_relative_float(55))];
            self.passwordTextField.secureTextEntry=YES;
            self.passwordTextField.clearButtonMode=UITextFieldViewModeAlways;
            self.passwordTextField.delegate = self;
            self.passwordTextField.returnKeyType = UIReturnKeyDone;
            [cellWithRowThr addSubview:self.passwordTextField];
            
            self.fixedLabel = [[UILabel alloc]init];
            self.fixedLabel.text = @"6-16位数字或者密码";
            self.fixedLabel.backgroundColor = [UIColor clearColor];
            self.fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
            self.fixedLabel.textAlignment = NSTextAlignmentRight;
            self.fixedLabel.textColor = [UIColor lightGrayColor];
            CGSize fixedContentSize = [self.fixedLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(12.)] constrainedToSize:CGSizeMake(270, CGFLOAT_MAX)];
            self.fixedLabel.frame = CGRectMake(CGRectGetWidth(self.passwordTextField.frame) - fixedContentSize.width, 0, fixedContentSize.width, self.passwordTextField.height);
            [self.passwordTextField addSubview:self.fixedLabel];
   
        }
        UIImageView *backgroundImageView = (UIImageView *)cellWithRowThr.backgroundView;
        // 计算文件名
        backgroundImageView.image = [MMHTool addBackgroundImageViewWithCellWithDataArray:self.dataSourceArray indexPath:indexPath];
        
        return cellWithRowThr;
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleGray;
            cellWithRowFour.backgroundColor = kStyleBackgroundColor;
            
            // 创建button
            self.actionNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
            self.actionNextButton.frame = CGRectMake(0, 0, tableView.bounds.size.width, mmh_relative_float(44));
            [self.actionNextButton setTitle:@"完成" forState:UIControlStateNormal];
            [self.actionNextButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
            [self.actionNextButton addTarget:self action:@selector(verification) forControlEvents:UIControlEventTouchUpInside];
            self.actionNextButton.layer.cornerRadius = mmh_relative_float(5.0f);
            
            [cellWithRowFour addSubview:self.actionNextButton];
        }
        return cellWithRowFour;
    }
    return nil;
}

#pragma mark  UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (self.accountType == MMHAccountTypeReset){
        if (section == 0){
            return mmh_relative_float(35);
        } else if ((section == 1) || (section == 2)){
            return mmh_relative_float(10);
        }  else if (section == 3){
            return mmh_relative_float(21);
        } else {
            return 10;
        }
    } else if (self.accountType == MMHAccountTypeBinding){
        if (section == 0){
            return mmh_relative_float(110);
        } else if ((section == 1) || (section == 2)){
            return mmh_relative_float(10);
        } else if (section == 3){
            return mmh_relative_float(21);
        } else{
            return 10;
        }
    } else{
        return 10;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((indexPath.section == 0) || (indexPath.section == 1) || (indexPath.section == 2)){
        return mmh_relative_float(55);
    } else {
        return mmh_relative_float(44);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnceHideKeyBoard)];
    [tapForHideKeyBoard setNumberOfTouchesRequired:1];
    [headerView addGestureRecognizer:tapForHideKeyBoard];
    [self.view addGestureRecognizer:tapForHideKeyBoard];

    
    if (self.accountType == MMHAccountTypeBinding){         // 已注册
        if (section == 0){
           
            UILabel *nicknameLabel = [[UILabel alloc]init];
            NSString *string = [NSString stringWithFormat:@"亲爱的 %@ ,",self.shareSDKUserDetailModel.screen_name];
            NSRange range1 = [string rangeOfString:@"亲爱的 "];
            NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"%@ ",self.shareSDKUserDetailModel.screen_name]];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"aaaaaa"] range:range1];
            [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"FB6A7C"] range:range2];
            CGSize contentOfSize = [string sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)];
            nicknameLabel.attributedText = str;
            nicknameLabel.font=[UIFont systemFontOfSize:mmh_relative_float(15.)];
            nicknameLabel.frame=CGRectMake(0, mmh_relative_float(35), tableView.bounds.size.width, contentOfSize.height);
            nicknameLabel.textAlignment = NSTextAlignmentLeft;
            nicknameLabel.numberOfLines=1;
            nicknameLabel.backgroundColor=[UIColor clearColor];
            [headerView addSubview:nicknameLabel];
            
            
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.text = @"为了您的账号安全，请绑定手机喔!";
            fixedLabel.frame = CGRectMake(nicknameLabel.frame.origin.x, CGRectGetMaxY(nicknameLabel.frame) + 10, 300, 20);
            fixedLabel.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
            fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
            [headerView addSubview:fixedLabel];
        }
    }
    return headerView;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - actionClick
-(void)timingWithInit{
    _timingWithNumber = 0;
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(changeTimingNumber) userInfo:nil repeats:YES];
    
}

-(void)changeTimingNumber{
    NSString *timingButtonTitle = @"";
    if (_timingWithNumber >0){
        _timingWithNumber --;
        self.timingButton.enabled = NO;
        timingButtonTitle = [NSString stringWithFormat:@"%li 秒后重新获取",(long)_timingWithNumber];
        self.timingButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
    } else {
        _timingWithNumber = 0;
        self.timingButton.enabled = YES;
        timingButtonTitle = self.isFirstIn?@"获取验证码":@"重新获取";
        self.timingButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
    }
    [self.timingButton setTitle:timingButtonTitle forState:UIControlStateNormal];
}


-(void)timingButtonClick{                   // 重新获取按钮
    if (!self.phoneNumberTextField.text.length){
        [MMHTool lockAnimationForView:self.phoneNumberTextField];
    } else if (![MMHTool validateMobile:self.phoneNumberTextField.text]){
        [MMHTool alertWithMessage:@"输入的手机号码不合法"];
    } else {                        // 跳转页面
        __weak typeof(self)weakSelf = self;
        [weakSelf verificationWithTextField:self.phoneNumberTextField];
    }
}

-(void)tapOnceHideKeyBoard{
    if ([self.phoneNumberTextField becomeFirstResponder]){
        [self.phoneNumberTextField resignFirstResponder];
    } else if ([self.verificationTextField becomeFirstResponder]){
        [self.verificationTextField resignFirstResponder];
    } else if ([self.passwordTextField becomeFirstResponder]){
        [self.passwordTextField resignFirstResponder];
    }
}


#pragma mark - Verification
-(void)verificationWithTextField:(UITextField *)textField{
    if (!textField.text.length){
        [MMHTool lockAnimationForView:textField];
    } else {
        if (![MMHTool validateMobile:textField.text]){
            [MMHTool alertWithMessage:@"输入的手机号码不合法"];
        } else {
            __weak typeof(self) weakSelf = self;
            [weakSelf sendRequestWithGetSms:textField.text];
        }
    }
}

-(void)verification{
    if (!self.phoneNumberTextField.text.length){
        [MMHTool lockAnimationForView:self.phoneNumberTextField];
    } else if (![MMHTool validateMobile:self.phoneNumberTextField.text]){
        [MMHTool alertWithMessage:@"输入的手机号码不合法"];
    } else {                    // 手机号码合法
        if(!self.verificationTextField.text.length){
            [MMHTool lockAnimationForView:self.verificationTextField];
        } else if (![MMHTool isPureNumandCharacters:self.verificationTextField.text]){
            [MMHTool alertWithMessage:@"输入的验证码不合法"];
        } else{                 // 验证码合法
            if ((self.passwordTextField.text.length >= 6) && (self.passwordTextField.text.length <= 16)){
                __weak typeof(self)weakSelf = self;
                if (self.accountType == MMHAccountTypeBinding){                 // 绑定
                    [weakSelf sendRequestWithBindingThirdLogin];
                } else if (self.accountType == MMHAccountTypeReset){            // 重置
                    [weakSelf sendRequestWithResetPassword];
                }
            } else {
                [MMHTool lockAnimationForView:self.passwordTextField];
            }
        }
    }
}

#pragma mark - customView
-(void)customWithCreateTextField:(UITextField *)textField placeHolder:(NSString *)placeHolder rectMake:(CGRect)rect{
    textField.backgroundColor = [UIColor clearColor];
    textField.textAlignment=NSTextAlignmentLeft;
    textField.placeholder = placeHolder;
    textField.frame = rect;
    textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    textField.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    textField.font=[UIFont systemFontOfSize:mmh_relative_float(18)];
    textField.returnKeyType=UIReturnKeyDone;
    textField.keyboardType=UIKeyboardTypeASCIICapable;
    textField.delegate=self;
    textField.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
}



#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (self.phoneNumberTextField == textField) {
        if ([toBeString length] > 11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    } else if (self.verificationTextField == textField){
        if (toBeString.length > 6){
            textField.text = [toBeString substringToIndex:6];
            return NO;
        }
    } else if (self.passwordTextField == textField){
        if([toBeString length]>16) {
            textField.text=[toBeString substringToIndex:16];
            return NO;
        } else if([toBeString length] == 0) {
            self.fixedLabel.text = @"6-16位数字或者密码";
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
        } else if (toBeString.length >= 6){
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
        } else  {
            self.fixedLabel.text = @"";
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.passwordTextField){
        [self verification];
    }
    return YES;
}


#pragma mark - UIScrollViewDelegate
//去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.accountType == MMHAccountTypeBinding){
        CGFloat sectionHeaderHeight = mmh_relative_float(110);
        if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

#pragma mark - 接口
#pragma mark 绑定
-(void)sendRequestWithBindingThirdLogin{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] bindingWithPhoneNumber:weakSelf.phoneNumberTextField.text password:weakSelf.passwordTextField.text extId:weakSelf.userIdentify extType:0 vcode:weakSelf.verificationTextField.text from:nil succeededHandler:^(MMHAccount *account) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"授权成功" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        }] show];
    } failedHandler:^(NSError *error) {
        [MMHTool alertWithMessage:[NSString stringWithFormat:@"%@",error]];
    }];
}

#pragma mark 忘记密码
-(void)sendRequestWithResetPassword{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] resetPasswordWithPhoneNumber:weakSelf.phoneNumberTextField.text newPassword:weakSelf.passwordTextField.text vcode:weakSelf.verificationTextField.text from:nil succeededHandler:^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"重置密码成功" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        }] show];
    } failedHandler:^(NSError *error) {
        [[UIAlertView alertViewWithTitle:@"找回密码失败" message:[error localizedDescription] buttonTitles:@[@"确定"] callback:nil] show];
    }];
}

#pragma mark 发送短信
-(void)sendRequestWithGetSms:(NSString *)phoneNumber{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] sendSmsForExistsWithPhoneNumber:phoneNumber from:nil succeededHandler:^{
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.isFirstIn = NO;
        strongSelf.timingWithNumber = 60;
        if (![strongSelf.verificationTextField becomeFirstResponder]){
            [strongSelf.verificationTextField becomeFirstResponder];
        }
    } failedHandler:^(NSError *error) {
        [[UIAlertView alertViewWithTitle:[error localizedDescription]  message:nil buttonTitles:@[@"确定"] callback:nil] show];
    }];
}
@end
