//
//  MMHResetPasswordViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHShareSDKUserDetailModel.h"
typedef NS_ENUM(NSInteger, MMHAccountType){
    MMHAccountTypeReset,                       // 已注册 - 忘记密码
    MMHAccountTypeBinding,                        // 未注册 - 绑定
    MMHAccountTypeNone
};


@interface MMHResetPasswordViewController : AbstractViewController

@property (nonatomic,copy)NSString *userIdentify;
@property (nonatomic,assign)MMHAccountType accountType;
@property (nonatomic,strong)MMHShareSDKUserDetailModel *shareSDKUserDetailModel;
@end
