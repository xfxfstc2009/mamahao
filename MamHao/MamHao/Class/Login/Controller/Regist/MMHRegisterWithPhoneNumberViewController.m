//
//  MMHRegisterWithPhoneNumberViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRegisterWithPhoneNumberViewController.h"
#import "MMHRegisterWithVerificationCodeViewController.h"               // 输入验证码
#import "MMHNetworkAdapter+Login.h"

@interface MMHRegisterWithPhoneNumberViewController()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate>{
    NSArray *tableViewDataSourceArray;
}
@property (nonatomic,strong)UITextField *userNameTextFiled;
@property (nonatomic,strong)UITableView *registerWithPhoneNumberTableView;
@property (nonatomic,strong)UIButton *actionNextButton;
@end

@implementation MMHRegisterWithPhoneNumberViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.userNameTextFiled becomeFirstResponder];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                                 // 页面设置
    [self arrayWithInit];                                               // 数组初始化
    [self createTableView];                                             // 创建tableView
    
}
#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"快速注册";
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    tableViewDataSourceArray = @[@[@""],@[@""],@[@""]];
}

#pragma mark - UITableView
-(void)createTableView{
    _registerWithPhoneNumberTableView = [[UITableView alloc] initWithFrame:CGRectMake(mmh_relative_float(kTableView_Margin), 0, self.view.bounds.size.width - (2 * mmh_relative_float(kTableView_Margin)), self.view.bounds.size.height) style:UITableViewStylePlain];
    _registerWithPhoneNumberTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    [_registerWithPhoneNumberTableView setDelegate:self];
    [_registerWithPhoneNumberTableView setDataSource:self];
    [_registerWithPhoneNumberTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [_registerWithPhoneNumberTableView setBackgroundColor:[UIColor clearColor]];
    [_registerWithPhoneNumberTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    [self.view addSubview:_registerWithPhoneNumberTableView];
}

#pragma mark -UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return tableViewDataSourceArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arrayOfSection = [tableViewDataSourceArray objectAtIndex:section];
    return arrayOfSection.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = kStyleBackgroundColor;
            
            // 背景
            UIImageView *cellBackgroundImageView = [[UIImageView alloc] init];
            cell.backgroundView = cellBackgroundImageView;
            
            // 创建logo
            UIImageView *logoImageView = [[UIImageView alloc]init];
            logoImageView.backgroundColor = [UIColor clearColor];
            logoImageView.frame = CGRectMake(17,(48 - 14)/2., 14 , 14);
            logoImageView.stringTag = @"logoImageView";
            logoImageView.image = [UIImage imageNamed:@"login_sign_phone"];
            [cell addSubview:logoImageView];

            // userNameTextField
            self.userNameTextFiled = [[UITextField alloc] init];
            self.userNameTextFiled.frame = CGRectMake(CGRectGetMaxX(logoImageView.frame) + 10, 0, 250, 48);            [self.userNameTextFiled setBackgroundColor:[UIColor clearColor]];
            self.userNameTextFiled.textAlignment=NSTextAlignmentLeft;
            self.userNameTextFiled.placeholder = @"请输入您的手机号";
            self.userNameTextFiled.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            self.userNameTextFiled.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
            self.userNameTextFiled.font = kStyleFont;
            self.userNameTextFiled.textColor = [UIColor hexChangeFloat:@"aaaaaa"];
            self.userNameTextFiled.returnKeyType=UIReturnKeyDone;
            self.userNameTextFiled.keyboardType=UIKeyboardTypeNumberPad;
            self.userNameTextFiled.delegate=self;
            [cell addSubview:self.userNameTextFiled];
            
            [logoImageView autoLayoutWithRect];
            [self.userNameTextFiled autoLayoutWithRect];
        }
        
        UIImageView *backgroundImageView = (UIImageView *)cell.backgroundView;
        // 计算文件名
        backgroundImageView.image = [MMHTool addBackgroundImageViewWithCellWithDataArray:tableViewDataSourceArray indexPath:indexPath];
        
        return cell;
    } else if (indexPath.section == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowTwo.backgroundColor = kStyleBackgroundColor;
                
                // 创建button
                self.actionNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
                self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
                self.actionNextButton.frame = CGRectMake(0, 0, tableView.bounds.size.width, mmh_relative_float(44));
                [self.actionNextButton setTitle:@"获取手机验证码" forState:UIControlStateNormal];
                [self.actionNextButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
                [self.actionNextButton addTarget:self action:@selector(verificationWithPhoneNumber) forControlEvents:UIControlEventTouchUpInside];
                self.actionNextButton.layer.cornerRadius = mmh_relative_float(5.0f);
                [cellWithRowTwo addSubview:self.actionNextButton];
            }
            return cellWithRowTwo;
        } else if (indexPath.section == 2){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowThr.backgroundColor = kStyleBackgroundColor;
                
                UILabel *headerLabel = [[UILabel alloc]init];
                NSString *string = [NSString stringWithFormat:@"如遇到问题你可以联系客服%@",CustomerServicePhoneNumber];
                NSRange range1 = [string rangeOfString:@"如遇到问题你可以"];
                NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"联系客服%@",CustomerServicePhoneNumber]];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"aaaaaa"] range:range1];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"447ed8"] range:range2];
                CGSize contentOfSize = [string sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)];
                headerLabel.attributedText = str;
                headerLabel.font=[UIFont systemFontOfSize:mmh_relative_float(15.)];
                headerLabel.frame=CGRectMake(0, 0, tableView.bounds.size.width, contentOfSize.height);
                headerLabel.textAlignment = NSTextAlignmentLeft;
                headerLabel.numberOfLines=1;
                headerLabel.backgroundColor=[UIColor clearColor];
                [cellWithRowThr addSubview:headerLabel];
            }
            return cellWithRowThr;
        }
    return nil;
}

#pragma mark -UITableViewDelegate
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
    [headerView addGestureRecognizer:tapGesture];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return  mmh_relative_float(35);
    } else if ((section == 1) || (section == 2)){
        return  mmh_relative_float(21);
    } else {
        return 20;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 ){
        return  mmh_relative_float(48);
    } else if (indexPath.section == 1){
        return  mmh_relative_float(44);
    } else {
        return 44;
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){
        [self.userNameTextFiled becomeFirstResponder];
    } else if (indexPath.section == 2){
        [self clauseClick];                                                             // 拨打客服电话
    }
}

#pragma mark - actionClick
-(void)clauseClick{                  // 拨打号码
    if ([self.userNameTextFiled becomeFirstResponder]){
        [self.userNameTextFiled resignFirstResponder];
    }
    
    __weak MMHRegisterWithPhoneNumberViewController *weakViewController = self;
    [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
}

-(void)tapGesture{
    if ([self.userNameTextFiled becomeFirstResponder]){
        [self.userNameTextFiled resignFirstResponder];
    }
}


#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string; {
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if (self.userNameTextFiled == textField) {
        if ([toBeString length] > 11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
        if (toBeString.length == 11){
            self.actionNextButton.enabled = YES;
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
        } else {
            self.actionNextButton.enabled = NO;
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
        }
    }
    return YES;
}

#pragma mark - 接口
-(void)obtainVerificationCode{
    // 1. 锁定按钮
    self.actionNextButton.enabled = NO;
    [self.view showProcessingView];
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] sendSmsForNoExistsWithPhoneNumber:weakSelf.userNameTextFiled.text from:nil succeededHandler:^{
        [self.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.actionNextButton.enabled = YES;
        MMHRegisterWithVerificationCodeViewController *verificationCodeVC = [[MMHRegisterWithVerificationCodeViewController alloc]init];
        verificationCodeVC.transferPhoneNumber = self.userNameTextFiled.text;
        [strongSelf.navigationController pushViewController:verificationCodeVC animated:YES];

    } failedHandler:^(NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.actionNextButton.enabled = YES;
        [self.view hideProcessingView];
        [[UIAlertView alertViewWithTitle:@"手机号码已经被注册" message:nil buttonTitles:@[@"确定"] callback:nil]show];
    }];
}

#pragma mark - Verification
-(void)verificationWithPhoneNumber{
    if (!self.userNameTextFiled.text.length){
        [MMHTool lockAnimationForView:self.userNameTextFiled];
    } else {
        if (![MMHTool validateMobile:self.userNameTextFiled.text]){
            [MMHTool alertWithMessage:@"输入的手机号码不合法"];
        } else {                        // 跳转页面
            __weak typeof(self)weakSelf = self;
            [weakSelf obtainVerificationCode];
        }
    }
}


@end
