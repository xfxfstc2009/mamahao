//
//  MMHRegisterViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHRegisterViewController.h"
#import "MMHNetworkAdapter+Login.h"

@interface MMHRegisterViewController()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>{

}
@property (nonatomic,strong)UITextField *userPassWordTextField;
@property (nonatomic,strong)UITableView *successTableView;
@property (nonatomic,strong)NSArray *dataSourceArray;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *actionNextButton;
@end

@implementation MMHRegisterViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.userPassWordTextField becomeFirstResponder];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.dataSourceArray = @[@[@""],@[@""],@[@""]];
}

#pragma mark pageSetting
-(void)pageSetting{
    self.barMainTitle = @"快速注册";
}

#pragma mark - UITableView
-(void)createTableView{
    self.successTableView = [[UITableView alloc] initWithFrame:CGRectMake(mmh_relative_float(kTableView_Margin), 0, self.view.bounds.size.width - (2 * mmh_relative_float(kTableView_Margin)), self.view.bounds.size.height) style:UITableViewStylePlain];
    self.successTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.successTableView.delegate = self;
    self.successTableView.dataSource = self;
    self.successTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.successTableView.backgroundColor = [UIColor clearColor];
    [self.successTableView setTableFooterView:[[UIView alloc]initWithFrame:CGRectZero]];
    self.successTableView.scrollEnabled = NO;
    [self.view addSubview:self.successTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArray = [self.dataSourceArray objectAtIndex:section];
    return sectionOfArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        static NSString *cellIdentify = @"cellIdentify";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = kStyleBackgroundColor;

            // 背景
            UIImageView *cellBackgroundImageView = [[UIImageView alloc] init];
            cell.backgroundView = cellBackgroundImageView;
            
            // 添加密码输入框
            self.userPassWordTextField = [[UITextField alloc] init];
            [self.userPassWordTextField setFrame:CGRectMake(mmh_relative_float(14), 0, tableView.bounds.size.width - 20, mmh_relative_float(55))];
            [self.userPassWordTextField setBackgroundColor:[UIColor clearColor]];
            self.userPassWordTextField.secureTextEntry=YES;
            self.userPassWordTextField.textAlignment=NSTextAlignmentLeft;
            self.userPassWordTextField.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
            self.userPassWordTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
            self.userPassWordTextField.placeholder = @"请输入您的密码";
            self.userPassWordTextField.clearButtonMode=UITextFieldViewModeAlways;
            self.userPassWordTextField.font=[UIFont systemFontOfSize:mmh_relative_float(18.)];
            self.userPassWordTextField.returnKeyType=UIReturnKeyDone;
            self.userPassWordTextField.keyboardType=UIKeyboardTypeASCIICapable;
            self.userPassWordTextField.delegate=self;
            [cell addSubview:self.userPassWordTextField];
            
            self.fixedLabel = [[UILabel alloc]init];
            self.fixedLabel.text = @"6-16位数字或者密码";
            self.fixedLabel.backgroundColor = [UIColor clearColor];
            self.fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
            self.fixedLabel.textAlignment = NSTextAlignmentRight;
            self.fixedLabel.textColor = [UIColor lightGrayColor];
            CGSize fixedContentSize = [self.fixedLabel.text sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(12.)] constrainedToSize:CGSizeMake(270, CGFLOAT_MAX)];
            self.fixedLabel.frame = CGRectMake(CGRectGetWidth(self.userPassWordTextField.frame) - fixedContentSize.width, 0, fixedContentSize.width, self.userPassWordTextField.height);
            [self.userPassWordTextField addSubview:self.fixedLabel];
        }
        UIImageView *backgroundImageView = (UIImageView *)cell.backgroundView;
        // 计算文件名
        backgroundImageView.image = [MMHTool addBackgroundImageViewWithCellWithDataArray:self.dataSourceArray indexPath:indexPath];
        
        return cell;
    } else if (indexPath.section == 1){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowTwo.backgroundColor = kStyleBackgroundColor;
                
                // 创建button
                self.actionNextButton = [UIButton buttonWithType:UIButtonTypeCustom];
                self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
                self.actionNextButton.frame = CGRectMake(0, 0, tableView.bounds.size.width, mmh_relative_float(44));
                [self.actionNextButton setTitle:@"完成" forState:UIControlStateNormal];
                [self.actionNextButton setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
                [self.actionNextButton addTarget:self action:@selector(successClick) forControlEvents:UIControlEventTouchUpInside];
                self.actionNextButton.layer.cornerRadius = mmh_relative_float(5.0f);
                [cellWithRowTwo addSubview:self.actionNextButton];
            }
            return cellWithRowTwo;
        } else if (indexPath.section == 2){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowThr.backgroundColor = kStyleBackgroundColor;
                
                UILabel *headerLabel = [[UILabel alloc]init];
                NSString *string = [NSString stringWithFormat:@"如遇到问题你可以联系客服%@",CustomerServicePhoneNumber];
                NSRange range1 = [string rangeOfString:@"如遇到问题你可以"];
                NSRange range2 = [string rangeOfString:[NSString stringWithFormat:@"联系客服%@",CustomerServicePhoneNumber]];
                NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:string];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"aaaaaa"] range:range1];
                [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"447ed8"] range:range2];
                CGSize contentOfSize = [string sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(tableView.bounds.size.width, CGFLOAT_MAX)];
                headerLabel.attributedText = str;
                headerLabel.font=[UIFont systemFontOfSize:mmh_relative_float(15.)];
                headerLabel.frame=CGRectMake(0, 0, tableView.bounds.size.width, contentOfSize.height);
                headerLabel.textAlignment = NSTextAlignmentLeft;
                headerLabel.numberOfLines=1;
                headerLabel.backgroundColor=[UIColor clearColor];
                [cellWithRowThr addSubview:headerLabel];
            }
            return cellWithRowThr;
        }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return  mmh_relative_float(35);
    } else if ((section == 1)|| (section == 2)){
        return  mmh_relative_float(21);
    } else {
        return 30;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture)];
    [headerView addGestureRecognizer:tapGesture];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return  mmh_relative_float(55);
    } else if (indexPath.section == 1){
        return  mmh_relative_float(44);
    } else {
        return 44;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 2){
        [self clauseClick];
    }
}

#pragma mark - actionClick
-(void)successClick{
    [self verificationWithPassWord];
}

-(void)clauseClick{
    if ([self.userPassWordTextField becomeFirstResponder]){
        [self.userPassWordTextField resignFirstResponder];
    }
    
    __weak MMHRegisterViewController *weakViewController = self;
    [MMHTool callCustomerServer:weakViewController.view phoneNumber:nil];
}

-(void)tapGesture{
    if ([self.userPassWordTextField becomeFirstResponder]){
        [self.userPassWordTextField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString *toBeString=[textField.text stringByReplacingCharactersInRange:range withString:string];
    if(self.userPassWordTextField == textField) {
        if (![MMHTool validatePassword:toBeString]){
            if (toBeString.length){
                textField.text = [toBeString substringToIndex:(toBeString.length - 1)];
            } else {
                textField.text = [textField.text substringToIndex:(textField.text.length - 1)];
            }
            return NO;
        }

        if([toBeString length]>16) {
            textField.text=[toBeString substringToIndex:16];
            return NO;
        } else if([toBeString length] == 0) {
            self.fixedLabel.text = @"6-16位数字或者密码";
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
        } else if (toBeString.length >= 6){
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"FC687C"];
            self.actionNextButton.enabled = YES;
        } else  {
            self.fixedLabel.text = @"";
            self.actionNextButton.backgroundColor = [UIColor hexChangeFloat:@"BFBEBE"];
            self.actionNextButton.enabled = NO;
        }
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.returnKeyType == UIReturnKeyDone){ // 显示下一个
        [self successClick];
    }
    return YES;
}


#pragma mark - Verification
-(void)verificationWithPassWord {
    if (!_userPassWordTextField.text.length) {
        [MMHTool lockAnimationForView:_userPassWordTextField];
    } else if (self.userPassWordTextField.text.length >= 6){
        __weak typeof(self)weakSelf = self;
        [weakSelf userRegister];
    }
}

#pragma mark - 接口
#pragma mark 注册
-(void)userRegister{
    [self.view showProcessingView];
    self.actionNextButton.enabled = NO;
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] registerWithPhoneNumber:weakSelf.transferPhoneNumber password:weakSelf.userPassWordTextField.text from:nil succeededHandler:^(MMHAccount *account) {
        [self.view hideProcessingView];
        self.actionNextButton.enabled = YES;
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (account){
            [[UIAlertView alertViewWithTitle:@"注册成功" message:nil buttonTitles:@[@"确定"] callback:^(UIAlertView *alertView, NSInteger buttonIndex) {
                [strongSelf.navigationController popToRootViewControllerAnimated:YES];
            }]show];
        }
    } failedHandler:^(NSError *error) {
        [self.view hideProcessingView];
        self.actionNextButton.enabled = YES;
        [MMHTool alertWithMessage:[NSString stringWithFormat:@"失败%@",error]];
    }];
}

@end
