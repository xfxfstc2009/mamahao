//
//  MMHRegisterWithVerificationCodeViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/7.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"

@interface MMHRegisterWithVerificationCodeViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferPhoneNumber;
@end
