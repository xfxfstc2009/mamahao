//
//  MMHShareSDKUserDetailModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@interface MMHShareSDKUserDetailModel : MMHFetchModel

@property (nonatomic,copy)NSString *profileImage;           // 用户头像图片
@property (nonatomic,copy)NSString *screen_name;            // 用户昵称

@end
