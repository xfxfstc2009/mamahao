//
//  MMHShareSDKUserInfoModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// shareSDK 用户信息
#import "MMHFetchModel.h"

@interface MMHShareSDKUserInfoModel : MMHFetchModel

@property (nonatomic,copy)NSString *uid;
@property (nonatomic,copy)NSString *token;
@property (nonatomic,copy)NSString *secret;
@property (nonatomic,strong)NSDate *expired;
@property (nonatomic,strong)NSDictionary *extInfo;
@end
