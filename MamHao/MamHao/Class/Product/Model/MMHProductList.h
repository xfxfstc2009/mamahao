//
//  MMHProductList.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHTimeline.h"


@class MMHFilter;


@interface MMHProductList : MMHTimeline

- (instancetype)initWithFilter:(MMHFilter *)filter;

@end
