//
//  MMHShoppingTipsModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHShoppingTipsSingleModel.h"

@interface MMHShoppingTipsModel : MMHFetchModel
@property (nonatomic,copy)NSString *desc;               // 购物描述
@property (nonatomic,strong)NSArray<MMHShoppingTipsSingleModel> *banner;            // banner
@end
