//
//  MMHSingleProductModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHFilter.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@class MMHCategory;
@class MonthAge;

@protocol  MMHSingleProductModel<NSObject>

@end

@interface MMHSingleProductModel : MMHProduct <MMHProductProtocol>

// final properties
@property (nonatomic, copy) NSString *shopId;
@property (nonatomic, copy) NSString *brandId;
@property (nonatomic, copy) NSString *imageURLString;
@property (nonatomic, copy) NSString *itemName;
@property (nonatomic, copy) NSString *itemNumId;
@property (nonatomic) MMHPrice minPrice; // for category products
@property (nonatomic) MMHPrice proPrice; // for shop products
@property (nonatomic) MMHPrice customPrice; // for shop products
@property (nonatomic) MMHPrice retailPrice; // for shop products
@property (nonatomic, strong) MonthAge *styleApplyAge;
@property (nonatomic, copy) NSString *thirdType;
@property (nonatomic) long totalSale;
@property (nonatomic, copy) NSString *areaId;
@property (nonatomic, readonly) MMHProductModule module;
@property (nonatomic, strong) MMHCategory *category;
@property (nonatomic,copy)NSString *companyId;

@property (nonatomic,copy)NSString *barCode;
- (void)configureWithFilter:(MMHFilter *)filter;


- (NSString *)displayPrice;
@end
