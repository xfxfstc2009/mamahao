//
//  MMHShoppingTipsSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol  MMHShoppingTipsSingleModel<NSObject>


@end


@interface MMHShoppingTipsSingleModel : MMHFetchModel

@property (nonatomic,copy)NSString *pic;
@property (nonatomic,copy)NSString *url;
@end
