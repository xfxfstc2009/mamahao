//
//  MMHProductsListModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHSingleProductModel.h"
@interface MMHProductsListModel : MMHFetchModel

@property (nonatomic,assign)NSInteger page;
@property (nonatomic,strong)NSArray *rows;
@property (nonatomic,assign)NSInteger total;
@end
