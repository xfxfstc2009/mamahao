//
//  MMHSingleProductModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHSingleProductModel.h"
#import "MMHCategory.h"
#import "MMHMonthAgeSelectionView.h"
#import "MamHao-Swift.h"
#import "MMHFilter.h"
#import "MMHCurrentLocationModel.h"


@interface MMHSingleProductModel ()

@property (nonatomic) MMHProductModule module;
@property (nonatomic) BOOL bindsShop;

@end


@implementation MMHSingleProductModel


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (BOOL)bindsShop
{
    if (self.module == MMHProductModuleShop) {
        return YES;
    }
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (instancetype)initWithJSONDict:(NSDictionary *)dict {
    self = [super initWithJSONDict:dict];
    if (self) {
        self.imageURLString = [dict stringForKey:@"goodsPic"];
        self.minPrice = [[dict stringForKey:@"minPrice"] priceValue];
        self.proPrice = [[dict stringForKey:@"proPrice"] priceValue];
        self.customPrice = [[dict stringForKey:@"customPrice"] priceValue];
        self.retailPrice = [[dict stringForKey:@"retailPrice"] priceValue];
        
//        self.styleApplyAge = [[MonthAge alloc] initWithString:dict[@"styleApplyAge"]];
        [self tryToFetchActualPrice];
    }
    return self;
}


- (NSArray *)propertiesForCoding {
    return @[@"goodsNo", @"goodsPic", @"goodsTitle", @"goodsSmallTitle", @"price", @"goodsChnnel", @"goodsDistance", @"shopId", @"shopName"];
}


- (void)configureWithFilter:(MMHFilter *)filter {
    switch (filter.module) {
        case MMHFilterModuleCategory:
            self.module = MMHProductModuleCategory;
            self.category = filter.category;
            break;
        case MMHFilterModuleSearching:
            self.module = MMHProductModuleCategory; // TODO: - Louis -
            break;
        case MMHFilterModuleShop:
            self.module = MMHProductModuleShop;
            self.shopId = filter.shopID;
            break;
        case MMHFilterModuleBrand:
            self.module = MMHProductModuleCategory; // TODO: - Louis - IMPORTANT
            self.brandId = filter.brandID;
            break;
        default:
            break;
    }
//    self.module = MMHProductModuleCategory;
//    self.category = filter.category;
}


- (NSDictionary *)parameters
{
//    if (self.module != MMHProductModuleCategory) {
//        return @{};
//    }

    switch (self.module) {
        case MMHProductModuleCategory: {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithDictionary:[[MMHCurrentLocationModel sharedLocation] parametersForCurrentLocation]];
            
            dictionary[@"templateId"] = self.templateId;
            dictionary[@"deviceId"] = [UIDevice deviceID];

            NSString *jsonTerm = [dictionary JSONString];
            return @{@"inlet" : @(self.module), @"jsonTerm" : jsonTerm};
            break;
        }
        case MMHProductModuleShop: {
            NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            dictionary[@"templateId"] = self.templateId;
            dictionary[@"shopId"] = self.shopId;
            NSString *jsonTerm = [dictionary JSONString];
            return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
            break;
        }
        default: {
            return @{};
            break;
        }
    }
    return @{};
}


- (NSString *)displayPrice
{
    MMHPrice price = self.actualPrice;
    if (price != 0) {
        return [NSString stringWithPrice:price];
    }
    
    switch (self.module) {
        case MMHProductModuleCategory:
            price = self.minPrice;
            break;
        case MMHProductModuleShop:
            if (self.proPrice != 0) {
                price = self.proPrice;
            }
            else if (self.customPrice != 0) {
                price = self.customPrice;
            }
            else {
                price = self.retailPrice;
            }
            break;
        default:
            break;
    }
    return [NSString stringWithPrice:price];
}


@end
