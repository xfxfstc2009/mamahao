//
//  MMHProductsListModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductsListModel.h"

@implementation MMHProductsListModel


- (instancetype)initWithJSONDict:(NSDictionary *)dict keyMap:(NSDictionary *)keyMap
{
    self = [super initWithJSONDict:dict keyMap:keyMap];
    if (self) {
        self.rows = [dict[@"rows"] modelArrayOfClass:[MMHSingleProductModel class]];
    }
    return self;
}

@end
