//
//  MMHProductDetailShopInfoModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"

@protocol MMHProductDetailShopInfoModel <NSObject>

@end

@interface MMHProductDetailShopInfoModel : MMHFetchModel

@property (nonatomic,copy)NSString *shopId;                 /**< 商店编号*/
@property (nonatomic,copy)NSString *shopName;               /**< 商店名称*/
@property (nonatomic,copy)NSString *shopPic;                /**< 商店图片*/
@property (nonatomic,strong)NSArray *gps;                   /**< gps数组*/

@end
