//
//  MMHProductDetailSuperModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【超级Model - 处理数据 , 未来数据格式服务端处理完成即可删除】

#import "MMHFetchModel.h"
#import "MMHProductDetailPriceModel.h"                          // 商品价格
#import "MMHProductDetailIntroductionModel.h"                   // 商品简介
#import "MMHMamSayModel.h"                                      // 妈妈说
#import "MMHDeliveryTimeModel.h"                                // 预计送达时间
#import "MMHProductDetailGoodsTagModel.h"                       // 商品服务标签
#import "MMHProductDetailShopInfoModel.h"                       // 门店信息

@interface MMHProductDetailSuperModel : MMHFetchModel

@property (nonatomic,strong)NSArray *pic;                                           /**< 商品图片*/
@property (nonatomic,strong)MMHProductDetailPriceModel *productDetailPriceModel;    /**< 商品详情价格栏目*/
@property (nonatomic,strong)MMHProductDetailIntroductionModel *productDetailIntroductionModel;      /**<商品简介*/
@property (nonatomic,strong)MMHMamSayModel *mamSayModel;                                 /**< 妈妈说*/
@property (nonatomic,strong)MMHDeliveryTimeModel *deliveryTime;                     /**< 预计送达时间*/
@property (nonatomic,strong)NSArray<MMHProductDetailGoodsTagModel>*goodsTag;        /**< 商品标签*/
@property (nonatomic,strong)NSArray *qualityPic;                                    /**< 质检单胞的URl*/
@property (nonatomic,strong)MMHProductDetailShopInfoModel *shopInfo;                /**< 商店信息*/
@end
