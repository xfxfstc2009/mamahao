//
//  MMHPublicPraiseModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 商品口碑model
#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@interface MMHPublicPraiseModel : MMHProduct<MMHProductProtocol>


@property (nonatomic,copy)NSString *itemId;
@property (nonatomic,copy)NSString *name;                     // 用户昵称
@property (nonatomic,copy)NSString *headPic;                  // 头像
@property (nonatomic, assign)BOOL isAuth;                     //是否认证
@property (nonatomic,copy)NSString *babyAge;                // 宝宝年龄
@property (nonatomic,copy)NSString *babyBirthday;                // 宝宝年龄
@property (nonatomic,assign)BOOL isBuy;                       //是否已经购买
@property (nonatomic,assign)NSInteger star;                   // 星等级
@property (nonatomic,copy)NSString *commentContent;             // 评价内容
@property (nonatomic,copy)NSString *commentTime;                // 发布时间
@property (nonatomic,strong)NSArray *pics;                      // 宝贝图片
@property (nonatomic,strong)NSString *bufferCommentContent;     //追加评论
@property (nonatomic,strong)NSString *bufferCommentTime;        //追加评论时间
@property (nonatomic,strong)NSArray *bufferPics;                //追加评价中的图片
@property (nonatomic,strong)NSString *shopReplyContent;         //店铺评价

@end
