//
//  MMHProductDetailModel.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailModel.h"
#import "MamHao-Swift.h"


@interface MMHProductDetailModel ()

@end


@implementation MMHProductDetailModel


- (NSString *)shopId
{
    return self.shopIdentify;
}


- (void)setShopId:(NSString * __nullable)shopId
{
    self.shopIdentify = shopId;
}


- (void)configureSpecsWithArray:(NSArray *)array {
    self.specFilter = [[ProductSpecFilter alloc] initWithSpecInfoArray:array productDetail:self];
//    [self.specFilter selectDefaultIndexes];
    self.specFilter.delegate = self;
}

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"shopIdentify": @"shopId"};
}


- (void)configureGoodsTagWithArray:(NSArray *)array
{
    if (array == nil) {
        return;
    }
    
    if (array.count == 0) {
        return;
    }
    
    self.goodsTag = (NSArray<MMHProductDetailGoodsTagModel> *)[array modelArrayOfClass:[MMHProductDetailGoodsTagModel class]];
}



//- (void)productSpecFilter:(ProductSpecFilter * __null_unspecified)productSpecFilter didUpdateWithSpecParameter:(ProductSpecParameter * __null_unspecified)specParameter
//{
//    self.itemId = specParameter.itemID;
//    self.shopId = specParameter.shopID;
//    self.companyId = specParameter.companyID;
//    self.warehouseId = specParameter.warehouseID;
//    self.price = specParameter.price;
//    self.originalPrice = specParameter.originalPrice;
//    //self.avatar = specParameter.pic; // TODO: - Louis - update avatar for product detail
//    self.goodsTag = (NSArray<MMHProductDetailGoodsTagModel> *)[specParameter.goodsTag modelArrayOfClass:[MMHProductDetailGoodsTagModel class]];
//    self.deliveryTime = [[MMHDeliveryTimeModel alloc] initWithJSONDict:specParameter.deliveryTime keyMap:nil];
//    self.shop = specParameter.shopName;
//    self.shopType = specParameter.shopType;
//    self.graphicDetails = specParameter.graphicDetails;
//    self.quality = specParameter.quality;
//    // all of above, please refer to ProductSpecParameter's init method
//
//    id <MMHProductDetailModelDelegate> delegate = self.delegate;
//    if (delegate) {
//        if ([delegate respondsToSelector:@selector(productDetailDidChange:)]) {
//            [delegate productDetailDidChange:self];
//        }
//    }
//}

@end
