//
//  MMHDeliveryTimeModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【预计送货时间】
#import "MMHFetchModel.h"

@interface MMHDeliveryTimeModel : MMHFetchModel

@property (nonatomic,copy)NSString *beginTime;          /**< 开始时间*/
@property (nonatomic,copy)NSString *endTime;            /**< 结束时间*/
@property (nonatomic,copy)NSString *arriveTime;         /**< 预计送达时间*/
@property (nonatomic,assign)NSInteger arriveType;       /**< 0 - 今日, 1 - 次日*/

@property (nonatomic,assign)CGFloat mailPriceTerm;      /**< 满多少*/
@property (nonatomic,assign)CGFloat mailPrice;          /**< 收取多少*/
@property (nonatomic,copy)NSString *shop;               /**< shop*/
@property (nonatomic,copy)NSString *shopId;             /**< 商店id*/
@end
