//
//  MMHGuessYouLikeSingleModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGuessYouLikeSingleModel.h"


@implementation MMHGuessYouLikeSingleModel


- (MMHProductModule)module
{
    return MMHProductModuleGuessYouLike;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters {
    if (self.module != MMHProductModuleGuessYouLike) {
        return @{};
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[@"templateId"] = self.styleNumId;
    
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"deviceId"] = [UIDevice deviceID];
    
    NSString *jsonTerm = [dictionary JSONString];
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
