//
//  MMHProductDetailGoodsTagModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【商品服务】
#import "MMHFetchModel.h"

@protocol MMHProductDetailGoodsTagModel <NSObject>

@end

@interface MMHProductDetailGoodsTagModel : MMHFetchModel

@property (nonatomic,copy)NSString *pic;            /**< 商品标签图片*/
@property (nonatomic,copy)NSString *view;           /**< 商品标签文字*/
@property (nonatomic,copy)NSString *data;           /**< 商品描述*/

@end
