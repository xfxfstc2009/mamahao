//
//  MMHMamSayModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【妈妈说Model】
#import "MMHFetchModel.h"

@interface MMHMamSayModel : MMHFetchModel

@property (nonatomic,copy)NSString *mamCare;                        /**< 妈妈说*/
@property (nonatomic,copy)NSString *specialistName;                 /**< 专家名称*/
@property (nonatomic,copy)NSString *specialistPhoto;                /**< 专家头像*/

@end
