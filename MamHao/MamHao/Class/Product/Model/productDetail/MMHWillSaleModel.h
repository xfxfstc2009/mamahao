//
//  MMHWillSaleModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【预售Model】
#import "MMHFetchModel.h"

@interface MMHWillSaleModel : MMHFetchModel

@property (nonatomic,copy)NSString *time;               /**< 预售时间*/
@property (nonatomic,assign)NSInteger saleSize;         /**< 预售数量*/
@property (nonatomic,assign)NSInteger isBuyCount;       /**< 预售商品已销售数量*/


@end
