//
//  MMHGuessYouLikeSingleModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHProduct.h"


@protocol MMHGuessYouLikeSingleModel <NSObject>


@end

@interface MMHGuessYouLikeSingleModel : MMHProduct <MMHProductProtocol>

/*
 "itemId": 321184,//商品id
 "itemName": "NIKE FREE WOVEN TD",//商品名
 "styleNumId": null,//款式ID
 "minPrice": null,//价格
 "pic": null,//图片
 "divNumId": 6,//
 "areaId": null,//区域ID（暂时可能不需要，保留）
 "applAge": null,//适用年龄段（暂时可能不需要，保留）
 "totalSale": null,//总销量（暂时可能不需要，保留）
 "isHot": null//是否热卖 null、0为否，1为是（暂时可能不需要，保留）
 */
@property (nonatomic,copy)NSString *itemId;
@property (nonatomic,copy)NSString *itemName;           /**< 商品名*/
@property (nonatomic,copy)NSString *styleNumId;         /**< 款式Id*/
@property (nonatomic,assign)CGFloat minPrice;           /**< 商品价格*/
@property (nonatomic,copy)NSString *pic;
@property (nonatomic,copy)NSString *divNumId;           /**< 事业部编号 （暂时可能不需要，保留）*/
@property (nonatomic,copy)NSString *areaId;             /**< 区域ID（暂时可能不需要，保留）*/
@property (nonatomic,assign)NSInteger applAge;          /**< 适用年龄段（暂时可能不需要，保留）*/
@property (nonatomic,assign)NSInteger totalSale;        /**< 总销量（暂时可能不需要，保留）*/
@property (nonatomic,assign)BOOL isHot;                 /**< 是否热卖 null、0为否，1为是（暂时可能不需要，保留）*/





@end
