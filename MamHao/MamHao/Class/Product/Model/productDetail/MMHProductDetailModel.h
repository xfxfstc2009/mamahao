//
//  MMHProductDetailModel.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHWillSaleModel.h"                                        // 预售Model
#import "MMHProductDetailGoodsTagModel.h"                           // 商品标签
#import "MMHDeliveryTimeModel.h"                                    // 预计送达时间
#import "MMHFilter.h"
#import "MMHProductDetailShopInfoModel.h"

@class ProductSpecFilter;
@class MMHProductDetailModel;
@protocol MMHProductDetailModelDelegate <NSObject>
- (void)productDetailDidChange:(MMHProductDetailModel * __nonnull)productDetail;
@end


@interface MMHProductDetailModel : MMHFetchModel
@property (nonatomic, weak, nullable) id<MMHProductDetailModelDelegate> delegate;

NS_ASSUME_NONNULL_BEGIN
// 1. 图片
@property (nonatomic,copy)NSString *templateId;                     /**< 款式ID*/
@property (nonatomic,copy)NSString *itemId;                         /**< 商品Id  inlet=3,4*/
@property (nonatomic,strong)NSArray *pic;                           /**< 商品图片*/

// 2. 价格
@property (nonatomic,assign)MMHPrice price;                         /**< 出售价格*/
@property (nonatomic,assign)MMHPrice originalPrice;                 /**< 原价*/
@property (nonatomic,copy)NSString *actEndTime;                     /**< -活动结束时间，客户端用于显示剩余时间  inlet=3,4*/
@property (nonatomic,assign)NSInteger actItemCount;                 /**< -活动商品剩余数量*/
@property (nonatomic,assign)BOOL isCollect;                         /**< 是否收藏*/
@property (nonatomic,copy)NSString *getmBean;                       /**< 购买此商品可获得的麻豆数量*/

// 2.1 价格-妈豆
@property (nonatomic,assign)NSInteger mBeanPay;                     /**< 妈豆支付价inlet=3,4*/
@property (nonatomic,assign)NSInteger purchaseQuantity;             /**< 用户限购数量inlet=3,4*/
@property (nonatomic,assign)NSInteger mBean;                        /**< 用户的麻豆*/

// 3. 标题
@property (nonatomic,assign)NSInteger shopType;                     /**< 1/2, -店铺的营业类型 自营店，2非自营店 inlet=2,3,4*/
@property (nonatomic,copy)NSString *title;                          /**< 商品标题*/
@property (nonatomic,copy)NSString *subtitle;                       /**< 商品副标题*/

// 4. 妈妈说
@property (nonatomic,copy)NSString *specialistName;                 /**< 专家名称*/
@property (nonatomic,copy)NSString *specialistPhoto;                /**< 专家头像*/
@property (nonatomic,copy)NSString *mamCare;                        /**< 妈妈说*/

// 5. 送至
@property (nonatomic,strong)MMHDeliveryTimeModel *deliveryTime;     /**< 预计送达时间*/
@property (nonatomic,assign)CGFloat mailPriceTerm;                  /**< 免邮标准-不达到此标准则收取邮费，0或为空都要收取*/
@property (nonatomic,assign)CGFloat mailPrice;                      /**< 邮费*/
@property (nonatomic,strong)NSArray<MMHProductDetailGoodsTagModel>*goodsTag;        /**< 商品标签*/
@property (nonatomic,copy)NSString *shop;                           /**< 商店*/

// 6.质检担保
@property (nonatomic,copy)NSString *quality;                        /**< 质检担保*/
@property (nonatomic,strong)NSArray *qualityPic;                    /**< 质检担保URL 数组*/
@property (nonatomic,copy)NSString *graphicDetails;                 /**< 图文详情*/

// 7.
@property (nonatomic,copy)NSString *shopIdentify;                           /**< 商店编号*/
@property (nonatomic,copy, nullable)NSString *shopId;                         /**< 所属商店，当为空时标识为平台，否则为实体店*/
@property (nonatomic,copy, nullable)NSString *companyId;                      /**< 公司id*/
@property (nonatomic,copy, nullable)NSString *warehouseId;                    /**< 仓储id*/
@property (nonatomic,copy)NSString *goodsNo;                                  /**< 商品编号*/

// 8.shopInfo
@property (nonatomic,strong)MMHProductDetailShopInfoModel *shopInfo;        /**< 商店信息*/

// 9. sku
@property (nonatomic, strong) ProductSpecFilter *specFilter;                /**< sku*/



@property (nonatomic) MMHProductModule module;                      /**< 进入入口*/
@property (nonatomic) BOOL bindsShop;                               /**< 是否绑定商店*/
@property (nonatomic) BOOL isBeanProduct;                           /**< 是否是妈豆商品*/
@property (nonatomic,strong)MMHWillSaleModel *willSale;             /**< 预售模块*/









@property (nonatomic,copy)NSString *goodsTempId;                    // 商品template ID


@property (nonatomic,copy)NSString *goodsBelongShopId;              // 商店所属id
@property (nonatomic,copy)NSString *goodsBelongShopName;            // 商店名称
@property (nonatomic,assign)NSInteger businessType;                 // 运营类型
@property (nonatomic,copy)NSString *specialistPortraitURLString;    // 专家头像图片地址
@property (nonatomic,assign)NSInteger banndBuying;                  // 限购数量
@property (nonatomic,copy)NSString *staticDetailUrl;                // 静态详情路径
@property (nonatomic,assign)NSInteger delivery_mode;                // 配送方式
@property (nonatomic,assign)NSInteger qualityTesting;               // 0/1(0有/1无质检报告)
@property (nonatomic,strong)NSArray *tag;                           // 7天退货等按钮


//@property (nonatomic,copy)NSString *graphicDetailsURLString;        // 图文详情页面地址

@property (nonatomic,copy)NSString *templateNo;                     //

@property (nonatomic,copy)NSString *areaId;
@property (nonatomic,copy)NSString *itemNo;                         // 商品编号




- (void)configureSpecsWithArray:(NSArray *)array;
- (void)configureGoodsTagWithArray:(NSArray * __nullable)array;
NS_ASSUME_NONNULL_END
@end
