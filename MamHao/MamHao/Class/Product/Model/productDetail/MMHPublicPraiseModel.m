//
//  MMHPublicPraiseModel.m
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPublicPraiseModel.h"

@implementation MMHPublicPraiseModel
- (instancetype)initWithJSONDict:(NSDictionary *)dict{
    if (self = [super initWithJSONDict:dict]) {
        self.templateId = dict[@"templateId"];
    }
    return self;
}
-(NSDictionary *)parameters{
    if (self.module!=MMHProductModuleComment) {
        return @{};
    }
    NSMutableDictionary *jsonDic =[NSMutableDictionary dictionary];
    if (self.templateId) {
        [jsonDic setObject:self.templateId forKey:@"templateId"];
    }
    if (_itemId) {
        [jsonDic setObject:_itemId forKey:@"itemId"];
    }
    NSString *areaId = [MMHCurrentLocationModel sharedLocation].areaId;
    if (areaId.length != 0) {
        [jsonDic setObject:areaId forKey:@"areaId"];
    }
    long lng =[MMHCurrentLocationModel sharedLocation].lng;
    long lat =[MMHCurrentLocationModel sharedLocation].lat;
    if (lng) {
        [jsonDic setObject:@(lng) forKey:@"lng"];
    }
    if (lat) {
        [jsonDic setObject:@(lat) forKey:@"lat"];
    }
    NSString *deviceId = [UIDevice deviceID];
    if (deviceId) {
        [jsonDic setObject:deviceId forKey:@"deviceId"];
    }
    NSString *jsonTerm = [jsonDic JSONString];
    return @{@"inlet":@(self.module), @"jsonTerm":jsonTerm};
}
- (MMHProductModule)module{
    return MMHProductModuleComment
    ;
}

- (MMHProductDetailSaleType)productDetailSaleType{
    return   MMHProductDetailSaleTypeRMB;
}
- (BOOL)isBeanProduct{
    return NO;
}
- (BOOL)bindsShop{
    return NO;
}



@end
