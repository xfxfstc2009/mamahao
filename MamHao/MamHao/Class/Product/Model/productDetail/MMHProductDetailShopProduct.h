//
//  MMHProductDetailShopProduct.h
//  MamHao
//
//  Created by SmartMin on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProduct.h"


@class MMHProductDetailModel;


@interface MMHProductDetailShopProduct : MMHProduct <MMHProductProtocol>

@property (nonatomic, strong) NSString *itemId;

- (instancetype)initWithProductDetail:(MMHProductDetailModel *)productDetail;

@end
