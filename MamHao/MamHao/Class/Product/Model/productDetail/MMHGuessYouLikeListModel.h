//
//  MMHGuessYouLikeListModel.h
//  MamHao
//
//  Created by SmartMin on 15/4/20.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHGuessYouLikeSingleModel.h"
@interface MMHGuessYouLikeListModel : MMHFetchModel

@property (nonatomic,strong)NSArray <MMHGuessYouLikeSingleModel> *data;

@end
