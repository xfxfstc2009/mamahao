//
//  MMHProductDetailPriceModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【商品详情 -价格栏目】
#import "MMHFetchModel.h"

@interface MMHProductDetailPriceModel : MMHFetchModel
@property (nonatomic,assign)BOOL isCollect;                         /**< 是否收藏*/
@property (nonatomic,copy)NSString *getmBean;                       /**< 购买此商品可获得的麻豆数量*/
@property (nonatomic,assign)MMHPrice price;                         /**< 出售价格*/
@property (nonatomic,assign)MMHPrice originalPrice;                 /**< 原价*/
@property (nonatomic,copy)NSString *itemId;                         /**< itemId*/

// 妈豆
@property (nonatomic,assign)NSInteger mBeanPay;                     /**< 妈豆*/

// 收藏
@property (nonatomic,copy)NSString *templateId;                     /**categoryID*/

@end
