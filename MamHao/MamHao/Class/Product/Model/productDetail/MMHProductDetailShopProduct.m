//
//  MMHProductDetailShopProduct.m
//  MamHao
//
//  Created by SmartMin on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailShopProduct.h"
#import "MMHProductDetailModel.h"


@implementation MMHProductDetailShopProduct


- (instancetype)initWithProductDetail:(MMHProductDetailModel *)productDetail
{
    self = [self init];
    if (self) {
        self.templateId = productDetail.templateId;
        self.itemId = productDetail.itemId;
    }
    return self;
}


- (MMHProductModule)module
{
    return MMHProductModuleProductDetailShop;
}


- (BOOL)bindsShop
{
    return NO;
}


- (BOOL)isBeanProduct
{
    return NO;
}


- (MMHProductDetailSaleType)productDetailSaleType
{
    return MMHProductDetailSaleTypeRMB;
}


- (NSDictionary *)parameters
{
    if (self.module != MMHProductModuleProductDetailShop) {
        return @{};
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    dictionary[@"templateId"] = self.templateId;
    dictionary[@"itemId"] = self.itemId;
    dictionary[@"areaId"] = [MMHCurrentLocationModel sharedLocation].areaId;
    dictionary[@"lat"] = @([MMHCurrentLocationModel sharedLocation].lat);
    dictionary[@"lng"] = @([MMHCurrentLocationModel sharedLocation].lng);
    
    dictionary[@"deviceId"] = [UIDevice deviceID];
    
    NSString *jsonTerm = [dictionary JSONString];
    LZLog(@"product detail shop product parameters is: %@", @{@"inlet": @(self.module), @"jsonTerm": jsonTerm});
    return @{@"inlet": @(self.module), @"jsonTerm": jsonTerm};
}


@end
