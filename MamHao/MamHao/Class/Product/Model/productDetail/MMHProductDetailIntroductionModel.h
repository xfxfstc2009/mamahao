//
//  MMHProductDetailIntroductionModel.h
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【商品介绍Model 】
#import "MMHFetchModel.h"

@interface MMHProductDetailIntroductionModel : MMHFetchModel

@property (nonatomic,copy)NSString *title;                          /**< 商品标题*/
@property (nonatomic,copy)NSString *subtitle;                       /**< 商品副标题*/
@property (nonatomic,assign)NSInteger shopType;                     /**< 1/2, -店铺的营业类型 自营店，2非自营店 inlet=2,3,4*/
@property (nonatomic,copy)NSString *shopId;                         /**< 所属商店，当为空时标识为平台，否则为实体店*/

@end
