//
//  MMHProductDetailSuperModel.m
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailSuperModel.h"

@implementation MMHProductDetailSuperModel

-(void)setProductDetailIntroductionModel:(MMHProductDetailIntroductionModel *)productDetailIntroductionModel{
    _productDetailIntroductionModel = productDetailIntroductionModel;
}

-(void)setProductDetailPriceModel:(MMHProductDetailPriceModel *)productDetailPriceModel{
    _productDetailPriceModel = productDetailPriceModel;
}

-(void)setMamSayModel:(MMHMamSayModel *)mamSayModel{
    _mamSayModel = mamSayModel;
}

-(void)setDeliveryTime:(MMHDeliveryTimeModel *)deliveryTime{
    _deliveryTime = deliveryTime;
}

-(void)setGoodsTag:(NSArray<MMHProductDetailGoodsTagModel> *)goodsTag{
    _goodsTag = goodsTag;
}
@end
