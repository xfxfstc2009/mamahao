//
//  MMHProductList.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/9.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductList.h"
#import "MMHFilter.h"
#import "MMHNetworkAdapter.h"
#import "MMHProductsListModel.h"
#import "MMHNetworkAdapter+Product.h"


@interface MMHProductList ()

@property (nonatomic, strong) MMHFilter *filter;
@end


@implementation MMHProductList


- (instancetype)initWithFilter:(MMHFilter *)filter
{
    self = [self init];
    if (self) {
        self.filter = filter;
    }
    return self;
}


- (void)fetchItemsAtPage:(NSInteger)page succeededHandler:(void (^)(NSArray *fetchedItems))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    MMHFilter *filter = self.filter;
    [[MMHNetworkAdapter sharedAdapter] fetchProductListWithFilter:self.filter
                                                             page:page
                                                         pageSize:self.pageSize
                                                             from:self
                                                 succeededHandler:^(MMHProductsListModel *productsListModel) {
                                                     for (MMHSingleProductModel *singleProduct in productsListModel.rows) {
                                                         [singleProduct configureWithFilter:filter];
                                                     }
                                                     succeededHandler(productsListModel.rows);
                                                 } failedHandler:failedHandler];
}


@end
