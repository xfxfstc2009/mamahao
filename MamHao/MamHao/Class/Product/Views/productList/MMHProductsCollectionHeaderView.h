//
//  MMHProductsCollectionHeaderView.h
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHFilter.h"
#import "MMHShoppingTipsModel.h"
#import "MMHSortView.h"

typedef void (^detailButtonClickBlock)();
typedef void (^headerViewHeightBlock)(CGFloat headerViewHeight);

@interface MMHProductsCollectionHeaderView : UICollectionReusableView

// bannerArray
@property (nonatomic,strong)UIScrollView *bannerScrollView;             // bannerScrollView

// 须知
@property (nonatomic,strong)UILabel *needKnowLabel;                     // 须知label

// block
@property (nonatomic,copy)detailButtonClickBlock detailButtonClickBlock;    // 跳转webView
@property (nonatomic,copy)headerViewHeightBlock headerViewHeightBlock;      // 设置header高度

// model
@property (nonatomic,strong)MMHShoppingTipsModel *shoppingTipsModel;         // 商品店招


@property (nonatomic, strong) MMHFilter *filter;
@property (nonatomic,strong)MMHSortView *sortView;

@end
