//
//  MMHProductsCollectionViewCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHSingleProductModel.h"                     // 单个商品的model
@interface MMHProductsCollectionViewCell : UICollectionViewCell

#pragma mark - UI
@property (nonatomic,strong)MMHImageView *productImageView;                         // 商品图片
@property (nonatomic,strong)UILabel *goodsTitleLabel;                               // 商品标题
@property (nonatomic,strong)UILabel *productPriceLabel;                             // 商品价格
@property (nonatomic,strong)UIView  *productBackgroundView;                         // 背景图片

#pragma mark - Model
@property (nonatomic,strong)MMHSingleProductModel *productModel;

@end
