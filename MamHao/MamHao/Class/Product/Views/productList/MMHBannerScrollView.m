//
//  MMHBannerScrollView.m
//  MamHao
//
//  Created by SmartMin on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHBannerScrollView.h"

@interface MMHBannerScrollView()<UIScrollViewDelegate>
@property (nonatomic,strong)UIPageControl *bannerPageControl;
@property (nonatomic,strong)NSTimer *myAutoTimer;

@end

@implementation MMHBannerScrollView


-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.bounces = NO;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.pagingEnabled = YES;
        self.delegate = self;
        self.frame = self.bounds;
        self.directionalLockEnabled = YES;//锁定滑动的方向

    }
    return self;
}



#pragma mark - setInfo
-(void)setBannerImageUrlArray:(NSArray *)bannerImageUrlArray{
    if (bannerImageUrlArray.count){
        _bannerImageUrlArray = bannerImageUrlArray;
        [self createImageViewInScrollViewWithImgArray:bannerImageUrlArray];
        self.contentSize = CGSizeMake(bannerImageUrlArray.count * self.bounds.size.width, self.bounds.size.height);

        // 创建pageControl
        [self createPageControlWithNumberOfPage:bannerImageUrlArray.count];
    }
}

-(void)setIsAutoItemer:(BOOL)isAutoItemer{
    if ([self.myAutoTimer isValid]){
        [self.myAutoTimer invalidate];
        self.myAutoTimer = nil;
    }
    if (isAutoItemer){
        self.myAutoTimer = [NSTimer scheduledTimerWithTimeInterval:.5f target:self selector:@selector(autoChangeScrollView) userInfo:nil repeats:YES];
    }
}


#pragma mark - customInitImageView
-(void)createImageViewInScrollViewWithImgArray:(NSArray *)imageArray{
    for (int i = 0 ; i < imageArray.count ; i++){
        [self createImageViewWithIndex:i];
    }
}

-(void)createImageViewWithIndex:(NSInteger)index{
    UIImageView *bannerImageView = [[UIImageView alloc]init];
    bannerImageView.backgroundColor = [UIColor blueColor];
    bannerImageView.frame = CGRectMake(self.bounds.size.width * index, 0, self.bounds.size.width, self.bounds.size.height);
    bannerImageView.image = [UIImage imageNamed:[_bannerImageUrlArray objectAtIndex:index]];
    [self addSubview:bannerImageView];
}

-(void)createPageControlWithNumberOfPage:(NSInteger)numbersOfPage{
    _bannerPageControl = [[UIPageControl alloc]init];
    _bannerPageControl.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:.2f];
    _bannerPageControl.numberOfPages = numbersOfPage;
    _bannerPageControl.currentPage = 0;
    _bannerPageControl.enabled = YES;
    _bannerPageControl.pageIndicatorTintColor = [UIColor hexChangeFloat:@"BFBEBE"];
    _bannerPageControl.currentPageIndicatorTintColor = [UIColor hexChangeFloat:@"FC687C"];
    [_bannerPageControl addTarget:self action:@selector(manualPageControlChange) forControlEvents:UIControlEventValueChanged];
    [self.superview addSubview:_bannerPageControl];
    // 计算frame
    CGSize pageControlSize = [_bannerPageControl sizeForNumberOfPages:numbersOfPage];
    _bannerPageControl.frame = CGRectMake(self.bounds.size.width - pageControlSize.width, self.bounds.size.height -pageControlSize.height, pageControlSize.width,pageControlSize.height);
    
}

#pragma mark - actionClick
-(void)autoChangeScrollView{
//    // 1.pagecontrol改变
    NSInteger page = self.contentOffset.x/self.bounds.size.width;
    page ++ ;
    if (page == _bannerImageUrlArray.count){
        page = 0;
        [self setContentOffset:CGPointMake(page * self.bounds.size.width, 0) animated:YES];
        _bannerPageControl.currentPage = page;
    } else {
        [self setContentOffset:CGPointMake(page * self.bounds.size.width, 0) animated:YES];
        _bannerPageControl.currentPage = page;
    }
}



// 手动修改frame
-(void)manualPageControlChange{
    NSLog(@"%li",(long)self.bannerPageControl.currentPage);
    // 1. 获取当前用户点击的点
    [self setContentOffset:CGPointMake(self.bannerPageControl.currentPage * self.bounds.size.width, 0) animated:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.isAutoItemer = NO;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    self.isAutoItemer = YES;
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
     NSInteger page = self.contentOffset.x/self.bounds.size.width;
    _bannerPageControl.currentPage = page;
}

@end
