//
//  MMHProductsCollectionViewCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductsCollectionViewCell.h"

@interface MMHProductsCollectionViewCell() <MMHProductDelegate>

@end

@implementation MMHProductsCollectionViewCell

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景
    _productBackgroundView = [[UIView alloc]init];
    _productBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _productBackgroundView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.productBackgroundView];
    
    // 2. 创建商品图片
    _productImageView = [[MMHImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, MMHFloat(173))];
    _productImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    _productImageView.backgroundColor = [UIColor clearColor];
    [self.productBackgroundView addSubview:self.productImageView];
    
    // 3. 创建商品标题
    _goodsTitleLabel = [[UILabel alloc]initWithFrame:CGRectMake(MMHFloat(7), CGRectGetMaxY(_productImageView.frame) + MMHFloat(13), self.bounds.size.width - 2 * MMHFloat(7), 0.0f)];
    _goodsTitleLabel.backgroundColor = [UIColor clearColor];
    _goodsTitleLabel.textColor = [UIColor blackColor];
    _goodsTitleLabel.numberOfLines = 0;
    _goodsTitleLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    _goodsTitleLabel.textAlignment = NSTextAlignmentLeft;
    [self.productBackgroundView addSubview:self.goodsTitleLabel];
    
    // 5.创建商品价格
    _productPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(MMHFloat(7), CGRectGetMaxY(self.goodsTitleLabel.frame) + MMHFloat(12), 0.0f, 0.0f)];
    _productPriceLabel.backgroundColor = [UIColor clearColor];
    _productPriceLabel.textColor = [UIColor redColor];
    _productPriceLabel.font = [UIFont fontWithCustomerSizeName:@"副标题"];
    _productPriceLabel.textAlignment = NSTextAlignmentLeft;
    [self.productBackgroundView addSubview:self.productPriceLabel];
}




#pragma mark - 赋值
-(void)setProductModel:(MMHSingleProductModel *)productModel{
    if (_productModel != productModel){
        _productModel = productModel;
        productModel.delegate = self;
        
        // 商品图片
        [self.productImageView updateViewWithImageAtURL:productModel.imageURLString];
        
        // 简介
        [self.goodsTitleLabel setText:productModel.itemName constrainedToLineCount:2];
        
        // 价格
        [self.productPriceLabel attachToBottomSideOfView:self.goodsTitleLabel byDistance:MMHFloat(12.0f)];
        [self.productPriceLabel setSingleLineText:[productModel displayPrice]];
    }
}


- (void)product:(MMHProduct *)product actualPriceFetched:(MMHPrice)price
{
    if (![self.productModel.templateId isEqualToString:product.templateId]) {
        return;
    }

    [self.productPriceLabel attachToBottomSideOfView:self.goodsTitleLabel byDistance:MMHFloat(12.0f)];
    [self.productPriceLabel setSingleLineText:[NSString stringWithPrice:price]];
}


@end
