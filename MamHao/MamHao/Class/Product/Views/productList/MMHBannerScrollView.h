//
//  MMHBannerScrollView.h
//  MamHao
//
//  Created by SmartMin on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMHBannerScrollView : UIScrollView

@property (nonatomic,strong)NSArray *bannerImageUrlArray;               // banner的图片信息
@property (nonatomic,assign)BOOL isAutoItemer;                          // 是否自动滚动
@property (nonatomic,assign)BOOL isShowPageControl;                     // 是否显示pageControl
@end
