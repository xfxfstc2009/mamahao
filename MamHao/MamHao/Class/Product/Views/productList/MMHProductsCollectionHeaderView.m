//
//  MMHProductsCollectionHeaderView.m
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductsCollectionHeaderView.h"
#import "MMHBannerScrollView.h"


#define kNeedKnowLabelFont  [UIFont systemFontOfSize:mmh_relative_float(12.)]
@interface MMHProductsCollectionHeaderView()


// needKone
@property (nonatomic,strong)UIView *needKnowBackgroundView;
@property (nonatomic,strong)UIButton *openButton;
@property (nonatomic,strong)UIButton *detailButton;
@property (nonatomic,assign)BOOL needKnowIsOpen;

// segment
@property (nonatomic,strong)UIView *lineView;
@end

@implementation MMHProductsCollectionHeaderView


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self createScrollView];                        // 创建banner
        [self createNeedKnowView];                      // 创建须知
        
    }
    return self;
}

#pragma mark createUI
-(void)createScrollView{
    self.bannerScrollView = [[MMHBannerScrollView alloc]init];
    self.bannerScrollView.backgroundColor = [UIColor clearColor];
    self.bannerScrollView.frame = CGRectMake(0, 0, self.bounds.size.width, self.shoppingTipsModel.banner.count * MMHFloat(140));
    [self addSubview:self.bannerScrollView];
}


-(void)createNeedKnowView{
    // 创建背景
    self.needKnowBackgroundView = [[UIView alloc]init];
    self.needKnowBackgroundView.backgroundColor = [UIColor clearColor];
    self.needKnowBackgroundView.frame = CGRectMake(0,CGRectGetMaxY(self.bannerScrollView.frame), self.bounds.size.width, 60);
    [self addSubview:self.needKnowBackgroundView];
    
    // 创建须知imageView
    UIImageView *needKnowLogoImageView = [[UIImageView alloc]init];
    needKnowLogoImageView.backgroundColor = [UIColor clearColor];
    needKnowLogoImageView.frame = CGRectMake(mmh_relative_float(11), 0, mmh_relative_float(12), mmh_relative_float(20));
    needKnowLogoImageView.image = [UIImage imageNamed:@"productList_needknow"];
    [self.needKnowBackgroundView addSubview:needKnowLogoImageView];
    
    // 创建须知文字
    UILabel *needKnowFixedLabel = [[UILabel alloc]init];
    needKnowFixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(14.)];
    needKnowFixedLabel.text = @"购买须知";
    needKnowFixedLabel.backgroundColor = [UIColor clearColor];
    needKnowFixedLabel.textColor = [UIColor mamhaoMainColor];
    CGSize needKonwFixedContentOfSize = [[NSString stringWithFormat:@"购买须知"]sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(14.)] constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    needKnowFixedLabel.frame = CGRectMake(CGRectGetMaxX(needKnowLogoImageView.frame) + mmh_relative_float(6), mmh_relative_float(4), 100, needKonwFixedContentOfSize.height);
    [self.needKnowBackgroundView addSubview:needKnowFixedLabel];
    
    // 创建须知label
    self.needKnowLabel = [[UILabel alloc]init];
    self.needKnowLabel.backgroundColor = [UIColor clearColor];
    CGSize needKnowSingleLineHeight = [[NSString stringWithFormat:@"购买须知"]sizeWithCalcFont:kNeedKnowLabelFont constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    self.needKnowLabel.frame = CGRectMake(mmh_relative_float(11),mmh_relative_float(31), self.needKnowBackgroundView.bounds.size.width - mmh_relative_float(11) - mmh_relative_float(15), needKnowSingleLineHeight.height);
    self.needKnowLabel.font = kNeedKnowLabelFont;
    self.needKnowLabel.textColor = [UIColor hexChangeFloat:@"666666"];
    [self.needKnowBackgroundView addSubview:self.needKnowLabel];
    
    // 创建button
    self.openButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.openButton.backgroundColor = [UIColor clearColor];
    [self.openButton addTarget:self action:@selector(needKnowOpneAndClose) forControlEvents:UIControlEventTouchUpInside];
    UIImage *openImage = [UIImage imageNamed:@"productList_down"];
    self.openButton.frame = CGRectMake(self.bounds.size.width - mmh_relative_float(25 + 16),mmh_relative_float(4),mmh_relative_float(30),mmh_relative_float(30));
    [self.openButton setImage:openImage forState:UIControlStateNormal];
    [self.needKnowBackgroundView addSubview:self.openButton];
    
    
    // 创建跳转button
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.detailButton.frame = CGRectMake(self.bounds.size.width - 150, CGRectGetMaxY(self.needKnowLabel.frame ), mmh_relative_float(100), mmh_relative_float(30));
    [self.detailButton addTarget:self action:@selector(detailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.detailButton.backgroundColor = [UIColor clearColor];
    NSString *detailString = @"查看更多";
    [self.detailButton setImage:[UIImage imageNamed:@"productList_knowDetail"] forState:UIControlStateNormal];
    self.detailButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.detailButton.adjustsImageWhenHighlighted = NO;
    [self.detailButton setTitle:detailString forState:UIControlStateNormal];
    self.detailButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(13.)];
    [self.detailButton setTitleEdgeInsets:UIEdgeInsetsMake(0,- 10, 0, 0)];
    [self.detailButton setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [self.detailButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [self.needKnowBackgroundView addSubview:self.detailButton];
    self.detailButton.hidden = YES;
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"dcdcdc"];
    self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.needKnowBackgroundView.frame) - 1, self.bounds.size.width, .5f);
    [self addSubview:self.lineView];
    
}

-(void)needKnowOpneAndClose{
    CGSize needKnowSingleLineHeight = [[NSString stringWithFormat:@"购买须知"]sizeWithCalcFont:kNeedKnowLabelFont constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)];
    // block 宽度
    CGFloat headerHeight = 0 ;
    
    if (self.needKnowIsOpen){                   // 展开状态 - 需要关闭
        // 1. 设置背景
        self.needKnowBackgroundView.frame = CGRectMake(0, CGRectGetMaxY(self.bannerScrollView.frame), self.bounds.size.width, 60);
        
        
            // 2. 设置label
            self.needKnowLabel.numberOfLines = 1;
            
            self.needKnowLabel.frame = CGRectMake(mmh_relative_float(11),mmh_relative_float(31), self.needKnowBackgroundView.bounds.size.width - mmh_relative_float(11) - mmh_relative_float(15), needKnowSingleLineHeight.height);
//        }];
        // 3. 隐藏详情button
        self.detailButton.hidden = YES;
        // 4.
        self.sortView.frame = CGRectMake(0 , CGRectGetMaxY(self.needKnowBackgroundView.frame),self.bounds.size.width, mmh_relative_float(42));
        
        // 5. 线
        self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.needKnowBackgroundView.frame) - 1, self.bounds.size.width, .5f);
        
        // 3. 动画
        [self rotationWithView:self.openButton andtrans:(0 * M_PI)/180];
        
        // 4. block - height
        headerHeight =  mmh_relative_float(42) + self.needKnowBackgroundView.frame.size.height + self.shoppingTipsModel.banner.count * MMHFloat(140);
        NSLog(@"%.2f",CGRectGetMaxY(self.needKnowBackgroundView.frame));
        
    } else {                                    // 关闭状态 - 需要展开
        
        // 1. 计算文字长度
        CGSize needKonwContentOfSize = [self.shoppingTipsModel.desc sizeWithCalcFont:kNeedKnowLabelFont constrainedToSize:CGSizeMake(self.bounds.size.width - mmh_relative_float(11) - mmh_relative_float(15), CGFLOAT_MAX)];
        
        // 2. 设置背景高度
        self.needKnowBackgroundView.frame = CGRectMake(0, CGRectGetMaxY(self.bannerScrollView.frame), self.bounds.size.width, needKonwContentOfSize.height + mmh_relative_float(31) + mmh_relative_float(30));
                NSLog(@"%.2f",CGRectGetMaxY(self.needKnowBackgroundView.frame));
//        [UIView animateWithDuration:.9f animations:^{
            // 3. 设置文字高度
            self.needKnowLabel.numberOfLines = 0;
            self.needKnowLabel.frame = CGRectMake(mmh_relative_float(11),mmh_relative_float(31), self.needKnowBackgroundView.bounds.size.width - mmh_relative_float(11) - mmh_relative_float(15), needKonwContentOfSize.height);
        // 4. 显示详情button
        self.detailButton.hidden = YES;
        self.detailButton.frame = CGRectMake(self.bounds.size.width - 100, CGRectGetMaxY(self.needKnowLabel.frame ), mmh_relative_float(100), mmh_relative_float(30));

//        }];
        // 4. 动画
        [self rotationWithView:self.openButton andtrans:(180 * M_PI) / 180.];
        // 5.segment
        self.sortView.frame = CGRectMake(0 , CGRectGetMaxY(self.needKnowBackgroundView.frame),self.bounds.size.width, mmh_relative_float(42));
        
        // 6. 线
        self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.needKnowBackgroundView.frame) - 1, self.bounds.size.width, .5f);

        // 5.
        headerHeight =  mmh_relative_float( 42) + self.needKnowBackgroundView.frame.size.height + self.shoppingTipsModel.banner.count * MMHFloat(140);

    }
    self.headerViewHeightBlock(headerHeight);
    self.needKnowIsOpen = !self.needKnowIsOpen;
}


- (void)setFilter:(MMHFilter *)filter{
    _filter = filter;
    // 创建segmentViewController
    if (!self.sortView) {
        self.sortView= [[MMHSortView alloc] initWithFrame:CGRectMake(0 , CGRectGetMaxY(self.needKnowBackgroundView.frame),self.bounds.size.width, mmh_relative_float(42)) filter:self.filter];
        self.sortView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.sortView];
    }
    else {
        self.sortView.filter = filter;
    }
}

-(void)setShoppingTipsModel:(MMHShoppingTipsModel *)shoppingTipsModel{
    _shoppingTipsModel = shoppingTipsModel;
    // banner
    if(!self.bannerScrollView.subviews.count){
        for (int i = 0 ; i < shoppingTipsModel.banner.count ; i ++){
            UIImageView *bannerImageView = [[UIImageView alloc]init];
            bannerImageView.backgroundColor =[UIColor clearColor];
            bannerImageView.frame = CGRectMake(0, 0 + i * (MMHFloat(140)), self.bannerScrollView.bounds.size.width, MMHFloat(140))
            ;
            bannerImageView.image = [UIImage imageNamed:@"login_thirdLogin_weibo"];
            [self.bannerScrollView addSubview:bannerImageView];
        }
        // needKnow
        self.needKnowLabel.text = shoppingTipsModel.desc;
        self.bannerScrollView.frame = CGRectMake(0, 0, self.bounds.size.width, self.shoppingTipsModel.banner.count * MMHFloat(140));
        if (shoppingTipsModel.banner.count){
            self.needKnowBackgroundView.frame = CGRectMake(0, CGRectGetMaxY(self.bannerScrollView.frame), self.bounds.size.width, 60);
            self.sortView.frame = CGRectMake(0 , CGRectGetMaxY(self.needKnowBackgroundView.frame),self.bounds.size.width, mmh_relative_float(42));
            
            // 6. 线
            self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.needKnowBackgroundView.frame) - 1, self.bounds.size.width, .5f);
        }
    }
}



#pragma mark 旋转动画
-(void)rotationWithView:(UIView *)view andtrans:(CGFloat)transForm{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.2];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    CGAffineTransform rotationTrans = CGAffineTransformMakeRotation(transForm);
    view.transform = rotationTrans;
    [UIView commitAnimations];
}


#pragma mark actionClick
-(void)detailButtonClick:(UIButton *)sender{
    self.detailButtonClickBlock();
}

@end
