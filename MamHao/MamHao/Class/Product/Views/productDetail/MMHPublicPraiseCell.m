//
//  MMHPublicPraiseCell.m
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPublicPraiseCell.h"
#import "MMHImageView.h"

@implementation MMHPublicPraiseCell


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


- (UIScrollView *)imageViewScroll {
    if (!_imageViewScroll) {
        self.imageViewScroll = [[UIScrollView alloc]init];
        self.imageViewScroll.backgroundColor = [UIColor clearColor];
        self.imageViewScroll.showsHorizontalScrollIndicator = NO;
        self.imageViewScroll.showsVerticalScrollIndicator = NO;
        [self addSubview:_imageViewScroll];
    }
    return _imageViewScroll;
}
#pragma mark 创建view
-(void)createView{
    // 1. 创建头像
    self.headerImageView  = [[MMHImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor redColor];
    self.headerImageView.frame = CGRectMake(mmh_relative_float(11), mmh_relative_float(20), mmh_relative_float(42), mmh_relative_float(42));
    self.headerImageView.layer.cornerRadius = mmh_relative_float(6.0f);
    [self addSubview:self.headerImageView];
    // 2. 创建用户昵称
    self.userNickLabel = [[UILabel alloc]init];
    self.userNickLabel.backgroundColor = [UIColor clearColor];
    self.userNickLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + mmh_relative_float(10), CGRectGetMinX(self.headerImageView.frame) + mmh_relative_float(5), 100, 20);
    self.userNickLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
    self.userNickLabel.textAlignment = NSTextAlignmentLeft;
    self.userNickLabel.textColor = [UIColor hexChangeFloat:@"585858"];
    [self addSubview:self.userNickLabel];
    
    // 3. 创建宝宝年龄
    self.ageLabel = [[UILabel alloc]init];
    self.ageLabel.backgroundColor = [UIColor clearColor];
    self.ageLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
    self.ageLabel.textAlignment = NSTextAlignmentLeft;
    self.ageLabel.textColor = [UIColor hexChangeFloat:@"b8b8b8"];
    self.ageLabel.frame = CGRectMake(self.userNickLabel.frame.origin.x, CGRectGetMaxY(self.userNickLabel.frame), 100, 20);
    [self addSubview:self.ageLabel];
    
    // 4. 创建星星
    self.starBackgroundView = [[UIView alloc]init];
    self.starBackgroundView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.starBackgroundView];
    
    
    // 5.创建发布时间
    self.dateTimeLabel = [[UILabel alloc]init];
    self.dateTimeLabel.backgroundColor = [UIColor clearColor];
    self.dateTimeLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
    self.dateTimeLabel.textAlignment = NSTextAlignmentRight;
    self.dateTimeLabel.textColor = [UIColor hexChangeFloat:@"b8b8b8"];
    [self addSubview:self.dateTimeLabel];
    
    // 6. 创建评论
    self.evaluationLabel = [[UILabel alloc]init];
    self.evaluationLabel.backgroundColor = [UIColor clearColor];
    self.evaluationLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
    self.evaluationLabel.textAlignment = NSTextAlignmentLeft;
    self.evaluationLabel.textColor = [UIColor hexChangeFloat:@"585858"];
    self.evaluationLabel.numberOfLines = 0;
    [self addSubview:self.evaluationLabel];
    
    
}

-(void)setPublicPraiseModel:(MMHPublicPraiseModel *)publicPraiseModel{
    _publicPraiseModel = publicPraiseModel;
    self.userNickLabel.text = publicPraiseModel.name;
    self.ageLabel.text = publicPraiseModel.babyAge;
    
    // 5. 发布时间
    CGSize dateOfSize = [publicPraiseModel.commentTime sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX,CGFLOAT_MAX)];
    self.dateTimeLabel.frame = CGRectMake(self.bounds.size.width - dateOfSize.width - 30, self.ageLabel.frame.origin.y, dateOfSize.width, dateOfSize.height);
    self.dateTimeLabel.text = publicPraiseModel.commentTime;
    
    // 6. 评论
    CGSize evaluationSize = [publicPraiseModel.commentContent sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15)] constrainedToSize:CGSizeMake(CGFLOAT_MAX,CGFLOAT_MAX)];
    self.evaluationLabel.text = publicPraiseModel.commentContent;
    self.evaluationLabel.frame = CGRectMake(mmh_relative_float(11), CGRectGetMaxY(self.headerImageView.frame) + mmh_relative_float(15), evaluationSize.width, evaluationSize.height);
    
  
   
    if (publicPraiseModel.pics){
        // 7. 宝贝图片
        self.imageViewScroll.backgroundColor = [UIColor redColor];
        self.imageViewScroll.frame = CGRectMake(mmh_relative_float(11), CGRectGetMaxY(self.evaluationLabel.frame) + mmh_relative_float(10), self.bounds.size.width - 2 * mmh_relative_float(8), mmh_relative_float(90));
        if (publicPraiseModel.pics.count){
            for (int i = 0 ; i < publicPraiseModel.pics.count;i++){
                MMHImageView *babyImageView = [[MMHImageView alloc]init];
                babyImageView.backgroundColor =[UIColor clearColor];
                babyImageView.frame = CGRectMake(mmh_relative_float(11) + i * (mmh_relative_float(90) + mmh_relative_float(11)), 0, mmh_relative_float(90), mmh_relative_float(90));
                [babyImageView updateViewWithImage:[UIImage imageNamed:[publicPraiseModel.pics objectAtIndex:i]]];
                [self.imageViewScroll addSubview:babyImageView];
                self.imageViewScroll.contentSize = CGSizeMake(mmh_relative_float(11) + (i + 1) * (mmh_relative_float(90) + mmh_relative_float(11)), self.imageViewScroll.bounds.size.height);
            }
        }
    }else{
        [self.imageViewScroll removeFromSuperview];
    }
   
  
    self.starBackgroundView.frame = CGRectMake(CGRectGetMaxX(self.dateTimeLabel.frame) - publicPraiseModel.star * MMHFloat(11),
                                               CGRectGetMinY(self.dateTimeLabel.frame) - MMHFloat(11), publicPraiseModel.star * MMHFloat(11), MMHFloat(11));
    
    
}
@end
