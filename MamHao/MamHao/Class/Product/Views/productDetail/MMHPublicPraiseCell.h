//
//  MMHPublicPraiseCell.h
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHPublicPraiseModel.h"



@interface MMHPublicPraiseCell : UITableViewCell

@property (nonatomic,strong)UIImageView *headerImageView;               // 头像
@property (nonatomic,strong)UILabel *userNickLabel;                     // 用户昵称
@property (nonatomic,strong)UILabel *ageLabel;                          // 宝宝年龄
@property (nonatomic,strong)UIView *starBackgroundView;                 // 星星imageView
@property (nonatomic,strong)UILabel *dateTimeLabel;                     // 发布时间
@property (nonatomic,strong)UILabel *evaluationLabel;                   // 评价label
@property (nonatomic,strong)UIScrollView *imageViewScroll;              // 宝贝图片

// model
@property (nonatomic,strong)MMHPublicPraiseModel *publicPraiseModel;

@end
