//
//  MMHProductDetailStoreCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【商品详情 - 门店】
#import <UIKit/UIKit.h>
#import "MMHProductDetailShopInfoModel.h"
@interface MMHProductDetailStoreCell : UITableViewCell

@property (nonatomic,strong)MMHProductDetailShopInfoModel *shopInfo;
+(CGFloat)cellHeightWithShopInfo:(MMHProductDetailShopInfoModel *)shopInfoModel;
@end
