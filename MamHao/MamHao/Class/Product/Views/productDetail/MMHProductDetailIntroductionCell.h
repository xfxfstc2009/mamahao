//
//  MMHProductDetailIntroductionCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【商品详情介绍】
#import <UIKit/UIKit.h>
#import "MMHProductDetailIntroductionModel.h"
#define kMMHProductDetailDec @"线上线下同品，若您担心质量问题，您可以去该专卖店自提验货"
@interface MMHProductDetailIntroductionCell : UITableViewCell

@property (nonatomic,strong)MMHProductDetailIntroductionModel * productDetailIntroductionModel;


// 计算高度
+(CGFloat)contentOfheight:(MMHProductDetailIntroductionModel *)productDetailIntroductionModel;
@end
