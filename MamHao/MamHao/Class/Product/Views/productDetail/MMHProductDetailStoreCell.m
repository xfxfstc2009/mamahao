//
//  MMHProductDetailStoreCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailStoreCell.h"
#import "MMHDistanceWithLocation.h"                 // 计算距离

@interface MMHProductDetailStoreCell()
@property (nonatomic,strong) MMHImageView *shopImageView;
@property (nonatomic,strong) UILabel *shopNameLabel;
@property (nonatomic,strong) UILabel *shopDescLabel;
@end

@implementation MMHProductDetailStoreCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    self.shopImageView = [[MMHImageView alloc]init];
    self.shopImageView.backgroundColor = [UIColor clearColor];
    self.shopImageView.frame = CGRectMake(MMHFloat(11), MMHFloat(10), MMHFloat(50), MMHFloat(50));
    [self addSubview:self.shopImageView];
    
    // 创建门店名称
    self.shopNameLabel = [[UILabel alloc]init];
    self.shopNameLabel.backgroundColor = [UIColor clearColor];
    self.shopNameLabel.font = F5;
    self.shopNameLabel.textColor = C6;
    self.shopNameLabel.textAlignment = NSTextAlignmentLeft;
    self.shopNameLabel.numberOfLines = 0;
    [self addSubview:self.shopNameLabel];
    
    // 创建门店内容
    self.shopDescLabel = [[UILabel alloc]init];
    self.shopDescLabel.backgroundColor = [UIColor clearColor];
    self.shopDescLabel.font = F2;
    self.shopDescLabel.textColor = C4;
    self.shopDescLabel.textAlignment = NSTextAlignmentLeft;
    self.shopDescLabel.numberOfLines = 1;
    [self addSubview:self.shopDescLabel];
}


-(void)setShopInfo:(MMHProductDetailShopInfoModel *)shopInfo{
    _shopInfo = shopInfo;
    // 1. 图片
    [self.shopImageView updateViewWithImageAtURL:[NSString stringWithFormat:@"http://bgo.oss-cn-hangzhou.aliyuncs.com/%@",shopInfo.shopPic]];
    // 2. 商品名称
    self.shopNameLabel.text = shopInfo.shopName;
    CGSize shopNameSize = [shopInfo.shopName sizeWithCalcFont:self.shopNameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(70) - MMHFloat(20), CGFLOAT_MAX)];
    self.shopNameLabel.frame = CGRectMake(CGRectGetMaxX(self.shopImageView.frame) + MMHFloat(10), MMHFloat(17), kScreenBounds.size.width - MMHFloat(70) - MMHFloat(20), shopNameSize.height);
    
    // 3. 商店距离
    CGFloat lng = [[shopInfo.gps objectAtIndex:1] floatValue];
    CGFloat lat = [[shopInfo.gps objectAtIndex:0] floatValue];

    
    CLLocation *currentLocation = [[CLLocation alloc]initWithLatitude:lat longitude:lng];
    CLLocation *targetLocation = [[CLLocation alloc]initWithLatitude:[MMHCurrentLocationModel sharedLocation].lat longitude:[MMHCurrentLocationModel sharedLocation].lng];
    
    self.shopDescLabel.frame = CGRectMake(self.shopNameLabel.orgin_x, CGRectGetMaxY(self.shopNameLabel.frame) + MMHFloat(9), self.shopNameLabel.size_width, [MMHTool contentofHeight:F2]);
    NSString *distance = [NSString stringWithFormat:@"距离我%.2fkm",(CGFloat)[currentLocation distanceFromLocation:targetLocation] / 1000.];
    if (distance.length){
        self.shopDescLabel.text = distance;
    } else {
        self.shopDescLabel.text = @"获取不到当前距离";
    }
    
    CGFloat cellHeight = MMHFloat(10);
    cellHeight += shopNameSize.height;
    cellHeight += MMHFloat(9);
    cellHeight += [MMHTool contentofHeight:F2];
    cellHeight += MMHFloat(10);
    cellHeight += MMHFloat(10);
    
    self.shopImageView.orgin_y = (cellHeight - MMHFloat(50)) / 2.;
}

+(CGFloat)cellHeightWithShopInfo:(MMHProductDetailShopInfoModel *)shopInfoModel{
    CGFloat cellHeight = MMHFloat(10);
    
     CGSize shopNameSize = [shopInfoModel.shopName sizeWithCalcFont:F5 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(70) - MMHFloat(20), CGFLOAT_MAX)];
    cellHeight += shopNameSize.height;
    cellHeight += MMHFloat(9);
    cellHeight += [MMHTool contentofHeight:F2];
    cellHeight += MMHFloat(10);
    cellHeight += MMHFloat(10);
    return cellHeight;
}

@end
