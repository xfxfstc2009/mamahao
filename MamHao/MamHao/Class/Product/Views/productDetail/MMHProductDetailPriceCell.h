//
//  MMHProductDetailPriceCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMHProductDetailPriceModel.h"          // 商品价格model
#import "MMHProductDetailViewController1.h"     // 商品详情
#import "MMHProductDetailModel.h"
@class MMHProductDetailViewController1;

@interface MMHProductDetailPriceCell : UITableViewCell

@property (nonatomic,strong)MMHProductDetailPriceModel *transferProductDetailPriceModel;            /**< 价格栏目model*/
@property (nonatomic,assign)MMHProductDetailSaleType   transferProductDetailSaleType;              /**< 传递过来的商品类型*/
@property (nonatomic,strong)MMHProductDetailModel *productDetailModel;

+(CGFloat)cellHeight:(MMHProductDetailPriceModel *)productDetailPriceModel type:(MMHProductDetailSaleType)productDetailSaleType;

@property (nonatomic,strong)UILabel *timeLabel;                 /**< 倒计时label*/
@property (nonatomic,strong)UILabel *amountLabel;               /**< 剩余数量*/
@property (nonatomic,strong)UIImageView *timeImageView;         /**< 倒计时*/

-(void)settingDisplayTimeRemainingWithTime:(NSString *)displayTimeRemaining SurplusCount:(NSInteger)surplusCount;
@end
