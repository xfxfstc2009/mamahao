//
//  MMHGuessYouLikeScrollView.h
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 猜你喜欢

#import <UIKit/UIKit.h>
#import "MMHGuessYouLikeSingleModel.h"

typedef void(^guessYouLikeWithScrollBlock)(MMHGuessYouLikeSingleModel *guessYouLikeSingleModel);

@interface MMHGuessYouLikeScrollView : UIScrollView

@property (nonatomic,strong)NSArray *guessYouLikeArray;
@property (nonatomic,copy)guessYouLikeWithScrollBlock guessYouLikeBlock;

@end
