//
//  MMHProductDetailIntroductionCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailIntroductionCell.h"

@interface MMHProductDetailIntroductionCell()
@property (nonatomic,strong)UILabel *channelLabel;              /**< 自营实体店*/
@property (nonatomic,strong)UILabel *productNameLabel;          /**< 商品名称*/
@property (nonatomic,strong)UILabel *subTitleLabel;             /**< 副标题*/

@end

@implementation MMHProductDetailIntroductionCell


-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 0.创建商品名称
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.frame = CGRectMake(MMHFloat(11), MMHFloat(18), kScreenBounds.size.width - 2 * MMHFloat(11), 20);
    self.productNameLabel.textAlignment = NSTextAlignmentLeft;
    self.productNameLabel.textColor = C7;
    self.productNameLabel.font = [F6 boldFont] ;
    [self addSubview:self.productNameLabel];
    
    // 创建商品副标题
    self.subTitleLabel = [[UILabel alloc]init];
    self.subTitleLabel.backgroundColor = [UIColor clearColor];
    self.subTitleLabel.font = F4;
    self.subTitleLabel.textColor = C4;
    self.subTitleLabel.numberOfLines = 0;
    [self addSubview:self.subTitleLabel];
}

-(void)setProductDetailIntroductionModel:(MMHProductDetailIntroductionModel *)productDetailIntroductionModel{
    _productDetailIntroductionModel = productDetailIntroductionModel;
    // 1. 商品名称
    if (productDetailIntroductionModel.title.length){
        NSString *flagString = productDetailIntroductionModel.shopId.length?@" 实体店 ":@" 自营 ";
        NSString *productNameString = [NSString stringWithFormat:@"%@ %@",flagString,productDetailIntroductionModel.title];
        CGSize productNameContentSize = [productNameString sizeWithCalcFont:[F6 boldFont] constrainedToSize:CGSizeMake(self.productNameLabel.size_width, CGFLOAT_MAX)];
        self.productNameLabel.numberOfLines = 0;
        self.productNameLabel.size_height = productNameContentSize.height;
        // 2. 从尾部计算
       self.productNameLabel.attributedText = [self attributedSetting:productNameString flagString:flagString];
    }
    
    // 3. 商品副标题
    NSString *subTitleString = productDetailIntroductionModel.subtitle.length?productDetailIntroductionModel.subtitle:kMMHProductDetailDec;
    self.subTitleLabel.text = subTitleString;
    CGSize subTitleContentSize = [self.subTitleLabel.text sizeWithCalcFont:self.subTitleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
    self.subTitleLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.productNameLabel.frame) + MMHFloat(10), kScreenBounds.size.width - 2 * MMHFloat(11), subTitleContentSize.height);
}

#pragma mark 计算高度
+(CGFloat)contentOfheight:(MMHProductDetailIntroductionModel *)productDetailIntroductionModel{
    CGFloat cellHeight = 0;
    cellHeight += MMHFloat(18);
    
    NSString *channelString = productDetailIntroductionModel.shopId.length?@" 实体店 ":@" 自营 ";
     NSString *productNameString = [NSString stringWithFormat:@"%@ %@",channelString,productDetailIntroductionModel.title];
     CGSize productNameContentSize = [productNameString sizeWithCalcFont:[F6 boldFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
    cellHeight += productNameContentSize.height;
    
    cellHeight += MMHFloat(12);

    // 副标题
    NSString *subTitleString = productDetailIntroductionModel.subtitle.length?productDetailIntroductionModel.subtitle:kMMHProductDetailDec;
    CGSize subTitleStringSize = [subTitleString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * MMHFloat(11), CGFLOAT_MAX)];
    cellHeight += subTitleStringSize.height;
    cellHeight += MMHFloat(10);
    cellHeight += MMHFloat(18);
    return cellHeight;
}


-(NSMutableAttributedString *)attributedSetting:(NSString *)titleString flagString:(NSString *)flagString{
    NSMutableAttributedString *tempMutableAttributedString;
    tempMutableAttributedString = [[NSMutableAttributedString alloc]initWithString:titleString];
    
    // 从尾部计算
    NSRange flagRange = [titleString rangeOfString:flagString];
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:F1 range:flagRange];
    [tempMutableAttributedString addAttribute:NSBackgroundColorAttributeName value:[UIColor colorWithCustomerName:@"红"] range:flagRange];
    [tempMutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithCustomerName:@"白"] range:flagRange];

    NSRange normalRange;
    normalRange.location = flagRange.length;
    normalRange.length = titleString.length - flagRange.length;
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:[F6 boldFont] range:normalRange];
    
    return tempMutableAttributedString;
}

@end
