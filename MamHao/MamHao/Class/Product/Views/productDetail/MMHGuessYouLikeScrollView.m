//
//  MMHGuessYouLikeScrollView.m
//  MamHao
//
//  Created by SmartMin on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGuessYouLikeScrollView.h"

#import "MMHProductListViewController.h"
@interface MMHGuessYouLikeScrollView()<UIScrollViewDelegate>{

}

@end

@implementation MMHGuessYouLikeScrollView


-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.bounces = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
//        self.pagingEnabled = YES;
        self.delegate = self;
        self.frame = self.bounds;
        self.directionalLockEnabled = YES;//锁定滑动的方向
    } return self;
}

-(void)setGuessYouLikeArray:(NSArray *)guessYouLikeArray{
    if (guessYouLikeArray.count){
        _guessYouLikeArray = guessYouLikeArray;
        for (int i = 0; i< guessYouLikeArray.count;i++){
            [self createItemWithIndex:i];
        }
    }
    
    // 2. 设置contentOfSize
    self.contentSize = CGSizeMake( (guessYouLikeArray.count) * MMHFloat(135 + 10), 0);
}


-(void)createItemWithIndex:(NSInteger)index {
    // 0. 获取当前的model
    MMHGuessYouLikeSingleModel *guessYouLikeSinglModel = [self.guessYouLikeArray objectAtIndex:index];
    
    // 1. 创建view
    UIView *backgroundView = [[UIView alloc]init];
    backgroundView.backgroundColor = [UIColor whiteColor];
    backgroundView.frame = CGRectMake(index * MMHFloat(135 + 10), 0, MMHFloat(145), MMHFloat(180));
    [self addSubview:backgroundView];
    
    // 2. 加载图片
    MMHImageView *productImageView = [[MMHImageView alloc]init];
    productImageView.backgroundColor = [UIColor clearColor];
    productImageView.frame = CGRectMake((MMHFloat(145) - MMHFloat(115))/ 2., MMHFloat(10), MMHFloat(115), MMHFloat(115));
    [productImageView updateViewWithImageAtURL:guessYouLikeSinglModel.pic];
    [backgroundView addSubview:productImageView];

    // 3. 添加商品详细
    UILabel *productNameLabel = [[UILabel alloc]init];
    productNameLabel.backgroundColor = [UIColor clearColor];
    productNameLabel.text = guessYouLikeSinglModel.itemName;
    productNameLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    productNameLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
    [backgroundView addSubview:productNameLabel];
    
    // 计算宽度
    CGSize productNameSize = [productNameLabel.text sizeWithCalcFont:productNameLabel.font constrainedToSize:CGSizeMake(MMHFloat(145) - 2 * MMHFloat(10), CGFLOAT_MAX)];
    if (productNameSize.height > [MMHTool contentofHeight:productNameLabel.font]){          // 2 行
        productNameLabel.numberOfLines = 2;
        productNameLabel.frame = CGRectMake(MMHFloat(10), CGRectGetMaxY(productImageView.frame) + MMHFloat(10), MMHFloat(145) - 2 * MMHFloat(10), 2 * [MMHTool contentofHeight:productNameLabel.font]);
    } else {
        productNameLabel.numberOfLines = 1;
        productNameLabel.frame = CGRectMake(MMHFloat(10), CGRectGetMaxY(productImageView.frame) + MMHFloat(10), MMHFloat(145) - 2 * MMHFloat(10),  [MMHTool contentofHeight:productNameLabel.font]);
    }
    
    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(backgroundView.bounds.size.width - .5f, 0, .5f, backgroundView.bounds.size.height);
    [backgroundView addSubview:lineView];
    
    // 4. 创建按钮
    UIButton *clickButton = [UIButton buttonWithType:UIButtonTypeCustom];
    clickButton.backgroundColor = [UIColor clearColor];
    clickButton.frame = backgroundView.bounds;
    clickButton.tag = index;
    [clickButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [backgroundView addSubview:clickButton];
    
}

-(void)buttonClick:(UIButton *)sender{
    UIButton *clickButton = (UIButton *)sender;
    MMHGuessYouLikeSingleModel *guessYouLikeSinglModel = [self.guessYouLikeArray objectAtIndex:clickButton.tag];
    self.guessYouLikeBlock(guessYouLikeSinglModel);
}

@end
