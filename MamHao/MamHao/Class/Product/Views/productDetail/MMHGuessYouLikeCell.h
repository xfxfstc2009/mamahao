//
//  MMHGuessYouLikeCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 【猜你喜欢】
#import <UIKit/UIKit.h>
#import "MMHGuessYouLikeListModel.h"

typedef void(^guessYouLikeBlock)(MMHGuessYouLikeSingleModel *guessYouLikeSingleModel);

@interface MMHGuessYouLikeCell : UITableViewCell

@property (nonatomic,strong)MMHGuessYouLikeListModel *guessYouLikeListModel;    /**< 猜你喜欢model */
@property (nonatomic,copy)guessYouLikeBlock guessYouLikeBlock;

+(CGFloat)contentOfHeight;

@end
