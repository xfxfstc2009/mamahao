//
//  MMHProductDetailPriceCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailPriceCell.h"
#import "MMHDeleteLineLabel.h"
#import "MMHAccountSession.h"
#import "MMHNetworkAdapter+Center.h"
@interface MMHProductDetailPriceCell()
@property (nonatomic,strong)UILabel *priceLabel;                /**< 现价*/
@property (nonatomic,strong)MMHDeleteLineLabel *oldpriceLabel;  /**< 原价*/
@property (nonatomic,strong)UILabel *mBeanLabel;                /**< 妈豆文案*/
@property (nonatomic,strong)UIButton *collectionButton;         /**< 收藏按钮*/
@property (nonatomic,strong)UIButton *howUseButton;             /**< 妈豆有什么用*/
@property (nonatomic,strong)UILabel *mBeanPriceLabel;           /**< 妈豆label*/
@property (nonatomic,assign)CGFloat priceWidth;                 /**< 文字长度*/

@end

@implementation MMHProductDetailPriceCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"粉"];
    self.priceLabel.font = [UIFont systemFontOfCustomeSize:25];
    self.priceLabel.frame = CGRectMake(MMHFloat(10), MMHFloat(13), 100, [MMHTool contentofHeight:self.priceLabel.font]);
    [self addSubview:self.priceLabel];
    
    // 2. 创建老价格
    self.oldpriceLabel = [[MMHDeleteLineLabel alloc]init];
    self.oldpriceLabel.backgroundColor = [UIColor clearColor];
    self.oldpriceLabel.textColor = C4;
    self.oldpriceLabel.font = F2;
    self.oldpriceLabel.strikeThroughEnabled = YES;
    self.oldpriceLabel.strikeThroughColor = C4;
    [self addSubview:self.oldpriceLabel];
    
    // 3. 创建妈豆
    self.mBeanLabel = [[UILabel alloc]init];
    self.mBeanLabel.backgroundColor = [UIColor clearColor];
    self.mBeanLabel.textAlignment = NSTextAlignmentLeft;
    self.mBeanLabel.font = F3;
    self.mBeanLabel.textColor = C5;
    [self addSubview:self.mBeanLabel];
    
    // 4. 创建收藏
    self.collectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.collectionButton addTarget:self action:@selector(collectionButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.collectionButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_nor"] forState:UIControlStateNormal];
    self.collectionButton.adjustsImageWhenHighlighted = NO;
    [self.collectionButton setTitle:@"收藏" forState:UIControlStateNormal];
    self.collectionButton.titleLabel.font = F1;
    [self.collectionButton setTitleColor:C5 forState:UIControlStateNormal];
    self.collectionButton.backgroundColor = [UIColor clearColor];
    self.collectionButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(17) -  MMHFloat(70), MMHFloat(2), MMHFloat(70), MMHFloat(60));
    [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
    [self addSubview:self.collectionButton];
    
    // 5. 创建妈豆有什么用
    self.howUseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.howUseButton.stringTag = @"howUseButton";
    self.howUseButton.backgroundColor = [UIColor clearColor];
    [self.howUseButton setTitle:@"妈豆有什么用?" forState:UIControlStateNormal];
    self.howUseButton.titleLabel.font = F3;
    CGSize contentOfSize = [@"妈豆有什么用？" sizeWithCalcFont:F3 constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:F3])];
    [self.howUseButton setTitleColor:C22 forState:UIControlStateNormal];
    self.howUseButton.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10) - contentOfSize.width, MMHFloat(80) - MMHFloat(8)- [MMHTool contentofHeight:F3], contentOfSize.width, [MMHTool contentofHeight:F3]);
    [self addSubview:self.howUseButton];
    
    //////// 妈豆////////
    self.mBeanPriceLabel = [[UILabel alloc]init];
    self.mBeanPriceLabel.backgroundColor = [UIColor clearColor];
    self.mBeanPriceLabel.textColor = C21;
    self.mBeanPriceLabel.font = [UIFont systemFontOfCustomeSize:25];
    self.mBeanPriceLabel.frame = CGRectMake(MMHFloat(10), MMHFloat(13), 100, [MMHTool contentofHeight:self.mBeanPriceLabel.font]);
    [self addSubview:self.mBeanPriceLabel];
    
    //创建时间图片
    self.timeImageView = [[UIImageView alloc]init];
    self.timeImageView.backgroundColor = [UIColor clearColor];
    self.timeImageView.image = [UIImage imageNamed:@"orderlist_icon_time"];
    self.timeImageView.frame = CGRectMake(self.mBeanPriceLabel.orgin_x, CGRectGetMaxY(self.mBeanPriceLabel.frame) + MMHFloat(15), MMHFloat(15), MMHFloat(15));
    [self addSubview:self.timeImageView];
    
    // 创建倒计时
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = F3;
    self.timeLabel.textColor = C5;
    [self addSubview:self.timeLabel];
    
    // 创建剩余
    self.amountLabel = [[UILabel alloc]init];
    self.amountLabel.backgroundColor = [UIColor clearColor];
    self.amountLabel.font = F3;
    self.amountLabel.textColor = C4;
    [self addSubview:self.amountLabel];
}

-(void)setTransferProductDetailSaleType:(MMHProductDetailSaleType)transferProductDetailSaleType{
    _transferProductDetailSaleType = transferProductDetailSaleType;
}

-(void)setProductDetailModel:(MMHProductDetailModel *)productDetailModel{
    _productDetailModel = productDetailModel;
    if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
        //没登录
        NSString *filePath = [self favoritefilePath];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
            NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:filePath];
            for (int i = 0; i<array.count; i++) {
                NSDictionary *dic = array[i];
                NSString *favId = dic[@"favId"];
                if ([productDetailModel.templateId isEqualToString:favId]) {
                    NSInteger status = [dic[@"status"] integerValue];
                    if (status == 0) {
                        self.productDetailModel.isCollect = NO;
                    }else{
                        self.productDetailModel.isCollect = YES;
                    }
                }
            }
        }
    }

    if (self.productDetailModel.isCollect){
        [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
        [self.collectionButton setTitle:@"已收藏" forState:UIControlStateNormal];
        [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
    } else {
        [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_nor"] forState:UIControlStateNormal];
        [self.collectionButton setTitle:@"收藏" forState:UIControlStateNormal];
        [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
    }
}

-(void)setTransferProductDetailPriceModel:(MMHProductDetailPriceModel *)transferProductDetailPriceModel{
    _transferProductDetailPriceModel = transferProductDetailPriceModel;
    if (self.transferProductDetailSaleType == MMHProductDetailSaleTypeMBean){                               // 【妈豆商品】
        // 1. 隐藏
        self.priceLabel.hidden = YES;
        self.collectionButton.hidden = YES;
        self.howUseButton.hidden = YES;
        // 2. 计算文字宽度
        NSString *mbeanString = @"";
        if (transferProductDetailPriceModel.mBeanPay && transferProductDetailPriceModel.price){
            mbeanString = [NSString stringWithFormat:@"%li妈豆 + ￥%.2f",(long)transferProductDetailPriceModel.mBeanPay,transferProductDetailPriceModel.price];
        } else {
            mbeanString =[NSString stringWithFormat:@"%li妈豆",(long)transferProductDetailPriceModel.mBeanPay];
        }
        self.mBeanPriceLabel.attributedText = [self mBeanpriceFontSetting:mbeanString];
        
        // 老价格
        self.oldpriceLabel.text = [NSString stringWithFormat:@"￥%.2f",transferProductDetailPriceModel.originalPrice];
        CGSize oldPriceSize = [self.oldpriceLabel.text sizeWithCalcFont:self.oldpriceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.oldpriceLabel.font])];
        self.oldpriceLabel.frame = CGRectMake(CGRectGetMaxX(self.mBeanPriceLabel.frame) + MMHFloat(8), self.mBeanPriceLabel.orgin_y + self.mBeanPriceLabel.size_height - [MMHTool contentofHeight:self.oldpriceLabel.font], oldPriceSize.width, [MMHTool contentofHeight:self.oldpriceLabel.font]);
        
        // 时间
        self.timeImageView.frame = CGRectMake(self.mBeanPriceLabel.orgin_x, CGRectGetMaxY(self.mBeanPriceLabel.frame) + MMHFloat(15), MMHFloat(15), MMHFloat(15));
        
        // 倒计时
        CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.timeLabel.font])];
        self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.timeImageView.frame) + MMHFloat(5), self.timeImageView.orgin_y, timeSize.width, [MMHTool contentofHeight:self.timeLabel.font]);
        
        
        
    } else if (self.transferProductDetailSaleType == MMHProductDetailSaleTypeRMB){                          // 【人民币商品】
        self.mBeanPriceLabel.hidden = YES;
        self.timeLabel.hidden = YES;
        self.timeImageView.hidden = YES;
        self.amountLabel.hidden = YES;
        
    
        if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
            //没登录
            NSString *filePath = [self favoritefilePath];
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
                NSMutableArray *array = [NSMutableArray arrayWithContentsOfFile:filePath];
                for (int i = 0; i<array.count; i++) {
                    NSDictionary *dic = array[i];
                    NSString *favId = dic[@"favId"];
                    if ([transferProductDetailPriceModel.templateId isEqualToString:favId]) {
                        NSInteger status = [dic[@"status"] integerValue];
                        if (status == 0) {
                            self.productDetailModel.isCollect = NO;
                        }else{
                            self.productDetailModel.isCollect = YES;
                        }
                    }
                }
            }
        }
        if (self.productDetailModel.isCollect){
            [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
            [self.collectionButton setTitle:@"已收藏" forState:UIControlStateNormal];
            [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
        } else {
            [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_nor"] forState:UIControlStateNormal];
            [self.collectionButton setTitle:@"收藏" forState:UIControlStateNormal];
            [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
        }
    
        if (transferProductDetailPriceModel.itemId){
            [self searchDataIsHasZan:transferProductDetailPriceModel.itemId];
        }
        
        // 商品价格
        NSString *priceLabelString = [NSString stringWithFormat:@"￥%.2f",transferProductDetailPriceModel.price];
        self.priceLabel.attributedText = [self priceFontSetting:priceLabelString];
        
        // 老价格
        self.oldpriceLabel.text = [NSString stringWithFormat:@"￥%.2f",transferProductDetailPriceModel.originalPrice];
        if (transferProductDetailPriceModel.price == transferProductDetailPriceModel.originalPrice){
            self.oldpriceLabel.hidden = YES;
        } else{
            self.oldpriceLabel.hidden = NO;
        }
        
        CGSize oldPriceSize = [self.oldpriceLabel.text sizeWithCalcFont:self.oldpriceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.oldpriceLabel.font])];
        self.oldpriceLabel.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame) + MMHFloat(8), self.priceLabel.orgin_y + self.priceLabel.size_height - [MMHTool contentofHeight:self.oldpriceLabel.font], oldPriceSize.width, [MMHTool contentofHeight:self.oldpriceLabel.font]);
        
        // 妈豆
        self.mBeanLabel.text = [NSString stringWithFormat:@"购买后可获得%@个妈豆",transferProductDetailPriceModel.getmBean];
        CGSize mBeanLabelSize = [self.mBeanLabel.text sizeWithCalcFont:self.mBeanLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.mBeanLabel.font])];
        self.mBeanLabel.frame = CGRectMake(self.priceLabel.orgin_x, MMHFloat(80) - MMHFloat(8) - [MMHTool contentofHeight:self.mBeanLabel.font],mBeanLabelSize.width, [MMHTool contentofHeight:self.mBeanLabel.font]);
        
        self.mBeanLabel.attributedText = [MMHTool rangeLabelWithContent:self.mBeanLabel.text hltContentArr:@[[NSString stringWithFormat:@"%@",transferProductDetailPriceModel.getmBean]] hltColor:C21 normolColor:C5];
    }
    self.oldpriceLabel.hidden = YES;
}

-(NSString *)favoritefilePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *doucumentsDirectory = paths.lastObject;
    NSString *filePath = [doucumentsDirectory stringByAppendingPathComponent:@"collect.plist"];
    return filePath;
}
#pragma mark actionClick
-(void)collectionButtonClick{

    if (!self.productDetailModel.isCollect){
        self.productDetailModel.isCollect= NO;
        [self animationWithCollectionWithButton:self.collectionButton isCollection:self.productDetailModel.isCollect];
        if (self.transferProductDetailPriceModel.templateId){
            [self clickZanWithDataWithProductId:self.transferProductDetailPriceModel.templateId andisCollection:self.productDetailModel.isCollect];
        }
    } else {
        [self showTips:@"请到我的收藏进行取消收藏"];
    }
}


#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIButton *)collectionButton isCollection:(BOOL)isCollection{
    if (isCollection){
        [collectionButton setImage:[UIImage imageNamed:@"product_collection_nor"] forState:UIControlStateNormal];
    } else {
        [collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;

    self.productDetailModel.isCollect = YES;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}


-(NSMutableAttributedString *)priceFontSetting:(NSString *)priceString{
    NSMutableAttributedString *tempMutableAttributedString;
    tempMutableAttributedString = [[NSMutableAttributedString alloc]initWithString:priceString];
    NSRange yangRange = [priceString rangeOfString:@"￥"];
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:20] range:yangRange];
    
    CGSize size1 = [@"￥" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:20] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:20]])];
    
    // 查找.前面的文字
    NSArray *priceArr = [priceString componentsSeparatedByString:@"."];
    NSString *firstNum = [[priceArr objectAtIndex:0] substringFromIndex:1];
    NSRange firstNumRange = [priceString rangeOfString:firstNum];
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:25] range:firstNumRange];
    CGSize size2 = [firstNum sizeWithCalcFont:[UIFont systemFontOfCustomeSize:25] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:25]])];
    
    NSString *lastNum = [priceArr lastObject];
    NSRange laseNumRange = [priceString rangeOfString:lastNum options:NSBackwardsSearch];
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:18] range:laseNumRange];
    CGSize size3 = [lastNum sizeWithCalcFont:[UIFont systemFontOfCustomeSize:18] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:18]])];
    
    self.priceLabel.size_width = size1.width + size3.width + size2.width + MMHFloat(10);
    
    return tempMutableAttributedString;
}

-(NSMutableAttributedString *)mBeanpriceFontSetting:(NSString *)priceString{
    NSMutableAttributedString *tempMutableAttributedString;
    tempMutableAttributedString = [[NSMutableAttributedString alloc]initWithString:priceString];

    NSArray *priceArr = [priceString componentsSeparatedByString:@"妈豆"];
    NSString *firstNum = [priceArr objectAtIndex:0];
    NSRange firstNumRange = [priceString rangeOfString:firstNum];
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:25] range:firstNumRange];
    
    
    NSRange lastRange;
    lastRange.location = firstNumRange.length;
    lastRange.length = priceString.length - firstNumRange.length;
    [tempMutableAttributedString addAttribute:NSFontAttributeName value:F5 range:lastRange];

    self.priceWidth = [[priceString substringWithRange:lastRange] sizeWithCalcFont:F5 constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:25]])].width + [firstNum sizeWithCalcFont:[UIFont systemFontOfCustomeSize:25] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:25]])].width;
    
    self.mBeanPriceLabel.frame = CGRectMake(MMHFloat(10), MMHFloat(23), self.priceWidth, [MMHTool contentofHeight:self.mBeanPriceLabel.font]);
    return tempMutableAttributedString;
}


+(CGFloat)cellHeight:(MMHProductDetailPriceModel *)productDetailPriceModel type:(MMHProductDetailSaleType)productDetailSaleType{
    if (productDetailSaleType == MMHProductDetailSaleTypeRMB){
        return MMHFloat(80);
    } else {
        CGFloat cellHeight = 0;
        cellHeight += MMHFloat(23);
        cellHeight +=[MMHTool contentofHeight:[UIFont systemFontOfCustomeSize:25]];
        cellHeight += MMHFloat(15);
        cellHeight += [MMHTool contentofHeight:F3];
        cellHeight += MMHFloat(8);
        return cellHeight;
    }
}


#pragma mark 点赞
#pragma mark 点赞逻辑
-(void)clickZanWithDataWithProductId:(NSString *)productId andisCollection:(BOOL)isCollection{
        [[MMHNetworkAdapter sharedAdapter] addCollectWithType:1 collectItemId:productId from:nil succeededHandler:^{
            [self animationWithCollectionWithButton:self.collectionButton isCollection:self.transferProductDetailPriceModel.isCollect];
            [self.collectionButton setTitle:@"已收藏" forState:UIControlStateNormal];
            [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
            self.transferProductDetailPriceModel.isCollect = YES;
        } failedHandler:^(NSError *error) {
            
        }];
        
}

-(void)searchDataIsHasZan:(NSString *)productId{
    NSString *path = [self favoritefilePath];
    NSMutableArray *zanArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
    for (int i = 0 ; i < zanArray.count ; i++){
        NSDictionary *dic = zanArray[i];
        if ([dic[@"favId"] isEqualToString:productId]){
            [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
            [self.collectionButton setTitle:@"已收藏" forState:UIControlStateNormal];
            [self.collectionButton makeVerticalWithPadding:MMHFloat(4)];
            self.productDetailModel.isCollect = YES;
        }
    }
}


#pragma mark 设置妈豆商品出售，倒计时和剩余数量
-(void)settingDisplayTimeRemainingWithTime:(NSString *)displayTimeRemaining SurplusCount:(NSInteger)surplusCount{
    // 倒计时
    self.timeLabel.text = displayTimeRemaining;
    CGSize timeSize = [displayTimeRemaining sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.timeImageView.frame) + MMHFloat(5), self.timeImageView.orgin_y, timeSize.width, [MMHTool contentofHeight:self.timeLabel.font]);
    
    // 剩余
    NSString *amountString = [NSString stringWithFormat:@"剩余%li份",(long)surplusCount];
    self.amountLabel.text = amountString;
    CGSize amountSize = [self.amountLabel.text sizeWithCalcFont:self.amountLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:self.amountLabel.font])];
    self.amountLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + MMHFloat(10), self.timeLabel.orgin_y, amountSize.width, [MMHTool contentofHeight:self.amountLabel.font]);
}

@end
