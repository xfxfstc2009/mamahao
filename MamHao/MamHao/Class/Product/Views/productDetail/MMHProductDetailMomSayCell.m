//
//  MMHProductDetailMomSayCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailMomSayCell.h"

@interface MMHProductDetailMomSayCell()
@property (nonatomic,strong)MMHImageView *headerImageView;
@property (nonatomic,strong)UILabel *mamNameLabel;
@property (nonatomic,strong)UILabel *mamSayLabel;
@property (nonatomic,strong)UIButton *headButton;

@end

@implementation MMHProductDetailMomSayCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    // 创建头像
    self.headerImageView = [[MMHImageView alloc] init];
    self.headerImageView.urlPrefixString = @"http://type-images.oss-cn-hangzhou.aliyuncs.com/";
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.frame = CGRectMake(MMHFloat(10) + (MMHFloat(82) - MMHFloat(40)) /2., MMHFloat(19), MMHFloat(40), MMHFloat(40));
    [self.headerImageView.layer setMasksToBounds:YES];
    [self.headerImageView.layer setCornerRadius:MMHFloat(19)];
//    self.headerImageView.layer.borderColor = [UIColor whiteColor].CGColor;
//    self.headerImageView.layer.borderWidth = 2;
    [self addSubview:self.headerImageView];
    
    // 创建用户label
    self.mamNameLabel = [[UILabel alloc]init];
    self.mamNameLabel.backgroundColor = [UIColor clearColor];
    self.mamNameLabel.textAlignment = NSTextAlignmentCenter;
    self.mamNameLabel.font = F2;
    self.mamNameLabel.textColor = C6;
    self.mamNameLabel.frame = CGRectMake(MMHFloat(10), CGRectGetMaxY(self.headerImageView.frame),MMHFloat(82), 20);
    [self addSubview:self.mamNameLabel];
    
    // 添加头像按钮
    self.headButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.headButton.frame = CGRectMake(0,0,MMHFloat(82) + MMHFloat(10),self.contentView.bounds.size.height);
    self.headButton.backgroundColor = [UIColor clearColor];
    [self.headButton addTarget:self action:@selector(headButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.headButton];
    
    // 创建妈妈说
    self.mamSayLabel = [[UILabel alloc]init];
    self.mamSayLabel.font = F4;
    self.mamSayLabel.textAlignment = NSTextAlignmentLeft;
    self.mamSayLabel.backgroundColor=[UIColor clearColor];
    [self addSubview:self.mamSayLabel];
    
    // 创建箭头符号
    self.arrowImageView = [[UIImageView alloc]init];
    self.arrowImageView.hidden = YES;
    self.arrowImageView.image = [UIImage imageNamed:@"home_icon_down"];
    [self addSubview:self.arrowImageView];
}

-(void)setMamCareIsOpen:(BOOL)mamCareIsOpen{
    _mamCareIsOpen = mamCareIsOpen;
}

-(void)setMamSayModel:(MMHMamSayModel *)mamSayModel{
    _mamSayModel = mamSayModel;
    
    if (mamSayModel.mamCare.length){
        self.mamSayLabel.attributedText = [MMHTool rangeLabelWithContent:[NSString stringWithFormat:@"好妈妈说: %@",mamSayModel.mamCare] hltContentArr:@[@"好妈妈说: "] hltColor:C21 normolColor:C5];
    } else {
        self.mamSayLabel.text = @"获取不到好妈妈说。";
    }
    CGSize mamCareSize = [[NSString stringWithFormat:@"好妈妈说: %@",mamSayModel.mamCare] sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(15) - CGRectGetMaxX(self.headButton.frame), CGFLOAT_MAX)];

    // 1. 计算文字高度
    NSInteger mamCareNumberOfLine = 0;
    CGFloat mamCareHeight = mamCareSize.height;
    CGFloat mamCareSingleHeight = [MMHTool contentofHeight:F4];
    
    mamCareNumberOfLine = (double)mamCareHeight / mamCareSingleHeight ;

    if (mamCareNumberOfLine > 3){                       // 3行以上
        if (self.mamCareIsOpen){                            // 打开状态
            self.mamSayLabel.numberOfLines = 0;
            self.mamSayLabel.frame = CGRectMake(CGRectGetMaxX(self.headButton.frame), self.headerImageView.orgin_y, kScreenBounds.size.width - MMHFloat(15) - CGRectGetMaxX(self.headButton.frame), mamCareSize.height);
            self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(10), CGRectGetMaxY(self.mamSayLabel.frame), MMHFloat(9), MMHFloat(6));
        } else {
            self.mamSayLabel.numberOfLines = 3;
            self.mamSayLabel.frame = CGRectMake(CGRectGetMaxX(self.headButton.frame), self.headerImageView.orgin_y, kScreenBounds.size.width - MMHFloat(15) - CGRectGetMaxX(self.headButton.frame), 3 * [MMHTool contentofHeight:F4]);
        }
        self.arrowImageView.hidden = NO;
        self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(15) - MMHFloat(9), CGRectGetMaxY(self.mamSayLabel.frame) + MMHFloat(5), MMHFloat(9), MMHFloat(6));
    } else {                                            // 3行以下
        self.mamSayLabel.numberOfLines = 0;
        self.mamSayLabel.frame = CGRectMake(CGRectGetMaxX(self.headButton.frame), self.headerImageView.orgin_y, kScreenBounds.size.width - MMHFloat(10) - CGRectGetMaxX(self.headButton.frame), mamCareSize.height);
    }
    
    // 专家头像
    [self.headerImageView updateViewWithImageAtURL:mamSayModel.specialistPhoto];
    // 专家名称
    self.mamNameLabel.text = mamSayModel.specialistName;
    // 专家 按钮
    self.headButton.size_height = MMHFloat(19) + MMHFloat(18) + self.mamSayLabel.size_height;
}



+ (CGFloat)contentOfHeightWithMamSayModel:(MMHMamSayModel *)mamSayModel andMamCareIsOpen:(BOOL)mamCareIsOpen{
    CGFloat cellHeight = 0;
    
    CGSize mamCareSize = [[NSString stringWithFormat:@"好妈妈说: %@",mamSayModel.mamCare] sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(kScreenBounds.size.width -  MMHFloat(15) - MMHFloat(92), CGFLOAT_MAX)];

    NSInteger mamCareNumberOfLine = 0;
    CGFloat mamCareHeight = mamCareSize.height;
    CGFloat mamCareSingleHeight = [MMHTool contentofHeight:F4];
    
    mamCareNumberOfLine = (double)mamCareHeight / mamCareSingleHeight ;
    
    if (mamCareNumberOfLine > 3){                       // 3行以上
        if (mamCareIsOpen){                            // 打开状态
            cellHeight+=mamCareSize.height;
        } else {
            cellHeight += 3 * [MMHTool contentofHeight:F4];
        }
        cellHeight += MMHFloat(19);
        cellHeight += MMHFloat(18);
    } else {                                            // 3行以下
        cellHeight += MMHFloat(19) + MMHFloat(80);
    }

    return cellHeight;
}

#pragma mark 旋转动画
-(void)rotationWithView:(UIView *)view andtrans:(CGFloat)transForm{
    CGAffineTransform rotationTrans = CGAffineTransformMakeRotation(transForm);
    view.transform = rotationTrans;
}


#pragma mark -actionClick
-(void)headButtonClick{
    NSLog(@"点击了头像");
}
@end
