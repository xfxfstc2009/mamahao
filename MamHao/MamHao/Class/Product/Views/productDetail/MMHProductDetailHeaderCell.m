//
//  MMHProductDetailHeaderCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailHeaderCell.h"

@interface MMHProductDetailHeaderCell()<UIScrollViewDelegate>
@property (nonatomic,strong)UIScrollView *productImageViewScrollView;
@property (nonatomic,strong)UILabel *dynamicPageLabel;
@property (nonatomic,strong)UILabel *fixedPageLabel;
@property (nonatomic,strong)UIImageView *alphaImageView;
@property (nonatomic,strong)UIPageControl *pageControl;

@end

@implementation MMHProductDetailHeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 创建ScrollView
    self.productImageViewScrollView = [[UIScrollView alloc]init];
    self.productImageViewScrollView.backgroundColor = [UIColor clearColor];
    self.productImageViewScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, MMHFloat(375));
    self.productImageViewScrollView.bounces = NO;
    self.productImageViewScrollView.showsHorizontalScrollIndicator = NO;
    self.productImageViewScrollView.showsVerticalScrollIndicator = NO;
    self.productImageViewScrollView.pagingEnabled = YES;
    self.productImageViewScrollView.delegate = self;
    self.productImageViewScrollView.directionalLockEnabled = YES;//锁定滑动的方向
    [self addSubview:self.productImageViewScrollView];
    
    // 创建pageBackground
    UIView *pageBackgroundView = [[UIView alloc]init];
    pageBackgroundView.backgroundColor = [UIColor blackColor];
    pageBackgroundView.alpha = .15f;
    pageBackgroundView.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - MMHFloat(40), MMHFloat(375) - MMHFloat(11) - MMHFloat(40), MMHFloat(40), MMHFloat(40));
    pageBackgroundView.layer.cornerRadius = MMHFloat(20.f);
    [self addSubview:pageBackgroundView];
    
    // 3. 创建动态page
    self.dynamicPageLabel = [[UILabel alloc]init];
    self.dynamicPageLabel.backgroundColor = [UIColor clearColor];
    self.dynamicPageLabel.font = [UIFont systemFontOfSize:mmh_relative_float(20)];
    self.dynamicPageLabel.text = @"1";
    self.dynamicPageLabel.textColor = [UIColor whiteColor];
    self.dynamicPageLabel.frame = CGRectMake(5, 7, 10, 20);
    [pageBackgroundView addSubview:self.dynamicPageLabel];

    
    // 4. 创建固定page
    self.fixedPageLabel = [[UILabel alloc]init];
    self.fixedPageLabel.backgroundColor = [UIColor clearColor];
    self.fixedPageLabel.textColor = [UIColor whiteColor];
    self.fixedPageLabel.font = [UIFont systemFontOfSize:MMHFloat(16.)];
    self.fixedPageLabel.frame = CGRectMake(CGRectGetMaxX(self.dynamicPageLabel.frame), self.dynamicPageLabel.frame.origin.y + mmh_relative_float(3), 15, 20);
    [pageBackgroundView addSubview:self.fixedPageLabel];
    
    // 5. 创建阴影imageView  pro_bg_blackshadow
    UIImageView *alphaImageView = [[UIImageView alloc]init];
    alphaImageView.backgroundColor = [UIColor clearColor];
    alphaImageView.image = [UIImage imageNamed:@"pro_bg_blackshadow"];
    alphaImageView.frame = CGRectMake(0, MMHFloat(375) - MMHFloat(20), kScreenBounds.size.width, MMHFloat(20));
    [self addSubview:alphaImageView];
    
//    // 6. 创建pageControl
//    self.pageControl = [[UIPageControl alloc]init];
//    self.pageControl.frame = CGRectMake(0, MMHFloat(375) - MMHFloat(45), kScreenBounds.size.width, MMHFloat(20));
//    self.pageControl.userInteractionEnabled = NO;
//    [self addSubview:self.pageControl];
}

-(void)setImageArray:(NSArray *)imageArray{
    _imageArray = imageArray;
    // 1. 创建图片
    if (imageArray.count){
        if (!self.productImageViewScrollView.subviews.count){
            for (int i = 0 ; i < imageArray.count ; i++){
                MMHImageView *productImageView = [[MMHImageView alloc]init];
                productImageView.backgroundColor = [UIColor clearColor];
                productImageView.stringTag = @"productImageView";
                productImageView.frame = CGRectMake(0 + i * kScreenBounds.size.width, 0,kScreenBounds.size.width, MMHFloat(375));
                [self.productImageViewScrollView addSubview:productImageView];
                [productImageView updateViewWithImageAtURL:[imageArray objectAtIndex:i]];
            }
        }
        self.productImageViewScrollView.contentSize = CGSizeMake(imageArray.count * kScreenBounds.size.width, 0);
        self.fixedPageLabel.text = [NSString stringWithFormat:@"/%lu",(unsigned long)imageArray.count];
    }
    // 创建pageControl
    self.pageControl.numberOfPages = imageArray.count;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.productImageViewScrollView){
        NSInteger page = scrollView.contentOffset.x/ kScreenBounds.size.width;
        self.dynamicPageLabel.text = [NSString stringWithFormat:@"%li",(long)page + 1];
    }
}

#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}

@end
