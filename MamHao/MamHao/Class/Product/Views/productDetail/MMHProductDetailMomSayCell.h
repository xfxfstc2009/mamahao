//
//  MMHProductDetailMomSayCell.h
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 【妈妈说】
#import <UIKit/UIKit.h>
#import "MMHMamSayModel.h"
@interface MMHProductDetailMomSayCell : UITableViewCell

@property (nonatomic,strong)MMHMamSayModel *mamSayModel;
@property (nonatomic,assign)BOOL mamCareIsOpen;               /**< 判断妈妈说是否打开状态*/
@property (nonatomic,strong)UIImageView *arrowImageView;      /**< 下拉箭头*/

+ (CGFloat)contentOfHeightWithMamSayModel:(MMHMamSayModel *)mamSayModel andMamCareIsOpen:(BOOL)mamCareIsOpen;

-(void)rotationWithView:(UIView *)view andtrans:(CGFloat)transForm;
@end
