//
//  MMHGuessYouLikeCell.m
//  MamHao
//
//  Created by SmartMin on 15/6/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHGuessYouLikeCell.h"
#import "MMHGuessYouLikeScrollView.h"               // 猜你喜欢

@interface MMHGuessYouLikeCell()
@property (nonatomic,strong)MMHGuessYouLikeScrollView *guessYouLikeScrollView;          /**< 猜你喜欢scrollView*/

@end

@implementation MMHGuessYouLikeCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self == [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建猜你喜欢
    UIView *guessYouLikeBackgroundView = [[UIView alloc]init];
    guessYouLikeBackgroundView.backgroundColor = [UIColor whiteColor];
    guessYouLikeBackgroundView.frame = CGRectMake(0, 0,kScreenBounds.size.width , MMHFloat(44));
    [self addSubview:guessYouLikeBackgroundView];
    
    // 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor hexChangeFloat:@"999999"];
    lineView.frame = CGRectMake(MMHFloat(11), guessYouLikeBackgroundView.frame.size.height / 2., kScreenBounds.size.width - 2 *     MMHFloat(11), .5f);
    [guessYouLikeBackgroundView addSubview:lineView];
    
    // 创建label
    UILabel *guessYouLikeLabel = [[UILabel alloc]init];
    guessYouLikeLabel.backgroundColor = [UIColor whiteColor];
    NSString *guessYouLikeString = @"猜你喜欢";
    guessYouLikeLabel.text = guessYouLikeString;
    CGSize guessYouLikeSize = [guessYouLikeString sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(43))];
    guessYouLikeLabel.textAlignment = NSTextAlignmentCenter;
    guessYouLikeLabel.font = F5;
    guessYouLikeLabel.textColor = C7;
    guessYouLikeLabel.frame = CGRectMake((kScreenBounds.size.width - guessYouLikeSize.width - 40) / 2., 0, guessYouLikeSize.width + 40, MMHFloat(44));
    [guessYouLikeBackgroundView addSubview:guessYouLikeLabel];
    
    // 创建line
    UIView *bottomLineView = [[UIView alloc]init];
    bottomLineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    bottomLineView.frame = CGRectMake(0, guessYouLikeBackgroundView.size_height - .5f, kScreenBounds.size.width, .5f);
    [guessYouLikeBackgroundView addSubview:bottomLineView];
    
    
    // 猜你喜欢
    self.guessYouLikeScrollView = [[MMHGuessYouLikeScrollView alloc]init];
    self.guessYouLikeScrollView.backgroundColor = [UIColor clearColor];
    self.guessYouLikeScrollView.frame = CGRectMake(0, CGRectGetMaxY(guessYouLikeBackgroundView.frame), kScreenBounds.size.width, MMHFloat(180));
    __weak typeof(self)weakSelf = self;
    self.guessYouLikeScrollView.guessYouLikeBlock = ^(MMHGuessYouLikeSingleModel *guessYouLikeSingleModel){
        weakSelf.guessYouLikeBlock(guessYouLikeSingleModel);
    };
    [self addSubview:self.guessYouLikeScrollView];
    
}

-(void)setGuessYouLikeListModel:(MMHGuessYouLikeListModel *)guessYouLikeListModel{
    _guessYouLikeListModel = guessYouLikeListModel;
    self.guessYouLikeScrollView.guessYouLikeArray = guessYouLikeListModel.data;
}

+(CGFloat)contentOfHeight{
    return MMHFloat(180 + 50);
}
@end
