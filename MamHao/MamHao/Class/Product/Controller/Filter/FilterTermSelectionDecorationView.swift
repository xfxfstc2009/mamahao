//
//  FilterTermSelectionDecorationView.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/15.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class FilterTermSelectionDecorationView: UICollectionReusableView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.whiteColor()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
