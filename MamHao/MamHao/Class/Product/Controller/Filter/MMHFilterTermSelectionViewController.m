//
//  MMHFilterTermSelectionViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFilterTermSelectionViewController.h"
#import "MMHFilter.h"
#import "MMHFilterTermSelectionBrandCell.h"
#import "MMHFilterTermSelectionAgeGroupCell.h"
#import "MMHFilterTerm.h"
#import "MamHao-Swift.h"
#import "MMHFilterTermData.h"
#import "UICollectionViewLeftAlignedLayout.h"


NSString * const MMHFilterTermSelectionBrandCellIdentifier = @"MMHFilterTermSelectionBrandCellIdentifier";
NSString * const MMHFilterTermSelectionAgeGroupCellIdentifier = @"MMHFilterTermSelectionAgeGroupCellIdentifier";
NSString * const MMHFilterTermSelectionHeaderIdentifier = @"MMHFilterTermSelectionHeaderIdentifier";
NSString * const MMHFilterTermSelectionDecorationViewIdentifier = @"MMHFilterTermSelectionDecorationViewIdentifier";


@interface MMHFilterTermSelectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) MMHFilter *filter;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, strong) UILabel *blankLabel;
@end


@implementation MMHFilterTermSelectionViewController


- (instancetype)initWithFilter:(MMHFilter *)filter
{
    self = [self init];
    if (self) {
        self.filter = filter;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"筛选";
    [self fetchTermData];
}


- (void)fetchTermData
{
    if (![self.filter isTermsDataAvailable]) {
        __weak __typeof(self) weakSelf = self;
        [self.view showProcessingView];
        [self.filter fetchTermsDataWithFinishedHandler:^(BOOL succeeded) {
            [self.view hideProcessingView];
            if (succeeded) {
                [weakSelf configureViews];
            }
            else {
                [self.view hideProcessingView];
                [weakSelf configureBlankViews];
            }
        }];
    }
    else {
        [self configureViews];
    }
}


- (void)configureViews
{
//    if (![self.filter isTermsDataAvailable]) {
//        return;
//    }

    UICollectionViewLeftAlignedLayout *layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.headerReferenceSize = MMHSizeMake(0.0f, 64.0f);
    layout.minimumLineSpacing = 0.0f;
    [layout registerClass:[FilterTermSelectionDecorationView class] forDecorationViewOfKind:MMHFilterTermSelectionDecorationViewIdentifier];
    layout.availableDecorationViewIdentifiers = @[MMHFilterTermSelectionDecorationViewIdentifier];

    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, self.view.bounds.size.height - MMHFloat(90.0f))
                                                          collectionViewLayout:layout];
//    collectionView.contentInset = MMHEdgeInsetsMake(0.0f, 0.0f, 90.0f, 0.0f);
    collectionView.allowsMultipleSelection = YES;
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.alwaysBounceVertical = YES;
    collectionView.backgroundColor = [UIColor colorWithHexString:@"f0f0f0"];
    [collectionView registerClass:[MMHFilterTermSelectionBrandCell class] forCellWithReuseIdentifier:MMHFilterTermSelectionBrandCellIdentifier];
    [collectionView registerClass:[MMHFilterTermSelectionAgeGroupCell class] forCellWithReuseIdentifier:MMHFilterTermSelectionAgeGroupCellIdentifier];
    [collectionView registerClass:[FilterTermSelectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MMHFilterTermSelectionHeaderIdentifier];
    [self.view addSubview:collectionView];
    self.collectionView = collectionView;
    
    UIView *confirmView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, MMHFloat(90.0f))];
    [confirmView moveToBottom:CGRectGetMaxY(self.view.bounds)];
    confirmView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    confirmView.backgroundColor = [MMHAppearance backgroundColor];
    [self.view addSubview:confirmView];

    UIButton *confirmButton = [[UIButton alloc] initWithFrame:mmh_relative_rect_make(0.0f, 0.0f, 200.0f, 40.0f)];
    confirmButton.backgroundColor = [UIColor colorWithHexString:@"fb4067"];
    confirmButton.layer.cornerRadius = 2.0f;
    confirmButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    confirmButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(16.0f)];
    [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmButton];
    [confirmButton moveToCenterOfSuperview];
    [confirmButton moveToBottom:CGRectGetMaxY(self.view.bounds) - mmh_relative_float(25.0f)];
    self.confirmButton = confirmButton;
}


- (void)configureBlankViews
{
    [self.view removeAllSubviews];
    
    UILabel *blankLabel = [[UILabel alloc] initWithFrame:self.view.bounds];
    blankLabel.numberOfLines = 0;
    blankLabel.textAlignment = NSTextAlignmentCenter;
    blankLabel.text = @"无法获取筛选数据\n请重试";
    [self.view addSubview:blankLabel];
    self.blankLabel = blankLabel;

    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(refresh:)];
    self.blankLabel.userInteractionEnabled = YES;
    [self.blankLabel addGestureRecognizer:tapGestureRecognizer];
}


- (void)refresh:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self fetchTermData];
}


- (void)confirm:(id)sender
{
    [self.filter confirmTermChangesWithIndexPaths:[self.collectionView indexPathsForSelectedItems]];
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)popViewController {
    Class aClass = self.shouldPopToViewControllerOfClassIfPoppedWithoutAnyFiltering;
    if (aClass != nil) {
        [self popToViewControllerOfClass:aClass];
    }
    else {
        [super popViewController];
    }
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    for (NSIndexPath *indexPath in [self.filter selectedIndexPaths]) {
        [self.collectionView selectItemAtIndexPath:indexPath
                                          animated:NO
                                    scrollPosition:UICollectionViewScrollPositionNone];
    }

//    for (NSInteger section = 0; section < [self.filter numberOfTermClasses]; section++) {
//        NSInteger row = [self.filter selectedTermIndexAtSection:section];
//        [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]
//                                          animated:NO
//                                    scrollPosition:UICollectionViewScrollPositionNone];
//    }
}


#pragma mark - UICollectionView data source and delegate


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.filter numberOfTermClasses];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.filter numberOfTermsAtSection:section];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MMHFilterTermType type = [self.filter termTypeAtSection:indexPath.section];
    switch (type) {
        case MMHFilterTermTypeNormal: {
            MMHFilterTermSelectionAgeGroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHFilterTermSelectionAgeGroupCellIdentifier
                                                                                                 forIndexPath:indexPath];
            cell.enabled = YES;
            FilterTermAge *term = (FilterTermAge *)[self.filter termAtIndex:indexPath.row atSection:indexPath.section];
            cell.term = term;
            if ([self.filter isIndexPathSelected:indexPath]) {
                cell.selected = YES;
            }
            else {
                cell.selected = NO;
            }
            return cell;
            break;
        }
        case MMHFilterTermTypeBrand: {
            MMHFilterTermSelectionBrandCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHFilterTermSelectionBrandCellIdentifier
                                                                                              forIndexPath:indexPath];
            FilterTermBrand *term = (FilterTermBrand *)[self.filter termAtIndex:indexPath.row atSection:indexPath.section];
            if (term == nil) {
            }
            else {
                cell.brand = term;
                if ([self.filter isIndexPathSelected:indexPath]) {
                    cell.selected = YES;
                }
                else {
                    cell.selected = NO;
                }
            }
            return cell;
            break;
        }
        case MMHFilterTermTypeCategory: {
            MMHFilterTermSelectionAgeGroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHFilterTermSelectionAgeGroupCellIdentifier
                                                                                                 forIndexPath:indexPath];
            cell.enabled = YES;
            FilterTermAge *term = (FilterTermAge *)[self.filter termAtIndex:indexPath.row atSection:indexPath.section];
            cell.term = term;
            if ([self.filter isIndexPathSelected:indexPath]) {
                cell.selected = YES;
            }
            else {
                cell.selected = NO;
            }
            return cell;
            break;
        }
        default:
            break;
    }
    
    return nil;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (![kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return nil;
    }
    
    FilterTermSelectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                              withReuseIdentifier:MMHFilterTermSelectionHeaderIdentifier
                                                                                     forIndexPath:indexPath];
    NSString *title = [self.filter titleForSection:indexPath.section];
    [headerView.titleLabel setText:title constrainedToLineCount:1];
    return headerView;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MMHFilterTermType type = [self.filter termTypeAtSection:indexPath.section];
    switch (type) {
        case MMHFilterTermTypeNormal: {
            FilterTermAge *term = (FilterTermAge *)[self.filter termAtIndex:indexPath.row atSection:indexPath.section];
            return [MMHFilterTermSelectionAgeGroupCell sizeWithTerm:term];
            break;
        }
        case MMHFilterTermTypeBrand:
            return mmh_relative_size_make(81.0f, 90.0f);
            break;
        case MMHFilterTermTypeCategory: {
            FilterTermCategory *term = (FilterTermCategory *)[self.filter termAtIndex:indexPath.row atSection:indexPath.section];
            return [MMHFilterTermSelectionAgeGroupCell sizeWithTerm:(FilterTermAge *)term];
            break;
        }
        default:
            break;
    }
    return CGSizeZero;
}


- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    MMHFilterTermType type = [self.filter termTypeAtSection:section];
    switch (type) {
        case MMHFilterTermTypeNormal:
        case MMHFilterTermTypeCategory:
            return mmh_relative_edgeInsets_make(0.0f, 20.0f, 16.0f, 20.0f);
            break;
        case MMHFilterTermTypeBrand:
            return mmh_relative_edgeInsets_make(0.0f, 10.0f, 16.0f, 10.0f);
            break;
        default:
            break;
    }
    return mmh_relative_edgeInsets_make(0.0f, 0.0f, 16.0f, 0.0f);
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    MMHFilterTermType type = [self.filter termTypeAtSection:section];
    switch (type) {
        case MMHFilterTermTypeNormal:
            return mmh_relative_float(16.0f);
            break;
        case MMHFilterTermTypeCategory:
            return mmh_relative_float(16.0f);
            break;
        case MMHFilterTermTypeBrand:
            return 10.0f;
            break;
        default:
            break;
    }
    return 0.0f;
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    MMHFilterTermType type = [self.filter termTypeAtSection:section];
    switch (type) {
        case MMHFilterTermTypeNormal:
            return mmh_relative_float(16.0f);
            break;
        case MMHFilterTermTypeCategory:
            return mmh_relative_float(16.0f);
            break;
        case MMHFilterTermTypeBrand:
            return 10.0f;
            break;
        default:
            break;
    }
    return 0.0f;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.filter multiSelectionEnabledAtSection:indexPath.section]) {
//        NSInteger selectedRow = [self.filter selectedTermIndexAtSection:indexPath.section];
//        [collectionView deselectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:indexPath.section] animated:NO];
        for (NSIndexPath *selectedIndexPath in [collectionView indexPathsForSelectedItems]) {
            if (selectedIndexPath.section == indexPath.section) {
                if (selectedIndexPath.row != indexPath.row) {
                    [collectionView deselectItemAtIndexPath:selectedIndexPath animated:NO];
                    [self.filter deselectTermAtIndexPath:selectedIndexPath];
                }
            }
        }
    }
    [self.filter selectTermAtIndexPath:indexPath];
}


- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.filter deselectTermAtIndexPath:indexPath];
//    [self.filter setSelectedTermIndex:-1 atSection:indexPath.section];
}


@end
