//
//  MMHFilterTermSelectionBrandCell.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFilterTermSelectionBrandCell.h"
#import "MMHFilter.h"
#import "MamHao-Swift.h"


@interface MMHFilterTermSelectionBrandCell ()

@property (nonatomic, strong) MMHImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@end


@implementation MMHFilterTermSelectionBrandCell


- (void)setBrand:(FilterTermBrand *)brand
{
    _brand = brand;

    [self.imageView updateViewWithImageAtURL:brand.imageURLString];
    [self.titleLabel setText:brand.name constrainedToLineCount:0];
}


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [[UIColor mamhaoMainColor] CGColor];

        UIView *backgroundView = [[UIView alloc] init];
        backgroundView.backgroundColor = [UIColor clearColor];
        self.backgroundView = backgroundView;

        MMHImageView *imageView = [[MMHImageView alloc] initWithFrame:mmh_relative_rect_make(10.0f, 5.0f, 60.0f, 60.0f)];
        [imageView setCenterX:CGRectGetMidX(self.bounds)];
//        imageView.backgroundColor = [UIColor orangeColor];
        [self addSubview:imageView];
        self.imageView = imageView;
    
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.bounds.size.width, 0.0f)];
        [titleLabel attachToBottomSideOfView:self.imageView byDistance:mmh_relative_float(5.0f)]; // TODO: - Louis -
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.0f)];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = @"";
        titleLabel.textColor = [UIColor colorWithHexString:@"666666"];
        [self.contentView addSubview:titleLabel];
        self.titleLabel = titleLabel;
    }
    return self;
}


- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
//    if (!self.enabled) {
//        return;
//    }
    
    if (selected) {
        self.layer.borderWidth = 1.0f;
    }
    else {
        self.layer.borderWidth = 0.0f;
    }
}


@end
