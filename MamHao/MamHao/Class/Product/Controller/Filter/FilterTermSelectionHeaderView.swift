//
//  FilterTermSelectionHeaderView.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class FilterTermSelectionHeaderView: UICollectionReusableView {
    
    let titleLabel: UILabel
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        self.titleLabel = UILabel(frame: MMHRectMake(10, 26, 40, 0))
        self.titleLabel.backgroundColor = UIColor.whiteColor()
        self.titleLabel.textColor = UIColor(hexString: "999999")
        self.titleLabel.font = MMHFontOfSize(14)
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        
        var headerView = UIView(frame: CGRectMake(0, 0, self.bounds.size.width, MMHFloat(10)))
        headerView.backgroundColor = UIColor(hexString: "f0f0f0")
        self.addSubview(headerView)
        
        self.addSubview(self.titleLabel)
    }

}
