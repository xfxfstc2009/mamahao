//
//  MMHFilterTermSelectionViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"


@class MMHFilter;


@interface MMHFilterTermSelectionViewController : AbstractViewController

@property (nonatomic, strong) Class shouldPopToViewControllerOfClassIfPoppedWithoutAnyFiltering;

- (instancetype)initWithFilter:(MMHFilter *)filter;

- (void)configureViews;

- (void)configureBlankViews;
@end
