//
//  MMHWebViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"

@interface MMHWebViewController : AbstractViewController

@property (nonatomic,strong)UIWebView *webView;

- (id)initWithResourceName:(NSString *)resoucePath title:(NSString *)title isRemote:(BOOL)fromRemote url:(NSString *)remoteURL;
@property (nonatomic,copy) NSString  *remoteUrl;

- (instancetype)initWithURLString:(NSString *)urlString title:(NSString *)title;


-(void)mamahaoHTMLManager:(NSString *)jsManager;
@end
