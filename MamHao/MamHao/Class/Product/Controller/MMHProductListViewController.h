//
//  MMHProductListViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHCartItem.h"


@class MMHFilter;
@class MMHProductList;


@interface MMHProductListViewController : AbstractViewController

- (instancetype)initWithFilter:(MMHFilter *)filter;

@end
