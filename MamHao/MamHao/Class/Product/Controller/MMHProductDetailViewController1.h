//
//  MMHProductDetailViewController1.h
//  MamHao
//
//  Created by SmartMin on 15/6/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHSingleProductModel.h"
#import "MMHProductDetailModel.h"

@interface MMHProductDetailViewController1 : AbstractViewController

@property (nonatomic,strong)MMHSingleProductModel * transfetSingleProductModel;         /**< 上个页面传递过来的Model */
@property (nonatomic,assign)MMHProductDetailSaleType productDetailSaleType;
@property (nonatomic,strong)MMHProductDetailModel *transferWithScanningproductDetail;   /**< 从扫码页面进入信息*/
@property (nonatomic, strong) NSTimer *beanProductTimer;

/**< 商品类别,妈豆商品，人民币商品*/
- (instancetype)initWithProduct:(MMHProduct<MMHProductProtocol> *)product;                        // 妈豆商品传入商品


@end
