//
//  MMHProductDetailViewController.h
//  MamHao
//
//  Created by SmartMin on 15/4/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

// 商品详情
#import "AbstractViewController.h"
#import "MMHSingleProductModel.h"                      // 单个商品


@interface MMHProductDetailViewController : AbstractViewController

- (instancetype)initWithSingleProduct:(MMHSingleProductModel *)singleProduct;
- (instancetype)initWithProduct:(id<MMHProductProtocol>)product;

- (id)initWithTemplateId:(NSString *)templateId;
@end
