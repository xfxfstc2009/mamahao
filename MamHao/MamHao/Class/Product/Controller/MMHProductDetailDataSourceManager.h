//
//  MMHProductDetailDataSourceManager.h
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 处理数据
#import <Foundation/Foundation.h>
#import "MMHProductDetailSuperModel.h"
#import "MMHProductDetailModel.h"

@interface MMHProductDetailDataSourceManager : NSObject

+ (MMHProductDetailSuperModel *)conversionWithProductDetailModel:(MMHProductDetailModel *)transferProductDetailModel;
@end
