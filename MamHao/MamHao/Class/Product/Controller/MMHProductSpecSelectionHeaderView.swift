//
//  MMHProductSpecSelectionHeaderView.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import UIKit


class ProductSpecSelectionHeaderView: UICollectionReusableView {
    
    let titleLabel: MMHTextLabel
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        self.titleLabel = MMHTextLabel(frame: MMHRectMake(10, 18, 40, 0))
        self.titleLabel.setWidth(mmh_screen_width() - MMHFloat(10) - MMHFloat(10))
        self.titleLabel.backgroundColor = UIColor.clearColor()
//        self.titleLabel.textColor = UIColor(hexString: "999999")
//        self.titleLabel.font = MMHFontOfSize(14)
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.whiteColor()
        
        var headerView = UIView(frame: CGRectMake(0, 0, self.bounds.size.width, 1))
        headerView.backgroundColor = UIColor(hexString: "ebebeb")
        self.addSubview(headerView)
        
        self.addSubview(self.titleLabel)
    }

}
