//
//  MMHProductDetailDataSourceManager.m
//  MamHao
//
//  Created by SmartMin on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailDataSourceManager.h"

@implementation MMHProductDetailDataSourceManager

+(MMHProductDetailSuperModel *)conversionWithProductDetailModel:(MMHProductDetailModel *)transferProductDetailModel{
    MMHProductDetailSuperModel *productDetailSuperModel = [[MMHProductDetailSuperModel alloc]init];
    // 1. 商品图片
    productDetailSuperModel.pic = transferProductDetailModel.pic;
    
    // 2. 商品价格栏目
    productDetailSuperModel.productDetailPriceModel = [[MMHProductDetailPriceModel alloc]init];
    productDetailSuperModel.productDetailPriceModel.price = transferProductDetailModel.price;
    productDetailSuperModel.productDetailPriceModel.originalPrice = transferProductDetailModel.originalPrice;
    productDetailSuperModel.productDetailPriceModel.isCollect = transferProductDetailModel.isCollect;
    productDetailSuperModel.productDetailPriceModel.getmBean = transferProductDetailModel.getmBean;
    productDetailSuperModel.productDetailPriceModel.itemId = transferProductDetailModel.itemId;
    // 妈豆支付
    productDetailSuperModel.productDetailPriceModel.mBeanPay = transferProductDetailModel.mBeanPay;
    // 收藏
    productDetailSuperModel.productDetailPriceModel.templateId = transferProductDetailModel.templateId;
    
    
    // 3. 商品名称
    productDetailSuperModel.productDetailIntroductionModel = [[MMHProductDetailIntroductionModel alloc]init];
    productDetailSuperModel.productDetailIntroductionModel.title = transferProductDetailModel.title;
    productDetailSuperModel.productDetailIntroductionModel.subtitle = transferProductDetailModel.subtitle;
    productDetailSuperModel.productDetailIntroductionModel.shopType = transferProductDetailModel.shopType;
    productDetailSuperModel.productDetailIntroductionModel.shopId = transferProductDetailModel.shopId;
    
    // 4. 妈妈说
    productDetailSuperModel.mamSayModel = [[MMHMamSayModel alloc]init];
    productDetailSuperModel.mamSayModel.mamCare = transferProductDetailModel.mamCare;
    productDetailSuperModel.mamSayModel.specialistName = transferProductDetailModel.specialistName;
    productDetailSuperModel.mamSayModel.specialistPhoto = transferProductDetailModel.specialistPhoto;
    
    // 5. 送至，以及物流
    productDetailSuperModel.deliveryTime = [[MMHDeliveryTimeModel alloc]init];
    
    // 6. 商品服务标记
    productDetailSuperModel.deliveryTime.mailPrice = transferProductDetailModel.mailPrice;
    productDetailSuperModel.deliveryTime.mailPriceTerm = transferProductDetailModel.mailPriceTerm;    
    productDetailSuperModel.deliveryTime.shop = transferProductDetailModel.shop;
    productDetailSuperModel.deliveryTime.shopId = transferProductDetailModel.shopId;
    productDetailSuperModel.goodsTag = transferProductDetailModel.goodsTag;
    
    // 7 质检担保
    productDetailSuperModel.qualityPic = transferProductDetailModel.qualityPic;
    
    // 8.店铺
    productDetailSuperModel.shopInfo =[[MMHProductDetailShopInfoModel alloc]init];
    productDetailSuperModel.shopInfo = transferProductDetailModel.shopInfo;
    
    return productDetailSuperModel;
}

@end
