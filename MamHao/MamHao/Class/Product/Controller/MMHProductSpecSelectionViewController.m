//
//  MMHProductSpecSelectionViewController.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductSpecSelectionViewController.h"
#import "MMHProductDetailModel.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "MMHFilterTermSelectionAgeGroupCell.h"
#import "MamHao-Swift.h"
#import "MMHAppearance.h"
#import "MMHTabBarController.h"
#import "UIImageView+WebCache.h"


NSString * const MMHProductSpecSelectionCellIdentifier = @"MMHProductSpecSelectionCellIdentifier";
NSString * const MMHProductSpecSelectionHeaderIdentifier = @"MMHProductSpecSelectionHeaderIdentifier";
//NSString * const MMHFilterTermSelectionDecorationViewIdentifier = @"MMHFilterTermSelectionDecorationViewIdentifier";


@interface MMHProductSpecSelectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate, StepperDelegate>

@property (nonatomic, strong) ProductSpecFilter *specFilter;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) MMHImageView *imageView;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *selectedSpecsLabel;
@property (nonatomic, strong) UIView *confirmView;
@property (nonatomic, strong) Stepper *quantityStepper;
@property (nonatomic, strong) UILabel *quantityTipsLabel;
@property (nonatomic, strong) UIButton *confirmButton;
@property (nonatomic, copy) void (^specSelectedHandler)();
//@property (nonatomic, strong) UIButton *deselectAllButton;
@property (nonatomic, strong) MMHProductDetailModel *productDetail;
@end


@implementation MMHProductSpecSelectionViewController


- (CGSize)sizeWhileFloating
{
    if ([self.specFilter hasAnySpecs]) {
        return CGSizeMake(mmh_screen_width(), MMHFloat(420.0f));
    }
    return CGSizeMake(mmh_screen_width(), MMHFloat(220.0f));
}


- (instancetype)initWithProductDetail:(MMHProductDetailModel *)productDetail specSelectedHander:(void (^)())specSelectedHandler
{
    self = [self init];
    if (self) {
        self.productDetail = productDetail;
        self.specFilter = productDetail.specFilter;
        self.specSelectedHandler = specSelectedHandler;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor clearColor];
    UIView *contentView = [[UIView alloc] initWithFrame:self.view.bounds];
    if ([self.specFilter hasAnySpecs]) {
        contentView.height = MMHFloat(420.0f);
    }
    else {
        contentView.height = MMHFloat(220.0f);
    }
    contentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentView];
    self.contentView = contentView;

    UIView *imageBackgroundView = [[UIView alloc] initWithFrame:MMHRectMake(10.0f, -20.0f, 100.0f, 100.0f)];
    imageBackgroundView.backgroundColor = [UIColor whiteColor];
    imageBackgroundView.layer.cornerRadius = MMHFloat(5.0f);
    imageBackgroundView.layer.borderWidth = 1.0f;
    imageBackgroundView.layer.borderColor = [[UIColor colorWithHexString:@"ebebeb"] CGColor];
    [self.contentView addSubview:imageBackgroundView];

    MMHImageView *imageView = [[MMHImageView alloc] initWithFrame:imageBackgroundView.bounds];
    [imageView setSize:MMHSizeMake(90.0f, 90.0f)];
    imageView.layer.borderColor = [[UIColor colorWithHexString:@"ebebeb"] CGColor];
    imageView.layer.borderWidth = 1.0f;
    [imageBackgroundView addSubview:imageView];
    self.imageView = imageView;
    [imageView moveToCenterOfSuperview];
    
//    UIImage *deselectAllImage = [UIImage imageNamed:@"pro_btn_close"];
//    UIButton *deselectAllButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, MMHFloat(30.0f), deselectAllImage.size.width, deselectAllImage.size.height)];
//    deselectAllButton.right = CGRectGetMaxX(self.contentView.bounds) - MMHFloat(10.0f);
//    [deselectAllButton setImage:deselectAllImage forState:UIControlStateNormal];
//    [deselectAllButton addTarget:self action:@selector(deselectAllSpecs:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:deselectAllButton];
//    self.deselectAllButton = deselectAllButton;

    UILabel *priceLabel = [[UILabel alloc] initWithFrame:MMHRectMake(120.0f, 20.0f, 0.0f, 10.0f)];
//    [priceLabel setMaxX:self.deselectAllButton.left];
    [priceLabel setMaxX:mmh_screen_width()];
    priceLabel.textColor = [MMHAppearance redColor];
    priceLabel.font = MMHFontOfSize(23.0f);
//    [priceLabel setSingleLineText:@"￥29999.88" constrainedToWidth:CGFLOAT_MAX];
    [self.contentView addSubview:priceLabel];
    self.priceLabel = priceLabel;

    UILabel *selectedSpecsLabel = [[UILabel alloc] init];
    selectedSpecsLabel.left = self.priceLabel.left;
    selectedSpecsLabel.width = self.priceLabel.width;
    selectedSpecsLabel.top = MMHFloat(48.0f);
    selectedSpecsLabel.font = MMHFontOfSize(14.0f);
    selectedSpecsLabel.textColor = [UIColor colorWithHexString:@"666666"];
    [self.contentView addSubview:selectedSpecsLabel];
    self.selectedSpecsLabel = selectedSpecsLabel;
    
    UICollectionViewLeftAlignedLayout * layout = [[UICollectionViewLeftAlignedLayout alloc] init];
    layout.headerReferenceSize = mmh_relative_size_make(0.0f, 35.0f);
    layout.sectionInset = MMHEdgeInsetsMake(15.0f, 10.0f, 15.0f, 10.0f);
    layout.minimumLineSpacing = 15.0f;
//    [layout registerClass:[FilterTermSelectionDecorationView class] forDecorationViewOfKind:MMHFilterTermSelectionDecorationViewIdentifier];

    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0.0f, MMHFloat(88.0f), self.contentView.bounds.size.width, self.contentView.bounds.size.height - MMHFloat(88.0f) - 130.0f)
                                                          collectionViewLayout:layout];
//    collectionView.contentInset = mmh_relative_edgeInsets_make(0.0f, 0.0f, 90.0f, 0.0f);
    collectionView.allowsMultipleSelection = YES;
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.alwaysBounceVertical = YES;
    collectionView.backgroundColor = [UIColor whiteColor];
//    [collectionView registerClass:[MMHFilterTermSelectionBrandCell class] forCellWithReuseIdentifier:MMHFilterTermSelectionBrandCellIdentifier];
    [collectionView registerClass:[MMHFilterTermSelectionAgeGroupCell class] forCellWithReuseIdentifier:MMHProductSpecSelectionCellIdentifier];
    [collectionView registerClass:[ProductSpecSelectionHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MMHProductSpecSelectionHeaderIdentifier];
    [self.contentView addSubview:collectionView];
    self.collectionView = collectionView;

    UIView *confirmView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.contentView.bounds.size.width, 130.0f)];
    [confirmView moveToBottom:CGRectGetMaxY(self.contentView.bounds)];
    confirmView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:confirmView];
    self.confirmView = confirmView;

    CGRect stepperFrame = MMHRectMake(0.0f, 15.0f, 134.0f, 40.0f);
    Stepper *quantityStepper = [[Stepper alloc] initWithFrame:stepperFrame
                                         minimumValue:1
                                         maximumValue:NSIntegerMax
                                                value:1];
    quantityStepper.right = CGRectGetMaxX(self.confirmView.bounds) - MMHFloat(10.0f);
//    quantityStepper.bottom = CGRectGetMaxY(self.confirmView.bounds) - MMHFloat(85.0f);
//    [quantityStepper addTarget:self action:@selector(quantityStepperStepped:) forControlEvents:UIControlEventValueChanged];
    quantityStepper.delegate = self;
    [self.confirmView addSubview:quantityStepper];
    self.quantityStepper = quantityStepper;
    
    UILabel *quantityTipsLabel = [[UILabel alloc] initWithFrame:MMHRectMake(10.0f, 0.0f, 100.0f, 0.0f)];
    quantityTipsLabel.font = MMHFontOfSize(15.0f);
    quantityTipsLabel.textColor = [UIColor colorWithHexString:@"666666"];
    [quantityTipsLabel setText:@"购买数量" constrainedToLineCount:0];
    quantityTipsLabel.centerY = self.quantityStepper.centerY;
    [self.confirmView addSubview:quantityTipsLabel];
    self.quantityTipsLabel = quantityTipsLabel;

    UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.confirmView.bounds.size.width, 50.0f)];
    confirmButton.bottom = CGRectGetMaxY(self.confirmView.bounds);
    [confirmButton setBackgroundImage:[UIImage patternImageWithColor:[MMHAppearance pinkColor]] forState:UIControlStateNormal];
    [confirmButton setBackgroundImage:[UIImage patternImageWithColor:[MMHAppearance separatorColor]] forState:UIControlStateDisabled];
//    confirmButton.backgroundColor = [MMHAppearance pinkColor];
    [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    confirmButton.titleLabel.font = MMHFontOfSize(18.0f);
    [confirmButton addTarget:self action:@selector(confirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmView addSubview:confirmButton];
    self.confirmButton = confirmButton;
    
    [self.confirmView addTopSeparatorLine];

//    [self.specFilter selectDefaultIndexes];
    [self updateViews];
}


- (void)updateViews
{
//    NSString *priceString = [NSString stringWithFormat:@"￥ %.2f", self.productDetail.price];
//    [self.priceLabel setText:priceString constrainedToLineCount:1];
//
    NSArray *selectedSpecNames = [self.specFilter selectedSpecOptionNames];
    if ([selectedSpecNames count] == 0) {
        self.selectedSpecsLabel.text = @"";
//        self.deselectAllButton.hidden = YES;
    }
    else {
        NSArray *quotedSpecNames = [selectedSpecNames transformedArrayUsingHandler:^id(id originalObject, NSUInteger index) {
            return [NSString stringWithFormat:@"“%@”", originalObject];
        }];
        NSString *selectedSpecNamesString = [quotedSpecNames componentsJoinedByString:@" "];
        [self.selectedSpecsLabel setText:selectedSpecNamesString constrainedToLineCount:0];
//        self.deselectAllButton.hidden = NO;
    }
    
    [self tryToUpdateSpecParameter];
}


- (void)deselectAllSpecs:(UIButton *)button
{
    return;
    
//    [self.specFilter deselectAll];
//    [self updateViews];
//    [self.collectionView reloadData];
}


- (void)confirm:(UIButton *)confirmButton
{
    confirmButton.enabled = NO;
    if (self.shouldShowAddingToCartAnimation) {
        [self addAnimationWithView];
    }
    else {
            __weak __typeof(self) weakSelf = self;
            [self.settledViewController dismissFloatingViewControllerAnimated:YES completion:^{
                if (weakSelf.specSelectedHandler) {
                    weakSelf.specSelectedHandler();
                }
            }];
    }
}

#pragma mark ares 
-(void)confirmationManager{
    //    NSString *unselectedSpecName = [self.specFilter unselectedSpecName];
    //    if (unselectedSpecName) {
    //        [self.contentView showTips:[NSString stringWithFormat:@"请选择%@", unselectedSpecName]];
    //        return;
    //    }
        __weak __typeof(self) weakSelf = self;
        [self.settledViewController dismissFloatingViewControllerAnimated:YES completion:^{
            if (weakSelf.specSelectedHandler) {
                weakSelf.specSelectedHandler();
            }
        }];
}


#pragma mark - StepperDelegate


- (BOOL)stepper:(Stepper * __nonnull)stepper shouldChangeValueFrom:(NSInteger)currentValue to:(NSInteger)newValue
{
    if ([self.specFilter isQuantityExceedsLimit:newValue]) {
        [self.view showTips:[NSString stringWithFormat:@"最多只能购买%ld件哦~", currentValue]];
        return NO;
    }
    else {
        self.specFilter.quantity = newValue;
        return YES;
    }
}


- (void)quantityStepperStepped:(Stepper *)stepper
{
    self.specFilter.quantity = stepper.value;
}


#pragma mark - UICollectionView data source and delegate


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return [self.specFilter numberOfSpecs];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.specFilter numberOfOptionsAtSpecIndex:section];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *name = [self.specFilter nameOfOptionAtIndexPath:indexPath];
    MMHFilterTermSelectionAgeGroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:MMHProductSpecSelectionCellIdentifier
                                                                                         forIndexPath:indexPath];
    cell.layer.cornerRadius = 3.0f;
    cell.title = name;
    
    if (![self.specFilter selectabilityForOptionAtIndex:indexPath.row forSpecAtSection:indexPath.section]) {
        cell.enabled = NO;
    }
    else {
        cell.enabled = YES;
    }
    
    if (indexPath.row == [self.specFilter selectedOptionIndexOfSpecAtIndex:indexPath.section]) {
        cell.selected = YES;
    }
    else {
        cell.selected = NO;
    }
    return cell;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (![kind isEqualToString:UICollectionElementKindSectionHeader]) {
        return nil;
    }

    ProductSpecSelectionHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:kind
                                                                                   withReuseIdentifier:MMHProductSpecSelectionHeaderIdentifier
                                                                                          forIndexPath:indexPath];
    NSString *title = [self.specFilter nameOfSpecAtIndex:indexPath.section];
    [headerView.titleLabel setText:title constrainedToLineCount:1];
    return headerView;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *nameOfOption = [self.specFilter nameOfOptionAtIndexPath:indexPath];
    CGSize size = [MMHFilterTermSelectionAgeGroupCell sizeWithString:nameOfOption];
    return CGSizeMake(MAX(size.width, 80.0f), MAX(size.height, 36.0f));
//    FilterTerm *term = [self.specFilter termAtIndex:indexPath.row atSection:indexPath.section];
//    return [MMHFilterTermSelectionAgeGroupCell sizeWithTerm:term];
}


//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return mmh_relative_edgeInsets_make(0.0f, 20.0f, 16.0f, 20.0f);
//}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 15.0f;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.specFilter selectabilityForOptionAtIndex:indexPath.row forSpecAtSection:indexPath.section]) {
        [self.contentView showTips:@"没有库存"];
        return;
    }
    
    NSInteger selectedRow = [self.specFilter selectedOptionIndexOfSpecAtIndex:indexPath.section];
    if (selectedRow == indexPath.row) {
        return;
    }
    
    [collectionView deselectItemAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:indexPath.section] animated:NO];
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:indexPath.section]];
    cell.selected = NO;
    for (NSIndexPath *selectedIndexPath in [collectionView indexPathsForSelectedItems]) {
        if (selectedIndexPath.section == indexPath.section) {
            if (selectedIndexPath.row != indexPath.row) {
                [collectionView deselectItemAtIndexPath:selectedIndexPath animated:NO];
            }
        }
    }
    [self.specFilter setSelectedOptionIndex:indexPath.row ofSpecAtIndex:indexPath.section];
    [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
    [self updateViews];
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.specFilter selectabilityForOptionAtIndex:indexPath.row forSpecAtSection:indexPath.section]) {
        return NO;
    }
    
    NSInteger selectedRow = [self.specFilter selectedOptionIndexOfSpecAtIndex:indexPath.section];
    if (selectedRow == indexPath.row) {
        return NO;
    }
    return YES;
}


- (void)tryToUpdateSpecParameter
{
    self.confirmButton.enabled = NO;
    if (![self.specFilter isAllSpecSelected]) {
        self.priceLabel.text = @"";
        return;
    }
    
    if (![self.specFilter isCurrentSelectionAvailableForSale]) {
        self.priceLabel.text = @"暂无供应";
        return;
    }
    else {
        self.priceLabel.text = @"";
    }
    
    ProductSpecParameter *selectedSpecParameter = self.specFilter.selectedSpecParameter;
    if (selectedSpecParameter != nil) {
        [self updateSpecParameterViewsWithSpecParameter:selectedSpecParameter];
        return;
    }
    
    [self.contentView showProcessingViewWithYOffset:0.0f];
    __weak typeof(self) weakSelf = self;
    [self.specFilter fetchParameterForCurrentSelectionWithCompletion:^(BOOL succeeded, ProductSpecParameter * __nullable specParameter) {
        [weakSelf.contentView hideProcessingView];
        if (!succeeded) {
            [weakSelf.contentView showTips:@"无法获取价格信息, 请检查网络"];
        }
        [weakSelf updateSpecParameterViewsWithSpecParameter:specParameter];
    }];
}


- (void)updateSpecParameterViewsWithSpecParameter:(ProductSpecParameter *)specParameter
{
    if (specParameter == nil) {
        [self.imageView sd_cancelCurrentImageLoad];
        [self.imageView updateViewWithImage:nil];
        [self.priceLabel setText:@"未知价格" constrainedToLineCount:1];
        self.confirmButton.enabled = NO;
    }
    else {
        [self.imageView updateViewWithImageAtURL:specParameter.imageURLString];
        if (self.productDetail.isBeanProduct) {
            NSString *priceString = [NSString beanPriceStringWithPrice:self.specFilter.selectedSpecParameter.price beanPrice:self.specFilter.selectedSpecParameter.beanPrice];
            [self.priceLabel setText:priceString constrainedToLineCount:1];
        }
        else {
            [self.priceLabel setText:[NSString stringWithPrice:specParameter.price] constrainedToLineCount:1];
        }
        self.confirmButton.enabled = YES;
    }
}


#pragma mark - Ares addShoppingCartAnimation
-(void)addAnimationWithView {
    UIImageView *productImageView = [[UIImageView alloc]init];
    productImageView.image = self.imageView.image;
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    CGRect rect=[self.imageView convertRect:self.imageView.bounds toView:window];
    productImageView.frame = rect;
    [self.view.window addSubview:productImageView];
    [self performSelector:@selector(CustomAnimation:) withObject:productImageView afterDelay:.5f];
}

-(void)CustomAnimation:(id)imageView{
    MMHImageView *productImageView = (MMHImageView *)imageView;
    [UIView animateWithDuration:1.0f animations:^{
        [productImageView setFrame:CGRectMake(kScreenBounds.size.width - MMHFloat(20),20 + MMHFloat(10), productImageView.size_width * .1f, productImageView.size_height * .1f)];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        CGAffineTransform scakeTrans = CGAffineTransformMakeScale(0.1f, 0.1f);
        productImageView.transform = scakeTrans;
        
    } completion:^(BOOL finished) {
        [productImageView removeFromSuperview];
        [self confirmationManager];
    }];
}


@end
