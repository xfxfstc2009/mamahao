//
//  MMHProductDetailViewController1.m
//  MamHao
//
//  Created by SmartMin on 15/6/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailViewController1.h"
// cell
#import "MMHProductDetailHeaderCell.h"                  // 商品详情
#import "MMHProductDetailPriceCell.h"                   // 价格
#import "MMHProductDetailIntroductionCell.h"            // 商品名称，商品简介
#import "MMHProductDetailMomSayCell.h"                  // 妈妈说
#import "MMHWordOfMouthTableViewCell.h"                 // 商品口碑
#import "MMHGuessYouLikeCell.h"                         // 猜你喜欢
#import "MMHProductDetailStoreCell.h"                   // 门店
#import "MMHDeleteLineLabel.h"                          // 删除线
#import "MMHPopView.h"                                  // 浮层view

// push
#import "MMHWordOfMouthViewController.h"                // 图文详情-商品参数跳转
#import "MMHDanbaoViewController.h"                     // 质检担保
#import "MMHAddressListViewController.h"                // 地址列表
#import "MMHChattingViewController.h"                   // 联系客服
#import "MMHChattingViewController+Preparation.h"       // 联系客服 - 1
#import "MMHConfirmationOrderViewController.h"          // 去结算
#import "MMHWebViewController.h"                        // 妈豆有神马用
#import "MMHTabBarController.h"
#import "MMHWordOfMouthViewController.h"                // 口碑页面
#import "MMHCartViewController.h"
#import "MMHChangeAddressViewController.h"              // 切换地址

// MODEL
#import "MMHMamSayModel.h"                              // 妈妈说model
#import "MMHProductDetailModel.h"
#import "MMHProductDetailDataSourceManager.h"           // 处理商品详情类目SuperModel
#import "MMHProductDetailIntroductionModel.h"           // 商品简介Model
#import "MMHProductSpecSelectionViewController.h"
#import "MMHMamhaoBeanSpecialProduct.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Cart.h"
#import "MMHProductDetailShopProduct.h"

// Manager
#import "MMHDistanceWithLocation.h"
#import "MMHCurrentLocationModel.h"                     // 获取当前定位地址
#import "MMHLocationManager.h"                          // 获取当前定位地址
#import "AbstractViewController+Chatting.h"
#import "MMHNetworkAdapter+WordOfMouth.h"
#import "MMHAccountSession.h"


typedef NS_ENUM(NSInteger, MMHProductDetailFail) {
    MMHProductDetailFailNormal,                       /**< 正常状态*/
    MMHProductDetailFailNotStartedYet,                /**< 活动还没有开始*/
    MMHProductDetailFailMbeanNotEnough,               /**< 妈豆不足*/
    MMHProductDetailFailNoProduct,                    /**< 没有商品强光了*/
    MMHProductDetailFailActivieyEnd,                  /**< 活动结束*/
};


@interface MMHProductDetailViewController1()<UITableViewDataSource,UITableViewDelegate,MMHProductDetailModelDelegate>
@property (nonatomic,strong)UITableView *productDetailTableView;                        /**< tableView*/
@property (nonatomic,strong)NSMutableArray *productMutableArray;                        /**< dataSource*/
@property (nonatomic,assign)BOOL mamCareIsOpen;                                         /**< 判断妈妈说是否打开*/
@property (nonatomic,strong)UIView *floatView;                                          /**< 浮层的view*/

// 商品口碑
@property (nonatomic, strong) NSMutableArray *womDataArray;                             /**<口碑数据数组*/
@property (nonatomic, strong) NSDictionary *WomMouthDic;                                /**<口碑字典*/
@property (nonatomic, strong) MMHProduct<MMHProductProtocol> *product;
@property (nonatomic, strong) MMHProductDetailModel *productDetailModel;                /**< 商品详情数据*/
@property (nonatomic, strong) MMHProductDetailSuperModel *productDetailSuperModel;      /**< 整理完成的商品详情数据*/
@property (nonatomic, strong) MMHGuessYouLikeListModel *guessYouLikeListModel;          /**< 猜你喜欢列表*/
@property (nonatomic, assign) MMHProductDetailFail productDetailFail;                   /**< 失败页面*/
@property (nonatomic, strong) UIButton *contactUsButton;
@property (nonatomic, strong) UIButton *addToCartButton;
@property (nonatomic, strong) UIButton *buyNowButton;                                   /**< 立即购买*/
@property (nonatomic, strong) MMHAddressSingleModel *addressSingleModel;                /**< 用来切换地址*/
@property (nonatomic, strong) UIView *failAlphaView;                                    /**< 失败条*/
@property (nonatomic,strong)UIButton *rightNavButton;                                   /**< navRightButton*/


@end

@implementation MMHProductDetailViewController1

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    __weak typeof(self)weakSelf = self;
    if (weakSelf.productDetailSaleType == MMHProductDetailSaleTypeMBean){       // 执行倒计时操作
        [weakSelf tryToUpdateViewsForBeanProduct];
        [weakSelf tryToStartTimerForBeanProduct];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    if (self.beanProductTimer) {
        [self.beanProductTimer invalidate];
        self.beanProductTimer = nil;
    }
}


- (void)dealloc
{
    self.productDetailTableView.dataSource = nil;
    self.productDetailTableView.delegate = nil;
    self.productDetailModel.delegate = nil;
}

- (NSMutableArray *)womDataArray {
    if (!_womDataArray) {
        self.womDataArray = [NSMutableArray array];
    }
    return _womDataArray;
}

- (MMHProductDetailSaleType)productDetailSaleType {
    if ([self.product respondsToSelector:@selector(productDetailSaleType)]) {
        return self.product.productDetailSaleType;
    }
    return MMHProductDetailSaleTypeRMB;
}

- (instancetype)initWithProduct:(MMHProduct<MMHProductProtocol> *)product {
    self = [self init];
    if (self) {
        self.product = product;
    }
    return self;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createFloatView];
    [self setActionButtonsEnabled:NO];
    __weak typeof(self)weakSelf =self;
    [weakSelf currentLocation];                                                                 // 获取商品详情
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"商品详情";
    __weak typeof(self)weakSelf = self;
    self.rightNavButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"product_icon_shop_nol"] barHltImage:[UIImage imageNamed:@"mmhtab_icon_shop_hlt"] action:^{
        [weakSelf.rightNavButton setImage:[UIImage imageNamed:@"product_icon_shop_nol"] forState:UIControlStateNormal];
        [MMHTabBarController redirectToCart];
        [MMHLogbook logEventType:MMHBuriedPointTypeShoppingCart];
    } ];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit {
    self.productMutableArray = [NSMutableArray array];
    self.productDetailModel = [[MMHProductDetailModel alloc]init];
    self.productDetailModel.delegate = self;
    self.guessYouLikeListModel = [[MMHGuessYouLikeListModel alloc]init];
    self.addressSingleModel = [[MMHAddressSingleModel alloc]init];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.productDetailTableView){
        self.productDetailTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.productDetailTableView.size_height = kScreenBounds.size.height - MMHFloat(49);
        self.productDetailTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.productDetailTableView.delegate = self;
        self.productDetailTableView.dataSource = self;
        self.productDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.productDetailTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.productDetailTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView != self.productDetailTableView) {
        return 0;
    }
    return self.productMutableArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.productMutableArray objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头部图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"头部图片"]){
        static NSString *cellIdentifyWithSecOneRowOne = @"cellIdentifyWithSecOneRowOne";
        MMHProductDetailHeaderCell *cellWithSecOneRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecOneRowOne];
        if (!cellWithSecOneRowOne){
            cellWithSecOneRowOne = [[MMHProductDetailHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecOneRowOne];
            cellWithSecOneRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        // 【赋值】
        cellWithSecOneRowOne.imageArray = self.productDetailSuperModel.pic;
        return cellWithSecOneRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品价格"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品价格"]){
        static NSString *cellIdentifyWithSecOneRowTwo = @"cellIdentifyWithSecOneRowTwo";
        MMHProductDetailPriceCell *cellWithSecOneRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecOneRowTwo];
        if (!cellWithSecOneRowTwo){
            cellWithSecOneRowTwo = [[MMHProductDetailPriceCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecOneRowTwo];
            cellWithSecOneRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithSecOneRowTwo.transferProductDetailSaleType = self.productDetailSaleType;
        cellWithSecOneRowTwo.transferProductDetailPriceModel = self.productDetailSuperModel.productDetailPriceModel;
        cellWithSecOneRowTwo.productDetailModel = self.productDetailModel;
        
        // 妈豆有神马用button
        UIButton *mBeanHowUse = (UIButton *)[cellWithSecOneRowTwo viewWithStringTag:@"howUseButton"];
        [mBeanHowUse addTarget:self action:@selector(howUseButtonClick) forControlEvents:UIControlEventTouchUpInside];
        
        return cellWithSecOneRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品介绍"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品介绍"]){                               // 【商品介绍】
        static NSString *cellIdentifyWithSecOneRowThr = @"cellIDentifyWithSecOneRowThr";
        MMHProductDetailIntroductionCell *cellWithSecOneRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecOneRowThr];
        if (!cellWithSecOneRowThr){
            cellWithSecOneRowThr = [[MMHProductDetailIntroductionCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecOneRowThr];
            cellWithSecOneRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithSecOneRowThr.productDetailIntroductionModel = self.productDetailSuperModel.productDetailIntroductionModel;
        
        return cellWithSecOneRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"妈妈说"] && indexPath.row == [self cellIndexPathRowWithcellData:@"妈妈说"]){                                   // 妈妈说
        static NSString *cellIdentifyWithSecTwoRowOne = @"cellIdentifyWithSecTwoRowOne";
        MMHProductDetailMomSayCell *cellWithSecTwoRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecTwoRowOne];
        if (!cellWithSecTwoRowOne){
            cellWithSecTwoRowOne = [[MMHProductDetailMomSayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecTwoRowOne];
            cellWithSecTwoRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithSecTwoRowOne.mamCareIsOpen = self.mamCareIsOpen;
        cellWithSecTwoRowOne.mamSayModel = self.productDetailSuperModel.mamSayModel;
        if (cellWithSecTwoRowOne.mamCareIsOpen){
            [cellWithSecTwoRowOne rotationWithView:cellWithSecTwoRowOne.arrowImageView andtrans:(180 * M_PI) / 180.];
        } else {
            [cellWithSecTwoRowOne rotationWithView:cellWithSecTwoRowOne.arrowImageView andtrans:(360 * M_PI) / 180.];
        }
        
        return cellWithSecTwoRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"送至"]){
        static NSString *cellIdentifyWithSecThr = @"cellIdentifyWithSecThr";
        UITableViewCell *cellWithSecThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecThr];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithSecThr){
            cellWithSecThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecThr];
            cellWithSecThr.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // fixedLabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.textAlignment = NSTextAlignmentLeft;
            fixedLabel.textColor = C4;
            fixedLabel.font = F4;
            CGSize fixedContentOfSize = [@"运费" sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
            fixedLabel.frame = CGRectMake(MMHFloat(16), (MMHFloat(34) - [MMHTool contentofHeight:fixedLabel.font]) / 2., fixedContentOfSize.width, [MMHTool contentofHeight:fixedLabel.font]);
            fixedLabel.stringTag = @"fixedLabel";
            [cellWithSecThr addSubview:fixedLabel];
            
            //dymicLabel
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.backgroundColor = [UIColor clearColor];
            dymicLabel.stringTag = @"dymicLabel";
            dymicLabel.textAlignment = NSTextAlignmentLeft;
            dymicLabel.numberOfLines = 1;
            dymicLabel.font = F4;
            dymicLabel.textColor = C6;
            dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), fixedLabel.orgin_y, kScreenBounds.size.width - MMHFloat(11) - CGRectGetMaxX(fixedLabel.frame) - MMHFloat(10), [MMHTool contentofHeight:dymicLabel.font]);
            [cellWithSecThr addSubview:dymicLabel];
            
            // 创建subTitleLabel
            UILabel *subTitleLabel = [[UILabel alloc]init];
            subTitleLabel.backgroundColor = [UIColor clearColor];
            subTitleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
            subTitleLabel.numberOfLines = 1;
            subTitleLabel.frame = CGRectMake(dymicLabel.orgin_x, CGRectGetMaxY(dymicLabel.frame) + MMHFloat(5), dymicLabel.size_width, [MMHTool contentofHeight:subTitleLabel.font]);
            subTitleLabel.stringTag = @"subTitleLabel";
            [cellWithSecThr addSubview:subTitleLabel];
            
            UIView *cellBgView = [[UIView alloc]init];
            cellBgView.backgroundColor = [UIColor clearColor];
            cellBgView.stringTag = @"cellBgView";
            cellBgView.frame = CGRectMake(0, MMHFloat(10), cellWithSecThr.size_width, cellHeight);
            cellBgView.hidden = YES;
            [cellWithSecThr addSubview:cellBgView];
            
            UIImageView *locationImageView = [[UIImageView alloc]init];
            locationImageView.backgroundColor = [UIColor clearColor];
            locationImageView.image = [UIImage imageNamed:@"order_icon_positioning"];
            locationImageView.hidden = YES;
            locationImageView.stringTag = @"locationImageView";
            [cellWithSecThr addSubview:locationImageView];
        }
        // 赋值
        UIView *cellBgView = (UIView *)[cellWithSecThr viewWithStringTag:@"cellBgView"];
        
        UILabel *fixedLabel = (UILabel *)[cellWithSecThr viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.productMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        UIImageView *locationImageView = (UIImageView *)[cellWithSecThr viewWithStringTag:@"locationImageView"];
        UILabel *subTitleLabel = (UILabel *)[cellWithSecThr viewWithStringTag:@"subTitleLabel"];
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"送至"]){
            subTitleLabel.hidden = NO;
            locationImageView.hidden = NO;
            if (self.productDetailModel.deliveryTime.arriveTime){
//                NSString *beginTime = self.productDetailModel.deliveryTime.beginTime.length?self.productDetailModel.deliveryTime.beginTime:@"/";
//                NSString *endTime = self.productDetailModel.deliveryTime.endTime.length?self.productDetailModel.deliveryTime.endTime:@"/";
                NSString *arriveTime = self.productDetailModel.deliveryTime.arriveTime.length?self.productDetailModel.deliveryTime.arriveTime:@"/";
                NSString *arriveDate = @"";
                if (self.productDetailModel.deliveryTime.arriveType == 0){
                    arriveDate = @"今日的";
                } else if (self.productDetailModel.deliveryTime.arriveType == 1){
                    arriveDate = @"次日的";
                } else {
                    arriveDate = [NSString stringWithFormat:@"两天后的"];
                }
                subTitleLabel.attributedText = [MMHTool rangeLabelWithContent:[NSString stringWithFormat:@"下单可在%@%@点送达",arriveDate,arriveTime] hltContentArr:@[arriveDate,arriveTime] hltColor:[UIColor colorWithCustomerName:@"粉"] normolColor:[UIColor colorWithCustomerName:@"浅灰"]];
            } else {
                subTitleLabel.text = @"";
//                self.productDetailModel.shopIdentify.length?@"该商品为门店发货":@"该商品为仓储发货";
                subTitleLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            }
        } else {
            subTitleLabel.hidden = YES;
        }
        
        UILabel *dymicLabel = (UILabel *)[cellWithSecThr viewWithStringTag:@"dymicLabel"];
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"送至"]){
            NSString *receiptAddress = @"";
            locationImageView.hidden = YES;
            MMHAddressSingleModel *addressSingleModel = [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel;
            if (addressSingleModel){
                if (addressSingleModel.province.length){
                    receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@>",addressSingleModel.province]];
                }
                if (addressSingleModel.city.length){
                    if (addressSingleModel.area.length){
                        receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@>%@",addressSingleModel.city,addressSingleModel.area]];
                    } else {
                        receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@",addressSingleModel.city]];
                    }
                }
            } else {
                if ([MMHCurrentLocationModel sharedLocation].province.length){
                    receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@>",[MMHCurrentLocationModel sharedLocation].province]];
                }
                if ([MMHCurrentLocationModel sharedLocation].city.length){
                    if ([MMHCurrentLocationModel sharedLocation].district.length){
                        receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@>%@",[MMHCurrentLocationModel sharedLocation].city,[MMHCurrentLocationModel sharedLocation].district]];
                    } else {
                        receiptAddress = [receiptAddress stringByAppendingString:[NSString stringWithFormat:@"%@",[MMHCurrentLocationModel sharedLocation].city]];
                    }
                }
            }
            dymicLabel.text = receiptAddress;
            CGFloat addressWidth = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:dymicLabel.font])].width;
            dymicLabel.size_width = addressWidth;
            locationImageView.frame = CGRectMake(CGRectGetMaxX(dymicLabel.frame) + MMHFloat(5), dymicLabel.orgin_y, MMHFloat(13), MMHFloat(17));
            dymicLabel.size_height = [MMHTool contentofHeight:F4];
            locationImageView.hidden = NO;
            fixedLabel.hidden = NO;
            dymicLabel.hidden = NO;
            cellBgView.hidden = YES;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"运费"]){
            locationImageView.hidden = YES;
            dymicLabel.text = [NSString stringWithFormat:@"订单总额不满%.2f元收取运费%.2f元",self.productDetailSuperModel.deliveryTime.mailPriceTerm,self.productDetailSuperModel.deliveryTime.mailPrice];
            dymicLabel.orgin_y = fixedLabel.orgin_y;
            dymicLabel.size_height = [MMHTool contentofHeight:F4];
            fixedLabel.hidden = NO;
            dymicLabel.hidden = NO;
            cellBgView.hidden = YES;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"服务"]){
            locationImageView.hidden = YES;
            dymicLabel.text = [NSString stringWithFormat:@"由 %@ 发货并提供售后服务",self.productDetailSuperModel.deliveryTime.shop.length?self.productDetailSuperModel.deliveryTime.shop:@"妈妈好"];
            dymicLabel.numberOfLines = 0;
            CGSize shopCustomSize = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(11) - CGRectGetMaxX(fixedLabel.frame) - MMHFloat(10), CGFLOAT_MAX)];
            dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10), fixedLabel.orgin_y,kScreenBounds.size.width - MMHFloat(11) - CGRectGetMaxX(fixedLabel.frame) - MMHFloat(10) , shopCustomSize.height);
            
            fixedLabel.hidden = NO;
            dymicLabel.hidden = NO;
            cellBgView.hidden = YES;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"服务标志"]){
            locationImageView.hidden = YES;
            if (!cellBgView.subviews.count){
                fixedLabel.hidden = YES;
                dymicLabel.hidden = YES;
                cellBgView.hidden = NO;
                CGFloat flagWidth = kScreenBounds.size.width - 2 * MMHFloat(10) - CGRectGetMaxX(fixedLabel.frame);
                CGFloat flagOriginX = CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10);
                CGFloat flagOriginY = 0;
                for (int i = 0 ; i < self.productDetailSuperModel.goodsTag.count;i++){
                    UIView *flagImageView = [self createServerFlagViewWithIndex:i];
                    if (flagImageView.size_width < flagWidth){                      // 不换行
                        flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
                        flagWidth -= (flagImageView.size_width + MMHFloat(10));
                        flagOriginX += (flagImageView.size_width + MMHFloat(10));
                    } else {
                        flagOriginY += MMHFloat(17) + MMHFloat(5);
                        flagOriginX = CGRectGetMaxX(fixedLabel.frame) + MMHFloat(10);
                        flagWidth = kScreenBounds.size.width - 2 * MMHFloat(10) - CGRectGetMaxX(fixedLabel.frame);
                        flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
                        flagWidth -= (flagImageView.size_width + MMHFloat(10));
                        flagOriginX += (flagImageView.size_width + MMHFloat(10));
                    }
                    [cellBgView addSubview:flagImageView];
                }
            }
        }
        return cellWithSecThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图文详情"]){
        static NSString *cellIdentifyWithSecFour = @"cellIdentifyWithSecFour";
        UITableViewCell *cellWithSecFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecFour];
        if (!cellWithSecFour){
            cellWithSecFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecFour];
            cellWithSecFour.selectionStyle = UITableViewCellSelectionStyleNone;
            
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            
            // 创建image
            UIImageView *logoImageView = [[UIImageView alloc]init];
            logoImageView.backgroundColor = [UIColor clearColor];
            logoImageView.frame = CGRectMake(MMHFloat(11), (cellHeight - MMHFloat(25)) / 2., MMHFloat(25), MMHFloat(25));
            logoImageView.stringTag = @"logoImageView";
            [cellWithSecFour addSubview:logoImageView];
            
            // 创建Fixedlabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(logoImageView.frame) + MMHFloat(10), 0, 200, cellHeight);
            [cellWithSecFour addSubview:fixedLabel];
            
            cellWithSecFour.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tool_arrow"]];
        }
        // 赋值
        // 1. imageView
        UIImageView *logoImageView = (UIImageView *)[cellWithSecFour viewWithStringTag:@"logoImageView"];
        
        // 2. fixedLabel
        UILabel *fixedLabel = (UILabel *)[cellWithSecFour viewWithStringTag:@"fixedLabel"];
        fixedLabel.text = [[self.productMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"质检担保"]){
            logoImageView.image = [UIImage imageNamed:@"product_guarantee"];
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"图文详情"]){
            logoImageView.image = [UIImage imageNamed:@"product_parameters"];
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"商品参数"]){
            logoImageView.image = [UIImage imageNamed:@"product_picturedetails"];
            fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        }
        return cellWithSecFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品口碑"]){               // 【商品口碑】
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSecFiv = @"cellIdentifyWithSecFiv";
            UITableViewCell *cellWithSectionFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecFiv];
            cellWithSectionFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            
            if (!cellWithSectionFiv){
                cellWithSectionFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecFiv];
                cellWithSectionFiv.selectionStyle = UITableViewCellSelectionStyleGray;
                
                CGSize cellOfSize = [tableView rectForRowAtIndexPath:indexPath].size;
                //
                UILabel *publicPraiseLabel = [[UILabel alloc]init];
                publicPraiseLabel.stringTag = @"publicPraiseLabel";
                publicPraiseLabel.backgroundColor = [UIColor clearColor];
                publicPraiseLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
                publicPraiseLabel.textAlignment = NSTextAlignmentLeft;
              //  publicPraiseLabel.textColor = [UIColor hexChangeFloat:@"b8b8b8"];
                publicPraiseLabel.frame = CGRectMake(MMHFloat(11), 0, 200, cellOfSize.height);
                [cellWithSectionFiv addSubview:publicPraiseLabel];
                
                cellWithSectionFiv.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tool_arrow"]];
            }
            // 赋值
            UILabel *publicPraiseLabel = (UILabel *)[cellWithSectionFiv viewWithStringTag:@"publicPraiseLabel"];
            
            publicPraiseLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            publicPraiseLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            NSInteger commentCount = [self.WomMouthDic[@"commentCount"] integerValue];
            NSString *percentRate = self.WomMouthDic[@"percentRate"];
            NSString *str1 = [NSString stringWithFormat:@"商品口碑(%ld 好评 %@)",(long)commentCount,percentRate];
           NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:str1];
            NSRange range = NSMakeRange(str1.length-percentRate.length-1, percentRate.length);
            UIColor *color= [UIColor colorWithCustomerName:@"红"];
            [str addAttribute:NSForegroundColorAttributeName value:color range:range];
            publicPraiseLabel.attributedText = str;
            return cellWithSectionFiv;
        } else {
            static NSString *cellIdentifyWithPublicPraise = @"cellIdentifyWithPublicPraise";
            MMHWordOfMouthTableViewCell *cellWithPublicPraiseCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithPublicPraise];
            if (!cellWithPublicPraiseCell){
                cellWithPublicPraiseCell = [[MMHWordOfMouthTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithPublicPraise];
                cellWithPublicPraiseCell.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithPublicPraiseCell.type = MMHWordOfMouthTableViewCellTypeOfGoodsDetail;
            }
            
            
            if (self.womDataArray.count == 2) {
                if (indexPath.row == 1) {
                    cellWithPublicPraiseCell.frameModel = self.womDataArray[0];
                }
                if (indexPath.row == 2) {
                    cellWithPublicPraiseCell.frameModel = self.womDataArray[1];
                }
            }else if (self.womDataArray.count ==1){
                cellWithPublicPraiseCell.frameModel = self.womDataArray[0];
            }else{
              //
             }
            return cellWithPublicPraiseCell;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"猜你喜欢"]){
        static NSString *cellIdentifyWithSecSix = @"cellIdentifyWithSecSix";
        MMHGuessYouLikeCell *cellWithSecSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecSix];
        if (!cellWithSecSix){
            cellWithSecSix = [[MMHGuessYouLikeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecSix];
            cellWithSecSix.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithSecSix.guessYouLikeListModel = self.guessYouLikeListModel;
        __weak typeof(self) weakSelf = self;
        cellWithSecSix.guessYouLikeBlock = ^(MMHGuessYouLikeSingleModel *guessYouLikeSingleModel){
            [weakSelf guessYouLikeInterfaceWith:guessYouLikeSingleModel];
        };
        return cellWithSecSix;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"门店"]){
        static NSString *cellIdentifyWithSecSev = @"cellIdentifyWithSecSev";
        MMHProductDetailStoreCell *cellWithSecSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecSev];
        if (!cellWithSecSev){
            cellWithSecSev = [[MMHProductDetailStoreCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecSev];
            cellWithSecSev.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithSecSev.shopInfo = self.productDetailSuperModel.shopInfo;
        
        return cellWithSecSev;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        UITableViewCell *cellWithOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithOther){
            cellWithOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            cellWithOther.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithOther.textLabel.text = [[self.productMutableArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithOther;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"妈妈说"] && indexPath.row == [self cellIndexPathRowWithcellData:@"妈妈说"]){
        [MMHLogbook logEventType:MMHBuriedPointTypeProductMotherSay];
        self.mamCareIsOpen = !self.mamCareIsOpen;
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"图文详情"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"质检担保"]){                            // 质监担保
            MMHDanbaoViewController *danbaoViewController = [[MMHDanbaoViewController alloc]init];
            danbaoViewController.transferDanbaoArr = self.productDetailSuperModel.qualityPic;
            [self.navigationController pushViewController:danbaoViewController animated:YES];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"商品参数"]){                     // 商品参数
            MMHWordOfMouthViewController *womVC = [[MMHWordOfMouthViewController alloc] initWithContentType:MMHWordOfMouthViewControllerContentTypeProductSpecs];
            womVC.goodsTemplateId = self.productDetailModel.templateId;
        
            [self.navigationController pushViewController:womVC animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeProductParameter];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"图文详情"]){                     // 图文详情
            MMHWordOfMouthViewController *womVC = [[MMHWordOfMouthViewController alloc] initWithContentType:MMHWordOfMouthViewControllerContentTypeProductDetailHTML];
            womVC.goodsTemplateId = self.productDetailModel.templateId;
    
            [self.navigationController pushViewController:womVC animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeProductImgTextDetail];
        }
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品口碑"]){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        if (indexPath.row == 0) {
            MMHWordOfMouthViewController *wordOfMouthViewController = [[MMHWordOfMouthViewController alloc]initWithContentType:MMHWordOfMouthViewControllerContentTypeWordOfMouth];
            wordOfMouthViewController.goodsTemplateId = self.productDetailModel.templateId;
            
            [self.navigationController pushViewController:wordOfMouthViewController animated:YES];
            [MMHLogbook logEventType:MMHBuriedPointTypeProductPraise];
        }
    }else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"服务标志"]) && (indexPath.row == [self cellIndexPathRowWithcellData:@"服务标志"])){
        CGFloat xWidth = self.view.bounds.size.width - 70.0f;
        CGFloat yHeight = 272.0f;
        CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
        MMHPopView *poplistview = [[MMHPopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
        poplistview.goodsTag = self.productDetailSuperModel.goodsTag;
        poplistview.usingType = MMHUsingTypeProductGuarantees;
        [poplistview viewShow];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"送至"] && indexPath.row == [self cellIndexPathRowWithcellData:@"送至"]){
        __weak typeof(self)weakSelf = self;
        MMHChangeAddressViewController *changeAddressViewController = [[MMHChangeAddressViewController alloc]init];
        changeAddressViewController.changeAddressPageUsingType = MMHChangeAddressPageUsingTypeProduct;
        changeAddressViewController.changedAddressBlock = ^ (MMHAddressSingleModel *addressSingleModel){
            weakSelf.addressSingleModel = addressSingleModel;
            [MMHCurrentLocationModel sharedLocation].selectedAddressSingleModel = addressSingleModel;
            [weakSelf sendRequestWithGetProductDetail];         // 数据请求
        };
        [weakSelf.navigationController pushViewController:changeAddressViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"门店"]){
//        [MMHLogbook logEventType:MMHBuriedPointTypeProductShop];
//        MMHProductDetailShopProduct *product = [[MMHProductDetailShopProduct alloc] initWithProductDetail:self.productDetailModel];
//        MMHProductDetailViewController1 *productDetailViewController = [[MMHProductDetailViewController1 alloc] initWithProduct:product];
//        [self pushViewController:productDetailViewController];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头部图片"] && indexPath.row == [self cellIndexPathRowWithcellData:@"头部图片"]){
        return MMHFloat(375);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品价格"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品价格"]){
       return [MMHProductDetailPriceCell cellHeight:self.productDetailSuperModel.productDetailPriceModel type:self.productDetailSaleType];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"商品介绍"] && indexPath.row == [self cellIndexPathRowWithcellData:@"商品介绍"]){
        return [MMHProductDetailIntroductionCell contentOfheight:self.productDetailSuperModel.productDetailIntroductionModel];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"妈妈说"] && indexPath.row == [self cellIndexPathRowWithcellData:@"妈妈说"]){
        return [MMHProductDetailMomSayCell contentOfHeightWithMamSayModel:self.productDetailSuperModel.mamSayModel andMamCareIsOpen:self.mamCareIsOpen];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"送至"]){
        if (indexPath.row == 0){
            if (self.productDetailModel.deliveryTime.arriveTime){
                return MMHFloat(15) + [MMHTool contentofHeight:F4] + MMHFloat(5) + [MMHTool contentofHeight:[UIFont fontWithCustomerSizeName:@"小提示"]] ;
            } else {
                return MMHFloat(34);
            }
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"服务"]){
            CGSize fixedContentOfSize = [@"运费" sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:F4])];
            NSString *dymicString = [NSString stringWithFormat:@"由 %@ 发货并提供售后服务",self.productDetailSuperModel.deliveryTime.shop.length?self.productDetailSuperModel.deliveryTime.shop:@"妈妈好"];
            CGSize shopCustomSize = [dymicString sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(kScreenBounds.size.width - MMHFloat(10) - fixedContentOfSize.width - MMHFloat(16) - MMHFloat(11), CGFLOAT_MAX)];
            return shopCustomSize.height +(MMHFloat(34) - [MMHTool contentofHeight:F4]);
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"运费"]){
            return MMHFloat(34);
        } else {
            return MMHFloat(34);
        }
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"猜你喜欢"]){
        return [MMHGuessYouLikeCell contentOfHeight];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"门店"]){
        return [MMHProductDetailStoreCell cellHeightWithShopInfo:self.productDetailSuperModel.shopInfo];
    }else if(indexPath.section == [self cellIndexPathSectionWithcellData:@"商品口碑"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"商品口碑"]) {
            return MMHFloat(44);
        }else if(indexPath.row == [self cellIndexPathRowWithcellData:@"商品评价1"]){
            MMHPublicPraiseFrameModel *model = self.womDataArray[0];
            return model.cellHeightF;
        }else if(indexPath.row == [self cellIndexPathRowWithcellData:@"商品评价2"]){
            MMHPublicPraiseFrameModel *model = self.womDataArray[1];
            return model.cellHeightF;
        }else{
            return 44;
        }
    }else {
        return 44;
    }

}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return MMHFloat(20);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == self.productMutableArray.count - 1){
        return MMHFloat(20);
    } else {
        return 0;
    }
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    return footerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.productDetailTableView) {
        if (indexPath.section != [self cellIndexPathSectionWithcellData:@"送至"]){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.productMutableArray objectAtIndex:indexPath.section] count] - 1) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([[self.productMutableArray objectAtIndex:indexPath.section] count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}


#pragma mark - CustomerView
-(void)createFloatView{
    // 1. 创建背景view
    self.floatView = [[UIView alloc]init];
    self.floatView.backgroundColor = [UIColor hexChangeFloat:@"fafafa"];
    self.floatView.frame = CGRectMake(0 , kScreenBounds.size.height - MMHFloat(49) - 64 , kScreenBounds.size.width, MMHFloat(49));
    [self.view addSubview:self.floatView];
    
    // 2. 创建lineView
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, .5f);
    [self.floatView addSubview:lineView];
    
    // 创建客服按钮
    UIButton *customerServiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customerServiceButton= [UIButton buttonWithType:UIButtonTypeCustom];
    customerServiceButton.frame = CGRectMake(0,0 ,100,MMHFloat(49));
    [customerServiceButton addTarget:self action:@selector(serversButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    customerServiceButton.backgroundColor = [UIColor clearColor];
    NSString *detailString = @"联系客服";
    customerServiceButton.stringTag = @"customerServiceButton";
    [customerServiceButton setImage:[UIImage imageNamed:@"order_free_chat_typing"] forState:UIControlStateNormal];
    customerServiceButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    customerServiceButton.adjustsImageWhenHighlighted = NO;
    [customerServiceButton setTitle:detailString forState:UIControlStateNormal];
    customerServiceButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [customerServiceButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [self.floatView addSubview:customerServiceButton];
    self.contactUsButton = customerServiceButton;
    
    // 创建加入购物车
    UIButton *shoppingCatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self createFloatButtonWithTitleWithButton:shoppingCatButton buttonIndx:0];
    self.addToCartButton = shoppingCatButton;
    
    // 创建立即购买
    UIButton *buyNowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self createFloatButtonWithTitleWithButton:buyNowButton buttonIndx:1];
    self.buyNowButton = buyNowButton;
}

//  创建浮层Button
-(void)createFloatButtonWithTitleWithButton:(UIButton *)button buttonIndx:(NSInteger)buttonIndex{
    
    NSString *title = @"";
    UIColor *titleColor = [[UIColor alloc]init];
    UIColor *backgroundColor = [[UIColor alloc]init];
    if (buttonIndex == 0){                  // 【加入购物车】
        title = @"加入购物车";
        titleColor = [UIColor hexChangeFloat:@"666666"];
        backgroundColor = [UIColor yellowColor];
        button.stringTag = @"shoppingCatButton";
        button.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
        button.layer.borderWidth = .5f;
        button.frame = CGRectMake(kScreenBounds.size.width - 2 * (MMHFloat(11) + MMHFloat(125)), MMHFloat(4), MMHFloat(125), MMHFloat(40));
        self.addToCartButton = button;
        if (self.productDetailSaleType != MMHProductDetailSaleTypeRMB){
            self.addToCartButton.hidden = YES;
        }
    } else if (buttonIndex == 1){           // 【立即购买】
        title = @"立即购买";
        titleColor = [UIColor colorWithCustomerName:@"白"];
        button.stringTag = @"buyNowButton";
        button.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
        button.frame = CGRectMake(kScreenBounds.size.width - MMHFloat(11) - MMHFloat(125),MMHFloat(4),MMHFloat(125),MMHFloat(40));
        self.buyNowButton = button;
    }
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:16.];
    [button addTarget:self action:@selector(serversButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    button.layer.cornerRadius = MMHFloat(10.f);
    [self.floatView addSubview:button];
}

//  创建failView
-(void)createFailViewWithFailType:(MMHProductDetailFail)productDetailFailType{
    NSString *failTitleString = @"";
    NSString *failSubTitleString = @"";
    if (productDetailFailType != MMHProductDetailFailNormal){
        self.buyNowButton.backgroundColor = [UIColor hexChangeFloat:@"CBCCCD"];
        self.buyNowButton.enabled = NO;
        self.addToCartButton.hidden = YES;
        
        if (productDetailFailType == MMHProductDetailFailNoProduct){            // 没有商品了
            failTitleString = @"不好意思，您来晚了商品已经被抢光了!";
        } else if (productDetailFailType == MMHProductDetailFailMbeanNotEnough){    // 妈豆不足
            failTitleString = @"您的妈豆不足，";
            failSubTitleString = @"如何获得更多妈豆?";
            
        } else if (productDetailFailType == MMHProductDetailFailActivieyEnd){       // 活动结束
            failTitleString = @"抱歉，您来晚了，活动已结束！";
        } else if (productDetailFailType == MMHProductDetailFailNotStartedYet){
            failTitleString = @"活动还未开始,敬请期待!";
        }
        
        self.failAlphaView = [[UIView alloc]init];
        self.failAlphaView.backgroundColor = [UIColor hexChangeFloat:@"46b5ba"];
        self.failAlphaView.alpha = .9f;
        [self.view addSubview:self.failAlphaView];
        
        // 创建failLabel
        UILabel *failLabel = [[UILabel alloc]init];
        failLabel.textColor = [UIColor whiteColor];
        failLabel.font = F4;
        failLabel.text = failTitleString;
        [self.failAlphaView addSubview:failLabel];
        
        // 创建button
        MMHDeleteLineLabel *failLineLabel = [[MMHDeleteLineLabel alloc]init];
        failLineLabel.backgroundColor = [UIColor clearColor];
        failLineLabel.textColor = [UIColor whiteColor];
        failLineLabel.font = F4;
        failLineLabel.isUnderLine = YES;
        failLineLabel.text = failSubTitleString;
        failLineLabel.strikeThroughEnabled = YES;
        failLineLabel.strikeThroughColor = [UIColor whiteColor];
        [self.failAlphaView addSubview:failLineLabel];
        self.failAlphaView.frame = CGRectMake(0, CGRectGetMinY(self.floatView.frame) - MMHFloat(30), kScreenBounds.size.width, MMHFloat(30));
        [self.failAlphaView addSubview:failLabel];
        
        if (failSubTitleString.length){
            failLabel.textAlignment = NSTextAlignmentLeft;
            CGSize tempSize = [failTitleString sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(30))];
            CGSize tempSubSize = [failSubTitleString sizeWithCalcFont:F4 constrainedToSize:CGSizeMake(CGFLOAT_MAX, MMHFloat(30))];

            CGFloat originX = (kScreenBounds.size.width - 2 * MMHFloat(11) - tempSize.width - tempSubSize.width) / 2.;
            failLabel.frame = CGRectMake(originX, 0, tempSize.width, MMHFloat(30));
            failLineLabel.frame = CGRectMake(CGRectGetMaxX(failLabel.frame), 0, tempSubSize.width, MMHFloat(30));
        } else {
            failLabel.textAlignment = NSTextAlignmentCenter;
            failLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * MMHFloat(11), MMHFloat(30));
        }
        
        //  创建按钮
        UIButton *failButton = [UIButton buttonWithType:UIButtonTypeCustom];
        failButton.backgroundColor = [UIColor clearColor];
        [failButton addTarget:self action:@selector(failButtonClick) forControlEvents:UIControlEventTouchUpInside];
        failButton.frame = self.failAlphaView.bounds;
        [self.failAlphaView addSubview:failButton];
        
        // 设置tableView size
        self.productDetailTableView.size_height -= self.failAlphaView.size_height;
    }
}

// 创建7天退货等button
-(UIView *)createServerFlagViewWithIndex:(NSInteger)imageViewIndex{
    MMHProductDetailGoodsTagModel *productDetailGoodsTagModel = [self.productDetailSuperModel.goodsTag objectAtIndex:imageViewIndex];
    UIView *flagBackgroundImageView = [[UIView alloc]init];
    flagBackgroundImageView.backgroundColor = [UIColor clearColor];
    
    // 创建imageView
    MMHImageView *flagImageView = [[MMHImageView alloc]init];
    flagImageView.backgroundColor = [UIColor clearColor];
    flagImageView.frame = CGRectMake(0, 0, MMHFloat(17), MMHFloat(17));
    flagImageView.stringTag = [NSString stringWithFormat:@"flagImageView%li",(long)imageViewIndex];
    [flagImageView updateViewWithImageAtURL:productDetailGoodsTagModel.pic];
    [flagBackgroundImageView addSubview:flagImageView];
    
    // 创建文字
    UILabel *flagLabel = [[UILabel alloc]init];
    flagLabel.backgroundColor = [UIColor clearColor];
    flagLabel.font = F2;
    flagLabel.textColor = C4;
    flagLabel.text = productDetailGoodsTagModel.view;
    CGSize flagLabelSize = [flagLabel.text sizeWithCalcFont:flagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [MMHTool contentofHeight:flagLabel.font])];
    flagLabel.frame = CGRectMake(CGRectGetMaxX(flagImageView.frame) + MMHFloat(4), 0, flagLabelSize.width, flagImageView.size_height);
    [flagBackgroundImageView addSubview:flagLabel];
    
    flagBackgroundImageView.bounds = CGRectMake(0, 0, flagImageView.size_width + flagLabel.size_width, flagImageView.size_height);
    return flagBackgroundImageView;
}


- (void)setActionButtonsEnabled:(BOOL)enabled {
    self.contactUsButton.enabled = enabled;
    self.addToCartButton.enabled = enabled;
    self.buyNowButton.enabled = enabled;
}



#pragma mark - ActionClick
-(void)serversButtonClick:(UIButton *)sender{
    UIButton *button = (UIButton *)sender;
    if ([button.stringTag isEqualToString:@"shoppingCatButton"]){// 【加入购物车】
        __weak typeof(self) weakSelf = self;
        MMHProductSpecSelectionViewController *specSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.productDetailModel specSelectedHander:^ {
            [weakSelf addToCart];
            [MMHLogbook logEventType:MMHBuriedPointTypeAddToCart];
        }];
        specSelectionViewController.shouldShowAddingToCartAnimation = YES;
        MMHTabBarController *tabBarController = (MMHTabBarController *) weakSelf.tabBarController;
        [tabBarController presentFloatingViewController:specSelectionViewController animated:YES];
    } else if ([button.stringTag isEqualToString:@"buyNowButton"]){        // 【立即购买】
        if (![[MMHAccountSession currentSession] alreadyLoggedIn]) {
            [self presentLoginViewControllerWithSucceededHandler:^() {
                
            }];
            return;
        }
        
        __weak typeof(self) weakSelf = self;
        MMHProductSpecSelectionViewController *specSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.productDetailModel specSelectedHander:^ {
            [MMHLogbook logEventType:MMHBuriedPointTypeBuyNow];
            [MMHLogbook logEventType:MMHBuriedPointTypeIsToConfirmation];
            [weakSelf buyNow];
        }];
        specSelectionViewController.shouldShowAddingToCartAnimation = NO;
        MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
        [tabBarController presentFloatingViewController:specSelectionViewController animated:YES];
    } else if ([button.stringTag isEqualToString:@"customerServiceButton"]){        // 联系客服
        [MMHLogbook logEventType:MMHBuriedPointTypeProductLikeCustomer];
        [self linkToCustomer];
    }
}

// 联系客服
-(void)linkToCustomer{
    [self startChattingWithContext:self.productDetailModel];
}

// 妈豆有神马用
-(void)howUseButtonClick{
    MMHWebViewController * smartVC = [[MMHWebViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:MMHHTML_Link_Beans];
    [self.navigationController pushViewController:smartVC animated:YES];
}

// 错误条点击效果
-(void)failButtonClick{
    if (self.productDetailFail == MMHProductDetailFailMbeanNotEnough){
        MMHWebViewController * smartVC = [[MMHWebViewController alloc]initWithResourceName:@"" title:@"" isRemote:YES url:MMHHTML_Link_Beans];
        [self.navigationController pushViewController:smartVC animated:YES];
    }
}

// 加入购物车
- (void)addToCart {
    ProductSpecParameter *selectedSpecParameter = [self.productDetailModel.specFilter selectedSpecParameter];
    if (selectedSpecParameter == nil) {
        [self.view showTips:@"无法加入购物车，请检查网络"];
        return;
    }
    
    [self.view showProcessingViewWithMessage:@"正在加入购物车"];
    __weak __typeof(self) weakSelf = self;
    
    [[MMHNetworkAdapter sharedAdapter] addProductToCart:self.productDetailModel succeededHandler:^{
        [weakSelf.rightNavButton setImage:[UIImage imageNamed:@"product_icon_shop_hlt"] forState:UIControlStateNormal];
        [weakSelf.rightNavButton sizeToFit];
        [weakSelf.view hideProcessingView];
        [weakSelf.view showTips:@"已加入购物车"];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        [weakSelf.view showTipsWithError:error];
    }];
}


// 立即购买
- (void)buyNow {
    if (!self.productDetailModel.itemId.length) {
        [self.view showTips:@"稍微等一下嘛，让页面刷一会"];
        return;
    }

    NSMutableDictionary *jsonTerm = [NSMutableDictionary dictionary];
    jsonTerm[@"itemId"] = self.productDetailModel.itemId;
    jsonTerm[@"templateId"] = self.productDetailModel.templateId;
    [jsonTerm setNullableObject:self.productDetailModel.shopId forKey:@"shopId"];
    [jsonTerm setNullableObject:self.productDetailModel.companyId forKey:@"companyId"];
    [jsonTerm setNullableObject:self.productDetailModel.warehouseId forKey:@"warehouseId"];
    jsonTerm[@"quantity"] = @(self.productDetailModel.specFilter.quantity);
    jsonTerm[@"isBindShop"] = self.productDetailModel.bindsShop ? @"1" : @"0";
    
    if (self.productDetailModel.isBeanProduct) {
        jsonTerm[@"price"] = @0;
    }
    MMHConfirmationOrderViewController *confirmationOrderViewController = [[MMHConfirmationOrderViewController alloc] initWithProductJSONTerm:jsonTerm bindsShop:self.productDetailModel.bindsShop inlet:(self.productDetailModel.isBeanProduct ? MMHConfirmationRedirectionTypeMbean : MMHConfirmationRedirectionTypeDirect)];
    confirmationOrderViewController.MBeanPayCount = self.productDetailSuperModel.productDetailPriceModel.mBeanPay;
    [self pushViewController:confirmationOrderViewController];
}


#pragma mark - 接口
// 获取商品详情
-(void)sendRequestWithGetProductDetail {
    [self.view showProcessingView];
    __weak typeof(self )weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailWithProduct:self.product from:self succeededHandler:^(MMHProductDetailModel *productDetail) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.view hideProcessingView];
        strongSelf.productDetailModel = productDetail;
        productDetail.delegate = strongSelf;
        NSArray *tempArr = @[@[@"头部图片",@"商品价格",@"商品介绍"],@[@"妈妈说"],@[@"送至",@"运费",@"服务"]];
        if (strongSelf.productMutableArray.count){
            [strongSelf.productMutableArray removeAllObjects];
        }
        [strongSelf.productMutableArray addObjectsFromArray:tempArr];
        if (strongSelf.productDetailSaleType == MMHProductDetailSaleTypeRMB){                             // 人民币商品
            [strongSelf.productMutableArray replaceObjectAtIndex:[strongSelf cellIndexPathSectionWithcellData:@"服务"] withObject:@[@"送至",@"运费",@"服务",@"服务标志"]];
        }
        strongSelf.productDetailSuperModel = [MMHProductDetailDataSourceManager conversionWithProductDetailModel:productDetail];
        // 如果有店铺
        if (strongSelf.productDetailSuperModel.shopInfo.shopId){
            [strongSelf.productMutableArray addObject:@[@"门店"]];
        }
        NSMutableArray *expressMutableArr = [NSMutableArray array];
        [expressMutableArr addObjectsFromArray:@[@"送至",@"服务"]];
        if ((strongSelf.productDetailSuperModel.goodsTag.count) && (strongSelf.productDetailSaleType == MMHProductDetailSaleTypeRMB)){
            [expressMutableArr addObject:@"服务标志"];
        }
        if (strongSelf.productDetailSuperModel.deliveryTime.mailPrice > 0){
            [expressMutableArr insertObject:@"运费" atIndex:1];
        }
        [strongSelf.productMutableArray replaceObjectAtIndex:[strongSelf cellIndexPathSectionWithcellData:@"服务"] withObject:expressMutableArr];
        
        NSInteger productIndex = [strongSelf cellIndexPathSectionWithcellData:@"送至"];
        if (strongSelf.productDetailSuperModel.qualityPic.count){
            [strongSelf.productMutableArray insertObject:@[@"质检担保",@"图文详情",@"商品参数"] atIndex:productIndex + 1];
        } else {
            [strongSelf.productMutableArray insertObject:@[@"图文详情",@"商品参数"] atIndex:productIndex + 1];
        }
        
        [strongSelf.productDetailTableView reloadData];
        [strongSelf sendRequestWithGuessYouLike];                             // 获取猜你喜欢
        [strongSelf fetchGoodsPraise];                                        // 获取口碑
        [strongSelf setActionButtonsEnabled:YES];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (kNoneNetworkReachable){
            [strongSelf.view showPrompt:@"网络在沉睡，先唤醒再试试~" withImage:[UIImage imageNamed:@"center_picture_box"] andImageAlignment:MMHPromptImageAlignmentTop];
        } else {
            [strongSelf showBlankViewOfType:MMHBlankViewTypeProductDetail belowView:nil];
            if ([error.localizedDescription isEqualToString:@"系统异常"]){
                [strongSelf.view showTips:@"亲爱的妈妈，该商品已售罄"];
            } else {
                [strongSelf.view showTipsWithError:error];
            }
        }
    }];
}

//  猜你喜欢
-(void)sendRequestWithGuessYouLike{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailGuessYouLikeWithProduct:self.product from:self succeededHandler:^(MMHGuessYouLikeListModel *guessYouLikeListModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.guessYouLikeListModel = guessYouLikeListModel;
        if (strongSelf.guessYouLikeListModel.data.count){
            if ([strongSelf cellIndexPathSectionWithcellData:@"猜你喜欢"] == -1){
                [strongSelf.productMutableArray addObject:@[@"猜你喜欢"]];
                NSInteger guessYouLikeIndex = [strongSelf cellIndexPathSectionWithcellData:@"猜你喜欢"];
                NSIndexSet *wordOfMouthSet = [NSIndexSet indexSetWithIndex:guessYouLikeIndex];
                [strongSelf.productDetailTableView insertSections:wordOfMouthSet withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
    } failedHandler:^(NSError *error) {
        
    }];
}


-(void)guessYouLikeInterfaceWith:(MMHGuessYouLikeSingleModel *)singleModel{
    MMHProductDetailViewController1 * productDetailViewController = [[MMHProductDetailViewController1 alloc]initWithProduct:singleModel];
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

//  获取当前定位地址
-(void)currentLocation{
    __weak typeof(self)weakSelf = self;
    if ([MMHCurrentLocationModel sharedLocation].city.length){
        [weakSelf sendRequestWithGetProductDetail];
    } else {
        [MMHLocationManager getArea];
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(currentLocationUpdate) name:MMHCurrentLocationNotification object:nil];
    }
}

- (void)fetchGoodsPraise{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchPublicPrasieWithTemplateId:self.product.templateId page:1 pageSize:2 from:nil succeededHandler:^(NSDictionary *dic) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        strongSelf.WomMouthDic = dic;
        NSArray *array = dic[@"rows"];
        NSInteger commentCount = [strongSelf.WomMouthDic[@"commentCount"] integerValue];
        if (commentCount == 0) {
            
        }
        else {
            for (NSDictionary *dic in array) {
                MMHPublicPraiseFrameModel *frameModel = [[MMHPublicPraiseFrameModel alloc] init];
                MMHPublicPraiseModel *praiseModel = [[MMHPublicPraiseModel alloc] initWithJSONDict:dic];
                frameModel.praiseModel = praiseModel;
                [strongSelf.womDataArray addObject:frameModel];
            }
            if ([strongSelf cellIndexPathSectionWithcellData:@"商品口碑"] == -1){
                NSInteger resultCount = [[strongSelf.WomMouthDic  objectForKey:@"resultCount"] integerValue];
                if (resultCount == 1) {
                    [strongSelf.productMutableArray addObject:@[@"商品口碑", @"商品评价1"]];
                }
                else {
                    [strongSelf.productMutableArray addObject:@[@"商品口碑", @"商品评价1", @"商品评价2"]];
                }
                NSInteger wordOfMouthSetIndex = [strongSelf cellIndexPathSectionWithcellData:@"商品口碑"];
                NSIndexSet *wordOfMouthSet = [NSIndexSet indexSetWithIndex:wordOfMouthSetIndex];
                if (wordOfMouthSet != nil) {
                    [strongSelf.productDetailTableView insertSections:wordOfMouthSet withRowAnimation:UITableViewRowAnimationLeft];
                }
            }

           
        }
        
    } failedHandler:^(NSError *error) {
        //
    }];
}
// 当前地址更新
-(void)currentLocationUpdate{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestWithGetProductDetail];
}


#pragma mark - ForBeanProducts
- (void)tryToUpdateViewsForBeanProduct {
    if (self.product == nil) {
        return;
    }
    if (self.productDetailModel == nil) {
        return;
    }
    if (![self.product isBeanProduct]) {
        return;
    }
    if (![self.product isKindOfClass:[MMHMamhaoBeanSpecialProduct class]]) {
        return;
    }
    
    MMHMamhaoBeanSpecialProduct *beanSpecialProduct = (MMHMamhaoBeanSpecialProduct *)self.product;
    MMHMamhaoBeanSpecialProductInfo *info = beanSpecialProduct.info;
    if (info == nil) {
        [self.beanProductTimer invalidate];
        self.beanProductTimer = nil;
        return;
    }

    switch (info.status) {
        case MMHMamhaoBeanSpecialProductStatusNotStartedYet: {  // 没开始
            self.productDetailFail = MMHProductDetailFailNotStartedYet;
            break;
        }
        case MMHMamhaoBeanSpecialProductStatusOnSale: {         // 在售
            if ([info isSoldOut]) {                             // 售完
                self.productDetailFail = MMHProductDetailFailNoProduct;
            } else {                                            // 没有售完
                self.productDetailFail = MMHProductDetailFailNormal;
            }
            break;
        }
        case MMHMamhaoBeanSpecialProductStatusExpired: {        // 结束
            self.productDetailFail = MMHProductDetailFailActivieyEnd;
            [self.beanProductTimer invalidate];
            self.beanProductTimer = nil;
            break;
        }
        default: {
            break;
        }
    }
    
    if (self.productDetailModel.mBeanPay > self.productDetailModel.mBean){
        self.productDetailFail = MMHProductDetailFailMbeanNotEnough;
    }
    NSString *displayTimeRemaining = info.displayTimeRemaining;             // 时间
    NSInteger stock = info.stock;
    NSInteger priceIndexSec = [self cellIndexPathSectionWithcellData:@"商品价格"];
    NSInteger priceIndexRow = [self cellIndexPathRowWithcellData:@"商品价格"];
    NSIndexPath *priceIndexPath = [NSIndexPath indexPathForRow:priceIndexRow inSection:priceIndexSec];
    MMHProductDetailPriceCell *cellWithSecOneRowTwo = (MMHProductDetailPriceCell *)[self.productDetailTableView cellForRowAtIndexPath:priceIndexPath];
    [cellWithSecOneRowTwo settingDisplayTimeRemainingWithTime:displayTimeRemaining SurplusCount:stock];
    
    // 修改失败条
    if (!self.failAlphaView){
        [self createFailViewWithFailType:self.productDetailFail];               // 失败页面
    }
}

- (void)tryToStartTimerForBeanProduct {
    if (self.product == nil) {
        return;
    }
    if (self.productDetailModel == nil) {
        return;
    }
    if (![self.product isBeanProduct]) {
        return;
    }
    if (![self.product isKindOfClass:[MMHMamhaoBeanSpecialProduct class]]) {
        return;
    }
    
    if (self.beanProductTimer) {
        return;
    }
    
    NSTimer *beanProductTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                     target:self
                                   selector:@selector(beanProductTimerFired:)
                                   userInfo:nil
                                    repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:beanProductTimer forMode:NSRunLoopCommonModes];
    self.beanProductTimer = beanProductTimer;
}


- (void)beanProductTimerFired:(NSTimer *)beanProductTimer {
    [self tryToUpdateViewsForBeanProduct];
}


-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.productMutableArray.count ; i++){
        NSArray *dataTempArr = [self.productMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.productMutableArray.count ; i++){
        NSArray *dataTempArr = [self.productMutableArray objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark - MMHProductDetailModelDelegate
- (void)productDetailDidChange:(MMHProductDetailModel * __nonnull)productDetail{
    self.productDetailSuperModel = [MMHProductDetailDataSourceManager conversionWithProductDetailModel:productDetail];
    NSInteger priceSecIndex = [self cellIndexPathSectionWithcellData:@"商品价格"];
    NSInteger priceRowIndex = [self cellIndexPathRowWithcellData:@"商品价格"];
    NSIndexPath *priceIndexPath = [NSIndexPath indexPathForRow:priceRowIndex inSection:priceSecIndex];
    [self.productDetailTableView reloadRowsAtIndexPaths:@[priceIndexPath] withRowAnimation:UITableViewRowAnimationNone];
}

@end
