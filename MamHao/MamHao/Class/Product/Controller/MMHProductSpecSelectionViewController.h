//
//  MMHProductSpecSelectionViewController.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "AbstractViewController.h"
#import "MMHFloatingViewController.h"


@class MMHProductDetailModel;


@interface MMHProductSpecSelectionViewController : MMHFloatingViewController

@property (nonatomic) BOOL shouldShowAddingToCartAnimation;

- (instancetype)initWithProductDetail:(MMHProductDetailModel *)productDetail specSelectedHander:(void (^)())specSelectedHandler;

@end
