//
//  MMHProductDetailViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/14.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductDetailViewController.h"
#import "UIImage+ImageEffects.h"
#import "MMHWordOfMouthTableViewCell.h"             //frameModel
#import "MMHPublicPraiseFrameModel.h"               //口碑frameModel；
#import "MMHPublicPraiseCell.h"                     // 口碑
#import "MMHPublicPraiseModel.h"                    // 口碑model
#import "MMHGuessYouLikeScrollView.h"               // 猜你喜欢scrollView
#import "MMHActionSheetViewController.h"            // 立即购买
#import "MMHActionSheetWithShareViewController.h"   // 分享
//#import "MMHCityChooseViewController.h"             // 城市选择

//接口
#import "MMHNetworkAdapter+Product.h"               // 商品接口
#import "MMHProductDetailModel.h"
#import "MMHPopView.h"
#import "MMHTabBarController.h"
#import "MMHProductSpecSelectionViewController.h"
#import "MMHGuessYouLikeListModel.h"
#import "MMHNavigationController.h"
#import "MMHWordOfMouthViewController.h"           //口碑
#import "MMHNetworkAdapter+Cart.h"
#import "MMHChattingViewController.h"
#import "MMHAccount.h"
#import "MMHAccountSession.h"
#import "MMHChattingViewController+Preparation.h"


@interface MMHProductDetailViewController() <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) MMHSingleProductModel *singleProduct;

@property (nonatomic, strong) UITableView *productDetailTableView;                // tableView
@property (nonatomic, strong) NSMutableArray *productDataSourceMutableArray;      // 数据详情数组
@property (nonatomic, strong) NSArray *fixedArray;                                // 快递信息array
@property (nonatomic, strong) UIScrollView *productImageViewScrollView;           // 商品图片
@property (nonatomic, strong) UILabel *dynamicPageLabel;                          // 动态页码
@property (nonatomic, assign) BOOL isCollection;                                  // 是否收藏
@property (nonatomic, strong) MMHProductDetailModel *dataSourceProductDetailModel;// 商品详情model

@property (nonatomic, strong) UIButton *collectionButton;                         // 收藏按钮

// 妈妈说
@property (nonatomic, strong) UILabel *mamSayLabel;                               // 妈妈说label
@property (nonatomic, assign) BOOL mamSayIsOpen;                                  // 妈妈说是否展开

// 邮费
@property (nonatomic, assign) BOOL postIsLoading;                                 // 邮费标记是否加载

// 猜你喜欢
@property (nonatomic, strong) MMHGuessYouLikeListModel *guessYouLikeListModel;    // 猜你喜欢列表
/**
 *  口碑数组
 */
@property (nonatomic, strong) NSMutableArray *womDataArray; //口碑数据数组
/**
 *  口碑字典
 */
@property (nonatomic, strong) NSDictionary *WomMouthDic; //口碑字典
@property (nonatomic, strong) id <MMHProductProtocol> product;
@end


@implementation MMHProductDetailViewController


- (NSMutableArray *)womDataArray {
    if (!_womDataArray) {
        self.womDataArray = [NSMutableArray array];
    }
    return _womDataArray;
}


- (instancetype)initWithSingleProduct:(MMHSingleProductModel *)singleProduct
{
    self = [self init];
    if (self) {
        self.singleProduct = singleProduct;
    }
    return self;
}


- (instancetype)initWithProduct:(id<MMHProductProtocol>)product
{
    self = [self init];
    if (self) {
        self.product = product;
    }
    return self;
}


- (id)initWithTemplateId:(NSString *)templateId
{
    return nil; // TODO:
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                 // 页面设置
    [self arrayWithInit];                               // arrayWithInit
    [self createTableView];                             // 创建tableView
    [self createFloatView];                             // 创建漂浮层
    __weak typeof(self)weakSelf = self;
//    [weakSelf sendRequestWithGetproductDetail];
    [weakSelf sendRequestWithGuessYouLike];
    [weakSelf sendRequestWithGetWomOfGoodsDetail];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"商品详情";

    UIView *rightBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 60, 31)];
    
    UIButton *shoppingCarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shoppingCarButton.frame=CGRectMake(0, 5, 25, 25);
    [shoppingCarButton setImage:[UIImage imageNamed:@"login_thirdLogin_wechat"] forState:UIControlStateNormal];
    [shoppingCarButton addTarget:self action:@selector(shoppingCarButtonClick)forControlEvents:UIControlEventTouchDown];
    [rightBarView addSubview:shoppingCarButton];
    
    UIButton *messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [messageButton setFrame:CGRectMake(30, 5, 25, 25)];
    [messageButton setImage:[UIImage imageNamed:@"login_thirdLogin_wechat"] forState:UIControlStateNormal];
    [messageButton addTarget:self action:@selector(messageButtonClick)forControlEvents:UIControlEventTouchDown];
    [rightBarView addSubview:messageButton];
    rightBarView.backgroundColor=[UIColor clearColor];
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc]initWithCustomView:rightBarView];
    self.navigationItem.rightBarButtonItem = rightBtn;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dataSourceProductDetailModel = [[MMHProductDetailModel alloc]init];
    self.productDataSourceMutableArray = [NSMutableArray array];
    /*
     if (section == 0){
     return 2;
     } else if (section == 1){
     return 1;
     } else if (section == 2){
     return 5;
     } else if (section == 3){
     return 3;
     } else if (section == 4){
     return 3;
     } else if (section == 5){
     return 1;
     } else {
     return 1;
     }
     */
    
    self.fixedArray = @[@[@"送至",@"",@"运费",@"服务",@""],@[@"浙江>杭州市>上城区",@"现在下单预计17：30前送达",
                                                       @"订单总额不满100元，收取运费10.00元",
                                                       @"由妈妈好 发货并提供售后服务",@""]];
    [self arrayAddDataWithTableViewDataSource];
}

-(void)arrayAddDataWithTableViewDataSource{
#if 0
    NSArray *tempArr;
    if (self.singleProduct.shopId){                // 门店
        tempArr = @[@[@"商品图片",@"商品价格"],@[@"妈妈说"],@[@"送至",@"fixed",@"运费",@"服务",@"productTag"],@[@"质检担保",@"商品参数",@"图文详情"],@[@"商品口碑",@"口碑1",@"口碑2"],@[@"门店信息"],@[@"猜你喜欢"],@[@"其他"]];
        [self.productDataSourceMutableArray addObjectsFromArray:tempArr];
    } else {                                                    // 仓储
        tempArr = @[@[@"商品图片",@"商品价格"],@[@"妈妈说"],@[@"送至",@"fixed",@"运费",@"服务",@"productTag"],@[@"质检担保",@"商品参数",@"图文详情"],@[@"商品口碑",@"口碑1",@"口碑2"],@[@"猜你喜欢"],@[@"其他"]];
        [self.productDataSourceMutableArray addObjectsFromArray:tempArr];
    }
#endif
}

#pragma mark UITableView
-(void)createTableView{
    self.productDetailTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.productDetailTableView setHeight:self.view.bounds.size.height];
    self.productDetailTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.productDetailTableView.delegate = self;
    self.productDetailTableView.dataSource = self;
    self.productDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.productDetailTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.productDetailTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 7;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 2;
    } else if (section == 1){
        return 1;
    } else if (section == 2){
        return 5;
    } else if (section == 3){
        return 3;
    } else if (section == 4){
        return 3;
    } else if (section == 5){
        return 1;
    } else {
        return 1;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentifyWithSectionOne = @"cellIdentifyWithSectionOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 1. 创建图片scrollView
            self.productImageViewScrollView = [[UIScrollView alloc]init];
            self.productImageViewScrollView.backgroundColor = [UIColor clearColor];
            self.productImageViewScrollView.frame = CGRectMake(0, 0, self.view.bounds.size.width, mmh_relative_float(375));
            self.productImageViewScrollView.bounces = NO;
            self.productImageViewScrollView.showsHorizontalScrollIndicator = NO;
            self.productImageViewScrollView.showsVerticalScrollIndicator = NO;
            self.productImageViewScrollView.pagingEnabled = YES;
            self.productImageViewScrollView.delegate = self;
            self.productImageViewScrollView.directionalLockEnabled = YES;//锁定滑动的方向
            
            [cellWithRowOne addSubview:self.productImageViewScrollView];
            
            // 2. 创建page
            UIView *pageBackgroundView = [[UIView alloc]init];
            pageBackgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.15f];
            pageBackgroundView.frame = CGRectMake(self.productImageViewScrollView.frame.size.width - mmh_relative_float(11 + 40), self.productImageViewScrollView.frame.size.height - mmh_relative_float(11 + 40),mmh_relative_float(40),mmh_relative_float(40) );
            pageBackgroundView.layer.cornerRadius = mmh_relative_float(20.0f);
            [cellWithRowOne addSubview:pageBackgroundView];
            
            // 3. 创建动态page
            self.dynamicPageLabel = [[UILabel alloc]init];
            self.dynamicPageLabel.backgroundColor = [UIColor clearColor];
            self.dynamicPageLabel.font = [UIFont systemFontOfSize:mmh_relative_float(20)];
            self.dynamicPageLabel.text = @"1";
            self.dynamicPageLabel.textColor = [UIColor whiteColor];
            self.dynamicPageLabel.frame = CGRectMake(5, 7, 10, 20);
            [pageBackgroundView addSubview:self.dynamicPageLabel];
            
            // 4. 创建固定page
            UILabel *fixedPageLabel = [[UILabel alloc]init];
            fixedPageLabel.backgroundColor = [UIColor clearColor];
            fixedPageLabel.textColor = [UIColor whiteColor];
            fixedPageLabel.font = [UIFont systemFontOfSize:mmh_relative_float(16.)];
            fixedPageLabel.stringTag = @"fixedPageLabel";
            fixedPageLabel.frame = CGRectMake(CGRectGetMaxX(self.dynamicPageLabel.frame), self.dynamicPageLabel.frame.origin.y + mmh_relative_float(3), 15, 20);
            [pageBackgroundView addSubview:fixedPageLabel];
            
            // 2.创建商品名称
            UILabel *productNameLabel = [[UILabel alloc]init];
            productNameLabel.backgroundColor = [UIColor clearColor];
            productNameLabel.stringTag = @"productNameLabel";
            productNameLabel.font = [UIFont systemFontOfSize:mmh_relative_float(16.)];
            productNameLabel.frame = CGRectMake(mmh_relative_float(11), CGRectGetMaxY(self.productImageViewScrollView.frame)+mmh_relative_float(20), self.view.bounds.size.width - 2 * mmh_relative_float(11), 30);
            productNameLabel.textAlignment = NSTextAlignmentLeft;
            productNameLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
            [cellWithRowOne addSubview:productNameLabel];
            
            // 创建副标题
            UILabel *subTitleLabel = [[UILabel alloc]init];
            subTitleLabel.backgroundColor = [UIColor clearColor];
            subTitleLabel.frame = CGRectMake(mmh_relative_float(11), CGRectGetMaxY(productNameLabel.frame) + MMHFloat(10), self.view.bounds.size.width - 2 * mmh_relative_float(11), 100);
            subTitleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(14)];
            subTitleLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            subTitleLabel.stringTag = @"subTitleLabel";
            subTitleLabel.numberOfLines = 0;
            [cellWithRowOne addSubview:subTitleLabel];
            
            // 创建imageView
            UILabel *channelLabel = [[UILabel alloc]init];
            channelLabel.backgroundColor = [UIColor hexChangeFloat:@"fc687c"];
            channelLabel.layer.cornerRadius = MMHFloat(6);
            channelLabel.font = [UIFont systemFontOfSize:MMHFloat(12.)];
            channelLabel.stringTag = @"channelLabel";
            channelLabel.clipsToBounds = YES;
            channelLabel.textColor = [UIColor hexChangeFloat:@"ffffff"];
            channelLabel.textAlignment = NSTextAlignmentCenter;
            [cellWithRowOne addSubview: channelLabel];
        }
        // 赋值
        // fixedPageLabel
        UILabel *fixedPageLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedPageLabel"];
        
        // 1. 创建图片
        if (self.dataSourceProductDetailModel.pic.count){
            if (!self.productImageViewScrollView.subviews.count){
                for (int i = 0 ; i < self.dataSourceProductDetailModel.pic.count ; i++){
                    UIImageView *productImageView = [[UIImageView alloc]init];
                    productImageView.backgroundColor = [UIColor clearColor];
                    productImageView.stringTag = @"productImageView";
                    productImageView.image = [UIImage imageNamed:@"login_thirdLogin_wechat"];
                    productImageView.frame = CGRectMake(0 + i * self.view.bounds.size.width, 0, self.view.bounds.size.width, mmh_relative_float(375));
                    [self.productImageViewScrollView addSubview:productImageView];
                }
            }
            self.productImageViewScrollView.contentSize = CGSizeMake(self.dataSourceProductDetailModel.pic.count * self.view.bounds.size.width, 0);
            fixedPageLabel.text = [NSString stringWithFormat:@"/%lu",(unsigned long)self.dataSourceProductDetailModel.pic.count];
        }
        
        // 2. 商品名称
        UILabel *productNameLabel =(UILabel *)[cellWithRowOne viewWithStringTag:@"productNameLabel"];
        if (self.dataSourceProductDetailModel.title.length){
            [self autoSettingFrameWith:productNameLabel andLabelString:self.dataSourceProductDetailModel.title];
        }
        
        // 3. 商品副标题
        UILabel *subTitleLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"subTitleLabel"];
        if (self.dataSourceProductDetailModel.subtitle.length){
            subTitleLabel.text = self.dataSourceProductDetailModel.subtitle;
            CGSize contentOfSize = [self.dataSourceProductDetailModel.subtitle sizeWithCalcFont:subTitleLabel.font constrainedToSize:CGSizeMake(subTitleLabel.frame.size.width, CGFLOAT_MAX)];
            
            subTitleLabel.frame = CGRectMake(subTitleLabel.frame.origin.x, CGRectGetMaxY(productNameLabel.frame) + MMHFloat(10), subTitleLabel.frame.size.width, contentOfSize.height);
        }
        
        
        
        // 1. 计算单个文字的高度
        CGFloat singleLineHeight = [[NSString stringWithFormat:@"文字"] sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(16.)] constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)].height;
        // 2. 计算文字长度
        CGFloat textWidth = [self.dataSourceProductDetailModel.title sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(16.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, singleLineHeight)].width;
        // 3. 获取控件的需要高度
        NSInteger numberOfLine = textWidth / (self.view.bounds.size.width - 2 * mmh_relative_float(11));
        CGFloat excessWidth = textWidth - numberOfLine * (self.view.bounds.size.width - 2 * mmh_relative_float(11));
        //
        UILabel *channelLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"channelLabel"];
       
        
     // 4. 计算《自营》《实体店》文字宽度
        channelLabel.text = self.dataSourceProductDetailModel.shopId.length?@"实体店":@"自营";
        CGFloat channelContentOfWidth = [channelLabel.text sizeWithCalcFont:channelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, singleLineHeight)].width;
            if (self.view.bounds.size.width - 2 * (MMHFloat(11)) - excessWidth > (channelContentOfWidth + MMHFloat(6)+MMHFloat(20))){
                channelLabel.frame = CGRectMake(excessWidth  + MMHFloat(30), CGRectGetMaxY(self.productImageViewScrollView.frame)+mmh_relative_float(20) + numberOfLine * singleLineHeight + numberOfLine * (.5f), channelContentOfWidth + MMHFloat(6), MMHFloat(17));
            } else {
                 channelLabel.frame = CGRectMake(MMHFloat(11), CGRectGetMaxY(self.productImageViewScrollView.frame)+mmh_relative_float(20) + (numberOfLine + 1) * singleLineHeight + numberOfLine * (.5f), channelContentOfWidth + MMHFloat(6), MMHFloat(17));
                if (self.dataSourceProductDetailModel.subtitle.length){
                    subTitleLabel.frame = CGRectMake(subTitleLabel.frame.origin.x,subTitleLabel.frame.origin.y + channelLabel.frame.size.height,subTitleLabel.frame.size.width,subTitleLabel.frame.size.height);
                }
            }
        
        return cellWithRowOne;
    } else if(indexPath.section == 0 && indexPath.row == 1){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            
            // 创建价格标志
            UILabel *priceFixedLabel = [[UILabel alloc]init];
            priceFixedLabel.backgroundColor = [UIColor clearColor];
            priceFixedLabel.textColor = [UIColor hexChangeFloat:@"fc687c"];
            priceFixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(20)];
            NSString *priceFixedString = @"￥";
            priceFixedLabel.text = priceFixedString;
            priceFixedLabel.frame = CGRectMake(mmh_relative_float(11), 0, 20, cellHeight);
            [cellWithRowTwo addSubview:priceFixedLabel];
            
            // 创建价格
            UILabel *priceLabel = [[UILabel alloc]init];
            priceLabel.stringTag = @"priceLabel";
            priceLabel.backgroundColor = [UIColor clearColor];
            priceLabel.textAlignment = NSTextAlignmentLeft;
            priceLabel.font = [UIFont systemFontOfSize:mmh_relative_float(25)];
            priceLabel.frame = CGRectMake(CGRectGetMaxX(priceFixedLabel.frame), 0, 200, cellHeight);
            priceLabel.textColor = [UIColor hexChangeFloat:@"fc687c"];
            [cellWithRowTwo addSubview:priceLabel];
            
            // 创建收藏按钮
            self.collectionButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self createCustomButtonWithButton:self.collectionButton buttonTag:@"collection" buttonImage:@"product_collection_nor" buttonName:@"收藏"];
            self.collectionButton.frame = CGRectMake(self.view.bounds.size.width - mmh_relative_float(11) - 2 * mmh_relative_float(50) - mmh_relative_float(5), mmh_relative_float(5), mmh_relative_float(50), mmh_relative_float(60));
            [cellWithRowTwo addSubview:self.collectionButton];
            
            // 创建lineView
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = [UIColor lightGrayColor];
            lineView.frame = CGRectMake(self.view.bounds.size.width - mmh_relative_float(11 + 50) - 2, 5, .5f, cellHeight - 10);
            [cellWithRowTwo addSubview:lineView];
            
            // 创建分享按钮
            UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self createCustomButtonWithButton:shareButton buttonTag:@"share" buttonImage:@"product_share" buttonName:@"分享"];
            shareButton.frame = CGRectMake(self.view.bounds.size.width - mmh_relative_float(11) - mmh_relative_float(50), mmh_relative_float(5), mmh_relative_float(50), mmh_relative_float(60));
            [cellWithRowTwo addSubview:shareButton];
        }
        //  赋值
        // 1. 价格
        UILabel *priceLabel = (UILabel *)[cellWithRowTwo viewWithStringTag:@"priceLabel"];
        priceLabel.text = [NSString stringWithFormat:@"%.2f",self.dataSourceProductDetailModel.price];
        
        return cellWithRowTwo;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowThrid = @"cellIdentifyWithRowThird";
        UITableViewCell *cellWithRowThrid = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThrid];
        CGFloat cellWithHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithRowThrid){
            cellWithRowThrid = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThrid];
            
            // 创建头像
            UIImageView *headerImageView = [[UIImageView alloc]init];
            headerImageView.backgroundColor = [UIColor clearColor];
            headerImageView.frame = CGRectMake(10,mmh_relative_float(20),45,45);
            headerImageView.stringTag = @"headerImageView";
            [headerImageView.layer setMasksToBounds:YES];
            [headerImageView.layer setCornerRadius:22.];
            headerImageView.layer.borderColor = [UIColor whiteColor].CGColor;
            headerImageView.layer.borderWidth = 2;
            [cellWithRowThrid addSubview:headerImageView];
            
            // 创建用户label
            UILabel *otherMamNameLabel = [[UILabel alloc]init];
            otherMamNameLabel.backgroundColor = [UIColor clearColor];
            otherMamNameLabel.textAlignment = NSTextAlignmentCenter;
            otherMamNameLabel.font = [UIFont systemFontOfSize:mmh_relative_float(12.)];
            otherMamNameLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            otherMamNameLabel.stringTag = @"otherMamNameLabel";
            otherMamNameLabel.frame = CGRectMake(0, CGRectGetMaxY(headerImageView.frame),70, 20);
            [cellWithRowThrid addSubview:otherMamNameLabel];
            
            // 添加头像按钮
            UIButton *headButton = [UIButton buttonWithType:UIButtonTypeCustom];
            headButton.frame = CGRectMake(0,0,70,cellWithHeight);
            headButton.backgroundColor = [UIColor clearColor];
            [headButton addTarget:self action:@selector(headButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [cellWithRowThrid addSubview:headButton];

            // 创建妈妈说
            self.mamSayLabel = [[UILabel alloc]init];
            self.mamSayLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
            self.mamSayLabel.frame=CGRectMake(70,mmh_relative_float(20),self.view.bounds.size.width - 70 - mmh_relative_float(11),cellWithHeight - 2 * mmh_relative_float(20));
            self.mamSayLabel.textAlignment = NSTextAlignmentLeft;
            self.mamSayLabel.backgroundColor=[UIColor clearColor];
            self.mamSayLabel.stringTag = @"mamSayLabel";
            [cellWithRowThrid addSubview:self.mamSayLabel];
        }
        // 赋值
        // 1. 头像
        UIImageView *headerImageView = (UIImageView *)[cellWithRowThrid viewWithStringTag:@"headerImageView"];
        headerImageView.image = [UIImage imageNamed:@"login_thirdLogin_wechat"];
        
        // 2. 昵称
        UILabel *otherMamNameLabel = (UILabel *)[cellWithRowThrid viewWithStringTag:@"otherMamNameLabel"];
        otherMamNameLabel.text = @"我是昵称昵";
        
        if (self.mamSayIsOpen){ // 展开状态
            self.mamSayLabel.numberOfLines = 0;
        } else {
            self.mamSayLabel.numberOfLines = 3;
        }
        
        // 妈妈说
        UILabel *mamSayLabel = (UILabel *)[cellWithRowThrid viewWithStringTag:@"mamSayLabel"];
        NSString *testMamSayString = self.dataSourceProductDetailModel.mamCare;
        NSString *mamSayString = [NSString stringWithFormat:@"好妈妈说:%@",testMamSayString];
        NSRange range1 = [mamSayString rangeOfString:@"好妈妈说:"];
        NSRange range2 = [mamSayString rangeOfString:[NSString stringWithFormat:@"%@",testMamSayString]];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:mamSayString];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"fc687c"] range:range1];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor hexChangeFloat:@"666666"] range:range2];
        mamSayLabel.attributedText = str;
        
        return cellWithRowThrid;
    } else if (indexPath.section == 2){
        if (indexPath.row == 4){
            static NSString *cellIdentifyWithSectionTwoAndRowZero = @"cellIdentifyWithSectionTwoAndRowZero";
            UITableViewCell *cellWithSectionRwoAndRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTwoAndRowZero];
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            if (!cellWithSectionRwoAndRowOne){
                cellWithSectionRwoAndRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTwoAndRowZero];
                cellWithSectionRwoAndRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            if (self.postIsLoading){
                // 赋值判断是否有加载过
                [self createServerButtonWithCellHeight:cellHeight andSubView:cellWithSectionRwoAndRowOne];
                self.postIsLoading = NO;
            }
            return cellWithSectionRwoAndRowOne;
        } else {
            static NSString *cellIdentifyWithSectionTwoAndRowOne = @"cellIdentifyWithSectionTwoAndRowOne";
            UITableViewCell *cellWithSectionTwoAndRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionTwoAndRowOne];
            if (!cellWithSectionTwoAndRowOne){
                cellWithSectionTwoAndRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionTwoAndRowOne];
                // 创建fixedlabel
                
                CGFloat cellWithHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
                
                UILabel *fixedLabel = [[UILabel alloc]init];
                fixedLabel.backgroundColor = [UIColor clearColor];
                fixedLabel.textAlignment = NSTextAlignmentLeft;
                fixedLabel.textColor = [UIColor hexChangeFloat:@"999999"];
                fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15)];
                fixedLabel.frame = CGRectMake(mmh_relative_float(11), 0, 50, cellWithHeight);
                fixedLabel.stringTag = @"fixedLabel";
                [cellWithSectionTwoAndRowOne addSubview:fixedLabel];
                
                // 创建subTitleLabel
                UILabel *subTitleLabel = [[UILabel alloc]init];
                subTitleLabel.backgroundColor = [UIColor clearColor];
                subTitleLabel.stringTag = @"subTitleLabel";
                subTitleLabel.textAlignment = NSTextAlignmentLeft;
                [cellWithSectionTwoAndRowOne addSubview:subTitleLabel];
                
                // 创建imageIcon
                UIImageView *locationImageView = [[UIImageView alloc]init];
                locationImageView.backgroundColor = [UIColor clearColor];
                locationImageView.stringTag = @"locationImageView";
                locationImageView.hidden = YES;
                locationImageView.image = [UIImage imageNamed:@"product_location"];
                [cellWithSectionTwoAndRowOne addSubview:locationImageView];
            }
            // 赋值
            if (indexPath.row == 0){
                cellWithSectionTwoAndRowOne.selectionStyle = UITableViewCellSelectionStyleGray;
            } else {
                cellWithSectionTwoAndRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            CGFloat cellWithHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            // fixedLabel;
            UILabel *fixedLabel = (UILabel *)[cellWithSectionTwoAndRowOne viewWithStringTag:@"fixedLabel"];
            fixedLabel.text = [[self.fixedArray objectAtIndex:0] objectAtIndex:indexPath.row];
            
            // 2. subTitleLabel
            UILabel *subTitleLabel = (UILabel *)[cellWithSectionTwoAndRowOne viewWithStringTag:@"subTitleLabel"];
            subTitleLabel.text = [[self.fixedArray objectAtIndex:1] objectAtIndex:indexPath.row];
            if (indexPath.row == 1){
                subTitleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(14.)];
                subTitleLabel.textColor = [UIColor hexChangeFloat:@"999999"];
                subTitleLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + mmh_relative_float(14),0, self.view.bounds.size.width - CGRectGetMaxX(fixedLabel.frame) - 30, cellWithHeight);
            } else {
                subTitleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
                subTitleLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
                if (indexPath.row == 0){
                    CGSize locationOfSize = [[[self.fixedArray objectAtIndex:1] objectAtIndex:indexPath.row] sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(15)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellWithHeight)];
                    subTitleLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + MMHFloat(14), 0, locationOfSize.width, cellWithHeight);
                } else {
                    subTitleLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame) + mmh_relative_float(14),0, self.view.bounds.size.width - CGRectGetMaxX(fixedLabel.frame) - 30, cellWithHeight);
                }
            }
            
            // 3.增加location图标判断
            UIImageView *locationImageView = (UIImageView *)[cellWithSectionTwoAndRowOne viewWithStringTag:@"locationImageView"];
            if (indexPath.row ==  0){
                locationImageView.hidden = NO;
                locationImageView.frame = CGRectMake(CGRectGetMaxX(subTitleLabel.frame)+ MMHFloat(5), (cellWithHeight - MMHFloat(17)) / 2., MMHFloat(12), MMHFloat(17));
            } else {
                locationImageView.hidden = YES;
            }
            return cellWithSectionTwoAndRowOne;
        }
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithSectionThrid = @"cellIdentifyWithSectionThrid";
        UITableViewCell *cellWithSectionThrid = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionThrid];
        if (!cellWithSectionThrid){
            cellWithSectionThrid = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionThrid];
            cellWithSectionThrid.selectionStyle = UITableViewCellSelectionStyleNone;
            
            CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
            
            // 创建image
            UIImageView *logoImageView = [[UIImageView alloc]init];
            logoImageView.backgroundColor = [UIColor clearColor];
            logoImageView.frame = CGRectMake(mmh_relative_float(11.), (cellHeight - 25) / 2., 25, 25);
            logoImageView.stringTag = @"logoImageView";
            [cellWithSectionThrid addSubview:logoImageView];
            
            // 创建Fixedlabel
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.stringTag = @"fixedLabel";
            fixedLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
            fixedLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(logoImageView.frame) + mmh_relative_float(10), 0, 200, cellHeight);
            [cellWithSectionThrid addSubview:fixedLabel];
            
            cellWithSectionThrid.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tool_arrow"]];
        }
        // 赋值
        // 1. imageView
        UIImageView *logoImageView = (UIImageView *)[cellWithSectionThrid viewWithStringTag:@"logoImageView"];
        
        // 2. fixedLabel
        UILabel *fixedLabel = (UILabel *)[cellWithSectionThrid viewWithStringTag:@"fixedLabel"];
        if (indexPath.row == 0){
            logoImageView.image = [UIImage imageNamed:@"product_guarantee"];
            fixedLabel.text = @"质监担保";
        } else if (indexPath.row == 1){
            logoImageView.image = [UIImage imageNamed:@"product_parameters"];
            fixedLabel.text = @"商品参数";
        } else if (indexPath.row == 2){
            logoImageView.image = [UIImage imageNamed:@"product_picturedetails"];
            fixedLabel.text = @"图文详情";
        }
        
        return cellWithSectionThrid;
    } else if (indexPath.section == 4){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithSecionFiv = @"cellIdentifyWithSecionFiv";
            UITableViewCell *cellWithSectionFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSecionFiv];
            if (!cellWithSectionFiv){
                cellWithSectionFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSecionFiv];
                cellWithSectionFiv.selectionStyle = UITableViewCellSelectionStyleGray;
                
                CGSize cellOfSize = [tableView rectForRowAtIndexPath:indexPath].size;
                //
                UILabel *publicPraiseLabel = [[UILabel alloc]init];
                publicPraiseLabel.stringTag = @"publicPraiseLabel";
                publicPraiseLabel.backgroundColor = [UIColor clearColor];
                publicPraiseLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
                publicPraiseLabel.textAlignment = NSTextAlignmentLeft;
                publicPraiseLabel.textColor = [UIColor hexChangeFloat:@"b8b8b8"];
                publicPraiseLabel.frame = CGRectMake(mmh_relative_float(11), 0, 200, cellOfSize.height);
                [cellWithSectionFiv addSubview:publicPraiseLabel];
                
                cellWithSectionFiv.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"tool_arrow"]];
            }
            // 赋值
            UILabel *publicPraiseLabel = (UILabel *)[cellWithSectionFiv viewWithStringTag:@"publicPraiseLabel"];
            NSString *str = @"商品口碑(%ld 好评 %ld%)";
            NSInteger commentCount = [self.WomMouthDic[@"commentCount"] integerValue];
            NSInteger percentage = [self.WomMouthDic[@"percentage"] integerValue];
            publicPraiseLabel.text = [NSString stringWithFormat:str,commentCount, percentage];
            
            return cellWithSectionFiv;
        } else {
            static NSString *cellIdentifyWithPublicPraise = @"cellIdentifyWithPublicPraise";
            MMHWordOfMouthTableViewCell *cellWithPublicPraiseCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithPublicPraise];
            cellWithPublicPraiseCell.type = MMHWordOfMouthTableViewCellTypeOfGoodsDetail;
            if (!cellWithPublicPraiseCell){
                cellWithPublicPraiseCell = [[MMHWordOfMouthTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithPublicPraise];
                cellWithPublicPraiseCell.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithPublicPraiseCell.width = [tableView rectForRowAtIndexPath:indexPath].size.width;
            }
            // 赋值
            MMHPublicPraiseModel *publicPraiseModel = [[MMHPublicPraiseModel alloc]init];
            publicPraiseModel.name = @"小豆豆";
            publicPraiseModel.babyAge = @"1年2个月";
            publicPraiseModel.commentContent = @"2015-2-14";
            publicPraiseModel.commentTime = @"我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论我是评论";
            publicPraiseModel.star = 5;
        //    publicPraiseModel.babyImageArray = @[@"login_thirdLogin_wechat",@"login_thirdLogin_wechat",@"login_thirdLogin_wechat",@"login_thirdLogin_wechat",@"login_thirdLogin_wechat",@"login_thirdLogin_wechat"];
            if (self.womDataArray.count == 2) {
                if (indexPath.row == 1) {
                    cellWithPublicPraiseCell.frameModel = self.womDataArray[0];
                }
                if (indexPath.row == 2) {
                    cellWithPublicPraiseCell.frameModel = self.womDataArray[1];
                }
            }
            

            return cellWithPublicPraiseCell;
        }
    } else if (indexPath.section == 5) {
        static NSString *cellIdentifyWithSctionWithSix = @"cellIdentifyWithSctionWithSix";
        UITableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSctionWithSix];
        if (!cellWithRowSix){
            cellWithRowSix = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSctionWithSix];
            cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
            
            CGSize cellSize = [tableView rectForRowAtIndexPath:indexPath].size;
            
            // 1. 创建猜你喜欢
            UIView *guessYouLikeBackgroundView = [[UIView alloc]init];
            guessYouLikeBackgroundView.backgroundColor = [UIColor whiteColor];
            guessYouLikeBackgroundView.frame = CGRectMake(0, 0,cellSize.width , mmh_relative_float(50));
            [cellWithRowSix addSubview:guessYouLikeBackgroundView];
            
            // 创建line
            UIView *lineView = [[UIView alloc]init];
            lineView.backgroundColor = [UIColor hexChangeFloat:@"999999"];
            lineView.frame = CGRectMake(mmh_relative_float(11), guessYouLikeBackgroundView.frame.size.height / 2., cellSize.width - 2 * mmh_relative_float(11), .5f);
            [guessYouLikeBackgroundView addSubview:lineView];
            
            // 创建label
            UILabel *guessYouLikeLabel = [[UILabel alloc]init];
            guessYouLikeLabel.backgroundColor = [UIColor whiteColor];
            NSString *guessYouLikeString = @"猜你喜欢";
            guessYouLikeLabel.text = guessYouLikeString;
            CGSize guessYouLikeSize = [guessYouLikeString sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, mmh_relative_float(50))];
            guessYouLikeLabel.textAlignment = NSTextAlignmentCenter;
            guessYouLikeLabel.font = [UIFont systemFontOfSize:mmh_relative_float(15.)];
            guessYouLikeLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            guessYouLikeLabel.frame = CGRectMake((cellSize.width - guessYouLikeSize.width - 40) / 2., 0, guessYouLikeSize.width + 40, mmh_relative_float(50));
            [guessYouLikeBackgroundView addSubview:guessYouLikeLabel];
            
            // 猜你喜欢
            MMHGuessYouLikeScrollView *guessYouLikeScrollView = [[MMHGuessYouLikeScrollView alloc]init];
            guessYouLikeScrollView.backgroundColor = [UIColor yellowColor];
            guessYouLikeScrollView.frame = CGRectMake(mmh_relative_float(11), CGRectGetMaxY(guessYouLikeBackgroundView.frame), self.view.bounds.size.width - 2 *mmh_relative_float(11), mmh_relative_float(180));
            guessYouLikeScrollView.guessYouLikeArray = self.guessYouLikeListModel.data;
            [cellWithRowSix addSubview:guessYouLikeScrollView];
            
        }
        return cellWithRowSix;
    }else{                  // 门店
        static NSString *cellIdentifyWithSectionFour = @"cellIdentifyWithSectionFour";
        UITableViewCell *cellWithSectionFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithSectionFour];
        CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        if (!cellWithSectionFour){
            cellWithSectionFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithSectionFour];
            cellWithSectionFour.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // shopImage
            UIImageView *shopImageView = [[UIImageView alloc]init];
            shopImageView.backgroundColor = [UIColor clearColor];
            shopImageView.frame = CGRectMake(MMHFloat(10), MMHFloat(10), MMHFloat(50), MMHFloat(50));
            shopImageView.stringTag = @"shopImageView";
            [cellWithSectionFour addSubview:shopImageView];
            
            // 创建label
            UILabel *shopNameLabel = [[UILabel alloc]init];
            shopNameLabel.backgroundColor = [UIColor clearColor];
            shopNameLabel.textAlignment = NSTextAlignmentLeft;
            shopNameLabel.frame = CGRectMake(CGRectGetMaxX(shopImageView.frame) + MMHFloat(10), shopImageView.frame.origin.y + MMHFloat(5), self.view.bounds.size.width - MMHFloat(50) - 2 *MMHFloat(10) - MMHFloat(50), 15);
            shopNameLabel.stringTag = @"shopNameLabel";
            shopNameLabel.font = [UIFont systemFontOfSize:MMHFloat(15)];
            shopNameLabel.textColor = [UIColor hexChangeFloat:@"383d40"];
            shopNameLabel.numberOfLines = 1;
            [cellWithSectionFour addSubview:shopNameLabel];
            
            // 创建locationLabel
            UILabel *locationLabel = [[UILabel alloc]init];
            locationLabel.backgroundColor = [UIColor clearColor];
            locationLabel.stringTag = @"locationLabel";
            locationLabel.font = [UIFont systemFontOfSize:MMHFloat(12)];
            locationLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            locationLabel.frame = CGRectMake(CGRectGetMaxX(shopImageView.frame) + MMHFloat(10), CGRectGetMaxY(shopNameLabel.frame)+MMHFloat(10), shopNameLabel.frame.size.width, 12);
            [cellWithSectionFour addSubview:locationLabel];
            
            // Arrow
            UIImageView *imagView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tool_arrow"]];
            [imagView setBackgroundColor:[UIColor clearColor]];
            imagView.frame=CGRectMake(self.view.frame.size.width - 27,(cellHeight - 15)/2.0, MMHFloat(9), 15);
            [cellWithSectionFour addSubview:imagView];
        }
        // 1. shopImageView
        UIImageView *shopImageView = (UIImageView *)[cellWithSectionFour viewWithStringTag:@"shopImageView"];
        shopImageView.image = [UIImage imageNamed:@"login_thirdLogin_wechat"];
        
        // 2. shopNameLabel
        UILabel *shopNameLabel = (UILabel *)[cellWithSectionFour viewWithStringTag:@"shopNameLabel"];
        NSString *testString = @"武林门好孩子旗舰店（实体店）";
        shopNameLabel.text = testString;
        
        
        // 3. locationLabel
        UILabel *locationLabel = (UILabel *)[cellWithSectionFour viewWithStringTag:@"locationLabel"];
        locationLabel.text = @"距您1000m";
        
        return cellWithSectionFour;
    }
    
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1){                //  妈妈说
        self.mamSayIsOpen = !self.mamSayIsOpen;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [self.productDetailTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }  else if (indexPath.section == 2 && indexPath.row == 0){              // 选择 城市
//        MMHCityChooseViewController *cityChooseViewController = [[MMHCityChooseViewController alloc] initWithRegionSelector:nil];
//        MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
//        MMHNavigationController *navigationController = [[MMHNavigationController alloc] initWithRootViewController:cityChooseViewController];
//        [tabBarController presentFloatingViewController:(MMHFloatingViewController *)navigationController animated:YES];
////        cityChooseViewController.isHasGesture = YES;
//        cityChooseViewController.cityBlock = ^(NSInteger cityId){
//                UIAlertView *showAlert = [UIAlertView alertViewWithTitle:@"选择的城市" message:@"123" buttonTitles:@[@"确定"] callback:nil];
//            [showAlert show];
//        };
//        [cityChooseViewController showInView:self.parentViewController];
    } else if (indexPath.section == 2 && indexPath.row == 4){
        CGFloat xWidth = self.view.bounds.size.width - 70.0f;
        CGFloat yHeight = 272.0f;
        CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
        MMHPopView *poplistview = [[MMHPopView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
        poplistview.usingType = MMHUsingTypeProductGuarantees;
        [poplistview viewShow];
    }
    else if (indexPath.section == 3){

    } else if (indexPath.section == 4){
        if (indexPath.row == 0){                            // 商品口碑
            MMHWordOfMouthViewController *womVC = [[MMHWordOfMouthViewController alloc]  initWithContentType:MMHWordOfMouthViewControllerContentTypeWordOfMouth];
            womVC.goodsTemplateId = [self.dataSourceProductDetailModel.goodsTempId integerValue];
           
            [self.navigationController pushViewController:womVC animated:YES];
        
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        CGFloat cellWithRowOneHeight = 0;
        if (self.dataSourceProductDetailModel.title.length){
            cellWithRowOneHeight += mmh_relative_float(375);                // 1. 图片scroll
            cellWithRowOneHeight += MMHFloat(20);                           // 2. 间距
            CGSize contentOfSize = [self.dataSourceProductDetailModel.title sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(16.)] constrainedToSize:CGSizeMake(self.view.bounds.size.width - 2 * mmh_relative_float(11), CGFLOAT_MAX)];
            cellWithRowOneHeight += contentOfSize.height;
            cellWithRowOneHeight += MMHFloat(MMHFloat(10));
   
            // 1. 计算单个文字的高度
            NSString *channelString = self.dataSourceProductDetailModel.shopId.length?@"实体店":@"自营";
            CGFloat singleLineHeight = [channelString sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(16.)] constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)].height;
            // 2. 计算文字长度
            CGFloat textWidth = [self.dataSourceProductDetailModel.title sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(16.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, singleLineHeight)].width;
            // 3. 获取控件的需要高度
            NSInteger numberOfLine = textWidth / (self.view.bounds.size.width - 2 * mmh_relative_float(11));
            CGFloat excessWidth = textWidth - numberOfLine * (self.view.bounds.size.width - 2 * mmh_relative_float(11));

            CGSize channelContentOfSize = [channelString sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, singleLineHeight)];
            if (self.view.bounds.size.width - 2 * (MMHFloat(11)) - excessWidth > (channelContentOfSize.width + MMHFloat(6)+MMHFloat(20))){
                cellWithRowOneHeight += 0;
            } else {
                cellWithRowOneHeight +=channelContentOfSize.height;
            }
        }
        if (self.dataSourceProductDetailModel.subtitle.length){
               CGSize contentOfSize = [self.dataSourceProductDetailModel.subtitle sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(14.)] constrainedToSize:CGSizeMake(self.view.bounds.size.width - 2 * mmh_relative_float(11), CGFLOAT_MAX)];
            cellWithRowOneHeight += contentOfSize.height;
            cellWithRowOneHeight += MMHFloat(15);
        }
        
        return cellWithRowOneHeight;
        
        
        
        
    } else if (indexPath.section == 0 && indexPath.row == 1){
        return MMHFloat(70);
    } else if (indexPath.section == 1){
        if (self.dataSourceProductDetailModel.mamCare.length){
            if (self.dataSourceProductDetailModel.mamCare.length){
                if (self.mamSayIsOpen){                     // 打开状态
                    CGSize mamSaySize = [[NSString stringWithFormat:@"好妈妈说:%@",self.dataSourceProductDetailModel.mamCare]  sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(15.)] constrainedToSize:CGSizeMake(self.view.bounds.size.width - 70 - mmh_relative_float(11), CGFLOAT_MAX)];
                    return mamSaySize.height + 2 * MMHFloat(20);
                } else {
                    return 98;
                }
            } else {
                return 98;
            }
        } else {
            return 98;
        }
    
    } else if (indexPath.section == 2){
        if (indexPath.row == 1){
            return 20;
        } else if (indexPath.row == 4){
            return 45;
            
        } else {
            return mmh_relative_float(180 / 5.);
        }
    } else if (indexPath.section == 3){
        return mmh_relative_float(50);
    } else if (indexPath.section == 4 ){
        if (indexPath.row == 0){
            return MMHFloat(50);
        } else{
            
            if (self.womDataArray.count == 2) {
                MMHPublicPraiseFrameModel *model = self.womDataArray[indexPath.row - 1];
                return model.cellHeightF;
            }
            return MMHFloat(235);
        }
    } else if (indexPath.section == 5){
        return 50 + 180;
    }else {
        return mmh_relative_float(100 + 49);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return MMHFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.productDetailTableView) {
        if (!(indexPath.section == 2)){
            SeparatorType separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"login"];
        }
        

    }
}



#pragma mark - actionClick
-(void)buttonClick:(UIButton *)sender{
    UIButton *clickButton = (UIButton *)sender;
    if ([clickButton.stringTag isEqualToString:@"collection"]){             // 收藏
        [self animationWithCollectionWithButton:clickButton];
        if (self.dataSourceProductDetailModel.itemNo){
            [self clickZanWithDataWithProductId:self.dataSourceProductDetailModel.itemNo andisCollection:self.isCollection];
        } else {
            NSLog(@"请稍等加载");
        }
    } else if([clickButton.stringTag isEqualToString:@"share"]){            // 分享
        MMHActionSheetWithShareViewController *shareViewController = [[MMHActionSheetWithShareViewController alloc]init];
        shareViewController.isHasGesture = YES;

        [shareViewController showInView:self.parentViewController];
    } 
}

// 头像点击
-(void)headButtonClick:(UIButton *)sender{
    NSLog(@"点击头像");
}

// 后期服务按钮点击
-(void)serversButtonClick:(UIButton *)sender{
    NSLog(@"123");
    UIButton *serversButton = (UIButton *)sender;
    if ([serversButton.stringTag isEqualToString:@"storeDistributionButton"]){                  // 门店配送
        NSLog(@"门店配送");
    } else if ([serversButton.stringTag isEqualToString:@"sevenDaysReturnButton"]){             // 7天退货
        NSLog(@"7天退货");
    } else if ([serversButton.stringTag isEqualToString:@"extractionWithSelfButton"]){          // 自提
        NSLog(@"自提");
    } else if ([serversButton.stringTag isEqualToString:@"customerServiceButton"]) {             // 立刻购买
        if ([MMHChattingViewController isReadyToChat]) {
            MMHChattingViewController *chattingViewController = [[MMHChattingViewController alloc] initWithContext:self.dataSourceProductDetailModel];
            [self.navigationController pushViewController:chattingViewController animated:YES];
        }
        else {
            [self.view showProcessingViewWithMessage:@"准备中"];
            __weak __typeof(self) weakSelf = self;
            [MMHChattingViewController prepareForChattingWithContext:self.dataSourceProductDetailModel
                                                    succeededHandler:^(MMHChattingViewController *chattingViewController) {
                                                        [weakSelf.view hideProcessingView];
                                                        [weakSelf.navigationController pushViewController:chattingViewController animated:YES];
                                                    } failedHander:^(NSError *error) {
                                                        [weakSelf.view hideProcessingView];
                                                        [weakSelf.view showTips:@"无法联系到客服，请重试"];
                                                    }];
        }
    }
    else if ([serversButton.stringTag isEqualToString:@"shoppingCatButton"]) {                 // 加入购物车
        __weak typeof(self) weakSelf = self;
        MMHProductSpecSelectionViewController *specSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.dataSourceProductDetailModel specSelectedHander:^ {
            [weakSelf addToCart];
        }];
        MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
        [tabBarController presentFloatingViewController:specSelectionViewController animated:YES];
    }
    else if ([serversButton.stringTag isEqualToString:@"buyNowButton"]) {                      // 立即购买
        MMHProductSpecSelectionViewController *specSelectionViewController = [[MMHProductSpecSelectionViewController alloc] initWithProductDetail:self.dataSourceProductDetailModel specSelectedHander:^{
        }];
        MMHTabBarController *tabBarController = (MMHTabBarController *) self.tabBarController;
        [tabBarController presentFloatingViewController:specSelectionViewController animated:YES];
    }
}


- (void)addToCart
{
#ifndef DEBUG_LOUIS
    ProductSpecParameter *selectedSpecParameter = [self.dataSourceProductDetailModel.specFilter selectedSpecParameter];
    if (selectedSpecParameter == nil) {
        [self.view showTips:@"无法加入购物车，请检查网络"];
        return;
    }
#endif

    [self.view showProcessingViewWithMessage:@"正在加入购物车"];
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] addProductToCart:self.dataSourceProductDetailModel succeededHandler:^{
        [weakSelf.view hideProcessingView];
        [weakSelf.view showTips:@"已加入购物车"];
    } failedHandler:^(NSError *error) {
        [weakSelf.view hideProcessingView];
        // TODO: failed
    }];
}


// Nav消息按钮click
-(void)messageButtonClick{
   self.dataSourceProductDetailModel.title = [self.dataSourceProductDetailModel.title stringByAppendingString:@"是"];
    NSIndexPath *celIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.productDetailTableView reloadRowsAtIndexPaths:@[celIndex] withRowAnimation:UITableViewRowAnimationNone];
}

// 购物车按钮click
-(void)shoppingCarButtonClick{
    NSLog(@"购物车");
    __weak typeof(self)weakSelf = self;
//    [weakSelf sendRequestWithGetproductDetail];
    
}
#pragma mark - customView
-(void)createCustomButtonWithButton:(UIButton *)sender buttonTag:(NSString *)tag buttonImage:(NSString *)buttonImage buttonName:(NSString *)buttonName {
    [sender addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [sender setImage:[UIImage imageNamed:buttonImage] forState:UIControlStateNormal];
    [sender setImageEdgeInsets:UIEdgeInsetsMake(MMHFloat(-10),MMHFloat(15),MMHFloat(10),MMHFloat(15))];
    sender.stringTag = tag;
    sender.imageView.contentMode = UIViewContentModeScaleAspectFit;
    sender.adjustsImageWhenHighlighted = NO;
    [sender setTitle:buttonName forState:UIControlStateNormal];
    sender.titleLabel.font = [UIFont systemFontOfSize:MMHFloat(12.)];
    [sender setTitleEdgeInsets:UIEdgeInsetsMake(MMHFloat(30),-16,MMHFloat(10),10)];
    [sender setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    sender.backgroundColor = [UIColor clearColor];
}

#pragma mark 创建7天退货等button
-(void)createServerButtonWithCellHeight:(CGFloat)cellHeight andSubView:(UITableViewCell *)cell{
    CGFloat singleLineHeight = [[NSString stringWithFormat:@"文字"] sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(100, CGFLOAT_MAX)].height;
    CGFloat margin = mmh_relative_float(11) + 50 + mmh_relative_float(14);
    if (self.dataSourceProductDetailModel.tag.count){
        for (int i = 0 ; i < self.dataSourceProductDetailModel.tag.count; i++){
            NSString *buttonName =   [[self.dataSourceProductDetailModel.tag objectAtIndex:i] objectForKey:@"view"];
            NSString *buttonPicture = [[self.dataSourceProductDetailModel.tag objectAtIndex:i] objectForKey:@"pic"];
            // test
            NSLog(@"%@",buttonPicture);
            
            // 1. 计算文字的宽度
            CGSize contentOfSize = [buttonName sizeWithCalcFont:[UIFont systemFontOfSize:MMHFloat(12.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, singleLineHeight)];
            // 2. 创建UI
            // 2。1 . 创建view
            UIView *bgView = [[UIView alloc]init];
            bgView.backgroundColor = [UIColor clearColor];
            [cell addSubview:bgView];
            
            // 2.2 创建buttonIcon
            UIImageView *btnImageView = [[UIImageView alloc]init];
            btnImageView.backgroundColor = [UIColor clearColor];
            // 质检报告
            
            btnImageView.image = [UIImage imageNamed:@"login_thirdLogin_wechat"];
            [bgView addSubview:btnImageView];
            
            // 创建text
            UILabel *btnContentLabel = [[UILabel alloc]init];
            btnContentLabel.backgroundColor = [UIColor clearColor];
            btnContentLabel.textAlignment = NSTextAlignmentLeft;
            btnContentLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            btnContentLabel.numberOfLines = 1;
            btnContentLabel.font = [UIFont systemFontOfSize:MMHFloat(12)];
            btnContentLabel.text = buttonName;
            [bgView addSubview:btnContentLabel];
            
            // 重新设置bgViewFrame
            btnImageView.frame = CGRectMake(0, (cellHeight - MMHFloat(17)) / 2., MMHFloat(17), MMHFloat(17));
            btnContentLabel.frame = CGRectMake(MMHFloat(17 + 3), 0, contentOfSize.width, cellHeight);
            bgView.frame = CGRectMake(margin, 0, btnImageView.frame.size.width + btnContentLabel.frame.size.width + MMHFloat(5), cellHeight);
            margin = margin + btnImageView.frame.size.width + btnContentLabel.frame.size.width + MMHFloat(15);
        }
    }
}

-(void)goodsTitleLabel:(UITableViewCell *)cell{

}


#pragma mark 创建漂浮view
-(void)createFloatView{
    // 1. 创建背景view
    UIView *floatView = [[UIView alloc]init];
    floatView.backgroundColor = [UIColor hexChangeFloat:@"fafafa"];
    floatView.frame = CGRectMake(0,self.view.bounds.size.height - mmh_relative_float(49) - 64 , self.view.bounds.size.width, mmh_relative_float(49));
    [self.view addSubview:floatView];
    
    // 2. 创建lineView
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1];
    lineView.frame = CGRectMake(0, 0, self.view.bounds.size.width, .5f);
    [floatView addSubview:lineView];
    
    // 创建客服按钮
    UIButton *customerServiceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customerServiceButton= [UIButton buttonWithType:UIButtonTypeCustom];
    customerServiceButton.frame = CGRectMake(mmh_relative_float(11),0 ,100,49);
    [customerServiceButton addTarget:self action:@selector(serversButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    customerServiceButton.backgroundColor = [UIColor clearColor];
    NSString *detailString = @"客服";
    customerServiceButton.stringTag = @"customerServiceButton";
    CGSize detailContentOfSize = [detailString sizeWithCalcFont:[UIFont systemFontOfSize:mmh_relative_float(13.)] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    [customerServiceButton setImage:[UIImage imageNamed:@"login_thirdLogin_wechat"] forState:UIControlStateNormal];
    [customerServiceButton setImageEdgeInsets:UIEdgeInsetsMake(5, -detailContentOfSize.width + 20, 5, 0)];
    customerServiceButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    customerServiceButton.adjustsImageWhenHighlighted = NO;
    [customerServiceButton setTitle:detailString forState:UIControlStateNormal];
    customerServiceButton.titleLabel.font = [UIFont systemFontOfSize:mmh_relative_float(13.)];
    [customerServiceButton setTitleEdgeInsets:UIEdgeInsetsMake(5,-50, 5, 0)];
    [customerServiceButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [floatView addSubview:customerServiceButton];
    

    // 创建加入购物车
    UIButton *shoppingCatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shoppingCatButton.backgroundColor = [UIColor whiteColor];
    [shoppingCatButton setTitle:@"加入购物车" forState:UIControlStateNormal];
    [shoppingCatButton setTitleColor:[UIColor hexChangeFloat:@"666666"] forState:UIControlStateNormal];
    shoppingCatButton.titleLabel.font = [UIFont systemFontOfSize:16.];
    shoppingCatButton.stringTag = @"shoppingCatButton";
    [shoppingCatButton addTarget:self action:@selector(serversButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    shoppingCatButton.frame = CGRectMake(self.view.frame.size.width - mmh_relative_float(125 + 8) * 2  ,mmh_relative_float(4),mmh_relative_float(125),mmh_relative_float(40));
    shoppingCatButton.layer.cornerRadius = mmh_relative_float(10.0f);
    shoppingCatButton.layer.borderColor = [UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1].CGColor;
    shoppingCatButton.layer.borderWidth = .5f;
    [floatView addSubview:shoppingCatButton];
    
    // 创建立即购买
    UIButton *buyNowButton = [UIButton buttonWithType:UIButtonTypeCustom];
    buyNowButton.backgroundColor = [UIColor hexChangeFloat:@"fc687c"];
    [buyNowButton setTitle:@"立即购买" forState:UIControlStateNormal];
    [buyNowButton setTitleColor:[UIColor hexChangeFloat:@"ffffff"] forState:UIControlStateNormal];
    buyNowButton.titleLabel.font = [UIFont systemFontOfSize:16.];
    buyNowButton.stringTag = @"buyNowButton";
    [buyNowButton addTarget:self action:@selector(serversButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    buyNowButton.frame = CGRectMake(self.view.frame.size.width - mmh_relative_float(125 + 8)   ,mmh_relative_float(4),mmh_relative_float(125),mmh_relative_float(40));
    buyNowButton.layer.cornerRadius = mmh_relative_float(10.0f);
    [floatView addSubview:buyNowButton];
}


#pragma mark - UIScrollViewDelegate 
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.productImageViewScrollView){
        NSInteger page = scrollView.contentOffset.x/ self.view.bounds.size.width;
        self.dynamicPageLabel.text = [NSString stringWithFormat:@"%li",(long)page + 1];
    }
}

#pragma mark 动画
-(void)animationWithCollectionWithButton:(UIButton *)collectionButton{
    if (self.isCollection){
        [collectionButton setImage:[UIImage imageNamed:@"product_collection_nor"] forState:UIControlStateNormal];
    } else {
        [collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
    }
    CAKeyframeAnimation *collectionOfAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    collectionOfAnimation.values = @[@(0.1),@(1.0),@(1.5)];
    collectionOfAnimation.keyTimes = @[@(0.0),@(0.5),@(0.8),@(1.0)];
    collectionOfAnimation.calculationMode = kCAAnimationLinear;
    
    self.isCollection = !self.isCollection;
    [collectionButton.layer addAnimation:collectionOfAnimation forKey:@"SHOW"];
}



#pragma mark - 接口
//-(void)sendRequestWithGetproductDetail{
//    __weak typeof(self)weakSelf = self;
//    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailWithSingleProduct:self.singleProduct
//                                                                      from:self
//                                                          succeededHandler:^(MMHProductDetailModel *productDetail) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        
//        self.postIsLoading = YES;
//        strongSelf.dataSourceProductDetailModel = productDetail;
//                                                              NSLog(@"%@", productDetail);
//        [strongSelf.productDetailTableView reloadData];
//        [self searchDataIsHasZan:self.dataSourceProductDetailModel.itemNo];
//        
//    } failedHandler:^(NSError *error) {
//        
//    }];
//    

    
//}

#pragma mark 商品详情下的口碑

-(void)sendRequestWithGetWomOfGoodsDetail{

    [[MMHNetworkAdapter sharedAdapter] fetchWomOfProductDetailWithMemberId:1 goodsTemplated:00000010 page:1 count:2 from:nil succeededHandler:^(NSDictionary *dic) {
        self.WomMouthDic = dic;
        NSArray *array = dic[@"rows"];
        for (NSDictionary *dic in array) {
            MMHPublicPraiseModel *model = [[MMHPublicPraiseModel alloc] initWithJSONDict:dic];
            MMHPublicPraiseFrameModel *frameModel = [[MMHPublicPraiseFrameModel alloc] init];
            frameModel.praiseModel = model;
            [self.womDataArray addObject:frameModel];
            [self.productDetailTableView reloadData];
        }
    } failedHandler:^(NSError *error) {
        NSLog(@"%@",error);
    }];

}
#pragma mark 猜你喜欢
-(void)sendRequestWithGuessYouLike{
    __weak typeof(self)weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchProductDetailGuessYouLikeWithSingleProduct:self.singleProduct
                                                                                  from:nil
                                                                      succeededHandler:^(MMHGuessYouLikeListModel *guessYouLikeListModel) {
                                                                          if (!weakSelf){
                                                                              return ;
                                                                          }
                                                                          __strong typeof(weakSelf)strongSelf = weakSelf;
                                                                          strongSelf.guessYouLikeListModel = guessYouLikeListModel;
                                                                          [strongSelf.productDetailTableView reloadData];
                                                                      } failedHandler:^(NSError *error) {
                                                                          
                                                                      }];
}



#pragma mark  -

-(void)autoSettingFrameWith:(UILabel *)label andLabelString:(NSString *)labelString{
    CGFloat labelOriginX = label.frame.origin.x;
    CGFloat labelOriginY = label.frame.origin.y;
    CGFloat labelSizeWidth = label.frame.size.width;
//    CGFloat labelSizeHeight = label.frame.size.height;
    
    label.text = labelString;
    CGSize contentOfSize = [labelString sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(labelSizeWidth, CGFLOAT_MAX)];
    
    label.frame = CGRectMake(labelOriginX, labelOriginY, labelSizeWidth, contentOfSize.height);
    label.numberOfLines = 0;
}

#pragma mark 点赞逻辑
-(void)clickZanWithDataWithProductId:(NSString *)productId andisCollection:(BOOL)isCollection{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:clickZanPlistName];
    NSFileManager *fileManager=[NSFileManager defaultManager];
    bool ifFind=[fileManager fileExistsAtPath:path];
    NSString * srcPath= [[NSBundle mainBundle]pathForResource:clickZanPlistName ofType:nil];
    if(!ifFind) {               // 如果不存在
        [fileManager copyItemAtPath:srcPath toPath:path error:nil];
    }
    NSMutableDictionary *data1 = [[NSMutableDictionary alloc] initWithContentsOfFile:srcPath];
    NSArray *zanArray = [data1 objectForKey:@"zanArray"];
    NSMutableArray *zanMutableArray = [NSMutableArray arrayWithArray:zanArray];
    
    
    if (isCollection){              // 收藏
        [zanMutableArray addObject:productId];
    } else {
        for (int i = 0 ; i <zanMutableArray.count;i++){
            if ([[zanMutableArray objectAtIndex:i] isEqualToString:productId]){
                [zanMutableArray removeObject:productId];
                break;
            }
        }
    }
  
    // 3. 写入数据
    [data1 setObject:zanMutableArray forKey:@"zanArray"];
    [data1 writeToFile:srcPath atomically:YES];
}

-(void)searchDataIsHasZan:(NSString *)productId{
    NSString * srcPath= [[NSBundle mainBundle]pathForResource:clickZanPlistName ofType:nil];
    NSMutableDictionary *data1 = [[NSMutableDictionary alloc] initWithContentsOfFile:srcPath];
    NSArray *zanArray = [data1 objectForKey:@"zanArray"];
    for (int i = 0 ; i < zanArray.count ; i++){
        if ([[zanArray objectAtIndex:i] isEqualToString:productId]){
            [self.collectionButton setImage:[UIImage imageNamed:@"product_collection_hlt"] forState:UIControlStateNormal];
            self.isCollection = YES;
        }
    }
}


#pragma mark - MMHProductDetailModelDelegate methods


- (void)productDetailDidChange:(MMHProductDetailModel * __nonnull)productDetail
{
    // TODO: smartMin
    // update subviews
}


@end
