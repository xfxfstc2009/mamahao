//
//  MMHWebViewController.m
//  MamHao
//
//  Created by SmartMin on 15/4/13.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHWebViewController.h"
#import "MMHActionSheetWithShareViewController.h"
#import "MMHHTMLModel.h"
#import <AVFoundation/AVFoundation.h>

@interface MMHWebViewController()<UIWebViewDelegate>
@property (nonatomic,copy)NSString *titleString;
@property (nonatomic,copy)NSString *resource;
@property (nonatomic, assign) BOOL isRemote;

@end
@implementation MMHWebViewController

-(id)initWithResourceName:(NSString *)resoucePath title:(NSString *)title isRemote:(BOOL)fromRemote url:(NSString *)remoteURL{
    if (self = [super init]){
       self.barMainTitle = title;
        [self setIsRemote:fromRemote];
        [self setRemoteUrl:remoteURL];
        [self setResource:resoucePath];
    }
    return self;
}


- (instancetype)initWithURLString:(NSString *)urlString title:(NSString *)title {
    return [self initWithResourceName:nil
                                title:title
                             isRemote:YES
                                  url:urlString];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    
    [self actionConfigureH5WebView];
    if (!self.isRemote) {
        NSError *contentEror = nil;
        NSArray *resourceArr = [self.resource componentsSeparatedByString:@"."];
        NSString *resourceName = [resourceArr objectAtIndex:0];
        NSString *resourceType = [resourceArr objectAtIndex:1];
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:resourceName ofType:resourceType];
        NSString *resourceContent = [NSString stringWithContentsOfFile:resourcePath encoding:NSUTF8StringEncoding error:&contentEror];
        if (!contentEror) {
            [self.webView loadHTMLString:resourceContent baseURL:nil];
        } else {
            [self.webView loadHTMLString:[contentEror localizedDescription] baseURL:nil];
        }
    } else {
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.remoteUrl]];
        urlRequest.timeoutInterval = 20.;
        urlRequest.HTTPShouldHandleCookies = YES;
        urlRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
        [self.webView loadRequest:urlRequest];
    }
}


-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    self.webView.scalesPageToFit = YES;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.webView.isLoading){
        [self.webView stopLoading];
    }
}

-(void)actionConfigureH5WebView{
    if ([MMHTool isEmpty:self.titleString]){
        self.barMainTitle = self.titleString;
    }
    if (!self.webView){
        self.webView = [[UIWebView alloc]initWithFrame:self.view.bounds];
        self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.webView.delegate = self;
        self.webView.backgroundColor = [UIColor whiteColor];
        self.webView.scalesPageToFit = YES;
        self.webView.dataDetectorTypes = UIDataDetectorTypeAll;
        
//        UIDataDetectorTypes dataDetectorTypes = UIDataDetectorTypeLink;
//        self.webView.dataDetectorTypes = dataDetectorTypes;
    }
    [self.view addSubview:self.webView];

    if (!self.isRemote) {
        NSError *contentEror = nil;
        NSArray *resourceArr = [self.resource componentsSeparatedByString:@"."];
        NSString *resourceName = [resourceArr objectAtIndex:0];
        NSString *resourceType = [resourceArr objectAtIndex:1];
        NSString *resourcePath = [[NSBundle mainBundle] pathForResource:resourceName ofType:resourceType];
        NSString *resourceContent = [NSString stringWithContentsOfFile:resourcePath encoding:NSUTF8StringEncoding error:&contentEror];
        if (!contentEror) {
            [self.webView loadHTMLString:resourceContent baseURL:nil];
        } else {
            [self.webView loadHTMLString:[contentEror localizedDescription] baseURL:nil];
        }
    } else {
        if (self.remoteUrl){
            NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.remoteUrl]];
            urlRequest.timeoutInterval = 20.;
            urlRequest.HTTPShouldHandleCookies = YES;
            [self.webView loadRequest:urlRequest];
        }
    }
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self actionDidFinishLoad];
}
- (void)actionDidFinishLoad {
    
//    if (self.isRemote && [MMHTool isEmpty:self.barMainTitle]) {
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
//        if (self.title){
//            self.barMainTitle = self.title;
//        } else {
    self.barMainTitle = title;
//        }
    
//    }
}

#pragma mark 自定义方法
-(void)mamahaoHTMLManager:(NSString *)jsManager{
    if ([jsManager isEqualToString:@"share"]){
        [self shareWithAPP];
      [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(shareNotification:) name:MMHConfirmationNotificationWithShare object:nil];
    } else if ([jsManager isEqualToString:@"redirectionToHome"]){
        [MMHTabBarController redirectToHomeWithAnimation];
    } else if ([jsManager isEqualToString:@"shake"]){
        [self shake];
    }
}

-(void)shareNotification:(NSNotification *)note{
    NSString *shareStatus = [note.userInfo objectForKey:@"shareStatus"];
    if ([shareStatus isEqualToString:@"message"]){
        [self.view showTips:@"短信分享成功"];
    } else if ([shareStatus isEqualToString:@"copy"]){
        [self.view showTips:@"复制成功"];
    } else if ([shareStatus isEqualToString:@"wechat"]){
        [self.view showTips:@"微信分享成功"];
    } else if ([shareStatus isEqualToString:@"pengyouquan"]){
        [self.view showTips:@"朋友圈分享成功"];
    }
}

#pragma mark shake
-(void)shake{
    SystemSoundID shakeSoundsId = [self loadSound:@"shake.mp3"];
    AudioServicesPlaySystemSound(shakeSoundsId);
}

#pragma mark 分享
-(void)shareWithAPP{
    NSString *backString = @"";
    backString = [self.webView stringByEvaluatingJavaScriptFromString:@"APP.iOS.Share();"];
    
    // 解析
    NSDictionary *dataSource = [NSJSONSerialization JSONObjectWithData:[backString dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
    MMHHTMLModel *shareModel = [[MMHHTMLModel alloc]initWithJSONDict:[dataSource objectForKey:@"data"]];

    MMHActionSheetWithShareViewController *shareViewController = [[MMHActionSheetWithShareViewController alloc]init];
    shareViewController.isHasGesture = YES;
    shareViewController.transferShareModel = shareModel;
    [shareViewController showInView:self.parentViewController];
}

#pragma mark json解析


#pragma mark - 初始化音效
- (SystemSoundID)loadSound:(NSString *)soundName {
    NSURL *url = [[NSBundle mainBundle]URLForResource:soundName withExtension:nil];
    
    // 创建声音Id
    SystemSoundID soundId;
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(url), &soundId);
    
    return soundId;
}

@end
