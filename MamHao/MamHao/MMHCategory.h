//
//  MMHCategory.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


// TODO:
// http://stackoverflow.com/questions/21096515/objective-c-custom-class-instance-as-dictionary-key-returns-nil-when-key-exists
@interface MMHCategory : NSObject

@property (nonatomic, copy) NSString *categoryID;

- (NSDictionary *)parameters;

- (id)initWithCategoryID:(NSString *)categoryID;


@end
