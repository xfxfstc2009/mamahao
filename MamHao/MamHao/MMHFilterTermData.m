//
//  MMHFilterTermData.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/11.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFilterTermData.h"
#import "MMHCategory.h"
#import "MMHFilterTerm.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Product.h"
#import "UIImage+RenderedImage.h"
#import "MamHao-Swift.h"
#import "MMHShopFilter.h"
#import "MMHSort.h"


@interface MMHFilterTermData ()

@property (nonatomic, strong) NSMutableDictionary *categoryTerms;
@property (nonatomic, strong) NSMutableDictionary *shopTerms;
@property (nonatomic, strong) NSMutableDictionary *brandTerms;
@end


@implementation MMHFilterTermData


+ (MMHFilterTermData *)sharedData
{
    static MMHFilterTermData *_data = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _data = [[MMHFilterTermData alloc] init];
    });
    return _data;
}


- (instancetype)init
{
    self = [super init];
    if (self) {
        self.categoryTerms = [[NSMutableDictionary alloc] init];
        self.shopTerms = [[NSMutableDictionary alloc] init];
        self.brandTerms = [[NSMutableDictionary alloc] init];
    }
    return self;
}


- (void)fetchTermsForFilter:(MMHFilter *)filter succeededHandler:(void(^)(FilterTermInfo *termInfo))succeededHandler failedHandler:(void(^)(NSError *error))failedHandler
{
    __weak __typeof(self) weakSelf = self;
    [[MMHNetworkAdapter sharedAdapter] fetchTermsForFilter:filter
                                          succeededHandler:^(FilterTermInfo *termInfo) {
                                              [weakSelf saveTermInfo:termInfo forFilter:filter];
                                              succeededHandler(termInfo);
                                          } failedHandler:^(NSError *error) {
                                              failedHandler(error);
                                          }];
}


- (void)saveTermInfo:(FilterTermInfo *)termInfo forFilter:(MMHFilter *)filter
{
    switch (filter.module) {
        case MMHFilterModuleCategory:
            [self saveTermInfo:termInfo forCategory:filter.category];
            break;
        case MMHFilterModuleShop:
            [self saveTermInfo:termInfo forShopID:filter.shopID];
            break;
        case MMHFilterModuleBrand:
            [self saveTermInfo:termInfo forBrandID:filter.brandID];
            break;
        default:
            break;
    }
}


- (void)saveTermInfo:(FilterTermInfo *)termInfo forCategory:(MMHCategory *)category
{
    if ([self.categoryTerms hasKey:category.categoryID]) {
        return;
    }

    self.categoryTerms[category.categoryID] = termInfo;
}


- (void)saveTermInfo:(FilterTermInfo *)termInfo forBrandID:(NSString *)brandID
{
    if ([self.brandTerms hasKey:brandID]) {
        return;
    }
    
    self.brandTerms[brandID] = termInfo;
}


- (BOOL)isTermsDataAvailableForFilter:(MMHFilter *)filter
{
    switch (filter.module) {
        case MMHFilterModuleCategory:
            return [self.categoryTerms hasKey:filter.category.categoryID];
            break;
        case MMHFilterModuleShop:
            return [self.shopTerms hasKey:filter.shopID];
            break;
        case MMHFilterModuleBrand:
            return [self.brandTerms hasKey:filter.brandID];
            break;
        default:
            break;
    }
    return NO;
//    return [self.categoryTerms hasKey:category.categoryID];
}


- (NSInteger)numberOfTermClassesForFilter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];
    return [termInfo.termClasses count];
}


- (NSInteger)numberOfTermsAtSection:(NSInteger)section forFilter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];
    FilterTermClass *termClass = termInfo.termClasses[section];
    return [termClass.terms count];
}


- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section forFilter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];

    if (section < 0) {
        return nil;
    }
    if (section >= [termInfo.termClasses count]) {
        return nil;
    }

    FilterTermClass *termClass = termInfo.termClasses[section];

    if (index < 0) {
        return nil;
    }
    if (index >= [termClass.terms count]) {
        return nil;
    }

    return termClass.terms[index];
}


- (NSString *)titleForSection:(NSInteger)section filter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];
    FilterTermClass *termClass = termInfo.termClasses[section];
    return termClass.name;
}


- (MMHFilterTermType)termTypeAtSection:(NSInteger)section forFilter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];
    if (section < 0) {
        return MMHFilterTermTypeNormal;
    }
    if (section >= [termInfo.termClasses count]) {
        return MMHFilterTermTypeNormal;
    }
    FilterTermClass *termClass = termInfo.termClasses[section];
    return termClass.type;
}


- (BOOL)multiSelectionEnabledAtSection:(NSInteger)section forFilter:(MMHFilter *)filter
{
    FilterTermInfo *termInfo = [self termInfoForFilter:filter];
    FilterTermClass *termClass = termInfo.termClasses[section];
    switch (termClass.type) {
        case MMHFilterTermTypeNormal:
        case MMHFilterTermTypeBrand:
            return YES;
        case MMHFilterTermTypeCategory:
            return NO;
        default:
            return YES;
    }
    return YES;
}


- (FilterTermInfo *)termInfoForFilter:(MMHFilter *)filter
{
    switch (filter.module) {
        case MMHFilterModuleCategory:
            return self.categoryTerms[filter.category.categoryID];
            break;
        case MMHFilterModuleShop:
            return self.shopTerms[filter.shopID];
            break;
        case MMHFilterModuleBrand:
            return self.brandTerms[filter.brandID];
            break;
        default:
            break;
    }
    return nil;
}
@end


@implementation MMHFilterTermData (shop)


//- (void)fetchTermsForShopID:(NSString *)shopID succeededHandler:(void(^)(FilterTermInfo *termInfo))succeededHandler failedHandler:(void(^)(NSError *error))failedHandler
//{
//    __weak __typeof(self) weakSelf = self;
//    [[MMHNetworkAdapter sharedAdapter] fetchTermsForShopID:shopID
//                                          succeededHandler:^(FilterTermInfo *termInfo) {
//                                              [weakSelf saveTermInfo:termInfo forShopID:shopID];
//                                              succeededHandler(termInfo);
//                                          } failedHandler:^(NSError *error) {
//                failedHandler(error);
//            }];
//}


- (void)saveTermInfo:(FilterTermInfo *)termInfo forShopID:(NSString *)shopID
{
    if ([self.shopTerms hasKey:shopID]) {
        return;
    }
    
    self.shopTerms[shopID] = termInfo;
}


- (NSInteger)numberOfTermClassesForShopID:(NSString *)shopID
{
    FilterTermInfo *termInfo = self.shopTerms[shopID];
    return [termInfo.termClasses count];
}


- (NSInteger)numberOfTermsAtSection:(NSInteger)section forShopID:(NSString *)shopID
{
    FilterTermInfo *termInfo = self.shopTerms[shopID];
    FilterTermClass *termClass = termInfo.termClasses[section];
    return [termClass.terms count];
}


- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section forShopID:(NSString *)shopID
{
    FilterTermInfo *termInfo = self.shopTerms[shopID];

    if (section < 0) {
        return nil;
    }
    if (section >= [termInfo.termClasses count]) {
        return nil;
    }

    FilterTermClass *termClass = termInfo.termClasses[section];

    if (index < 0) {
        return nil;
    }
    if (index >= [termClass.terms count]) {
        return nil;
    }

    return termClass.terms[index];
}


- (NSString *)titleForSection:(NSInteger)section shopID:(NSString *)shopID
{
    FilterTermInfo *termInfo = self.shopTerms[shopID];
    FilterTermClass *termClass = termInfo.termClasses[section];
    return termClass.name;
}


- (MMHFilterTermType)termTypeAtSection:(NSInteger)section forShopID:(NSString *)shopID
{
    FilterTermInfo *termInfo = self.shopTerms[shopID];
    if (section < 0) {
        return MMHFilterTermTypeNormal;
    }
    if (section >= [termInfo.termClasses count]) {
        return MMHFilterTermTypeNormal;
    }
    FilterTermClass *termClass = termInfo.termClasses[section];
    return termClass.type;
}


@end
