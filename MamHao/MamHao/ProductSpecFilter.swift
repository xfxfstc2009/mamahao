//
//  ProductSpecFilter.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/16.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


struct ProductSpecProductItem {
    var itemID: String
    var indexes: [Int]
}


@objc protocol ProductSpecFilterDelegate: NSObjectProtocol {
    func productSpecFilter(productSpecFilter: ProductSpecFilter!, didUpdateWithSpecParameter specParameter: ProductSpecParameter!) -> Void
}


class ProductSpecFilter: NSObject {
    
    var delegate: ProductSpecFilterDelegate? = nil
    
    let productItems: [ProductSpecProductItem]
    
    let templateID: String
    var module = MMHProductModule.Category
    var shopId: String?
    var companyId: String?
    var warehouseId: String?
    
    let specs: [ProductSpec]
    
    var selectedIndexes: [Int]
    var quantity = 1
//
    var selectabilityForLastSection: [Bool] = [Bool]()

    var productSpecParameters: [ProductSpecParameter] = [ProductSpecParameter]()
    
    
    init(specInfoArray: [[String: AnyObject]], productDetail: MMHProductDetailModel) {
        var specs = [ProductSpec]()
        var productItems = [ProductSpecProductItem]()
        var selectedIndexes: [Int]? = nil
        for itemDictionary in specInfoArray {
            let itemID = itemDictionary.nonnullStringForKey("itemId")
            let specArray = itemDictionary["spec"] as! [[String: String]]
            let numberOfSpecs = specArray.count
            var indexes = [Int](count: numberOfSpecs, repeatedValue: -1)
            for specOptionDictionary in specArray {
                let optionName = specOptionDictionary["key"]!
                let optionValue = specOptionDictionary["value"]!
                
                // try to add spec data to 'specs' if not exist
                var specDataAdded = false
                for (i, spec) in enumerate(specs) {
                    if spec.name == optionName {
                        spec.addOptionIfNotExist(optionValue)
                        indexes[i] = spec.indexOfOption(optionValue)
                        specDataAdded = true
                        break
                    }
                }
                if !specDataAdded {
                    let spec = ProductSpec(optionName: optionName, optionValue: optionValue)
                    specs.append(spec)
                    indexes[specs.count - 1] = spec.indexOfOption(optionValue)
                }
            } // end of 'for specOptionDictionary in specArray'
            let productItem = ProductSpecProductItem(itemID: itemID, indexes: indexes)
            NSLog("++++++>> got item: %@, %@", itemID, indexes)
            productItems.append(productItem)
            if itemID == productDetail.itemId {
                selectedIndexes = indexes
            }
        }
        self.productItems = productItems
        self.specs = specs
        if selectedIndexes != nil {
            self.selectedIndexes = selectedIndexes!
        }
        else {
            self.selectedIndexes = [Int](count:specs.count, repeatedValue: -1)
        }
        self.templateID = productDetail.templateId
        self.module = productDetail.module
        if let shopId = productDetail.shopId {
            if count(shopId) == 0 {
                self.shopId = nil
            }
            else {
                self.shopId = shopId
            }
        }
        else {
            self.shopId = productDetail.shopId
        }
        if let companyId = productDetail.companyId {
            if count(companyId) == 0 {
                self.companyId = nil
            }
            else {
                self.companyId = companyId
            }
        }
        else {
            self.companyId = productDetail.companyId
        }
        if let warehouseId = productDetail.warehouseId {
            if count(warehouseId) == 0 {
                self.warehouseId = nil
            }
            else {
                self.warehouseId = warehouseId
            }
        }
        else {
            self.warehouseId = productDetail.warehouseId
        }

        
//        var lastSpecOptionCount: Int = 0
//        if let lastSpec = self.specs.last {
//            lastSpecOptionCount = lastSpec.options.count
//        }
//        self.selectabilityForLastSection = [Bool](count: lastSpecOptionCount, repeatedValue: false)
        self.productSpecParameters = [ProductSpecParameter]()
        
        super.init()
        
//        self.reloadSelectabilityForLastSection()
    }
    
    
    func numberOfSpecs() -> Int {
        return self.specs.count
    }
    
    
    func numberOfOptionsAtSpecIndex(index: Int) -> Int {
        let spec = self.specs[index]
        return spec.options.count
    }
    
    
//    func specAtIndex(index: Int) -> ProductSpec {
//        return self.specs[index]
//    }
    
    func nameOfOptionAtIndexPath(indexPath: NSIndexPath) -> String {
        let spec = self.specs[indexPath.section]
        return spec.options[indexPath.row]
    }
    
    
    func selectedOptionIndexOfSpecAtIndex(index: Int) -> Int {
        return self.selectedIndexes[index]
    }
    
    
    func nameOfSpecAtIndex(index: Int) -> String {
        let spec = self.specs[index]
        return spec.name
    }
    
    
    func setSelectedOptionIndex(optionIndex: Int, ofSpecAtIndex specIndex: Int) {
        self.selectedIndexes[specIndex] = optionIndex
        self.reloadSelectabilityForLastSection()
    }
    
    
    func unselectedSpecName() -> String? {
        for (i, selectedIndex) in enumerate(self.selectedIndexes) {
            if selectedIndex == -1 {
                let spec = self.specs[i]
                return spec.name
            }
        }
        return nil
    }
    
    
//    func allSpecsSelected() -> Bool {
//        return true
//    }
    
    
    func selectedSpecOptionNames() -> [String] {
        var names = [String]()
        for (i, selectedIndex) in enumerate(self.selectedIndexes) {
            if selectedIndex == -1 {
                continue
            }
            let spec = self.specs[i]
            let option = spec.options[selectedIndex]
            names.append(option)
        }
        return names
    }
    
    
//    func deselectAll() {
//        let count = self.selectedIndexes.count
//        self.selectedIndexes = [Int](count:count, repeatedValue: -1)
//    }
    
    
    private func reloadSelectabilityForLastSection() {
        if self.selectedIndexes.count == 0 {
            return
        }
        if self.selectedIndexes.count == 1 {
//            self.selectabilityForLastSection = self.stocks
            return
        }
        var finalIndex = 0
        for i in 0..<self.selectedIndexes.count - 1 {
            let optionCount = self.numberOfOptionsAtSpecIndex(i)
            let selectedIndex = self.selectedOptionIndexOfSpecAtIndex(i)
            if selectedIndex == -1 {
                var lastSpecOptionCount: Int = 0
                if let lastSpec = self.specs.last {
                    lastSpecOptionCount = lastSpec.options.count
                }
                self.selectabilityForLastSection = [Bool](count: lastSpecOptionCount, repeatedValue: false)
                return
            }
            else {
                finalIndex = finalIndex * optionCount + selectedIndex
            }
        }
        let optionCountOfLastSpec = self.numberOfOptionsAtSpecIndex(self.specs.count - 1)
        finalIndex *= optionCountOfLastSpec
        for j in 0..<optionCountOfLastSpec {
//            selectabilityForLastSection[j] = self.stocks[finalIndex + j]
        }
    }

}


extension ProductSpecFilter {
    
    internal func isCurrentSelectionAvailableForSale() -> Bool {
        for productItem in self.productItems {
            if productItem.indexes == self.selectedIndexes {
                return true
            }
        }
        return false
    }
    
    internal func selectDefaultIndexes() {
        let count = self.numberOfSpecs()
        let indexes = [Int](count: count, repeatedValue: 0)
        self.selectedIndexes = indexes
    }
    
    internal func hasAnySpecs() -> Bool {
        return self.specs.count != 0
    }
}


extension ProductSpecFilter { // TODO: - Louis - deal this extension
    
    internal func selectabilityForOptionAtIndex(index: Int, forSpecAtSection section: Int) -> Bool {
        return true
//        let specCount = self.specs.count
//        if specCount == 0 {
//            return false
//        }
//        if section != specCount - 1 {
//            return true
//        }
//        return self.selectabilityAtIndexForLastSection(index)
    }
    
    private func selectabilityAtIndexForLastSection(index: Int) -> Bool {
        if index < 0 {
            return false
        }
        if index >= self.selectabilityForLastSection.count {
            return false
        }
        let selectability = self.selectabilityForLastSection[index]
        return selectability
    }
    
}


class ProductSpecParameter: NSObject {
    var templateID: String
    var itemID: String
    var shopID: String?
    var companyID: String?
    var warehouseID: String?
    var price: MMHPrice
    var originalPrice: MMHPrice
    var beanPrice: Int
    var imageURLString: String?
    
    var goodsTag: [[String: String]]?
    var deliveryTime: [String: AnyObject]?
    var shopName: String?
    var shopType: Int
    var graphicDetails: String?
    var quality: String?
    
    var mBeanPay: Int?
    var purchaseQuantity: Int?
    
    var indexPath: [Int]
    
    init(dictionary: [String: AnyObject], indexPath: [Int], templateID: String) {
        self.itemID = dictionary.nonnullStringForKey("itemId")
        self.shopID = dictionary.stringForKey("shopId")
        self.companyID = dictionary.stringForKey("companyId")
        self.warehouseID = dictionary.stringForKey("warehouseId")
        self.price = dictionary.nonnullPriceForKey("price")
        self.originalPrice = dictionary.nonnullPriceForKey("originalPrice")
        self.beanPrice = dictionary.nonnullIntForKey("mBeanPay")
        
        let cocoaDictionary = dictionary as NSDictionary
        if cocoaDictionary.hasKey("pic") {
            self.imageURLString = dictionary.nonnullStringForKey("pic")
        }
        else if cocoaDictionary.hasKey("itemPic") {
            self.imageURLString = dictionary.nonnullStringForKey("itemPic")
        }
        else {
            self.imageURLString = ""
        }
        
        self.goodsTag = dictionary["goodsTag"] as? [[String: String]]
        self.deliveryTime = dictionary["deliveryTime"] as? [String: AnyObject]
        self.shopName = dictionary.stringForKey("shopName")
        if let shopTypeNumber = dictionary["shopType"] as? NSNumber {
            self.shopType = shopTypeNumber.integerValue
        }
        else {
            self.shopType = -1
        }
        
//        self.shopType = dictionary.nonnullIntForKey("shopType")// dictionary["shopType"] as! Int
        self.graphicDetails = dictionary.stringForKey("graphicDetails")
        self.quality = dictionary.stringForKey("quality")
        
        self.mBeanPay = dictionary.intForKey("mBeanPay")
        self.purchaseQuantity = dictionary.intForKey("purchaseQuantity")
        
        self.indexPath = indexPath
        self.templateID = templateID
        
        super.init()
    }
}


extension ProductSpecFilter {
    
    internal func selectedItemID() -> String {
        let selectedIndexes = self.selectedIndexes
        for item in self.productItems {
            if item.indexes == selectedIndexes {
                return item.itemID
            }
        }
        let firstItem = self.productItems.first
        return firstItem!.itemID
    }
    
    internal func isAllSpecSelected() -> Bool {
        for (i, selectedIndex) in enumerate(self.selectedIndexes) {
            if selectedIndex == -1 {
                return false
            }
        }
        return true
    }

    internal func parameterAtIndexPath(indexPath: [Int]) -> ProductSpecParameter? {
        for (i, parameter) in enumerate(self.productSpecParameters) {
            if parameter.indexPath == indexPath {
                return parameter
            }
        }
        return nil;
    }
    
    internal func selectedSpecParameter() -> ProductSpecParameter? {
        if let specParameter = self.parameterAtIndexPath(self.selectedIndexes) {
            if specParameter.shopID != nil {
                self.shopId = specParameter.shopID!
            }
            if specParameter.companyID != nil {
                self.companyId = specParameter.companyID
            }
            if specParameter.warehouseID != nil {
                self.warehouseId = specParameter.warehouseID
            }
//            self.productSpecParameters.append(specParameter)
            if let delegate = self.delegate {
                delegate.productSpecFilter(self, didUpdateWithSpecParameter: specParameter)
            }
            return specParameter
        }
        return nil
    }
    
    internal func isQuantityExceedsLimit(quantity: Int) -> Bool {
        if let parameter = self.selectedSpecParameter() {
            if let limit = parameter.purchaseQuantity {
                return quantity > limit
            }
        }
        return false
    }
    
//    internal func jsonTerm() -> String {
//        var array = [[String: AnyObject]]()
//        
//        for (i, spec) in enumerate(self.specs) {
//            let selectedIndex = self.selectedIndexes[i]
////            let key = spec.key
////            let value = spec.options[selectedIndex]
////            array.append(["key": key, "value": value])
//        }
//        
//        return (array as NSArray).JSONString()
//    }
    
    internal func fetchParameterForCurrentSelectionWithCompletion(completion: (Bool, ProductSpecParameter?) -> Void) {
        if !self.isAllSpecSelected() {
            completion(false, nil);
            return;
        }
        
        var indexPath = self.selectedIndexes
        
        MMHNetworkAdapter.sharedAdapter().fetchProductDetailSpecParameterWithProductDetailSpecFilter(self, succeededHandler: { (responseDictionary: [NSObject : AnyObject]) -> Void in
            let specParameter = ProductSpecParameter(dictionary: (responseDictionary as! [String: AnyObject]), indexPath: indexPath, templateID: self.templateID)
            if specParameter.shopID != nil {
                self.shopId = specParameter.shopID!
            }
            if specParameter.companyID != nil {
                self.companyId = specParameter.companyID
            }
            if specParameter.warehouseID != nil {
                self.warehouseId = specParameter.warehouseID
            }
            self.productSpecParameters.append(specParameter)
            if let delegate = self.delegate {
                delegate.productSpecFilter(self, didUpdateWithSpecParameter: specParameter)
            }
            completion(true, specParameter)
            }, failedHandler: { (error: NSError!) -> Void in
                println("\(error)")
                completion(false, nil)
        })
    }
}


extension MMHProductDetailModel: ProductSpecFilterDelegate {
    func productSpecFilter(productSpecFilter: ProductSpecFilter!, didUpdateWithSpecParameter specParameter: ProductSpecParameter!) {
        self.itemId = specParameter.itemID
        if let shopID = specParameter.shopID {
            self.shopId = shopID
        }
        if let companyID = specParameter.companyID {
            self.companyId = companyID
        }
        if let warehouseID = specParameter.warehouseID {
            self.warehouseId = warehouseID
        }
        self.price = specParameter.price
        self.originalPrice = specParameter.originalPrice
        self.configureGoodsTagWithArray(specParameter.goodsTag)
//        if let goodsTagDictionary = specParameter.goodsTag as? NSDictionary {
//            self.goodsTag = goodsTagDictionary.modelArrayOfClass(MMHProductDetailGoodsTagModel.classForCoder())
//        }
        if let deliveryTime = specParameter.deliveryTime {
            self.deliveryTime = MMHDeliveryTimeModel(JSONDict: deliveryTime, keyMap: nil)
        }
        if let shopName = specParameter.shopName {
            self.shop = shopName
        }
        let shopType = specParameter.shopType
        if shopType != -1 {
            self.shopType = shopType
        }
        if let graphicDetails = specParameter.graphicDetails {
            self.graphicDetails = graphicDetails
        }
        if let quality = specParameter.quality {
            self.quality = quality
        }
        
        if let mBeanPay = specParameter.mBeanPay {
            self.mBeanPay = mBeanPay
        }
        if let purchaseQuantity = specParameter.purchaseQuantity {
            self.purchaseQuantity = purchaseQuantity
        }
        
        self.delegate?.productDetailDidChange(self)
    }
}


