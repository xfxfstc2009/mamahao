//
//  MMHTimeline.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHProductList.h"
#import "MMHTimeline.h"
#import "MMHLogger.h"
#import "UIView+MMHPrompt.h"


@interface MMHTimeline ()

@property (nonatomic, strong) NSMutableArray *items;
@property (nonatomic) NSInteger nextPage;
@property (nonatomic) BOOL allItemsFetched;
@end


@implementation MMHTimeline


- (NSMutableArray *)items
{
    if (_items == nil) {
        _items = [NSMutableArray array];
    }
    return _items;
}


- (NSInteger)pageSize
{
    return 20;
}


- (NSInteger)numberOfItems
{
    return self.items.count;
}


- (id)itemAtIndex:(NSInteger)index
{
    return [self.items nullableObjectAtIndex:index];
}


- (BOOL)hasMoreItems
{
    return !self.allItemsFetched;
}


- (void)refetch
{
    __weak __typeof(self) weakSelf = self;
    __weak id <MMHTimelineDelegate> delegate = self.delegate;

    [self.items removeAllObjects];
    self.nextPage = 0;
    [self fetchItemsAtPage:self.nextPage succeededHandler:^(NSArray *fetchedItems) {
        if (fetchedItems.count < self.pageSize) {
            weakSelf.allItemsFetched = YES;
        }
        weakSelf.items = [fetchedItems mutableCopy];
        weakSelf.nextPage++;
        if ([delegate respondsToSelector:@selector(timelineDataRefetched:)]) {
            [delegate timelineDataRefetched:weakSelf];
        }
    } failedHandler:^(NSError *error) {
        if ([delegate respondsToSelector:@selector(timeline:fetchingFailed:)]) {
            [delegate timeline:weakSelf fetchingFailed:error];
        }
    }];
}


- (void)fetchMore {
    __weak __typeof(self) weakSelf = self;
    __weak id <MMHTimelineDelegate> delegate = self.delegate;

    [self fetchItemsAtPage:self.nextPage succeededHandler:^(NSArray *fetchedItems) {
        if (fetchedItems.count < self.pageSize) {
            weakSelf.allItemsFetched = YES;
        }
        [weakSelf.items addObjectsFromArray:fetchedItems];
        weakSelf.nextPage++;
        if ([delegate respondsToSelector:@selector(timelineMoreDataFetched:)]) {
            [delegate timelineMoreDataFetched:weakSelf];
        }
    } failedHandler:^(NSError *error) {
        if ([delegate respondsToSelector:@selector(timeline:fetchingFailed:)]) {
            [delegate timeline:weakSelf fetchingFailed:error];
        }
    }];
}


- (void)fetchItemsAtPage:(NSInteger)page succeededHandler:(void (^)(NSArray *fetchedItems))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler
{
    NSError *error = [NSError errorWithDomain:MMHErrorDomain code:-1 userInfo:@{NSLocalizedDescriptionKey: @"Unable to fetch data"}];
    failedHandler(error);
}


@end
