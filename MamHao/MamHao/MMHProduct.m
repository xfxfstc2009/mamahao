//
//  MMHProduct.m
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Crashlytics/Crashlytics.h>
#import "MMHProduct.h"
#import "MMHNetworkAdapter.h"
#import "MMHNetworkAdapter+Product.h"
#import "MMHNetworkAdapter+Center.h"
#import "MMHCurrentLocationModel.h"



@implementation MMHProduct


- (instancetype)initWithJSONDict:(NSDictionary *)dict
{
    if (self = [super initWithJSONDict:dict]) {
        if ([dict hasKey:@"STYLE_NUM_ID"]) {
            self.templateId = [dict stringForKey:@"STYLE_NUM_ID"];
        }
        if ([dict hasKey:@"styleNumId"]) {
            self.templateId = [dict stringForKey:@"styleNumId"];
        }

    }
    return self;
}


- (void)setActualPrice:(MMHPrice)actualPrice
{
    _actualPrice = actualPrice;
    id <MMHProductDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(product:actualPriceFetched:)]) {
        [delegate product:self actualPriceFetched:actualPrice];
    }
}


- (void)tryToFetchActualPrice
{
    if (!self.hasOwnPrice) {
        return;
    }

    [[MMHNetworkAdapter sharedAdapter] fetchActualPriceForProduct:self
                                                             from:self
                                                 succeededHandler:^(MMHPrice price) {
                                                     self.actualPrice = price;
                                                 } failedHandler:^(NSError *error) {
                                                     
                                                 }];
};


- (MMHCategory *)category
{
    return nil;
}


- (NSString *)shopId
{
    return nil;
}


@end
