//
//  MMHFilter.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFilterTermData.h"


typedef NS_ENUM(NSInteger, MMHProductModule) {
    MMHProductModuleShop = 1,                       /**< 1【店铺的商品列表进入】默认返回第一个商品信息*/
    MMHProductModuleMamhaoBeanSpecial,              /**< 2【麻豆商品列表进入】单独的表，默认返回第一个商品信息*/
    MMHProductModulePreOrder,                       /**< 3【预售商品列表进入】单独的表-暂时不做*/
    MMHProductModuleOrder,                          /**< 4【订单列表进入】*/
    MMHProductModuleComment,                        /**< 5【口碑列表进入】*/
    MMHProductModuleCategory,                       /**< 6【类目商品列表进入】默认返回第一个商品信息*/
    MMHProductModuleScanning,                       /**< 7 扫码进入*/
    MMHProductModuleFavourite = MMHProductModuleCategory,                      /**< 8【收藏列表进入*/
    MMHProductModuleMamhaoUserFeatured = MMHProductModuleCategory,
    MMHProductModuleMamhaoFeatured = MMHProductModuleComment,
    MMHProductModuleGuessYouLike = MMHProductModuleCategory,
    MMHProductModuleCart = MMHProductModuleComment,
    MMHProductModuleProductDetailShop = MMHProductModuleComment,
};


typedef NS_ENUM(NSInteger, MMHFilterModule) {
    MMHFilterModuleCategory = 1,
    MMHFilterModuleSearching,
    MMHFilterModuleShop,
    MMHFilterModuleBrand,
};



@class MMHCategory;
@class MMHSort;
@class MMHFilter;
@class FilterTerm;


@protocol MMHFilterDelegate <NSObject>

- (void)filterDidChange:(MMHFilter *)filter;

@end


@interface MMHFilter : NSObject

@property (nonatomic, weak) id<MMHFilterDelegate> delegate;
@property (nonatomic, readonly, strong) MMHSort *sort;
@property (nonatomic, readonly, strong) MMHCategory *category;
@property (nonatomic, copy) NSString *shopID;
@property (nonatomic, copy) NSString *shopGroupID;
@property (nonatomic, copy) NSString *brandID;
@property (nonatomic) MMHFilterModule module;
@property (nonatomic, copy) NSString *keyword;
- (MMHFilter *)initWithCategory:(MMHCategory *)category;
- (id)initWithKeyword:(NSString *)keyword;
+ (MMHFilter *)filterWithShopID:(NSString *)shopID;
+ (MMHFilter *)filterWithShopID:(NSString *)shopID groupID:(NSString *)shopGroupID;
+ (MMHFilter *)filterWithBrandID:(NSString *)brandID;

- (NSDictionary *)parametersForTermInfo;

- (NSDictionary *)parameters;

- (void)fetchTermsDataWithFinishedHandler:(void (^)(BOOL succeeded))finishedHandler;
- (BOOL)isTermsDataAvailable;

//- (NSInteger)selectedTermIndexAtSection:(NSInteger)section;
- (NSArray *)selectedIndexPaths;
- (BOOL)isIndexPathSelected:(NSIndexPath *)indexPath;
- (void)selectTermAtIndexPath:(NSIndexPath *)indexPath;
- (void)deselectTermAtIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)numberOfTermClasses;
- (NSInteger)numberOfTermsAtSection:(NSInteger)section;
- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section;

- (NSString *)titleForSection:(NSInteger)section;
- (MMHFilterTermType)termTypeAtSection:(NSInteger)section;

- (void)confirmTermChangesWithIndexPaths:(NSArray *)indexPaths;

- (NSString *)termString;

- (BOOL)multiSelectionEnabledAtSection:(NSInteger)section;

- (BOOL)hasFilterTerms;
@end
