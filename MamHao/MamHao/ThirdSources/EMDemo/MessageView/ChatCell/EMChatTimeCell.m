/************************************************************
  *  * EaseMob CONFIDENTIAL 
  * __________________ 
  * Copyright (C) 2013-2014 EaseMob Technologies. All rights reserved. 
  *  
  * NOTICE: All information contained herein is, and remains 
  * the property of EaseMob Technologies.
  * Dissemination of this information or reproduction of this material 
  * is strictly forbidden unless prior written permission is obtained
  * from EaseMob Technologies.
  */

#import "EMChatTimeCell.h"
#import "NSDate+Category.h"


@implementation EMChatTimeCell


- (void)setDate:(NSDate *)date
{
    _date = date;

    [self.dateLabel setSingleLineText:[date formattedTime] constrainedToWidth:mmh_screen_width() withEdgeInsets:UIEdgeInsetsMake(0.0f, 7.0f, 0.0f, 7.0f)];
    [self.dateLabel setHeight:20.0f];
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    self.backgroundColor = [UIColor clearColor];
//    self.textLabel.backgroundColor = [UIColor clearColor];
//    self.textLabel.textAlignment = NSTextAlignmentCenter;
//    self.textLabel.font = [UIFont systemFontOfSize:14];
//    self.textLabel.textColor = [UIColor grayColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;

    CGFloat dateLabelHeight = 20.0f;
    CGFloat cellHeight = 40.0f;
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, ceilf((cellHeight - dateLabelHeight) * 0.5f), mmh_screen_width(), dateLabelHeight)];
    dateLabel.backgroundColor = [UIColor colorWithHexString:@"d4d4d4"];
    dateLabel.textColor = [UIColor whiteColor];
    dateLabel.font = [UIFont systemFontOfSize:12.0f];
    dateLabel.layer.cornerRadius = 3.0f;
    dateLabel.clipsToBounds = YES;
    dateLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:dateLabel];
    self.dateLabel = dateLabel;
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


+ (CGFloat)defaultHeight
{
    return 40.0f;
}


-(void)layoutSubviews
{
    [super layoutSubviews];
    
}
@end
