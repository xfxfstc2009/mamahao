//
//  LESAlertView.m
//  Special Chinese Converter
//
//  Created by Louis Zhu on 13-4-25.
//  Copyright (c) 2013 L'epinardsoft. All rights reserved.
//

#import "LESAlertView.h"


@interface LESAlertView () <UIAlertViewDelegate>

@property (nonatomic, strong) NSMutableArray *handlers;

@end


@implementation LESAlertView


- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
          cancelButtonHandler:(dispatch_block_t)cancelButtonHandler
{
    self = [self initWithTitle:title
                       message:message
                      delegate:self
             cancelButtonTitle:cancelButtonTitle
             otherButtonTitles:nil];
    if (self) {
        if (cancelButtonTitle == nil) {
            self.handlers = [NSMutableArray array];
        }
        else {
            if (cancelButtonHandler == nil) {
                self.handlers = [NSMutableArray arrayWithObject:^{}];
            }
            else {
                self.handlers = [NSMutableArray arrayWithObject:cancelButtonHandler];
            }
        }
    }
    return self;
}


+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
  cancelButtonHandler:(dispatch_block_t)cancelButtonHandler
{
    LESAlertView *alertView = [[self alloc] initWithTitle:title
                                                  message:message
                                        cancelButtonTitle:cancelButtonTitle
                                      cancelButtonHandler:cancelButtonHandler];
    [alertView show];
}


+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
  cancelButtonHandler:(dispatch_block_t)cancelButtonHandler
   confirmButtonTitle:(NSString *)confirmButtonTitle
 confirmButtonHandler:(dispatch_block_t)confirmButtonHandler
{
    LESAlertView *alertView = [[self alloc] initWithTitle:title
                                                  message:message
                                        cancelButtonTitle:cancelButtonTitle
                                      cancelButtonHandler:cancelButtonHandler];
    [alertView addButtonWithTitle:confirmButtonTitle handler:confirmButtonHandler];
    [alertView show];
}


- (void)addButtonWithTitle:(NSString *)title handler:(dispatch_block_t)handler
{
    if (title == nil) {
        return;
    }
    
    if (handler == nil) {
        return;
    }
    
    [self addButtonWithTitle:title];
    [self.handlers addObject:[handler copy]];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    dispatch_block_t handler = self.handlers[buttonIndex];
    handler();
}


@end
