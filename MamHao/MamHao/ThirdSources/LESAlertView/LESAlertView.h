//
//  LESAlertView.h
//  Special Chinese Converter
//
//  Created by Louis Zhu on 13-4-25.
//  Copyright (c) 2013 L'epinardsoft. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LESAlertView : UIAlertView

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
            cancelButtonTitle:(NSString *)cancelButtonTitle
          cancelButtonHandler:(dispatch_block_t)cancelButtonHandler;

+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
  cancelButtonHandler:(dispatch_block_t)cancelButtonHandler;

+ (void)showWithTitle:(NSString *)title
              message:(NSString *)message
    cancelButtonTitle:(NSString *)cancelButtonTitle
  cancelButtonHandler:(dispatch_block_t)cancelButtonHandler
   confirmButtonTitle:(NSString *)confirmButtonTitle
 confirmButtonHandler:(dispatch_block_t)confirmButtonHandler;

- (void)addButtonWithTitle:(NSString *)title handler:(dispatch_block_t)handler;

@end
