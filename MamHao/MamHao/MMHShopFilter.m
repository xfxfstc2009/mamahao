//
//  MMHShopFilter.m
//  MamHao
//
//  Created by Louis Zhu on 15/6/28.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHShopFilter.h"


@implementation MMHShopFilter


- (id)initWithShopID:(NSString *)shopID groupID:(NSString *)groupID
{
    self = [super init];
    if (self) {
        self.shopID = shopID;
        self.shopGroupID = groupID;
        self.module = MMHFilterModuleShop;
    }
    return self;
}


- (NSInteger)numberOfTermClasses
{
    return [[MMHFilterTermData sharedData] numberOfTermClassesForShopID:self.shopID];
}


- (NSInteger)numberOfTermsAtSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] numberOfTermsAtSection:section forShopID:self.shopID];
}


- (FilterTerm *)termAtIndex:(NSInteger)index atSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] termAtIndex:index atSection:section forShopID:self.shopID];
}


- (NSString *)titleForSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] titleForSection:section shopID:self.shopID];
}


- (MMHFilterTermType)termTypeAtSection:(NSInteger)section
{
    return [[MMHFilterTermData sharedData] termTypeAtSection:section forShopID:self.shopID];
}


@end
