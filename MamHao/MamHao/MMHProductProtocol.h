//
//  MMHProductProtocol.h
//  MamHao
//
//  Created by Louis Zhu on 15/6/17.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHFilter.h"


typedef NS_ENUM(NSInteger, MMHProductDetailSaleType) {
    MMHProductDetailSaleTypeRMB,                          /**< 人民币商品*/
    MMHProductDetailSaleTypeMBean,                        /**< 妈豆商品*/
    MMHProductDetailSaleTypeScanning,                     /**< 扫码进入*/
};


@protocol MMHProductProtocol <NSObject>

@required
@property (nonatomic, readonly) MMHProductModule module;
@property (nonatomic, readonly) BOOL bindsShop;

@optional
@property (nonatomic, readonly) MMHProductDetailSaleType productDetailSaleType;
- (BOOL)isBeanProduct;

@required
- (NSDictionary *)parameters;

@end
