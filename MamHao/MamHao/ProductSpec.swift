//
//  ProductSegment.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


class ProductSpec: NSObject {
    
//    let key: Int
    let name: String
    var options: [String]
    
//    let itemId: String
    
    
//    init(dictionary: [String: AnyObject]) {
//        self.key = (dictionary["key"] as! NSNumber).integerValue
//        self.name = dictionary["name"] as! String
//        self.options = dictionary["data"] as! [String]
//        
//        super.init()
//    }
    
    init(optionName: String, optionValue: String) {
        self.name = optionName
        self.options = [optionValue]
        NSLog("------>>init %@, add %@", self.name, self.options)
        super.init()
    }
    
    func addOptionIfNotExist(optionValue: String) {
        let op = self.options as NSArray
        if !op.containsObject(optionValue) {
            self.options.append(optionValue)
        NSLog("------>>add %@, value %@", self.name, optionValue)
        }
    }
    
    func indexOfOption(optionValue: String) -> Int {
        let op = self.options as NSArray
        return op.indexOfObject(optionValue)
    }
    
}
