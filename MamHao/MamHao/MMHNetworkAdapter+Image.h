//
//  MMHNetworkAdapter+Image.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/3.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHNetworkAdapter.h"


@interface MMHNetworkAdapter (Image)

- (void)fetchOSSTokenWithContentString:(NSString *)contentString from:(id)requester succeededHandler:(void (^)(NSString *token))succeededHandler failedHandler:(void (^)(NSError *error))failedHandler;

- (void)uploadImage:(UIImage *)image from:(id)requester succeededHandler:(void(^)(NSString *fileID))succeededHandler failedHandler:(MMHNetworkFailedHandler)failedHandler;

@end
