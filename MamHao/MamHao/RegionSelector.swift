//
//  RegionSelector.swift
//  MamHao
//
//  Created by Louis Zhu on 15/4/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

import Foundation


@objc protocol RegionSelectorDelegate {
    func regionSelector(regionSelector: RegionSelector, didSelectRegionID regionID: Int, regionDisplayName: String)
}


class RegionSelector: NSObject {
    
    weak var delegate: RegionSelectorDelegate?
    
    var regionData: [String]
    var selectedIndex: Int?
//    = [String]()
//        {
//        NSLog("loading the region data")
//        
//        let path = NSBundle.mainBundle().pathForResource("regions", ofType: "txt")
//        let content = NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
//        NSLog("cou::: %@", content!)
//        let array = content?.componentsSeparatedByString("\n") as! [String]
//        
//        for string in array {
//            
//        }
//
//        return [String]()
//    }
    
    
    override init() {
        let path = NSBundle.mainBundle().pathForResource("regions", ofType: "txt")
        let content = NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
//        NSLog("cou::: %@", content!)
        let array = content?.componentsSeparatedByString("\n") as! [String]
        
        self.regionData = array
        
        super.init()
    }
    
    
    internal func levelNeedsToSelect() -> Int {
        if let selectedLevel = self.selectedLevel() {
            return selectedLevel + 1
        }
        else {
            return 0
        }
    }
    
    
    private func selectedLevel() -> Int? {
        if self.selectedIndex == nil {
            return nil
        }
        
        let selectedRawString = self.regionData[self.selectedIndex!]
        return self.levelOfRawString(selectedRawString)
    }
    
    
    private func levelOfRawString(rawString: String) -> Int {
        var numberOfBrackets = 0
        for character in rawString {
            if character == "[" {
                numberOfBrackets++
            }
            else {
                break
            }
        }
        return numberOfBrackets - 1
    }
    
    
    internal func numberOfItemsForLevel(level: Int) -> Int {
        let selectedIndexForPreviousLevel = self.selectedIndexForLevel(level - 1)
        let startIndex = selectedIndexForPreviousLevel + 1
        
        var count = 0
        for i in startIndex..<self.regionData.count {
            let l = self.levelAtIndex(i)
            if l == level {
                count++
            }
            else if l < level {
                break
            }
        }
        
        return count
    }
    
    
    internal func itemIndexAtRow(row: Int, forLevel level: Int) -> Int {
        let selectedIndexForPreviousLevel = self.selectedIndexForLevel(level - 1)
        let startIndex = selectedIndexForPreviousLevel + 1
        
        var count = 0
        for i in startIndex..<self.regionData.count {
            let l = self.levelAtIndex(i)
            if l == level {
                count++
            }
            else if l < level {
                break
            }
            
            if count == row + 1 {
                return i
            }
        }
        
        return NSNotFound
    }
    
    
    private func levelAtIndex(index: Int) -> Int {
        let rawString = self.regionData[index]
        return self.levelOfRawString(rawString)
    }
    
    
    private func selectedIndexForLevel(level: Int) -> Int {
        if let index = self.selectedIndex {
            for var i = index; i >= 0; i-- {
                if self.levelAtIndex(i) == level {
                    return i
                }
            }
        }
        else {
            return -1
        }
        return -1
    }
    
    
    internal func itemNameAtIndex(index: Int) -> String {
        let rawString = self.regionData[index]
        return self.itemNameOfRawString(rawString)
    }
    
    
    private func itemNameOfRawString(rawString: String) -> String {
        let string = rawString.stringByReplacingOccurrencesOfString("[", withString: "").stringByReplacingOccurrencesOfString("]", withString: "").stringByReplacingOccurrencesOfString(" ", withString: "")
        return string
    }
    
    
    internal func indexSelected(index: Int) {
        self.selectedIndex = index
        if !self.hasNextLevelToSelect() {
            var displayNameComponents = [String]()
            var currentLevel = self.levelAtIndex(index)
            for var i = index; i >= 0; i-- {
                if self.levelAtIndex(i) == currentLevel {
                    displayNameComponents.insert(self.itemNameAtIndex(i), atIndex: 0)
                    currentLevel--
                    if currentLevel < 0 {
                        break
                    }
                }
            }
            let xxx = displayNameComponents as NSArray
            let displayNameString = xxx.componentsJoinedByString(" ")
            self.delegate?.regionSelector(self, didSelectRegionID: index, regionDisplayName: displayNameString)
        }
    }
    
    
    internal func hasNextLevelToSelect() -> Bool {
        let index = self.selectedIndex
        if index == nil {
            return true
        }
        
        if index! == self.regionData.count - 1 {
            return false
        }
        
        let selectedLevel = self.levelAtIndex(index!)
        let levelAtNextIndex = self.levelAtIndex(index! + 1)
        
        return levelAtNextIndex > selectedLevel
    }
    
    
}
