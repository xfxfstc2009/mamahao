//
//  MMHProfile.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface MMHProfile : NSObject

@property (nonatomic) MMHUserIdentity userIdentity;
@property (nonatomic, strong) NSArray *children;

- (id)initWithUserIdentity:(MMHUserIdentity)identity childDictionary:(NSDictionary *)dictionary;

- (BOOL)hasAnyChild;
@end
