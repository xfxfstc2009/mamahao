//
//  MMHChild.h
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMHLogger.h"


@interface MMHChild : NSObject

@property (nonatomic, strong) NSDate *birthday;

@property (nonatomic) NSInteger gender;

@property (nonatomic, copy) NSString *portraitURLString;

@property (nonatomic, copy) NSString *nickname;

- (id)initWithDictionary:(NSDictionary *)dictionary;
@end
