//
//  MMHProduct.h
//  MamHao
//
//  Created by Louis Zhu on 15/4/10.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHFetchModel.h"
#import "MMHProductProtocol.h"
#import "MMHCategory.h"


@class MMHProduct;


@protocol MMHProductDelegate <NSObject>

- (void)product:(MMHProduct *)product actualPriceFetched:(MMHPrice)price;

@end



@interface MMHProduct : MMHFetchModel

@property (nonatomic, weak) id<MMHProductDelegate> delegate;

@property (nonatomic, copy) NSString *templateId;
@property (nonatomic) BOOL hasOwnPrice;
@property (nonatomic) MMHPrice actualPrice;

- (MMHCategory *)category;
- (NSString *)shopId;

- (void)tryToFetchActualPrice;
@end
