//
//  AppDelegate.m
//  MamHao
//
//  Created by SmartMin on 15/3/30.
//  Copyright (c) 2015年 SmartMin. All rights reserved.
//

#import "AppDelegate.h"
#import "MMHTestViewController.h"
#import "MMHLoginViewController.h"
#import "MMHAccountSession.h"
#import "MMHStartAnimationViewController.h"                 // 开场动画

// shareSDK
#import <ShareSDK/ShareSDK.h>
#import "WXApi.h"
#import "WeiboSDK.h"
// QQ 和 QQ空间
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
// 开启QQ和Facebook网页授权需要
#import <QZoneConnection/QZoneConnection.h>
#import "MMHAppearance.h"
#import "MMHTabBarController.h"
#import "EaseMob.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

// 支付宝
#import <AlipaySDK/AlipaySDK.h>

// map
#import <MAMapKit/MAMapKit.h>

// 新特性界面后选择妈妈状态
#import "MMHTestViewController.h"
#import "MMHWriteBabyInfoAnimationViewController.h"
#import "MobClick.h"
#import "MMHUserAgent.h"
#import "MMHChattingSession.h"
#import "MMHLocationSyncManager.h"
#import "MMHLogbook.h"


@interface AppDelegate ()<WXApiDelegate,CLLocationManagerDelegate>{
}

@end


@implementation AppDelegate



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [MMHAccountSession start];
    [MMHLocationSyncManager sharedLocationSyncManager];
    [MMHLogbook start];                                     // 埋点
    [self shareSDKConnectApp];                              // shareSDKAPIKey
    [self createMap];                                       // 高德地图APIKey
    [[EaseMob sharedInstance] registerSDKWithAppKey:@"mamahao#mamahao" apnsCertName:nil];
    [[MMHChattingSession currentSession] unreadMessageCount];
    [[EaseMob sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    [MMHAppearance configureGlobalAppearancesWithWindow:self.window];
    [MMHUserAgent configureGlobalUserAgent];

    [Fabric with:@[CrashlyticsKit]];
    [self createStartAnimation];                            // 开场动画
    [MMHTool userDefaulteWithKey:MMHIsOpenLocation Obj:@"No"];
    return YES;
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



#pragma mark - shareSDK
-(void)shareSDKConnectApp{
    [ShareSDK registerApp:@"891a1be9fe94"];                        // APIKey
    [self initializePlat];
}
- (void)initializePlat
{
    /**
     连接新浪微博开放平台应用以使用相关功能，此应用需要引用SinaWeiboConnection.framework
     http://open.weibo.com上注册新浪微博开放平台应用，并将相关信息填写到以下字段
     **/
    [ShareSDK connectSinaWeiboWithAppKey:@"568898243"
                               appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                             redirectUri:@"http://www.sharesdk.cn"];
    
    [ShareSDK connectSinaWeiboWithAppKey:@"568898243"
                               appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                             redirectUri:@"http://www.sharesdk.cn"
                             weiboSDKCls:[WeiboSDK class]];
    
    /**
     连接QQ空间应用以使用相关功能，此应用需要引用QZoneConnection.framework
     http://connect.qq.com/intro/login/上申请加入QQ登录，并将相关信息填写到以下字段
     
     如果需要实现SSO，需要导入TencentOpenAPI.framework,并引入QQApiInterface.h和TencentOAuth.h，将QQApiInterface和TencentOAuth的类型传入接口
     **/
    [ShareSDK connectQZoneWithAppKey:@"100371282"
                           appSecret:@"aed9b0303e3ed1e27bae87c33761161d"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    /**
     连接微信应用以使用相关功能，此应用需要引用WeChatConnection.framework和微信官方SDK
     http://open.weixin.qq.com上注册应用，并将相关信息填写以下字段
     **/
    //    [ShareSDK connectWeChatWithAppId:@"wx4868b35061f87885" wechatCls:[WXApi class]];
    [ShareSDK connectWeChatWithAppId:@"wx49f25cc1d77c9dd3"
                           appSecret:@"aef574b855dbb54b736eab080a5a2ada"
                           wechatCls:[WXApi class]];
    /**
     连接QQ应用以使用相关功能，此应用需要引用QQConnection.framework和QQApi.framework库
     http://mobile.qq.com/api/上注册应用，并将相关信息填写到以下字段
     **/
    //旧版中申请的AppId（如：QQxxxxxx类型），可以通过下面方法进行初始化
    //    [ShareSDK connectQQWithAppId:@"QQ075BCD15" qqApiCls:[QQApi class]];
    
    [ShareSDK connectQQWithQZoneAppKey:@"100371282"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //连接拷贝
    [ShareSDK connectCopy];
    //连接短信分享
    [ShareSDK connectSMS];
}

#pragma mark - 如果使用SSO（可以简单理解成客户端授权），以下方法是必要的
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [ShareSDK handleOpenURL:url  wxDelegate:self];
}

//iOS 4.2+
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([url.scheme isEqualToString:@"mamahao"]) {
        if ([url.host isEqualToString:@"shaolingongfuhaoyezhendihaotashitietougongwuditietougong"]) {
            NSDictionary *parameters = [url queryParameters];
            NSLog(@"oh xixi: %@", parameters);
            if ([parameters hasKey:@"server"]) {
                [[NSUserDefaults standardUserDefaults] setObject:parameters forKey:MMHUserDefaultsKeyServerAddress];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
    }
    else if ([url.scheme isEqualToString:@"mamhao-alipay"]) {
        if ([url.host isEqualToString:@"safepay"]) {
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@", resultDic);
            }];
        }
        else if ([url.host isEqualToString:@"platformapi"]) {//支付宝钱包快登授权返回 authCode
            [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@", resultDic);
            }];
        }
    }
    else {    // shareSDK
        return [ShareSDK handleOpenURL:url sourceApplication:sourceApplication annotation:annotation wxDelegate:self];
    }
    return YES;
}


- (void)userInfoUpdateHandler:(NSNotification *)notif {
    NSMutableArray *authList = [NSMutableArray arrayWithContentsOfFile:[NSString stringWithFormat:@"%@/authListCache.plist",NSTemporaryDirectory()]];
    if (authList == nil) {
        authList = [NSMutableArray array];
    }
    
    NSString *platName = nil;
    NSInteger plat = [[[notif userInfo] objectForKey:SSK_PLAT] integerValue];
    switch (plat) {
        case ShareTypeSinaWeibo:
            platName = NSLocalizedString(@"TEXT_SINA_WEIBO", @"新浪微博");
            break;
        case ShareTypeDouBan:
            platName = NSLocalizedString(@"TEXT_DOUBAN", @"豆瓣");
            break;
        case ShareTypeFacebook:
            platName = @"Facebook";
            break;
        case ShareTypeKaixin:
            platName = NSLocalizedString(@"TEXT_KAIXIN", @"开心网");
            break;
        case ShareTypeQQSpace:
            platName = NSLocalizedString(@"TEXT_QZONE", @"QQ空间");
            break;
        case ShareTypeRenren:
            platName = NSLocalizedString(@"TEXT_RENREN", @"人人网");
            break;
        case ShareTypeTencentWeibo:
            platName = NSLocalizedString(@"TEXT_TENCENT_WEIBO", @"腾讯微博");
            break;
        case ShareTypeTwitter:
            platName = @"Twitter";
            break;
        case ShareTypeInstapaper:
            platName = @"Instapaper";
            break;
        case ShareTypeYouDaoNote:
            platName = NSLocalizedString(@"TEXT_YOUDAO_NOTE", @"有道云笔记");
            break;
        case ShareTypeGooglePlus:
            platName = @"Google+";
            break;
        case ShareTypeLinkedIn:
            platName = @"LinkedIn";
            break;
        default:
            platName = NSLocalizedString(@"TEXT_UNKNOWN", @"未知");
    }
    
    id<ISSPlatformUser> userInfo = [[notif userInfo] objectForKey:SSK_USER_INFO];
    BOOL hasExists = NO;
    for (int i = 0; i < [authList count]; i++) {
        NSMutableDictionary *item = [authList objectAtIndex:i];
        ShareType type = (ShareType)[[item objectForKey:@"type"] integerValue];
        if (type == plat) {
            [item setObject:[userInfo nickname] forKey:@"username"];
            hasExists = YES;
            break;
        }
    }
    
    if (!hasExists) {
        NSDictionary *newItem = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 platName,
                                 @"title",
                                 [NSNumber numberWithInteger:plat],
                                 @"type",
                                 [userInfo nickname],
                                 @"username",
                                 nil];
        [authList addObject:newItem];
    }
    
    [authList writeToFile:[NSString stringWithFormat:@"%@/authListCache.plist",NSTemporaryDirectory()] atomically:YES];
}


#pragma mark 开场动画
-(void)createStartAnimation{
    // 1. 判断是否第一次使用此版本
    NSString *versionKey = @"CFBundleShortVersionString";
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:versionKey];
    NSString *currentVersionCode = [[NSBundle mainBundle].infoDictionary objectForKey:versionKey];
    if ([lastVersionCode isEqualToString:currentVersionCode]) {                 // 非第一次使用软件
        [UIApplication sharedApplication].statusBarHidden = NO ;
        [self startAnimtion:NO];
    } else {                                                                    // 第一次使用软件
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:versionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 跳转开场动画
        MMHStartAnimationViewController *startAnimationViewController = [[MMHStartAnimationViewController alloc] init];
        startAnimationViewController.startBlock = ^(BOOL isBundingBaby){
            [self startAnimtion:isBundingBaby];                 // 是否绑定宝贝
        };
        self.window.rootViewController = startAnimationViewController;
    }
}

#pragma mark 跳转主界面
-(void)startAnimtion:(BOOL)isBundingBaby{
    [UIApplication sharedApplication].statusBarHidden = YES;
        MMHTabBarController *tabBarController = [[MMHTabBarController alloc] init];
    if (isBundingBaby){                                 // 绑定宝贝
        [MMHTool userDefaulteWithKey:@"isShowAnimation" Obj:@"Yes"];
    } else {                                            // 跳转主页
        [MMHTool userDefaulteWithKey:@"isShowAnimation" Obj:@"No"];
    }
    self.window.rootViewController = tabBarController;
}

#pragma mark - 高德地图
-(void)createMap{
    [MAMapServices sharedServices].apiKey = (NSString *)MMHAMapKey;
}



@end
