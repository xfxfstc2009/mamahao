//
//  Constants.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#ifndef MamHao_Constants_h
#define MamHao_Constants_h


#import <Foundation/Foundation.h>


#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define clickZanPlistName @"collect.plist"
#define ADDRESS_PLIST_NAME @"area.plist"
#define ADDRESS_TXT_NAME  @"regions"
#define kSystemCookie    @"systemCookie"

// 错误
#define NETWORK_ERR_PROMPT  @"亲，您的网络似乎开了点小差！"


//
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)

// DLog
#ifndef  DEBUG
#define DLog(fmt,...) {NSLog((@"%s [Line:%d]" fmt),__PRETTY_FUNCTION__,__LINE__,##__VA_ARGS__);}
#else
#define DLog(...)
#endif


// style
#define kStyleBackgroundColor [UIColor hexChangeFloat:@"F6F6F6"]
#define kTableView_Margin 16
#define kStyleFont  [UIFont systemFontOfSize:mmh_relative_float(18)]


#pragma mark - Basics


#define kScreenBounds               [[UIScreen mainScreen] bounds]
#define kApplicationFrame           [[UIScreen mainScreen] applicationFrame]


#define kScreenScale                ([UIScreen instancesRespondToSelector:@selector(scale)]?[[UIScreen mainScreen] scale]:(1.0f))
#define kScreenWidth                ([[UIScreen mainScreen] applicationFrame].size.width)

#define kUserInterfaceIdiomIsPhone  (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kUserInterfaceIdiomIsPad    (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define kScreenIs35InchRetina       (([UIScreen mainScreen].scale == 2.0f) && (CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320.0f, 480.0f))))
#define kScreenIs4InchRetina        (([UIScreen mainScreen].scale == 2.0f) && (CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(320.0f, 568.0f))))
#define kScreenIs47InchRetina       (([UIScreen mainScreen].scale == 2.0f) && (CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(375.0f, 667.0f))))
#define kScreenIs55InchRetinaHD     (([UIScreen mainScreen].scale == 3.0f) && (CGSizeEqualToSize([UIScreen mainScreen].bounds.size, CGSizeMake(414.0f, 736.0f))))

#define ios7 ([[UIDevice currentDevice].systemVersion doubleValue] >= 7.0)
#define ios8 ([[UIDevice currentDevice].systemVersion doubleValue] >= 8.0)
#define ios6 ([[UIDevice currentDevice].systemVersion doubleValue] >= 6.0 && [[UIDevice currentDevice].systemVersion doubleValue] < 7.0)
#define ios5 ([[UIDevice currentDevice].systemVersion doubleValue] < 6.0)
#define iphone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iphone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iphone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define iphone4  ([UIScreen mainScreen].bounds.size.height == 480)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)


#define kPathTemp                   NSTemporaryDirectory()
#define kPathDocuments              [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]


#define kSystemVersion              [[UIDevice currentDevice] systemVersion]
#define kSystemVersionProiorToIOS6  ([kSystemVersion compare:@"6.0" options:NSNumericSearch range:NSMakeRange(0, 3)] == NSOrderedAscending)


#define kNoneNetworkReachable       ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable)



static NSString *const MMHErrorDomain = @"MMHErrorDomain";
static NSString *const MMHCurrentLocationNotification = @"MMHCurrentLocationNotification";

static NSString *const MMHUserDidLogoutNotification = @"MMHUserDidLogoutNotification";
static NSString *const MMHChildInfoEditedNotification = @"MMHChildInfoEditedNotification";

// 获取当前默认地址
static NSString *const MMHGetNormalAddressNotification = @"MMHGetNormalAddressNotification";
static NSString *const MMHLocationChangedNotification = @"MMHLocationChangedNotification";

// 支付宝
//9000 订单支付成功
//8000 正在处理中
//4000 订单支付失败
//6001 用户中途取消
//6002 网络连接出错
static NSString *const MMHAlipayNotification = @"MMHAlipayNotification";/**< 支付宝支付通知*/


typedef float MMHPrice;
static MMHPrice const MMHPriceZero = 0.0f;
typedef long MMHID;


typedef NS_ENUM(NSInteger, MMHActivityType) {
    MMHActivityTypeReachThenReduce = 1, // 满减
    MMHActivityTypeReachThenRebate = 2, // 满折
};


typedef NS_ENUM(NSInteger, MMHUserIdentity) {
    MMHUserIdentityUnspecified = 0,
    MMHUserIdentityPregnant = 1,
    MMHUserIdentityMom = 2,
};


typedef NS_ENUM(NSInteger, MMHGender) {
    MMHGenderUnspecified = 0,
    MMHGenderMale = 1,
    MMHGenderFemale = 2,
};


static NSString *const MMHUserDefaultsKeyServerAddress = @"MMHUserDefaultsKeyServerAddress";
//static NSString *const MMHUserDefaultsKeyMyAvatarURLString = @"MMHUserDefaultsKeyMyAvatarURLString";

// 判断是否开启定位
static NSString *const MMHIsOpenLocation = @"MMHIsOpenLocation";

#pragma mark 确认订单页面通知
static NSString *const MMHConfirmationNotificationWithMBean = @"MMHConfirmationNotificationWithMBean";/**< 确认订单妈豆通知*/
static NSString *const MMHConfirmationNotificationWithGB = @"MMHConfirmationNotificationWithGB";/**< 确认订单GB积分通知*/
static NSString *const MMHConfirmationNotificationWithMC = @"MMHConfirmationNotificationWithMC";/**< 确认订单MC积分通知*/
static NSString *const MMHConfirmationNotificationWithShare = @"MMHConfirmationNotificationWithShare";/**<分享*/
#endif
