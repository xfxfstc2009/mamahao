//
//  Constants.h
//  MamHao
//
//  Created by SmartMin on 15/3/31.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#ifndef MamHao_Constants_h
#define MamHao_Constants_h


// 错误
#define NETWORK_ERR_PROMPT  @"亲，您的网络似乎开了点小差！"

//
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)

// DLog
#ifndef  DEBUG
#define DLog(fmt,...) {NSLog((@"%s [Line:%d]" fmt),__PRETTY_FUNCTION__,__LINE__,##__VA_ARGS__);}
#else
#define DLog(...)
#endif














//***************** 接口 *****************//
#define kTest               @"gd-app-api/member/login.htm"              // 测试


#endif
