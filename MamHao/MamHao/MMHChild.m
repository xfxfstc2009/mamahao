//
//  MMHChild.m
//  MamHao
//
//  Created by Louis Zhu on 15/7/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHChild.h"
#import "MMHNetworkAdapter+Product.h"


@interface MMHChild ()

@property (nonatomic, copy) NSString *childId;
@property (nonatomic, copy) NSString *memberId;
@end


@implementation MMHChild


- (id)initWithDictionary:(NSDictionary *)dictionary {
    self = [self init];
    if (self) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy-MM-dd";
        NSString *birthdayString = [dictionary stringForKey:@"babyBirthday"];
        self.birthday = [dateFormatter dateFromString:birthdayString];
        self.gender = [dictionary integerForKey:@"babyGender"];
        self.portraitURLString = [dictionary stringForKey:@"babyImg"];
        self.nickname = [dictionary stringForKey:@"babyNickName"];
        
        self.childId = [dictionary stringForKey:@"babyId"];
        self.memberId = [dictionary stringForKey:@"memberId"];
    }
    return self;
}
@end
